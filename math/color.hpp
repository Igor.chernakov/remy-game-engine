#pragma once

#include "math.hpp"

namespace math
{
	struct Color
	{
		static const int Identity = 0xFF;

		Color() : color(0xFFFFFFFF)
		{
		}

		Color(float _r, float _g, float _b, float _a = 1.0f)
		{
			SetRed(_r);
			SetGreen(_g);		
			SetBlue(_b);
			SetAlpha(_a);
		}

		Color(unsigned int color) : color(color)
		{
		}

		Color(const std::string& name);

		inline float GetRed() const { return v[2] / float(Identity); }
		inline float GetGreen() const { return v[1] / float(Identity); }
		inline float GetBlue() const { return v[0] / float(Identity); }
		inline float GetAlpha() const { return v[3] / float(Identity); }
		inline void SetRed(float value) { v[2] = int(MIN(1.0f, MAX(0.0f, value)) * Identity); }
		inline void SetGreen(float value) { v[1] = int(MIN(1.0f, MAX(0.0f, value)) * Identity); }
		inline void SetBlue(float value) { v[0] = int(MIN(1.0f, MAX(0.0f, value)) * Identity); }
		inline void SetAlpha(float value) { v[3] = int(MIN(1.0f, MAX(0.0f, value)) * Identity); }
		
		// a helper function for OpenGL
		inline void CopyBGRA(const Color& color)
		{
			r = color.b;
			g = color.g;
			b = color.r;
			a = color.a;
		}

		inline operator Vector4() const
		{
			return Vector4(GetRed(), GetGreen(), GetBlue(), GetAlpha());
		}

		inline Color operator* (float k) const
		{
			return *this * Color(k, k, k);
		}

		inline Color operator* (const Color& right) const
		{
			Color out;

			out.a = (unsigned char)((int)a * (int)right.a / Identity);
			out.r = (unsigned char)((int)r * (int)right.r / Identity);
			out.g = (unsigned char)((int)g * (int)right.g / Identity);
			out.b = (unsigned char)((int)b * (int)right.b / Identity);

			return out;
		}

		inline Color operator+ (const Color& right) const
		{
			Color out;

			out.a = (unsigned char)MIN((int)a + (int)right.a, 255);
			out.r = (unsigned char)MIN((int)r + (int)right.r, 255);
			out.g = (unsigned char)MIN((int)g + (int)right.g, 255);
			out.b = (unsigned char)MIN((int)b + (int)right.b, 255);

			return out;
		}

		inline static Color Lerp(float d, const Color& left, const Color& right)
		{
			Color result;
			result.SetRed(left.GetRed() * (1 - d) + right.GetRed() * d);
			result.SetGreen(left.GetGreen() * (1 - d) + right.GetGreen() * d);
			result.SetBlue(left.GetBlue() * (1 - d) + right.GetBlue() * d);
			result.SetAlpha(left.GetAlpha() * (1 - d) + right.GetAlpha() * d);
			return result;
		}

		inline bool operator== (const Color& right) const
		{
			return color == right.color;
		}

		inline bool operator!= (const Color& right) const
		{
			return color != right.color;
		}

		inline Color& operator*= (const Color& right)
		{
			return *this = *this * right;
		}

		union
		{
			int color;
			unsigned char v[4];
			struct { unsigned char b, g, r, a; };
		};
	};
}