#pragma once

#include <boost/shared_ptr.hpp>
#include <vector>

#include "../interfaces/imaterial.hpp"
#include "../interfaces/isurface.hpp"

using boost::shared_ptr;

namespace client
{
	struct Surface: ISurface
	{
		Surface() :
			pIndices(0),
			pVertices(0),
			pMatrix(0),
			nVertices(0),
			nIndices(0)
		{
		}


		virtual const ITexture* GetLightmap() const { return pLightmap.get(); }
		virtual const IMaterial* GetMaterial() const { return pMaterial.get(); }

		virtual const vertex_t* GetVertices() const { return pVertices; }
		virtual const index_t* GetIndices() const { return pIndices; }
		
		virtual size_t GetVerticesCount() const { return nVertices; }
		virtual size_t GetIndicesCount() const { return nIndices; }
		
		virtual const mat4_t* GetMatrix() const { return pMatrix; }
		
		virtual const IRendererBuffer* GetVertexBuffer() const { return 0; }
		virtual const IRendererBuffer* GetIndexBuffer() const { return 0; }

		shared_ptr<const IMaterial> pMaterial;
		shared_ptr<const ITexture> pLightmap;

		const index_t* pIndices;
		const vertex_t* pVertices;
		const mat4_t *pMatrix;
		size_t nVertices;
		size_t nIndices;

		static Surface CreateBox(vertex_t* v, const vec3_t& x, const vec3_t& y, const vec3_t& z);
		static Surface CreateQuad(vertex_t* v, const box2_t& quad, const box2_t& uv = box2_t(0,0,1,1));

		void SetColor(const color_t& value);
	};

	namespace surface
	{
		class Mesh: public ISurface
		{
		public:
			Mesh(): userdata(0), use_matrix(true) {}

			Mesh& AddBox(const vec3_t& x, const vec3_t& y, const vec3_t& z);
			Mesh& AddQuad(const box2_t& quad, const box2_t& uv = box2_t(0,0,1,1), const color_t& color = -1);

			const vertex_t* GetVertices() const { return &*m_vertices.begin(); }
			const index_t* GetIndices() const { return m_indices.size() ? &*m_indices.begin() : 0; }
			const IRendererBuffer* GetVertexBuffer() const { return 0; }
			const IRendererBuffer* GetIndexBuffer() const { return 0; }
			size_t GetVerticesCount() const { return m_vertices.size(); }
			size_t GetIndicesCount() const { return m_indices.size(); }
			const IMaterial* GetMaterial() const { return material.get(); }
			const ITexture* GetLightmap() const { return 0; }
			const mat4_t* GetMatrix() const { return use_matrix ? &matrix : 0; }

			void SetColor(const color_t& value);

			shared_ptr<const IMaterial> material;
			mat4_t matrix;

			bool use_matrix;
		//private:
			std::vector<vertex_t> m_vertices;
			std::vector<index_t> m_indices;

			int userdata;
		};
	}
}