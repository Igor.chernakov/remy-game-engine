#pragma once

#include "iobject.hpp"
#include "itexture.hpp"

struct ILayer: IObject
{
	enum Blend
	{
		BLEND_UNKNOWN = 0,
		BLEND_ZERO = 1,
		BLEND_ONE = 2,
		BLEND_SRCCOLOR = 3,
		BLEND_INVSRCCOLOR = 4,
		BLEND_SRCALPHA = 5,
		BLEND_INVSRCALPHA = 6,
		BLEND_DESTALPHA = 7,
		BLEND_INVDESTALPHA = 8,
		BLEND_DESTCOLOR = 9,
		BLEND_INVDESTCOLOR = 10,
		BLEND_SRCALPHASAT = 11,
		BLEND_BOTHSRCALPHA = 12,
		BLEND_BOTHINVSRCALPHA = 13,
	};

	enum Filter
	{
		FILTER_NONE = 0, // filter disabled (valid for mip filter only)
		FILTER_POINT = 1, // nearest
		FILTER_LINEAR = 2, // linear interpolation
		FILTER_ANISOTROPIC = 3, // anisotropic
		FILTER_FLATCUBIC = 4, // cubic
		FILTER_GAUSSIANCUBIC = 5, // different cubic kernel
	};

	//vaiable definitions

	enum Addressing
	{
		ADDRESS_REPEAT,
		ADDRESS_CLAMP,
	} address;

	enum Type
	{
		TYPE_DEFAULT,
		TYPE_LIGHTMAP,
	} type;

	enum TcGen
	{
		TCGEN_VERTEX,
		TCGEN_ENVIRONMENT
	} tcgen;

	struct Blending
	{
		Blend src, dst;
	} blend;

	struct Filtering
	{
		Filter mag, min, mip;
	} filter;

	float anim_speed;

	virtual const ITexture* GetTexture(int i) const = 0;
	virtual int GetTexturesCount() const = 0;
	virtual int GetWidth() const = 0;
	virtual int GetHeight() const = 0;
};

