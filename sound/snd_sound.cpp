#include "../interfaces/iclientframework.hpp"
#include "../shared/resource.hpp"

#include "../openal/include/al.h"
#include "../openal/include/alc.h"
#include "../oggvorbis/vorbis/vorbisfile.h"

#include "ogg_stream.h"

#ifndef WIN32
#	include <sys/time.h>
#	define Sleep sleep
#else
#	include <windows.h>
#endif

float g_SoundVolume = 0.75f;
float g_MusicVolume = 0.75f;

bool g_musicEnabled = true;

namespace sound {
	
	IClientFramework* framework;
	
#ifndef MIN
#	define MIN(x, y) ((x) < (y) ? (x) : (y))
#endif
	
#ifndef MAX
#	define MAX(x, y) ((x) > (y) ? (x) : (y))
#endif
	
	static enum
	{
  Unintialized,                 /* ALUT has not been initialized yet or has been de-initialised */
  ALUTDeviceAndContext,         /* alutInit has been called successfully */
  ExternalDeviceAndContext      /* alutInitWithoutContext has been called */
	} initialisationState = Unintialized;
	
	static ALCcontext *alutContext;
	
	bool mySoundSanityCheck (void)
	{
		ALCcontext *context;
		
		if (initialisationState == Unintialized)
		{
			return false;
		}
		
		context = alcGetCurrentContext ();
		if (context == NULL)
		{
			return false;
		}
		
		if (alGetError () != AL_NO_ERROR)
		{
			return false;
		}
		
		if (alcGetError (alcGetContextsDevice (context)) != ALC_NO_ERROR)
		{
			return false;
		}
		
		return true;
	}
	
	bool mySoundInit()
	{
		ALCdevice *device;
		ALCcontext *context;
		
		if (initialisationState != Unintialized)
		{
			return false;
		}
		
		device = alcOpenDevice (NULL);
		if (device == NULL)
		{
			return false;
		}
		
		context = alcCreateContext (device, NULL);
		if (context == NULL)
		{
			alcCloseDevice (device);
			return false;
		}
		
		if (!alcMakeContextCurrent (context))
		{
			alcDestroyContext (context);
			alcCloseDevice (device);
			return false;
		}
		
		initialisationState = ALUTDeviceAndContext;
		alutContext = context;
		return true;
	}
	
	bool mySoundExit()
	{
		ALCdevice *device;
		
		if (initialisationState == Unintialized)
		{
			return false;
		}
		
		if (initialisationState == ExternalDeviceAndContext)
		{
			initialisationState = Unintialized;
			return true;
		}
		
		if (!mySoundSanityCheck ())
		{
			return false;
		}
		
		if (!alcMakeContextCurrent (NULL))
		{
			return false;
		}
		
		device = alcGetContextsDevice (alutContext);
		alcDestroyContext (alutContext);
		if (alcGetError (device) != ALC_NO_ERROR)
		{
			return false;
		}
		
		if (!alcCloseDevice (device))
		{
			return false;
		}
		
		initialisationState = Unintialized;
		return true;
	}
	
	class SoundSystem: public ISoundSystem
	{
	public:
		SoundSystem(): bSound(true), bMusic(true)
		{
			if (!mySoundInit()) {
				framework->Log(IFramework::MSG_ERROR, "Failed to initialize OpenAL");
				return;
			}
			alListener3f(AL_POSITION, 0.0f, 0.0f, 0.0f);
		}
		~SoundSystem()
		{
			mySoundExit();
		}
		
		virtual boost::shared_ptr<const ISound> GetMusic() const;
		virtual void Activate(bool enable);
		
		virtual boost::shared_ptr<const ISound> LoadSound(const std::string& path);
		virtual boost::shared_ptr<const ISound> LoadMusic(const std::string& path);
		
		virtual bool IsSoundEnabled() const { return bSound; }
		virtual bool IsMusicEnabled() const { return bMusic; }
		virtual void SetSoundEnabled(bool value) { bSound = value; }
		virtual void SetMusicEnabled(bool value);// { bMusic = value; }
		
		virtual void Update(float timestep);
		
		bool bSound, bMusic;
		
		boost::weak_ptr<const class Music> music;
		
		static SoundSystem* instance;
	};
	
	SoundSystem* SoundSystem::instance = 0;
	
	class Sound
	: public ISound
	, public shared::Resource
	{
	protected:
		mutable bool enabled;
		mutable float volume;
		
		ALenum m_format;                          // The sound data m_format
		ALsizei m_freq;                           // The frequency of the sound data
		ALuint m_bufferID;                        // The OpenAL sound buffer ID
		ALuint m_sourceID;                        // The OpenAL sound source
		
	public:
		Sound(const std::string& path)
		: Resource(path)
		, m_format(0)
		, m_freq(0)
		, m_bufferID(0)
		, m_sourceID(0)
		, volume(1.f)
		, enabled(true)
		{
			int endian = 0;                         // 0 for Little-Endian, 1 for Big-Endian
			int bitStream;
			long bytes;
			char array[BUFFER_SIZE];                // Local fixed size array
			
			file_t file = framework->GetFileSystem()->LoadFile(path);
			
			if (!file) {
				framework->Log(IFramework::MSG_ERROR, "Cannot open %s for reading.", path.c_str());
				return;
			}
			
			OggVorbis_File oggFile;
			
			if (ov_open(file, &oggFile, NULL, 0) != 0)
			{
				framework->Log(IFramework::MSG_ERROR, "Cannot open %s for decoding.", path.c_str());
				exit(-1);
			}
			
			// Get some information about the OGG file
			vorbis_info *pInfo = ov_info(&oggFile, -1);
			
			// Check the number of channels... always use 16-bit samples
			if (pInfo->channels == 1)
				m_format = AL_FORMAT_MONO16;
			else
				m_format = AL_FORMAT_STEREO16;
			// end if
			
			// The frequency of the sampling rate
			m_freq = pInfo->rate;
			
			static std::vector<char> m_buffer(BUFFER_SIZE * 10);
			m_buffer.clear();
			
			// Keep reading until all is read
			do
			{
				// Read up to a buffer's worth of decoded sound data
				bytes = ov_read(&oggFile, array, BUFFER_SIZE, endian, 2, 1, &bitStream);
				
				if (bytes < 0)
				{
					ov_clear(&oggFile);
					framework->Log(IFramework::MSG_ERROR, "Error decoding %s", path.c_str());
					return;
				}
				// end if
				
				// Append to end of buffer
				m_buffer.insert(m_buffer.end(), array, array + bytes);
			}
			while (bytes > 0);
			
			// Clean up!
			ov_clear(&oggFile);
			
			// Create sound buffer and source
			alGenBuffers(1, &m_bufferID);
			alGenSources(1, &m_sourceID);
			
			// Upload sound data to buffer
			alBufferData(m_bufferID, m_format, &m_buffer[0], static_cast<ALsizei>(m_buffer.size()), m_freq);
			
			// Attach sound buffer to source
			alSourcei(m_sourceID, AL_BUFFER, m_bufferID);
			alSource3f(m_sourceID, AL_POSITION, 0.0f, 0.0f, 0.0f);
		}
		
		~Sound() {
			if (m_sourceID) {
				/*int queued = 0;
				 alGetSourcei(m_sourceID, AL_BUFFERS_QUEUED, &queued);
				 while(queued--) {
				 ALuint buffer;
				 alSourceUnqueueBuffers(m_sourceID, 1, &buffer);
				 }*/
				alDeleteSources(1, &m_sourceID);
			}
			if (m_bufferID) {
				alDeleteBuffers(1, &m_bufferID);
			}
		}
		
		bool IsLoaded() const {
			return m_bufferID != 0;
		}
		
		void Play(float volume, bool loop = false) const {
			this->volume = volume;
			
			bool zero_volume = false;
			
			if (!IsEnabled())
			{
				if (SkipIfDisabled())
				{
					return;
				}
				else
				{
					zero_volume = true;
				}
			}
			//ALint state;
			//do {
			//	alGetSourcei(sourceID, AL_SOURCE_STATE, &state);
			//}
			//while (state != AL_STOPPED);
			alSourcei(m_sourceID, AL_LOOPING, loop);
			alSourcef(m_sourceID, AL_GAIN, (zero_volume ? 0 : volume) * g_SoundVolume);
			alSourcePlay(m_sourceID);
		}
		
		void Stop() const {
			alSourceStop(m_sourceID);
		}
		
		void Pause() const {
			alSourcePause(m_sourceID);
		}
		
		virtual void SetVolume(float volume) const {
			this->volume = volume;
			alSourcef(m_sourceID, AL_GAIN, volume * g_SoundVolume);
		}
		
		virtual float GetVolume() const {
			return this->volume;
		}
		
		virtual void Enable(bool set) const {
			enabled = set && IsEnabled();
			alSourcef(m_sourceID, AL_GAIN, (enabled ? this->volume : 0) * g_SoundVolume);
		}
		
		virtual bool IsEnabled() const {
			return SoundSystem::instance->IsSoundEnabled();
		}
		
		virtual bool SkipIfDisabled() const {
			return true;
		}
		
		virtual float GetLength() const {
			ALint sizeInBytes;
			ALint channels;
			ALint bits;
			
			alGetBufferi(m_bufferID, AL_SIZE, &sizeInBytes);
			alGetBufferi(m_bufferID, AL_CHANNELS, &channels);
			alGetBufferi(m_bufferID, AL_BITS, &bits);
			
			if (channels == 0 || bits == 0)
				return 5.f;
			
			ALint lengthInSamples = sizeInBytes * 8 / (channels * bits);
			ALint frequency;
			
			alGetBufferi(m_bufferID, AL_FREQUENCY, &frequency);
			
			return frequency > 0 ? (float)lengthInSamples / (float)frequency : 5.f;
		}
		
	};
	
	struct lock_t {
		bool* lock;
		lock_t(bool& target) : lock(&target) {
			while (*lock)
				Sleep(1);
			*lock = true;
		};
		~lock_t() {
			*lock = false;
		}
	};
	
	class Music // uses streaming
	: public ISound
	, public shared::Resource
	{
		mutable ogg_stream ogg;
		mutable float volume;
		mutable bool open;
		mutable bool busy;
		
	public:
		Music(const std::string& path)
		: Resource(path)
		, volume(0)
		, open(false)
		, busy(false)
		{
		}
		
		~Music()
		{
			if (open) {
				ogg.release();
			}
		}
		
#define MUSIC_VOLUME ((g_musicEnabled && SoundSystem::instance->IsMusicEnabled()) ? volume : 0) * g_MusicVolume
		
		void Update() const
		{
			lock_t lock(busy);
			
			if(open && ogg.update()) {
				if(!ogg.playing()) {
					ogg.playback(MUSIC_VOLUME);
				}
				
				if (ogg.finished()) {
					//framework->Log(IFramework::MSG_LOG, "Rewinding music");
					//ogg.rewind();
					ogg.release();
					ogg.open(GetPath());
					ogg.playback(MUSIC_VOLUME);
				}
			}
		}
		
		virtual void Play(float volume = 1.0f, bool loop = false) const
		{
			lock_t lock(busy);
			
			if (!open) {
				ogg.open(GetPath());
			}
			
			this->volume = volume;
			//ogg.playback(SoundSystem::instance->IsMusicEnabled() ? volume : 0);
			open = true;
		}
		
		virtual void Stop() const
		{
			lock_t lock(busy);
			
			if (open) {
				ogg.release();
				open = false;
			}
		}
		
		virtual void Pause() const
		{
		}
		
		virtual bool IsLoaded() const {
			return open;
		}
		
		virtual void SetVolume(float volume) const
		{
			lock_t lock(busy);
			
			if (open) {
				this->volume = volume;
				alSourcef(ogg.source, AL_GAIN, MUSIC_VOLUME);
			}
		}
		
		virtual float GetVolume() const {
			if (!open) {
				return 0;
			}
			return this->volume;
		}
		
		virtual float GetLength() const {
			return 0;
		}
		
		virtual void Enable(bool set) const {
			lock_t lock(busy);
			g_musicEnabled = set;
			if (open) {
				alSourcef(ogg.source, AL_GAIN, MUSIC_VOLUME);
			}
		}
	};
	
	boost::shared_ptr<const ISound> SoundSystem::LoadSound(const std::string& path)
	{
		return framework->GetResources()->Load<Sound>(path);
	}
	boost::shared_ptr<const ISound> SoundSystem::LoadMusic(const std::string& path)
	{
		music = framework->GetResources()->Load<Music>(path);
		return music.lock();
	}
	
	boost::shared_ptr<const ISound> SoundSystem::GetMusic() const
	{
		return music.lock();
	}
	
	void SoundSystem::Activate(bool enable)
	{
		g_musicEnabled = enable;
		
		if (GetMusic()) {
			GetMusic()->Enable(enable);
		}
	}
	
	void SoundSystem::SetMusicEnabled(bool value)
	{
		bMusic = value;
		
		if (!music.expired())
		{
			music.lock()->Enable(value);
		}
	}
	
	
	void SoundSystem::Update(float timestep)
	{
		if (!music.expired())
		{
			music.lock()->Update();
		}
	}
}

ISoundSystem* CreateSoundSystem(IClientFramework* _framework)
{
	sound::framework = _framework;
	
	return sound::SoundSystem::instance = new sound::SoundSystem();
}
