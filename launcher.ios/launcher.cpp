#include <iostream>

#include "launcher.h"
#include "window.h"

#include "../launcher/timer.hpp"
#include "../launcher/framework.hpp"
#include "../launcher/mycout.hpp"

#include "../interfaces/iclient.hpp"
#include "../interfaces/iserver.hpp"
#include "../interfaces/ifilesystem.hpp"
#include "../interfaces/isoundsystem.hpp"

extern IFileSystem* CreateFileSystem(IFramework*);
extern ISoundSystem* CreateSoundSystem(IClientFramework*);
extern IRenderer* CreateRenderer(IClientFramework*);
extern IClient* CreateClient(int argc, char **argv, const char*, const char*, IClientFramework*, IRenderer*);

extern char documentsDirectoryC[512];

IClientFramework* clientframework = 0;
IClient* client = 0;
IRenderer* renderer = 0;

enum AppFlags {
	APPFLAG_CONSOLE = 1 << 0,
	APPFLAG_ERROR = 1 << 1,
	APPFLAG_WINDOW = 1 << 2,
	APPFLAG_DEVWINDOW = 1 << 3,
	APPFLAG_ALLOWENVPYTHON = 1 << 4,
	APPFLAG_RIGHTWINDOW = 1 << 5,
};

int g_AppFlags = 0;

bool set_fullscreen(bool fulscreen)
{
	return 0;
}

bool is_fullscreen()
{
	return true;
}

void SetErrorState()
{
	g_AppFlags |= APPFLAG_ERROR;
}

int RenderGameFrame()
{
	if (!client)
		return 0;
	return (int)client->RenderFrame();
}

void RunGame()
{
	try
	{
		ClientFramework c_framework;

		clientframework = &c_framework;

		c_framework.FileSystem.reset(CreateFileSystem(&c_framework));
		c_framework.Timer.reset(CreateTimer());
		c_framework.SoundSystem.reset(CreateSoundSystem(&c_framework));
		c_framework.Variables.reset(CreateVariables());
		c_framework.Window.reset(new VideoWindow());

		if (renderer = CreateRenderer(&c_framework))
		{
			if (client = CreateClient(0, 0, ".", documentsDirectoryC, &c_framework, renderer))
			{
				std::cout << "Starting client" << std::endl;

				client->Run();

				IClient *tmp = client;

				client = NULL;
				
				delete tmp;
			}
		}
	}
	catch (const std::exception& ex)
	{
		std::cout << ex.what() << std::endl;
	}
}