#import <Foundation/Foundation.h>

@interface iosGameCenter : NSObject {
    BOOL gameCenterAvailable;
    BOOL userAuthenticated;
}

@property (assign, readonly) BOOL gameCenterAvailable;

+ (iosGameCenter *)sharedInstance;
- (void)authenticateLocalUser;

@end
