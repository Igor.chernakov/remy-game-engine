#include "Plugin.h"

#include "ExportMesh.h"

#include <stdarg.h>

#include <iostream>
#include <fstream>
#include <algorithm>


static const char* WORLDOBJECT_NAME = "scene";
static const char* BONEOBJECT_NAME = "bee.Object";
static const char* CAMERAOBJECT_NAME = "bee.media.Camera";
static const char* MODELOBJECT_NAME = "bee.Model";

static bool bExportSelected = false;

class SceneExportClassDesc :
	public ClassDesc
{
public:
	int IsPublic(void)
	{
		return(1);
	}

	void* Create(BOOL loading = FALSE)
	{
		return(new XmlSceneExport);
	}

	const TCHAR* ClassName(void)
	{
		return("GameEngine Scene Export");
	}

	SClass_ID SuperClassID(void)
	{
		return(SCENE_EXPORT_CLASS_ID);
	}

	Class_ID ClassID(void)
	{
		return(Class_ID(0x636628c5,0x7129636b));
	}

	const TCHAR* Category(void)
	{
		return("Scene Export");
	}
};

ClassDesc* GetSceneExportClassDesc(void)
{
	return(new SceneExportClassDesc);
}

//////////////////////////////////////////////////////////////////////////////

void XmlSceneExport::StreamNode(FileOutput& file, INode* node, Interface* gi, const std::string& indent)
{
	int nodes_count = 0;

	std::string tab = "\t";

	CStr sClassName = BONEOBJECT_NAME;

	if(node->IsRootNode())
	{
		sClassName = WORLDOBJECT_NAME;

		file << indent << "<" << sClassName.data();

		goto process_children;
	} 

	Object* obj = node->EvalWorldState(gi->GetTime(), true).obj;

	switch(obj->SuperClassID())
	{
	case HELPER_CLASS_ID:
		/*
		if(obj->ClassID() == ENTITYHELPER_CLASS_ID)
		{
			obj->GetClassName(sClassName); //export game entity
		}*/
		break;
	case CAMERA_CLASS_ID:
		sClassName = CAMERAOBJECT_NAME;
		break;
	}

	file << indent << "<" << sClassName.data() << " name=\"" << node->GetName() << "\"";

	StreamNodeProps(file, node, gi->GetTime());

process_children:

	for (models_t::iterator mdl = std::find(models.begin(), models.end(), node); mdl != models.end(); mdl = std::find(++mdl, models.end(), node), nodes_count++)
	{
		std::string scenename = file.filename.substr(0, file.filename.find_last_of(".")).substr(file.filename.find_last_of("\\") + 1);;
		std::string modelname = scenename + "." + std::string(mdl->model->GetName()) + ".model";

		FileOutput::Mode filemode = FileOutput::BINARY;

		if (pFlags[FLAG_TEXTUALMESHES])
		{
			modelname += "x";
			filemode = FileOutput::TEXTUAL;
		}

		FileOutput modelfile(modelname.c_str(), filemode);

		if (nodes_count == 0)
		{
			file << ">" << std::endl;

			file << indent << tab << "<nodes>" << std::endl;
		}

		file << indent << tab << tab << "<" << MODELOBJECT_NAME << " name=\"" << mdl->model->GetName() << "\" source=\"" << modelname << "\" >";

		SkinnedMeshes meshes;

		meshes.push_back(SkinnedMesh(mdl->model, (ISkin*)mdl->skin->GetInterface(I_SKIN)));

		ProcessMeshes(modelfile, mdl->bone, meshes, gi);

		file << indent << tab << "</" << MODELOBJECT_NAME << " >";

		modelfile.close();
	}

	for(int i = 0; i < node->NumberOfChildren(); i++, nodes_count++)
	{
		if (nodes_count == 0)
		{
			file << ">" << std::endl;

			file << indent << tab << "<nodes>" << std::endl;
		}

		StreamNode(file, node->GetChildNode(i), gi, indent + tab + tab);
	}

	if(node)
	{
		std::string str;

		str = sClassName.data();
		str = str.substr(0,str.find_first_of(" "));

		CStr sClassName(str.c_str());

		if (nodes_count == 0)
		{
			file << "/>" << std::endl;
		}
		else
		{
			file << indent << tab << "</nodes>" << std::endl;
			file << indent << "</" << sClassName.data() << ">" << std::endl;
		}
	}
}

void XmlSceneExport::CollectModels(INode* node, Interface* gi)
{
	if (GetSkinModifier(node) != NULL)
	{
		model_t model;

		model.model = node;
		model.skin = GetSkinModifier(node);
		model.bone = GetRootBoneForSkinnedMesh((ISkin*)GetSkinModifier(node)->GetInterface(I_SKIN));

		models.push_back(model);
	}

	for(int i = 0; i < node->NumberOfChildren(); i++)
	{
		CollectModels(node->GetChildNode(i), gi);
	}
}


// Dialog proc
static INT_PTR CALLBACK ExportDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	XmlSceneExport *exp =(XmlSceneExport*)GetWindowLongPtr(hWnd, GWLP_USERDATA); 

	switch(msg)
	{
		case WM_INITDIALOG:
			exp =(XmlSceneExport*)lParam;

			SetWindowLongPtr(hWnd,GWLP_USERDATA,lParam); 

			CenterWindow(hWnd, GetParent(hWnd)); 
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDC_EXPORTMESHES:
					EnableWindow(GetDlgItem(hWnd, IDC_TEXTUALMESHES), IsDlgButtonChecked(hWnd, IDC_EXPORTMESHES));

					exp->pFlags[XmlSceneExport::FLAG_EXPORTMESHES] = IsDlgButtonChecked(hWnd, IDC_EXPORTMESHES);
					break;

				case IDC_TEXTUALMESHES:
					exp->pFlags[XmlSceneExport::FLAG_TEXTUALMESHES] = IsDlgButtonChecked(hWnd, IDC_TEXTUALMESHES);
					break;

				case IDOK:
					EndDialog(hWnd, 1);
					break;

				case IDCANCEL:
					EndDialog(hWnd, 0);
					break;
			}
			break;

		default:
			return FALSE;
	}

	return TRUE;
}       


int XmlSceneExport::DoExport(const TCHAR*	 name, ExpInterface*	ei, Interface*	gi, BOOL SuppressPrompts, DWORD options)
{
	if(!DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_SCENEEXPORT_DLG), gi->GetMAXHWnd(), ExportDlgProc,(LPARAM)this))
	{
		return 1;
	}

	bExportSelected =(options & SCENE_EXPORT_SELECTED) ? true : false;

	FileOutput file(name);

	file << "<?xml version=\"1.0\"?>" << std::endl;

	if (pFlags[FLAG_EXPORTMESHES])
	{
		CollectModels(gi->GetRootNode(), gi);
	}

	StreamNode(file, gi->GetRootNode(), gi, "");

	file.close();

	return(1);
}
