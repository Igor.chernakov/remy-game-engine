#include "fileoutput.h"

static std::string LowerCaseExtension(const std::string& in)
{
	std::string out = in;

	for (std::string::size_type n = out.find_last_of("."); n < out.length(); n++)
	{
		out[n] = tolower(out[n]);
	}

	return out;
}

FileOutput::FileOutput(const std::string& filename, Mode mode)
	: mode(mode)
	, filename(LowerCaseExtension(filename))
	, std::ofstream(LowerCaseExtension(filename).c_str(), (mode == TEXTUAL) ? (std::ios::out) : (std::ios::out | std::ios::binary))
{}

FileOutput::tag_t::tag_t() 
	: offset(0)
	, name("NONE")
	, node(false)
{};

FileOutput::tag_t::header_t::header_t(std::string& name, std::streamoff block_size)
	: size(block_size)
{
	memset(m_name, 0, NAMELEN);
	memcpy(m_name, name.data(), name.length() > NAMELEN ? NAMELEN : name.length());
}

void FileOutput::tag_t::StreamHeader(FileOutput* file)
{
	const std::streamoff position = file->tellp();

	std::streamoff absoffset;

	if (node == false) 
	{
		absoffset = position - offset;
	}
	else
	{
		absoffset = offset - position;
	}

	const header_t header(name, absoffset);

	file->seekp(offset - sizeof(header_t));
	file->write((char*)&header, sizeof(header_t));
	file->seekp(position);
}

void FileOutput::PopTag() 
{
	if (mode != BINARY || state.size() == 0)
	{
		throw new std::exception("Exception at PopTag().");
	}

	if (state.back().name != "NONE")
	{
		state.back().StreamHeader(this);
	}

	state.pop_back();
}

void FileOutput::PushTag(const std::string& name)
{
	tag_t tag;

	tag.name = name;

	if (state.size() > 0)
		state.back().node = true;

	seekp(tellp() + std::streamoff(sizeof(tag_t::header_t)));

	tag.offset = tellp();

	state.push_back(tag);
}

void FileOutput::Finalize()
{
}
