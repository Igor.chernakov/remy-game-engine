/**
	In-game world TEXT object

	See summary in the bottom
**/

#include "c_client.hpp"
#include "c_entity.hpp"
#include "c_material.hpp"

namespace client {
	class Text
		: public Entity
		, private Overlay
		, public boost::python::wrapper<Text>
		, public IWrapper
	{
	public:
		Text(
			const std::string& name,
			const vec3_t& position,
			const quat_t& rotation,
			const vec3_t& scale,
			const color_t& color,
			const std::wstring& text,
			const std::string& font,
			IOverlay::Align align,
			float size)
			: Entity(name, position, rotation, scale, color)
			, font(font)
			, surface(0)
			, align(align)
			, size(size)
			, text(text)
			, width(0) {
			RenderCallback = (RenderCallbackT)&Text::OnRender;
			Recreate();
		}

		const std::wstring& GetText() const { return text; }
		void SetText(const std::wstring& value) { 
			text = value;
			Recreate();
		}

		const std::string& GetFont() const { return font; }
		void SetFont(const std::string& value) { 
			font = value;
			Recreate();
		}

		Align GetAlign() const { return align; }
		void SetAlign(Align value) {
			align = value;
			Recreate();
		}

		float GetWidth() const {
			return width;
		}

		void OnRender(IClientScene* scene) const
		{
			if (!surface)
				return;

			Text* self = (Text*)this;

			if (surface::Mesh* s = &(*self->m_surfaces.begin())) {
				s->SetColor(GetColor());
				s->matrix = GetMatrix();
				if (s->GetMaterial()) {
					IMaterial::DepthFunction original = s->GetMaterial()->depthfunc;
					((Material*)&*s->GetMaterial())->depthfunc.func = IMaterial::FUNC_LESSEQUAL;
					((Material*)&*s->GetMaterial())->depthfunc.mask = false;
					((Material*)&*s->GetMaterial())->renderpass = IMaterial::RENDER_ALPHA;
					scene->PushSurface(s);
					//((Material*)&*s->GetMaterial())->depthfunc = original;
				} else {
					scene->PushSurface(s);
				}
			}
		}

		void Recreate() {
			Overlay::Erase();

			width = 0;

			if (surface = String(text, mat4_t(), font, GetColor())) {
				float _size = size / (float)Overlay::GetFont(font)->info.size;

				for (int i = 0; i < m_surfaces.begin()->m_vertices.size(); i++) {
					m_surfaces.begin()->m_vertices[i].position.y -=	(float)Overlay::GetFont(font)->common.lineHeight / 2;
					m_surfaces.begin()->m_vertices[i].position.z = -m_surfaces.begin()->m_vertices[i].position.y;
					m_surfaces.begin()->m_vertices[i].position.y = 0;
					m_surfaces.begin()->m_vertices[i].position *= _size;
					width = MAX(width, m_surfaces.begin()->m_vertices[i].position.x);
				}

				if (align == ALIGN_CENTER) {
					for (int i = 0; i < m_surfaces.begin()->m_vertices.size(); i++) 
						m_surfaces.begin()->m_vertices[i].position.x -= width / 2;
				} else if (align == ALIGN_RIGHT) {
					for (int i = 0; i < m_surfaces.begin()->m_vertices.size(); i++) 
						m_surfaces.begin()->m_vertices[i].position.x -= width;
				}

				((surface::Mesh*)surface)->use_matrix = true;
			}
		}

		float GetSize() const { return size; }
		void SetSize(float value) {
			size = value;
			Recreate();
		}

		virtual shared_ptr<client::Entity> shared_from_this()
		{
			return boost::python::extract<shared_ptr<client::Entity> >(GetPythonObject());
		}

		virtual shared_ptr<const client::Entity> shared_from_this() const
		{
			return boost::python::extract<shared_ptr<const client::Entity> >(GetPythonObject());
		}

		virtual PyObject* GetPythonObject() const
		{
			return boost::python::detail::wrapper_base_::get_owner(*this);
		}

	private:
		Align align;
		std::wstring text;
		std::string font;
		const ISurface* surface;
		float size, width;
	};
}

void export_Text()
{
	namespace py = boost::python;
	using client::Text;

	DISPOSEME(4) py::class_<Text, shared_ptr<Text>, py::bases<client::Entity>, boost::noncopyable>("Text", py::no_init)
		.def(py::init<std::string, vec3_t, quat_t, vec3_t, color_t, std::wstring, std::string, IOverlay::Align, float>((
			py::arg("id") = std::string("Model#"),
			py::arg("position") = vec3_t::Zero,
			py::arg("rotation") = quat_t::Identity,
			py::arg("scale") = vec3_t::One,
			py::arg("color") = color_t(),
			py::arg("text") = std::wstring(),
			py::arg("font") = "system",
			py::arg("align") = IOverlay::ALIGN_CENTER,
			py::arg("size") = 50)))
		.add_property("text", py_cref(&Text::GetText), &Text::SetText)
		.add_property("font", py_cref(&Text::GetFont), &Text::SetFont)
		.add_property("size", &Text::GetSize, &Text::SetSize)
		.add_property("align", &Text::GetAlign, &Text::SetAlign)
		.add_property("width", &Text::GetWidth)
		;

	py::implicitly_convertible<shared_ptr<Text>, shared_ptr<client::Entity> >();
}
