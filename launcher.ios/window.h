//
//  Header.h
//  core
//
//  Created by Igor Chernakov on 01/05/2013.
//  Copyright (c) 2013 Igor Chernakov. All rights reserved.
//

#ifndef core_Header_h
#define core_Header_h

#include "../interfaces/iwindow.hpp"

#include "launcher.h"

extern int screen_x, screen_y;

struct VideoWindow: IWindow {
	virtual void Show(bool show) {}
	virtual bool IsVisible() const { return true; }
	virtual bool Centralize() { return true; }
	virtual bool IsFullscreen() const { return false; }
	virtual void* GetWindow() const { return NULL; }
	virtual dim_t GetSize() const { return dim_t(1024, 768); }
	virtual point_t GetCursorPos() const
	{
		return point_t(mouse_x, mouse_y);
	}
	virtual void SetCursorPos(const point_t&) {}
	virtual void SetCursor(Cursor) {}
	virtual Cursor GetCursor() const { return CURSOR_ARROW; }
};

#endif
