#pragma once;

//structure for *.w3e file stuff
struct w3e_t {
	w3e_t(const BYTE **offset)
		: header(new header_t(offset))
		, data(new data_t(header, offset))
	{
		for (int x = 0; x < header->width; x++) {
			for (int y = 0; y < header->height; y++) {
				data_t::TilePoint *t = GetTilepoint(x, y);
				t->pGroundMaterial = header->pGroundMaterials[t->GetGroundID()];
				t->pCliffMaterial = header->pCliffMaterials[t->GetCliffID()];
				t->height -= TILEDEFAULTHEIGHT;
				t->c[data_t::TilePoint::CENTER] = GetTilepoint(x, y);
				t->c[data_t::TilePoint::TOP] = GetTilepoint(x, y+1);
				t->c[data_t::TilePoint::RIGHT] = GetTilepoint(x+1, y);
				t->c[data_t::TilePoint::TOP_RIGHT] = GetTilepoint(x+1, y+1);

				float fLayer = t->GetLayer();

				if (t->GetFlags() & data_t::TilePoint::FLAG_RAMP) {
					BYTE maximal_layer = 0;
					BYTE minimal_layer = -1;

					int ids[4][2] = { {-1, -1}, {1, -1}, {1, 1}, {-1, 1} };

					for (int i = 0; i < 4; i++) {
						const data_t::TilePoint *t2 = GetTilepoint(x + ids[i][0], y + ids[i][1]);
						if (!t2 || !(t2->GetFlags() & data_t::TilePoint::FLAG_RAMP))
							continue;
						maximal_layer = max(maximal_layer, t2->GetLayer());
						minimal_layer = min(minimal_layer, t2->GetLayer());
					}

					if (maximal_layer != minimal_layer)
						fLayer = (maximal_layer + minimal_layer) / 2.0f;
				}

				t->position = math::Vector3(x * TILESIZE, y * TILESIZE, (fLayer - TILEZEROLAYER) * TILELAYERHEIGHT + t->height / float(TILESUBHEIGHT) - 1.0f);

				if (t->IsCliff()) {
					int n = t->GetCliffID();

					const char sID[5] = {
						header->cliff_ids[n][0], 
						header->cliff_ids[n][1], 
						header->cliff_ids[n][2],
						header->cliff_ids[n][3],
						'\0'
					};

					const std::string *pGroundTile = CliffsSheet::Get()->GetSlot("groundTile", sID);

					if (pGroundTile) {
						int nGroundID = 0;
						for (int m = 0; m < header->ground_num; m++) {
							const char sID[5] = {
								header->ground_ids[m][0], 
								header->ground_ids[m][1], 
								header->ground_ids[m][2],
								header->ground_ids[m][3],
								'\0'
							};
							if (*pGroundTile == file::File::FormatFilename(sID)) {
								nGroundID = m;
								break;
							}
						}
						for (int i = 0; i < data_t::TilePoint::CORNERS_COUNT; i++) {
							if (!t->c[i])
								continue;
							t->c[i]->SetGroundID(nGroundID);
						}
					}
				}
			}
		}

		for (int x = 0; x < header->width; x++) {
			for (int y = 0; y < header->height; y++) {
				data_t::TilePoint *t = GetTilepoint(x, y);

				t->normal = math::Vector3(0,0,0);
				t->soften_height = t->position.z;

				if (GetTilepoint(x - 1, y) && GetTilepoint(x, y - 1))
					t->normal -= (t->position - GetTilepoint(x - 1, y)->position).cross(GetTilepoint(x, y - 1)->position - t->position).normalized();

				if (GetTilepoint(x, y - 1) && GetTilepoint(x + 1, y))
					t->normal -= (t->position - GetTilepoint(x, y - 1)->position).cross(GetTilepoint(x + 1, y)->position - t->position).normalized();

				if (GetTilepoint(x + 1, y) && GetTilepoint(x, y + 1))
					t->normal -= (t->position - GetTilepoint(x + 1, y)->position).cross(GetTilepoint(x, y + 1)->position - t->position).normalized();

				if (GetTilepoint(x, y + 1) && GetTilepoint(x - 1, y))
					t->normal -= (t->position - GetTilepoint(x, y + 1)->position).cross(GetTilepoint(x - 1, y)->position - t->position).normalized();

				t->normal.normalize();
			}
		}

		for (int i = 0; i < 2; i++) {
			for (int x = 0; x < header->width; x++) {
				for (int y = 0; y < header->height; y++) {
					data_t::TilePoint *t = GetTilepoint(x, y);

					int nPoints = 1;
					if (GetTilepoint(x - 1, y)) {
						t->soften_height += GetTilepoint(x - 1, y)->soften_height;
						nPoints++;
					}
					if (GetTilepoint(x + 1, y)) {
						t->soften_height += GetTilepoint(x + 1, y)->soften_height;
						nPoints++;
					}
					if (GetTilepoint(x, y - 1)) {
						t->soften_height += GetTilepoint(x, y - 1)->soften_height;
						nPoints++;
					}
					if (GetTilepoint(x, y + 1)) {
						t->soften_height += GetTilepoint(x, y + 1)->soften_height;
						nPoints++;
					}
					t->soften_height = t->soften_height / nPoints;
				}
			}
		}

		for (int x = 0; x < TILEDIVISION * (header->width - 1); x++) {
			for (int y = 0; y < TILEDIVISION * (header->height - 1); y++) {
				data->pStaticGrid[y * TILEDIVISION * (header->width - 1) + x] = 
					GetTilepoint(x / TILEDIVISION, y / TILEDIVISION)->IsCliff() ? FLAG_NOMOVENODE : FLAG_FREENODE;
			}
		}
	};

	struct header_t {
		header_t(const BYTE **offset) {
			COPYVAR(header, offset);
			COPYVAR(version, offset);
			COPYVAR(tileset, offset);
			COPYVAR(custom, offset);
			COPYVAR(ground_num, offset);
			ground_ids = new id_t[ground_num];
			COPYMEM(ground_ids, offset, sizeof(id_t) * ground_num);
			COPYVAR(cliff_num, offset);
			cliff_ids = new id_t[cliff_num];
			COPYMEM(cliff_ids, offset, sizeof(id_t) * cliff_num);
			COPYVAR(width, offset);
			COPYVAR(height, offset);
			COPYVAR(center, offset);

			pGroundMaterials = new const Material*[ground_num];
			pCliffMaterials = new const Material*[cliff_num];

			memset(pGroundMaterials, 0, sizeof(const Material*) * ground_num);
			memset(pCliffMaterials, 0, sizeof(const Material*) * cliff_num);

			for (int i = 0; i < ground_num; i++) {
				const char sID[5] = {ground_ids[i][0], ground_ids[i][1], ground_ids[i][2], ground_ids[i][3], '\0'};
				pGroundMaterials[i] = file::FileManager::Get()->Load2<file::MaterialFile>(
					*TerrainSheet::Get()->GetSlot("dir", sID) + "\\" + *TerrainSheet::Get()->GetSlot("file", sID) + ""
					);
			}

			for (int i = 0; i < cliff_num; i++) {
				const char sID[5] = {cliff_ids[i][0], cliff_ids[i][1], cliff_ids[i][2], cliff_ids[i][3], '\0'};
				const char sPrefix[3] = {tileset, '_', '\0'};

				const std::string *pTexDir = CliffsSheet::Get()->GetSlot("texDir", sID);
				const std::string *pTexFile = CliffsSheet::Get()->GetSlot("texFile", sID);

				if (!pTexDir || !pTexFile)
					continue;

				const std::string sName = *pTexDir + "\\" + sPrefix + *pTexFile;

				try {
					pCliffMaterials[i] = file::FileManager::Get()->Load2<file::MaterialFile>(sName);
				} catch (std::exception&) {
					const std::string sName = *CliffsSheet::Get()->GetSlot("texDir", sID) + "\\" + *CliffsSheet::Get()->GetSlot("texFile", sID);

					pCliffMaterials[i] = file::FileManager::Get()->Load2<file::MaterialFile>(sName);
				}
			}
		};

		typedef char id_t[4];

		char header[4];
		int version;
		char tileset;
		int custom;
		int ground_num;
		id_t *ground_ids;
		int cliff_num;
		id_t *cliff_ids;
		int width;
		int height;
		math::Vector2 center;

		const Material **pGroundMaterials;
		const Material **pCliffMaterials;
	} *header;

	struct data_t {
		data_t(const header_t *header, const BYTE **offset) {
			pStaticGrid = new FLAG_TYPE[TILEDIVISION * TILEDIVISION * (header->width - 1) * (header->height - 1)];
			tilepoints = new TilePoint[header->width * header->height];
			for (int i = 0; i < header->width * header->height; i++)
				COPYMEM(&tilepoints[i], offset, 7);
			for (int id = 0;; id++) {
				try {
					if (id < 10)
						pWaterAnimation.push_back(file::FileManager::Get()->Load2<file::MaterialFile>(FormatString("Textures/terrain/water/A_Water0%i", id)));
					else
						pWaterAnimation.push_back(file::FileManager::Get()->Load2<file::MaterialFile>(FormatString("Textures/terrain/water/A_Water%i", id)));
				} catch (const std::exception&) {
					break;
				};
			};
		};

		class TilePoint {
		public:
			TilePoint() {
				memset(c, 0, sizeof(c));
				c[CENTER] = this;
			};

			bool IsCliff() const {
				for (int i = 0; i < CORNERS_COUNT; i++)
					if (c[i] && !(c[i]->GetFlags() & FLAG_RAMP))
						goto check_for_similar_level;
				return false;
check_for_similar_level:
				const short nLayer = c[CENTER]->GetLayer();
				for (int i = 0; i < CORNERS_COUNT; i++)
					if (c[i] && c[i]->GetLayer() != nLayer)
						return true;
				return false;
			}

			int Bit(int id) const {
				int m = 1;
				for (; id > 0; id--)
					m = m << 1;
				return m;
			}

			bool GetUV(int nGround, int &nX, int &nY, const Material **pMaterial) const {
				int nTileFlags = 0;

				for (int j = 0; j < CORNERS_COUNT; j++) {
					if (c[j] && c[j]->GetGroundID() == nGround) {
						*pMaterial = c[j]->pGroundMaterial;
						nTileFlags |= Bit(j);
					}
				}

				if (nTileFlags == 0)
					return false;

				if (nTileFlags == (Bit(TOP_RIGHT))) { nX = 0; nY = 1; }
				if (nTileFlags == (Bit(TOP))) { nX = 0; nY = 2; }
				if (nTileFlags == (Bit(TOP) | Bit(TOP_RIGHT))) { nX = 0; nY = 3; }
				if (nTileFlags == (Bit(RIGHT))) { nX = 1; nY = 0; }
				if (nTileFlags == (Bit(RIGHT) | Bit(TOP_RIGHT))) { nX = 1; nY = 1; }
				if (nTileFlags == (Bit(RIGHT) | Bit(TOP))) { nX = 1; nY = 2; }
				if (nTileFlags == (Bit(RIGHT) | Bit(TOP_RIGHT) | Bit(TOP))) { nX = 1; nY = 3; }
				if (nTileFlags == (Bit(CENTER))) { nX = 2; nY = 0; }
				if (nTileFlags == (Bit(CENTER) | Bit(TOP_RIGHT))) { nX = 2; nY = 1; }
				if (nTileFlags == (Bit(CENTER) | Bit(TOP))) { nX = 2; nY = 2; }
				if (nTileFlags == (Bit(CENTER) | Bit(TOP_RIGHT) | Bit(TOP))) { nX = 2; nY = 3; }
				if (nTileFlags == (Bit(CENTER) | Bit(RIGHT))) { nX = 3; nY = 0; }
				if (nTileFlags == (Bit(CENTER) | Bit(TOP_RIGHT) | Bit(RIGHT))) { nX = 3; nY = 1; }
				if (nTileFlags == (Bit(CENTER) | Bit(TOP) | Bit(RIGHT))) { nX = 3; nY = 2; }
				if (nTileFlags == (Bit(CENTER) | Bit(TOP_RIGHT) | Bit(RIGHT) | Bit(TOP))) { nX = 4 + GetDetails() % 4; nY = GetDetails() / 4; }

				return true;
			}

			BYTE GetFlags() const { return data[0] >> 4; }
			BYTE GetGroundID() const { return data[0] & 0xf; }
			BYTE GetDetails() const { return data[1] & 0xf; }
			BYTE GetCliffID() const { return data[2] >> 4; }
			BYTE GetLayer() const { return data[2] & 0xf; }

			void SetGroundID(BYTE id) {	data[0] = (GetFlags() << 4) | id;	}

			enum Flags {
				FLAG_RAMP = 0x01,
			};

			//public vars
		public:
			short height;
			short water_level;

		private:
			BYTE data[3];

		public:
			enum corners_t {
				CENTER,
				TOP,
				TOP_RIGHT,
				RIGHT,
				CORNERS_COUNT
			};

			TilePoint *c[CORNERS_COUNT];

			const Material *pGroundMaterial, *pCliffMaterial;

			math::Vector3 position, normal;
			float soften_height;
		} *tilepoints;

		std::list<const Material*> pWaterAnimation;

		FLAG_TYPE *pStaticGrid;
	} *data;

	const Material *GetWaterMaterial() const {
		const Material *pMaterial = NULL;
		int frame = int(es::Timer::Get()->GetWorldTime() * 15) % (int)data->pWaterAnimation.size();
		for (std::list<const Material*>::const_iterator it = data->pWaterAnimation.begin(); frame >= 0; it++, frame--) {
			pMaterial = *it;
		}
		return pMaterial;
	}

	data_t::TilePoint *GetTilepoint(int x, int y) const {
		if (!header || !data || x < 0 || y < 0 || x >= header->width || y >= header->height)
			return NULL;
		return &data->tilepoints[y * header->width + x];
	}

	~w3e_t() {
		delete header;
		delete data;
	}
};