#include "Plugin.h"
#include "stdmat.h"

#include "ExportMesh.h"

#include <stdarg.h>

#include <iostream>
#include <fstream>
#include <algorithm>
#include <map>
#include <string>
#include <vector>

static const float EPS = 0.001f;

extern Interface* gi;

struct vertex_t
{
	Point3 position;
	Point3 normal;
	UVVert uv;
	DWORD smthg;
	int id;

	bool operator== (const vertex_t& v) const
	{
		return (position == v.position) && (uv == v.uv) && ((smthg & v.smthg) > 0);
	}

	bool operator== (const Point3& p) const
	{
		return position == p;
	}
};

struct face_t
{
	face_t(size_t* v)
	{
		this->v[0] = (unsigned short)v[2];
		this->v[1] = (unsigned short)v[1];
		this->v[2] = (unsigned short)v[0];
	}

	unsigned short v[3];
};

extern void StreamTextures(FileOutput& file, std::vector<BitmapTex*>& textures, bool use_nodraw);
extern void GatherTexture(Mtl* mtl2, std::vector<BitmapTex*>& textures_out, int& channel_out, int& id_out);
extern void WriteTexture(FileOutput& file, BitmapTex* texture, const char* tag);

class EffectMeshExporter : public SceneExport, public XmlMeshExport
{
public:
	EffectMeshExporter(void)
	{
		memset(flags, 0, sizeof(flags));
	}

	int ExtCount(void)
	{ // Number of extensions supported
		return(1);
	}

	const TCHAR* Ext(int n)
	{ // Extension #n(i.e. "3DS")
		return("EFFECT");
	}

	const TCHAR* LongDesc(void)
	{ // Long ASCII description(i.e. "Autodesk 3D Studio File")
		return("GameEngine Effect Model File");
	}

	const TCHAR* ShortDesc(void)
	{ // Short ASCII description(i.e. "3D Studio")
		return("GameEngine Effect Model");
	}

	const TCHAR* AuthorName(void)
	{ // ASCII Author name
		return("Igor Chernakov");
	}

	const TCHAR* CopyrightMessage(void)
	{ // ASCII Copyright message
		return("");
	}

	const TCHAR* OtherMessage1(void)
	{ // Other message #1
		return("");
	}

	const TCHAR* OtherMessage2(void)
	{ // Other message #2
		return("");
	}

	unsigned int Version(void)
	{ // Version number * 100(i.e. v3.01 = 301)
		return(100);
	}

	void ShowAbout(HWND hWnd)
	{ // Show DLL's "About..." box
	}

	int DoExport(const TCHAR* name, ExpInterface* ei, Interface* i, BOOL suppressPrompts = FALSE, DWORD options = 0);

	BOOL SupportsOptions(int ext, DWORD options)
	{
		return FALSE;
	}

	BOOL CanExport()
	{
		return pMeshes.size() > 0;
	}

	void ProcessMeshes(FileOutput& file)
	{
		std::vector<BitmapTex*> textures;
		std::map<INode*, int> node_textures;
		std::map<INode*, int> node_textures_channel;

		for (Meshes::const_iterator it = pMeshes.begin(); it != pMeshes.end(); ++it)
		{
			if (Mtl* mtl = (*it)->GetMtl()) {
				GatherTexture(mtl, textures, node_textures_channel[*it], node_textures[*it]);
			}
		}

		file.PushTag("EFCT");

		StreamTextures(file, textures, false);


		Interval intrvl(gi->GetAnimRange().Start() / GetTicksPerFrame(), gi->GetAnimRange().End() / GetTicksPerFrame());

		extern bool bExportAnimation;

		if (bExportAnimation) {
			int framerate = intrvl.End() - intrvl.Start() + 1;

			file.PushTag("SIZE");
				file.write((char*)&framerate, sizeof(framerate));
			file.PopTag();

			framerate = GetFrameRate();

			file.PushTag("RATE");
				file.write((char*)&framerate, sizeof(framerate));
			file.PopTag();
		}

		for (Meshes::const_iterator it = pMeshes.begin(); it != pMeshes.end(); ++it)
		{
			file.PushTag("MESH");
			{
				Object* obj = (*it)->EvalWorldState(0, true).obj;

				if (!obj->CanConvertToType(triObjectClassID))
				{
					file.PopTag();
					continue;
				}

				{
					file.PushTag("NAME");
					file.write((*it)->GetName(), strlen((*it)->GetName()));
					file.PopTag();
				}

				Matrix3 matrix = (*it)->GetObjTMAfterWSM(gi->GetTime());

				if (node_textures.find(*it) != node_textures.end())
				{
					file.PushTag("TXID");
					file.write((char*)&node_textures[*it], sizeof(int));
					file.PopTag();
				}

				{
					std::vector<face_t> faces;

					bool got_faces = false;

					file.PushTag("VRTX");
					for (int nFrame = gi->GetAnimRange().Start(); 
						nFrame <= (bExportAnimation ? gi->GetAnimRange().End() : gi->GetAnimRange().Start()); 
						nFrame += GetTicksPerFrame())
					{
						std::vector<vertex_t> vertices;

						TriObject* tri = (TriObject*)(obj->ConvertToType(nFrame, triObjectClassID));

						Mesh* mesh = new Mesh(tri->GetMesh());

						struct my_color_t {
							union {
								int c;
								struct { unsigned char b, g, r, a; };
							};
						};

						my_color_t color;
						color.c = -1;

						if (Mtl* mtl = (*it)->GetMtl())
						{
							Interval iii = intrvl;

							mtl->Update(nFrame, iii);
							color.a = (1 - mtl->GetXParency()) * 255;
						}

						for (int nFace = 0; nFace < mesh->numFaces; nFace++)
						{
							Face* face = mesh->faces + nFace;
							TVFace* tvface = mesh->tvFace + nFace;

							size_t triangle[3];

							Point3 face_normal = 
								((mesh->verts[face->v[1]] - mesh->verts[face->v[0]]) ^ 
								(mesh->verts[face->v[2]] - mesh->verts[face->v[1]])).Normalize();

							for (int n = 0; n < 3; n++)
							{
								vertex_t v;

								v.id = vertices.size();
								v.position = mesh->verts[face->v[n]];
								v.smthg = face->getSmGroup();
								v.uv = Point3(0,0,0);

								if (mesh->tVerts)
								{
									v.uv = mesh->tVerts[tvface->t[n]];
									v.uv.y = 1 - v.uv.y; //IMPORTANT: flip y-axis
								}

								std::vector<vertex_t>::const_iterator vertex_it =
									std::find(vertices.begin(), vertices.end(), v);

								if (vertex_it == vertices.end())
								{
									triangle[n] = vertices.size();
									vertices.push_back(v);
								}
								else 
								{
									triangle[n] = vertex_it->id;
								}
							}

							if (!got_faces)
								faces.push_back(face_t(triangle));
						}

						Matrix3 node_matrix = (*it)->GetNodeTM(nFrame);
						Matrix3 node_rotation_matrix = node_matrix;
						node_rotation_matrix.SetTrans(Point3(0,0,0));
						for (int i = 0; i < vertices.size(); i++)
						{
							Point3 transformed_pos = vertices[i].position * node_matrix;
							Point3 transformed_normal = vertices[i].normal * node_rotation_matrix;
							file.write((char*)&transformed_pos, sizeof(Point3));
							file.write((char*)&transformed_normal, sizeof(Point3));
							file.write((char*)&color, sizeof(int));
							file.write((char*)&vertices[i].uv, sizeof(Point2));
							file.write((char*)&vertices[i].uv, sizeof(Point2));
						}

						got_faces = true;

						if (tri != (TriObject*)*it)
						{
							tri->AutoDelete();
						}

						delete mesh;
					}
					file.PopTag();

					file.PushTag("FACE");
					for (int i = 0; i < faces.size(); i++)
					{
						file.write((char*)&faces[i], sizeof(face_t));
					}
					file.PopTag();
				}

			}
			file.PopTag();
		}
		file.PopTag();
	}

	typedef std::list<INode*> Meshes;

	Meshes pMeshes;

	std::string filename;

	Interface* gi;

private:
	enum Flags {
		FLAG_EXPORTMESHES,
		FLAG_TEXTUALMESHES,
		FLAG_EXPORTANIMATIONS,
		FLAG_LAST
	};

	UINT flags[FLAG_LAST];
};

class EffectExportClassDesc :
	public ClassDesc
{
public:
	int IsPublic(void)
	{
		return(1);
	}

	void* Create(BOOL loading = FALSE)
	{
		return(new EffectMeshExporter);
	}

	const TCHAR* ClassName(void)
	{
		return("GameEngine Effect Model Export");
	}

	SClass_ID SuperClassID(void)
	{
		return(SCENE_EXPORT_CLASS_ID);
	}

	Class_ID ClassID(void)
	{
		return(Class_ID(0x52342d61, 0x76020b9a));
	}

	const TCHAR* Category(void)
	{
		return("Scene Export");
	}
};

ClassDesc* GetEffectExportClassDesc(void)
{
	return(new EffectExportClassDesc);
}

struct RootBoneSelectDialog2 : public HitByNameDlgCallback
{
	TCHAR *dialogTitle() 
	{
		return "Select root bone";
	}
	TCHAR *buttonText()
	{
		return "Select";
	}
	BOOL singleSelect()
	{
		return FALSE;
	}
	BOOL useProc()
	{
		return FALSE;
	}
	int filter(INode *node)
	{
		Object* obj = node->EvalWorldState(0, true).obj;

		return obj && obj->SuperClassID() == GEOMOBJECT_CLASS_ID;
	}
};


// Dialog proc
static INT_PTR CALLBACK ExportDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	EffectMeshExporter *exp = (EffectMeshExporter*)GetWindowLongPtr(hWnd, GWLP_USERDATA); 

	switch(msg)
	{
	case WM_INITDIALOG:
		exp =(EffectMeshExporter*)lParam;

		EnableWindow(GetDlgItem(hWnd, IDOK), exp->CanExport());
		SetWindowLongPtr(hWnd,GWLP_USERDATA,lParam); 
		CenterWindow(hWnd, GetParent(hWnd)); 
		CheckDlgButton(hWnd, IDC_EMBEDTEXTURES, TRUE);
		CheckDlgButton(hWnd, IDC_EXPORTANIMATIONS, TRUE);
		break;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDC_ADDMESH:
			{
				RootBoneSelectDialog2 meshes;
				GetCOREInterface()->ClearNodeSelection();
				GetCOREInterface()->DoHitByNameDialog(&meshes);
				if (GetCOREInterface()->GetSelNodeCount() > 0)
				{
					for (int i = 0; i < GetCOREInterface()->GetSelNodeCount(); i++)
					{
						INode* node = GetCOREInterface()->GetSelNode(i);
						EffectMeshExporter::Meshes::const_iterator m = std::find(exp->pMeshes.begin(), exp->pMeshes.end(), node);
						if (m != exp->pMeshes.end())
						{
							continue;
						}
						exp->pMeshes.push_back(node);
						SendMessage(GetDlgItem(hWnd, IDC_MESHESLIST), LB_ADDSTRING, 0, (LPARAM)node->GetName());
					}

					GetCOREInterface()->ClearNodeSelection();
				}
			}

			EnableWindow(GetDlgItem(hWnd, IDOK), exp->CanExport());

			break;
		case IDC_REMOVEMESH:
			{
				int sel = SendMessage(GetDlgItem(hWnd, IDC_MESHESLIST), LB_GETCURSEL, 0, 0);
				if (sel > -1)
				{
					SendMessage(GetDlgItem(hWnd, IDC_MESHESLIST), LB_DELETESTRING, sel, 0);
					for (EffectMeshExporter::Meshes::iterator m = exp->pMeshes.begin(); m != exp->pMeshes.end(); m++, sel--)
					{
						if (sel == 0)
						{
							exp->pMeshes.erase(m);
							break;
						}
					}
				}
			}

			EnableWindow(GetDlgItem(hWnd, IDOK), exp->CanExport());

			break;

		case IDOK:
			{
				extern bool bEmbedTextures, bExportAnimation;
				FileOutput::Mode filemode = FileOutput::BINARY;
				FileOutput modelfile(exp->filename, filemode);
				bEmbedTextures = IsDlgButtonChecked(hWnd, IDC_EMBEDTEXTURES);
				bExportAnimation = IsDlgButtonChecked(hWnd, IDC_EXPORTANIMATIONS);
				exp->ProcessMeshes(modelfile);
				modelfile.close();
			}
			EndDialog(hWnd, 1);
			break;

		case IDCANCEL:
			EndDialog(hWnd, 0);
			break;
		}

		break;
	default:
		return FALSE;
	}

	return TRUE;
}       


int EffectMeshExporter::DoExport(const TCHAR* name, ExpInterface* ei, Interface* gi, BOOL SuppressPrompts, DWORD options)
{
	this->filename = name;
	this->gi = gi;

	if(!DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_EFFECTEXPORT_DLG), gi->GetMAXHWnd(), ExportDlgProc,(LPARAM)this))
	{
		return 1;
	}

	return(1);
}