#pragma once

#include <list>

enum
{
	EXPORT_ROTATION = 0x1,
	EXPORT_ROTATION_AS_SHORT = 0x8,
	EXPORT_POSITION = 0x2,
	EXPORT_SCALE = 0x4,
};

class XmlMeshExport {
public:
	static Modifier* GetSkinModifier(INode* node);
	static void StreamNodeProps(FileOutput& file, INode* node, TimeValue time, int flags = 0xFFFFFFFF);

	struct SkinnedMesh
	{
		SkinnedMesh(INode* node, ISkin* skin)
		{
			this->node = node;
			this->skin = skin;
		}

		INode* node;
		ISkin* skin;
	};

	typedef std::list<SkinnedMesh> SkinnedMeshes;

public:
	void ProcessMeshes(FileOutput& file, INode* root, SkinnedMeshes& meshes, Interface* gi) const;
	void ProcessAnimations(FileOutput& file, std::list<INode*>& roots, Interface* gi, TimeValue a, TimeValue b) const;

	static INode *GetRootBoneForSkinnedMesh(ISkin* skin);

private:
	static void GetBoneChainForNode(INode* node, std::list<INode*>& chain);
};

class XmlSceneExport :
	public SceneExport,
	private XmlMeshExport
{
public:
	XmlSceneExport(void)
	{
		memset(pFlags, 0, sizeof(pFlags));
	}

	~XmlSceneExport(void)
	{
	}

	int ExtCount(void)
	{ // Number of extensions supported
		return(1);
	}

	const TCHAR* Ext(int n)
	{ // Extension #n(i.e. "3DS")
		return("XML");
	}

	const TCHAR* LongDesc(void)
	{ // Long ASCII description(i.e. "Autodesk 3D Studio File")
		return("GameEngine Scene File");
	}

	const TCHAR* ShortDesc(void)
	{ // Short ASCII description(i.e. "3D Studio")
		return("GameEngine Scene");
	}

	const TCHAR* AuthorName(void)
	{ // ASCII Author name
		return("Igor Chernakov");
	}

	const TCHAR* CopyrightMessage(void)
	{ // ASCII Copyright message
		return("");
	}

	const TCHAR* OtherMessage1(void)
	{ // Other message #1
		return("");
	}

	const TCHAR* OtherMessage2(void)
	{ // Other message #2
		return("");
	}

	unsigned int Version(void)
	{ // Version number * 100(i.e. v3.01 = 301)
		return(100);
	}

	void ShowAbout(HWND hWnd)
	{ // Show DLL's "About..." box
	}

	int DoExport(const TCHAR* name, ExpInterface* ei, Interface* i, BOOL suppressPrompts = FALSE, DWORD options = 0);

	BOOL SupportsOptions(int ext, DWORD options)
	{
		return FALSE;
	}

	void CollectModels(INode* node, Interface* gi);

	struct model_t {
		INode* model;
		INode* bone;

		Modifier* skin;

		bool operator==(const INode* bone) {
			return this->bone == bone;
		}
	};

	typedef std::list<model_t> models_t;

	models_t models;

private:
	void StreamNode(FileOutput& file, INode* node, Interface* gi, const std::string& indent);

public:
	enum Flags {
		FLAG_EXPORTMESHES,
		FLAG_TEXTUALMESHES,
		FLAG_EXPORTANIMATIONS,
		FLAG_LAST
	};

	UINT pFlags[FLAG_LAST];
};
