//
//  iosAppDelegate.h
//  launcher.ios
//
//  Created by Igor Chernakov on 13/10/2013.
//  Copyright (c) 2013 Igor Chernakov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface iosAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
