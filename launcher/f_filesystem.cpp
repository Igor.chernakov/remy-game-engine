#include <boost/foreach.hpp>

#include "../shared/shared.hpp"
#include "../interfaces/iframework.hpp"

#include "f_filesystem.hpp"

//#include <io.h>
//#include <direct.h>
static IFramework* framework = 0;

#define MAX_FILEPATH 1024

FileSystem::FileSystem()
{
	framework->Log(IFramework::MSG_LOG, "Standard filesystem initialized");

	//AddPath(".");
}

void FileSystem::AddPath(const std::string& path) {
	search.push_front(path);

	char pakfile[MAX_PATH];

	for (int i = 0; i < 50; ++i) {
		sprintf(pakfile, "%s/pak%i.pak", path.c_str(), i);
		
		FILE* fp = fopen(pakfile, "rb");

		if (!fp)
			break;

		/*
		ZIPEnd End;
		ZIPCtrlHeader File;

		// Read the final header
		fseek( fp, -22, SEEK_END );
		fread( &End,  sizeof( End ), 1,  fp );

		if(End.Signature != ZIPEndSig )
		{
			fclose(fp);
			framework->Log(IFramework::MSG_ERROR, "Error in package %s", pakfile);
			continue;
		}

		Pack packet;

		packet.name = pakfile;

		// Read the header of each file
		fseek( fp, End.Offset, SEEK_SET );

		for(int n = 0; n < End.FilesOnDisk; n++ )
		{
			fread(&File, sizeof(File), 1, fp);

			if(File.Signature != ZIPCtrlHeaderSig)
			{
				fclose(fp);
			framework->Log(IFramework::MSG_ERROR, "Error in package %s", pakfile);
				break;			// continue with next pak
			}

			Pack::PackedFile current_file;

			current_file.size = File.UnCompressedSize;
			current_file.offset = File.Offset;

			char tmp[1024];

			memset(tmp, 0, sizeof(tmp));	// FIXME: hack???
			fread(tmp, 1, File.FileNameLength, fp);
			current_file.name = tmp;
			packet.files.push_back(current_file);
			fseek(fp, File.ExtraLength + File.CommentLength, SEEK_CUR);
		}

		packets.push_back(packet);
		*/
		fclose(fp);
		
		framework->Log(IFramework::MSG_LOG, "Package %s was successfuly added", pakfile);
	}
}

file_t FileSystem::LoadFile(const std::string& path) const
{
	file_t file;
	file.name = path;
	BOOST_FOREACH(const std::string& dir, this->search)
	{
		std::string path = (dir + "/" + file.name);
		FILE* fp = fopen(path.c_str(), "rb");
		if (fp == 0)
			continue;
		fseek(fp, 0L, SEEK_END);
		file.handle = fp;
		file.size = ftell(fp);
		rewind(fp);
		if (file.size == 0) {
			fclose(fp);
			return file_t();
		}
		return file;
	}
	/*
	BOOST_FOREACH(const Pack& p, this->packets)
	{
		BOOST_FOREACH(const Pack::PackedFile& f, p.files)
		{
			if (file->GetPath() == f.name)
			{
				FILE* fp = fopen(p.name.c_str(), "rb");
				if(fp == 0)
				{
					continue;
				}
				fseek(fp, f.offset, SEEK_SET);
				file->data.resize(f.size);
				char* buf = UnZip(fp);
				memcpy(&*file->data.begin(), buf, f.size);
				fclose(fp);
				free(buf);
			}
		}
	}
	*/
	framework->Log(IFramework::MSG_ERROR, "File %s is not found", path.c_str());

	return file_t();
}

bool FileSystem::CheckFile(const std::string& filename) const
{
	BOOST_FOREACH(const std::string& dir, this->search)
	{
		FILE* fp = fopen((dir + "/" + filename).c_str(), "rb");
		if (fp == 0)
		{
			continue;
		}
		fclose(fp);
		return true;
	}

	BOOST_FOREACH(const Pack& p, this->packets)
	{
		BOOST_FOREACH(const Pack::PackedFile& f, p.files)
		{
			if (filename == f.name)
			{
				FILE* fp = fopen(p.name.c_str(), "rb");
				if(fp == 0)
				{
					continue;
				}
				fclose(fp);
				return true;
			}
		}
	}

	return false;
}

IFileSystem* CreateFileSystem(IFramework* framework_)
{
	framework = framework_;
	return new FileSystem();
}
