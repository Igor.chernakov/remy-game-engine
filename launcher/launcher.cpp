#include <boost/thread.hpp>

#include <direct.h>

#include "timer.hpp"
#include "framework.hpp"
#include "window.hpp"
#include "mycout.hpp"

#include "../shared/shared.hpp"

#ifdef WIN32
#	include "DbgHelp.h"
#	define EXTERN_FUNC __cdecl
#	define CLIENT_MODULE_NAME "./core.client.dll"
#else
#	include <dlfcn.h>
#	define EXTERN_FUNC
#	define HINSTANCE void*
#	define CLIENT_MODULE_NAME "./libclient.so"
#	define LoadLibrary(name) dlopen(name, RTLD_LAZY)
#	define FreeLibrary(lib) dlclose(lib)
#endif

#include "../interfaces/iclient.hpp"
#include "../interfaces/iserver.hpp"
#include "../interfaces/ifilesystem.hpp"
#include "../interfaces/isoundsystem.hpp"

 
const int ENGINE_VERSION = 110; //Version * 100, i.e. 1.01 = 101
 
std::string game_directory = "."; 

#include <Shlobj.h> 
std::string GetAppDataFolder() { 
	char szPath[MAX_PATH];
	SHGetFolderPathA(NULL, CSIDL_APPDATA|CSIDL_FLAG_CREATE, NULL, 0, szPath);
	return szPath;
}

#include <direct.h>
void CreateFolder(const std::string& folder) {
	_mkdir( folder.c_str() );
}

extern IFileSystem* CreateFileSystem(IFramework*);
extern ISoundSystem* CreateSoundSystem(IClientFramework*);
extern IRenderer* CreateRenderer(IClientFramework*);
__declspec(dllimport) IClient* CreateClient(int argc, char **argv, const char*, const char*, IClientFramework*, IRenderer*);

enum AppFlags { 
	APPFLAG_CONSOLE = 1 << 0,
	APPFLAG_ERROR = 1 << 1,
	APPFLAG_WINDOW = 1 << 2,
	APPFLAG_DEVWINDOW = 1 << 3,
	APPFLAG_ALLOWENVPYTHON = 1 << 4,
	APPFLAG_RIGHTWINDOW = 1 << 5,
	APPFLAG_DEVELOPER = 1 << 6,
	APPFLAG_PHONE = 1 << 7,
};

int g_AppFlags = 0;

#define PARSE_ARGUMENT_FLAG(Flag) else if (boost::iequals(command.c_str(), "-"#Flag)) g_AppFlags |= APPFLAG_##Flag;

template<typename T>
void ParseCmdLine(int argc, T argv)
{
	for (int i = 0; i < argc && argv; ++i)
	{
		std::string command = argv[i];

		if (boost::starts_with(command, "-g="))
		{
			game_directory = command.substr(3);
		}
		PARSE_ARGUMENT_FLAG(CONSOLE)
		PARSE_ARGUMENT_FLAG(WINDOW)
		PARSE_ARGUMENT_FLAG(DEVWINDOW)
		PARSE_ARGUMENT_FLAG(ALLOWENVPYTHON)
		PARSE_ARGUMENT_FLAG(RIGHTWINDOW)
		PARSE_ARGUMENT_FLAG(DEVELOPER)
		PARSE_ARGUMENT_FLAG(PHONE)
	}
}

#undef PARSE_ARGUMENT_FLAG

void ProcessConsoleInput(shared_ptr<IClient> client)
{
	while (true)
	{
		std::string command;
		std::cin >> command;
		client->Evaluate(command);
	}
}

IClientFramework* framework = 0;
IClient* client = 0;

std::string client_compile_date;

#ifdef WIN32

typedef BOOL (__stdcall *tMDWD)( 
	IN HANDLE hProcess, 
	IN DWORD ProcessId, 
	IN HANDLE hFile, 
	IN MINIDUMP_TYPE DumpType, 
	IN CONST PMINIDUMP_EXCEPTION_INFORMATION ExceptionParam, OPTIONAL 
	IN CONST PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam, OPTIONAL 
	IN CONST PMINIDUMP_CALLBACK_INFORMATION CallbackParam OPTIONAL 
	); 

static tMDWD s_pMDWD; 
static HMODULE s_hDbgHelpMod; 
static MINIDUMP_TYPE s_dumpTyp = MiniDumpNormal; 
static std::string s_miniDumpFileName = "crash.dmp";

#ifdef _DEBUG   
#ifndef DBG_NEW      
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW   
#endif
#endif  // _DEBUG

//int *a = new int();

static LONG __stdcall MyCrashHandlerExceptionFilter(EXCEPTION_POINTERS* pEx) 
{   
#ifdef _M_IX86 
	if (pEx->ExceptionRecord->ExceptionCode == EXCEPTION_STACK_OVERFLOW)   
	{ 
		// be sure that we have enought space... 
		static char MyStack[1024*128];   
		// it assumes that DS and SS are the same!!! (this is the case for Win32) 
		// change the stack only if the selectors are the same (this is the case for Win32) 
		//__asm push offset MyStack[1024*128]; 
		//__asm pop esp; 
		__asm mov eax,offset MyStack[1024*128]; 
		__asm mov esp,eax; 
	} 
#endif 
	bool bFailed = true; 
	HANDLE hFile; 
	hFile = CreateFile(s_miniDumpFileName.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL); 
	if (hFile != INVALID_HANDLE_VALUE) 
	{ 
		MINIDUMP_EXCEPTION_INFORMATION stMDEI; 
		stMDEI.ThreadId = GetCurrentThreadId(); 
		stMDEI.ExceptionPointers = pEx; 
		stMDEI.ClientPointers = TRUE; 
		// try to create an miniDump: 
		if (s_pMDWD(GetCurrentProcess(), GetCurrentProcessId(), hFile, s_dumpTyp, &stMDEI, NULL, NULL )) 
		{ 
			bFailed = false;  // suceeded 
		} 
		CloseHandle(hFile); 
	} 

	if (bFailed) 
	{ 
		return EXCEPTION_CONTINUE_SEARCH; 
	} 

	// or return one of the following: 
	// - EXCEPTION_CONTINUE_SEARCH 
	// - EXCEPTION_CONTINUE_EXECUTION 
	// - EXCEPTION_EXECUTE_HANDLER 
	return EXCEPTION_CONTINUE_SEARCH;  // this will trigger the "normal" OS error-dialog 
} 

#endif

IRenderer* renderer = 0;

void SetErrorState()
{
	g_AppFlags |= APPFLAG_ERROR;
}

#ifdef WIN32
struct argv_holder_t {
	argv_holder_t(): argv(0) {}
	~argv_holder_t() { if (argv) free(argv); }
	char **argv;
} ah;
int argc = 1;
char **argv = 0;
int ProcessCommandLine(LPSTR lpCmdLine)
{
	int i;
	char seps[] = " ";
	char *token;

	if (!(*lpCmdLine))
	{
		return 0;
	}

	strlwr(lpCmdLine);

	token = lpCmdLine;

	// Find out the number of tokens in the string
	if (*token == ' ')
	{
		while (*token++ == ' ')		// Trim left
			;
	}

	if (*token != '\0')
	{
		i = lstrlen(lpCmdLine);

		while (lpCmdLine[--i] == ' ')	// Trim right
		{
			lpCmdLine[i] = '\0';
		}
	}

	while (*token != '\0')
	{
		if (*token == ' ')
		{
			argc++;
		}

		while (*token++ == ' ')
			;
	}

	argc++;		// the last token in the string won't have a trailing space.

	if ((ah.argv = (char **) malloc((argc + 1) * sizeof(char *))) == NULL)
	{
		return 1;
	}

	argv = ah.argv;

	argv[0] = "autolock";
	argv[1] = strtok(lpCmdLine, seps);

	for (i = 2; i < argc; i++)
	{
		argv[i] = strtok(NULL, seps);	// pull out any others
	}

	// finish the array off.
	argv[argc] = '\0';

	return 0;
}

HINSTANCE g_hInstance = 0;

FILE* logfile = 0;
int width = 1024, height = 768;

std::string ExePath() {
    char buffer[MAX_PATH];
    GetModuleFileName( NULL, buffer, MAX_PATH );
	std::string::size_type pos = std::string( buffer ).find_last_of( "\\/" );
	return std::string( buffer ).substr( 0, pos);
}

//int main(int argc, char **argv)
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
#ifdef WIN32
	//_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

	g_hInstance = hInstance;
	ProcessCommandLine(lpCmdLine);
#else
int main(int argc, char **argv)
{
#endif

	SetCurrentDirectory(ExePath().c_str());

	std::string companyDir = "/CorePunch Game Studio";
	std::string gameDir = "/Among The Heavens";

	CreateFolder(GetAppDataFolder());
	CreateFolder(GetAppDataFolder() + companyDir);
	CreateFolder(GetAppDataFolder() + companyDir + gameDir);

	std::string doc_directory = GetAppDataFolder() + companyDir + gameDir;

	bool b_fullscreen = true, b_tutorial = true, b_sound = true, b_music = true;

	std::string appdata = GetAppDataFolder() + companyDir + gameDir;

	s_miniDumpFileName = appdata + "/crash.dmp";

	if (FILE* f = fopen((appdata + "/options.dat").c_str(), "rb")) {
		//_mkdir("savedata");
		fread(&b_fullscreen, 1, 1, f);
		fread(&b_tutorial, 1, 1, f);
		fread(&b_sound, 1, 1, f);
		fread(&b_music, 1, 1, f);
		fclose(f);
	}

	ParseCmdLine(argc, argv);

	if (g_AppFlags & APPFLAG_CONSOLE) {
		AllocConsole();
		freopen("CONOUT$", "wb", stdout);
		freopen("CONIN$", "rb", stdin);
	}

	if (!(logfile = fopen((appdata + "/system.log").c_str(), "w"))) {
		std::cout << "Could not open system log for writing" << std::endl;
	}

	std::cout
		<< "Framework version "
		<< ENGINE_VERSION / 100
		<< "."
		<< ENGINE_VERSION - ENGINE_VERSION / 100 * 100
		<< std::endl;

	try
	{
		if (g_AppFlags & APPFLAG_PHONE)
		{
			width = 960;
			height = 640;
		}

#ifdef WIN32
		if ((s_hDbgHelpMod = LoadLibrary("dbghelp.dll")) != NULL) {
			if (s_pMDWD = (tMDWD) GetProcAddress(s_hDbgHelpMod, "MiniDumpWriteDump")) {
				SetUnhandledExceptionFilter(MyCrashHandlerExceptionFilter);
				std::cout << "Debug helper initialized" << std::endl;
			} else {
				std::cout << "Debug helper could not be initialized, crash dump will not be created" << std::endl;
			}
		} else {
			std::cout << "Debug helper library not found, crash dump will not be created" << std::endl;
		}
#endif

		//ServerFramework s_framework;

		//s_framework.FileSystem.reset(CreateFileSystem());
		//s_framework.Timer.reset(CreateTimer());

		//create local server
		//if (shared_ptr<IServer> server = shared_ptr<IServer>(CreateServer(game_directory.c_str(), &s_framework)))
		{
			//boost::thread server_thread(boost::bind(&IServer::Run, server));
			ClientFramework c_framework;

			framework = &c_framework;

			c_framework.FileSystem.reset(CreateFileSystem(&c_framework));
			c_framework.Timer.reset(CreateTimer());
			c_framework.SoundSystem.reset(CreateSoundSystem(&c_framework));
			c_framework.Variables.reset(CreateVariables());
			c_framework.Window.reset(new VideoWindow("Among The Heavens", dim_t(width, height), 
				b_fullscreen && !(g_AppFlags & (APPFLAG_WINDOW | APPFLAG_DEVWINDOW)), 
#ifdef WIN32
				g_hInstance,
#else
				0,
#endif
				g_AppFlags & APPFLAG_DEVWINDOW,
				g_AppFlags & APPFLAG_RIGHTWINDOW
				));

			c_framework.SoundSystem->SetSoundEnabled(b_sound);
			c_framework.SoundSystem->SetMusicEnabled(b_music);

			c_framework.GetVariables()->SetVariable("opt_tutorial", b_tutorial ? "1" : "0");

			if (renderer = CreateRenderer(&c_framework)) 
			{
				if (client = CreateClient(argc, argv, game_directory.c_str(), doc_directory.c_str(), framework, renderer))
				{
					framework->Log(IClientFramework::MSG_LOG, "Starting client from %s", ExePath().c_str());

					client->Run();

					delete client;
					
					if (FILE* f = fopen((appdata + "/options.dat").c_str(), "wb")) {
						b_tutorial = c_framework.GetVariables()->GetBoolean("opt_tutorial");
						b_fullscreen = renderer->IsFullscreen();
						b_sound = c_framework.GetSoundSystem()->IsSoundEnabled();
						b_music = c_framework.GetSoundSystem()->IsMusicEnabled();
						fwrite(&b_fullscreen, 1, 1, f);
						fwrite(&b_tutorial, 1, 1, f);
						fwrite(&b_sound, 1, 1, f);
						fwrite(&b_music, 1, 1, f);
						fclose(f);
					}
			 	}
				else 
				{
					throw std::runtime_error("Can't open client module");
				}

				delete renderer;
			}
			else
			{
				throw std::runtime_error("Can't create renderer");
			}
		}

		//Py_Finalize();
	}
	catch (const std::exception& ex)
	{
		std::cout << ex.what() << std::endl;
		g_AppFlags |= APPFLAG_ERROR;
	}

	std::cout << std::endl << "Launcher quit" << std::endl;
	if (g_AppFlags & APPFLAG_CONSOLE) {
		if (g_AppFlags & APPFLAG_ERROR) {
			std::cin.ignore();
			std::cin.get();
		}
		fclose(stdout);
		fclose(stdin);
		FreeConsole();
	}

	fclose(logfile);

	return 0;
}