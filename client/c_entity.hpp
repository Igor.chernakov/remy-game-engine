#ifndef ENTITY_INCLUDED

#ifndef DECLARE_WIDGET
#	define ENTITY Entity
#	define ENTITY_(Class) Entity_##Class
#else
#	define ENTITY Widget
#	define ENTITY_(Class) Widget_##Class
#endif

#ifndef DECLARE_WIDGET

#	include "../interfaces/iobject.hpp"
#	include "../interfaces/ihelper.hpp"
#	include "../interfaces/ioverlay.hpp"
#	include "../interfaces/iclientscene.hpp"

#	include "c_overlay.hpp"
#	include "c_event.hpp"
#	include "c_controller.hpp"

#	define PCLASS_FOCUS "focus"
#	define PCLASS_HOVER "hover"
#	define PCLASS_PRESSED "click"
#	define PCLASS_ENABLED "active"
#	define PCLASS_DISABLED "disabled"

void g_NotifyLoading();

struct IWrapper: IObject
{
	virtual PyObject* GetPythonObject() const = 0;
};

template <class T>
class tree_with_python_objects: public Tree<T>
{
public:
	tree_with_python_objects()
		: _parent_pyobject(0)
		, _parent_raw_ptr(0)
	{
	}

	virtual void SetParent(T* __p)
	{
		if (IWrapper* wrapper = dynamic_cast<IWrapper*>(__p))
		{
			this->_parent_pyobject = wrapper->GetPythonObject();
			Tree<T>::SetParent(0);
		}
		else
		{
			this->_parent_pyobject = 0;
			Tree<T>::SetParent(__p);
		}

		_parent_raw_ptr = __p;
	}

	virtual shared_ptr<T> GetSharedParent() const
	{
		return _parent_pyobject ? boost::python::extract<shared_ptr<T> >(_parent_pyobject) : Tree<T>::GetSharedParent();
	}

	virtual T* GetParent() const
	{
		return _parent_raw_ptr;
	}

private:
	PyObject* _parent_pyobject;
	T* _parent_raw_ptr;
};


template <typename _Tx>
class predicated_queue : private std::list<shared_ptr<_Tx> >
{
public:
	shared_ptr<_Tx> front() const
	{
		return this->size() > 0 ? this->std::list<shared_ptr<_Tx> >::front() : null_ptr();
	};

	void flush()
	{
		m_cache = front();
	}

	shared_ptr<_Tx> cache() const
	{
		return m_cache;
	}

	void push(shared_ptr<_Tx> _Px, bool (*_Pred)(shared_ptr<const _Tx>, shared_ptr<const _Tx>))
	{
		this->push_back(_Px);
		this->sort(_Pred);
	}

	void push(shared_ptr<_Tx> _Px)
	{
		this->push_front(_Px);
	}

	void pop(shared_ptr<const _Tx> _Px)
	{
		if (std::find(this->begin(), this->end(), _Px) != this->end())
		{
			this->erase(std::find(this->begin(), this->end(), _Px));
		}
	}

	void clear()
	{
		this->std::list<shared_ptr<_Tx> >::clear();
	};

private:
	shared_ptr<_Tx> m_cache;
};

#endif

namespace client
{
	class ENTITY
#ifdef DECLARE_WIDGET
		: protected Overlay
#else
		: public IObject
#endif
		, public tree_with_python_objects<ENTITY>
	{
		friend class Model;

	public:
		enum Flag
		{
			FLAG_DISPOSED =		1 << 0,
			FLAG_VISIBLE =		1 << 1,
			FLAG_ACTIVE =		1 << 2,
			FLAG_HOVERED =		1 << 3,
			FLAG_SELECTED =		1 << 4,
			FLAG_NOPAUSE =		1 << 5,
			FLAG_NOCLIP =		1 << 6,
			FLAG_NOHOVER =		1 << 7,
			FLAG_NOTIME =		1 << 8,
			FLAG_NOEVENTS =		1 << 9,
			//FLAG_NOLIGHT =		1 << 10,
			FLAG_NOSIGNALS =	1 << 11,
			FLAG_NOSCISSOR =	1 << 12,
			FLAG_NORENDER =		1 << 13, //local, will not affect children
			FLAG_NOSORT =		1 << 14, //local, will not affect children
			FLAG_WORKSPACE =	1 << 15,
			FLAG_NOPRNTROT =	1 << 16,
			FLAG_NOPRNTPOS =	1 << 17,
			FLAG_NOPRNTSCL =	1 << 18,
			FLAG_NOPRNTCLR =	1 << 19,
			FLAG_NOUPDATE =		1 << 20,
			FLAG_NOHOVERSOLO =	1 << 21,
			FLAG_NOCAMERAFRAME= 1 << 22,
			FLAG_NOSTYLEUPDATE= 1 << 23,
		};

		enum StateFlag
		{
			STATE_RENDER = 1 << 0,
			STATE_DEBUG = 2 << 0,
			STATE_ANIMATE = 2 << 0,
		};

		enum TransformFlag
		{
			TF_POSITION = 1 << 0,
			TF_ROTATION = 1 << 1,
			TF_SCALE = 1 << 2,
			TF_COLOR = 1 << 3,
		};

		void Dispose() {SetFlags(FLAG_DISPOSED, true); }
		void Undispose() {SetFlags(FLAG_DISPOSED, false); }

		virtual void ApplySameSettingsTo(Entity* entity) {}

#ifndef DECLARE_WIDGET

	public:
		ENTITY(
			const std::string& name = "Object#",
			const vec3_t& position = vec3_t::Zero,
			const quat_t& rotation = quat_t::Identity,
			const vec3_t& scale = vec3_t::One,
			const color_t& color = color_t());
#else
	public:
		ENTITY(
			const std::string& name = "Widget#",
			const vec2_t& position = vec2_t::Zero,
			const vec_t& width = 100,
			const vec_t& height = 100,
			const vec_t& rotation = 0,
			const vec2_t& scale = vec2_t::One,
			const color_t& color = color_t(),
			const std::wstring& text = std::wstring(),
			shared_ptr<const IMaterial> image = null_ptr());
#endif
		virtual ~ENTITY();
		shared_ptr<ENTITY> GetHovered() const { return m_hoverlist.cache(); } 
		shared_ptr<ENTITY> FindEntity(const std::string& name) const;
		shared_ptr<ENTITY> FindEntityHash(int hash) const;

		static bool Predicate(shared_ptr<const ENTITY> left, shared_ptr<const ENTITY> right);

	protected:
		void ProcessHovers();

	private:
		void Recalculate(bool recursive = false) const;
		bool RecalculateIfDirty() const;

	public:
		void Animate(float timestep);

	public:
		bool IsUnhoverable(bool lazy = false) const;
		bool IsInteractive() const;

		virtual const box3_t& GetBoundingBox() const { static box3_t box; return box; }
		virtual const mat4_t& GetMatrix() const;
		virtual const mat4_t& GetLocalMatrix() const;

		virtual void SetMatrix(const mat4_t& value);
		virtual void SetLocalMatrix(const mat4_t& value);

		virtual color_t GetColor() const;

		float GetTransparency() const { return GetLocalColor().GetAlpha(); }
		void SetTransparency(float value)
		{
			color_t c = GetLocalColor(); c.SetAlpha(value);
			return SetLocalColor(c);
		}

		virtual void SetFlags(int m_flags, bool bSet);
		virtual bool GetFlags(int m_flags) const;

		virtual PyObject* GetPythonObject() const { return 0; }
		
		const std::string& GetName() const { return m_name; }
		void SetName(const std::string& a);
		const color_t& GetLocalColor() const { return m_color; }

		bool operator== (shared_ptr<const ENTITY> right) const { return this && right && this->uid == right->uid; }
		bool operator!= (shared_ptr<const ENTITY> right) const { return !this || !right || this->uid != right->uid; }

		bool Detach();
		bool OnDetach();
		void SetParent(ENTITY* parent);
		void Attach(shared_ptr<ENTITY> element);//, bool absolute_transformation = false);
		bool AddController(shared_ptr<Controller> controller);
		bool AddEventHandler(const std::string& name, const std::string& script, bool with_params = true);
		bool RemoveEventHandler(const std::string& name);
		void SetDirty(int flags = -1) const;

		void Clear();

		template <typename T>
		inline void RemoveControllers()
		{
			for (Controllers::iterator it = m_controllers.begin(); it != m_controllers.end(); ++it)
			{
				if (dynamic_pointer_cast<T>(*it)) 
				{
					(*it)->Dispose();
				}
			}
		}


	public:
		const int uid;

		float m_speed;

		bool Show(const std::string& entity_name = std::string());
		bool Hide(const std::string& entity_name = std::string());

		inline bool IsSelected() const { return GetFlags(FLAG_SELECTED); }
		void Select(bool select);

		vec3_t m_pivot;
		
	private:
		inline void Hack_EnsureParent(ENTITY* parent) {
			_parent = parent;
		}

#ifndef DECLARE_WIDGET
	public:
		void Debug(IHelper* helper) const;
		void Render(IClientScene* scene) const;

		virtual bool CanBeSeen() const;
		virtual std::string GetClassName() const { return "Object"; }

		void CallColorChangedCallback();

		bool CheckIfIntersectsBoundingSphere(const line_t& line) const;

		virtual float GetAnimLayerWeight(int layer) const { return 0; }
		virtual void SetAnimLayerWeight(int layer, float weight) {}

		inline const vec3_t& GetPosition() const { return m_position; }
		inline const quat_t& GetRotation() const { return m_rotation; }
		inline const vec3_t& GetScale() const { return m_scale; }
		inline void SetPosition(const vec3_t& value) { m_position = value; SetDirty(TF_POSITION); }
		inline void SetRotation(const quat_t& value) { m_rotation = value; SetDirty(TF_ROTATION); }
		inline void SetScale(const vec3_t& value) { m_scale = value; SetDirty(TF_SCALE); }
		inline void SetLocalColor(const color_t& value) { m_color = value; CallColorChangedCallback(); }
		inline bool IsRenderable() const { return RenderCallback || m_renderlist.size() > 0; }
		inline bool IsDebugable() const { return DebugCallback || m_debuglist.size() > 0; }
		inline bool IsAnimatable() const
		{
			return AnimateCallback || IntersectionChecker || 
				m_animatelist.size() > 0 || m_controllers.size() > 0;
		}
		inline color_t GetTeam() const { return m_team; }
		inline void SetTeam(color_t value) { if (TeamChangedCallback) (this->*TeamChangedCallback)(value); m_team = value; }

		bool IsPaused() const;

		bool Intersect(const line_t& line) const;

	public:
		mutable vec3_t m_hotspot;

	protected:
		typedef std::list<ENTITY*> EntityList;
		EntityList m_renderlist, m_animatelist, m_debuglist;

		color_t m_team;

		/**
			CALLBACKS instead of VIRTUAL FUNCTIONS are to make it simplier to figure out if 
			inherited class instance is ANIMATED, RENDERED or INTERSECTABLE and optimize update routine.
		**/

		typedef bool (Entity::*IntersectionCheckerT)(const math::Line&) const; //line is in *local* space
		typedef void (Entity::*RenderCallbackT)(IClientScene*) const;
		typedef void (Entity::*DebugCallbackT)(IHelper*) const;
		typedef void (Entity::*AnimateCallbackT)(float);
		typedef void (Entity::*RecalculateCallbackT)();
		typedef void (Entity::*TeamChangedCallbackT)(color_t);
		typedef void (Entity::*ColorChangedCallbackT)();

		IntersectionCheckerT IntersectionChecker;
		AnimateCallbackT AnimateCallback;
		RenderCallbackT RenderCallback;
		DebugCallbackT DebugCallback;
		RecalculateCallbackT RecalculateCallback;
		TeamChangedCallbackT TeamChangedCallback;
		ColorChangedCallbackT ColorChangedCallback;
#else
	public:
		//basic visual accessors
		virtual bool IsHovered() const;
		virtual void Render(IClientScene* scene) const;
		virtual int GetWidth() const;
		virtual int GetHeight() const;
		virtual int GetTextWidth() const;
		virtual int GetTextHeight() const;
		virtual std::string GetClassName() const { return "Widget"; }
		inline vec2_t GetPosition() const { return vec2_t(m_position.x, m_position.y); }
		inline float GetRotation() const { return m_rotation.GetRoll(); }
		inline vec2_t GetScale() const  { return vec2_t(m_scale.x, m_scale.y); }
		inline void SetPosition(const vec2_t& value) { m_position.Assign(value.x, value.y, 0); SetDirty(TF_POSITION); }
		inline void SetRotation(float value) { m_rotation = quat_t::RotationYawPitchRoll(0, 0, value);  SetDirty(TF_ROTATION); }
		inline void SetScale(const vec2_t& value) { m_scale.Assign(value.x, value.y, 1); SetDirty(TF_SCALE); }
		inline void SetWidth(int value) { if (m_custom.width != value) { m_custom.width = value; OnStyleChanged(); } }
		inline void SetHeight(int value) { if (m_custom.height != value) { m_custom.height = value; OnStyleChanged(); } }
		inline int GetLevel() const { return GetParent() ? (GetParent()->GetLevel() + 1) : 0; }
		inline shared_ptr<const IMaterial> GetImage() const { return m_image; }
		inline void SetImage(shared_ptr<const IMaterial> value) { m_image = value; OnStyleChanged(); }
		inline shared_ptr<const shared::css::Sheet> GetStyle() const
		{
			return (!m_stylesheet && GetParent()) ? GetParent()->GetStyle() : m_stylesheet;
		}
		int GetColumnWidth() const { return GetWidth() / m_columns; }
		int GetFrameWidth() const;
		int GetFrameHeight(bool force = false) const;
		dim_t GetFrameSize() const { return dim_t(GetFrameWidth(), GetFrameHeight(true)); }
		const std::wstring& GetText() const;
		void SetLocalColor(const color_t& value);
		void SetText(const std::wstring& value);
		void SetCustomClasses(const std::string& initializer);
		bool HasClass(const std::string& name) const;
		void LoadStyle(const std::string& filename);
		void AddPseudoClass(const std::string& value);
		void RemovePseudoClass(const std::string& value);
		bool AddTimeHandler(float sec, const std::string& script);
		bool RemoveTimeHandler(float sec);
		//sensor-related
		inline float GetTime() const { return m_time; }
		inline void SetTime(float value) { SetFlags(FLAG_NOTIME, false), m_time = value; }

		int GetColumns() const { return m_columns;}
		void SetColumns(int value) { m_columns = value > 1 ? value : 1; bStyleChanged = true; }

		bool IgnoreDim() const;

		void RecalculateColors(bool recursive);

		shared_ptr<const ENTITY> GetWindow() const { 
			return (GetParent() && GetParent()->GetParent() && !GetFlags(FLAG_WORKSPACE)) ? 
				GetParent()->GetWindow() : shared_from_this();
		}

		void UpdateStyle(bool force = false); // should be protected :(

	protected:
		void OnStyleChanged();

		bool bStyleChanged;

	public:
		enum float_t { FLOAT_NONE, FLOAT_LEFT, FLOAT_RIGHT };
		enum fontstyle_t { FONTSTYLE_NORMAL, FONTSTYLE_ITALIC };
		enum position_t { POSITION_STATIC, POSITION_ABSOLUTE, POSITION_FIXED, POSITION_RELATIVE, POSITION_INHERIT };

		std::list<std::string>::const_iterator custom_classes_begin() /*const*/ { return m_custom_classes.begin(); }
		std::list<std::string>::const_iterator custom_classes_end() /*const*/ { return m_custom_classes.end(); }

		const box2_t& GetUV() const { return image_uv; }
		void SetUV(const box2_t& value) { image_uv = value; OnStyleChanged(); }

	protected:
		vec2_t m_default_position;
		std::wstring m_text;
		std::list<std::string> m_custom_classes, m_pseudo_classes;

		struct {
			std::list<hash_t> custom_classes, pseudo_classes;
		} m_hash;

		shared_ptr<const IMaterial> m_image;
		shared_ptr<ICallback> m_text_callback;

	public:
		struct style_t {
			struct {
				std::string family;
				int size;
				fontstyle_t style;
				std::string variant, weight;
			} font;
			struct {
				IOverlay::Align align;
				std::string decoration, indent, transform;
				struct {
					struct { int shadow; } h, v;
					color_t color;
				} shadow;
				struct {
					struct { int outline; } h, v;
					color_t color;
				} outline;
			} text;
			struct {
				struct {
					int width;
					std::string style;
					color_t color;
				} top, left, bottom, right;
				struct {
					shared_ptr<const IMaterial> source;
					IOverlay::Repeat repeat;
				} image;
			} border;
			struct { int top, left, bottom, right; } margin, padding;
			struct {
				color_t color;
				shared_ptr<const IMaterial> image;
				int padding;
			} background;
			struct {
				int width;
			} max;
			struct {
				int width;
			} min;
			struct {
				shared_ptr<const ISound> mouseup, mousedown, mouseenter, mouseleave;
			} snd;
			struct { int spacing; } letter;
			struct { int align; } vertical;
			struct { IOverlay::Whitespace space; } white;
			struct { int height; } line;
			struct { struct { color_t min, max; } color; } highlight;
			//color_t color;
			struct { color_t min, max; } color;
			std::string direction;
			position_t position;
			int top, left;//, bottom, right;
			int width, height;
			float_t _float;
		};
		
	private:
		style_t m_style;
		bool b_styleWasSet;

		struct {
			int width, height;
		} m_custom;

		shared_ptr<const shared::css::Sheet> m_stylesheet;
		shared::css::Class m_cached_style;
		const ISurface* m_text_surface;
		typedef std::list<std::pair<float, std::string> > Timers;
		Timers m_timers;
		float m_time;
		int m_sortvalue;
		int m_columns;

		box2_t image_uv;

		dim_t text_size;

	public:
		float text_shuffle;
#endif
	public:
		Event
#ifndef DECLARE_WIDGET
			//Trigger* functions are hacky so far
			TriggerEnter,
			TriggerLeave,
#endif
			Click,
			DoubleClick,
			MouseMove,
			MouseEnter,
			MouseLeave,
			MouseDown,
			MouseUp,
			/*
			DragEnter,
			DragLeave,
			DragDrop,
			*/
			GotFocus,
			LostFocus,
			ParentChanged,
			VisibleChanged,
			KeyDown,
			KeyUp,
			Created,
			Initializing,
			Disposed;

		void AddTriggerObject(shared_ptr<ENTITY> entity) {
			triggers[entity] = false;
		}

		typedef std::map<shared_ptr<ENTITY>, bool> Triggers;
		Triggers triggers;

		typedef std::list<shared_ptr<Controller> > Controllers;
		const Controllers& GetControllers() const { return m_controllers; }

		Controllers m_controllers; //HAHA!
	protected:
		predicated_queue<ENTITY> m_hoverlist;

		vec3_t m_position;
		quat_t m_rotation;
		vec3_t m_scale;
		color_t m_color;

		static unsigned int WaitForSync;

		int m_flags;

	public:
		float xdelay;
		/**
			A very useful function to optimize searching by name
		**/
		inline bool NameMatchesHash(int hash) const { return m_namehash == hash; }
		inline int GetNameHash() const { return m_namehash; }

		mat4_t *overrideParent;

	private:
		int m_namehash;
		std::string m_name;

		mutable bool m_dirty;
		mutable mat4_t m_worldmatrix, m_localmatrix;
		mutable int m_transform;

		bool m_hack_initialized; // skip one frame of being hoverable

	public:
		mutable int userdata;

#ifndef DECLARE_WIDGET
		bool m_TopMost;
#else
		bool b_UnderWorld;
#endif
		
		int m_UserFlags;
		bool is_Background;
	};

};

#undef ENTITY
#undef ENTITY_

#ifndef DECLARE_WIDGET
#	define DECLARE_WIDGET
#	include "c_entity.hpp"
#	undef DECLARE_WIDGET
#endif

#define ENTITY_INCLUDED
#endif
