#pragma once

#include "iobject.hpp"

struct IRendererObject: IObject
{
	virtual void Update(struct IRenderer* renderer) = 0;
	virtual void Unload(struct IRenderer* renderer) = 0;
	virtual bool IsLoaded() const = 0;
};
