#pragma once

#if (DIRECT3D_VERSION == 0x0800)
#	include <d3d8.h>
#	include <d3d8types.h>
#	include <d3dx8tex.h>
#	include <d3dx8.h>
#	include <dxerr8.h>
#	pragma comment(lib, "d3dx8.lib")
#	pragma comment(lib, "d3d8.lib")
#	pragma comment(lib, "dxerr8.lib")
	static const char* sRenderName = "DirectX 8.0";
#elif (DIRECT3D_VERSION == 0x0900)
#	include <d3d9.h>
#	include <d3d9types.h>
#	include <d3dx9tex.h>
#	include <dxerr.h>
#	pragma comment(lib, "d3dx9.lib")
#	pragma comment(lib, "d3d9.lib")
#	pragma comment(lib, "dxerr.lib")
	static const char* sRenderName = "DirectX 9.0";
#endif

#include <dsetup.h>

#define D3D_OVERLOADS
#define WIN32_LEAN_AND_MEAN 

#define DIRECTINPUT_VERSION DIRECT3D_VERSION

#include "config.hpp"

#include "../shared/shared.hpp"
#include "../interfaces/iclientframework.hpp"

#define STREAM(x) \
{ \
	HRESULT hr = x;\
	if (hr != S_OK)  {\
	framework->Log(IFramework::MSG_ERROR, "%s returned %s at %s(%d)", #x, DXGetErrorDescription(hr), __FILE__, __LINE__);\
	} \
}

extern IClientFramework* framework;

D3DDISPLAYMODE g_displayMode;
DWORD g_behaviourFlags = D3DCREATE_HARDWARE_VERTEXPROCESSING;

class Driver {
public:
	static bool Open(HWND hWnd, D3DPRESENT_PARAMETERS *params)
	{
		if (!hWnd || !params)
		{
			framework->Log(IFramework::MSG_ERROR, "No parameters passed to renderer");
			return false;
		}

		if (!(m_pD3D = Direct3DCreate(D3D_SDK_VERSION)))
		{
			framework->Log(IFramework::MSG_ERROR, "Direct3DCreate failed");
			return false;
		}

		m_pD3D->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &g_displayMode);
		params->BackBufferFormat = g_displayMode.Format;

		/*
#if (DIRECT3D_VERSION == 0x0800)
		if( SUCCEEDED(m_pD3D->CheckDeviceMultiSampleType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, params->BackBufferFormat, FALSE, D3DMULTISAMPLE_4_SAMPLES)))
#elif (DIRECT3D_VERSION == 0x0900)
		if( SUCCEEDED(m_pD3D->CheckDeviceMultiSampleType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, params->BackBufferFormat, FALSE, D3DMULTISAMPLE_4_SAMPLES, NULL)) &&
			SUCCEEDED(m_pD3D->CheckDeviceMultiSampleType(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, params->AutoDepthStencilFormat, FALSE, D3DMULTISAMPLE_4_SAMPLES, NULL)))
#endif
		{
			params->MultiSampleType = D3DMULTISAMPLE_4_SAMPLES;
			params->SwapEffect      = D3DSWAPEFFECT_DISCARD;
		}
		*/

		D3DCAPS caps;
		m_pD3D->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &caps);
#if (DIRECT3D_VERSION == 0x0800)
		if ((caps.VertexProcessingCaps & D3DVTXPCAPS_NO_VSDT_UBYTE4)/* || (caps.MaxVertexBlendMatrixIndex < 24)*/)
		{
			framework->Log(IFramework::MSG_ERROR, "Vertex skinning is not supported by the graphics device");
			return false;
		}
#else
		framework->Log(IFramework::MSG_LOG, "Maximum vertex shader constants: %d", caps.MaxVertexShaderConst);

		const int iVertexShaderModel_Major = D3DSHADER_VERSION_MAJOR(caps.VertexShaderVersion);
		const int iPixelShaderModel_Major = D3DSHADER_VERSION_MAJOR(caps.PixelShaderVersion);
		const int iVertexShaderModel_Minor = D3DSHADER_VERSION_MINOR(caps.VertexShaderVersion);
		const int iPixelShaderModel_Minor = D3DSHADER_VERSION_MINOR(caps.PixelShaderVersion);
		framework->Log(IFramework::MSG_LOG, "Supported VertexShaderVersion %d.%d", iVertexShaderModel_Major, iVertexShaderModel_Minor);
		framework->Log(IFramework::MSG_LOG, "Supported PixelShaderVersion %d.%d", iPixelShaderModel_Major, iPixelShaderModel_Minor);
		if (caps.VertexShaderVersion < D3DVS_VERSION(1, 1))
		{
			framework->Log(IFramework::MSG_ERROR, "Required shader model is not supported by device, enabling SOFTWARE_VERTEXPROCESSING");
			
			g_behaviourFlags = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
		}

		if (!(b_PixelShadersSupported = (caps.PixelShaderVersion >= D3DPS_VERSION(2, 0))))
		{
			framework->Log(IFramework::MSG_WARNING, "Pixel shaders 2.0 are not supported");
		}
		//b_PixelShadersSupported = false;
#endif

		if (FAILED(m_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, g_behaviourFlags, params, &m_pD3DDevice)))
		{
			framework->Log(IFramework::MSG_ERROR, "CreateDevice() failed");

			g_behaviourFlags = D3DCREATE_SOFTWARE_VERTEXPROCESSING;

			if (FAILED(m_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, g_behaviourFlags, params, &m_pD3DDevice)))
			{
				framework->Log(IFramework::MSG_ERROR, "CreateDevice() with SOFTWARE_VERTEXPROCESSING failed");

				if (FAILED(m_pD3D->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, hWnd, g_behaviourFlags, params, &m_pD3DDevice)))
				{
					framework->Log(IFramework::MSG_ERROR, "CreateDevice() with SOFTWARE_VERTEXPROCESSING and D3DDEVTYPE_REF failed");

					return false;
				}
				else 
				{
					framework->Log(IFramework::MSG_ERROR, "Managed to CreateDevice() with SOFTWARE_VERTEXPROCESSING and D3DDEVTYPE_REF");
				}
			}
			else
			{
				framework->Log(IFramework::MSG_ERROR, "Managed to CreateDevice() with SOFTWARE_VERTEXPROCESSING");
			}
		}

		framework->Log(IFramework::MSG_LOG, "%s renderer initialized", sRenderName);
		framework->Log(IFramework::MSG_LOG, "Video mode: %dx%d", params->BackBufferWidth, params->BackBufferHeight);

		m_Params = *params;
		p_RenderTexture = NULL;
		p_BackBuffer = NULL;

		m_pD3DDevice->BeginScene();
		m_pD3DDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x00000000, 1.0f, 0);
		m_pD3DDevice->EndScene();
		m_pD3DDevice->Present(NULL, NULL, NULL, NULL);

		RestoreDeviceObjects();

		return true;
	}

	static void RestoreDeviceObjects() {
#if (DIRECT3D_VERSION == 0x0800)
		STREAM(m_pD3DDevice->GetBackBuffer(0, D3DBACKBUFFER_TYPE_MONO, &p_BackBuffer));

		STREAM(m_pD3DDevice->CreateVertexBuffer(
			VERTEX_BUFFER_SIZE * INTERNAL_VERTEX_SIZE,
			D3DUSAGE_WRITEONLY | D3DUSAGE_DYNAMIC,
			INTERNAL_VERTEX_FORMAT,
			D3DPOOL_DEFAULT,
			&p_VertexBuffer));

		STREAM(m_pD3DDevice->CreateIndexBuffer(
			INDEX_BUFFER_SIZE * sizeof(WORD),
			D3DUSAGE_WRITEONLY | D3DUSAGE_DYNAMIC,
			D3DFMT_INDEX16,
			D3DPOOL_DEFAULT,
			&p_IndexBuffer));

#elif (DIRECT3D_VERSION == 0x0900)
		STREAM(m_pD3DDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &p_BackBuffer));

		STREAM(m_pD3DDevice->CreateVertexBuffer(
			VERTEX_BUFFER_SIZE * INTERNAL_VERTEX_SIZE,
			D3DUSAGE_WRITEONLY | D3DUSAGE_DYNAMIC,
			INTERNAL_VERTEX_FORMAT,
			D3DPOOL_DEFAULT,
			&p_VertexBuffer,
			NULL));

		STREAM(m_pD3DDevice->CreateIndexBuffer(
			INDEX_BUFFER_SIZE * sizeof(WORD),
			D3DUSAGE_WRITEONLY | D3DUSAGE_DYNAMIC,
			D3DFMT_INDEX16,
			D3DPOOL_DEFAULT,
			&p_IndexBuffer,
			NULL));

		D3DSURFACE_DESC desc;
		STREAM(p_BackBuffer->GetDesc(&desc));
		STREAM(m_pD3DDevice->CreateTexture(
			desc.Width,
			desc.Height,
			1,
			0,
			D3DFMT_A8R8G8B8,
			D3DPOOL_SYSTEMMEM,
			&m_pSysTexture, NULL));

		D3DVERTEXELEMENT9 decl[] = {
			{0,  0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
			{0, 12, D3DDECLTYPE_FLOAT4,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDWEIGHT, 0 },
			{0, 28, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0 },
			{0, 32, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_NORMAL, 0},
			{0, 44, D3DDECLTYPE_D3DCOLOR, D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_COLOR, 0},
			{0, 48, D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
			{0, 56, D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 1},
			D3DDECL_END()
		};

		STREAM(m_pD3DDevice->CreateVertexDeclaration(decl, &p_VertexDecl));
		
#endif
	}

	static void InvalidateDeviceObjects() {
		if (p_BackBuffer) {
			p_BackBuffer->Release();
			p_BackBuffer = 0;
		}
		if (p_RenderTexture) {
			p_RenderTexture->Release();
			p_RenderTexture = 0;
		}
		if (p_VertexBuffer) {
			p_VertexBuffer->Release();
			p_VertexBuffer = 0;
		}
		if (p_IndexBuffer) {
			p_IndexBuffer->Release();
			p_IndexBuffer = 0;
		}

#if (DIRECT3D_VERSION == 0x0900)
		if (p_VertexDecl) {
			p_VertexDecl->Release();
			p_VertexDecl = 0;
		}
		if (m_pSysTexture) {
			m_pSysTexture->Release();
			m_pSysTexture = 0;
		}
#endif

	}

	static void Close() {
		InvalidateDeviceObjects();

		if (m_pD3D) {
			m_pD3D->Release();
			m_pD3D = 0;
		}
		if (m_pD3DDevice) {
			m_pD3DDevice->Release();
			m_pD3DDevice = 0;
		}
#if (DIRECT3D_VERSION == 0x0900)
		if (m_pSysTexture) {
			m_pSysTexture->Release();
		}
#endif
	}

	static LPDIRECT3DSURFACE GetBackBufferSurface() {
		return p_BackBuffer;
	}

	static LPDIRECT3DSURFACE GetDepthStencilSurface() {
		LPDIRECT3DSURFACE pSurface = NULL;
		m_pD3DDevice->GetDepthStencilSurface(&pSurface);
		return pSurface;
	}

	static bool b_PixelShadersSupported;

	static LPDIRECT3D m_pD3D;
	static LPDIRECT3DDEVICE m_pD3DDevice;
	static LPDIRECT3DTEXTURE p_RenderTexture;
	static LPDIRECT3DSURFACE p_BackBuffer;
	static D3DPRESENT_PARAMETERS m_Params;

#if (DIRECT3D_VERSION == 0x0900)
	static LPDIRECT3DTEXTURE m_pSysTexture; //a workaround for copybackbuffer
	static LPDIRECT3DVERTEXDECLARATION9 p_VertexDecl;
#endif

public:
	static const int VERTEX_BUFFER_SIZE = 16384;
	static const int INDEX_BUFFER_SIZE = 32768;
	static const DWORD INTERNAL_VERTEX_FORMAT = D3DFVF_XYZB5 | D3DFVF_LASTBETA_UBYTE4 | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1;
	static const size_t INTERNAL_VERTEX_SIZE = sizeof(vertex_t);

	static LPDIRECT3DVERTEXBUFFER p_VertexBuffer;
	static LPDIRECT3DINDEXBUFFER p_IndexBuffer;
};
