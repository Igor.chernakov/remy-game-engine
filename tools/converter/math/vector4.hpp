#pragma once

#include "math.hpp"

namespace math
{
	struct Matrix;

	struct Vector4
	{
		static const int Dimensions = 4;

		union
		{
			Real v[Dimensions];

			struct
			{
				Real x,y,z,w;
			};
		};

		static const Vector4 Zero;
		static const Vector4 One;

		Vector4();
		Vector4(Real x, Real y, Real z, Real w);
		Vector4(const std::string& param);
		Vector4(const Real* param);
		Vector4(const Vector4& param);

		DEFINE_DEFAULT_VECTOR_OPS(Vector4);

		Vector4& Assign(Real x, Real y, Real z, Real w);

		Vector4& Maximize();
		Vector4& Minimize();

		Vector4& operator*=(const Matrix& m);
		Vector4 operator*(const Matrix& m) const;

		static Vector4 Multiply(const Vector4& v, const Matrix& m);
		Vector4& Multiply(const Matrix& m);
	};
};
