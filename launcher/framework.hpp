#pragma once

#include "../interfaces/iclientframework.hpp"
#include "../interfaces/iserverframework.hpp"
#include "../interfaces/iclient.hpp"

#include "timer.hpp"
#include "variables.hpp"

#define DEFINE_GETSET(Class, Name)\
	shared_ptr<Class> Get##Name() const { return this->Name; } \
	void Set##Name(shared_ptr<Class> value) { this->Name = value; } \
	shared_ptr<Class> Name;

struct ClientFramework: IClientFramework
{
	DEFINE_GETSET(IClientScene, Scene);
	DEFINE_GETSET(IFileSystem, FileSystem);
	DEFINE_GETSET(ISoundSystem, SoundSystem);
	DEFINE_GETSET(IResources, Resources);
	DEFINE_GETSET(ITimer, Timer);
	DEFINE_GETSET(IWindow, Window);
	DEFINE_GETSET(IVariables, Variables);
	DEFINE_GETSET(IBucket, Bucket);
	DEFINE_GETSET(ILocalization, Localization);
	DEFINE_GETSET(IRendererObjectManager, RendererObjectManager);
	
	void Log(msg_type_t mt, const char* message, ...) const {
		char buffer[1024] = { 0 };
		va_list args;
		va_start(args, message);
		vsprintf(buffer, message, args);
		va_end(args);

		extern void SetErrorState();
		extern IClient* client;

		if (client) {
			switch (mt) {
			case MSG_LOG: client->PublishMessage(buffer, color_t(1,1,1)); break;
			case MSG_WARNING: client->PublishMessage(buffer, color_t(1,1,0)); break;
			case MSG_ERROR: client->PublishMessage(buffer, color_t(1,0,0)); SetErrorState(); break;
			}
		}
		std::cout << buffer << std::endl;
#ifdef WIN32
		extern FILE* logfile;
		if (logfile)
		{
			fprintf(logfile, "%s\n", buffer);
		}
#endif
	}
};

struct ServerFramework: IServerFramework
{
	DEFINE_GETSET(IServerScene, Scene);
	DEFINE_GETSET(IFileSystem, FileSystem);
	DEFINE_GETSET(IResources, Resources);
	DEFINE_GETSET(ITimer, Timer);
	DEFINE_GETSET(IVariables, Variables);
	DEFINE_GETSET(IBucket, Bucket);
};

#undef DEFINE_GETSET
