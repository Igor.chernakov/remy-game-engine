#pragma once

#ifdef WIN32

#	define WIN32_LEAN_AND_MEAN
#	include <windows.h>

// Unwanted VC++ level 4 warnings to disable.
#	pragma warning(disable : 4244) /* conversion to float, possible loss of data                     */
//#	pragma warning(disable : 4699) /* creating precompiled header                                 */
#	pragma warning(disable : 4200) /* Zero-length array item at end of structure, a VC-specific extension   */
#	pragma warning(disable : 4100) /* unreferenced formal parameter                              */
//#	pragma warning(disable : 4514) /* unreferenced inline function has been removed                  */
//#	pragma warning(disable : 4201) /* nonstandard extension used : nameless struct/union               */
#	pragma warning(disable : 4710) /* inline function not expanded                                 */
//#	pragma warning(disable : 4702) /* unreachable code in inline expanded function                     */
//#	pragma warning(disable : 4711) /* function selected for autmatic inlining                        */
//#	pragma warning(disable : 4725) /* Pentium fdiv bug                                          */
#	pragma warning(disable : 4127) /* Conditional expression is constant                           */
//#	pragma warning(disable : 4512) /* assignment operator could not be generated                           */
//#	pragma warning(disable : 4530) /* C++ exception handler used, but unwind semantics are not enabled     */
//#	pragma warning(disable : 4245) /* conversion from 'enum ' to 'unsigned long', signed/unsigned mismatch */
#	pragma warning(disable : 4305) /* truncation from 'const double' to 'float'                            */
//#	pragma warning(disable : 4238) /* nonstandard extension used : class rvalue used as lvalue             */
//#	pragma warning(disable : 4251) /* needs to have dll-interface to be used by clients of class 'ULinker' */
//#	pragma warning(disable : 4275) /* non dll-interface class used as base for dll-interface class         */
//#	pragma warning(disable : 4511) /* copy constructor could not be generated                              */
//#	pragma warning(disable : 4284) /* return type is not a UDT or reference to a UDT                       */
//#	pragma warning(disable : 4355) /* this used in base initializer list                                   */
//#	pragma warning(disable : 4097) /* typedef-name '' used as synonym for class-name ''                    */
//#	pragma warning(disable : 4291) /* typedef-name '' used as synonym for class-name ''                    */

#endif

//STL includes

#include <iostream>
#include <sstream>
#include <memory>
#include <string>
#include <list>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>



//threading
#include <boost/thread.hpp>

#include "types.hpp"

#include "resource.hpp"
#include "css.hpp"

#include "tinyxml2.h"

#define XML_FOREACH(variable, name, parent) \
	if (parent) \
	for (const tinyxml2::XMLElement *variable = parent->FirstChildElement(name); \
		variable; \
		variable = variable->NextSiblingElement(name))

static std::string f_DosToUnixName(const std::string& name){
	std::string s = name;
	std::replace(s.begin(), s.end(), '\\', '/');
	return s;
}

static std::string f_UnixToDosName(const std::string& name){
	std::string s = name;
	std::replace(s.begin(), s.end(), '/', '\\');
	return s;
}

static std::string f_StripExtension(const std::string& filename) {
    size_t last = filename.find_last_of(".");
    if (last == std::string::npos)
		return filename;
    return filename.substr(0, last); 
}

static std::string f_Extension(const std::string& filename){
    size_t last = filename.find_last_of(".");
    if (last == std::string::npos)
		return std::string();
    return filename.substr(last); 
}
static std::string f_StripName(const std::string& filename){
	std::string worker = f_DosToUnixName(filename);
    size_t last = filename.find_last_of("/");
    if (last == std::string::npos)
		return ".";
    return filename.substr(0, last); 
}

static std::string f_Name(const std::string& filename){
	std::string worker = f_DosToUnixName(filename);
    size_t last = filename.find_last_of("/");
    if (last == std::string::npos)
		return filename;
    return filename.substr(last + 1);
}

#define F_BlockSkip(file) \
fseek(file, F_BlockReadSize(file), SEEK_CUR);

#define F_BlockReadLoop(file, size) \
for (long eof = ftell(file) + size; ftell(file) < eof;)

#define F_BlockRead(file, variable) \
fread(&variable, F_BlockReadSize(file), 1, file);

#define F_BlockReadConverVector(file, ptr, type) \
if (int size = F_BlockReadSize(file)) { \
	std::vector<type> _load(size / sizeof(type)); \
	fread((char*)&*_load.begin(), sizeof(type), _load.size(), file); \
	ptr.resize(_load.size()); \
	for (size_t i = 0; i < ptr.size(); ptr[i] = _load[i], ++i); \
}

#define F_BlockReadVector(file, ptr, type) \
if (int size = F_BlockReadSize(file)) { \
	ptr.resize(size / sizeof(type)); \
	fread((char*)&*ptr.begin(), sizeof(type), ptr.size(), file); \
}

#define F_BlockReadArray(file, array, type) \
array##Num = F_BlockReadSize(file) / sizeof(type); \
array = malloc(sizeof(type) * array##Num);\
fread(array, sizeof(type), array##Num, file);

#define F_BlockReadArrayMax(file, array, type, count) \
array##Num = F_BlockReadSize(file) / sizeof(type); \
fread(array, sizeof(type), array##Num, file);

static int F_BlockReadHeader(FILE* file) {
	int header;
	fread(&header, 4, 1, file);
	return header;
}

static int F_BlockReadSize(FILE* file) {
	int size;
	fread(&size, 4, 1, file);
	return size < 0 ? -size : size;
}

static std::string F_BlockReadString(FILE* file) {
	int size = F_BlockReadSize(file); 
	char* buffer = new char[size];
	fread(buffer, size, 1, file);
	std::string variable(buffer, size);
	delete[] buffer;
	return variable;
}

// trim from start
static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
}

