#include <stdio.h>
#include <string.h>
#include "launcher.h"

extern int mouse_x, mouse_y;

struct gamemessage_t {
	enum my_message_t message_type;
	int param;
	int readme;
};

const int MESSAGE_QUEUE_SIZE = 128;
int write_message = 0;
int read_message = 0;

struct gamemessage_t g_MessageQueue[MESSAGE_QUEUE_SIZE];

void ResetGameMessages()
{
	memset(g_MessageQueue, 0, sizeof(g_MessageQueue));
	write_message = 0;
	read_message = 0;
}

int PopGameMessage(enum my_message_t* message, int* param)
{
	struct gamemessage_t *gamemessage = &g_MessageQueue[read_message % MESSAGE_QUEUE_SIZE];
	if (gamemessage->readme == 0)
		return 0;
	if (message != NULL)
		*message = gamemessage->message_type;
	if (param != NULL)
		*param = gamemessage->param;
	gamemessage->readme = 0;
	read_message++;
	return 1;
}

void SendGameMessage(enum my_message_t message, int param)
{
	struct gamemessage_t gamemessage = {message, param, 1};

	if (write_message > 0 &&
		g_MessageQueue[(write_message - 1) % MESSAGE_QUEUE_SIZE].message_type == message &&
		g_MessageQueue[(write_message - 1) % MESSAGE_QUEUE_SIZE].readme)
	{
		write_message--; //update this message, not add a new one
	}

	g_MessageQueue[write_message++ % MESSAGE_QUEUE_SIZE] = gamemessage;
	//if (!clientframework || !clientframework->GetScene())
	//	return;
	//clientframework->GetScene()->OnSystemMsg(IClientScene::MSG_KEY_DOWN, (int)lParam);
}

