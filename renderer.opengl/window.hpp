#pragma once

#include "../interfaces/iclientframework.hpp"
#include "../interfaces/iwindow.hpp"

#include "../shared/types.hpp"

#ifdef WIN32
#	include <windows.h>
#else
#	include <X11/Xlib.h>
#	include <X11/Xutil.h>
#	include <X11/keysym.h>
#	include <X11/cursorfont.h>
#	include <GL/gl.h>
#	include <GL/glx.h>
#endif

extern IClientFramework* framework;

class VideoWindow: public IWindow
{
public:

#ifdef WIN32
	VideoWindow(const std::string& name, const dim_t& size, bool fullscreen)
	{
		WNDCLASSEX wndClass = { 0 };

	//  wndClass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
	//  wndClass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
		wndClass.cbSize = sizeof(wndClass);
		wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
		wndClass.lpfnWndProc = VideoWindow::WndProc;
		wndClass.lpszClassName = "T_OPENGL";
		wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

		RegisterClassEx(&wndClass);

		this->hWnd = CreateWindowEx(
			0,
			"T_OPENGL",
			name.c_str(),
			WS_OVERLAPPED|WS_CAPTION|WS_MINIMIZEBOX|WS_SYSMENU,
			0,
			0,
			size.width,
			size.height,
			NULL,
			NULL,
			NULL,
			NULL);

		Centralize();


		Show(true);

		SetCursor(CURSOR_ARROW);
	}

	void Show(bool show)
	{
		if (show)
		{
			::ShowWindow(hWnd, SW_SHOW);
		}
		else
		{
			::ShowWindow(hWnd, SW_HIDE);
		}

		::UpdateWindow(hWnd);
	}

	bool IsVisible() const
	{
		return false;
	}

	bool Centralize()
	{
		DEVMODE dmSettings;

		RECT cRect, rWindow;

		GetWindowRect(hWnd, &cRect);

		if(EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&dmSettings))
		{
			rWindow.left  = (dmSettings.dmPelsWidth - (cRect.right - cRect.left)) / 2;
			rWindow.right	= (cRect.right - cRect.left) + rWindow.left;
			rWindow.top = (dmSettings.dmPelsHeight - (cRect.bottom - cRect.top)) / 2;
			rWindow.bottom	= (cRect.bottom - cRect.top) + rWindow.top;
		}
		else
		{
			return false;
		}

		AdjustWindowRect( &rWindow, GetWindowLong(hWnd, GWL_STYLE), false);

		SetWindowPos(hWnd, NULL, rWindow.left, rWindow.top, rWindow.right  - rWindow.left, rWindow.bottom - rWindow.top, 0);

		return true;
	}

	HWND GetWindow() const
	{
		return this->hWnd;
	}

	HDC GetDC() const
	{
		return this->hDC;
	}

	dim_t GetSize() const
	{
		RECT r;

		GetWindowRect(hWnd, &r);

		return dim_t(r.right - r.left, r.bottom - r.top);
	}

	point_t GetCursorPos() const
	{
		POINT _mouse;

		::GetCursorPos(&_mouse);

		::ScreenToClient(this->hWnd, &_mouse);

		return point_t(_mouse.x, _mouse.y);
	}

	void SetCursorPos(const point_t& pos)
	{
		POINT p;

		p.x = pos.x;

		p.y = pos.y;

		::ClientToScreen(this->hWnd, &p);

		::SetCursorPos(p.x, p.y);
	}

	void SetCursor(Cursor cursor)
	{
		this->m_cursor = cursor;

		switch (cursor)
		{
			case CURSOR_NONE:
				::SetCursor(LoadCursor(NULL,IDC_ARROW));
				::ShowCursor(false);
				break;
			case CURSOR_ARROW:
				::SetCursor(LoadCursor(NULL,IDC_ARROW));
				::ShowCursor(true);
				break;
			case CURSOR_HAND:
				::SetCursor(LoadCursor(NULL,IDC_HAND));
				::ShowCursor(true);
				break;
			case CURSOR_HOLD:
				::SetCursor(LoadCursor(NULL,IDC_HAND));
				::ShowCursor(true);
				break;
			case CURSOR_WAIT:
				::SetCursor(LoadCursor(NULL,IDC_WAIT));
				::ShowCursor(true);
				break;
		}
	}

private:

	static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		if (framework->GetScene())
		{
			switch(msg)
			{
			case WM_LBUTTONDOWN: framework->GetScene()->OnSystemMsg(IClientScene::MSG_LBUTTON_DOWN, lParam); break;
			case WM_LBUTTONUP: framework->GetScene()->OnSystemMsg(IClientScene::MSG_LBUTTON_UP, lParam); break;
			case WM_RBUTTONDOWN: framework->GetScene()->OnSystemMsg(IClientScene::MSG_RBUTTON_DOWN, lParam); break;
			case WM_RBUTTONUP: framework->GetScene()->OnSystemMsg(IClientScene::MSG_RBUTTON_UP, lParam); break;
			case WM_KEYDOWN: framework->GetScene()->OnSystemMsg(IClientScene::MSG_KEY_DOWN, lParam); break;
			}
		}

		switch(msg)
		{
	//		if (renderer)
	//			HANDLE_MSG(hWnd,WM_ACTIVATE, renderer->OnActivate);

		case WM_KEYDOWN:
		case WM_SYSKEYDOWN:
			switch (wParam)
			{
				case 115:
					if (GetKeyState(VK_MENU) & 0x80)
						PostQuitMessage(0);
					break;
				case 123:
					//if (framework->GetRenderer())
					//{
					//	framework->GetRenderer()->TakeScreenshot();
					//}
					break;
			};
			return TRUE;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		};
		return DefWindowProc(hWnd,msg,wParam,lParam);
	};

private:

	HWND hWnd;
	HDC hDC;

#else

	VideoWindow(const std::string& name, const dim_t& size, bool fullscreen)
	{
		int attrib[] =
		{
			GLX_RGBA, GLX_DOUBLEBUFFER,
			GLX_RED_SIZE, 4,
			GLX_GREEN_SIZE, 4,
			GLX_BLUE_SIZE, 4,
			GLX_DEPTH_SIZE, 16,
			None
		};
   		int scrnum;
		unsigned long mask;
		Window root;
		XVisualInfo *visinfo;
		GLint max[2] = { 0, 0 };

		this->dpy = XOpenDisplay(0);
		this->screen = XDefaultScreenOfDisplay(this->dpy);

		scrnum = XScreenNumberOfScreen(screen);
		root   = XRootWindow(dpy, scrnum);
		visinfo = glXChooseVisual( dpy, scrnum, attrib );

		// window attributes
		attr.background_pixel = 0;
		attr.border_pixel = 0;
		attr.colormap = XCreateColormap( dpy, root, visinfo->visual, AllocNone);
		attr.event_mask = ExposureMask | KeyPressMask | ButtonPressMask | StructureNotifyMask;
		mask = CWBorderPixel | CWColormap | CWEventMask;

		this->win = XCreateWindow( dpy, root, 0, 0, size.width, size.height, 0, visinfo->depth, InputOutput, visinfo->visual, mask, &attr );

		{
			Atom wmDelete = XInternAtom(this->dpy, "WM_DELETE_WINDOW", True);
			XSetWMProtocols(this->dpy, this->win, &wmDelete, 1);
			XSetStandardProperties(this->dpy, this->win, name.c_str(), name.c_str(), None, NULL, 0, NULL);
			XMapRaised(this->dpy, this->win);
		}

		this->ctx = glXCreateContext( dpy, visinfo, NULL, True );

		XFree(visinfo);

		//XMapWindow(this->dpy, this->win);
		glXMakeCurrent(this->dpy, this->win, this->ctx);

		// Check for maximum size supported by the GL rasterizer
		glGetIntegerv(GL_MAX_VIEWPORT_DIMS, max);

		this->x = 0, this->y = 0;
		this->width = size.width, this->height = size.height;

		SetCursor(CURSOR_ARROW);
	}

	~VideoWindow()
	{
		glXDestroyContext(this->dpy, this->ctx);
		XDestroyWindow(this->dpy, this->win);
		XCloseDisplay(this->dpy);
	}

	void Show(bool show)
	{
	}

	bool IsVisible() const
	{
		return false;
	}

	bool Centralize()
	{
		return true;
	}

	Window* GetWindow() const
	{
		return &this->win;
	}

	Display* GetDisplay() const
	{
		return this->dpy;
	}

	dim_t GetSize() const
	{
		return dim_t(width, height);
	}

	point_t GetCursorPos() const
	{
		XEvent e;

		XQueryPointer(this->dpy, this->win,
				&e.xbutton.root, &e.xbutton.window,
				&e.xbutton.x_root, &e.xbutton.y_root,
				&e.xbutton.x, &e.xbutton.y,
				&e.xbutton.state);

		return point_t(e.xbutton.x, e.xbutton.y);
	}

	void SetCursorPos(const point_t& pos)
	{
		XWarpPointer(this->dpy, this->win, this->win, 0, 0, this->width, this->height, pos.x, pos.y);
	}

	void SetCursor(Cursor cursor)
	{
		this->m_cursor = cursor;

		switch (cursor)
		{
			case CURSOR_NONE:
				{
					Pixmap bm_no;
					Colormap cmap;
					::Cursor no_ptr;
					XColor black, dummy;
					static char bm_no_data[] = {0, 0, 0, 0, 0, 0, 0, 0};
					cmap = DefaultColormap(this->dpy, XScreenNumberOfScreen(this->screen));
					XAllocNamedColor(this->dpy, cmap, "black", &black, &dummy);
					bm_no = XCreateBitmapFromData(this->dpy, this->win, bm_no_data, 8, 8);
					no_ptr = XCreatePixmapCursor(this->dpy, bm_no, bm_no, &black, &black, 0, 0);
					XDefineCursor(this->dpy, this->win, no_ptr);
					XFreeCursor(this->dpy, no_ptr);
					if (bm_no != None) XFreePixmap(this->dpy, bm_no);
					XFreeColors(this->dpy, cmap, &black.pixel, 1, 0);
				}
				break;
			case CURSOR_ARROW:
				XDefineCursor(this->dpy, this->win, XCreateFontCursor(this->dpy, XC_left_ptr));
				break;
			case CURSOR_HAND:
				XDefineCursor(this->dpy, this->win, XCreateFontCursor(this->dpy, XC_hand2));
				break;
			case CURSOR_HOLD:
				XDefineCursor(this->dpy, this->win, XCreateFontCursor(this->dpy, XC_hand1));
				break;
			case CURSOR_WAIT:
				XDefineCursor(this->dpy, this->win, XCreateFontCursor(this->dpy, XC_watch));
				break;
		}
	}

private:

    mutable Display *dpy;
    mutable Screen* screen;
    mutable Window win;
	XSetWindowAttributes attr;
    GLXContext ctx;
    int x, y;
    unsigned int width, height;
#endif

	Cursor GetCursor() const { return m_cursor; }

    Cursor m_cursor;
};
