#import "BasicOpenGLView.h"
#include <Foundation/NSFileManager.h>

#import "launcher.h"

int mouse_x = 0, mouse_y = 0;
int screen_x = 1024, screen_y = 768;

extern int desktop_width, desktop_height, flag_fullscreen;

float timeSinceLastUpdate = 0.1;

#pragma mark ---- OpenGL Capabilities ----

NSPoint location;
char documentsDirectoryC[512];

@implementation BasicOpenGLView

// pixel format definition
+ (NSOpenGLPixelFormat*) basicPixelFormat
{
    NSOpenGLPixelFormatAttribute attributes [] = {
        NSOpenGLPFAWindow,
        NSOpenGLPFADoubleBuffer,	// double buffered
        NSOpenGLPFADepthSize, (NSOpenGLPixelFormatAttribute)16, // 16 bit depth buffer
        NSOpenGLPFAStencilSize, (NSOpenGLPixelFormatAttribute)8, // 8 bit stencil buffer
        (NSOpenGLPixelFormatAttribute)nil
    };
    return [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
}

- (void)animationTimer:(NSTimer *)timer
{
	[self drawRect:[self bounds]];
	
	if (isFullscreen != (flag_fullscreen > 0))
	{
		[self setFullscreen:(flag_fullscreen > 0)];
	}
}

- (void)setFullscreen:(bool)value
{
	isFullscreen = value;
	
	if (isFullscreen)
	{
		[[self window] setContentSize:NSMakeSize(desktop_width, desktop_height)];
		[[self window] setStyleMask:NSBorderlessWindowMask];
		[[self window] setLevel:NSMainMenuWindowLevel + 1];
		[[self window] setFrameOrigin:NSMakePoint(0, 0)];
		[[self window] makeKeyWindow];
	
		screen_x = desktop_width;
		screen_y = desktop_height;
	}
	else
	{
		[[self window] setContentSize:NSMakeSize(1024, 768)];
		[[self window] setStyleMask:NSTitledWindowMask|NSClosableWindowMask];
		[[self window] setLevel:NSNormalWindowLevel];
		[[self window] center];

		screen_x = 1024;
		screen_y = 768;
	}
}

#pragma mark ---- Method Overrides ----

-(void)keyDown:(NSEvent *)theEvent
{
    NSString *characters = [theEvent characters];
    if ([characters length]) {
		const char *c = [characters cStringUsingEncoding:NSWindowsCP1252StringEncoding];
        //unichar character = [characters characterAtIndex:0];
		SendGameMessage(MSG_KEY_DOWN_FORMATTED, (int)*c);
	}
}

- (void)mouseDown:(NSEvent *)theEvent
{
	location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	SendGameMessage(MSG_LBUTTON_DOWN, 0);
}

- (void)rightMouseDown:(NSEvent *)theEvent
{
	location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	SendGameMessage(MSG_RBUTTON_DOWN, 0);
}

- (void)otherMouseDown:(NSEvent *)theEvent
{
	location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	SendGameMessage(MSG_MBUTTON_DOWN, 0);
}

- (void)mouseUp:(NSEvent *)theEvent
{
	SendGameMessage(MSG_LBUTTON_UP, 0);
}

- (void)rightMouseUp:(NSEvent *)theEvent
{
	[self mouseUp:theEvent];
	SendGameMessage(MSG_RBUTTON_UP, 0);
}

- (void)otherMouseUp:(NSEvent *)theEvent
{
	[self mouseUp:theEvent];
	SendGameMessage(MSG_MBUTTON_UP, 0);
}

- (void)mouseDragged:(NSEvent *)theEvent
{
	location = [self convertPoint:[theEvent locationInWindow] fromView:nil];

	mouse_x = location.x;
	mouse_y = location.y;

	SendGameMessage(MSG_MOUSE_MOVE, 0);
}

- (BOOL)isFlipped {
	return YES;
}

- (void)mouseMoved:(NSEvent *)theEvent
{
	location = [self convertPoint:[theEvent locationInWindow] fromView:nil];
	mouse_x = location.x;
	mouse_y = location.y;
}

- (void)scrollWheel:(NSEvent *)theEvent
{
	float wheelDelta = [theEvent deltaX] +[theEvent deltaY] + [theEvent deltaZ];
}

- (void)rightMouseDragged:(NSEvent *)theEvent
{
	[self mouseDragged: theEvent];
}

- (void)otherMouseDragged:(NSEvent *)theEvent
{
	[self mouseDragged: theEvent];
}

- (void) drawRect:(NSRect)rect
{
	if (RenderGameFrame())
	{
		[[self openGLContext] flushBuffer];
	}
}

-(id) initWithFrame: (NSRect) frameRect
{
	NSOpenGLPixelFormat * pf = [BasicOpenGLView basicPixelFormat];

	self = [super initWithFrame: frameRect pixelFormat: pf];

    return self;
}

- (BOOL)acceptsFirstResponder
{
  return YES;
}

- (BOOL)becomeFirstResponder
{
  return  YES;
}

- (BOOL)resignFirstResponder
{
  return YES;
}

- (void)myThreadMainMethod
{
	RunGame();
	
	[[NSApplication sharedApplication] terminate:nil];
}

/* Set the working directory to the .app's parent directory */
- (void) setupWorkingDirectory
{
    //setting the working directoy to MyApp.app/Contents/Resources/
    NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
    [[NSFileManager defaultManager] changeCurrentDirectoryPath:resourcePath];
	NSArray* paths = NSSearchPathForDirectoriesInDomains( NSApplicationSupportDirectory, NSUserDomainMask, YES );
	NSString *applicationSupportDirectory = [paths objectAtIndex:0];
	NSString *folder = @"CorePunch Game Studio/Among The Heavens";
	NSString *folderPath = [applicationSupportDirectory stringByAppendingPathComponent:folder];
	const char *c = [folderPath UTF8String];createDirectoryAtPath:withIntermediateDirectories:attributes:error:
	memset(documentsDirectoryC, 0, 512);
	strncpy(documentsDirectoryC, c, [folderPath length]);
	NSFileManager *fileMgr = [NSFileManager defaultManager];
	NSError *errorw;
	[fileMgr createDirectoryAtPath: folderPath withIntermediateDirectories:YES attributes:nil error:&errorw];
}

- (void) awakeFromNib
{
	//NSFileManager *filemgr = [[NSFileManager alloc] init];
	//NSString *currentpath = [filemgr currentDirectoryPath];

	[self setupWorkingDirectory];
	
	// set start values...
	fInfo = 1;
	fAnimate = 1;
	time = CFAbsoluteTimeGetCurrent ();  // set animation time start time
	fDrawHelp = 1;
	
	// start animation timer
	timer = [NSTimer timerWithTimeInterval:(1.0f/30.0f) target:self selector:@selector(animationTimer:) userInfo:nil repeats:YES];
	[[NSRunLoop currentRunLoop] addTimer:timer forMode:NSDefaultRunLoopMode];
	[[NSRunLoop currentRunLoop] addTimer:timer forMode:NSEventTrackingRunLoopMode]; // ensure timer fires during resize

	[NSThread detachNewThreadSelector:@selector(myThreadMainMethod) toTarget:self withObject:nil];
}


@end
