#pragma once

#include "math.hpp"

namespace math {

	struct Quaternion
	{
		union
		{
			Real v[4];

			struct
			{
				Real x, y, z, w;
			};
		};

		static const int Dimensions = 4;

		static const Quaternion Identity;
		static const Quaternion Zero;

		Quaternion();
		Quaternion(Real x, Real y, Real z, Real _w);
		Quaternion(Real yaw, Real pitch, Real roll);
		Quaternion(const Real* _v);
		Quaternion(const Quaternion& _v);
		Quaternion(const Matrix& m);
		Quaternion(const std::string& param);

		DEFINE_DEFAULT_VECTOR_OPS(Quaternion);

		void SetYaw(float value);
		float GetYaw() const;
		void SetPitch(float value);
		float GetPitch() const;
		void SetRoll(float value);
		float GetRoll() const;

		void Assign(Real x, Real y, Real z, Real w);

		static Quaternion RotationYawPitchRoll(Real yaw, Real pitch, Real roll);
		static Quaternion RotationMatrix(const Matrix& m);
	};

};
