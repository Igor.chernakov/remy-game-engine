#pragma once

//*.md3mesh file

struct StaticMesh {
	static const float scale;

	StaticMesh(const std::string &filename) : meshes(NULL) {
		const file::File file(filename.c_str());
		memcpy(&(header), file.mem, sizeof(header_t));
		meshes = new mesh_t[header.numMeshes];
		long mesh_offset = header.meshStart;
		for (int i = 0; i < header.numMeshes; i++) {
			memcpy(&meshes[i].header, file.mem + mesh_offset, sizeof(mesh_t::header_t));
			meshes[i].header.numIndices = meshes[i].header.numIndices * 3;
			meshes[i].skins = new mesh_t::skin_t[meshes[i].header.numSkins];
			meshes[i].pIndices = new int[meshes[i].header.numIndices];
			meshes[i].pNeighbours = new int[meshes[i].header.numIndices];
			meshes[i].pTexCoords = new math::Vector2[meshes[i].header.numVertices];
			meshes[i].pVertices = new mesh_t::vertex_t[meshes[i].header.numVertices * meshes[i].header.numMeshFrames];
			memcpy(meshes[i].skins, file.mem + mesh_offset + meshes[i].header.skinStart, sizeof(mesh_t::skin_t) * meshes[i].header.numSkins);
			memcpy(meshes[i].pTexCoords, file.mem + mesh_offset + meshes[i].header.uvStart, sizeof(math::Vector2) * meshes[i].header.numVertices);
			memcpy(meshes[i].pIndices, file.mem + mesh_offset + meshes[i].header.triStart, sizeof(int) * meshes[i].header.numIndices);
			memcpy(meshes[i].pVertices, file.mem + mesh_offset + meshes[i].header.vertexStart, sizeof(mesh_t::vertex_t) * meshes[i].header.numMeshFrames * meshes[i].header.numVertices);
			meshes[i].Calculate();
			mesh_offset += meshes[i].header.meshSize;
		}
		header.numFrames--;
	};

	~StaticMesh() {
		if (meshes) {
			for (int i = 0; i < header.numMeshes; i++) {
				delete [] meshes[i].skins;
				delete [] meshes[i].pIndices;
				delete [] meshes[i].pNeighbours;
				delete [] meshes[i].pTexCoords;
				delete [] meshes[i].pVertices;
			}
			delete [] meshes;
		};
	};

	struct header_t { 
		char	fileID[4];
		int		version;
		char	strFile[64];
		int 	nFlags;
		int		numFrames;
		int		numTags;
		int		numMeshes;
		int		numSkins;
		int		frameStart;
		int		tagStart;
		int		meshStart;
		int		fileSize;
	} header;

	struct mesh_t {
		struct header_t {
			char	meshID[4];
			char	strName[64];
			int 	nFlags;
			int		numMeshFrames;
			int		numSkins;
			int 	numVertices;
			int		numIndices;
			int		triStart;
			int		skinStart;
			int 	uvStart;
			int		vertexStart;
			int		meshSize;
		} header;

		struct skin_t {
			char strName[64];
			Material *shader;
		} *skins;

		int *pIndices;
		int *pNeighbours;

		math::Vector2 *pTexCoords;

		struct vertex_t {
			bool operator == (const vertex_t& b) {
				return 
					vertex[0] == b.vertex[0] &&
					vertex[1] == b.vertex[1] &&
					vertex[2] == b.vertex[2];
			};

			signed short vertex[3];
			unsigned char normal[2];
		} *pVertices;

		void Calculate() {
			for (int i = 0; i < header.numIndices / 3; i++) {
				int *pNeighbour = &pNeighbours[i * 3];

				pNeighbour[0] = pNeighbour[1] = pNeighbour[2] = -1;

				for (int j = 0; j < header.numIndices / 3; j++) {
					if (i == j)
						continue;

					mesh_t::vertex_t *p[3] = {
						&pVertices[pIndices[j * 3 + 0]],
						&pVertices[pIndices[j * 3 + 1]],
						&pVertices[pIndices[j * 3 + 2]]
					};

					for (int k = 0; k < 3; k++) {
						mesh_t::vertex_t *v[2] = {
							&pVertices[pIndices[i * 3 + k]],
							&pVertices[pIndices[i * 3 + (k + 1) % 3]]
						};

						if (*v[0] == *p[0] || *v[0] == *p[1] || *v[0] == *p[2])
							if (*v[1] == *p[0] || *v[1] == *p[1] || *v[1] == *p[2])
								pNeighbour[k] = j;
					}
				}
			}
		}
	} *meshes;

	bool MakeShadowVolume(Surface *pOutput, const math::Matrix *pMatrix) const {
		if (!pOutput || !pMatrix)
			return false;

		const math::Matrix m = pMatrix->Inverted();
		const math::Vector3 vLightDirection = m.MulNormal(-math::Vector3(World::Get()->pSun->vPosition.v)).normalized();

		const float fGround = fabs(vLightDirection.normalized().z);

		pOutput->nVertices = 0;

		for (const mesh_t *pMesh = meshes; pMesh - meshes < header.numMeshes; pMesh++)
			pOutput->nVertices += pMesh->header.numIndices * 6;

		pOutput->pVertices = new Surface::Vertex[pOutput->nVertices];
		pOutput->pIndices = new Surface::Index[pOutput->nVertices];

		pOutput->nVertices = 0;
		pOutput->nIndices = 0;

		struct stored_vertex_t {
			stored_vertex_t(int a, int b) 
				: nIndexID(a)
				, nStoredID(b) 
			{};

			int nIndexID;
			int nStoredID;
		};

		std::list<const stored_vertex_t> pStore;

		for (const mesh_t *pMesh = meshes; pMesh - meshes < header.numMeshes; pMesh++) {
			bool *pFacingness = new bool[pMesh->header.numIndices / 3];
			memset(pFacingness, 0, pMesh->header.numIndices / 3);

			for (int j = 0; j < pMesh->header.numIndices / 3; j++) {
				const int *tri = &pMesh->pIndices[j * 3];
				const math::Vector3 v[3] = {
					math::Vector3(pMesh->pVertices[tri[0]].vertex[0], pMesh->pVertices[tri[0]].vertex[1], pMesh->pVertices[tri[0]].vertex[2]),
					math::Vector3(pMesh->pVertices[tri[1]].vertex[0], pMesh->pVertices[tri[1]].vertex[1], pMesh->pVertices[tri[1]].vertex[2]),
					math::Vector3(pMesh->pVertices[tri[2]].vertex[0], pMesh->pVertices[tri[2]].vertex[1], pMesh->pVertices[tri[2]].vertex[2])
				};

				pFacingness[j] = (v[0] - v[1]).cross(v[0] - v[2]).dot(vLightDirection) > 0;
			}

			for (int j = 0; j < pMesh->header.numIndices / 3; j++) {
				if (!pFacingness[j])
					continue;

				int *tri = &pMesh->pIndices[j * 3];

				for (int k = 0; k < 3; k++) {
					if (pMesh->pNeighbours[j * 3 + k] != -1 && pFacingness[pMesh->pNeighbours[j * 3 + k]])
						continue;

					stored_vertex_t v[2] = {
						stored_vertex_t(tri[k], -1),
						stored_vertex_t(tri[(k+1)%3], -1),
					};

					const math::Vector3 a = 
						math::Vector3(
						pMesh->pVertices[v[0].nIndexID].vertex[0],
						pMesh->pVertices[v[0].nIndexID].vertex[1],
						pMesh->pVertices[v[0].nIndexID].vertex[2]
					) * scale * (*pMatrix);

					const math::Vector3 b = 
						math::Vector3(
						pMesh->pVertices[v[1].nIndexID].vertex[0],
						pMesh->pVertices[v[1].nIndexID].vertex[1],
						pMesh->pVertices[v[1].nIndexID].vertex[2]
					) * scale * (*pMatrix);

					const math::Vector3 c =
						b - math::Vector3(World::Get()->pSun->vPosition) * (b.z / fGround);

					const math::Vector3 d =
						a - math::Vector3(World::Get()->pSun->vPosition) * (a.z / fGround);

					for (std::list<const stored_vertex_t>::const_iterator it = pStore.begin(); it != pStore.end(); it++) {
						if (v[0].nIndexID == (*it).nIndexID)
							v[0].nStoredID = (*it).nStoredID;
						if (v[1].nIndexID == (*it).nIndexID)
							v[1].nStoredID = (*it).nStoredID;
					}

					if (v[0].nStoredID < 0) {
						v[0].nStoredID = pOutput->nVertices;
						pStore.push_back(v[0]);

						pOutput->pVertices[pOutput->nVertices++].vPosition = a;
						pOutput->pVertices[pOutput->nVertices++].vPosition = d;

					}

					if (v[1].nStoredID < 0) {
						v[1].nStoredID = pOutput->nVertices;
						pStore.push_back(v[1]);

						pOutput->pVertices[pOutput->nVertices++].vPosition = b;
						pOutput->pVertices[pOutput->nVertices++].vPosition = c;
					}

					pOutput->pIndices[pOutput->nIndices++] = v[0].nStoredID + 0;
					pOutput->pIndices[pOutput->nIndices++] = v[0].nStoredID + 1;
					pOutput->pIndices[pOutput->nIndices++] = v[1].nStoredID + 1;
					pOutput->pIndices[pOutput->nIndices++] = v[1].nStoredID + 1;
					pOutput->pIndices[pOutput->nIndices++] = v[1].nStoredID + 0;
					pOutput->pIndices[pOutput->nIndices++] = v[0].nStoredID + 0;
				}
			}
			delete [] pFacingness;
		}
		return true;
	};
};

const float StaticMesh::scale = 1.0f / 64.0f;

class MeshManager : 
	public ResourceManager<StaticMesh>,
	public Singleton<MeshManager>
{};
