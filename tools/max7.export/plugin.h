#pragma once

#include "max.h"
#include "plugapi.h"
#include "iparamb2.h"
#include "iparamm2.h"
#include "istdplug.h"
#include "decomp.h"
#include "modstack.h"
#include "iskin.h"

#include "resource.h"

#include "FileOutput.h"

#define STRINGLEN 32
#define SHADERNAMELEN 128

extern TCHAR *GetString(int id);

