#pragma once

#ifdef __APPLE__
#	include "TargetConditionals.h"
#endif

#if TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR
#	include <OpenGLES/ES2/gl.h>
#	include <OpenGLES/ES2/glext.h>
#else
#	include "glee.h"
#endif

#ifdef __linux
#   include "GL/glu.h"
#endif

#ifdef WIN32
#	pragma comment(lib, "opengl32.lib")
#	pragma comment(lib, "glu32.lib")
#endif

#include "image.hpp"

#include "../shared/types.hpp"
#include "../interfaces/irenderer.hpp"
#include "../interfaces/imaterial.hpp"
#include "../interfaces/iwindow.hpp"
#include "../interfaces/isurface.hpp"

void ScaleImage(GLenum format, INT inwidth, INT inheight, const BYTE *in, INT outwidth, INT outheight, BYTE *out);
void BuildMipmaps(GLenum target, INT width, INT height, GLenum format, const BYTE *data);

GLint __gluBuild2DMipMaps(GLint component, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *data);


// taken from mesa:			(TODO: use our custom functions)
GLint glScaleImage( GLenum format, GLint widthin, GLint heightin, GLenum typein, const void *datain, GLint widthout, GLint heightout, GLenum typeout, void *dataout );


GLint glBuild2DMipmaps (GLenum target, GLint component, GLsizei width, GLsizei height, GLenum format, GLenum type, const void *data);
