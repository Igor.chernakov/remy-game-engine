#pragma once

#include "../shared/shared.hpp"

#include "../interfaces/ifilesystem.hpp"

#include <list>

class FileSystem: public IFileSystem
{
public:
	FileSystem();

	virtual void AddPath(const std::string& path);
	virtual bool CheckFile(const std::string& filename) const;
	virtual file_t LoadFile(const std::string& path) const;
private:

	struct Pack
	{
		std::string name;

		struct PackedFile
		{
			std::string name;
			size_t size, offset;
		};

		std::list<PackedFile> files;
	};

	std::list<Pack> packets;
	std::list<std::string> search;
};
