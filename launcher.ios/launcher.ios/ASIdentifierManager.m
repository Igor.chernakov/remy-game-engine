#import "ASIdentifierManager.h"

@implementation ASIdentifierManager

- (id)init {
	return (self = [super init]);
}

+ (ASIdentifierManager *)sharedManager {
	static dispatch_once_t once;
	static ASIdentifierManager * sharedInstance;
	dispatch_once(&once, ^{
		sharedInstance = [[self alloc] init];
	});
	return sharedInstance;
}

- (NSUUID *)advertisingIdentifier {
	return [[UIDevice currentDevice] identifierForVendor];
}

- (BOOL)isAdvertisingTrackingEnabled {
	return FALSE;
}

@end
