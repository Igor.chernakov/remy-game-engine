#pragma once

#include "iobject.hpp"

struct ICamera: IObject
{
	enum Projection {
		PROJ_PERSPECTIVE = 0,
		PROJ_ORTHOGONAL
	};

	enum Mode {
		MODE_NONE,
		MODE_SCENE,
		MODE_INTERFACE,
		MODE_RAW_SCENE,
		MODE_RAW_INTERFACE,
	};

	enum ClipFlags {
		CLIP_NONE = 0,
		CLIP_SCISSOR = 1 << 0,
		CLIP_OFFSET = 1 << 1,
	};

	virtual void LookAt(const vec3_t &eye, const vec3_t &target, const vec3_t &yaw = vec3_t(0,0,1)) = 0;
	virtual void SetFov(float fov) = 0;
	virtual void SetMode(Mode mode, struct IRenderer*, int clip_flags = CLIP_SCISSOR | CLIP_OFFSET) = 0;
	virtual Mode GetMode() const = 0;
	virtual vec2_t GetScreenCoords(const vec3_t& point) const = 0;
	virtual vec3_t GetEye() const = 0;
	virtual vec3_t GetDirection() const = 0;
	virtual vec3_t GetYaw() const = 0;
	virtual float GetFov() const = 0;
	virtual float GetZFar() const = 0;
	virtual float GetZNear() const = 0;
	virtual math::Line GetMouseRay() const = 0;
	virtual math::Line GetRay(const point_t& point) const = 0;
	virtual int GetWidth() const = 0;
	virtual int GetHeight() const = 0;
	virtual quat_t GetOrientation() const = 0;
	virtual bool BoxInFrustum(const box3_t& box, const mat4_t& matrix) const = 0;
	virtual float GetPixelSize(const vec3_t& origin) const = 0;
	virtual Projection GetProjection() const = 0;
	virtual void SetProjection(Projection mode) = 0;
	virtual void Update() = 0;
	virtual float GetDistance(const vec3_t& position) const = 0;
	virtual const point_t& GetOffset() const = 0;
	virtual const rect_t& GetViewport() const = 0;
};