/**
	This file exports common WIN32 components into Python (used to make fully-scripted editor)
	See summary in the bottom
**/

#include "c_client.hpp"
#include "c_event.hpp"

#include <boost/python/raw_function.hpp>
#include <boost/python/stl_iterator.hpp>

#include  <commctrl.h>

#pragma comment(lib, "comctl32.lib")

extern hash_t BuildHash(const char *_str);

namespace py = boost::python;

namespace client { namespace system {
	class Widget: public boost::enable_shared_from_this<Widget>, public IObject
	{
		static int Counter;
	protected:
		Widget(const std::string& _class, int style = 0)
			: m_text("")
			, m_position(point_t(100, 100))
			, m_size(dim_t(100, 100))
			, m_handle(0)
			, m_disposed(false)
			, m_uid(Counter++)
			, m_class(_class)
			, m_style(style)
			, m_enabled(true)
		{
			SetName("SysWidget#");
		}

		~Widget()
		{
			if (m_handle)
			{
				DestroyWindow(m_handle);
			}
		}

		virtual void Spawn(HWND parent)
		{
			m_handle = CreateWindowA(
				m_class.c_str(),
				0,//m_text.c_str(),
				WS_CHILD | WS_VISIBLE | m_style,
				GetPosition().x, GetPosition().y,
				GetSize().width, GetSize().height,
				m_parent = parent,
				(HMENU)GetUID(),
				NULL,
				0);
		}

	public:
		void SetName(const std::string& value)
		{
			boost::algorithm::replace_first(m_name = value, "#", boost::lexical_cast<std::string>(m_uid));
			m_namehash = BuildHash(m_name.c_str());
		}

		const std::string& GetName() const 
		{
			return m_name;
		}

		virtual bool PostInitialize()
		{
			SetWindowLongPtr(m_handle, GWL_USERDATA, (LONG_PTR)this);
			ShowWindow(m_handle, SW_NORMAL);
			SendMessage(m_handle, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), TRUE);

			SetText(m_text);
			SetEnabled(m_enabled);

			return true;
		}

		shared_ptr<Widget> Append(shared_ptr<Widget> child)
		{
			m_widgets.push_back(child);
			return child;
		}

		static boost::python::object Attach(boost::python::tuple args, boost::python::dict kwargs)
		{
			using namespace boost::python;

			object self = args[0], entity = args[1]();

			if (extract<shared_ptr<Widget> >(entity).check())
			{
				extract<shared_ptr<Widget> >(self)()->Append(extract<shared_ptr<Widget> >(entity)());
			}
			else
			{
				framework->Log(IFramework::MSG_ERROR,"Can't create an unsupported class instance");
				return object();
			}

			struct set_attr
			{
				object self;
				object entity;
				dict kwargs;
				set_attr(object self, object entity, dict kwargs): self(self), entity(entity), kwargs(kwargs) {}
				void operator()(object key)
				{
					entity.attr(key) = kwargs[key];
				}
			};

			std::for_each(
				stl_input_iterator<object>(kwargs.keys()),
				stl_input_iterator<object>(),
				set_attr(self, entity, kwargs));

			return entity;
		}

		shared_ptr<Widget> FindByName(const std::string& name) const 
		{
			int hash = BuildHash(name.c_str());
			for (std::list<shared_ptr<Widget> >::const_iterator it = m_widgets.begin(); it != m_widgets.end(); ++it)
			{
				if ((*it)->m_namehash == hash)
					return *it;
			}
			return shared_ptr<Widget>();
		}

		Widget* FindByUID(int uid) const 
		{
			for (std::list<shared_ptr<Widget> >::const_iterator it = m_widgets.begin(); it != m_widgets.end(); ++it)
			{
				if ((*it)->GetUID() == uid)
					return it->get();
			}
			return 0;
		}

		Widget* FindByHandle(HWND hWnd) const 
		{
			for (std::list<shared_ptr<Widget> >::const_iterator it = m_widgets.begin(); it != m_widgets.end(); ++it)
			{
				if ((*it)->GetHandle() == hWnd)
					return it->get();
			}
			return 0;
		}

		virtual void OnCommand(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
		}

		HWND GetParentHandle() const
		{
			return m_parent;
		}

	public:
		const std::string& GetText() const
		{
			if (m_handle)
			{
				int len = GetWindowTextLength(m_handle) + 1;
				char* text = new char[len];
				GetWindowTextA(m_handle, text, len);
				m_text = text;
				delete[] text;
			}
			return m_text;
		}

		void SetText(const std::string& value)
		{
			m_text = value;
			SetWindowTextA(m_handle, m_text.c_str());
		}

		const point_t& GetPosition() const
		{
			return m_position;
		}
		void SetPosition(const point_t& value)
		{
			m_position = value;
			SetWindowPos(m_handle, NULL, m_position.x, m_position.y, m_size.width, m_size.height, 0);
		}

		const dim_t& GetSize() const
		{
			return m_size;
		}
		void SetSize(const dim_t& value)
		{
			m_size = value;
			SetWindowPos(m_handle, NULL, m_position.x, m_position.y, m_size.width, m_size.height, 0);
		}

		bool GetEnabled() const
		{
			return m_handle ? m_enabled = IsWindowEnabled(m_handle) : m_enabled;
		}
		void SetEnabled(bool value)
		{
			EnableWindow(m_handle, m_enabled = value);
		}

		HWND GetHandle() const { return m_handle; }
		int GetUID() const { return m_uid; }

		const std::list<shared_ptr<Widget> >& GetWidgets() { return m_widgets; }

		inline void Dispose() { Disposed(shared_from_this(), 0); m_disposed = true; }
		inline bool IsDisposed() const { return m_disposed; }

		client::Event Click, Disposed;

	private:
		mutable std::string m_text;
		mutable point_t m_position;
		mutable dim_t m_size;
		mutable bool m_enabled;
		bool m_disposed;
		std::list<shared_ptr<Widget> > m_widgets;
		const int m_uid;
		const std::string m_class;

		int m_namehash;
		std::string m_name;

	protected:
		HWND m_handle, m_parent;
		int m_style;
	};

	int Widget::Counter = 0;

	class Window: public Widget
	{
		static Window* CurrentWindow;

	public:
		Window(const std::string& text, const point_t& position, const dim_t& size)
			: Widget("MYWINDOW", WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_POPUP)
		{
			SetText(text);
			SetPosition(position);
			SetSize(size);
		}

		void Spawn(HWND) {}

		void Raise()
		{
			SetWindowPos(m_handle, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE|SWP_NOSIZE|SWP_NOACTIVATE);
		}

		void Show()
		{
			WNDCLASSEX wndClass = { 0 };

			wndClass.cbSize = sizeof(wndClass);
			wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
			wndClass.lpfnWndProc = Window::WndProc;
			wndClass.lpszClassName = L"MYWINDOW";
			wndClass.hbrBackground = (HBRUSH)(COLOR_WINDOW);

			RegisterClassEx(&wndClass);

			CurrentWindow = this;

			m_handle = CreateWindowExA(
				WS_EX_TOOLWINDOW,
				"MYWINDOW",
				GetText().c_str(),
				m_style,
				GetPosition().x,
				GetPosition().y,
				GetSize().width,
				GetSize().height,
				NULL,
				NULL,
				NULL,
				NULL);

			SetWindowLongPtr(m_handle, GWL_USERDATA, (LONG_PTR)this);
			ShowWindow(m_handle, SW_SHOWNOACTIVATE);
			SendMessage(m_handle, WM_SETFONT, (WPARAM)GetStockObject(DEFAULT_GUI_FONT), TRUE);

			std::for_each(GetWidgets().begin(), GetWidgets().end(), boost::bind(&Widget::PostInitialize, _1));
		}

		static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			Widget* window = (Widget*)GetWindowLongPtr(hWnd, GWL_USERDATA);
			switch(msg)
			{
			case WM_CREATE:
				std::for_each(CurrentWindow->GetWidgets().begin(), CurrentWindow->GetWidgets().end(), 
					boost::bind(&Widget::Spawn, _1, hWnd));
				return DefWindowProc(hWnd,msg,wParam,lParam);
			case WM_COMMAND:
				if (Widget* widget = window->FindByUID(LOWORD(wParam))) {
					widget->OnCommand(hWnd, msg, HIWORD(wParam), lParam);
				}
				return 0;
			case WM_HSCROLL:
				if (Widget* widget = window->FindByHandle((HWND)lParam)) {
					widget->OnCommand(hWnd, msg, LOWORD(wParam), lParam);
				}
				return 0;
			case WM_CLOSE:
				window->Dispose();
				return 0;
			default:
				return DefWindowProc(hWnd,msg,wParam,lParam);
			}
		}

		shared_ptr<Widget> GetAttr(const std::string& name) {
			if (shared_ptr<Widget> found = FindByName(name)) {
				return found;
			} else {
				PyErr_Format(PyExc_AttributeError, "'%s' object has no attribute or child object '%s'.", 
					GetName().c_str(), name.c_str());
				return shared_ptr<Widget>();
			}
		}
	};

	Window *Window::CurrentWindow = 0;

	class Label: public Widget {
	public:
		Label()
			: Widget("STATIC")
		{
		}
	};

	class Button: public Widget {
	public:
		Button()
			: Widget("BUTTON", BS_PUSHBUTTON)
		{
		}

		void OnCommand(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			Click(shared_from_this(), lParam);
		}
	};

	class TextBox: public Widget {
	public:
		TextBox()
			: Widget("EDIT", WS_BORDER)
		{
		}

		void OnCommand(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			switch (wParam)
			{
			case EN_UPDATE:
				TextChanged(shared_from_this(), GetText());
				break;
			}
		}

		Event TextChanged;
	};

	class ComboBox: public Widget {
	public:
		ComboBox()
			: Widget("COMBOBOX", CBS_DROPDOWNLIST | CBS_HASSTRINGS | WS_VSCROLL)
			, m_selectedindex(0)
		{
		}

		bool PostInitialize()
		{
			SetSelectedIndex(m_selectedindex);

			return Widget::PostInitialize();
		}

		void OnCommand(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			switch (wParam)
			{
			case CBN_SELCHANGE:
				SelectedIndexChanged(shared_from_this(), GetSelectedIndex());
				SelectedItemChanged(shared_from_this(), GetSelectedItem());
			}
		}

		bool GetSortEnabled() const { return m_style & CBS_SORT; }
		void SetSortEnabled(bool value) { m_style = value ? (m_style | CBS_SORT) : (m_style & ~CBS_SORT); }

		void Clear()
		{
			SendMessage(m_handle, CB_RESETCONTENT, 0, 0);
			m_items.clear();
		}

		void SetSelectedIndex(int value)
		{
			SendMessage(m_handle, CB_SETCURSEL, m_selectedindex = value, 0);
		}
		
		int GetSelectedIndex() const
		{
			return m_selectedindex = m_handle ? SendMessage(m_handle, CB_GETCURSEL, 0, 0) : m_selectedindex;
		}
		
		void AddItem(py::object item, int position = -1)
		{
			m_items.push_back(item);
			std::string s = py::extract<std::string>(item.attr("__str__")());
			SendMessageA(m_handle, CB_INSERTSTRING, position, reinterpret_cast<LPARAM>(s.c_str()));
		}
		py::object GetItem(int position) const
		{
			if (position >= 0 && position < m_items.size()) 
				return m_items[position];
			else
				return py::object();
		}

		void SetItem(py::object item, int position)
		{
			if (position >= 0 && position < m_items.size()) {
				m_items[position] = item;
				std::string s = py::extract<std::string>(item.attr("__str__")());
				SendMessageA(m_handle, CB_DELETESTRING, position, 0);
				SendMessageA(m_handle, CB_INSERTSTRING, position, reinterpret_cast<LPARAM>(s.c_str()));
				SetSelectedIndex(m_selectedindex);
			}
		}

		void RemoveItem(int position) 
		{
			if (position < m_items.size()) {
				m_items.erase(m_items.begin() + position);
				SendMessageA(m_handle, CB_DELETESTRING, position, 0);
			}
		}

		py::object GetSelectedItem() const
		{
			return GetItem(GetSelectedIndex());
		}

		void SetSelectedItem(py::object item)
		{
			return SetItem(item, GetSelectedIndex());
		}

		int GetItemsCount() const { return m_items.size(); }

		std::vector<py::object>::iterator items_begin() { return m_items.begin(); }
		std::vector<py::object>::iterator items_end() { return m_items.end(); }

		Event 
			SelectedIndexChanged,
			SelectedItemChanged;

	private:
		mutable int m_selectedindex;
		std::vector<py::object> m_items;
	};

	class ListBox: public Widget {
	public:
		ListBox()
			: Widget("LISTBOX", LBS_HASSTRINGS | LBS_NOTIFY | WS_VSCROLL | WS_BORDER | LBS_NOINTEGRALHEIGHT)
			, m_selectedindex(0)
		{
		}

		bool PostInitialize()
		{
			SetSelectedIndex(m_selectedindex);

			return Widget::PostInitialize();
		}

		void OnCommand(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			switch (wParam)
			{
			case LBN_SELCHANGE:
				SelectedIndexChanged(shared_from_this(), GetSelectedIndex());
				SelectedItemChanged(shared_from_this(), GetSelectedItem());
			}
		}

		bool GetSortEnabled() const { return m_style & LBS_SORT; }
		void SetSortEnabled(bool value) { m_style = value ? (m_style | LBS_SORT) : (m_style & ~LBS_SORT); }

		void Clear()
		{
			SendMessage(m_handle, LB_RESETCONTENT, 0, 0);
			m_items.clear();
		}

		void SetSelectedIndex(int value)
		{
			SendMessage(m_handle, LB_SETCURSEL, m_selectedindex = value, 0);
		}
		
		int GetSelectedIndex() const
		{
			return m_selectedindex = m_handle ? SendMessage(m_handle, LB_GETCURSEL, 0, 0) : m_selectedindex;
		}
		
		void AddItem(py::object item, int position = -1)
		{
			m_items.push_back(item);
			std::string s = py::extract<std::string>(item.attr("__str__")());
			SendMessageA(m_handle, LB_INSERTSTRING, position, reinterpret_cast<LPARAM>(s.c_str()));
		}

		py::object GetItem(int position) const
		{
			if (position >= 0 && position < m_items.size()) 
				return m_items[position];
			else
				return py::object();
		}

		void SetItem(py::object item, int position)
		{
			if (position >= 0 && position < m_items.size()) {
				m_items[position] = item;
				std::string s = py::extract<std::string>(item.attr("__str__")());
				SendMessageA(m_handle, LB_DELETESTRING, position, 0);
				SendMessageA(m_handle, LB_INSERTSTRING, position, reinterpret_cast<LPARAM>(s.c_str()));
				SetSelectedIndex(m_selectedindex);
			}
		}

		void RemoveItem(int position) 
		{
			if (position < m_items.size()) {
				m_items.erase(m_items.begin() + position);
				SendMessageA(m_handle, LB_DELETESTRING, position, 0);
			}
		}

		py::object GetSelectedItem() const
		{
			return GetItem(GetSelectedIndex());
		}

		void SetSelectedItem(py::object item)
		{
			return SetItem(item, GetSelectedIndex());
		}

		int GetItemsCount() const { return m_items.size(); }

		std::vector<py::object>::iterator items_begin() { return m_items.begin(); }
		std::vector<py::object>::iterator items_end() { return m_items.end(); }

		Event 
			SelectedIndexChanged,
			SelectedItemChanged;

	private:
		mutable int m_selectedindex;
		std::vector<py::object> m_items;
	};

	class CheckBox: public Widget {
	public:
		CheckBox()
			: Widget("BUTTON", BS_AUTOCHECKBOX)
			, m_checked(false)
		{
		}

		bool PostInitialize()
		{
			SetChecked(m_checked);

			return Widget::PostInitialize();
		}

		bool GetPushLike() const { return m_style & BS_PUSHLIKE; }
		void SetPushLike(bool value) { m_style = value ? (m_style | BS_PUSHLIKE) : (m_style & ~BS_PUSHLIKE); }

		bool GetChecked() const  { return m_checked = IsDlgButtonChecked(GetParentHandle(), GetUID()); }
		void SetChecked(bool value)  { CheckDlgButton(GetParentHandle(), GetUID(), m_checked = value); }

		void OnCommand(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			CheckStateChanged(shared_from_this(), GetChecked());
		}

		Event CheckStateChanged;

	private:
		mutable bool m_checked;
	};

	class RadioButton: public Widget {
	public:
		RadioButton()
			: Widget("BUTTON", BS_AUTORADIOBUTTON)
			, m_checked(false)
		{
		}

		bool PostInitialize()
		{
			SetChecked(m_checked);

			return Widget::PostInitialize();
		}

		bool GetPushLike() const { return m_style & BS_PUSHLIKE; }
		void SetPushLike(bool value) { m_style = value ? (m_style | BS_PUSHLIKE) : (m_style & ~BS_PUSHLIKE); }

		bool GetChecked() const  { return m_checked = IsDlgButtonChecked(GetParentHandle(), GetUID()); }
		void SetChecked(bool value)  { CheckDlgButton(GetParentHandle(), GetUID(), m_checked = value); }

		void OnCommand(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			Click(shared_from_this(), lParam);
		}

	private:
		mutable bool m_checked;
	};

	class GroupBox: public Widget {
	public:
		GroupBox()
			: Widget("BUTTON", BS_GROUPBOX)
		{
		}
	};

	class TrackBar: public Widget {
	public:
		TrackBar()
			: Widget(TRACKBAR_CLASSA, TBS_AUTOTICKS | TBS_FIXEDLENGTH)
			, m_value(0)
			, m_range(0, 100)
			, m_thumblength(20)
		{
		}

		bool PostInitialize()
		{
			SetThumbLength(m_thumblength);
			SetRange(m_range);
			SetSelRange(m_selrange);
			SetValue(m_value);

			return Widget::PostInitialize();
		}

		void OnCommand(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			if (msg == WM_HSCROLL)
			{
				switch(wParam)
				{
				case TB_THUMBTRACK:
				case TB_ENDTRACK:
					ValueChanged(shared_from_this(), GetValue());
					break;
				}
			}
		}

		bool GetSelectionEnabled() const { return m_style & TBS_ENABLESELRANGE; }
		void SetSelectionEnabled(bool value) { m_style = value ? (m_style | TBS_ENABLESELRANGE) : (m_style & ~TBS_ENABLESELRANGE); }

		int GetThumbLength() const 
		{
			return m_thumblength = m_handle ? SendMessage(m_handle, TBM_GETTHUMBLENGTH, 0, 0) : m_thumblength;
		}
		void SetThumbLength(int value)
		{
			SendMessage(m_handle, TBM_SETTHUMBLENGTH, m_thumblength = value, 0);
		}

		int GetValue() const { return m_value = m_handle ? SendMessage(m_handle, TBM_GETPOS, 0, 0) : m_value; }
		void SetValue(int value) { SendMessage(m_handle, TBM_SETPOS, TRUE, m_value = value); }

		const range_t& GetRange() const
		{
			m_range.min_ = m_handle ? SendMessage(m_handle, TBM_GETRANGEMIN, 0, 0) : m_range.min_;
			m_range.max_ = m_handle ? SendMessage(m_handle, TBM_GETRANGEMAX, 0, 0) : m_range.max_;
			return m_range;
		}
		void SetRange(const range_t& value) const
		{
			m_range = value;
			SendMessage(m_handle, TBM_SETRANGEMIN, 0, m_range.min_);
			SendMessage(m_handle, TBM_SETRANGEMAX, 0, m_range.max_);
		}

		const range_t& GetSelRange() const
		{
			m_selrange.min_ = m_handle ? SendMessage(m_handle, TBM_GETSELSTART, 0, 0) : m_selrange.min_;
			m_selrange.max_ = m_handle ? SendMessage(m_handle, TBM_GETSELEND, 0, 0) : m_selrange.max_;
			return m_range;
		}
		void SetSelRange(const range_t& value) const
		{
			m_selrange = value;
			SendMessage(m_handle, TBM_SETSELSTART, FALSE, m_selrange.min_);
			SendMessage(m_handle, TBM_SETSELEND, TRUE, m_selrange.max_);
		}

		Event ValueChanged;

	private:
		mutable int m_value, m_thumblength;
		mutable range_t m_range, m_selrange;
	};

	class ProgressBar: public Widget {
	public:
		ProgressBar()
			: Widget(PROGRESS_CLASSA, PBS_SMOOTH)
			, m_value(0)
			, m_range(0, 100)
		{
		}

		bool PostInitialize()
		{
			SetRange(m_range);
			SetValue(m_value);

			return Widget::PostInitialize();
		}

		void OnCommand(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
		}

		int GetValue() const { return m_value = m_handle ? SendMessage(m_handle, PBM_GETPOS, 0, 0) : m_value; }
		void SetValue(int value) { SendMessage(m_handle, PBM_SETPOS, m_value = value, 0); }

		void SetRange(const range_t& value) const
		{
			m_range = value;
			SendMessage(m_handle, PBM_SETRANGE, 0, MAKELPARAM(m_range.min_, m_range.max_));
		}
		const range_t& GetRange() const
		{
			m_range.min_ = m_handle ? SendMessage(m_handle, PBM_GETRANGE, FALSE, 0) : m_range.min_;
			m_range.max_ = m_handle ? SendMessage(m_handle, PBM_GETRANGE, TRUE, 0) : m_range.max_;
			return m_range;
		}

	private:
		mutable int m_value;
		mutable range_t m_range;
	};

}}

void export_SysWidgets()
{
	using namespace client::system;

	DISPOSEME(3) py::class_<Widget, shared_ptr<Widget>, boost::noncopyable>("Widget", py::no_init)
		.def("Attach", raw_function(&Widget::Attach, 2))
		//.def("Append", &Widget::Append)
		.add_property("id", py_cref(&Widget::GetName), &Widget::SetName)
		.add_property("text", py_cref(&Widget::GetText), &Widget::SetText)
		.add_property("position", py_cref(&Widget::GetPosition), &Widget::SetPosition)
		.add_property("size", py_cref(&Widget::GetSize), &Widget::SetSize)
		.add_property("enabled", &Widget::GetEnabled, &Widget::SetEnabled)
		.add_property("disposed", &Widget::IsDisposed)
		.def_readwrite("Click", &Widget::Click)
		;

	DISPOSEME(3) py::class_<Window, shared_ptr<Window>, py::bases<Widget>, boost::noncopyable>("Window", py::no_init)
		.def(py::init<std::string, point_t, dim_t>((
			py::arg("text"),
			py::arg("position"),
			py::arg("size"))))
		.def("__getattr__", &Window::GetAttr)
		.def("Show", &Window::Show)
		.def("Raise", &Window::Raise)
		.def("Dispose", &Window::Dispose)
		.def_readwrite("Disposed", &Window::Disposed)
		;

	DISPOSEME(3) py::class_<Label, shared_ptr<Label>, py::bases<Widget>, boost::noncopyable>("Label")
		;

	DISPOSEME(3) py::class_<GroupBox, shared_ptr<GroupBox>, py::bases<Widget>, boost::noncopyable>("GroupBox")
		;

	DISPOSEME(3) py::class_<TextBox, shared_ptr<TextBox>, py::bases<Widget>, boost::noncopyable>("TextBox")
		.def_readwrite("TextChanged", &TextBox::TextChanged)
		;

	DISPOSEME(3) py::class_<Button, shared_ptr<Button>, py::bases<Widget>, boost::noncopyable>("Button")
		;

	DISPOSEME(3) py::class_<ComboBox, shared_ptr<ComboBox>, py::bases<Widget>, boost::noncopyable>("ComboBox")
		.def("AddItem", &ComboBox::AddItem, (py::arg("item"), py::arg("position") = -1))
		.def("Clear", &ComboBox::Clear)
		.def("__getitem__", &ComboBox::GetItem)
		.def("__setitem__", &ComboBox::SetItem)
		.def("__len__", &ComboBox::GetItemsCount)
		.def("Clear", &ComboBox::Clear)
		.add_property("sort", &ComboBox::GetSortEnabled, &ComboBox::SetSortEnabled)
		.add_property("items", py::range(&ComboBox::items_begin, &ComboBox::items_end))
		.add_property("selectedindex", &ComboBox::GetSelectedIndex, &ComboBox::SetSelectedIndex)
		.add_property("selecteditem", &ComboBox::GetSelectedItem, &ListBox::SetSelectedItem)
		.def_readwrite("SelectedIndexChanged", &ComboBox::SelectedIndexChanged)
		.def_readwrite("SelectedItemChanged", &ComboBox::SelectedItemChanged)
		;

	DISPOSEME(3) py::class_<ListBox, shared_ptr<ListBox>, py::bases<Widget>, boost::noncopyable>("ListBox")
		.def("AddItem", &ListBox::AddItem, (py::arg("item"), py::arg("position") = -1))
		.def("RemoveItem", &ListBox::RemoveItem)
		.def("__getitem__", &ListBox::GetItem)
		.def("__setitem__", &ListBox::SetItem)
		.def("__len__", &ListBox::GetItemsCount)
		.def("Clear", &ListBox::Clear)
		.add_property("sort", &ListBox::GetSortEnabled, &ListBox::SetSortEnabled)
		.add_property("selectedindex", &ListBox::GetSelectedIndex, &ListBox::SetSelectedIndex)
		.add_property("selecteditem", &ListBox::GetSelectedItem, &ListBox::SetSelectedItem)
		.add_property("items", py::range(&ListBox::items_begin, &ListBox::items_end))
		.def_readwrite("SelectedIndexChanged", &ListBox::SelectedIndexChanged)
		.def_readwrite("SelectedItemChanged", &ListBox::SelectedItemChanged)
		;

	DISPOSEME(3) py::class_<RadioButton, shared_ptr<RadioButton>, py::bases<Widget>, boost::noncopyable>("RadioButton")
		.add_property("checked", &RadioButton::GetChecked, &RadioButton::SetChecked)
		.add_property("pushlike", &RadioButton::GetPushLike, &RadioButton::SetPushLike)
		;

	DISPOSEME(3) py::class_<CheckBox, shared_ptr<CheckBox>, py::bases<Widget>, boost::noncopyable>("CheckBox")
		.add_property("checked", &CheckBox::GetChecked, &CheckBox::SetChecked)
		.add_property("pushlike", &CheckBox::GetPushLike, &CheckBox::SetPushLike)
		.def_readwrite("CheckStateChanged", &CheckBox::CheckStateChanged)
		;

	DISPOSEME(3) py::class_<TrackBar, shared_ptr<TrackBar>, py::bases<Widget>, boost::noncopyable>("TrackBar")
		.add_property("value", &TrackBar::GetValue, &TrackBar::SetValue)
		.add_property("range", py_cref(&TrackBar::GetRange), &TrackBar::SetRange)
		.add_property("thumblength", &TrackBar::GetThumbLength, &TrackBar::SetThumbLength)
		.add_property("selectable", &TrackBar::GetSelectionEnabled, &TrackBar::SetSelectionEnabled)
		.add_property("selection", py_cref(&TrackBar::GetSelRange), &TrackBar::SetSelRange)
		.def_readwrite("ValueChanged", &TrackBar::ValueChanged)
		;

	DISPOSEME(3) py::class_<ProgressBar, shared_ptr<ProgressBar>, py::bases<Widget>, boost::noncopyable>("ProgressBar")
		.add_property("value", &ProgressBar::GetValue, &ProgressBar::SetValue)
		.add_property("range", py_cref(&ProgressBar::GetRange), &ProgressBar::SetRange)
		;

	DISPOSEME(3) py::objects::detail::demand_iterator_class("iterator", 
		(std::vector<py::object>::iterator*)0, py::objects::default_iterator_call_policies());

}