#pragma once

#define BOOST_PYTHON_STATIC_LIB

#include <boost/asio.hpp>
#include <boost/python.hpp>
#include <boost/optional.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/filesystem/operations.hpp>

namespace asio = boost::asio;

namespace boost
{
	namespace python
	{
		template<class T1, class T2>
		struct pair_to_tuple
		{
			static PyObject* convert(const std::pair<T1, T2>& p)
			{
				return incref(make_tuple(p.first, p.second).ptr());
			}
		};

		template<class T1, class T2>
		struct tuple_to_pair
		{
			tuple_to_pair()
			{
				converter::registry::push_back(&convertible, &construct, type_id<std::pair<T1, T2> >());
			}

			static void* convertible(PyObject* x)
			{
				return PyTuple_Check(x) ? x: 0;
			}

			static void construct(PyObject* x, converter::rvalue_from_python_stage1_data* data)
			{
				void* storage = ((converter::rvalue_from_python_storage<std::pair<T1, T2> >*)data)->storage.bytes;

				object o(borrowed(x));

				std::pair<T1, T2> p;

				p.first = extract<T1>(o[0]);

				p.second = extract<T2>(o[1]);

				new (storage) std::pair<T1, T2>(p);

				data->convertible = storage;
			}
		};

		template <typename T1, typename T2>
		struct pair_register
		{
			pair_register()
			{
				to_python_converter<std::pair<T1, T2>, pair_to_tuple<T1, T2> >();
				tuple_to_pair<T1, T2>();
			}
		};
	}
}

#include "../shared/shared.hpp"
#include "../math/math.hpp"

#include "../interfaces/iserver.hpp"
#include "../interfaces/iserverframework.hpp"

extern IServerFramework* framework;
