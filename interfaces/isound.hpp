#pragma once

#include "iobject.hpp"

struct ISound: IObject
{
	virtual void Play(float volume = 1.0f, bool loop = false) const = 0;
	virtual void Stop() const = 0;
	virtual void Pause() const = 0;
	virtual bool IsLoaded() const = 0;
	virtual void Enable(bool set) const = 0;
	virtual void SetVolume(float volume) const = 0;
	virtual float GetVolume() const = 0;
	virtual float GetLength() const = 0;
};