#pragma once

#include <boost/shared_ptr.hpp>

#include "ifilesystem.hpp"
#include "iresource.hpp"
#include "itimer.hpp"
#include "ivariables.hpp"
#include "ibucket.hpp"

#include "../shared/types.hpp"

typedef Manager<IResource> IResources;

struct IFramework
{
	virtual boost::shared_ptr<IFileSystem> GetFileSystem() const = 0;
	virtual boost::shared_ptr<IResources> GetResources() const = 0;
	virtual boost::shared_ptr<ITimer> GetTimer() const = 0;
	virtual boost::shared_ptr<IVariables> GetVariables() const = 0;
	virtual boost::shared_ptr<IBucket> GetBucket() const = 0;

	virtual void SetFileSystem(boost::shared_ptr<IFileSystem> ) = 0;
	virtual void SetResources(boost::shared_ptr<IResources> ) = 0;
	virtual void SetTimer(boost::shared_ptr<ITimer> ) = 0;
	virtual void SetVariables(boost::shared_ptr<IVariables> ) = 0;
	virtual void SetBucket(boost::shared_ptr<IBucket> ) = 0;

	enum msg_type_t { MSG_LOG, MSG_WARNING, MSG_ERROR };

	virtual void Log(msg_type_t mt, const char* message, ...) const = 0;
};
