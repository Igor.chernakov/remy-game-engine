#pragma once

#include "irendererobject.hpp"

struct IRendererBuffer: IRendererObject
{
	enum Type
	{
		UNKNOWN_BUFFER,
		VERTEX_BUFFER,
		INDEX_BUFFER,
	};

	Type type;

	unsigned int render_info;

	virtual void* GetDataPointer() const = 0;
	virtual size_t GetSize() const = 0;

	IRendererBuffer()
		: type(UNKNOWN_BUFFER) 
	{
	}
};