#include "math.hpp"

namespace math
{
	Line::Line(const Vector3& a, const Vector3& b): a(a), b(b)
	{
	}

	Line Line::Multiply(const Line& line, const Matrix& matrix)
	{
		return Line(line.a * matrix, line.b * matrix);
	}

	//this is a classical code below, i didn't modify it much :)
	bool Line::Intersects(const Line& line, const Triangle& triangle, Vector3* out)
	{
		// ��������� ���������� ����� ������� ������� � ���������� ������������.
		float r1 = triangle.n.Dot(line.a - triangle.a);
		float r2 = triangle.n.Dot(line.b - triangle.a);
		// ���� ��� ����� ������� ����� �� ���� ������� �� ���������, �� �������
		// �� ���������� �����������.
		if( math::sign(r1) == math::sign(r2) )
			return false;
		// ��������� ����� ����������� ������� � ���������� ������������.
		vec3_t pc = line.a + (line.a - line.b) * (r1 / (r2 - r1));
		// ���������, ��������� �� ����� ����������� ������ ������������.
		if( (triangle.n.Dot(vec3_t::Cross(triangle.b - triangle.a, pc - triangle.a))) < 0)
			return false;
		if( (triangle.n.Dot(vec3_t::Cross(triangle.c - triangle.b, pc - triangle.b))) < 0)
			return false;
		if( (triangle.n.Dot(vec3_t::Cross(triangle.a - triangle.c, pc - triangle.c))) < 0)
			return false;
		if (out)
			*out = pc;
		return true;
	}

	static int inline GetIntersection(float fDst1, float fDst2, const Vector3& P1, const Vector3& P2, Vector3* Hit)
	{
		if ( (fDst1 * fDst2) >= 0.0f)
			return 0;
		if ( fDst1 == fDst2)
			return 0; 
		*Hit = P1 + (P2-P1) * ( -fDst1/(fDst2-fDst1) );
		return 1;
	}

	static int inline InBox(const Vector3& Hit, const box3_t &box, const int Axis)
	{
		if ( Axis==0 && Hit.z > box.vMin.z && Hit.z < box.vMax.z && Hit.y > box.vMin.y && Hit.y < box.vMax.y)
			return 1;
		if ( Axis==1 && Hit.z > box.vMin.z && Hit.z < box.vMax.z && Hit.x > box.vMin.x && Hit.x < box.vMax.x)
			return 1;
		if ( Axis==2 && Hit.x > box.vMin.x && Hit.x < box.vMax.x && Hit.y > box.vMin.y && Hit.y < box.vMax.y)
			return 1;
		return 0;
	}

	// returns true if line (line.a, line.b) intersects with the box (B1, B2)
	// returns intersection point in Hit
	bool Line::Intersects(const Line& line, const Box3& box, Vector3* Hit)
	{
		float st, et, fst = 0, fet = 1;  
		float const *bmin = &box.vMin.x;  
		float const *bmax = &box.vMax.x;  
		float const *si = &line.a.x;  
		float const *ei = &line.b.x;  

		for (int i = 0; i < 3; i++) {  
			if (*si < *ei) {  
				if (*si > *bmax || *ei < *bmin)  
					return false;  
				float di = *ei - *si;  
				st = (*si < *bmin)? (*bmin - *si) / di: 0;  
				et = (*ei > *bmax)? (*bmax - *si) / di: 1;  
			}  
			else {  
				if (*ei > *bmax || *si < *bmin)  
					return false;  
				float di = *ei - *si;  
				st = (*si > *bmax)? (*bmax - *si) / di: 0;  
				et = (*ei < *bmin)? (*bmin - *si) / di: 1;  
			}  

			if (st > fst) fst = st;  
			if (et < fet) fet = et;  
			if (fet < fst)  
				return false;  
			bmin++; bmax++;  
			si++; ei++;  
		}  

		if (Hit)
			*Hit = line.a + (line.b - line.a) * fst; 
		return true;  
	} 

}