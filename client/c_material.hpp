#pragma once

#include "../interfaces/imaterial.hpp"

#include <vector>

namespace client
{
	class Material
		: public IMaterial
		, public IResource
	{
	public:
		Material();
		Material(const std::string& filename, shared_ptr<const ITexture> default_texture = null_ptr());
		virtual ~Material()
		{
			static int material_deallocations = 0;
			material_deallocations++;
		}

		const std::string& GetPath() const { return m_name; }
		int GetLayersCount() const { return m_layers.size(); }
		int GetWidth() const;
		int GetHeight() const;
		void ClearLayers() { m_layers.clear(); }
		const ILayer* GetLayer(int i) const { return m_layers[i].get(); }
		ILayer* GetLayer(int i) { return m_layers[i].get(); }
		ILayer* AddLayer();
		ILayer* AddLayer(const std::string& texture_filename);
		ILayer* AddLayer(shared_ptr<const ITexture> texture);
		virtual const IShader* GetShader() const { return m_shader.get(); }
		bool DuplicateLayer(int i);

		shared_ptr<const IShader> m_shader;
	protected:
		bool AddTextureToLayer(ILayer* layer, const std::string& filename, shared_ptr<const ITexture> texture = null_ptr()) const;
		void Read(const std::string& filename, shared_ptr<const ITexture> default_texture);
		void Read(const tinyxml2::XMLElement *node, shared_ptr<const ITexture> default_texture);
		void Reset();

	protected:
		std::vector<shared_ptr<ILayer> > m_layers;

		shared_ptr<const ITexture> (*textureloader)(const std::string&);
	};

}