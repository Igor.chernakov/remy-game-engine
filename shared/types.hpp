#pragma once

#include "macros.hpp"

#include <numeric>

#ifdef WIN32
#	include <windows.h>
#	include <vld.h>
#elif defined __APPLE__
#   include "TargetConditionals.h"
#   ifdef __OBJC__
#       import <Cocoa/Cocoa.h>
#   endif
#else
#	include <X11/Xlib.h>
#endif

#ifndef WIN32
#	define stricmp strcasecmp
#	define MAX_PATH 256
#	define Sleep(x) usleep(x * 1000)
#endif

//Boost includes

#include <boost/algorithm/string.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <vector>

namespace boost
{
	struct null_ptr
	{
		template<typename T>
		operator shared_ptr<T>()  const
		{
			return shared_ptr<T>();
		}
	};
}

#undef tolower
#undef toupper

typedef unsigned char BYTE;
typedef unsigned short WORD;
typedef unsigned long DWORD;
typedef	signed char SBYTE;
typedef signed short SWORD;
typedef signed int INT;
typedef float FLOAT;
typedef double DOUBLE;

using boost::shared_ptr;
using boost::weak_ptr;
using boost::null_ptr;
using boost::dynamic_pointer_cast;
using boost::static_pointer_cast;
using boost::const_pointer_cast;

template <typename T>
struct deleter_t
{
	void operator() (T* ptr) const
	{
		if (ptr)
			delete ptr;
	}
};

template <typename T>
struct push_back_list_t
{
	std::list<T>& atoms;
	push_back_list_t(std::list<T>& atoms): atoms(atoms) {}
	void operator()(const T& add_this) {
		atoms.push_back(add_this);
	}
};

template <typename T>
struct push_back_vector_t
{
	std::vector<T>& atoms;
	push_back_vector_t(std::vector<T>& atoms): atoms(atoms) {}
	void operator()(const T& add_this) {
		atoms.push_back(add_this);
	}
};

struct point_t
{
	static const int Dimensions = 2;

	point_t() : x(0), y(0)
	{
	}

	point_t(int _x, int _y) : x(_x), y(_y)
	{
	}

	point_t(const std::string& value) : x(0), y(0)
	{
		sscanf(value.c_str(), "%d,%d", &x, &y);
	}

#ifdef WIN32
	point_t(POINT p) : x(p.x), y(p.y)
	{
	}

	operator POINT() const
	{
		POINT p = { x, y };
		return p;
	}
#endif

	bool operator== (const point_t& other) const
	{
		return x == other.x && y == other.y;
	}

	union
	{
		int v[2];

		struct
		{
			int x, y;
		};
	};
};

struct dim_t
{
	static const int Dimensions = 2;

	dim_t() : width(0), height(0)
	{
	}

	dim_t(int _width, int _height) : width(_width), height(_height)
	{
	}

	dim_t(const std::string& value) : width(0), height(0)
	{
		sscanf(value.c_str(), "%d,%d", &width, &height);
	}

	union
	{
		int v[2];

		struct
		{
			int width, height;
		};
	};
};

struct range_t
{
	static const int Dimensions = 2;

	range_t() : min_(0), max_(0)
	{
	}

	range_t(int min__, int max__) : min_(min__), max_(max__)
	{
	}

	range_t(const std::string& value) : min_(0), max_(0)
	{
		sscanf(value.c_str(), "%d,%d", &min_, &max_);
	}

	union
	{
		int v[2];

		struct
		{
			int min_, max_;
		};
	};
};

struct rect_t
{
	static const int Dimensions = 4;

	rect_t() : left(0), top(0), right(0), bottom(0)
	{
	}

	rect_t(int _left, int _top, int _right, int _bottom) : left(_left), top(_top), right(_right), bottom(_bottom)
	{
	}

	rect_t(const std::string& value) : left(0), top(0), right(0), bottom(0)
	{
		sscanf(value.c_str(), "%d,%d,%d,%d", &left, &top, &right, &bottom);
	}

#ifdef WIN32

	rect_t(RECT r) : left(r.left), right(r.right), top(r.top), bottom(r.bottom)
	{
	}

	operator RECT() const
	{
		RECT r;
		r.left = left, r.right = right, r.top = top, r.bottom = bottom;
		return r;
	}

#endif

	inline bool Contains(const point_t& point) const
	{
		return point.x >= left && point.y >= top && point.x < right && point.y < bottom;
	}

	inline dim_t GetSize() const
	{
		return dim_t(right - left, bottom - top);
	}

	inline void SetSize(const dim_t& size)
	{
		bottom = top + size.height;
		right = left + size.width;
	}

	inline point_t GetPosition() const
	{
		return point_t(left, top);
	}

	inline void SetPosition(const point_t& position)
	{
		dim_t size = GetSize();

		left = position.x;
		right = position.x;

		SetSize(size);
	}

	union
	{
		int v[4];

		struct
		{
			int left, top, right, bottom;
		};
	};
};

template <typename T>
class stat_t {
	static const int HISTORY_SIZE = 64;
	T values[HISTORY_SIZE];
	int curent;
public:
	stat_t(): curent(0) {
		Reset();
	}
	T GetAverage() const {
		return std::accumulate(values, values + HISTORY_SIZE, 0) / HISTORY_SIZE;
	}
	void Append(const T& value) {
		values[(curent = (curent++ % HISTORY_SIZE))] = value;
	}
	void Reset() {
		memset(values, sizeof(T) * HISTORY_SIZE, 0);
	}
};

/**
	TREE CLASS used as base for several classes with std::for_each compatibility
**/

#include <vector>
#include <iterator>
#include <list>

template <
	typename _Tx,
	typename _NodeT = shared_ptr<_Tx>,
	typename _NodesT = std::list<_NodeT> >
class Tree: public boost::enable_shared_from_this<_Tx>
{
	friend class iterator;

public:
	typedef _Tx* Parent;
	typedef _NodeT Node;
	typedef _NodesT Nodes;

protected:
	Nodes _nodes;
	Parent _parent;

protected:
	Tree()
		: _parent(0)
	{
	}

	~Tree()
	{
		std::for_each(_nodes.begin(), _nodes.end(), boost::bind(&Tree::SetParent, _1, Parent(0)));
	}

public:
	virtual void Attach(Node __child)
	{
		if (__child)
		{
			__child->Detach();
			__child->SetParent((Parent)this);
			_nodes.push_back(__child);
		}
	}

	virtual bool Detach()
	{
		if (Parent p = GetParent())
		{
			if (std::find(p->_nodes.begin(), p->_nodes.end(), shared_from_this()) == p->_nodes.end())
			{
				return false;
			}
			p->_nodes.erase(std::find(p->_nodes.begin(), p->_nodes.end(), shared_from_this()));
			SetParent(0);
			return true;
		}

		return false;
	}

	virtual Parent GetParent() const
	{
		return _parent;
	}

	virtual Node GetSharedParent() const
	{
		return GetParent() ? GetParent()->shared_from_this() : null_ptr();
	}

	const Nodes& GetLinked() const
	{
		return _nodes;
	}

	Nodes& GetLinked()
	{
		return _nodes;
	}

	const Parent GetRoot() const
	{
		return GetParent() ? GetParent()->GetRoot() : (const Parent)this;
	}

	Parent GetRoot()
	{
		return GetParent() ? GetParent()->GetRoot() : (Parent)this;
	}

	virtual boost::shared_ptr<_Tx> shared_from_this()
	{
		return boost::enable_shared_from_this<_Tx>::shared_from_this();
	}

	virtual boost::shared_ptr<const _Tx> shared_from_this() const
	{
		return boost::enable_shared_from_this<_Tx>::shared_from_this();
	}

protected:
	virtual void SetParent(Parent __p)
	{
		_parent = __p;
	}

private:
	template <typename _Iter, typename _Cnt>
	struct _Layer
	{
		_Layer()
			: _list(0)
		{
		}

		_Layer(_Cnt* _list)
			: _list(_list)
			, _current(_list->begin())
			, _end(_list->end())
		{
		}

		_Layer(_Cnt* _list, const _Iter& _begin, const _Iter& _end)
			: _list(_list)
			, _current(_begin)
			, _end(_end)
		{
		}

		template<typename A, typename B>
		_Layer(const _Layer<A, B>& left)
			: _list(left._list)
			, _current(left._current)
			, _end(left._end)
		{
		}

		_Cnt* _list;
		_Iter _current;
		_Iter _end;
	};

	template <typename _Iter, typename _Cnt, typename _Ty>
	class _Iterator
	{
		friend class Tree;

	public:
		typedef _Layer<_Iter, _Cnt> layer;
		typedef _Ty& reference;
		typedef _Ty* pointer;
		typedef _Ty value_type;
		typedef std::forward_iterator_tag iterator_category;
		typedef ptrdiff_t difference_type;

	protected:
		_Iterator()
		{
			stack.reserve(128);
		}

		_Iterator(_Cnt* _list)
		{
			stack.push_back(_list);
		}

		_Iterator(_Cnt* _list, const _Iter& _begin, const _Iter& _end)
		{
			stack.push_back(layer(_list, _begin, _end));
		}

	public:
		_Iterator(const _Iterator& _it)
		{
			stack.resize(_it.stack.size());
			std::copy(_it.stack.begin(), _it.stack.end(), this->stack.begin());
		}

	public:
		_Iterator& operator++ ()
		{
			if ((*stack.back()._current)->GetLinked().size() > 0)
			{
				stack.push_back(&(*stack.back()._current)->GetLinked());
			}
			else
			{
				while (*this && ++(stack.back()._current) == stack.back()._end)
				{
					stack.pop_back();
				}
			}

			return *this;
		}

		_Iterator& operator-- ()
		{
			throw std::runtime_error("_Iterator& Tree::_Iterator::operator-- () is not implemented.");
		}

		_Iterator operator++ (int)
		{
			_Iterator tmp = *this;
			++*this;
			return tmp;
		}

		_Iterator operator-- (int)
		{
			_Iterator tmp = *this;
			--*this;
			return tmp;
		}

		template<typename It_, typename Cn_, typename Tp_>
		bool operator== (const _Iterator<It_, Cn_, Tp_>& v) const
		{
			if (this->stack.size() > 0 && v.stack.size() > 0)
			{
				return this->stack.back()._list == v.stack.back()._list
					&& this->stack.back()._current == v.stack.back()._current;
			}
			else if (this->stack.size() == 0 && v.stack.size() == 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		template<typename It_, typename Cn_, typename Tp_>
		bool operator!= (const _Iterator<It_, Cn_, Tp_>& v) const
		{
			return !(*this == v);
		}

		reference operator* () const
		{
			return *stack.back()._current;
		}

		pointer operator-> () const
		{
			return &**this;
		}

	protected:
		operator bool() const
		{
			return stack.size() > 0;
		}

	protected:
		std::vector<layer> stack;
	};


public:
	typedef _Iterator<typename Nodes::iterator, Nodes, Node> iterator;
	typedef _Iterator<typename Nodes::const_iterator, const Nodes, const Node> const_iterator;

public:
	iterator begin()
	{
		if (GetLinked().size() > 0)
		{
			return iterator(&GetLinked());
		}
		else
		{
			return end();
		}
	}

	iterator end()
	{
		return iterator();
	}

	const_iterator begin() const
	{
		if (GetLinked().size() > 0)
		{
			return const_iterator(&GetLinked());
		}
		else
		{
			return end();
		}
	}

	const_iterator end() const
	{
		return const_iterator();
	}

	size_t size() const
	{
		int _size = 0;
		for (const_iterator it = begin(); it != end(); ++it, ++_size);
		return _size;
	}

	bool is_childless() const
	{
		return _nodes.size() == 0;
	}
};

template <typename _Tx>
class Manager
{
	typedef boost::shared_ptr<const _Tx> _Res;

public:
	template <typename AllocType>
	boost::shared_ptr<const AllocType> Load(const std::string& filename)
	{
		_Res& _slot = _Data[filename];

		if (!_slot)
		{
			_slot = boost::shared_ptr<AllocType>(new AllocType(filename));
		}

		return boost::static_pointer_cast<const AllocType>(_slot);
	}

	bool Remove(const std::string& filename)
	{
		if (!IsLoaded(filename))
		{
			return false;
		}
		_Data.erase(_Data.find(filename));
		return true;
	}

	bool Append(const std::string& filename, _Res res, bool replace_if_exists = false)
	{
		if (replace_if_exists)
		{
			Remove(filename);
		}

		if (IsLoaded(filename))
		{
			return false;
		}

		_Data[filename] = res;

		return true;
	}

	bool IsLoaded(const std::string& filename) const
	{
		return _Data.find(filename) != _Data.end();
	}

	void Clear()
	{
		_Data.clear();
	}

protected:
	typedef std::map<std::string, _Res> DataT;
	DataT _Data;
};

struct ignorecase_less: std::binary_function<std::string, std::string, bool>
{
	static bool ignorecase_less_char(char x, char y) {
		return toupper(x) < toupper(y);
	}

	bool operator() (const std::string& left, const std::string& right) const
	{
		return std::lexicographical_compare(left.begin(), left.end(), right.begin(), right.end(),
			ignorecase_less_char);
	}
};

struct file_t {
	file_t() : handle(0), size(0) {}
	void close() {
		if (handle) {
			fclose(handle);
			handle = 0;
		}
	}
	operator FILE*() const {
		return handle;
	}
	operator bool() const {
		return valid();
	}
	bool valid() const {
		return handle != NULL;
	}
	std::string name;
	FILE* handle;
	int size;
};
