//
//  launcher.h
//  core
//
//  Created by Igor Chernakov on 01/05/2013.
//  Copyright (c) 2013 Igor Chernakov. All rights reserved.
//

#ifndef __core__launcher__
#define __core__launcher__

enum my_message_t
{
	MSG_LBUTTON_DOWN,
	MSG_LBUTTON_UP,
	MSG_RBUTTON_DOWN,
	MSG_RBUTTON_UP,
	MSG_MBUTTON_DOWN,
	MSG_MBUTTON_UP,
	MSG_MOUSE_MOVE,
	MSG_KEY_DOWN,
	MSG_ACTIVATE,
	MSG_KEY_DOWN_FORMATTED,
};

#ifdef __cplusplus
#define CFUNCTION extern "C"
#else
#define CFUNCTION
#endif

CFUNCTION void RunGame();
CFUNCTION void SendGameMessage(enum my_message_t message, int param);
CFUNCTION int PopGameMessage(enum my_message_t* message, int* param);
CFUNCTION int RenderGameFrame();


extern int mouse_x, mouse_y;

#endif /* defined(__core__launcher__) */
