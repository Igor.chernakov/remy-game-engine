#pragma once

#include "vector3.hpp"
#include "triangle.hpp"
#include "box3.hpp"

namespace math
{
	struct Line
	{
		Vector3 a, b;

		Line() {}
		Line(const Vector3& a, const Vector3& b);

		static Line Multiply(const Line& line, const Matrix& matrix);
		static bool Intersects(const Line& line, const Triangle& triangle, Vector3* out = 0);
		static bool Intersects(const Line& line, const Box3& box, Vector3* out = 0);

		Line operator* (const Matrix& matrix) const { return Multiply(*this, matrix); }
		Line& operator*= (const Matrix& matrix) { return *this = Multiply(*this, matrix); }

		bool Intersects(const Triangle& triangle, Vector3* out = 0) const { return Intersects(*this, triangle, out); }
		bool Intersects(const Box3& box, Vector3* out = 0) const { return Intersects(*this, box, out); }
	};
}