#pragma once

#include "../shared/types.hpp"
#include "../interfaces/iresource.hpp"

namespace shared
{
	class Resource: public IResource
	{
	protected:
		Resource(const std::string& path): __Path(path)
		{
		}

	public:
		virtual const std::string& GetPath() const
		{
			return __Path;
		}

	private:
		const std::string __Path;
	};
}