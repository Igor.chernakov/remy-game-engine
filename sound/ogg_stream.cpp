#include "../interfaces/iclientframework.hpp"
#include "../shared/resource.hpp"

#include "ogg_stream.h"
namespace sound {
	extern IClientFramework* framework;
}

void ogg_stream::open(string path)
{
	int result;

	file_t file = sound::framework->GetFileSystem()->LoadFile(path);

	if (!file) {
		sound::framework->Log(IFramework::MSG_ERROR, "Cannot open %s for reading.", path.c_str());
		return;
	}

	oggFile = file;

	if((result = ov_open(oggFile, &oggStream, NULL, 0)) < 0) {
		fclose(oggFile);
		oggFile = NULL;
	}

	vorbisInfo = ov_info(&oggStream, -1);
	vorbisComment = ov_comment(&oggStream, -1);

	if(vorbisInfo->channels == 1)
		format = AL_FORMAT_MONO16;
	else
		format = AL_FORMAT_STEREO16;

	is_finished = false;

	alGenBuffers(2, buffers);
	check();
	alGenSources(1, &source);
	check();

	alSource3f(source, AL_POSITION,        0.0, 0.0, 0.0);
	alSource3f(source, AL_VELOCITY,        0.0, 0.0, 0.0);
	alSource3f(source, AL_DIRECTION,       0.0, 0.0, 0.0);
	alSourcef (source, AL_ROLLOFF_FACTOR,  0.0          );
	alSourcei (source, AL_SOURCE_RELATIVE, AL_TRUE      );
}




void ogg_stream::release()
{
	if (!oggFile)
		return;

	alSourceStop(source);
	empty();
	alDeleteSources(1, &source);
	check();
	alDeleteBuffers(2, buffers);
	check();

	ov_clear(&oggStream);
}




void ogg_stream::display()
{
	cout
		<< "version         " << vorbisInfo->version         << "\n"
		<< "channels        " << vorbisInfo->channels        << "\n"
		<< "rate (hz)       " << vorbisInfo->rate            << "\n"
		<< "bitrate upper   " << vorbisInfo->bitrate_upper   << "\n"
		<< "bitrate nominal " << vorbisInfo->bitrate_nominal << "\n"
		<< "bitrate lower   " << vorbisInfo->bitrate_lower   << "\n"
		<< "bitrate window  " << vorbisInfo->bitrate_window  << "\n"
		<< "\n"
		<< "vendor " << vorbisComment->vendor << "\n";

	for(int i = 0; i < vorbisComment->comments; i++)
		cout << "   " << vorbisComment->user_comments[i] << "\n";

	cout << endl;
}




bool ogg_stream::playback(float volume)
{
	if(playing())
		return true;

	if(!stream(buffers[0]))
		return false;

	if(!stream(buffers[1]))
		return false;

	ov_time_seek(&oggStream, 0.f);

	//alSourcei(source, AL_LOOPING, false);
	alSourcef(source, AL_GAIN, volume);
	alSourceQueueBuffers(source, 2, buffers);
	alSourcePlay(source);

	is_finished = false;

	return true;
}

void ogg_stream::rewind() {
	ov_raw_seek(&oggStream, 0.f);

	is_finished = false;
}


bool ogg_stream::playing()
{
	ALenum state;

	alGetSourcei(source, AL_SOURCE_STATE, &state);

	return (state == AL_PLAYING);
}


bool ogg_stream::finished() const
{
	return is_finished;
}

bool ogg_stream::update()
{
	int processed;
	bool active = true;

	alGetSourcei(source, AL_BUFFERS_PROCESSED, &processed);

	while(processed--)
	{
		ALuint buffer;

		alSourceUnqueueBuffers(source, 1, &buffer);
		check();

		if (active = stream(buffer))
		{
			alSourceQueueBuffers(source, 1, &buffer);
		}
		check();
	}

	return active;
}


static char*
ogg_error(int code)
{
	char string[256];

	switch(code)
	{
	case OV_EREAD:
		return "Ogg: Read from media.";
	case OV_ENOTVORBIS:
		return "Ogg: Not Vorbis data.";
	case OV_EVERSION:
		return "Ogg: Vorbis version mismatch.";
	case OV_EBADHEADER:
		return "Ogg: Invalid Vorbis header.";
	case OV_EFAULT:
		return "Ogg: Internal logic fault (bug or heap/stack corruption.";
	default:
		sprintf( string, "Ogg: Unknown Ogg error (%d).", code );
		//	__asm int 3;
		return string;
	}
}

bool ogg_stream::stream(ALuint buffer)
{
	char pcm[BUFFER_SIZE];
	int  size = 0;
	int  section;
	int  result;

	while(size < BUFFER_SIZE)
	{
		result = ov_read(&oggStream, pcm + size, BUFFER_SIZE - size, 0, 2, 1, &section);

		if (result==0)
			is_finished = true;

		if(result > 0)
			size += result;
		else
			if(result < 0)
				printf("Ogg error: %s\n", ogg_error(result));
			else
				break;
	}

	if(size == 0)
		return false;

	alBufferData(buffer, format, pcm, size, vorbisInfo->rate);
	check();

	return true;
}




void ogg_stream::empty()
{
	int queued;

	alGetSourcei(source, AL_BUFFERS_QUEUED, &queued);

	while(queued--)
	{
		ALuint buffer;

		alSourceUnqueueBuffers(source, 1, &buffer);
		check();
	}
}




void ogg_stream::check()
{
	int error = alGetError();

	//if(error != AL_NO_ERROR)
	//	throw string("OpenAL error was raised.");
}



string ogg_stream::errorString(int code)
{
	switch(code)
	{
	case OV_EREAD:
		return string("Read from media.");
	case OV_ENOTVORBIS:
		return string("Not Vorbis data.");
	case OV_EVERSION:
		return string("Vorbis version mismatch.");
	case OV_EBADHEADER:
		return string("Invalid Vorbis header.");
	case OV_EFAULT:
		return string("Internal logic fault (bug or heap/stack corruption.");
	default:
		return string("Unknown Ogg error.");
	}
}
