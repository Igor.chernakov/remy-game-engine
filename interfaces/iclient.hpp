#pragma once

#include "iobject.hpp"

struct IClient: IObject
{
	virtual bool Connect(const std::string& ip, int port) = 0;
	virtual void Run() = 0;
	virtual bool RenderFrame() = 0; //used for mulithreading
	virtual void Evaluate(const std::string& command) = 0;
	virtual void TakeScreenshot() = 0;
	virtual const char* CompileDate() const = 0;
	virtual void PublishMessage(const std::string& message, const color_t& color) = 0;
};
