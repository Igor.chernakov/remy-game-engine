#import "iosAppDelegate.h"
#import "iosGameCenter.h"
#import "../ga/GameAnalytics.h"

const int MAGIC_GA_NUMBER = 123456789;

void GA_addProgressionStartEvent(const char *progression) {
	NSString *_progression = [NSString stringWithUTF8String:progression];
	[GameAnalytics addProgressionEventWithProgressionStatus:GAProgressionStatusStart
											  progression01:_progression
											  progression02:NULL
											  progression03:NULL];
}

void GA_addProgressionCompleteEvent(const char *progression, int score) {
	NSString *_progression = [NSString stringWithUTF8String:progression];
	[GameAnalytics addProgressionEventWithProgressionStatus:GAProgressionStatusComplete
											  progression01:_progression
											  progression02:NULL
											  progression03:NULL
													  score:score];
}

void GA_addProgressionFailEvent(const char *progression, int score) {
	NSString *_progression = [NSString stringWithUTF8String:progression];
	[GameAnalytics addProgressionEventWithProgressionStatus:GAProgressionStatusFail
											  progression01:_progression
											  progression02:NULL
											  progression03:NULL
													  score:score];
}

void GA_addErrorEvent(const char *msg) {
	NSString *_message = [NSString stringWithUTF8String:msg];
	[GameAnalytics addErrorEventWithSeverity:GAErrorSeverityDebug message:_message];
}

void GA_addDesignEvent(const char *evt, int value) {
	NSString *_evt = [NSString stringWithUTF8String:evt];
	if (value == MAGIC_GA_NUMBER) {
		[GameAnalytics addDesignEventWithEventId:_evt];
	} else {
		NSNumber *num = [NSNumber numberWithInt:value];
		[GameAnalytics addDesignEventWithEventId:_evt value:num];
	}
}

@implementation iosAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	// Enable implementation log (disable in production)
	[GameAnalytics setEnabledInfoLog:YES];
	[GameAnalytics setEnabledVerboseLog:NO];
	// Set build version
	[GameAnalytics configureBuild:@"release 1.2.2"];
	// Set available virtual currencies and item types
	[GameAnalytics configureAvailableResourceCurrencies:@[@"gems", @"gold"]];
	[GameAnalytics configureAvailableResourceItemTypes:@[@"boost", @"lives"]];
	// Set available custom dimensions
	[GameAnalytics configureAvailableCustomDimensions01:@[@"ninja", @"samurai"]];
	[GameAnalytics configureAvailableCustomDimensions02:@[@"whale", @"dolphin"]];
	[GameAnalytics configureAvailableCustomDimensions03:@[@"horde", @"alliance"]];
    // Initialize
	[GameAnalytics initializeWithGameKey:@"c7e2a2d261ad8da380a54f898e2b0c96"
							  gameSecret:@"b27f823e90d6660e35d69c786db286f316f9c604"];
	//[[GameCenter sharedInstance] authenticateLocalUser];
	return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
