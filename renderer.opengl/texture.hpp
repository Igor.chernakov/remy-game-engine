#pragma once

#include "../interfaces/iclientframework.hpp"

extern IClientFramework* framework;

static unsigned pow2(unsigned int value)
{
	unsigned int r = 1;
	for (r; r < value; r <<= 1)
	{
	}
	return r;
}

class Texture: public ITexture, public IResource
{
public:
	Texture(int width, int height, Format format, Pool pool, Usage usage, int miplevels);
	Texture(const char* file, int size, Format format, Pool pool, Usage usage, int miplevels);
	Texture(const Path& filename, Format format = FMT_UNKNOWN, Pool pool = D3DPOOL_DEFAULT, Usage usage = USAGE_DEFAULT, int miplevels = 0);
	virtual ~Texture();
	virtual const Path& GetPath() const { return m_name; }
	virtual int GetWidth() const { return m_width; }
	virtual int GetHeight() const { return m_height; }
	virtual int GetPicMip() const { return m_picmip; }
	virtual int GetMipLevels() const { return m_miplevels; }
	virtual Usage GetUsage() const { return m_usage; }
	virtual Pool GetPool() const { return m_pool; }
	virtual Format GetFormat() const { return m_format; }
	virtual const void* GetRenderInfo() const { return m_renderer_info; }
	virtual void SetWidth(int value) { m_width = pow2(value); }
	virtual void SetHeight(int value) { m_height = pow2(value); }
	virtual void SetPicMip(int value) { m_picmip = value; }
	virtual void SetMipLevels(int value) { m_miplevels = value; }
	virtual void SetUsage(Usage value) { m_usage = value; }
	virtual void SetPool(Pool value) { m_pool = value; }
	virtual void SetFormat(Format value) { m_format = value; }
	virtual void SetRenderInfo(const void* value) { m_renderer_info = value; }
	virtual char* Lock() const { return 0; }
	void Unlock() const { }

protected:
	Path m_name;
	shared_ptr<IRenderer> m_renderer;
	const void* m_renderer_info;
	int m_width, m_height, m_picmip, m_miplevels;
	Usage m_usage;
	Pool m_pool;
	Format m_format;
};

