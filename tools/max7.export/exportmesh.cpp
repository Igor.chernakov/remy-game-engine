#include "Plugin.h"
#include "stdmat.h"

#include "ExportMesh.h"

#include <stdarg.h>

#include <iostream>
#include <fstream>
#include <memory>
#include <list>
#include <vector>
#include <algorithm>

#define MIN(x, y) ((x>y)?(y):(x))
#define MAX(x, y) ((x<y)?(y):(x))

extern bool bExportColors;

TimeValue g_bindPoseTime = 0;

struct Color2
{
	static const int Identity = 0xFF;

	Color2() : color(0xFFFFFFFF)
	{
	}

	Color2(float _r, float _g, float _b, float _a = 1.0f)
	{
		SetRed(_r);
		SetGreen(_g);
		SetBlue(_b);
		SetAlpha(_a);
	}

	Color2(unsigned int color) : color(color)
	{
	}

	inline float GetRed() const { return v[2] / float(Identity); }
	inline float GetGreen() const { return v[1] / float(Identity); }
	inline float GetBlue() const { return v[0] / float(Identity); }
	inline float GetAlpha() const { return v[3] / float(Identity); }
	inline void SetRed(float value) { v[2] = unsigned char(MIN(1.0f, MAX(0.0f, value)) * Identity); }
	inline void SetGreen(float value) { v[1] = unsigned char(MIN(1.0f, MAX(0.0f, value)) * Identity); }
	inline void SetBlue(float value) { v[0] = unsigned char(MIN(1.0f, MAX(0.0f, value)) * Identity); }
	inline void SetAlpha(float value) { v[3] = unsigned char(MIN(1.0f, MAX(0.0f, value)) * Identity); }

	inline Color2 operator* (float k) const
	{
		return *this * Color2(k, k, k);
	}

	inline Color2 operator* (const Color2& right) const
	{
		Color2 out;

		out.a = (unsigned char)((int)a * (int)right.a / Identity);
		out.r = (unsigned char)((int)r * (int)right.r / Identity);
		out.g = (unsigned char)((int)g * (int)right.g / Identity);
		out.b = (unsigned char)((int)b * (int)right.b / Identity);

		return out;
	}

	inline Color2 operator+ (const Color2& right) const
	{
		Color2 out;

		out.a = (unsigned char)MIN((int)a + (int)right.a, 255);
		out.r = (unsigned char)MIN((int)r + (int)right.r, 255);
		out.g = (unsigned char)MIN((int)g + (int)right.g, 255);
		out.b = (unsigned char)MIN((int)b + (int)right.b, 255);

		return out;
	}

	inline static Color2 Lerp(float d, const Color2& left, const Color2& right)
	{
		Color2 result;
		result.SetRed(left.GetRed() * (1 - d) + right.GetRed() * d);
		result.SetGreen(left.GetGreen() * (1 - d) + right.GetGreen() * d);
		result.SetBlue(left.GetBlue() * (1 - d) + right.GetBlue() * d);
		result.SetAlpha(left.GetAlpha() * (1 - d) + right.GetAlpha() * d);
		return result;
	}

	inline bool operator== (const Color2& right) const
	{
		return color == right.color;
	}

	inline bool operator!= (const Color2& right) const
	{
		return color != right.color;
	}

	inline Color2& operator*= (const Color2& right)
	{
		return *this = *this * right;
	}

	union
	{
		int color;
		unsigned char v[4];
		struct { unsigned char b, g, r, a; };
	};
};


std::list<INode*> root_bones;

bool IsRootBone(const INode* node)
{
	return std::find(root_bones.begin(), root_bones.end(), node) != root_bones.end();
}

static const size_t MESHFORMAT_VERSION = 100;
static const float EPS = 0.001f;

extern bool bExportRotationSequence;
extern bool bExportPositionSequence;
extern bool bExportScaleSequence;
extern bool bOptimizeSequences;
extern bool bEmbedTextures;
extern bool bExportAnimations;
extern bool bMoveToZero;

extern TimeValue g_bindPoseTime;

std::vector<BitmapTex*> textures;

struct streamable_t
{
	virtual void Stream(FileOutput* file) const = 0;
};

struct bone_t : streamable_t
{
	bone_t(int id, int parent, INode* node)
	{
		this->id = id;
		this->parent = parent;
		this->node = node;
	}

	int id;
	int parent;

	INode* node;

	void Stream(FileOutput* file) const
	{
		if (file->mode == FileOutput::BINARY)
		{
			char name[STRINGLEN];
			
			memset(name, 0, STRINGLEN);

			strcpy_s(name, STRINGLEN, node->GetName());

			file->write(name, STRINGLEN);
			file->write((char*)&id, sizeof(id));
			file->write((char*)&parent, sizeof(parent));

			XmlMeshExport::StreamNodeProps(*file, (parent < 0 && bMoveToZero) ? 0 : node, g_bindPoseTime);
		}
		else
		{
			*file << "<Bone name=\"" << node->GetName() << "\" id=\"" << id << "\" parent=\"" << parent << "\"";

			XmlMeshExport::StreamNodeProps(*file, (parent < 0 && bMoveToZero) ? 0 : node, g_bindPoseTime);

			*file << "/>" << std::endl;
		}
	}

	bool operator== (const INode* node) const {
		return this->node == node;
	}
};

struct mesh_t : streamable_t
{
	mesh_t() {}

	struct vertex_t : streamable_t
	{
		Point3 position;
		Point3 normal;
		UVVert uv;
		DWORD smthg;
		DWORD color;

		size_t id;

		struct weight_t {
			size_t id;
			size_t count;
		};

		weight_t weight;

		void Stream(FileOutput* file) const
		{
			if (file->mode == FileOutput::BINARY)
			{
				file->write((char*)&uv, sizeof(float) * 2);
				file->write((char*)&weight, sizeof(weight_t));
			}
			else
			{
				*file << "<Vertex uv=\"" << uv.x << "," << uv.y << "\" weight=\"" << weight.id << "," << weight.count << "\" />" << std::endl;
			}
		}

		void StreamColor(FileOutput* file) const
		{
			if (file->mode == FileOutput::BINARY)
			{
				file->write((char*)&color, 4);
			}
			else
			{
				//*file << "<Vertex color=\"" << uv.x << "," << uv.y << "\" weight=\"" << weight.id << "," << weight.count << "\" />" << std::endl;
			}
		}

		

		bool operator== (const vertex_t& v) const
		{
			return (position == v.position) && (uv == v.uv) && ((smthg & v.smthg) > 0);
		}

		bool operator== (const Point3& p) const
		{
			return position == p;
		}
	};

	struct weight_t : streamable_t
	{
		weight_t() {}

		weight_t(Point3 position, Point3 normal, size_t bone, float weight)
		{
			this->bone = bone;
			this->weight = weight;
			this->position = position;
			this->normal = normal;
		}

		void Stream(FileOutput* file) const
		{
			if (file->mode == FileOutput::BINARY)
			{
				file->write((char*)&position, sizeof(Point3));
				file->write((char*)&normal, sizeof(Point3));
				file->write((char*)&bone, sizeof(size_t));
				file->write((char*)&weight, sizeof(float));
			}
			else
			{
				*file << "<Weight " <<
					"position=\"" << position[0] << "," << position[1] << "," << position[2] << "\" " <<
					"bone=\"" << bone << "\" " <<
					"weight=\"" << weight << "\" " <<
					"/>" << std::endl;
			}
		}

		Point3 position;
		Point3 normal;
		size_t bone;
		float weight;
	};

	struct face_t : streamable_t
	{
		face_t(size_t* v)
		{
			this->v[0] = (unsigned short)v[2];
			this->v[1] = (unsigned short)v[1];
			this->v[2] = (unsigned short)v[0];
		}

		void Stream(FileOutput* file) const
		{
			if (file->mode == FileOutput::BINARY)
			{
				file->write((char*)&v, sizeof(v));
			}
			else
			{
				*file << "<Face vertices=\"" << v[0] << "," << v[1] << "," << v[2] << "\" />" << std::endl;
			}
		}

		unsigned short v[3];
	};

	void Stream(FileOutput* file) const {

		if (file->mode == FileOutput::BINARY)
		{
			size_t numverts = vertices.size();
			size_t numfaces = faces.size();
			size_t numweights = weights.size();

			int flags = 0;

			if (strstr(name.data(), "shadow") != NULL) {
				flags = 1; //it's a shadow
			}

			file->PushTag("MESH");
			file->PushTag("NAME"); file->write(name.data(), name.Length()); file->PopTag();
			file->PushTag("FLAG"); file->write((char*)&flags, sizeof(flags)); file->PopTag();
			if (texture_id >= 0)
			{
				file->PushTag("TXID");
				file->write((char*)&texture_id, sizeof(texture_id));
				file->PopTag();
			}
			file->PushTag("VRTX");
			std::for_each(vertices.begin(), vertices.end(), std::bind2nd(std::mem_fun_ref(&mesh_t::vertex_t::Stream), file));
			file->PopTag();
			if (bExportColors) {
				file->PushTag("VCLR");
				std::for_each(vertices.begin(), vertices.end(), std::bind2nd(std::mem_fun_ref(&mesh_t::vertex_t::StreamColor), file));
				file->PopTag();
			}
			file->PushTag("FACE");
			std::for_each(faces.begin(), faces.end(), std::bind2nd(std::mem_fun_ref(&mesh_t::face_t::Stream), file));
			file->PopTag();
			file->PushTag("WGHT");
			std::for_each(weights.begin(), weights.end(), std::bind2nd(std::mem_fun_ref(&mesh_t::weight_t::Stream), file));
			file->PopTag();
			file->PopTag();
		}
		else
		{
			*file <<
				"<Mesh " <<
				"shader=\"" << texture_id << "\" " <<
				"numverts=\"" << vertices.size() << "\" " <<
				"numfaces=\"" << faces.size() << "\" " <<
				"numweights=\"" << weights.size() << "\" " <<
				">" << std::endl;

			std::for_each(vertices.begin(), vertices.end(), std::bind2nd(std::mem_fun_ref(&mesh_t::vertex_t::Stream), file));
			std::for_each(faces.begin(), faces.end(), std::bind2nd(std::mem_fun_ref(&mesh_t::face_t::Stream), file));
			std::for_each(weights.begin(), weights.end(), std::bind2nd(std::mem_fun_ref(&mesh_t::weight_t::Stream), file));

			*file << "</Mesh>" << std::endl;
		}
	}

	typedef std::vector<vertex_t> vertices_t;
	typedef std::vector<face_t> faces_t;
	typedef std::vector<weight_t> weights_t;

	vertices_t vertices;
	faces_t faces;
	weights_t weights;

	MtlID material;

	int texture_id;

	CStr name;
};

void GatherTexture(Mtl* mtl, std::vector<BitmapTex*>& textures, int& channel, int& id)
{
	for (int i = 0; mtl && i < mtl->NumSubTexmaps(); i++)
	{
		CStr name = mtl->GetSubTexmapSlotName(i);
		if (mtl->GetSubTexmap(i) && mtl->GetSubTexmap(i)->ClassID() == Class_ID(BMTEX_CLASS_ID, 0))
		{
			if (BitmapTex *bmt = (BitmapTex*)mtl->GetSubTexmap(i))
			{
				channel = bmt->GetMapChannel();
				if (std::find(textures.begin(), textures.end(), bmt) == textures.end())
				{
					textures.push_back(bmt);
					id = textures.size() - 1;
				}
				else
				{
					id = &*std::find(textures.begin(), textures.end(), bmt) - &textures[0];
				}
				return;
			}
		}
	}

	channel = 0;

	if (std::find(textures.begin(), textures.end(), (BitmapTex*)0) == textures.end())
	{
		textures.push_back((BitmapTex*)0);
		id = textures.size() - 1;
	}
	else
	{
		id = &*std::find(textures.begin(), textures.end(), (BitmapTex*)0) - &textures[0];
	}
}

Point3 GetVertexNormal(Mesh* mesh, int faceNo, RVertex* rv)
{
    Face* f = &mesh->faces[faceNo];
    DWORD smGroup = f->smGroup;
    int numNormals;
    Point3 vertexNormal;

    if (rv->rFlags & SPECIFIED_NORMAL)
    {
        vertexNormal = rv->rn.getNormal();
    }
    else if ((numNormals = rv->rFlags & NORCT_MASK) && smGroup)
    {
        if (numNormals == 1)
        {
            vertexNormal = rv->rn.getNormal();
        }
        else
        {
            for (int i = 0; i < numNormals; i++)
            {
                if (rv->ern[i].getSmGroup() & smGroup)
                {
                    vertexNormal = rv->ern[i].getNormal();
                }
            }
        }
    }
    else
    {
        vertexNormal = mesh->getFaceNormal(faceNo);
    }

    return vertexNormal;
}

static void CollectSubMeshes(INode* node, Mesh* mesh, ISkin* skin, Interface* gi, std::list<mesh_t>& submeshes, std::list<bone_t>& bones) {
	ISkinContextData* skincontext = skin->GetContextInterface(node);

	if (skincontext == NULL)
	{
		return;
	}

	std::list<MtlID> materials;

	extern bool bSeparateByMatID;

	if ((node->GetMtl() && node->GetMtl()->ClassID() == Class_ID(0x200, 0)) || bSeparateByMatID) //multimaterial
	{
		for (int nFace = 0; nFace < mesh->numFaces; nFace++)
		{
			if (std::find(materials.begin(), materials.end(), mesh->faces[nFace].getMatID()) == materials.end())
			{
				materials.push_back(mesh->faces[nFace].getMatID());
			}
		}
	}
	else
	{
		materials.push_back(0);
	}

	materials.sort();

	for (std::list<MtlID>::iterator material = materials.begin(); material != materials.end(); material++)
	{
		submeshes.push_back(mesh_t());
		mesh_t* submesh = &submeshes.back();
		submesh->material = *material;
		submesh->texture_id = -1;
		int channel = 0;

		if (node->GetMtl()) {
			if (node->GetMtl()->ClassID() == Class_ID(BAKE_SHELL_CLASS_ID, 0)) {
				GatherTexture(node->GetMtl()->GetSubMtl(1), textures, channel, submesh->texture_id);
			} else if (node->GetMtl()->GetSubMtl(*material)) {
				if (node->GetMtl()->GetSubMtl(*material)->ClassID() == Class_ID(BAKE_SHELL_CLASS_ID, 0)) {
					GatherTexture(node->GetMtl()->GetSubMtl(*material)->GetSubMtl(1), textures, channel, submesh->texture_id);
				} else {
					GatherTexture(node->GetMtl()->GetSubMtl(*material), textures, channel, submesh->texture_id);
				}
			} else {
				GatherTexture(node->GetMtl(), textures, channel, submesh->texture_id);
			}
		}

		//GatherTexture((node->GetMtl() && node->GetMtl()->GetSubMtl(*material)) ? 
		//	node->GetMtl()->GetSubMtl(*material) : node->GetMtl(),
		//	textures, channel, submesh->texture_id);

		mesh->buildNormals();

		for (int nFace = 0; nFace < mesh->numFaces; nFace++)
		{
			Face* face = mesh->faces + nFace;
			TVFace* tvface = &mesh->mapFaces(channel)[nFace];

			if (materials.size() > 1 && face->getMatID() != submesh->material)
			{
				continue;
			}

			size_t triangle[3];

			for (int n = 0; n < 3; n++)
			{
				mesh_t::vertex_t v;

				v.id = submesh->vertices.size();
				v.position = mesh->getVert(face->getVert(n));
				
				v.smthg = face->getSmGroup();
				if (mesh->vertCol && mesh->numCVerts > face->getVert(n)) {
					Point3 c = mesh->vertCol[face->getVert(n)];
					v.color = Color2(c.x, c.y, c.z).color;
				} else {
					v.color = 0xffffffff;
				}

				if (mesh->mapVerts(channel))
				{
					v.uv = mesh->mapVerts(channel)[tvface->t[n]];
					v.uv.y = 1 - v.uv.y; //IMPORTANT: flip y-axis
				}
				else
				{
					v.uv = Point3(0,0,0);
				}

				mesh_t::vertices_t::const_iterator vertex_it = std::find(submesh->vertices.begin(), submesh->vertices.end(), v);

				if (vertex_it == submesh->vertices.end())
				{
					mesh_t::vertices_t::const_iterator org_it = std::find(submesh->vertices.begin(), submesh->vertices.end(), v.position);

					triangle[n] = submesh->vertices.size();

					if (org_it == submesh->vertices.end())
					{
						int vertex_id = face->v[n];

						v.weight.id = submesh->weights.size();
						v.weight.count = 0;

						for (int i = 0; i < skincontext->GetNumAssignedBones(vertex_id); i++)
						{
							int bone_id = skincontext->GetAssignedBone(vertex_id, i);
							float weight = skincontext->GetBoneWeight(vertex_id, i);

							if (weight > 0.0001f)
							{
								Matrix3 node_matrix = skin->GetBone(bone_id)->GetNodeTM(gi->GetTime());
								Matrix3 node_rotation_matrix = node_matrix;
								node_rotation_matrix.SetTrans(Point3(0,0,0));
								Point3 position = v.position * Inverse(node_matrix);
								Point3 normal = GetVertexNormal(
									mesh, nFace, mesh->getRVertPtr(face->getVert(n))) * Inverse(node_rotation_matrix);
								normal = normal.Normalize();
								std::list<bone_t>::const_iterator bone = std::find(bones.begin(), bones.end(), skin->GetBone(bone_id));
								if (bone == bones.end())
								{
									MessageBox(NULL, "Incorrect root bone.", "Error", MB_OK);
									return;
								}
								v.weight.count++;
								submesh->weights.push_back(mesh_t::weight_t(position, normal, bone->id, weight));
							}
						}
					}
					else 
					{
						v.weight.id = submesh->vertices[org_it->id].weight.id;
						v.weight.count = submesh->vertices[org_it->id].weight.count;
					}

					submesh->vertices.push_back(v);
				}
				else 
				{
					triangle[n] = vertex_it->id;
				}
			}

			submesh->faces.push_back(mesh_t::face_t(triangle));
			submesh->name = node->GetName();
		}
	}
}

static void CollectBones(INode* node, std::list<bone_t>& bones, bool add_this = true)
{
	if (node->IsNodeHidden()) {
		return;
	}

	if (add_this)
	{
		std::list<bone_t>::const_iterator parent = std::find(bones.begin(), bones.end(), node->GetParentNode());

		if (parent == bones.end())
		{
			bones.push_back(bone_t(bones.size(), -1, node)); //root bone
		}
		else
		{
			bones.push_back(bone_t(bones.size(), parent->id, node));
		}
	}

	for (int i = 0; i < node->NumberOfChildren(); i++)
	{
		CollectBones(node->GetChildNode(i), bones);
	}
}

INode *XmlMeshExport::GetRootBoneForSkinnedMesh(ISkin* skin)
{
	int numbones = skin->GetNumBones();

	std::list<INode*> result_chain;

	for (int i = 0; i < numbones; i++)
	{
		INode* bone = skin->GetBone(i);

		if (i == 0)
		{
			GetBoneChainForNode(bone, result_chain);
		}
		else 
		{
			std::list<INode*> bone_chain;

			GetBoneChainForNode(bone, bone_chain);

			for (std::list<INode*>::iterator it = result_chain.begin(); it != result_chain.end();)
			{
				std::list<INode*>::const_iterator pos = std::find(bone_chain.begin(), bone_chain.end(), *it);

				if (pos == bone_chain.end()) 
				{
					it = result_chain.erase(it);
				}
				else
				{
					it++;
				}
		
			}
		}
	}

	if (result_chain.size() > 0)
	{
		return result_chain.back();
	}
	else
	{
		return 0;
	}
}

void XmlMeshExport::GetBoneChainForNode(INode* node, std::list<INode*>& chain)
{
	if (node->IsRootNode())
	{
		return;
	}
	else
	{
		chain.push_front(node);

		return GetBoneChainForNode(node->GetParentNode(), chain);
	}
}

Modifier* XmlMeshExport::GetSkinModifier(INode* node)
{
	Object* obj = node->EvalWorldState(0, true).obj;

	if(obj && obj->SuperClassID() == GEOMOBJECT_CLASS_ID)
	{
		Object* ref = node->GetObjectRef();

		while(ref &&(ref->SuperClassID() == GEN_DERIVOB_CLASS_ID))
		{
			IDerivedObject* derived =(IDerivedObject*)ref;

			for(int m = 0; m < derived->NumModifiers(); m++)
			{
				Modifier* modifier = derived->GetModifier(m);

				if(modifier->ClassID() == SKIN_CLASSID)
				{
					return modifier;
				}
			}

			ref = derived->GetObjRef();
		}
	}

	return NULL;
}

static void StreamVersion(FileOutput& file)
{
	if (file.mode == FileOutput::BINARY)
	{
		file.PushTag("VERS");

		{

			file.write((char*)&MESHFORMAT_VERSION, sizeof(MESHFORMAT_VERSION));
		
		}

		file.PopTag();
	}
	else
	{
	}
}

static void StreamFrameRate(FileOutput& file)
{
	if (file.mode == FileOutput::BINARY)
	{
		file.PushTag("RATE");

		{
			int framerate = GetFrameRate();

			file.write((char*)&framerate, sizeof(framerate));
		}

		file.PopTag();
	}
	else
	{
	}
}

static void StreamFrameCount(FileOutput& file, Interface* gi, TimeValue time_start, TimeValue time_end)
{
	if (file.mode == FileOutput::BINARY)
	{
		file.PushTag("SIZE");

		{
			Interval interval = gi->GetAnimRange();

			int framecount = (time_end - time_start) / GetTicksPerFrame() + 1;

			file.write((char*)&framecount, sizeof(framecount));
		}

		file.PopTag();
	}
	else
	{
	}
}

static void StreamFrames(FileOutput& file, std::list<bone_t>& bones, Interface* gi, TimeValue time_start, TimeValue time_end)
{
	if (file.mode == FileOutput::BINARY)
	{
		typedef int flag_type;
		const int flag_moved = 0x1;
		const int flag_rotated = 0x2;
		const int flag_scaled = 0x4;

		std::vector<flag_type> _flags(bones.size());
		memset(&*_flags.begin(), 0xFFFFFFFF, _flags.size() * sizeof(flag_type));
		TimeValue time_step = GetTicksPerFrame();

		if (bOptimizeSequences)
		{
			memset(&*_flags.begin(), 0, _flags.size() * sizeof(flag_type));
			flag_type* flag = &*_flags.begin();

			const int _step = time_step * 4; /*some optimization*/

			for (std::list<bone_t>::iterator b = bones.begin(); b != bones.end(); ++b, ++flag)
			{
				Matrix3 matrix;

				if (IsRootBone(b->node))
				{
					matrix = b->node->GetNodeTM(time_start);
				}
				else
				{
					matrix = b->node->GetNodeTM(time_start) * Inverse(b->node->GetParentTM(time_start));
				}

				AffineParts _old;

				decomp_affine(matrix, &_old);

				Control *cscl = b->node->GetTMController()->GetScaleController();
				Class_ID cid = cscl->ClassID();
				if (cid == Class_ID(HYBRIDINTERP_SCALE_CLASS_ID, 0)) {
					ScaleValue skey;
					cscl->GetValue(time_start, &skey, FOREVER);
					_old.k = Point3(skey.s.x, skey.s.y, skey.s.z);
				}

				for (TimeValue time = gi->GetAnimRange().Start(); time <= gi->GetAnimRange().End(); time += _step)
				{
					Matrix3 matrix;

					if (IsRootBone(b->node))
					{
						matrix = b->node->GetObjectTM(time);
					}
					else
					{
						matrix = b->node->GetNodeTM(time) * Inverse(b->node->GetParentTM(time));
					}

					AffineParts _new;

					decomp_affine(matrix, &_new);

					Control *cscl = b->node->GetTMController()->GetScaleController();
					Class_ID cid = cscl->ClassID();
					if (cid == Class_ID(HYBRIDINTERP_SCALE_CLASS_ID, 0)) {
						ScaleValue skey;
						cscl->GetValue(time, &skey, FOREVER);
						_new.k = Point3(skey.s.x, skey.s.y, skey.s.z);
					}



					const float eps = 0.001f;

	#define animated(Value) fabs(_old.Value - _new.Value) > eps

					if (animated(t.x) || animated(t.y) || animated(t.z))
					{
						*flag |= flag_moved;
					}

					if (animated(q.x) || animated(q.y) || animated(q.z) || animated(q.w))
					{
						*flag |= flag_rotated;
					}

					if (animated(k.x) || animated(k.y) || animated(k.z))
					{
						*flag |= flag_scaled;
					}

	#undef animated
				}
			}

			file.PushTag("FLAG");
			file.write((char*)&*_flags.begin(), sizeof(flag_type) * _flags.size());
			file.PopTag();
		}

		if (bExportRotationSequence)
		{
			file.PushTag("ROTN");

			for (TimeValue n = time_start; n <= time_end; n += GetTicksPerFrame())
			{
				const flag_type* flag = &*_flags.begin();

				for (std::list<bone_t>::iterator b = bones.begin(); b != bones.end(); ++b, ++flag)
				{
					if (*flag & flag_rotated)
					{
						XmlMeshExport::StreamNodeProps(file, b->node, n, EXPORT_ROTATION_AS_SHORT);
					}
				}
			}

			file.PopTag();
		}

		if (bExportPositionSequence)
		{
			file.PushTag("POSN");

			for (TimeValue n = time_start; n <= time_end; n += GetTicksPerFrame())
			{
				const flag_type* flag = &*_flags.begin();

				for (std::list<bone_t>::iterator b = bones.begin(); b != bones.end(); ++b, ++flag)
				{
					if (*flag & flag_moved)
					{
						XmlMeshExport::StreamNodeProps(file, b->node, n, EXPORT_POSITION);
					}
				}
			}

			file.PopTag();
		}

		if (bExportScaleSequence)
		{
			file.PushTag("SCAL");

			for (TimeValue n = time_start; n <= time_end; n += GetTicksPerFrame())
			{
				const flag_type* flag = &*_flags.begin();

				for (std::list<bone_t>::iterator b = bones.begin(); b != bones.end(); ++b, ++flag)
				{
					if (*flag & flag_scaled)
					{
						TCHAR* name = b->node->GetName();

						XmlMeshExport::StreamNodeProps(file, b->node, n, EXPORT_SCALE);
					}
				}
			}

			file.PopTag();
		}
	}
	else
	{
		file << "<Frames>" << std::endl;

		for (TimeValue n = time_start; n <= time_end; n += GetTicksPerFrame())
		{
			file << "<Frame" << n / GetTicksPerFrame() << ">" << std::endl;

			for (std::list<bone_t>::iterator b = bones.begin(); b != bones.end(); b++)
			{
				file << "<Bone" << b->id;

				XmlMeshExport::StreamNodeProps(file, b->node, n);

				file << ">" << std::endl;
			}

			file << "</Frame" << n / GetTicksPerFrame() << ">" << std::endl;
		}

		file << "</Frames>" << std::endl;
	}
}

static void StreamHierarchy(FileOutput& file, std::list<bone_t>& bones, Interface* gi)
{
	if (file.mode == FileOutput::BINARY)
	{
		file.PushTag("BONE");
		std::for_each(bones.begin(), bones.end(), std::bind2nd(std::mem_fun_ref(&bone_t::Stream), &file));
		file.PopTag();
	}
	else
	{
		file << "<Hierarchy numbones=\"" << bones.size() << "\" >" << std::endl;
		std::for_each(bones.begin(), bones.end(), std::bind2nd(std::mem_fun_ref(&bone_t::Stream), &file));
		file << "</Hierarchy>" << std::endl;
	}
}

static void StreamMeshes(FileOutput& file, std::list<bone_t>& bones, std::list<mesh_t>& submeshes, Interface* gi)
{
	if (file.mode == FileOutput::BINARY)
	{
		file.PushTag("GEOM");
		std::for_each(submeshes.begin(), submeshes.end(), std::bind2nd(std::mem_fun_ref(&mesh_t::Stream), &file));
		file.PopTag();
	}
	else
	{
		file << "<Meshes nummeshes=\"" << submeshes.size() << "\" >" << std::endl;
		std::for_each(submeshes.begin(), submeshes.end(), std::bind2nd(std::mem_fun_ref(&mesh_t::Stream), &file));
		file << "</Meshes>" << std::endl;
	}
}

void WriteTexture(FileOutput& file, BitmapTex* texture, const char* tag = 0)
{
	if (Bitmap* map = texture->GetBitmap(0))
	{
		TCHAR szTempFileName[MAX_PATH];  
		TCHAR lpTempPathBuffer[MAX_PATH];

		std::string filename;

		GetTempPath(MAX_PATH, lpTempPathBuffer);
		GetTempFileName(lpTempPathBuffer, TEXT("bmm"), 0, szTempFileName);

		filename = std::string(szTempFileName, MAX_PATH);
		filename = filename.substr(0, filename.find_last_of(".")) + ".tga";

		BitmapInfo bi;
		bi.SetName(filename.c_str());

		map->OpenOutput(&bi);
		map->Write(&bi);
		map->Close(&bi);

		if (FILE* fp = fopen(filename.c_str(), "rb"))
		{
			if (tag) file.PushTag(tag);
			fseek(fp, 0L, SEEK_END);
			int size = ftell(fp);
			char* buffer = new char[size];
			rewind(fp);
			fread(buffer, 1, size, fp);
			file.write(buffer, size);
			delete[] buffer;
			fclose(fp);			
			if (tag) file.PopTag();
		}

		remove(szTempFileName);
		remove(filename.c_str());
	}
}

void StreamTextures(FileOutput& file, std::vector<BitmapTex*>& textures, bool use_nodraw)
{
	for (std::vector<BitmapTex*>::const_iterator it = textures.begin(); it != textures.end(); ++it)
	{
		file.PushTag("TXTR");
		file.PushTag("NAME");
		if (*it)
		{
			std::string name = (*it)->GetMapName();
			file.write(name.data() + name.find_last_of("\\") + 1, name.find_last_of(".") - name.find_last_of("\\") - 1);
		}
		else
		{
			if (use_nodraw)
			{
				file.write("$nodraw", strlen("$nodraw"));
			}
			else
			{
				file.write("$depthonly", strlen("$depthonly"));
			}
		}
		file.PopTag();
		if (bEmbedTextures && *it)
		{
			WriteTexture(file, *it, "BTMP");
		}
		file.PopTag();
	}
}

void XmlMeshExport::ProcessMeshes(FileOutput& file, INode* root, SkinnedMeshes& meshes, Interface* gi) const
{
	g_bindPoseTime = 0;

	std::list<mesh_t> submeshes;
	std::list<bone_t> bones;

	root_bones.clear();
	root_bones.push_back(root);

	CollectBones(root, bones);

	textures.clear();

	for (SkinnedMeshes::iterator it = meshes.begin(); it != meshes.end(); it++)
	{
		INode* node = it->node;
		ISkin* skin = it->skin;

		const ObjectState& os = node->EvalWorldState(gi->GetTime());

		if (!os.obj || (os.obj->SuperClassID() != GEOMOBJECT_CLASS_ID && os.obj->SuperClassID() != SHAPE_CLASS_ID))
		{
			return;
		}

		if (!os.obj->CanConvertToType(triObjectClassID))
		{
			return;
		}

		TriObject* tri = (TriObject*)(os.obj->ConvertToType(gi->GetTime(), triObjectClassID));

		if (!tri)
		{
			return;
		}

		Mesh* mesh = new Mesh(tri->GetMesh());

		if (os.obj != tri)
			tri->AutoDelete();

		Matrix3 matrix = node->GetObjTMAfterWSM(gi->GetTime());

		for (int i = 0; i < mesh->getNumVerts(); i++)
		{
			mesh->verts[i] = mesh->verts[i] * matrix;
		}

		CollectSubMeshes(node, mesh, skin, gi, submeshes, bones);

		delete mesh;
	}

	if (file.mode == FileOutput::BINARY)
	{
		file.PushTag("MODL");
		StreamVersion(file);
		StreamHierarchy(file, bones, gi);
		StreamTextures(file, textures, true);
		StreamMeshes(file, bones, submeshes, gi);
		file.PopTag();
	}
	else
	{
		file << "<Model>" << std::endl;
		StreamHierarchy(file, bones, gi);
		StreamMeshes(file, bones, submeshes, gi);
		file << "</Model>" << std::endl;
	}
}

void XmlMeshExport::ProcessAnimations(FileOutput& file, std::list<INode*>& roots, Interface* gi, TimeValue a, TimeValue b) const 
{
	std::list<bone_t> bones_tmp, bones;

	g_bindPoseTime = a;

	root_bones = roots;

	bMoveToZero = false;

	for (std::list<INode*>::const_iterator it = root_bones.begin(); it != root_bones.end(); ++it)
	{
		CollectBones(*it, bones_tmp, false);
	}

	for (std::list<INode*>::const_iterator it = root_bones.begin(); it != root_bones.end();)
	{
		std::list<bone_t>::const_iterator it2;
		for (it2 = bones_tmp.begin(); it2 != bones_tmp.end() && it2->node != *it; ++it2) {}
		if (it2 != bones_tmp.end())
		{
			it = root_bones.erase(it);
		}
		else
		{
			CollectBones(*it++, bones);
		}
	}

	if (file.mode == FileOutput::BINARY)
	{
		file.PushTag("ANIM");
		{
			StreamVersion(file);
			StreamFrameRate(file);
			StreamFrameCount(file, gi, a, b);
			StreamHierarchy(file, bones, gi);
			StreamFrames(file, bones, gi, a, b);
		}
		file.PopTag();
	}
	else
	{
		file << "<Animation framerate=\"" << GetFrameRate() <<  "\">" << std::endl;
		StreamHierarchy(file, bones, gi);
		StreamFrames(file, bones, gi, a, b);
		file << "</Animation>" << std::endl;
	}
}

void XmlMeshExport::StreamNodeProps(FileOutput& file, INode* node, TimeValue time, int flags)
{
	Matrix3 matrix;

	matrix.IdentityMatrix();

	if (node)
	{
		if (IsRootBone(node))
		{
			matrix = node->GetNodeTM(time);
		}
		else
		{
			matrix = node->GetNodeTM(time) * Inverse(node->GetParentTM(time));
		}
	}

	AffineParts ap;
	decomp_affine(matrix, &ap);

	//write data

	if (file.mode == FileOutput::BINARY)
	{
		if (flags & EXPORT_POSITION)
		{
			file.write((char*)&Point3(ap.t.x, ap.t.y, ap.t.z), sizeof(Point3));
		}
		if (flags & EXPORT_ROTATION)
		{
			file.write((char*)&Quat(ap.q.x, ap.q.y, ap.q.z, -ap.q.w), sizeof(Quat));
		}
		else if (flags & EXPORT_ROTATION_AS_SHORT)
		{
			short v[3] = { ap.q.x * 32767, ap.q.y * 32767, ap.q.z * 32767 };

			if (ap.q.w > 0)
			{
				v[0] = -v[0];
				v[1] = -v[1];
				v[2] = -v[2];
			}

			file.write((char*)v, sizeof(v));
		}

		if (flags & EXPORT_SCALE)
		{
			Control *cscl = node->GetTMController()->GetScaleController();
			Class_ID cid = cscl->ClassID();
			if (cid == Class_ID(HYBRIDINTERP_SCALE_CLASS_ID, 0)) {
				ScaleValue skey;
				cscl->GetValue(time, &skey, FOREVER);
				Point3 p(skey.s.x, skey.s.y, skey.s.z);
				file.write((char*)&p, sizeof(Point3));
			} else {
				file.write((char*)&ap.k, sizeof(Point3));
			}
		}
	}
	else
	{
		if (flags & EXPORT_POSITION)
		{
			if(fabs(ap.t.x) > EPS || fabs(ap.t.y) > EPS || fabs(ap.t.z) > EPS)
			{
				file << " position=\"" << ap.t.x << "," << ap.t.y << "," << ap.t.z << "\"";
			}
		}

		if (flags & EXPORT_ROTATION || flags & EXPORT_ROTATION_AS_SHORT)
		{
			if(fabs(ap.q.x) > EPS || fabs(ap.q.y) > EPS || fabs(ap.q.z) > EPS || fabs(ap.q.w - 1) > EPS)
			{
				file << " rotation=\"" << ap.q.x << "," << ap.q.y << "," << ap.q.z << "," << -ap.q.w << "\"";
			}
		}

		if (flags & EXPORT_SCALE)
		{
			if(fabs(ap.k.x - 1) > EPS || fabs(ap.k.y - 1) > EPS || fabs(ap.k.z - 1) > EPS)
			{
				file << " scale=\"" << ap.k.x << "," << ap.k.y << "," << ap.k.z << "\"";
			}
		}
	}
}
