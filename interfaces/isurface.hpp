#pragma once

#include "imaterial.hpp"
#include "itexture.hpp"
#include "irendererbuffer.hpp"
#include "iobject.hpp"

#include "../math/math.hpp"

#include <boost/shared_ptr.hpp>

struct ISurface: IObject
{
	ISurface()
		: fillmode(FILL_SOLID)
		, primitive(TRIANGLES)
		, time(0)
		, light(0)
		, alphareference(1.0f)
		, flags(0)
		, sortvalue(0)
		, hack_overinterface(false)
		, hack_underworld(false)
	{
	}

	enum FillMode
	{
		FILL_UNKNOWN,
		FILL_POINT,
		FILL_WIREFRAME,
		FILL_SOLID,
	} fillmode;

	enum PrimType
	{
		TRIANGLES,
		LINES,
		POINTS
	} primitive;

	enum VarType
	{
		VARTYPE_INT,
		VARTYPE_BOOL,
		VARTYPE_FLOAT,
		VARTYPE_VECTOR4,
		VARTYPE_MATRIX,
	};

	struct variable_t { 
		VarType vartype;
		char name[32];
		void* value; 
		int count;
	};

	float time;
	const light_t* light;
	color_t color;
	float alphareference;
	int flags;
	float sortvalue;
	vec3_t pivot;

	bool hack_overinterface;
	bool hack_underworld;

	// shader-related functions

	virtual size_t GetVariablesCount() const { return 0; }
	virtual const variable_t* GetVariables() const { return 0; }

	// basic functions

	virtual const vertex_t* GetVertices() const = 0;
	virtual const index_t* GetIndices() const = 0;

	virtual const IRendererBuffer* GetVertexBuffer() const = 0;
	virtual const IRendererBuffer* GetIndexBuffer() const = 0;

	virtual size_t GetVerticesCount() const = 0;
	virtual size_t GetIndicesCount() const = 0;

	virtual const IMaterial* GetMaterial() const = 0;
	virtual const ITexture* GetLightmap() const = 0;

	virtual const mat4_t* GetMatrix() const = 0;
};	