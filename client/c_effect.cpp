/**
	Simple STATIC MESH class, supports LIGHTMAPS
**/

#include "c_client.hpp"
#include "c_entity.hpp"
#include "c_material.hpp"

namespace client
{
	extern shared_ptr<const IMaterial> LoadMaterial(FILE*, int, const std::string&, bool);

	namespace io 
	{
		struct EffectFile: shared::Resource
		{
			void Mod_ReadEFCT(FILE* file, int size) {
				F_BlockReadLoop(file, size) {
					switch (F_BlockReadHeader(file)) {
					case MAKEID('T', 'X', 'T', 'R'):
						textures.push_back(LoadMaterial(file, F_BlockReadSize(file), 
							f_StripExtension(GetPath()), true));
						break;
					case MAKEID('R', 'A', 'T', 'E'):
						F_BlockRead(file, rate);
						break;
					case MAKEID('S', 'I', 'Z', 'E'):
						F_BlockRead(file, num_frames);
						break;
					case MAKEID('M', 'E', 'S', 'H'):
						meshes.push_back(Submesh(this, file, F_BlockReadSize(file)));
						break;
					default:
						F_BlockSkip(file);
					}
				}
			}

			EffectFile(const std::string& filename)
				: shared::Resource(filename)
				, num_frames(1)
				, rate(30)
			{
				file_t file = framework->GetFileSystem()->LoadFile(filename);

				if (!file.valid())
					return;

				switch (F_BlockReadHeader(file)) {
				case  MAKEID('E', 'F', 'C', 'T'):
					Mod_ReadEFCT(file, F_BlockReadSize(file));
					break;
				default:
					framework->Log(IFramework::MSG_ERROR, "%s is not a effect file", file.name.c_str());
					F_BlockSkip(file);
				}

				file.close();
			}

			struct Submesh
			{
				Submesh(EffectFile* effect, FILE* file, int size)
				{
					num_frames = effect->num_frames;
					F_BlockReadLoop(file, size) {
						switch (F_BlockReadHeader(file)) {
						case MAKEID('N', 'A', 'M', 'E'):
							name = F_BlockReadString(file);
							break;
						case MAKEID('T', 'X', 'I', 'D'): {
							int txid = 0;
							F_BlockRead(file, txid);
							material = effect->textures[txid];
							break;
						}
						case MAKEID('V', 'R', 'T', 'X'):
							F_BlockReadConverVector(file, vertices, StaticVertex);
							break;
						case MAKEID('F', 'A', 'C', 'E'):
							F_BlockReadVector(file, triangles, Triangle);
							break;
						default:
							F_BlockSkip(file);
						}
					}
				}

				struct Triangle { WORD index[3]; };

				shared_ptr<const IMaterial> material;

				std::vector<vertex_t> vertices;
				std::vector<Triangle> triangles;
				std::string name;
				int num_frames;
			};

			std::vector<Submesh> meshes;
			std::vector<shared_ptr<const IMaterial> > textures;
			int num_frames, rate;
		};
	}

	class Effect: public Entity
	{
	public:
		Effect(
			const std::string& name,
			const vec3_t& position,
			const quat_t& rotation,
			const vec3_t& scale,
			const color_t& color,
			const std::string& source)
			: Entity(name, position, rotation, scale, color)
		{
			RenderCallback = (RenderCallbackT)&Effect::OnRender;
			AnimateCallback = (AnimateCallbackT)&Effect::OnAnimate;

			if (framework->GetFileSystem()->CheckFile(source))
			{
				SetModel(source);

				this->frame = 0;
				this->is_paused = (m_model && m_model->num_frames > 1) ? false : true;
				this->destroy_when_finished = true;
			}
			else
			{
				this->frame = 0;
				this->is_paused = true;
				this->destroy_when_finished = true;
			}
		}



		void SetModel(const std::string& source)
		{
			m_model = framework->GetResources()->Load<io::EffectFile>(source);
		}

		std::string GetModel() const
		{
			return m_model ? m_model->GetPath() : std::string();
		}

		void OnAnimate(float timestep) 
		{
			if (!is_paused && m_model) {
				frame += timestep * m_model->rate;
				while (frame >= m_model->num_frames - 1) {
					if (destroy_when_finished) {
						Dispose();
						return;
					}
				}
			}
		}

		void OnRender(IClientScene* scene) const
		{
			mat4_t matrix;

			vertex_t buffer[2048];

			if (GetFlags(FLAG_DISPOSED))
				return;

			BOOST_FOREACH(const io::EffectFile::Submesh& submesh, m_model->meshes)
			{
				Surface surface;
				surface.nVertices = submesh.vertices.size() / submesh.num_frames;
				for (int i = 0; i < surface.nVertices; i++)
				{
					const vertex_t& a = submesh.vertices[((int)frame % submesh.num_frames) * surface.nVertices + i];
					const vertex_t& b = submesh.vertices[(((int)frame + 1) % submesh.num_frames) * surface.nVertices + i];
					float k = frame - (int)frame;
					buffer[i].position = vec3_t::Lerp(k, a.position, b.position);
					buffer[i].uv = vec2_t::Lerp(k, a.uv, b.uv);
					buffer[i].color = color_t::Lerp(k, a.color, b.color) * GetColor();
				}
				surface.pMatrix = &GetMatrix();
				surface.pMaterial = submesh.material;
				surface.pVertices = buffer;
				surface.pIndices = (WORD*)&*submesh.triangles.begin();
				surface.nIndices = submesh.triangles.size() * 3;
				scene->PushSurface(&surface);
			}
		}

		float frame;
		bool is_paused;
		bool destroy_when_finished;

	private:
		shared_ptr<const io::EffectFile> m_model;
	};
}

void export_Effect()
{
	namespace py = boost::python;

	typedef client::Effect T;

	DISPOSEME(4) py::class_<T, shared_ptr<T>, py::bases<client::Entity>, boost::noncopyable>("Effect", py::no_init)
		.def(py::init<std::string, vec3_t, quat_t, vec3_t, color_t, std::string>((
			py::arg("id") = std::string("Effect#"),
			py::arg("position") = vec3_t::Zero,
			py::arg("rotation") = quat_t::Identity,
			py::arg("scale") = vec3_t::One,
			py::arg("color") = color_t(),
			py::arg("source") = std::string("none"))))
		.add_property("source", &T::GetModel, &T::SetModel)
		.def_readwrite("paused", &T::is_paused)
		.def_readwrite("frame", &T::frame)
		.def_readwrite("destroy_when_finished", &T::destroy_when_finished)
		;
}
