#include "Driver.hpp"
#include "Render.hpp"

LPDIRECT3D Driver::m_pD3D = NULL;
LPDIRECT3DDEVICE Driver::m_pD3DDevice = NULL;
LPDIRECT3DVERTEXBUFFER Driver::p_VertexBuffer = NULL;
LPDIRECT3DINDEXBUFFER Driver::p_IndexBuffer = NULL;
LPDIRECT3DTEXTURE Driver::p_RenderTexture = NULL;
LPDIRECT3DSURFACE Driver::p_BackBuffer = NULL;
D3DPRESENT_PARAMETERS Driver::m_Params;
