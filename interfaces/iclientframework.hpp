#pragma once

#include "iframework.hpp"
#include "iclientscene.hpp"
#include "irenderer.hpp"
#include "isoundsystem.hpp"
#include "iwindow.hpp"
#include "ilocalization.hpp"
#include "irendererobjectmanager.hpp"

struct IClientFramework: IFramework
{
	virtual boost::shared_ptr<IClientScene> GetScene() const = 0;
	virtual boost::shared_ptr<ISoundSystem> GetSoundSystem() const = 0;
	virtual boost::shared_ptr<IWindow> GetWindow() const = 0;
	virtual boost::shared_ptr<ILocalization> GetLocalization() const = 0;
	virtual boost::shared_ptr<IRendererObjectManager> GetRendererObjectManager() const = 0;

	virtual void SetScene(boost::shared_ptr<IClientScene> ) = 0;
	virtual void SetSoundSystem(boost::shared_ptr<ISoundSystem> ) = 0;
	virtual void SetWindow(boost::shared_ptr<IWindow> ) = 0;
	virtual void SetLocalization(boost::shared_ptr<ILocalization> ) = 0;
	virtual void SetRendererObjectManager(boost::shared_ptr<IRendererObjectManager> ) = 0;
};
