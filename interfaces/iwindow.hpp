#pragma once

#include "iobject.hpp"

#include "../shared/types.hpp"

struct IWindow: IObject
{
	enum Cursor
	{
		CURSOR_NONE,
		CURSOR_ARROW,
		CURSOR_HAND,
		CURSOR_HOLD,
		CURSOR_WAIT,
	};

	virtual void Show(bool show) = 0;
	virtual bool IsVisible() const = 0;
	virtual bool Centralize() = 0;
	virtual bool IsFullscreen() const = 0;
#ifdef WIN32
	virtual HWND GetWindow() const = 0;
	virtual HDC GetDC() const = 0;
#elif defined __APPLE__
	virtual void* GetWindow() const = 0;
#else
	virtual Display* GetDisplay() const = 0;
	virtual Window* GetWindow() const = 0;
#endif
	virtual dim_t GetSize() const = 0;

	virtual point_t GetCursorPos() const = 0;
	virtual void SetCursorPos(const point_t&) = 0;

	virtual void SetCursor(Cursor) = 0;
	virtual Cursor GetCursor() const = 0;
};
