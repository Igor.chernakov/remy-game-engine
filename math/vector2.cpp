#include "math.hpp"
#include <sstream>

using namespace math;

const Vector2 Vector2::Zero(0,0);
const Vector2 Vector2::One(1,1);

Vector2::Vector2()
{
	Assign(0, 0);
}

Vector2::Vector2(const Vector2& param)
{
	Assign(param[0], param[1]);
}

Vector2::Vector2(Real X, Real Y)
{
	Assign(X, Y);
}

Vector2::Vector2(const Real* param)
{
	Assign(param[0], param[1]);
}

Vector2::Vector2(const std::string& param)
{
	std::stringstream(param) >> *this;
}

Vector2& Vector2::Maximize()
{
	v[0] = FLT_MAX;
	v[1] = FLT_MAX;
	return *this;
}

Vector2& Vector2::Minimize()
{
	v[0] = -FLT_MAX;
	v[1] = -FLT_MAX;
	return *this;
}

Vector2& Vector2::Assign(Real x, Real y)
{
	v[0] = x;
	v[1] = y;
	return *this;
}

bool Vector2::Equal(const Vector2& left, const Vector2& right)
{
	return fabs(left.x - right.x) < EPS && fabs(left.y - right.y) < EPS;
}

Vector2 Vector2::Add(const Vector2& left, const Vector2& right)
{
	return Vector2(left.x + right.x, left.y + right.y);
}

Vector2 Vector2::Substract(const Vector2& left, const Vector2& right)
{
	return Vector2(left.x - right.x, left.y - right.y);
}

Vector2 Vector2::Multiply(const Vector2& left, const Vector2& right)
{
	return Vector2(left.x * right.x, left.y * right.y);
}

Vector2 Vector2::Divide(const Vector2& left, const Vector2& right)
{
	return Vector2(left.x / right.x, left.y / right.y);
}

Vector2 Vector2::Scale(const Vector2& param, Real d)
{
	return Vector2(param.x * d, param.y * d);
}

Vector2 Vector2::Negate(const Vector2& param)
{
	return Vector2(-param.x, -param.y);
}

Vector2 Vector2::Normalize(const Vector2& param)
{
	Real length = Length(param);

	if (length > 0)
	{
		return param / length;
	}
	else
	{
		return param;
	}
}

Real Vector2::Length(const Vector2& param)
{
	return (Real)sqrt((param.x * param.x) + (param.y * param.y));
}

Real Vector2::Dot(const Vector2& left, const Vector2& right)
{
	return left.x * right.x + left.y * right.y;
}

Vector2 Vector2::Lerp(Real d, const Vector2& left, const Vector2& right)
{
	return left * (1 - d) + right * d;
}
