#include "math.hpp"
#include <sstream>

using namespace math;

const Vector4 Vector4::Zero(0,0,0,0);

const Vector4 Vector4::One(1,1,1,1);

Vector4::Vector4()
{
	Assign(0, 0, 0, 0);
}

Vector4::Vector4(Real x, Real y, Real z, Real w)
{
	Assign(x, y, z, w);
}

Vector4::Vector4(const Real* param)
{
	Assign(param[0], param[1], param[2], param[3]);
}

Vector4::Vector4(const Vector4& param)
{
	Assign(param[0], param[1], param[2], param[3]);
}

Vector4::Vector4(const std::string& param)
{
	std::stringstream(param) >> *this;
}

bool Vector4::Equal(const Vector4& left, const Vector4& right)
{
	return fabs(left.x - right.x) < EPS && fabs(left.y - right.y) < EPS && fabs(left.z - right.z) < EPS && fabs(left.w - right.w) < EPS;
}

Vector4 Vector4::Add(const Vector4& left, const Vector4& right)
{
	return Vector4(left.x + right.x, left.y + right.y, left.z + right.z, left.w + right.w);
}

Vector4 Vector4::Substract(const Vector4& left, const Vector4& right)
{
	return Vector4(left.x - right.x, left.y - right.y, left.z - right.z, left.w - right.w);
}

Vector4 Vector4::Multiply(const Vector4& left, const Vector4& right)
{
	return Vector4(left.x * right.x, left.y * right.y, left.z * right.z, left.w * right.w);
}

Vector4 Vector4::Divide(const Vector4& left, const Vector4& right)
{
	return Vector4(left.x / right.x, left.y / right.y, left.z / right.z, left.w / right.w);
}

Vector4 Vector4::Scale(const Vector4& param, Real d)
{
	return Vector4(param.x * d, param.y * d, param.z * d, param.w * d);
}

Vector4 Vector4::Negate(const Vector4& param)
{
	return Vector4(-param.x, -param.y, -param.z, -param.w);
}

Vector4 Vector4::Normalize(const Vector4& v)
{
	Real length = Length(v);

	if (length > 0)
	{
		return v / length;
	}
	else
	{
		return v;
	}
}

Real Vector4::Length(const Vector4& param)
{
	return (Real)sqrt((param.x * param.x) + (param.y * param.y) + (param.z * param.z) + (param.w * param.w));
}

Real Vector4::Dot(const Vector4& left, const Vector4& right)
{
	return left.x * right.x + left.y * right.y + left.z * right.z + left.w * right.w;
}

Vector4 Vector4::Lerp(Real d, const Vector4& left, const Vector4& right)
{
	return left * (1 - d) + right * d;
}

Vector4& Vector4::Maximize()
{
	v[0] = FLT_MAX;

	v[1] = FLT_MAX;

	v[2] = FLT_MAX;

	v[3] = FLT_MAX;

	return *this;
}

Vector4& Vector4::Minimize()
{
	v[0] = -FLT_MAX;

	v[1] = -FLT_MAX;

	v[2] = -FLT_MAX;

	v[3] = -FLT_MAX;

	return *this;
}

Vector4& Vector4::Assign(Real x, Real y, Real z, Real w)
{
	v[0]=x, v[1]=y, v[2]=z, v[3]=w;

	return *this;
}

//Matrix operations

Vector4 Vector4::Multiply(const Vector4& v, const Matrix& m)
{
	Vector4 r;

	r.x = m.v[0][0] * v.x + m.v[1][0] * v.y + m.v[2][0] * v.z + m.v[3][0] * v.w;
	r.y = m.v[0][1] * v.x + m.v[1][1] * v.y + m.v[2][1] * v.z + m.v[3][1] * v.w;
	r.z = m.v[0][2] * v.x + m.v[1][2] * v.y + m.v[2][2] * v.z + m.v[3][2] * v.w;
	r.w = m.v[0][3] * v.x + m.v[1][3] * v.y + m.v[2][3] * v.z + m.v[3][3] * v.w;

	return r;
}

Vector4& Vector4::Multiply(const Matrix& m)
{
	return *this = Vector4::Multiply(*this, m);
}

Vector4 Vector4::operator*(const Matrix& m) const
{
	return Vector4::Multiply(*this, m);
}

Vector4& Vector4::operator*=(const Matrix& m)
{
	return *this = Vector4::Multiply(*this, m);
}
