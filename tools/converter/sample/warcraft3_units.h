#pragma once

#include "warcraft3_doo.h"

struct units_t {
	units_t(const BYTE **offset)
		: header(new doo_t::header_t(offset))
		, data(new data_t(header, offset))
	{};

	~units_t() {
		delete header;
		delete data;
	};

	doo_t::header_t *header;

	struct data_t {
		data_t(const doo_t::header_t *header, const BYTE **offset) {
			doodads = new doodad_t[header->doodads_count];

			ZeroMemory(doodads, sizeof(doodad_t) * header->doodads_count);

			for (int i = 0; i < header->doodads_count; i++) {
				COPYVAR(doodads[i].id, offset);
				COPYVAR(doodads[i].variation, offset);
				COPYVAR(doodads[i].position, offset);
				COPYVAR(doodads[i].angle, offset);
				COPYVAR(doodads[i].scale, offset);
				COPYVAR(doodads[i].nFlags, offset);
				COPYVAR(doodads[i].nPlayer, offset);
				COPYVAR(doodads[i].nUnknown1, offset);
				COPYVAR(doodads[i].nUnknown2, offset);
				COPYVAR(doodads[i].nHitPoints, offset);
				COPYVAR(doodads[i].nManaPoints, offset);
				COPYVAR(doodads[i].nDropped, offset);

				if (doodads[i].nDropped > 0) {
					doodads[i].pDropped = new doodad_t::dropped_item_t[doodads[i].nDropped];
					for (int j = 0; j < doodads[i].nDropped; j++) {
						COPYVAR(doodads[i].pDropped[j].nDropable, offset);
						if (doodads[i].pDropped[j].nDropable > 0) {
							doodads[i].pDropped[j].pDropable = new doodad_t::dropped_item_t::dropable_item_t[doodads[i].pDropped[j].nDropable];
							COPYMEM(doodads[i].pDropped[j].pDropable, offset, sizeof(doodad_t::dropped_item_t::dropable_item_t) * doodads[i].pDropped[j].nDropable);
						}
					}
				}

				COPYVAR(doodads[i].nGold, offset);
				COPYVAR(doodads[i].fTargetAcquisition, offset);
				COPYVAR(doodads[i].nHeroLevel, offset);
				COPYVAR(doodads[i].nItems, offset);

				if (doodads[i].nItems > 0) {
					doodads[i].pItems = new doodad_t::item_t[doodads[i].nItems];
					COPYMEM(doodads[i].pItems, offset, sizeof(doodad_t::item_t) * doodads[i].nItems);
				}

				COPYVAR(doodads[i].nAbilities, offset);

				if (doodads[i].nAbilities > 0) {
					doodads[i].pAbilities = new doodad_t::ability_t[doodads[i].nAbilities];
					COPYMEM(doodads[i].pAbilities, offset, sizeof(doodad_t::ability_t) * doodads[i].nAbilities);
				}

				COPYVAR(doodads[i].nRandomUnitFlag, offset);

				switch (doodads[i].nRandomUnitFlag) {
							case 0:
								*offset = *offset + 4; //skip
								break;
							case 1:
								*offset = *offset + 8; //skip
								break;
							case 2:
								COPYVAR(doodads[i].nRandomUnits, offset);
								doodads[i].pRandomUnits = new doodad_t::random_unit_t[doodads[i].nRandomUnits];
								COPYMEM(doodads[i].pRandomUnits, offset, sizeof(doodad_t::random_unit_t) * doodads[i].nRandomUnits);
								break;
				};

				COPYVAR(doodads[i].nCustomColor, offset);
				COPYVAR(doodads[i].nWayGate, offset);
				COPYVAR(doodads[i].nCreationNumber, offset);
			}
		}

#pragma pack( push, Doodads )
#pragma pack(1)

		struct doodad_t {
			char id[4];
			int variation;
			math::Vector3 position;
			float angle;
			math::Vector3 scale;
			BYTE nFlags;
			int nPlayer;
			BYTE nUnknown1;
			BYTE nUnknown2;
			int nHitPoints;
			int nManaPoints;

			int nDropped;
			struct dropped_item_t {
				int nDropable;
				struct dropable_item_t {
					char id[4];
					int nChance;
				} *pDropable;
			} *pDropped;

			int nGold;
			float fTargetAcquisition;
			int nHeroLevel;

			int nItems;
			struct item_t {
				int nSlot;
				char id[4];
			} *pItems;

			int nAbilities;
			struct ability_t {
				char id[4];
				int nActive;
				int nLevel;
			} *pAbilities;

			int nRandomUnitFlag;

			int nRandomUnits;
			struct random_unit_t {
				char id[4];
				int nChance;
			} *pRandomUnits;

			int nCustomColor;
			int nWayGate;
			int nCreationNumber;
		} *doodads;
	} *data;

#pragma pack( pop, Doodads )

};
