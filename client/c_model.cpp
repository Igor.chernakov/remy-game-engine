/**
	SKELETAL MODEL is a common visible object

	See summary in the bottom
**/

#include "c_client.hpp"
#include "c_entity.hpp"
#include "c_material.hpp"

#define MAX_ANIM_LAYERS 8

extern float BLEND_FUNC(float t);

#ifdef USE_GPU_SHADERS
#	define USE_GPU_SKINNING
#endif

#define USE_PLANARSHADOWS

extern IRenderer* __HackRenderer;
extern hash_t BuildHash(const char *_str);

struct Bone {
	char name[32];
	int id;
	int parent;
	vec3_t position;
	quat_t rotation;
	vec3_t scale;
};

struct event_t {
	char name[32];
	int frame;
};

struct BoneEx: Bone {
	BoneEx() {}
	BoneEx(const Bone& bone) : Bone(bone) {
		namehash = BuildHash(bone.name);
		parentBone = 0;
	}

	void Calculate() {
		matrix = rotation;
		matrix.SetFront(matrix.GetFront() * scale.x);
		matrix.SetRight(matrix.GetRight() * scale.y);
		matrix.SetUp(matrix.GetUp() * scale.z);
		matrix.SetTranslation(position);

		if (parentBone) {
			matrix = parentBone->matrix * matrix;
		}
	}
	BoneEx *parentBone;
	int namehash;
	mat4_t matrix;
};


namespace client
{
	//extern shared_ptr<const IMaterial> LoadMaterial(const std::string&, const shared::BinaryBlock&, bool);

	shared_ptr<const IMaterial> LoadMaterial(FILE* file, int size, const std::string& folder, bool is_global) {
		std::string texturename;
		shared_ptr<const Material> material;

		F_BlockReadLoop(file, size) {
			switch (F_BlockReadHeader(file)) {
			case MAKEID('N', 'A', 'M', 'E'):
				texturename = F_BlockReadString(file);
				break;
			case MAKEID('B', 'T', 'M', 'P'):
				if (!framework->GetResources()->IsLoaded(folder + "/" + texturename)) {
				int size = F_BlockReadSize(file);
					char *buffer = new char[size];
					fread(buffer, 1, size, file);
					material.reset(new Material(folder + "/" + texturename, 
						framework->GetRendererObjectManager()->LoadTexture(buffer, size)));
					framework->GetResources()->Append(folder + "/" + texturename, material);
					delete[] buffer;
				} else {
					F_BlockSkip(file);
					material = framework->GetResources()->Load<Material>(folder + "/" + texturename);
				}
				break;
			default:
				F_BlockSkip(file);
			}
		}

		if (!material && texturename.length() > 0) {
			material = framework->GetResources()->Load<Material>(
				(is_global ? "textures" : f_StripName(folder)) + "/" + texturename);
			if (is_global && material->GetLayersCount() == 1) {
				((Material*)&*material)->GetLayer(0)->address = ILayer::ADDRESS_REPEAT;
				if (ILayer* lightmap = (((Material*)&*material))->AddLayer()) {
					lightmap->blend.src = ILayer::BLEND_DESTCOLOR;
					lightmap->blend.dst = ILayer::BLEND_ZERO;
					lightmap->filter.min = ILayer::FILTER_LINEAR;
					lightmap->filter.mag = ILayer::FILTER_LINEAR;
					lightmap->filter.mip = ILayer::FILTER_LINEAR;
					lightmap->address = ILayer::ADDRESS_CLAMP;
					lightmap->tcgen = ILayer::TCGEN_VERTEX;
					lightmap->type = ILayer::TYPE_LIGHTMAP;
				}
			}
		}

		return material;
	}


	namespace io
	{
		/**
		This class loads SKELETAL MODEL from BlockFile (exported from 3dsmax)
		Chunk structure is following:
			MODL - header of the first block
				TXTR - (can be multiple) embedded or external texture 
				BONE - skeleton structure
				MESH - (can be multiple) mesh data
					TXID - texture id to use (from TXTR)
					VRTX - skin vertices (UVs and weight data)
					VCLR - vertex colors
					FACE - faces
					WGHT - weights
		**/

		struct ModelFile: shared::Resource
		{
			void Mod_ReadMODL(FILE* file, long size) {
				F_BlockReadLoop(file, size) {
					switch (F_BlockReadHeader(file)) {
						case MAKEID('V', 'E', 'R', 'S'):
							F_BlockRead(file, version);
							if (version < 200) 
								return;
							break;
						case MAKEID('T', 'X', 'T', 'R'):
							textures.push_back(LoadMaterial(file, F_BlockReadSize(file), f_StripExtension(GetPath()), false));
							break;
						case MAKEID('B', 'O', 'N', 'E'):
							F_BlockReadVector(file, bones, Bone);
							break;
#ifdef USE_GPU_SKINNING
						case MAKEID('B', 'I', 'N', 'D'):
							F_BlockReadVector(file, bindpose, mat4_t);
							break;
						case MAKEID('I', 'N', 'V', 'B'):
							F_BlockReadVector(file, inv_bindpose, mat4_t);
							break;
#endif
						case MAKEID('M', 'E', 'S', 'H'):
							meshes.push_back(Mesh(this, file, F_BlockReadSize(file)));
							break;
						case MAKEID('A', 'N', 'I', 'M'):
							F_BlockReadVector(file, animations, Animation);
							break;
						case MAKEID('F', 'R', 'A', 'M'):
							F_BlockReadVector(file, frames, ap_t);
							break;
						case MAKEID('E', 'V', 'N', 'T'):
							F_BlockReadVector(file, events, event_t);
							break;
						default:
							F_BlockSkip(file);
					}
				}
			}

			ModelFile(const std::string& filename)
				: Resource(filename)
			{
				file_t file = framework->GetFileSystem()->LoadFile(filename);

				if (!file.valid())
					return;

				switch (F_BlockReadHeader(file)) {
				case MAKEID('M', 'O', 'D', 'L'):
					Mod_ReadMODL(file, F_BlockReadSize(file));
					break;
				default:
					framework->Log(IFramework::MSG_ERROR, "%s is not a model file", file.name.c_str());
					F_BlockSkip(file);
				}

				file.close();

				SetupGPUSkinning();
			}

			void SetupGPUSkinning() {
				if (version < 200)
					return;

#ifdef USE_GPU_SKINNING
				Mesh* mesh = &*meshes.begin();

				int bone_stride = __HackRenderer->GetStats().bone_index_stride;

				for (int j = 0; j < meshes.size(); ++j, ++mesh) {
					vertex_t *output = &*mesh->vertices.begin();
					for (int i = 0; i < mesh->vertices.size(); ++i, ++output) {
						for (int k = 0; k < 4; ++k) {
							output->blend_indices.v[k] *= bone_stride;
						}
					}
					
					mesh->vertex_buffer = framework->GetRendererObjectManager()->CreateBuffer(
						IRendererBuffer::VERTEX_BUFFER, 
						(const char*)&*mesh->vertices.begin(),
						sizeof(vertex_t) * mesh->vertices.size());

					mesh->index_buffer = framework->GetRendererObjectManager()->CreateBuffer(
						IRendererBuffer::INDEX_BUFFER, 
						(const char*)&*mesh->triangles.begin(),
						sizeof(Mesh::Triangle) * mesh->triangles.size());
				}
#endif
			}

			struct Animation {
				Animation() 
					: framerate(10)
					, framestart(0)
					, framefinish(0) 
					, visflags(-1)
				{
					memset(name, 0, 32);
				}

				char name[32];
				int framestart;
				int framefinish;
				int visflags;
				int framerate;
			};

			struct Mesh {
				Mesh(ModelFile* model, FILE* file, int size) : flags(0) {
					F_BlockReadLoop(file, size) {
						switch (F_BlockReadHeader(file)) {
							case MAKEID('N', 'A', 'M', 'E'):
								name = F_BlockReadString(file);
								break;
							case MAKEID('F', 'L', 'A', 'G'):
								F_BlockRead(file, flags);
								break;
							case MAKEID('T', 'X', 'I', 'D'): {
									int txid = 0;
									F_BlockRead(file, txid);
									material = model->textures[txid];
									break;
								}
							case MAKEID('F', 'A', 'C', 'E'):
								F_BlockReadVector(file, triangles, Mesh::Triangle);
								break;
#ifdef USE_GPU_SKINNING
							case MAKEID('R', 'V', 'T', 'X'):
								F_BlockReadVector(file, vertices, vertex_t);
								break;
#else 
							case MAKEID('V', 'R', 'T', 'X'):
								F_BlockReadVector(file, vertices, Mesh::Vertex);
								break;
							case MAKEID('V', 'C', 'L', 'R'):
								F_BlockReadVector(file, colors, color_t);
								break;
							case MAKEID('W', 'G', 'H', 'T'):
								F_BlockReadVector(file, weights, Mesh::Weight);
								break;

#endif
							default:
								F_BlockSkip(file);
						}
					}



				}

				shared_ptr<const IMaterial> material;

				std::string name;

				struct Triangle {
					WORD index[3];
				};

				std::vector<Triangle> triangles;

				int flags;

#ifdef USE_GPU_SKINNING
				std::vector<vertex_t> vertices;
				shared_ptr<const IRendererBuffer> vertex_buffer, index_buffer;
#else
				struct Vertex {
					vec2_t st;
					struct Weight { int start, count; } weight;
				};

				struct Weight {
					vec3_t position;
					vec3_t normal;
					int joint;
					float bias;
				};

				std::vector<Vertex> vertices;
				std::vector<Weight> weights;
				std::vector<color_t> colors;
#endif
			};

			class MeshBuffer: public ISurface
			{
			public:
				MeshBuffer(const Mesh& mesh)
					: m_material(mesh.material)
					, m_mesh(&mesh)
					, hidden(false)
#ifndef USE_GPU_SKINNING
					, m_vertices(mesh.vertices.size() + 1) //one extra for shadow target
					, m_indices(mesh.triangles.size() * 3)
#endif
				{
					fillmode = FILL_SOLID;
					primitive = TRIANGLES;

#ifndef USE_GPU_SKINNING
					memcpy((void*)&*m_indices.begin(), (const void*)&*m_mesh->triangles.begin(), 
						GetIndicesCount() * sizeof(index_t));

					for (int i = 0; i < m_mesh->vertices.size(); i++) {
						for (int j = 0; j < vertex_t::TEXCOORDS; j++) {
							m_vertices[i].tc[j] = m_mesh->vertices[i].st;
						}
					}
				}

				const index_t* GetIndices() const { return &*m_indices.begin(); }
				size_t GetIndicesCount() const { return m_indices.size(); }
				const IMaterial* GetMaterial() const { return m_material.get(); }
#else
				}

				const index_t* GetIndices() const { return 0; }
				size_t GetIndicesCount() const { return m_mesh->triangles.size() * 3; }
				const IMaterial* GetMaterial() const { return m_material.get(); }
#endif

				size_t GetVariablesCount() const { return m_variables.size(); }
				const variable_t* GetVariables() const { return &*m_variables.begin(); }
				void ClearVariables() { m_variables.clear(); }
				void SetVariable(VarType vartype, const char* name, void* value, int count) {
					variable_t var;
					memset(&var, sizeof(var), 0);
					var.vartype = vartype;
					var.value = value;
					strcpy(var.name, name);
					var.count = count;
					m_variables.push_back(var);
				}

#ifdef USE_GPU_SKINNING
				const IRendererBuffer* GetVertexBuffer() const { return m_mesh->vertex_buffer.get(); }
				const IRendererBuffer* GetIndexBuffer() const { return m_mesh->index_buffer.get(); }
				const vertex_t* GetVertices() const { return 0; }
#else
				const IRendererBuffer* GetVertexBuffer() const { return 0; }
				const IRendererBuffer* GetIndexBuffer() const { return 0; }
				const vertex_t* GetVertices() const { return &*m_vertices.begin(); }
#endif
				size_t GetVerticesCount() const { return m_mesh->vertices.size(); }
				void SetMaterial(shared_ptr<const IMaterial> material) { m_material = material; }
				const ITexture* GetLightmap() const { return 0; }
				const mat4_t* GetMatrix() const { return &m_matrix; }
				void SetMatrix(const mat4_t& matrix) const { m_matrix = matrix; }
				const Mesh* GetMesh() const { return m_mesh; }

				shared_ptr<const IMaterial> m_material;

				bool hidden;

			private:
				const Mesh* m_mesh;

#ifndef USE_GPU_SKINNING
				std::vector<vertex_t> m_vertices;
				std::vector<index_t> m_indices;
#endif
				std::vector<variable_t> m_variables;

				mutable mat4_t m_matrix;
			};

			std::vector<Mesh> meshes;
			std::vector<Animation> animations;
			std::vector<Bone> bones;
			std::vector<ap_t> frames;
			std::vector<event_t> events;
			std::vector<shared_ptr<const IMaterial> > textures;

			int version;

#ifdef USE_GPU_SKINNING
			std::vector<mat4_t> bindpose, inv_bindpose;
#endif
		};
	}

	class Model
		: public Entity
		, public boost::python::wrapper<Model>
		, public IWrapper
	{
		bool loaded, alpha_processed;

	public:
		Model(
			const std::string& name,
			const vec3_t& position,
			const quat_t& rotation,
			const vec3_t& scale,
			const color_t& color,
			const std::string& source,
			shared_ptr<const IMaterial> material,
			color_t team)
			: Entity(name, position,  rotation, scale, color)
			, model(source.empty() ? null_ptr() : (framework->GetResources()->Load<io::ModelFile>(source)))
			, box_set(false)
			, dirty(false)
			, loaded(false)
			, time(0)
			, alpha_processed(true)
#ifdef USE_PLANARSHADOWS
			, m_bShadow(false)
			, __hack_bShadowIsolated(true)
			, shadow_offset(0)
			, m_bOffsetShadowToFeet(0)
#endif
			, m_bOutline(false)
			, overlay_color(1, 1, 1, 0)
			, hack_every_frame(false)
			, hack_overinterface(false)
			, enable_z_look(false)
			, hack_z(0)
			, host_model(0)
		{
			if (model)
			{
				bones.resize(model->bones.size());
				std::for_each(model->bones.begin(), model->bones.end(), boost::bind(&Model::Collect, this, _1));
				std::for_each(model->meshes.begin(), model->meshes.end(), 
					push_back_vector_t<io::ModelFile::MeshBuffer>(m_surfaces));
				if (model->animations.size() > 0) {
					animLayers->lastAnimation = animLayers->animation = &model->animations[0];
					animLayers->frame0 = animLayers->animation->framestart;
					animLayers->frame1 = animLayers->frame0 + 1;
				}
			}

			if (material)
			{
				SetMaterial(material);
			}

			RenderCallback = (RenderCallbackT)&Model::OnRender;
			AnimateCallback = (AnimateCallbackT)&Model::OnAnimate;
			IntersectionChecker = (IntersectionCheckerT)&Model::CheckIntersection;
			ColorChangedCallback = (ColorChangedCallbackT)&Model::OnColorChanged;
			DebugCallback = (DebugCallbackT)&Model::OnDebug;

			SetTeam(team);
			FillSurfaces();

			lookat.enabled = false;
			lookat.k1 = 0;
			lookat.k2 = 0;
			loaded = true;
		}

	public:
		bool SwitchMesh(const std::string& source, int src_mesh_id, int dst_mesh_id)
		{
			if (src_mesh_id >= m_surfaces.size())
				return false;
			if (shared_ptr<const io::ModelFile> model = framework->GetResources()->Load<io::ModelFile>(source)) {
				if (dst_mesh_id >= model->meshes.size())
					return false;
				m_surfaces[src_mesh_id] = io::ModelFile::MeshBuffer(model->meshes[dst_mesh_id]);
				return true;
			}
			return false;
		}

		void OnColorChanged()
		{
			const color_t color = GetColor();

			BOOST_FOREACH(io::ModelFile::MeshBuffer& surface, m_surfaces)
			{
				SetColor(surface, color);
			}
		}

		void SetColor(io::ModelFile::MeshBuffer& surface, const color_t& color) const
		{
			surface.color = color;

			if (!surface.GetVertices())
			{
				return;
			}

			if (!surface.GetMaterial() || surface.GetMaterial()->surfaceflags & IMaterial::SF_NO_LIGHT)
			{
				for (int n = 0; n < surface.GetMesh()->vertices.size(); 
					((vertex_t*)surface.GetVertices())[n++].color = color);
			}
			else
			{
				for (int n = 0; n < surface.GetMesh()->vertices.size(); 
					((vertex_t*)surface.GetVertices())[n++].color.a = color.a);
			}
		}

		void OnDebug(IHelper* helper) const
		{
			helper->Box(GetBoundingBox(), color_t(1, 1, 0), &GetMatrix());
		}

		void OnAnimate(float timestep)
		{
			time += timestep;

			for (Surfaces::iterator s = m_surfaces.begin(); s != m_surfaces.end(); s->time = time, ++s);

			if (GetFlags(FLAG_NORENDER) || !CanBeSeen())
				return;

			if (hack_every_frame || ApplyAnimation(timestep))
				FillSurfaces();
		}
		mat4_t ComputeShadowMatrix(const Model* self) const 
		{
			mat4_t shadow;

#ifdef USE_PLANARSHADOWS
			float shadow_offset2 = self->shadow_offset * GetMatrix().GetScale().z;

			const light_t *light = framework->GetScene()->GetLight();

			if (!light)
			{
				return mat4_t();
			}

			if (self->m_bOffsetShadowToFeet)
			{
				shadow_offset2 -= self->m_bOffsetShadowToFeet->GetMatrix().GetTranslation().z;
			}

			if (light->fake_shadow_direction.Length() > 0.01f) {
				vec3_t dir = -light->fake_shadow_direction * 10000;
				shadow = mat4_t::Shadow(vec4_t(dir.x, dir.y, dir.z, 1), vec4_t(0, 0, 1, shadow_offset2));
			} else if (light->type != light_t::LIGHT_DIRECTIONAL) {
				vec3_t pos = light->position;
				shadow = mat4_t::Shadow(vec4_t(pos.x, pos.y, pos.z, 0), vec4_t(0, 0, 1, shadow_offset2));
			} else {
				vec3_t dir = -light->direction * 10000;
				shadow = mat4_t::Shadow(vec4_t(dir.x, dir.y, dir.z, 1), vec4_t(0, 0, 1, shadow_offset2));
			}
#endif
			return shadow;
		}
		void OnRender(IClientScene* scene) const
		{
			for (int rendering_shadow = 0; rendering_shadow < 2; ++rendering_shadow)
			{
				bool shadow_rendered = false;
				int meshId = 0;

				BOOST_FOREACH(const io::ModelFile::MeshBuffer& surface, m_surfaces)
				{
					int mesh_id = meshId;
					meshId++;

					if (surface.hidden)
						continue;

					if (animLayers->animation) {
						if ((animLayers->animation->visflags & (1 << mesh_id)) == 0) {
							continue;
						}
					}

					if (animLayers->lastAnimation) {
						if ((animLayers->lastAnimation->visflags & (1 << mesh_id)) == 0) {
							if (animLayers->frame0 < animLayers->animation->framestart || 
								animLayers->frame0 >= animLayers->animation->framefinish) {
								continue;
							}
						}
					}

					bool is_shadow = rendering_shadow || (surface.GetMesh()->flags & 1);

					shadow_rendered |= is_shadow;

					if (is_shadow
#ifdef USE_PLANARSHADOWS
						 && ((!m_bShadow || !__hack_bShadowIsolated) || !framework->GetScene()->GetLight())
#endif
					) {
						continue;
					}

					const Model* self = host_model ? host_model : this;

					((io::ModelFile::MeshBuffer*)&surface)->flags = m_flags;

					surface.SetMatrix(is_shadow ? ComputeShadowMatrix(self) * GetMatrix() : GetMatrix());

					io::ModelFile::MeshBuffer* buffer = (io::ModelFile::MeshBuffer*)&surface;

					buffer->hack_overinterface = hack_overinterface;

#ifdef USE_GPU_SKINNING
					vec4_t overlay_color = self->overlay_color;
					vec4_t team_color(GetTeam());
					vec4_t surface_color(GetColor());
					vec3_t camdir3(framework->GetScene()->GetCamera()->GetDirection());
					vec4_t camdir4 = vec4_t(camdir3.x, camdir3.y, camdir3.z, 0) * mat4_t::Invert(GetMatrix());

					buffer->ClearVariables();
					buffer->SetVariable(ISurface::VARTYPE_MATRIX, "MatrixPalette", (void*)transform, model->bones.size());
					if (!is_shadow)
					{
						buffer->SetVariable(ISurface::VARTYPE_VECTOR4, "OverlayColor", (void*)&overlay_color, 1);
						buffer->SetVariable(ISurface::VARTYPE_VECTOR4, "TeamColor", (void*)&team_color, 1);
					}
					buffer->SetVariable(ISurface::VARTYPE_VECTOR4, "SurfaceColor", (void*)&surface_color, 1);
					buffer->SetVariable(ISurface::VARTYPE_VECTOR4, "CameraDirection", (void*)&camdir4, 1);

					if (const light_t* light = framework->GetScene()->GetLight())
					{
						vec3_t dir;
						float k = 1.0f;

						if (light->type == light_t::LIGHT_POINT)
						{
							dir = GetMatrix().GetTranslation() - light->position;
							float d = dir.Length();
							k = 1.0f / MAX(1.0f, light->attenuation.constant + 
								light->attenuation.linear * d + light->attenuation.quadratic * (d * d));
							dir = vec3_t::Normalize(dir);
						}
						else 
						{
							dir = vec3_t::Normalize(light->direction);
						}

						vec4_t light_ambient(light->ambient);
						vec4_t light_diffuse(light->diffuse * k);
						vec4_t light_direction = vec4_t(dir.x, dir.y, dir.z, 0) * mat4_t::Invert(GetMatrix());
						buffer->SetVariable(ISurface::VARTYPE_VECTOR4, "LightAmbient", (void*)&light_ambient, 1);
						buffer->SetVariable(ISurface::VARTYPE_VECTOR4, "LightDiffuse", (void*)&light_diffuse, 1);
						buffer->SetVariable(ISurface::VARTYPE_VECTOR4, "LightDirection", (void*)&light_direction, 1);
					}
					else
					{
						vec4_t light_ambient(1, 1, 1, 1);
						buffer->SetVariable(ISurface::VARTYPE_VECTOR4, "LightAmbient", (void*)&light_ambient, 1);
					}
#endif

					if (!is_shadow && m_bOutline && outline_material.get()) {
						scene->PushSurface(&surface, outline_material.get());
					}

					scene->PushSurface(&surface, 
#ifdef USE_PLANARSHADOWS
						(is_shadow && shadow_material) ? shadow_material.get() : 
#endif
						0
						);
				}

#ifdef USE_PLANARSHADOWS
				if (shadow_rendered || !shadow_material)
				{
					break;
				}
#endif
			}
		}

		bool CheckIntersection(const math::Line& line) const
		{
			return line.Intersects(m_box, &m_hotspot) && (box_set || Intersection(line, &m_hotspot));
		}

		box3_t cached_box;

		static const int MAX_MATRICES = 64;
		mat4_t transform[MAX_MATRICES];

		void FillSurfaces() 
		{
			BOOST_FOREACH(const Bone& bone, model->bones)
			{
#ifdef USE_GPU_SKINNING
				transform[bone.id] = bones[bone.id].matrix * model->inv_bindpose[bone.id];
#else
				transform[bone.id] = bones[bone.id].matrix;
#endif
			}

#ifndef USE_GPU_SKINNING
			if (!box_set)
				m_box.Reset();

			BOOST_FOREACH(io::ModelFile::MeshBuffer& surface, m_surfaces)
			{
				Fill(&surface);

				if (box_set)
					continue;

				if ((surface.GetMaterial() && !(surface.GetMaterial()->surfaceflags & IMaterial::SF_NO_BBOX)))
				{
					for (const vertex_t* s = 
						(const vertex_t*)surface.GetVertices(), *v = s; v - s < (int)surface.GetVerticesCount(); ++v)
					{
						m_box += v->position;
					}
				}
			}

			cached_box = m_box;
#endif
		}

		void ApplyAnimationLayer(float timestep, int layerId) {
			animLayer_t *layer = &animLayers[layerId];
			
			if (!layer->animation) 
				return;

			layer->framelerp += layer->animation->framerate * timestep;

			for (; layer->framelerp >= 1; layer->framelerp -= 1) {
				if (layer->weight > 0.5f || layerId == 0) {
					for (int i = 0; i < model->events.size(); i++) {
						const event_t &evt = model->events[i];
						if (evt.frame == layer->frame0) {
							AnimationEvent(shared_from_this(), std::string(evt.name));
						} else if (evt.frame > layer->frame0) { 
							break; // they're sorted
						}
					}
				}

				layer->frame0 = layer->frame1;
				layer->frame1++;
				layer->lastAnimation = layer->animation;

				if (layer->frame1 >= layer->animation->framefinish) {
					if (layer->nextAnimation) {
						layer->animation = layer->nextAnimation;
						layer->nextAnimation = 0;
					}
					layer->frame1 = layer->animation->framestart;
					if (layerId == 0) {
						AnimationFinished(shared_from_this(), std::string(layer->animation->name));
					}
				}
			}
		}

		inline bool ApplyAnimation(float timestep = 0) {
			if(!animLayers->animation)
				return false;

			for (int i = 0; i < MAX_ANIM_LAYERS; ApplyAnimationLayer(timestep, i++));

			struct transform {
				const Model *model;
				const animLayer_t *layer;
				transform(const Model* model, int layer)
					: model(model)
					, layer(&model->animLayers[layer])
				{}
				inline void operator()(BoneEx& bone) const {
					const ap_t *f0 = &model->model->frames[layer->frame0 * model->model->bones.size() + bone.id];
					const ap_t *f1 = &model->model->frames[layer->frame1 * model->model->bones.size() + bone.id];
					bone.position = vec3_t::Lerp(layer->framelerp, f0->position, f1->position);
					bone.rotation = quat_t::Lerp(layer->framelerp, f0->rotation, f1->rotation);
					bone.scale = vec3_t::Lerp(layer->framelerp, f0->scale, f1->scale);
				}
			};

			struct blend {
				const Model *model;
				const animLayer_t *layer;
				blend(const Model* model, int layer)
					: model(model)
					, layer(&model->animLayers[layer])
				{}
				inline void operator()(BoneEx& bone) const {
					const ap_t *f0 = &model->model->frames[layer->frame0 * model->model->bones.size() + bone.id];
					const ap_t *f1 = &model->model->frames[layer->frame1 * model->model->bones.size() + bone.id];
					BoneEx tmp;
					tmp.position = vec3_t::Lerp(layer->framelerp, f0->position, f1->position);
					tmp.rotation = quat_t::Lerp(layer->framelerp, f0->rotation, f1->rotation);
					tmp.scale = vec3_t::Lerp(layer->framelerp, f0->scale, f1->scale);
					bone.position = vec3_t::Lerp(layer->weight, bone.position, tmp.position);
					bone.rotation = quat_t::Lerp(layer->weight, bone.rotation, tmp.rotation);
					bone.scale = vec3_t::Lerp(layer->weight, bone.scale, tmp.scale);
				}
			};

			std::for_each(bones.begin(), bones.end(), transform(this, 0));

			for (int i = 1; i < MAX_ANIM_LAYERS; i++) {
				const animLayer_t *layer = &animLayers[i];
				if (!layer->animation || layer->weight < 0.001f) 
					continue;
				std::for_each(bones.begin(), bones.end(), blend(this, i));
			}

			if (bones.size() > 0) {
				bones[0].position.z += hack_z;
			}

			if (over_r.size() || over_p.size()) {
				for (int i = 0; i < bones.size(); i++) {
					BoneEx* bone = &bones[i];
					OverR::const_iterator r = over_r.find(bone->name);
					OverP::const_iterator p = over_p.find(bone->name);
					if (r != over_r.end())
						bone->rotation = r->second;
					if (p != over_p.end())
						bone->position = p->second;
				}
			}
			
			std::for_each(bones.begin(), bones.end(), boost::bind(&BoneEx::Calculate, _1));

			return true;
		}

		virtual bool Intersection(math::Line line, vec3_t *output) const {
			bool result = false;
			BOOST_FOREACH(const io::ModelFile::MeshBuffer& surface, m_surfaces) {
				const vertex_t* vertices = (const vertex_t*)surface.GetVertices();
				BOOST_FOREACH(const io::ModelFile::Mesh::Triangle& triangle, surface.GetMesh()->triangles) {
					if (line.Intersects(
						math::Triangle(
						vertices[triangle.index[0]].position,
						vertices[triangle.index[1]].position,
						vertices[triangle.index[2]].position),
						&line.b)) {
						if (output) {
							result = true, *output = line.b;
						} else {
							return true;
						}
					}
				}
			}

			return result;
		}

		virtual float GetAnimLayerWeight(int layer) const {
			return animLayers[layer].weight;
		}

		virtual void SetAnimLayerWeight(int layer, float weight) {
			if (layer == 0) 
				return;
			animLayers[layer].weight = weight;
		}

		std::string GetAnimationOnLayer(int layerId) const {
			const animLayer_t *layer = &animLayers[layerId];
			return layer->animation ? layer->animation->name : "";
		}

		std::string GetAnimation() const {
			return GetAnimationOnLayer(0);
		}

		bool SetAnimationOnLayer(const std::string& name, int layerId) {
			animLayer_t *layer = &animLayers[layerId];
			if (!layer->nextAnimation && layer->animation && layer->animation->name == name)
				return false;
			if (layer->nextAnimation && layer->nextAnimation->name == name)
				return false;
			for (int i = 0; i < model->animations.size(); i++) {
				if (model->animations[i].name == name) {
					layer->nextAnimation = 0;
					layer->animation = &model->animations[i];
					layer->frame0 = layer->framelerp > 0.5 ? layer->frame1 : layer->frame0;
					layer->frame1 = layer->animation->framestart;
					layer->framelerp = 0;
					return true;
				}
			}
			framework->Log(IFramework::MSG_LOG, "Animation %s was not found", name.c_str());
			return false;
		}
		
		bool SetAnimation(const std::string& name) {
			return SetAnimationOnLayer(name, 0);
		}

		int GetAnimationFrameOnLayer(int layerId) const {
			const animLayer_t *layer = &animLayers[layerId];
			return layer->animation ? layer->frame1 - layer->animation->framestart : 0;
		}

		int GetAnimationFrame() const {
			return GetAnimationFrameOnLayer(0);
		}

		float GetAnimationLength(const std::string& name) const {
			for (int i = 0; i < model->animations.size(); i++) {
				if (model->animations[i].name == name) {
					return (model->animations[i].framefinish - model->animations[i].framestart) / 
						(float)model->animations[i].framerate;
				}
			}
			return 1.f; // TODO: bad default value?
		}

		void SetAnimationThroughOnLayer(const std::string& bridge, const std::string& name, int layerId) {
			animLayer_t *layer = &animLayers[layerId];

			const io::ModelFile::Animation *nextAnimation = 0;
			const io::ModelFile::Animation *animation = 0;

			for (int i = 0; i < model->animations.size(); i++) {
				if (model->animations[i].name == name) {
					nextAnimation = &model->animations[i];
					break;
				}
			}

			for (int i = 0; i < model->animations.size(); i++) {
				if (model->animations[i].name == bridge) {
					animation = &model->animations[i];
					break;
				}
			}

			if (animation && nextAnimation) {
				layer->nextAnimation = nextAnimation;
				layer->animation = animation;
				layer->frame0 = layer->framelerp > 0.5 ? layer->frame1 : layer->frame0;
				layer->frame1 = layer->animation->framestart;
				layer->framelerp = 0;
			} else if (nextAnimation) {
				layer->nextAnimation = 0;
				layer->animation = nextAnimation;
				layer->frame0 = layer->framelerp > 0.5 ? layer->frame1 : layer->frame0;
				layer->frame1 = layer->animation->framestart;
				layer->framelerp = 0;
			} else {
				framework->Log(IFramework::MSG_LOG, "Animation %s was not found", name.c_str());
			}
		}

		void SetAnimationThrough(const std::string& bridge, const std::string& name) {
			SetAnimationThroughOnLayer(bridge, name, 0);
		}

		virtual const box3_t& GetBoundingBox() const
		{
			return m_box;
		}

		void SetBoundingBox(const box3_t& box)
		{
			m_box = box;
			box_set = box.vMin != box.vMax;
		}

		void SetMaterial(shared_ptr<const IMaterial> value)
		{
			if (m_surfaces.size())
			{
				m_surfaces.front().m_material = value;
			}
		}

		shared_ptr<const IMaterial> GetMaterial() const
		{
			return m_surfaces.size() > 0 ? m_surfaces.front().m_material : null_ptr();
		}

		void SetMeshMaterial(int mesh, shared_ptr<const IMaterial> value)
		{
			if (mesh >= m_surfaces.size())
				return;
			m_surfaces[mesh].SetMaterial(value);
		}

		shared_ptr<const IMaterial> GetMeshMaterial(int mesh) const
		{
			if (mesh >= m_surfaces.size())
				return null_ptr();
			return m_surfaces[mesh].m_material;
		}

		void SetTime(float value)
		{
			time = value;
		}

		float GetTime() const
		{
			return time;
		}

		const std::string& GetSource() const {
			static std::string a, b;
			if (!model)
				return a;
			return b = model->GetPath();
		}

		virtual shared_ptr<client::Entity> shared_from_this()
		{
			return boost::python::extract<shared_ptr<client::Entity> >(GetPythonObject());
		}

		virtual shared_ptr<const client::Entity> shared_from_this() const
		{
			return boost::python::extract<shared_ptr<const client::Entity> >(GetPythonObject());
		}

		virtual PyObject* GetPythonObject() const
		{
			return boost::python::detail::wrapper_base_::get_owner(*this);
		}

	protected:
		virtual void Collect(const Bone& bone) {
			BoneEx _bone(bone);
			if (bone.parent != -1) {
				_bone.parentBone = &bones[bone.parent];
			}
			bones[bone.id] = _bone;
		}

	private:
		typedef std::map<std::string, math::Quaternion> OverR;
		typedef std::map<std::string, math::Vector3> OverP;
		OverR over_r;
		OverP over_p;

	public:
		struct lookat_t {
			shared_ptr<const Entity> actor;
			float angle, k1, k2;
			bool in_sight;
			bool enabled;
			vec3_t offset;
		} lookat;

		Event LostSight, GotSight, EmotionChanged;

		void SpineLookAt(shared_ptr<const Entity> actor, float angle, const vec3_t& offset) {
			for (int i = 0; i < spine.size(); i++) {
				spine[i].previous = spine[i].last;
			}
			if (!actor) {
				lookat.enabled = false;
				return;
			}
			lookat.actor = actor;
			lookat.angle = angle;
			lookat.in_sight = true;
			lookat.enabled = true;
			lookat.offset = offset;
			//lookat.k1 = 0;
			lookat.k2 = 0;
			for (int i = 0; i < spine.size(); i++) {
				spine[i].last = spine[i].initial = spine[i].bone->rotation;
			}
		}

		void OverrideBoneRotation(const std::string& name, const math::Quaternion& rotation) {
			over_r[name] = rotation;
			OnAnimate(0);
		}

		void RemoveOverridenBoneRotation(const std::string& name) {
			over_r.erase(name);
			OnAnimate(0);
		}

		void OverrideBonePosition(const std::string& name, const math::Vector3& position) {
			over_p[name] = position;
			OnAnimate(0);
		}

		void RemoveOverridenBonePosition(const std::string& name) {
			over_p.erase(name);
			OnAnimate(0);
		}

		inline vec3_t MulMat3(const mat4_t& m, const vec3_t& v, float bias) {
			float fInvW = 1.0f / ( m.v[0][3] * v.x + m.v[1][3] * v.y + m.v[2][3] * v.z + m.v[3][3] );

			return vec3_t(
				( m.v[0][0] * v.x + m.v[1][0] * v.y + m.v[2][0] * v.z ) * fInvW,
				( m.v[0][1] * v.x + m.v[1][1] * v.y + m.v[2][1] * v.z ) * fInvW,
				( m.v[0][2] * v.x + m.v[1][2] * v.y + m.v[2][2] * v.z ) * fInvW) * bias;
		}

		inline vec3_t MulMat4(const mat4_t& m, const vec3_t& v, float bias) {
			float fInvW = 1.0f / ( m.v[0][3] * v.x + m.v[1][3] * v.y + m.v[2][3] * v.z + m.v[3][3] );

			return vec3_t(
				( m.v[0][0] * v.x + m.v[1][0] * v.y + m.v[2][0] * v.z + m.v[3][0] ) * fInvW,
				( m.v[0][1] * v.x + m.v[1][1] * v.y + m.v[2][1] * v.z + m.v[3][1] ) * fInvW,
				( m.v[0][2] * v.x + m.v[1][2] * v.y + m.v[2][2] * v.z + m.v[3][2] ) * fInvW) * bias;
		}

		inline bool MulNormalVec3(const vec3_t& a, const vec3_t& b, const vec3_t& c, const vec3_t& light) {
			const vec3_t _c = (a - b).Cross(a - c);
			return (_c.x * light.x + _c.y * light.y + _c.y * light.y) > 0;
		}

		/**
			CPU SKINNING routine
		**/

		void Fill(io::ModelFile::MeshBuffer* surface)
		{
			const color_t color = GetColor();

			vertex_t* output = (vertex_t*)surface->GetVertices();

			if (!surface->GetMaterial() || surface->GetMaterial()->surfaceflags & IMaterial::SF_NO_LIGHT)
			{
				surface->light = 0;
			}
			else
			{
				surface->light = framework->GetScene()->GetLight(); 
			}

			const io::ModelFile::Mesh* mesh = surface->GetMesh();

			surface->color = color;
			surface->pivot = m_pivot;

			color_t color_;
			color_.a = color.a;

			if (!surface->light)
			{
				color_ = color;
			}

#ifndef USE_GPU_SKINNING
			const color_t* c  = mesh->colors.size() ? &mesh->colors.front() : 0;
			const io::ModelFile::Mesh::Vertex* v = mesh->vertices.size() ? &mesh->vertices.front() : 0;
			if (color.a < 255 || !alpha_processed || !surface->light)
			{
				for (int n = 0; n < surface->GetMesh()->vertices.size(); ++n, ++v, ++output) {
					const io::ModelFile::Mesh::Weight *weight = &mesh->weights[v->weight.start];

					output->position = MulMat4(transform[weight->joint], weight->position, weight->bias);
					output->normal = MulMat3(transform[weight->joint], weight->normal, weight->bias);

					for (int j = 1; ++weight && j < v->weight.count; ++j) {
						output->position += MulMat4(transform[weight->joint], weight->position, weight->bias);
						output->normal += MulMat3(transform[weight->joint], weight->normal, weight->bias);
					}

					if (c) {
						output->color = color_ * *(c++);
					} else {
						output->color = color_;
					}
				}

				alpha_processed = (color.a == 255);
			}
			else
			{
				for (int n = 0; n < surface->GetMesh()->vertices.size(); ++n, ++v, ++output) {
					const io::ModelFile::Mesh::Weight *weight = &mesh->weights[v->weight.start];

					output->position = MulMat4(transform[weight->joint], weight->position, weight->bias);
					output->normal = MulMat3(transform[weight->joint], weight->normal, weight->bias);

					for (int j = 1; ++weight && j < v->weight.count; ++j) {
						output->position += MulMat4(transform[weight->joint], weight->position, weight->bias);
						output->normal += MulMat3(transform[weight->joint], weight->normal, weight->bias);
					}

					if (c) {
						output->color = *(c++);
					}
				}
			}

#endif 
		}

		void ToggleMesh(int num)
		{
			if (num >= m_surfaces.size())
				return;
			m_surfaces[num].hidden = !m_surfaces[num].hidden;
		}

		void ShowMesh(int num, bool value)
		{
			if (num >= m_surfaces.size())
				return;
			m_surfaces[num].hidden = !value;
		}

		std::string GetMeshName(int num)
		{
			if (num >= m_surfaces.size())
				return std::string();
			return m_surfaces[num].GetMesh()->name;
		}

		int GetMeshCount()
		{
			return m_surfaces.size();
		}

		void ReplaceModel(const std::string& source)
		{
			model = source.empty() ? null_ptr() : (framework->GetResources()->Load<io::ModelFile>(source));
			m_surfaces.clear();
			std::for_each(model->meshes.begin(), model->meshes.end(), push_back_vector_t<io::ModelFile::MeshBuffer>(m_surfaces));
			FillSurfaces();
		}

		void AttachToBone(shared_ptr<Entity> ent, const std::string& name) {
			int hash = BuildHash(name.c_str());
			ent->overrideParent = 0;
			for (int i = 0; i < bones.size(); i++) {
				if (bones[i].namehash == hash) {
					ent->overrideParent = &bones[i].matrix;
				}
			}
		}

	public:
		bool GetOffsetShadowToFeet() const { return m_bOffsetShadowToFeet == this; }
		void SetOffsetShadowToFeet(bool value) { m_bOffsetShadowToFeet = value ? this : 0; }

	public:
		struct animLayer_t {
			animLayer_t() 
				: animation(0)
				, nextAnimation(0)
				, lastAnimation(0)
				, weight(0)
				, framelerp(0)
				, frame0(0)
				, frame1(1)
			{}
			const io::ModelFile::Animation *animation;
			const io::ModelFile::Animation *nextAnimation;
			const io::ModelFile::Animation *lastAnimation;
			int frame0, frame1;
			float framelerp;
			float weight;
		};
		animLayer_t animLayers[MAX_ANIM_LAYERS];

		bool hack_every_frame, hack_overinterface, enable_z_look;
		const Entity* m_bOffsetShadowToFeet;
		const Model* host_model;
		float hack_z;
		color_t overlay_color;

		Event AnimationFinished;
		Event AnimationEvent;

		bool m_bOutline;
		shared_ptr<const IMaterial> outline_material;

#ifdef USE_PLANARSHADOWS
		bool m_bShadow, __hack_bShadowIsolated;
		shared_ptr<const IMaterial> shadow_material;
		float shadow_offset;
#ifdef USE_GPU_SKINNING
		void SetShadowMesh(const std::string& source) { 
			if (shadow_model || !(shadow_model = framework->GetResources()->Load<io::ModelFile>(source))) {
				return;
			}
			
			std::for_each(shadow_model->meshes.begin(), shadow_model->meshes.end(), 
				push_back_vector_t<io::ModelFile::MeshBuffer>(m_surfaces));
		}
		shared_ptr<const io::ModelFile> shadow_model;
#endif
#endif

	private:
		std::vector<BoneEx> bones;

		bool dirty;

		typedef std::vector<io::ModelFile::MeshBuffer> Surfaces;

		Surfaces m_surfaces;

		shared_ptr<const ITexture> proxy;
		shared_ptr<const io::ModelFile> model;

		box3_t m_box;

		struct spine_t {
			quat_t initial, last, previous;
			BoneEx* bone;
		};

		std::vector<spine_t> spine;

		float time;

		std::string last_emotion;

		bool box_set;
	};
}

namespace boost
{
	template<class T>
	inline T* get_pointer(shared_ptr<const T> const& p)
	{
		return const_cast<T*>(p.get());
	}

	namespace python
	{
		template<class T>
		struct pointee<shared_ptr<T const> >
		{
			typedef T type;
		};
	}
}

void export_Model()
{
	namespace py = boost::python;

	using client::Model;

	py::scope _e = py::class_<Model, shared_ptr<Model>, py::bases<client::Entity>, boost::noncopyable>("Model", py::no_init)
		.def(py::init<std::string, vec3_t, quat_t, vec3_t, color_t, std::string, shared_ptr<const IMaterial>, color_t>((
			py::arg("id") = std::string("Model#"),
			py::arg("position") = vec3_t::Zero,
			py::arg("rotation") = quat_t::Identity,
			py::arg("scale") = vec3_t::One,
			py::arg("color") = color_t(),
			py::arg("source") = std::string(),
			py::arg("material") = shared_ptr<const IMaterial>(),
			py::arg("team") = color_t())))
		.add_property("source", py_cref(&Model::GetSource))
		.add_property("material", &Model::GetMaterial, &Model::SetMaterial)
		.add_property("time", &Model::GetTime, &Model::SetTime)
		.add_property("bbox", py_cref(&Model::GetBoundingBox), &Model::SetBoundingBox)
		.def_readwrite("GotSight", &Model::GotSight)
		.def_readwrite("LostSight", &Model::LostSight)
		.def_readwrite("EmotionChanged", &Model::EmotionChanged)
		.def_readwrite("overlay", &Model::overlay_color)
		.def_readwrite("enable_z_look", &Model::enable_z_look)
		.def_readwrite("hack_every_frame", &Model::hack_every_frame)
		.def_readwrite("hack_z", &Model::hack_z)
		.def_readwrite("host_model", &Model::host_model)
		.def_readwrite("outline", &Model::m_bOutline)
		.def_readwrite("outlinematerial", &Model::outline_material)
#ifdef USE_PLANARSHADOWS
		.def_readwrite("castshadow", &Model::m_bShadow)
		.def_readwrite("castshadow_isolated", &Model::__hack_bShadowIsolated)
		.def_readwrite("shadowmaterial", &Model::shadow_material)
		.def_readwrite("shadowoffset", &Model::shadow_offset)
#ifdef USE_GPU_SKINNING
		.def("SetShadowMesh", &Model::SetShadowMesh)
#endif
		.add_property("shadowatfeet", &Model::GetOffsetShadowToFeet, &Model::SetOffsetShadowToFeet)
#endif
		.def_readwrite("AnimationFinished", &Model::AnimationFinished)
		.def_readwrite("AnimationEvent", &Model::AnimationEvent)
		.def_readwrite("hack_overinterface", &Model::hack_overinterface)
		.add_property("animation", &Model::GetAnimation, &Model::SetAnimation)
		.add_property("frame", &Model::GetAnimationFrame)
		.def("SetAnimation", &Model::SetAnimation)
		.def("SetAnimationOnLayer", &Model::SetAnimationOnLayer)
		.def("SetMeshMaterial", &Model::SetMeshMaterial)
		.def("GetMeshMaterial", &Model::GetMeshMaterial)
		.def("SetAnimationThrough", &Model::SetAnimationThrough)
		.def("FillSurfaces", &Model::FillSurfaces)
		.def("OverrideBoneRotation", &Model::OverrideBoneRotation)
		.def("RemoveOverridenBoneRotation", &Model::RemoveOverridenBoneRotation)
		.def("OverrideBonePosition", &Model::OverrideBonePosition)
		.def("RemoveOverridenBonePosition", &Model::RemoveOverridenBonePosition)
		.def("SwitchMesh", &Model::SwitchMesh)
		.def("ToggleMesh", &Model::ToggleMesh)
		.def("SpineLookAt", &Model::SpineLookAt, (
			py::arg("target"),
			py::arg("angle") = 90,
			py::arg("offset") = vec3_t()))
		.def("ReplaceModel", &Model::ReplaceModel)
		.def("GetMeshName", &Model::GetMeshName)
		.def("ShowMesh", &Model::ShowMesh)
		.def("GetMeshCount", &Model::GetMeshCount)
		.def("AttachToBone", &Model::AttachToBone)
		.def("Animate", &Model::OnAnimate)
		.def("GetAnimationLength", &Model::GetAnimationLength)
		;

	py::implicitly_convertible<shared_ptr<Model>, shared_ptr<client::Entity> >();

	DISPOSEME(4) _e;
}
