/**
OVERLAY is an object which draws 2D primitives and text
WIDGET class is inherited from it
**/

#include "c_client.hpp"
#include "c_overlay.hpp"
#include "c_material.hpp"
#include "../interfaces/imaterial.hpp"

#include <numeric>

void export_Overlay()
{
	namespace py = boost::python;

	/*py::scope scope = py::class_<IOverlay, shared_ptr<IOverlay>, boost::noncopyable>("Overlay", boost::python::no_init)
	.def("DrawImage", &IOverlay::Quad)
	.def("DrawString", &IOverlay::String)
	;
	*/
	py::enum_<IOverlay::Align>("Align")
		.value("Inherit", IOverlay::ALIGN_INHERIT)
		.value("Left", IOverlay::ALIGN_LEFT)
		.value("Right", IOverlay::ALIGN_RIGHT)
		.value("Center", IOverlay::ALIGN_CENTER)
		.export_values()
		;
}

namespace client
{
	Overlay::Overlay()
	{
		m_fonts["system"] = framework->GetResources()->Load<Font2>("system"); /*system font*/
	}

	Overlay::~Overlay()
	{
	}

	void Overlay::Render(IRenderer* renderer) const
	{
		struct render_surface
		{
			IRenderer* renderer;
			render_surface(IRenderer* renderer): renderer(renderer) {}
			inline void operator()(const surface::Mesh& surface) const
			{
				if (surface.GetMaterial())
				{
					IMaterial::DepthFunction original = surface.GetMaterial()->depthfunc;
					((Material*)&*surface.GetMaterial())->depthfunc.func = IMaterial::FUNC_UNKNOWN;
					((Material*)&*surface.GetMaterial())->depthfunc.mask = false;
					renderer->PushSurface(&surface);
					((Material*)&*surface.GetMaterial())->depthfunc = original;
				}
				else
				{
					renderer->PushSurface(&surface);
				}
			}
		};
		std::for_each(m_surfaces.begin(), m_surfaces.end(), render_surface(renderer));
	}

	void Overlay::Erase()
	{
		m_surfaces.clear();
	}

	const ISurface* Overlay::Quad(
		shared_ptr<const IMaterial> material,
		const box2_t& screen,
		const box2_t& uv,
		const color_t& color,
		const mat4_t *matrix)
	{
		m_surfaces.push_back(surface::Mesh());

		surface::Mesh& s = m_surfaces.back();

		s.AddQuad(box2_t(0, 0, 1, 1), box2_t(uv.vMin, uv.vMax));
		s.SetColor(color);

		s.material = material;
		
		mat4_t mymatrix;

		mymatrix *= mat4_t::Scaling(screen.vMax.x - screen.vMin.x, screen.vMax.y - screen.vMin.y, 0);
		mymatrix.SetTranslation(screen.vMin.x, screen.vMin.y, 0);
		mymatrix = matrix ? ((*matrix) * mymatrix) : mymatrix;

		for (int i = 0; i < s.m_vertices.size(); s.m_vertices[i++].position *= mymatrix);

		s.use_matrix = false;

		return &s;
	}

	int Overlay::GetLineWidth(const std::wstring& text, const std::string& fontname, int size, int max_length, int letter_spacing) const {
		Overlay* self = (Overlay*)this;
		shared_ptr<const Font2> font = self->GetFont(fontname);
		if (size <= 0)
			size = font->info.size;
		int length = 0;
		int max = -1;
		int last_space_length = 0;
		int last_space_pos = -1;
		float k = (float)size / (float)font->info.size;
		max_length = max_length <= 0 ? INT_MAX : max_length;
		for (int i = 0; i < text.size(); i++) {
			if (text[i] == 1)
				continue;
			if (text[i] == L'~') {
				if (text[i + 1]) 
					i++;
				continue;
			}
			if (text[i] == L'\n') {
				max = max > length ? max : length;
				length = 0;
				last_space_length = 0;
				last_space_pos = -1;
				continue;
			}
			if (text[i] == L' ') {
				last_space_length = length;
				last_space_pos = i;
			}

			int clen = ceil((font->GetChar(text[i]).xadvance + font->info.outline) * k) + letter_spacing;

			if (length + clen > max_length) {
				if (last_space_pos > 0) {
					i = last_space_pos;
					max = max > last_space_length ? max : last_space_length;
					length = 0;
				} else {
					max = max > length ? max : length;
					length = 0;
					i--;
				}
				last_space_length = 0;
				last_space_pos = -1;
			} else {
				length += clen;
			}
		}
		return (length > max ? length : max);
	}

	/**
	STRING renderer supports forced and automatic new-line when reaches boundaries
	ITALIC is supported (use '~i')
	**/

	const ISurface* Overlay::String(
		const std::wstring& text,
		const mat4_t& matrix,
		const std::string& fontname,
		const ColorPair& color,
		const ColorPair& nHiglightColor,
		const TextDimensions& td,
		dim_t* out_size)
	{
		if (text.length() == 0 || fontname.length() == 0)
			return 0;

		shared_ptr<const Font2> font = GetFont(fontname);

		if (!font)
			return 0;

		std::list<std::wstring> lines;
		boost::split(lines, text, boost::is_any_of(L"\n\r"));

		m_surfaces.push_back(surface::Mesh());

		surface::Mesh* mesh = &m_surfaces.back();

		struct state_t {
			shared_ptr<const Font2> font;
			ColorPair color, super_color, *use_this_color;
			bool italic, bold;
			float k, shuffle;
			int number, letter_spacing;
		};

		struct print_line {
			print_line(surface::Mesh* out, state_t* state, int width, int line_height, Align align)
				: count(0)
				, out(out)
				, width(width)
				, align(align)
				, line_height(line_height)
				, state(state)
			{
			}

			inline size_t length(const std::wstring& line, bool bold) {
				int length = 0;
				for (int i = 0; i < line.size(); i++) {
					if (line[i] == 1)
						continue;
					if (line[i] == L'~') {
						if (line[i + 1]) 
							i++;
						if (line[i] == L'b')
							bold = !bold;
						continue;
					}
					length += ceil((state->font->GetChar(line[i]).xadvance + state->font->info.outline) * state->k) + bold + state->letter_spacing;
				}
				return length;
			}

			inline void operator()(const std::wstring& line) {
				struct print_char
				{
					bool wait_for_color;
					print_char(float x, float y, surface::Mesh* out, state_t* state)
						: cursor(x, y), out(out), state(state), wait_for_color(false)
					{}
					inline void operator()(unsigned short _c) {
						if (_c == 1) {
							state->color.top.a = 0;
							state->color.bottom.a = 0;
							state->super_color.top.a = 0;
							state->super_color.bottom.a = 0;
							return;
						} else if (wait_for_color) {
							wait_for_color = false;
							int _cl = 0;
							if (_c == L'i') {
								state->italic = !state->italic;
								return;
							} else if (_c == L'b') {
								state->bold = !state->bold;
								return;
							} else if (_c == L'c') {
								if (state->use_this_color == &state->color) {
									state->use_this_color = &state->super_color;
								} else {
									state->use_this_color = &state->color;
								}
							}
							return;
						} else if (_c == L'~') {
							wait_for_color = true;
							return;
						}

						unsigned short c = *(unsigned short*)&_c;
						
						unsigned
							width = state->font->common.scaleW,
							height = state->font->common.scaleH;

						const Font2::font_char_t& chr = state->font->GetChar(c);

						const box2_t dim_screen(
							cursor.x + chr.xoffset * state->k, 
							cursor.y + chr.yoffset * state->k,
							cursor.x + (chr.xoffset + chr.width) * state->k, 
							cursor.y + (chr.yoffset + chr.height) * state->k);

						const box2_t dim_uv = chr.uv;

						out->AddQuad(dim_screen, dim_uv, color_t());
						
						vertex_t* v = &out->m_vertices[out->m_vertices.size() - 4];
						
						v[3].color = state->use_this_color->bottom;
						v[2].color = state->use_this_color->bottom;
						v[1].color = state->use_this_color->top;
						v[0].color = state->use_this_color->top;

						float yoffset = state->font->info.yoffset * state->k;

						v[3].position.y += yoffset;
						v[2].position.y += yoffset;
						v[1].position.y += yoffset;
						v[0].position.y += yoffset;

						if (fabs(state->shuffle) > 0.001f) {
							float offset = state->font->info.size * state->k * state->shuffle;

							if (state->number % 2) {
								v[3].position.y += offset;
								v[2].position.y += offset;
								v[1].position.y += offset;
								v[0].position.y += offset;
							} else {
								v[3].position.y -= offset;
								v[2].position.y -= offset;
								v[1].position.y -= offset;
								v[0].position.y -= offset;
							}
						}

						if (state->italic) {
							float offset = state->font->info.size * state->k * 0.1;
							v[3].position.x -= offset;
							v[2].position.x -= offset;
							v[1].position.x += offset;
							v[0].position.x += offset;
						}
						
						if (state->bold) {
							out->AddQuad(dim_screen, dim_uv, color_t());

							vertex_t
								*v1 = &out->m_vertices[out->m_vertices.size() - 8],
								*v2 = &out->m_vertices[out->m_vertices.size() - 4];

							for (int i = 0; i < 4; ++i, ++v1, ++v2) {
								*v2 = *v1;
								v2->position.x += 1.0f;
							}

							cursor.x += 1;
						}
						state->number++;
						cursor.x += ceil((state->font->GetChar(_c).xadvance + state->font->info.outline) * state->k) + state->letter_spacing;
					}
					vec2_t cursor;
					surface::Mesh* out;
					state_t* state;
				};

				int start = 0, len = 0;

				switch (align) {
				case ALIGN_RIGHT:
					len = length(line, state->bold);
					if (len < width)
						start = width - len;
					break;
				case ALIGN_CENTER: 
					len = length(line, state->bold);
					if (len < width)
						start = (width - len) / 2; 
					break;
				default: 
					start = 0;
				}

				int char_height = line_height != 0 ? line_height : (state->font->common.lineHeight * state->k);
				std::for_each(line.begin(), line.end(), print_char((float)start, (float)count * char_height, out, state));
				++count;
			}
			int count, width, line_height;
			Align align;
			surface::Mesh* out;
			state_t* state;
		};

		state_t state = { 
			GetFont(fontname), // font
			color, nHiglightColor, 0, // colors
			false, false, // italic, bold
			td.size > 0 ? ((float)td.size / (float)font->info.size) : 1.0f, // k (size)
			td.shuffle, // shuffle amount
			0,  // char number
			td.letter_spacing, //letter_spacing
		};

		state.use_this_color = &state.color;

		int lines_count = 0;

		if (td.max_length > 0 && td.whitespace != WHITESPACE_NOWRAP) {
			struct split {
				int spacing;
				split(shared_ptr<const client::Overlay::Font2> font, unsigned int width, std::list<std::wstring>* lines, float k, int spacing)
					: font(font)
					, width(width)
					, lines(lines)
					, k(k)
					, spacing(spacing)
				{
				}
				inline size_t length(const std::wstring& line) {
					int length = 0;
					for (int i = 0; i < line.size(); i++) {
						if (line[i] == 1)
							continue;
						if (line[i] == L'~') {
							if (line[i + 1]) 
								i++;
							continue;
						}
						length += ceil((font->GetChar(line[i]).xadvance + font->info.outline) * k) + spacing;
					}
					return length;
				}
				inline void operator()(const std::wstring& line) {
					struct join {
						join(split* splitter, bool insert_space): splitter(splitter), insert_space(insert_space) {}
						inline void operator()(const std::wstring& word) {
							std::wstring& line = splitter->lines->back();
							const std::wstring target = line + (insert_space && line.length() > 0 ? L" " : L"") + word;
							if (splitter->length(target) > splitter->width) {
								splitter->lines->push_back(word);
							} else {
								line = target;
							}
						}
					private:
						split* splitter;
						bool insert_space;
					};
					std::list<std::wstring> words;
					boost::split(words, line, boost::is_space());
					bool insert_space = true;
					// JAPANESE LANGUAGE SUPPORT
					const int ONE_WORD_SENTENCE = 10;
					const int ONE_WORD_SENTENCE_TAIL = 5;
					if (words.size() <= 1 && line.size() > ONE_WORD_SENTENCE) {
						words.clear();
						for (int i = 0; i < line.size() - ONE_WORD_SENTENCE_TAIL; i++) {
							words.push_back(std::wstring(&line[i], 1));
						}
						words.push_back(std::wstring(&line[line.size() - ONE_WORD_SENTENCE_TAIL], ONE_WORD_SENTENCE_TAIL));
						insert_space = false;
					}
					// END OF JAPANESE LANGUAGE SUPPORT
					lines->push_back(std::wstring());
					std::for_each(words.begin(), words.end(), join(this, insert_space));
				}
				float k;
				std::list<std::wstring>* lines;
				shared_ptr<const client::Overlay::Font2> font;
				unsigned int width;
			};
			std::list<std::wstring> formatted_lines;
			std::for_each(lines.begin(), lines.end(), split(GetFont(fontname), td.max_length, &formatted_lines, state.k, state.letter_spacing));
			std::for_each(formatted_lines.begin(), formatted_lines.end(), print_line(mesh, &state, td.max_length, td.line_height, td.align));

			lines_count = formatted_lines.size();
		} else {
			std::for_each(lines.begin(), lines.end(), print_line(mesh, &state, td.max_length, td.line_height, td.align));

			lines_count = lines.size();
		}

		float _outwidth = GetLineWidth(text, fontname, td.size, td.whitespace == WHITESPACE_NOWRAP ? 0 : td.max_length, td.letter_spacing);

		if (td.max_length > 0 && td.whitespace == WHITESPACE_NOWRAP && _outwidth > td.max_length) {
			float k = td.max_length / _outwidth;

			_outwidth = td.max_length;

			for (int i = 0; i < mesh->m_vertices.size(); mesh->m_vertices[i++].position.x *= k);
		}

		if (out_size) {
			//const vertex_t
			//	*a = mesh->GetVertices(),
			//	*b = mesh->GetVertices() + mesh->GetVerticesCount();
			//out_size->height =(int)*std::max_element(
			//	make_transform_iterator(a, bind(&vec3_t::y, bind(&vertex_t::position, _1))),
			//	make_transform_iterator(b, bind(&vec3_t::y, bind(&vertex_t::position, _1))));
			out_size->height = lines_count * (td.line_height != 0 ? td.line_height : (font->common.lineHeight * state.k));
			out_size->width = _outwidth;
		}

		for (int i = 0; i < mesh->m_vertices.size(); mesh->m_vertices[i++].position *= matrix);

		mesh->material = GetFont(fontname)->GetPage(0);
		mesh->use_matrix = false;

		return mesh;
	}

	Overlay::Font2::Font2(const std::string& filename)
	{
		memset(font_chars, 0, sizeof(font_char_t) * max_chars);

		file_t file(framework->GetFileSystem()->LoadFile((std::string("fonts/") + filename) + ".fnt"));

		if (!file)
			return;

		using namespace tinyxml2;

		tinyxml2::XMLDocument doc;

		doc.LoadFile(file);

		XMLElement *pFont = doc.FirstChildElement("font");
		XMLElement *pInfo = pFont->FirstChildElement("info");
		XMLElement *pCommon = pFont->FirstChildElement("common");
		XMLElement *pChars = pFont->FirstChildElement("chars");
		XMLElement *pPages = pFont->FirstChildElement("pages");

		info_t info = { 32, 0, 0 };
		common_t common = { 32, 32, 256, 256 };

		pInfo->QueryIntAttribute("size", &info.size);
		pInfo->QueryIntAttribute("outline", &info.outline);
		pInfo->QueryIntAttribute("yoffset", &info.yoffset);

		pCommon->QueryIntAttribute("lineHeight", &common.lineHeight);
		pCommon->QueryIntAttribute("base", &common.base);
		pCommon->QueryIntAttribute("scaleW", &common.scaleW);
		pCommon->QueryIntAttribute("scaleH", &common.scaleH);

		if (info.size < 0) 
			info.size = -info.size;


		XML_FOREACH(xmlChar, "char", pChars) {
			font_char_t chr = {
				xmlChar->IntAttribute("id"),
				(short)xmlChar->IntAttribute("x"),
				(short)xmlChar->IntAttribute("y"),
				(short)xmlChar->IntAttribute("width"),
				(short)xmlChar->IntAttribute("height"),
				(short)xmlChar->IntAttribute("xoffset"),
				(short)xmlChar->IntAttribute("yoffset"),
				(short)xmlChar->IntAttribute("xadvance"),
				(short)xmlChar->IntAttribute("page"),
				(short)xmlChar->IntAttribute("chnl"),
			};

			if (chr.id >= max_chars) {
				framework->Log(IFramework::MSG_WARNING, "Character %i won't be loaded", chr.id);
				continue;
			}

			chr.uv = box2_t(
				(float)chr.x / (float)common.scaleW, 
				(float)chr.y / (float)common.scaleH, 
				(float)(chr.x + chr.width) / (float)common.scaleW, 
				(float)(chr.y + chr.height) / (float)common.scaleH); 
			
			font_chars[chr.id] = chr;
		}

		XML_FOREACH(xmlPage, "page", pPages) {
			font_pages[xmlPage->IntAttribute("id")] = 
				framework->GetResources()->Load<Material>(std::string("fonts/") + 
					f_StripExtension(xmlPage->Attribute("file")));
		}
		
		this->info = info;
		this->common = common;

		file.close();
	}

	shared_ptr<const Overlay::Font2> Overlay::GetFont(const std::string& name) const
	{
		if (!m_fonts[name])
		{
			m_fonts[name] = framework->GetResources()->Load<Font2>(name);
		}

		return m_fonts[name];
	}


}