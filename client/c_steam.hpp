#pragma once

#include <string>

namespace Steam {
	void init();
	void shutdown();
	std::string getUserName();
	void setAchievement(const std::string &ach);
	void clearAchievement(const std::string &ach);
	std::string getLanguage();
	void runCallbacks();
	void showOverlay(int id);
	void showWebPage(int id);
	bool isOverlaysEnabled();
}