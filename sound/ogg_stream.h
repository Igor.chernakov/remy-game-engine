#ifndef __ogg_h__
#define __ogg_h__

#include <string>
#include <iostream>
using namespace std;

#include "../openal/include/al.h"
#include "../oggvorbis/vorbis/vorbisfile.h"

#include "../interfaces/iclientframework.hpp"
#include "../shared/resource.hpp"



#define BUFFER_SIZE (4096 * 10)



class ogg_stream
{
public:

	void open(string path);
	void release();
	void display();
	void rewind();
	bool playback(float volume);
	bool playing();
	bool update();

	bool finished() const;

protected:

	bool stream(ALuint buffer);
	void empty();
	void check();
	string errorString(int code);

private:

	FILE*           oggFile;
	OggVorbis_File  oggStream;
	vorbis_info*    vorbisInfo;
	vorbis_comment* vorbisComment;

	bool is_finished;

	ALuint buffers[2];
	ALenum format;

public:
	ALuint source;
};


#endif // __ogg_h__
