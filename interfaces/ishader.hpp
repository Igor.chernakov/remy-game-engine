#pragma once

#include "iobject.hpp"
#include "../math/math.hpp"

struct IShader: IRendererObject 
{
	IShader()
		: vertex(0)
		, pixel(0)
		, constants(0)
		, program(0)
		, constants2(0)
	{
	}

	unsigned int constants, vertex, pixel, program, constants2;
};

struct IShaderManager: IObject
{
	virtual boost::shared_ptr<const IShader> LoadShader(const std::string& filename) = 0;
	virtual void Update(struct IRenderer* renderer) = 0;
	virtual void Unload(struct IRenderer* renderer) = 0;
	virtual void Clear(struct IRenderer* renderer) = 0;
};
