#include "math.hpp"
#include <sstream>

using namespace math;

const Vector3 Vector3::Zero(0,0,0);

const Vector3 Vector3::One(1,1,1);

Vector3::Vector3()
{
	Assign(0, 0, 0);
}
Vector3::Vector3(Real x, Real y, Real z)
{
	Assign(x, y, z);
}
Vector3::Vector3(const Real* param)
{
	Assign(param[0], param[1], param[2]);
}
Vector3::Vector3(const Vector3& param)
{
	Assign(param[0], param[1], param[2]);
}

Vector3::Vector3(const std::string& param)
{
	std::stringstream(param) >> *this;
}

Vector3& Vector3::Assign(Real x, Real y, Real z)
{
	v[0]=x, v[1]=y, v[2]=z;

	return *this;
}

bool Vector3::Equal(const Vector3& left, const Vector3& right)
{
	return fabs(left.x - right.x) < EPS && fabs(left.y - right.y) < EPS && fabs(left.z - right.z) < EPS;
}

Vector3 Vector3::Add(const Vector3& left, const Vector3& right)
{
	return Vector3(left.x + right.x, left.y + right.y, left.z + right.z);
}

Vector3 Vector3::Substract(const Vector3& left, const Vector3& right)
{
	return Vector3(left.x - right.x, left.y - right.y, left.z - right.z);
}

Vector3 Vector3::Multiply(const Vector3& left, const Vector3& right)
{
	return Vector3(left.x * right.x, left.y * right.y, left.z * right.z);
}

Vector3 Vector3::Divide(const Vector3& left, const Vector3& right)
{
	return Vector3(left.x / right.x, left.y / right.y, left.z / right.z);
}

Vector3 Vector3::Scale(const Vector3& param, Real d)
{
	return Vector3(param.x * d, param.y * d, param.z * d);
}

Vector3 Vector3::Negate(const Vector3& param)
{
	return Vector3(-param.x, -param.y, -param.z);
}

Vector3 Vector3::Normalize(const Vector3& v)
{
	Real length = Length(v);

	if (length > 0)
	{
		return v / length;
	}
	else
	{
		return v;
	}
}

Real Vector3::Length(const Vector3& param)
{
	return (Real)sqrt((param.x * param.x) + (param.y * param.y) + (param.z * param.z));
}

Vector3& Vector3::Maximize()
{
	v[0] = FLT_MAX;

	v[1] = FLT_MAX;

	v[2] = FLT_MAX;

	return *this;
}

Vector3& Vector3::Minimize()
{
	v[0] = -FLT_MAX;

	v[1] = -FLT_MAX;

	v[2] = -FLT_MAX;

	return *this;
}

Vector3 Vector3::Cross(const Vector3& left, const Vector3& right)
{
	return Vector3(

		(left.v[1] * right.v[2]) - (left.v[2] * right.v[1]),

		(left.v[2] * right.v[0]) - (left.v[0] * right.v[2]),

		(left.v[0] * right.v[1]) - (left.v[1] * right.v[0]));
}

Vector3& Vector3::Cross(const Vector3& param)
{
	*this = Cross(*this, param);

	return *this;
}

Real Vector3::Dot(const Vector3& left, const Vector3& right)
{
	return left.x * right.x + left.y * right.y + left.z * right.z;
}

Vector3 Vector3::Lerp(Real d, const Vector3& left, const Vector3& right)
{
	return left * (1 - d) + right * d;
}

//Matrix operations

Vector3 Vector3::Multiply(const Vector3& v, const Matrix& m)
{
	Vector4 r;

	Real fInvW = 1.0f / ( m.v[0][3] * v.x + m.v[1][3] * v.y + m.v[2][3] * v.z + m.v[3][3] );

	r.x = ( m.v[0][0] * v.x + m.v[1][0] * v.y + m.v[2][0] * v.z + m.v[3][0] ) * fInvW;
	r.y = ( m.v[0][1] * v.x + m.v[1][1] * v.y + m.v[2][1] * v.z + m.v[3][1] ) * fInvW;
	r.z = ( m.v[0][2] * v.x + m.v[1][2] * v.y + m.v[2][2] * v.z + m.v[3][2] ) * fInvW;

	return Vector3(r.x, r.y, r.z);
}

Vector3& Vector3::Multiply(const Matrix& m)
{
	return *this = Vector3::Multiply(*this, m);
}

Vector3 Vector3::operator*(const Matrix& m) const
{
	return Vector3::Multiply(*this, m);
}

Vector3& Vector3::operator*=(const Matrix& m)
{
	return *this = Vector3::Multiply(*this, m);
}

//Quaternion operations

Vector3 Vector3::Multiply(const Vector3& in, const Quaternion& q)
{
	Quaternion tmp, inv;

	Vector3 final;

	inv.x = -q.x; inv.y = -q.y; inv.z = -q.z; inv.w = q.w;
	//inv.Normalize(); // causes crash????

	tmp.w = - (q.x * in.x) - (q.y * in.y) - (q.z * in.z);
	tmp.x =   (q.w * in.x) + (q.y * in.z) - (q.z * in.y);
	tmp.y =   (q.w * in.y) + (q.z * in.x) - (q.x * in.z);
	tmp.z =   (q.w * in.z) + (q.x * in.y) - (q.y * in.x);

	final.x = (tmp.x * inv.w) + (tmp.w * inv.x) + (tmp.y * inv.z) - (tmp.z * inv.y);
	final.y = (tmp.y * inv.w) + (tmp.w * inv.y) + (tmp.z * inv.x) - (tmp.x * inv.z);
	final.z = (tmp.z * inv.w) + (tmp.w * inv.z) + (tmp.x * inv.y) - (tmp.y * inv.x);

	return final;
}

Vector3& Vector3::Multiply(const Quaternion& q)
{
	return *this = Vector3::Multiply(*this, q);
}

Vector3 Vector3::operator*(const Quaternion& q) const
{
	return Vector3::Multiply(*this, q);
}

Vector3& Vector3::operator*=(const Quaternion& q)
{
	return *this = Vector3::Multiply(*this, q);
}
