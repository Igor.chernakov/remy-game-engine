#include "Plugin.h"

#include "ExportMesh.h"

#include <stdarg.h>

#include <iostream>
#include <fstream>
#include <algorithm>

static const float EPS = 0.001f;

bool bMoveToZero;
bool bEmbedTextures;
bool bExportColors;
bool bSeparateByMatID;
bool bExportAnimation;

class ModelExport :
	public SceneExport,
	public XmlMeshExport
{
public:
	ModelExport(void)
		: pRootBone(NULL)
	{
		memset(pFlags, 0, sizeof(pFlags));
	}

	int ExtCount(void)
	{ // Number of extensions supported
		return(1);
	}

	const TCHAR* Ext(int n)
	{ // Extension #n(i.e. "3DS")
		return("MODEL");
	}

	const TCHAR* LongDesc(void)
	{ // Long ASCII description(i.e. "Autodesk 3D Studio File")
		return("GameEngine Skinned Model File");
	}

	const TCHAR* ShortDesc(void)
	{ // Short ASCII description(i.e. "3D Studio")
		return("GameEngine Skinned Model");
	}

	const TCHAR* AuthorName(void)
	{ // ASCII Author name
		return("Igor Chernakov");
	}

	const TCHAR* CopyrightMessage(void)
	{ // ASCII Copyright message
		return("");
	}

	const TCHAR* OtherMessage1(void)
	{ // Other message #1
		return("");
	}

	const TCHAR* OtherMessage2(void)
	{ // Other message #2
		return("");
	}

	unsigned int Version(void)
	{ // Version number * 100(i.e. v3.01 = 301)
		return(100);
	}

	void ShowAbout(HWND hWnd)
	{ // Show DLL's "About..." box
	}

	int DoExport(const TCHAR* name, ExpInterface* ei, Interface* i, BOOL suppressPrompts = FALSE, DWORD options = 0);

	BOOL SupportsOptions(int ext, DWORD options)
	{
		return FALSE;
	}

	BOOL CanExport()
	{
		return pRootBone && pMeshes.size() > 0;
	}

	INode* pRootBone;

	typedef std::list<INode*> Meshes;

	Meshes pMeshes;

	std::string filename;

	Interface* gi;

private:
	enum Flags {
		FLAG_EXPORTMESHES,
		FLAG_TEXTUALMESHES,
		FLAG_EXPORTANIMATIONS,
		FLAG_LAST
	};

	UINT pFlags[FLAG_LAST];
};

class ModelExportClassDesc :
	public ClassDesc
{
public:
	int IsPublic(void)
	{
		return(1);
	}

	void* Create(BOOL loading = FALSE)
	{
		return(new ModelExport);
	}

	const TCHAR* ClassName(void)
	{
		return("GameEngine Skinned Model Export");
	}

	SClass_ID SuperClassID(void)
	{
		return(SCENE_EXPORT_CLASS_ID);
	}

	Class_ID ClassID(void)
	{
		return(Class_ID(0x502c1e1f, 0x16017e59));
	}

	const TCHAR* Category(void)
	{
		return("Scene Export");
	}
};

ClassDesc* GetModelExportClassDesc(void)
{
	return(new ModelExportClassDesc);
}

struct RootBoneSelectDialog : public HitByNameDlgCallback
{
	TCHAR *dialogTitle() 
	{
		return "Select root bone";
	}
	TCHAR *buttonText()
	{
		return "Select";
	}
	BOOL singleSelect()
	{
		return TRUE;
	}
	BOOL useProc()
	{
		return FALSE;
	}
	int filter(INode *node)
	{
		return TRUE;
	}
};

struct MeshesSelectDialog : public HitByNameDlgCallback
{
	TCHAR *dialogTitle() 
	{
		return "Add meshes to export";
	}
	TCHAR *buttonText()
	{
		return "Add";
	}
	BOOL singleSelect()
	{
		return FALSE;
	}
	BOOL useProc()
	{
		return FALSE;
	}
	int filter(INode *node)
	{
		if (XmlMeshExport::GetSkinModifier(node))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
};

extern Interface* gi;


// Dialog proc
static INT_PTR CALLBACK ExportDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	ModelExport *exp = (ModelExport*)GetWindowLongPtr(hWnd, GWLP_USERDATA); 

	switch(msg)
	{
		case WM_INITDIALOG:
			exp =(ModelExport*)lParam;
			EnableWindow(GetDlgItem(hWnd, IDOK), exp->CanExport());
			CheckDlgButton(hWnd, IDC_EMBEDTEXTURES, TRUE);
			SetWindowLongPtr(hWnd,GWLP_USERDATA,lParam); 
			CenterWindow(hWnd, GetParent(hWnd)); 
			{
				XmlSceneExport xml;
				xml.CollectModels(exp->gi->GetRootNode(), exp->gi);
				if (xml.models.size() == 1) //if we have only one model, let's add it to the list
				{
					exp->pMeshes.push_back(xml.models.back().model);
					SendMessage(GetDlgItem(hWnd, IDC_MESHESLIST), LB_ADDSTRING, 0,
						(LPARAM)(xml.models.back().model->GetName()));

					if (exp->pRootBone = xml.models.back().bone)
					{
						SetDlgItemText(hWnd, IDC_ROOTBONENAME, exp->pRootBone->GetName());
						EnableWindow(GetDlgItem(hWnd, IDOK), exp->CanExport());
					}
				}
			}
			break;

		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDC_SELECTROOTBONE:
					{
						RootBoneSelectDialog root;
						GetCOREInterface()->ClearNodeSelection();
						GetCOREInterface()->DoHitByNameDialog(&root);
						if (GetCOREInterface()->GetSelNodeCount() > 0)
						{
							exp->pRootBone = GetCOREInterface()->GetSelNode(0);
							SetDlgItemText(hWnd, IDC_ROOTBONENAME, exp->pRootBone->GetName());
							GetCOREInterface()->ClearNodeSelection();
						}
					}

					EnableWindow(GetDlgItem(hWnd, IDOK), exp->CanExport());

					break;

				case IDC_ADDMESH:
					{
						MeshesSelectDialog meshes;
						GetCOREInterface()->ClearNodeSelection();
						GetCOREInterface()->DoHitByNameDialog(&meshes);
						if (GetCOREInterface()->GetSelNodeCount() > 0)
						{
							for (int i = 0; i < GetCOREInterface()->GetSelNodeCount(); i++)
							{
								INode* node = GetCOREInterface()->GetSelNode(i);
								ModelExport::Meshes::const_iterator m = std::find(exp->pMeshes.begin(), exp->pMeshes.end(), node);
								if (m != exp->pMeshes.end())
								{
									continue;
								}
								exp->pMeshes.push_back(node);
								SendMessage(GetDlgItem(hWnd, IDC_MESHESLIST), LB_ADDSTRING, 0, (LPARAM)node->GetName());

								if (!exp->pRootBone)
								{
									exp->pRootBone = XmlMeshExport::GetRootBoneForSkinnedMesh(
										(ISkin*)XmlMeshExport::GetSkinModifier(node)->GetInterface(I_SKIN));
									SetDlgItemText(hWnd, IDC_ROOTBONENAME, exp->pRootBone->GetName());
								}

							}

							GetCOREInterface()->ClearNodeSelection();
						}
					}

					EnableWindow(GetDlgItem(hWnd, IDOK), exp->CanExport());

					break;
				case IDC_REMOVEMESH:
					{
						int sel = SendMessage(GetDlgItem(hWnd, IDC_MESHESLIST), LB_GETCURSEL, 0, 0);
						if (sel > -1)
						{
							SendMessage(GetDlgItem(hWnd, IDC_MESHESLIST), LB_DELETESTRING, sel, 0);
							for (ModelExport::Meshes::iterator m = exp->pMeshes.begin(); m != exp->pMeshes.end(); m++, sel--)
							{
								if (sel == 0)
								{
									exp->pMeshes.erase(m);
									break;
								}
							}
						}
					}

					EnableWindow(GetDlgItem(hWnd, IDOK), exp->CanExport());

					break;

				case IDOK:
					{
						FileOutput::Mode filemode = FileOutput::BINARY;
						if (IsDlgButtonChecked(hWnd, IDC_EXPORTASXML))
						{
							filemode = FileOutput::TEXTUAL;
						}
						FileOutput modelfile(exp->filename, filemode);
						XmlMeshExport::SkinnedMeshes meshes;
						for (ModelExport::Meshes::iterator it = exp->pMeshes.begin(); it != exp->pMeshes.end(); it++)
						{
							meshes.push_back(XmlMeshExport::SkinnedMesh(*it, (ISkin*)XmlMeshExport::GetSkinModifier(*it)->GetInterface(I_SKIN)));
						}

						bMoveToZero = IsDlgButtonChecked(hWnd, IDC_MOVETOZERO);
						bEmbedTextures = IsDlgButtonChecked(hWnd, IDC_EMBEDTEXTURES);
						bExportColors = IsDlgButtonChecked(hWnd, IDC_ADDVERTEXCOLORS);
						bSeparateByMatID = IsDlgButtonChecked(hWnd, IDC_SEPARATEBYMATID);

						exp->ProcessMeshes(modelfile, exp->pRootBone, meshes, exp->gi);

						modelfile.close();
					}
					EndDialog(hWnd, 1);
					break;

				case IDCANCEL:
					EndDialog(hWnd, 0);
					break;
			}

			break;
		default:
			return FALSE;
	}

	return TRUE;
}       


int ModelExport::DoExport(const TCHAR* name, ExpInterface* ei, Interface* gi, BOOL SuppressPrompts, DWORD options)
{
	this->filename = name;
	this->gi = gi;

	if(!DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_MODELEXPORT_DLG), gi->GetMAXHWnd(), ExportDlgProc,(LPARAM)this))
	{
		return 1;
	}

	return(1);
}
