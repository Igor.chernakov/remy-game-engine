/**
	SPRITE is Targa TEXTURE ATLAS baked by CONVERTER tool (in "/tools" directory) with 
	meta data written in the end of the file

	All of the images are CROPPED from the sides if ALPHA = 0 to save space

	See summary in the bottom
**/

#include "c_client.hpp"
#include "c_entity.hpp"
#include "c_material.hpp"

#include <strstream>

extern IRenderer* __HackRenderer;

namespace client
{
	namespace io
	{
		/**
		This class loads SPRITE from BlockFile (exported from Converter)
		Chunk structure is following:
			SPRT - header of the first block
				ANIM - (can be multiple) sprite animation 
					NAME - name of the animations
					RATE - speed of the animation
					FRAM - (can be multiple) animation frame mapping
						NAME - name of the original file
						SIZE - original sprite size
						RECT - cropped rectangle (unused space to the sides of the picture is cropped)
						PACK - packed UVs
		**/

		struct SpriteFile: shared::Resource
		{
			void Mod_ReadSPRT(FILE* file, int size) {
				loaded = true;
				F_BlockReadLoop(file, size) {
					switch (F_BlockReadHeader(file)) {
					case MAKEID('A', 'N', 'I', 'M'): {
						Animation animation(this, file, F_BlockReadSize(file));
						animations[animation.name] = animation;
						if (!default_animation) {
							default_animation = &animations[animation.name];
						}
						break;
					}
					default:
						F_BlockSkip(file);
					}
				}
			}

			SpriteFile(const std::string& filename)
				: shared::Resource(filename)
				, default_animation(0)
				, loaded(false)
			{
				file_t file = framework->GetFileSystem()->LoadFile(filename);

				if (!file.valid())
					return;

				int id, size;
				fseek(file, file.size - 4, SEEK_CUR);
				fread(&id, 4, 1, file);
				if (id != MAKEID('S', 'P', 'R', 'T')) {
					fclose(file);
					return;
				}
				fseek(file, -8, SEEK_CUR);
				fread(&size, 4, 1, file);
				fseek(file, - (size + 4), SEEK_CUR);

				switch (F_BlockReadHeader(file)) {
				case  MAKEID('S', 'P', 'R', 'T'):
					Mod_ReadSPRT(file, F_BlockReadSize(file));
					break;
				default:
					framework->Log(IFramework::MSG_ERROR, "%s is not a sprite file", file.name.c_str());
					F_BlockSkip(file);
				}

				file.close();
			}

			struct Animation
			{
				Animation() : rate(0) {}

				Animation(SpriteFile* self, FILE* file, int size) : rate(0)
				{
					F_BlockReadLoop(file, size) {
						switch (F_BlockReadHeader(file)) {
						case MAKEID('N', 'A', 'M', 'E'):
							name = F_BlockReadString(file);
							break;
						case MAKEID('R', 'A', 'T', 'E'):
							F_BlockRead(file, rate);
							break;
						case MAKEID('F', 'R', 'A', 'M'):
							frames.push_back(Frame(self, file, F_BlockReadSize(file)));
							break;
						default:
							F_BlockSkip(file);
						}
					}
				}
				
				struct Frame
				{
					Frame() {}

					Frame(SpriteFile* self, FILE* file, int size) {
						F_BlockReadLoop(file, size) {
							switch (F_BlockReadHeader(file)) {
							case MAKEID('N', 'A', 'M', 'E'):
								name = F_BlockReadString(file);
								break;
							case MAKEID('S', 'I', 'Z', 'E'):
								F_BlockRead(file, this->size);
								break;
							case MAKEID('R', 'E', 'C', 'T'):
								F_BlockRead(file, rect);
								break;
							case MAKEID('P', 'A', 'C', 'K'):
								F_BlockRead(file, pack);
								break;
							default:
								F_BlockSkip(file);
							}
						}
					}

					rect_t rect;
					dim_t size;
					std::string name;
					box2_t pack;
				};

				int rate;
				std::string name;
				std::vector<Frame> frames;
			};

			const Animation* default_animation;
			std::map<std::string, Animation> animations;
			bool loaded;
		};
	}

	/**
		SPRITE can be rendered in different ways but will always appear flat to the viewer
		It can be aligned to ANY AXIS, be drawn with L-SHAPE (i.e. the bottom half is aligned to the floor,
		but the top is drawn normally - to avoid clipping) or be drawn normally - aligned to CAMERA

		Also SLICING is supported (used for CLOCK-LIKE progress display or SIDE-TO-SIDE one)
	**/

	class Sprite
		: public Entity
		, public boost::python::wrapper<Sprite>
		, public IWrapper
	{
	public:
		enum SpriteFlag
		{
			SPRITEFLAG_NONE = 0,
			SPRITEFLAG_NONFIXED = 1 << 0,
			SPRITEFLAG_LSHAPE = 1 << 1,
			SPRITEFLAG_LINEAR = 1 << 2,
			SPRITEFLAG_IGNOREOFFSET = 1 << 3,
			SPRITEFLAG_NODEPTH = 1 << 4,
			SPRITEFLAG_ADDALPHA = 1 << 5,
		};

		enum Align {
			SA_CAMERA,
			SA_AXISX,
			SA_AXISY,
			SA_AXISZ,
		} m_Align;

		std::string m_Path;

	public:
		Sprite(
			const std::string& name,
			const vec3_t& position,
			const vec3_t& scale,
			const color_t& color,
			const vec2_t& size,
			const std::string& source,
			int flags,
			int frame,
			const vec2_t& slice,
			float offset,
			int align,
			const std::string& animation)
			: Entity(name, position, quat_t(), scale, color)
			, m_Flags(flags)
			, m_ExactSize(size)
			, m_Frame(frame)
			, m_Rate(0)
			, m_Animation(0)
			, m_PreviousAnimation(0)
			, m_Pause(false)
			, m_CurrentFrame(0)
			, m_Path(source)
			//, m_Slice(slice)
			, m_Offset(offset)
			, m_Align((Align)align)
			, m_AlphaTest(1)
			, m_ActiveRect(0, 0, 1, 1)
		{
			m_SpriteFile = framework->GetResources()->Load<io::SpriteFile>(source);

			if (m_SpriteFile->loaded) {
				m_Animation = m_SpriteFile->default_animation;
				m_CurrentFrame = &m_Animation->frames[0];
			}

			SetMaterial(framework->GetResources()->Load<Material>(f_StripExtension(source)), flags);

			RenderCallback = (RenderCallbackT)&Sprite::OnRender;
			IntersectionChecker = (IntersectionCheckerT)&Sprite::CheckIntersection;
			AnimateCallback = (AnimateCallbackT)&Sprite::OnAnimate;

			if (!animation.empty())
			{
				SetAnimation(animation);
				m_Frame = frame;
			}
		}
		
		~Sprite()
		{
		}

		bool AngleBetween(float x, float left, float right) {
			while (x < 0) x += 360;
			while (x >= 360) x -= 360;
			while (left < 0) left += 360;
			while (left >= 360) left -= 360;
			while (right < 0) right += 360;
			while (right >= 360) right -= 360;
			if (left <= right) {
				return x >= left && x < right;
			} else {
				return x >= left || x < right;
			}
		}

		void OnAnimate(float timestep)
		{
			if (m_Animation && m_Animation->frames.size() > 0 && !m_Pause)
			{
				m_Frame += timestep * m_Rate;

				for (bool fired = false; m_Frame >= (float)m_Animation->frames.size(); 
					fired = true, m_Frame -= m_Animation->frames.size())
				{
					if (!fired)
					{
						AnimationFinished();
						fired = true;

						if (m_PreviousAnimation)
						{
							m_Animation = m_PreviousAnimation;
							m_PreviousAnimation = 0;
							m_Frame = 0.0f;
							return;
						}
					}
				}

				int frame(m_Frame);

				if (frame >= m_Animation->frames.size()) {
					frame = m_Animation->frames.size() - 1;
					framework->Log(IFramework::MSG_ERROR, "Incorrect conversion to int!!!");
				}

				m_CurrentFrame = &m_Animation->frames[frame];
			}

			vec3_t direction;
			if (m_Material)
			{
				box2_t er;

				int width = m_Material->GetWidth(), height = m_Material->GetHeight();

				if (m_CurrentFrame)
				{
					width = m_CurrentFrame->size.width;
					height = m_CurrentFrame->size.height;
				}

				if (m_Animation)
				{
					const rect_t* r = &m_Animation->frames[(int)floor(m_Frame) % m_Animation->frames.size()].rect;
					er.vMin.x = r->left;
					er.vMin.y = r->top;
					er.vMax.x = r->right;
					er.vMax.y = r->bottom;
				}
				else
				{
					er.vMax.x = (float)m_Material->GetWidth();
					er.vMax.y = (float)m_Material->GetHeight();
				}
				float ps = framework->GetScene()->GetCamera()->GetPixelSize(GetMatrix().GetTranslation());
				float oshift = __HackRenderer->GetStats().needs_ortho_shift ? 0.0f : 0.5f;
				const mat4_t& matrix = GetMatrix();
				const vec2_t shift(((width + 1) % 2) * 0.5f, ((height + 1) % 2) * 0.5f);
				const vec2_t size(GetSize());
				const vec3_t right(vec3_t::Normalize(vec3_t::Cross(GetNormal(), -GetFront())));
				const vec3_t up(vec3_t::Normalize(vec3_t::Cross(GetNormal(), -right)));
				const vec2_t p(framework->GetScene()->GetCamera()->GetScreenCoords(matrix.GetTranslation()));
				const vec2_t px(floor(p.x + 0.5f) - p.x + oshift, floor(p.y + 0.5f) - p.y + 0.65f);

				mat4_t projector;

				const bool is_ortho = framework->GetScene()->GetCamera()->GetProjection() == ICamera::PROJ_ORTHOGONAL;

				const vec3_t camera = is_ortho ? 
					matrix.GetTranslation() + framework->GetScene()->GetCamera()->GetDirection() * 10000000 : //TODO: hack
					framework->GetScene()->GetCamera()->GetEye();

				switch (m_Align) {
					case SA_AXISX:
						projector = mat4_t::Shadow(
							vec4_t(camera.x, camera.y, camera.z, is_ortho ? 1 : 0),
							vec4_t(-1, 0, 0, matrix.GetTranslation().x + math::EPS));
						break;
					case SA_AXISY:
						projector = mat4_t::Shadow(
							vec4_t(camera.x, camera.y, camera.z, is_ortho ? 1 : 0),
							vec4_t(0, -1, 0, matrix.GetTranslation().y + math::EPS));
						break;
					default:
						projector = mat4_t::Shadow(
							vec4_t(camera.x, camera.y, camera.z, is_ortho ? 1 : 0),
							vec4_t(0, 0, -1, matrix.GetTranslation().z + math::EPS));
						break;
				}

				box2_t r(
					size.x * (er.vMin.x / (float)width - 0.5f) + (px.x - shift.x) * ps,
					size.y * (er.vMin.y / (float)height - 0.5f) + (px.y - shift.y) * ps,
					size.x * (er.vMax.x / (float)width - 0.5f) + (px.x - shift.x) * ps,
					size.y * (er.vMax.y / (float)height - 0.5f) + (px.y - shift.y) * ps),
					r_uv(m_ActiveRect);

				r.vMax.x = r.vMin.x + (r.vMax.x - r.vMin.x) * m_ActiveRect.vMax.x;

				s.pIndices = i;

				//vertical plane
				v[0].position = (right * r.vMin.x + up * r.vMin.y) * matrix; v[0].uv.x = r_uv.vMin.x; v[0].uv.y = r_uv.vMin.y;
				v[1].position = (right * r.vMax.x + up * r.vMin.y) * matrix; v[1].uv.x = r_uv.vMax.x; v[1].uv.y = r_uv.vMin.y;
				v[2].position = (right * r.vMax.x + up * r.vMax.y) * matrix; v[2].uv.x = r_uv.vMax.x; v[2].uv.y = r_uv.vMax.y;
				v[3].position = (right * r.vMin.x + up * r.vMax.y) * matrix; v[3].uv.x = r_uv.vMin.x; v[3].uv.y = r_uv.vMax.y;
		
				if (m_Slice != vec2_t())
				{
					float 
						_sx = sin(deg2rad(90 - m_Slice.x)),
						_cx = cos(deg2rad(90 - m_Slice.x)),
						_sy = sin(deg2rad(90 - m_Slice.y)),
						_cy = cos(deg2rad(90 - m_Slice.y));
					
					vec2_t 
						pointx = vec2_t::Normalize(m_InnerSpace.y * _sx + m_InnerSpace.x * _cx),
						pointy = vec2_t::Normalize(m_InnerSpace.y * _sy + m_InnerSpace.x * _cy);
					
					float angle_x = atan2(pointx.y, pointx.x), angle_y = atan2(pointy.y, pointy.x);
					float a_x = 90 - rad2deg(angle_x), a_y = 90 - rad2deg(angle_y);

					vec2_t a(cos(angle_x), sin(angle_x)), b(cos(angle_y), sin(angle_y));

					if (fabs(a.x) > fabs(a.y)) {
						a.y /= fabs(a.x);
						a.x /= fabs(a.x);
					} else {
						a.x /= fabs(a.y);
						a.y /= fabs(a.y);
					}

					if (fabs(b.x) > fabs(b.y)) {
						b.y /= fabs(b.x);
						b.x /= fabs(b.x);
					} else {
						b.x /= fabs(b.y);
						b.y /= fabs(b.y);
					}

					a.x = (a.x + 1) / 2;//int((a.x + 1) / 2 * width) / (float)width;
					b.x = (b.x + 1) / 2;//int((b.x + 1) / 2 * width) / (float)width;

					a.y = 1 - (a.y + 1) / 2;//int((a.y + 1) / 2 * height) / (float)height;
					b.y = 1 - (b.y + 1) / 2;//int((b.y + 1) / 2 * height) / (float)height;

					v[4].position = (
						right * (r.vMin.x + (r.vMax.x - r.vMin.x) * a.x) +
						up *	(r.vMin.y + (r.vMax.y - r.vMin.y) * a.y)) * matrix;
					v[4].uv.x = a.x;
					v[4].uv.y = a.y;
					
					v[5].position = (
						right * (r.vMin.x + (r.vMax.x - r.vMin.x) * b.x) +
						up *	(r.vMin.y + (r.vMax.y - r.vMin.y) * b.y)) * matrix;
					v[5].uv.x = b.x;
					v[5].uv.y = b.y;

					v[6].position = (right * (r.vMin.x + r.vMax.x) / 2 + up * (r.vMin.y + r.vMax.y) / 2) * matrix;
					v[6].uv.x = 0.5;
					v[6].uv.y = 0.5;

					if (m_Align == SA_AXISX || m_Align == SA_AXISY || m_Align == SA_AXISZ)
					{
						for (int i = 0; i < 7; v[i++].position *= projector);
					}

					s.nIndices = 0;

					bool started = false, finished = false;

					for (int d = 0; !finished; d++) {
						bool just_started = false;
						float _a1 = d * 90 - 45, _a2 = (d + 1) * 90 - 45;
						if (AngleBetween(a_x, _a1, _a2) && !started) {
							i[s.nIndices++] = 4;
							started = true;
							just_started = true;
						} else {
							if (!started)
								continue;
							i[s.nIndices++] = d % 4;
						}
						if (AngleBetween(a_y, _a1, _a2) && (!just_started || !AngleBetween(a_y, _a1, a_x))) {
							i[s.nIndices++] = 5;
							finished = true;
						} else {
							i[s.nIndices++] = (d + 1) % 4;
						}

						i[s.nIndices++] = 6;
					}

				} else 
				if (m_Align != SA_CAMERA || ((m_Flags & SPRITEFLAG_LSHAPE) && (r.vMax.y / size.y > 0)))
				{
					//horizontal plane
					v[4] = v[0]; v[4].position = v[4].position * projector;
					v[5] = v[1]; v[5].position = v[5].position * projector;
					v[6] = v[2]; v[6].position = v[6].position * projector;
					v[7] = v[3]; v[7].position = v[7].position * projector;

					//middle edge
					v[8].position = right * r.vMax.x * matrix; v[8].uv = vec2_t(r_uv.vMax.x, -r.vMin.y / (r.vMax.y - r.vMin.y));
					v[9].position = right * r.vMin.x * matrix; v[9].uv = vec2_t(r_uv.vMin.x, -r.vMin.y / (r.vMax.y - r.vMin.y));

					if ((m_Flags & SPRITEFLAG_LSHAPE) && (r.vMin.y / size.y < 0))
					{
						index_t indices[12] = { 0, 1, 8, 8, 9, 0, 9, 8, 7, 7, 8, 6 };
						memcpy(i, indices, sizeof(indices));
						s.nIndices = 12;
					}
					else
					{
						index_t indices[6] = { 4, 5, 6, 6, 7, 4 };
						memcpy(i, indices, sizeof(indices));
						s.nIndices = 6;
					}
				}
				else
				{
					index_t indices[6] = { 0, 1, 2, 2, 3, 0 };
					memcpy(i, indices, sizeof(indices));
					s.nIndices = 6;
				}
				s.pVertices = v;
				s.nVertices = 10;
				s.pMaterial = m_Material;
				if (m_CurrentFrame) {
					for (int i = 0; i < 10; i++) {
						v[i].uv.x = v[i].uv.x * m_CurrentFrame->pack.Width() + m_CurrentFrame->pack.vMin.x;
						v[i].uv.y = v[i].uv.y * m_CurrentFrame->pack.Height() + m_CurrentFrame->pack.vMin.y;
					}
				}
				s.color = GetColor();
				s.SetColor(s.color);

				direction = framework->GetScene()->GetCamera()->GetDirection();

				switch (m_Align)
				{
					case SA_AXISX:
						direction *= (m_Offset / fabs(direction.x));
						break;
					case SA_AXISY:
						direction *= (m_Offset / fabs(direction.y));
						break;
					case SA_AXISZ:
						direction *= (m_Offset / fabs(direction.z));
						break;
					default:
						direction *= m_Offset;
						break;
				}

				for (int i = 0; i < s.nVertices; i++)
				{
					v[i].position += direction;
				}
			}

			s.flags = m_flags;
			s.pivot = m_pivot;

			if (m_Flags & SPRITEFLAG_IGNOREOFFSET) {
				s.sortvalue = 0;
				return;
			}

			s.sortvalue = framework->GetScene()->GetCamera()->GetDistance(GetMatrix().GetTranslation() + direction);
		}

		void OnRender(IClientScene* scene) const
		{
			scene->PushSurface(&s);
		}

		inline shared_ptr<const IMaterial> GetMaterial() const
		{
			return m_Material;
		}

		inline void SetMaterial(shared_ptr<const IMaterial> value, int flags)
		{
			m_Material = value;
		}

		inline vec3_t GetNormal() const
		{
			return -framework->GetScene()->GetCamera()->GetDirection() *
				mat4_t::Invert(mat4_t(GetMatrix().GetFront(), GetMatrix().GetRight(), GetMatrix().GetUp()));
		}

		inline vec3_t GetFront() const
		{
			return framework->GetScene()->GetCamera()->GetYaw() *
				mat4_t::Invert(mat4_t(GetMatrix().GetFront(), GetMatrix().GetRight(), GetMatrix().GetUp()));
		}

		inline vec2_t GetSize() const
		{
			if (m_Flags & SPRITEFLAG_NONFIXED)
			{
				return GetExactSize();
			}
			else
			{
				return GetExactSize() * framework->GetScene()->GetCamera()->GetPixelSize(GetMatrix().GetTranslation());
			}
		}

		inline vec2_t GetExactSize() const
		{
			if (m_ExactSize.x > 1 || m_ExactSize.y > 1 || !m_Material || !m_Material->GetLayersCount())
				return m_ExactSize;
			else
				return m_CurrentFrame ? 
					vec2_t((float)m_CurrentFrame->size.width, (float)m_CurrentFrame->size.height) :
					vec2_t((float)m_Material->GetWidth(), (float)m_Material->GetHeight());
		}

		inline void SetExactSize(const vec2_t& value)
		{
			m_ExactSize = value;
		}

		inline bool PlayAnimation(const std::string& name)
		{
			const io::SpriteFile::Animation* tmp = m_Animation;
			if (SetAnimation(name))
			{
				if (!m_PreviousAnimation)
				{
					m_PreviousAnimation = tmp;
				}
				return true;
			}
			else
			{
				return false;
			}
		}

		inline bool SetAnimation(const std::string& name)
		{
			if (m_SpriteFile->animations.find(name) != m_SpriteFile->animations.end())
			{
				m_Animation = &m_SpriteFile->animations.find(name)->second;
				m_CurrentFrame = &m_Animation->frames[0];
				m_Frame = 0;
				m_Rate = m_Animation->rate;

				return true;
			}
			else
			{
				return false;
			}
		}

		inline std::string GetAnimation() const
		{
			return m_Animation ? m_Animation->name : "";
		}

		inline int GetFrame() const 
		{
			return (int)floor(m_Frame);
		}

		inline void SetFrame(int value)
		{
			m_Frame = (float)value;
			if (m_Animation && m_Animation->frames.size() > 0) {
				m_CurrentFrame = &m_Animation->frames[value % m_Animation->frames.size()];
			} else {
				m_CurrentFrame = 0;
			}
		}

		inline float GetRate() const 
		{
			return floor(m_Rate);
		}

		inline void SetRate(float value)
		{
			m_Rate = value;
		}

		inline bool GetPause() const 
		{
			return m_Pause;
		}

		inline void SetPause(bool value)
		{
			m_Pause = value;
		}

		inline int GetFramesCount() const 
		{
			return m_Animation ? m_Animation->frames.size() : 1;
		}

		bool CheckIntersection(const math::Line& line) const
		{
			const vec3_t
				rt(vec3_t::Normalize(vec3_t::Cross(GetNormal(), -GetFront()))),
				up(vec3_t::Normalize(vec3_t::Cross(GetNormal(), -rt)));

			vertex_t v[6];

			Surface s(Surface::CreateBox(v, rt * GetSize().x / 2, up * GetSize().y / 2, vec3_t::Zero));

			return  
				line.Intersects(math::Triangle(v[1].position, v[0].position, v[2].position, GetNormal()), &m_hotspot) ||
				line.Intersects(math::Triangle(v[4].position, v[3].position, v[5].position, GetNormal()), &m_hotspot);
		}

		std::string GetSource() const 
		{
			return m_Path;
		}

		virtual shared_ptr<client::Entity> shared_from_this()
		{
			return boost::python::extract<shared_ptr<client::Entity> >(GetPythonObject());
		}

		virtual shared_ptr<const client::Entity> shared_from_this() const
		{
			return boost::python::extract<shared_ptr<const client::Entity> >(GetPythonObject());
		}

		virtual PyObject* GetPythonObject() const
		{
			return boost::python::detail::wrapper_base_::get_owner(*this);
		}

		const vec2_t& GetSlice() const {
			return m_Slice;
		}

		void SetSlice(const vec2_t& value) {
			m_Slice = value;
		}

		struct frame_info_t {
			rect_t rect;
			box2_t pack;
		};

		frame_info_t GetFrameInfo(const std::string& animation, int frame) {
			frame_info_t fi;
			memset(&fi, 0, sizeof(frame_info_t));
			if (m_SpriteFile && m_SpriteFile->animations.find(animation) != m_SpriteFile->animations.end())
			{
				const io::SpriteFile::Animation *anim = &m_SpriteFile->animations.find(animation)->second;
				if (anim->frames.size() > frame) {
					fi.rect = anim->frames[frame].rect;
					fi.pack = anim->frames[frame].pack;
				}
			}
			return fi;
		};

	public:
		Event AnimationFinished;
		float m_AlphaTest;

	private:
		vec2_t m_ExactSize;
		vec2_t m_Slice;
		int m_Flags;
		float m_Frame, m_Rate;
		shared_ptr<const IMaterial> m_Material;

		shared_ptr<const io::SpriteFile> m_SpriteFile;
		const io::SpriteFile::Animation *m_Animation, *m_PreviousAnimation;
		const io::SpriteFile::Animation::Frame *m_CurrentFrame;
		bool m_Pause;

		vertex_t v[32];
		index_t i[64];
		Surface s;

	public:

		struct InnerSpace
		{
			InnerSpace(): x(1, 0), y(0, 1), angle(0) {}
			vec2_t x, y;
			float angle;
		};

		float m_Offset;

		InnerSpace m_InnerSpace;

		box2_t m_ActiveRect;
	};
}

void export_Sprite()
{
	namespace py = boost::python;

	using client::Sprite;

	py::scope e = py::class_<Sprite, shared_ptr<Sprite>, py::bases<client::Entity>, boost::noncopyable>("Sprite", py::no_init)
		.def(py::init<std::string, vec3_t, vec3_t, color_t, vec2_t, std::string, int, int, vec2_t, float, int, std::string>((
			py::arg("id") = std::string("Sprite#"),
			py::arg("position") = vec3_t::Zero,
			py::arg("scale") = vec3_t::One,
			py::arg("color") = color_t(),
			py::arg("size") = vec2_t::One,
			py::arg("source") = std::string(),
			py::arg("flags") = 0, 
			py::arg("frame") = 0, 
			py::arg("slice") = vec2_t::Zero,
			py::arg("offset") = 0,
			py::arg("align") = (int)Sprite::SA_CAMERA,
			py::arg("animation") = std::string()))) //FIXME: hack
		.def("PlayAnimation", &Sprite::PlayAnimation)
		.def("GetFrameInfo", &Sprite::GetFrameInfo)
		.add_property("source", &Sprite::GetSource)
		.add_property("size", &Sprite::GetExactSize, &Sprite::SetExactSize)
		.add_property("frame", &Sprite::GetFrame, &Sprite::SetFrame)
		.add_property("rate", &Sprite::GetRate, &Sprite::SetRate)
		.add_property("pause", &Sprite::GetPause, &Sprite::SetPause)
		.add_property("count", &Sprite::GetFramesCount)
		.add_property("animation", &Sprite::GetAnimation, &Sprite::SetAnimation)
		.add_property("slice", py_cref(&Sprite::GetSlice), &Sprite::SetSlice)
		.def_readwrite("innerspace", &Sprite::m_InnerSpace)
		.def_readwrite("offset", &Sprite::m_Offset)
		.def_readwrite("align", &Sprite::m_Align)
		.def_readwrite("active_rect", &Sprite::m_ActiveRect)
		.def_readwrite("alphareference", &Sprite::m_AlphaTest)
		.def_readwrite("AnimationFinished", &Sprite::AnimationFinished)
	;

	DISPOSEME(2) py::class_<Sprite::InnerSpace>("InnerSpace", py::no_init)
		.def_readwrite("x", &Sprite::InnerSpace::x)
		.def_readwrite("y", &Sprite::InnerSpace::y)
		.def_readwrite("angle", &Sprite::InnerSpace::angle)
	;

	DISPOSEME(2) py::class_<Sprite::frame_info_t>("FrameInfo", py::no_init)
		.def_readwrite("rect", &Sprite::frame_info_t::rect)
		.def_readwrite("pack", &Sprite::frame_info_t::pack)
	;

	py::enum_<Sprite::SpriteFlag>("Flag")
		.value("None", Sprite::SPRITEFLAG_NONE)
		.value("NonFixed", Sprite::SPRITEFLAG_NONFIXED)
		.value("LShape", Sprite::SPRITEFLAG_LSHAPE)
		.value("Linear", Sprite::SPRITEFLAG_LINEAR)
		.value("NoDepth", Sprite::SPRITEFLAG_NODEPTH)
		.value("IgnoreOffset", Sprite::SPRITEFLAG_IGNOREOFFSET)
		.value("AddAlpha", Sprite::SPRITEFLAG_ADDALPHA)
		.export_values()
		;

	py::enum_<Sprite::Align>("Align")
		.value("Camera", Sprite::SA_CAMERA)
		.value("AxisX",  Sprite::SA_AXISX)
		.value("AxisY",  Sprite::SA_AXISY)
		.value("AxisZ",  Sprite::SA_AXISZ)
		.export_values()
		;

	py::implicitly_convertible<shared_ptr<Sprite>, shared_ptr<client::Entity> >();

	DISPOSEME(4) e;
}
