#pragma once

#include "iobject.hpp"

struct ITimer : IObject
{
	virtual float Update() = 0;
	virtual unsigned long GetMilliseconds() = 0;
	virtual float GetFPS() const = 0;
	virtual void Reset() = 0;
};
