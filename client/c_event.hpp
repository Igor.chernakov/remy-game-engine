#pragma once

/**
	EVENTS are used in Python the same way as in C-Sharp, i.e self.StyleChanged += self.OnStyleChanged
**/

#include <list>
#include <algorithm>
#include <boost/python.hpp>

namespace client
{
	enum MouseButtons
	{
		BUTTON_NONE = 0,
		BUTTON_LEFT = 1 << 0,
		BUTTON_RIGHT = 1 << 1,
		BUTTON_MIDDLE = 1 << 2,
		BUTTON_X1 = 1 << 3,
		BUTTON_X2 = 1 << 4,
	};

	struct MouseEventArgs
	{
		MouseButtons button;
		int clicks, delta, x, y;
	};

	class Event
	{
		struct Handler {
			Handler(boost::python::object first_, boost::python::object second_)
				: first(first_)
				, second(second_)
				, useless(false)
			{}
			boost::python::object first, second;
			bool useless;
		};

	public:
		Event();
		~Event();

		int GetHandlerCount() const;

		Event& operator+= (boost::python::object handler);
		Event& operator-= (boost::python::object handler);

		static boost::python::object Fire(boost::python::tuple args, boost::python::dict kw);
		boost::python::object operator()(
			boost::python::tuple args = boost::python::tuple(),
			boost::python::dict kw = boost::python::dict());

		template <typename T, typename Y>
		boost::python::object operator()(const T& sender, const Y& e)
		{
			boost::python::dict kwds;
			kwds["sender"] = sender;
			kwds["e"] = e;
			return (*this)(boost::python::tuple(), kwds);
		}

		void AddOneTimer(boost::python::object handler);

		int GetCallCount() const { return m_call_count;}

		void Clear() { m_handlers.clear(); };

	private:
		int m_call_count;
		std::list<Handler> m_handlers;
		std::list<Handler> m_onetimers;
	};
}