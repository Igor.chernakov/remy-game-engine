#pragma once

#include "../math/math.hpp"

#include "iobject.hpp"

struct IServerSceneResult
{
	virtual std::string GetNextScene() const = 0;
	virtual void SetNextScene(const std::string& name) = 0;
};

struct IServerScene: IObject
{
	virtual shared_ptr<IServerSceneResult> Run() = 0;
	virtual void Animate(float) = 0;
};
