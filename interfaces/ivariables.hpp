#pragma once

#include <string>

#include "iobject.hpp"

struct IVariables: IObject
{
	virtual float GetFloat(const std::string&) const = 0;
	virtual int GetInteger(const std::string&) const = 0;
	virtual bool GetBoolean(const std::string&) const = 0;
	virtual const std::string& GetString(const std::string&) const = 0;
	virtual void SetVariable(const std::string&, const std::string&) = 0;
};