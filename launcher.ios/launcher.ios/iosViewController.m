#import "iosViewController.h"

#import "launcher.h"

int mouse_x = 0, mouse_y = 0;
int screen_x = 1024, screen_y = 768;
float timeSinceLastUpdate = 0.1;
bool is_phone = false;

@interface iosViewController()

@property (strong, nonatomic) EAGLContext *context;
@property (strong, nonatomic) GLKBaseEffect *effect;

- (void)setupGL;
- (void)tearDownGL;
- (void)myThreadMainMethod;

@end

@implementation iosViewController

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [[event touchesForView:self.view] anyObject];
	
    CGPoint location = [touch locationInView:self.view];
	
	mouse_x = 1024 * location.x / self.view.frame.size.width;
	mouse_y = 768 * location.y / self.view.frame.size.height;
	
	SendGameMessage(MSG_LBUTTON_DOWN, 0);
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch* touch = [[event touchesForView:self.view] anyObject];
	
    CGPoint location = [touch locationInView:self.view];
	
	mouse_x = 1024 * location.x / self.view.frame.size.width;
	mouse_y = 768 * location.y / self.view.frame.size.height;
	
	SendGameMessage(MSG_LBUTTON_UP, 0);
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch* touch = [[event touchesForView:self.view] anyObject];
	
    CGPoint location = [touch locationInView:self.view];
	
	mouse_x = 1024 * location.x / self.view.frame.size.width;
	mouse_y = 768 * location.y / self.view.frame.size.height;
	
	SendGameMessage(MSG_MOUSE_MOVE, 0);
}


char documentsDirectoryC[512];

/* Set the working directory to the .app's parent directory */
- (void) setupWorkingDirectory
{
    //setting the working directoy to MyApp.app/Contents/Resources/
    NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
    [[NSFileManager defaultManager] changeCurrentDirectoryPath:resourcePath];
	
	
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	
	if(![documentsDirectory getCString:documentsDirectoryC maxLength:512 encoding:NSUTF8StringEncoding]) {
		NSLog(@"Command buffer too small");
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];

    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
//	view.contentScaleFactor = 1;
	view.drawableColorFormat = GLKViewDrawableColorFormatRGBA8888;
	view.drawableStencilFormat = GLKViewDrawableStencilFormat8;
    view.drawableDepthFormat = GLKViewDrawableDepthFormat24;
	
	if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
		is_phone = true;
	}
	
    [self setupGL];
	
	[self setupWorkingDirectory];
	
	[NSThread detachNewThreadSelector:@selector(myThreadMainMethod) toTarget:self withObject:nil];
	
	self.preferredFramesPerSecond = 30;
}

- (void)myThreadMainMethod {
	RunGame();
}

- (void)dealloc
{    
    [self tearDownGL];
    
    if ([EAGLContext currentContext] == self.context) {
        [EAGLContext setCurrentContext:nil];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

    if ([self isViewLoaded] && ([[self view] window] == nil)) {
        self.view = nil;
        
        [self tearDownGL];
        
        if ([EAGLContext currentContext] == self.context) {
            [EAGLContext setCurrentContext:nil];
        }
        self.context = nil;
    }

    // Dispose of any resources that can be recreated.
}

- (void)setupGL
{
    [EAGLContext setCurrentContext:self.context];
}

- (void)tearDownGL
{
    [EAGLContext setCurrentContext:self.context];
}

#pragma mark - GLKView and GLKViewController delegate methods

- (void)update
{
	timeSinceLastUpdate = self.timeSinceLastUpdate;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
	screen_x = view.drawableWidth;
	screen_y = view.drawableHeight;
	
	//NSLog(@"Width: %d", screen_x);
	//NSLog(@"Height: %d", screen_y);
	
	RenderGameFrame();
}

@end
