#pragma once

#include "../shared/types.hpp"

#include "../interfaces/iclientframework.hpp"
#include "../interfaces/irenderer.hpp"
#include "../interfaces/itexture.hpp"
#include "../interfaces/iresource.hpp"

bool LoadTextureFromMemory(ITexture* texture, const char* mem2, int size);
DWORD Log2(DWORD val);

struct render_info_t {
	render_info_t(): texture_id(0), bitmap(0) {}
	unsigned int texture_id;
	unsigned char* bitmap;
};