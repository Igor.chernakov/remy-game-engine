#import <Foundation/Foundation.h>

@interface ASIdentifierManager : NSObject

- (id)init;
+ (ASIdentifierManager *)sharedManager;

@property(nonatomic, readonly, getter=isAdvertisingTrackingEnabled) BOOL advertisingTrackingEnabled;
@property(nonatomic, readonly) NSUUID *advertisingIdentifier;

@end
