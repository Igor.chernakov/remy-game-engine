#include "math.hpp"

namespace math
{
	float random()
	{
		return rand() / (float)RAND_MAX;
	}
	
	float signed_random()
	{
		return math::random() * 2.0f - 1.0f;
	}
	
	float random_range(float x, float y)
	{
		return math::random() * (y - x) + x;
	}
	
	float random_range(const Vector2& range)
	{
		return random_range(range.x, range.y);
	}
	
	int sign(const float& k)
	{
		return k > 0 ? 1 : -1;
	}
	
	unsigned int log2(unsigned int val)
	{
		unsigned int answer = 0;
		for (; val >>= 1; ++answer);
		return answer;
	}
	
	unsigned int pow2(unsigned int value)
	{
		unsigned int r = 0;
		for (r = 1; r < value; r <<= 1);
		return r;
	}
}

#ifdef _MSC_VER
std::istream& operator>>(std::istream& is, char& c)
#else
std::istream& operator>>(std::istream&& is, char& c)
#endif
{
	char got;
	
#ifdef _MSC_VER
	is.read(&got, 1);
#else
	is >> got;
#endif
	
	if (got != c)
	{
		is.setstate(std::ios::failbit);
	}
	
	return is;
}


template <typename T>
std::istream& read(std::istream& str, T& vec)
{
	float* raw = (float*)&vec;
	char comma = ',';
	memset(raw, 0, T::Dimensions * sizeof(math::Real));
	str >> raw[0];
	for (int i = 1; i < T::Dimensions; str >> comma >> raw[i++]);
	return str;
}

template <typename T>
std::ostream& write(std::ostream& str, const T& vec)
{
	const float* raw = (const float*)&vec;
	str << raw[0];
	for (int i = 1; i < T::Dimensions; str << ',' << raw[i++]);
	return str;
}

#ifdef _MSC_VER
std::istream& operator>>(std::istream& str, vec2_t& v) { return read(str, v); }
std::istream& operator>>(std::istream& str, vec3_t& v) { return read(str, v); }
std::istream& operator>>(std::istream& str, vec4_t& v) { return read(str, v); }
std::istream& operator>>(std::istream& str, quat_t& v) { return read(str, v); }
std::istream& operator>>(std::istream& str, box2_t& v) { return read(str, v); }
std::istream& operator>>(std::istream& str, box3_t& v) { return read(str, v); }
#else
std::istream& operator>>(std::istream&& str, vec2_t& v) { return read(str, v); }
std::istream& operator>>(std::istream&& str, vec3_t& v) { return read(str, v); }
std::istream& operator>>(std::istream&& str, vec4_t& v) { return read(str, v); }
std::istream& operator>>(std::istream&& str, quat_t& v) { return read(str, v); }
std::istream& operator>>(std::istream&& str, box2_t& v) { return read(str, v); }
std::istream& operator>>(std::istream&& str, box3_t& v) { return read(str, v); }
#endif

std::ostream& operator<<(std::ostream& str, const vec2_t& v) { return write(str, v); }
std::ostream& operator<<(std::ostream& str, const vec3_t& v) { return write(str, v); }
std::ostream& operator<<(std::ostream& str, const vec4_t& v) { return write(str, v); }
std::ostream& operator<<(std::ostream& str, const quat_t& v) { return write(str, v); }
std::ostream& operator<<(std::ostream& str, const box2_t& v) { return write(str, v); }
std::ostream& operator<<(std::ostream& str, const box3_t& v) { return write(str, v); }

#ifdef _MSC_VER
std::istream& operator>>(std::istream& str, color_t& v)
#else
std::istream& operator>>(std::istream&& str, color_t& v)
#endif
{
	float r = 1, g = 1, b = 1, a = 1;
	char comma = ',';
	str >> r >> comma >> g >> comma >> b >> comma >> a;
	v.SetAlpha(a), v.SetRed(r), v.SetGreen(g), v.SetBlue(b);
	return str;
}

std::ostream& operator<<(std::ostream& str, const color_t& v)
{
	return str << v.GetRed() << ',' << v.GetGreen() << ',' << v.GetBlue() << ',' << v.GetAlpha();
}
