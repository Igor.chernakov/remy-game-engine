#pragma once

#include <string>

#include "iobject.hpp"

struct IResource: IObject
{
	virtual const std::string& GetPath() const = 0;
};