//-----------------------------------------------------------------------------
// Copyright NVIDIA Corporation 2004
// TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THIS SOFTWARE IS PROVIDED 
// *AS IS* AND NVIDIA AND ITS SUPPLIERS DISCLAIM ALL WARRANTIES, EITHER EXPRESS 
// OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF 
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  IN NO EVENT SHALL 
// NVIDIA OR ITS SUPPLIERS BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT, OR 
// CONSEQUENTIAL DAMAGES WHATSOEVER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR 
// LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS 
// INFORMATION, OR ANY OTHER PECUNIARY LOSS) ARISING OUT OF THE USE OF OR 
// INABILITY TO USE THIS SOFTWARE, EVEN IF NVIDIA HAS BEEN ADVISED OF THE 
// POSSIBILITY OF SUCH DAMAGES.
// 
// File: Packer.h
// Desc: Header file Packer class
//-----------------------------------------------------------------------------

#ifndef PACKER_H
#define PACKER_H

#include <map>
#include <vector>

#include <stdio.h>
#include <assert.h>

#include <d3d9.h>
#include <d3d9types.h>
#include <d3dx9tex.h>
#include <dxerr.h>

#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>

namespace convert
{
	extern LPDIRECT3D9 d3d;
	extern LPDIRECT3DDEVICE9 device;
}

namespace nv 
{
	class Texture2D;
	class AtlasObject;

	enum {
		kFilenameLength    = 256,
		kPrintStringLength = 512,
	};

	class Packer2D;


	//-----------------------------------------------------------------------------
	// Name: OffsetStructure
	// Desc: Simple structure that is to indicate where in an atlas a texture
	//       was stored.
	//-----------------------------------------------------------------------------
	struct OffsetStructure
	{
		long   uOffset;
		long   vOffset;
		long   width;
		long   height;
		long   slice;
	};

	//-----------------------------------------------------------------------------
	// Name: TextureObject
	// Desc: Pure virtual base class to store texture objects.  The textures
	//       that need to be inserted into atlases, as well as the different
	//       types of atlases derive from this class.
	//-----------------------------------------------------------------------------
	class TextureObject
	{
	public:
		enum eTextureType
		{
			TEXTYPE_UNINITIALIZED = 0,
			TEXTYPE_2D,
			TEXTYPE_ATLAS2D,
			TEXTYPE_ATLASVOLUME,
			TEXTYPE_ATLASCUBE,
			TEXTYPE_NUM,
		};

	public:
		TextureObject();
		~TextureObject();

		virtual D3DFORMAT   GetFormat()                  const = 0;
		virtual bool        IsSupportedFormat(D3DFORMAT) const = 0;
		virtual long        GetNumTexels()               const = 0;

		LPDIRECT3DDEVICE9  GetDevice()   const;
		const std::string&        GetFilename() const;
		eTextureType        GetType()     const;

		void Init(LPDIRECT3DDEVICE9 pD3d, const std::string& pFilename);

	protected:
		void PrintError(char const * pText) const;

	protected:
		LPDIRECT3DDEVICE9          mpD3DDev;
		std::string                 mpFilename;
		eTextureType                mType;
	};

	//-----------------------------------------------------------------------------
	// Name: Texture2D
	// Desc: Derived class to store the textures to be inserted into atlases.
	//       that need to be inserted into atlases, as well as the different
	//       types of atlases derive from this class.
	//-----------------------------------------------------------------------------
	class AtlasObject;
	class Texture2D : public TextureObject
	{
	public:
		Texture2D();
		~Texture2D();

		virtual D3DFORMAT   GetFormat()                  const;
		virtual bool        IsSupportedFormat(D3DFORMAT) const;
		virtual long        GetNumTexels()               const;

		virtual long        GetWidth()  const;
		virtual long        GetHeight() const;

		HRESULT             LoadTexture(const char* mem, int size);
		void                SetAtlas(boost::shared_ptr<const AtlasObject> pAtlas, OffsetStructure const &offset);

		LPDIRECT3DTEXTURE9   GetD3DTexture() const;
		LPDIRECT3DTEXTURE9   GetD3DAtlasTexture() const;
		void                GetScaleFactors( float* fScaleX, float* fScaleY, float* fWidthOffset, float* fHeightOffset )const;

		const void* GetRenderInfo() const { return mpTexture2D; }
		const std::string& GetPath() const { return mpFilename; }

	private:
		LPDIRECT3DTEXTURE9          mpTexture2D;
		boost::shared_ptr<const AtlasObject> mpAtlas;
		OffsetStructure             mOffset;
	};

	//-----------------------------------------------------------------------------
	// Name: AtlasObject
	// Desc: Derived class that is pure virtual base class to the different 
	//       atlas types.
	//-----------------------------------------------------------------------------
	class AtlasObject : public TextureObject
	{
	public:
		AtlasObject();
		virtual ~AtlasObject();

		virtual D3DFORMAT   GetFormat() const = 0;
		virtual bool        IsSupportedFormat(D3DFORMAT) const = 0;
		virtual long        GetNumTexels()               const = 0;

		virtual bool Insert(Texture2D *pTexture) = 0;
		virtual void Shrink();
		virtual void WriteToDisk( const char* filename ) const = 0;
		virtual long GetWidth()    const = 0;
		virtual long GetHeight()   const = 0;

		int          GetId()       const;
		const std::string& GetFilename() const;

	protected:
		int         mAtlasId;
		std::string        mFilename;
	};

	//-----------------------------------------------------------------------------
	// Name: Atlas2D
	// Desc: Derived class representing 2D Atlases.
	//       Most notably it stores a pointer to a Packer2D object so it 
	//       knows how to deal w/ insertions.
	//-----------------------------------------------------------------------------
	class Atlas2D : public AtlasObject
	{
		Atlas2D( const std::string& szAtlasFileName, LPDIRECT3DDEVICE9 pDev, int uiWidth, int uiHeight );

	public:
		static boost::shared_ptr<Atlas2D> Create( const std::string& szAtlasFileName, LPDIRECT3DDEVICE9 pDev, int uiWidth, int uiHeight );

		virtual ~Atlas2D();

		virtual D3DFORMAT   GetFormat()                  const;
		virtual bool        IsSupportedFormat(D3DFORMAT) const;
		virtual long        GetNumTexels()               const;

		virtual bool        Insert(Texture2D *pTexture);
		virtual void        Shrink();
		virtual void        WriteToDisk( const char* filename ) const;
		virtual long        GetWidth()    const;
		virtual long        GetHeight()   const;

		LPDIRECT3DTEXTURE9  GetD3DTexture() const;

	private:
		LPDIRECT3DTEXTURE9          mpTexture2D;
		Packer2D *                  mpPacker2D;
	};

	//-----------------------------------------------------------------------------
	// Name: Texture2DGreater
	// Desc: struct used for sorting of Texture2D objects
	//-----------------------------------------------------------------------------
	typedef struct _TEXTURE2DGREATER 
	{
		bool operator()(Texture2D const *s1, Texture2D const *s2) const
		{
			if (s1->GetHeight()*s1->GetWidth() > s2->GetHeight()*s2->GetWidth())
				return true;
			else if (   (s1->GetHeight() > s2->GetHeight())
				&& (s1->GetHeight()*s1->GetWidth() == s2->GetHeight()*s2->GetWidth()))
				return true;
			else if (   (s1->GetWidth()  > s2->GetWidth())
				&& (s1->GetHeight()*s1->GetWidth() == s2->GetHeight()*s2->GetWidth()))
				return true;
			else 
				return false;
		}
	} Texture2DGreater;

	//-----------------------------------------------------------------------------
	// Name: Region
	// Desc: Simple calss representing a rectangle. It knows how to intersect 
	//       against other such rectangles.
	//       This class is used to store dirty (used) rectangles representing 
	//       areas in a texture that valid data has been copied into.
	//       A collection of these thus shows which texels are not 
	//       available for storing new information (and thus which texels
	//       *are* available).
	//-----------------------------------------------------------------------------
	class Region
	{
	public:
		Region();
		~Region();

		long GetWidth() const;
		long GetHeight() const;
		bool Intersect(Region const &region) const; 

	public:
		long     mLeft;
		long     mRight;
		long     mTop;
		long     mBottom;
	};

	//-----------------------------------------------------------------------------
	// Name: Packer
	// Desc: Pure virtual base class for Packer objects.  For example, a Packer2D
	//       object knows how to insert new 2D textures into a 2D atlas.
	//       The base class defines the common entry points and some house-keeping 
	//       data
	//-----------------------------------------------------------------------------
	class Packer
	{
	public:
		Packer();
		~Packer();

		virtual bool Insert(Texture2D *pTexture) = 0; 

		int GetMaxMipLevels() const;

	protected:
		int  SizeOfTexel (D3DFORMAT format) const;
		bool IsDXTnFormat(D3DFORMAT format) const;

	protected:
		int     mTotalFreeTexels;
		int     mMaxNumberMipLevels;
	};

	//-----------------------------------------------------------------------------
	// Name: Packer2D
	// Desc: Derived class that knows how to deal with Atlas2D objects,
	//       specifically how to insert Texture2D objects into Atlas2D objects
	//-----------------------------------------------------------------------------
	class Packer2D : public Packer
	{
	public:
		Packer2D(boost::shared_ptr<Atlas2D> pAtlas);
		~Packer2D();

		virtual bool Insert(Texture2D *pTexture); 

		Region const * Intersects(Region const &region) const;
		void           CopyBits(Region const &test, LPDIRECT3DTEXTURE9 pTexture);

	private:
		void Merge(Region * pNewRegion);

	private:
		boost::weak_ptr<Atlas2D> mpAtlas;
		std::vector<Region *>   mUsedRegions;

	};
}

#include <vector>
namespace convert {
class PackedTexture
{
public:
	PackedTexture(const char* mem, int size);
	~PackedTexture() { delete m_texture; }

	virtual const std::string& GetPath() const { return m_name; }
	virtual int GetWidth() const { return m_texture ? m_texture->GetWidth() : 0;; }
	virtual int GetHeight() const { return m_texture ? m_texture->GetHeight() : 0; }
	virtual D3DFORMAT GetFormat() const { return m_texture ? m_texture->GetFormat() : D3DFMT_UNKNOWN; }
	virtual const void* GetRenderInfo() const { return m_texture ? m_texture->GetD3DAtlasTexture() : 0; }
	virtual const float* GetActiveArea() const 
	{
		if (m_texture)
		{
			m_texture->GetScaleFactors(&m_maxx, &m_maxy, &m_minx, &m_miny);
			m_maxx += m_minx;
			m_maxy += m_miny;
		}
		return &m_minx; 
	}

protected:
	std::string m_name;
	nv::Texture2D* m_texture;
	mutable float m_minx, m_miny, m_maxx, m_maxy;
};

extern std::vector<boost::weak_ptr<nv::Atlas2D> > packers;
}

#endif // PACKER_H

