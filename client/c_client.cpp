#include "c_client.hpp"

#include "../shared/shared.hpp"
#include "../math/math.hpp"

#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include "../interfaces/iclient.hpp"

#include "c_material.hpp"

#include <string>
#include <fstream>
#include <streambuf>

std::vector<std::wstring> g_loadingStrings;

void AddLoadingString(const std::wstring& loading) {
	g_loadingStrings.push_back(loading);
}

std::wstring GetLoadingString() {
	if (g_loadingStrings.size() == 0)
		return L"";
	return g_loadingStrings[rand() % g_loadingStrings.size()];
}



#ifdef WIN32
#	pragma comment(lib, "python27.lib")
#endif


static std::string GameDirectory;

std::string documents_dir;

const IFramework* GetFramework()
{
	return framework;
}

IBucket* CreateBucket();
ILocalization* CreateLocalization();
IRendererObjectManager* CreateRendererObjectManager();

extern bool b_IsFirstFrame;
extern bool b_IsLoadingScreen;

#ifdef WIN32
#include <Shlobj.h>
std::string GetGameFolder() {
	char szPath[MAX_PATH];
	GetCurrentDirectoryA(MAX_PATH, szPath);
	return std::string(szPath) + "/" + GameDirectory;
}
#include <direct.h>
void CreateFolder(const std::string& folder) {
	_mkdir( folder.c_str() );
}
std::string GetAppDataFolder() {
	//char szPath[MAX_PATH];
	//SHGetFolderPathA(NULL, CSIDL_APPDATA|CSIDL_FLAG_CREATE, NULL, 0, szPath);
	//return szPath;
	return documents_dir;
}

#elif defined __APPLE__
std::string GetGameFolder() {
	return std::string(".");
}

#include <sys/types.h>
#include <sys/stat.h>

void CreateFolder(const std::string& folder) {
	mkdir(folder.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
}
std::string GetAppDataFolder() {
	//char szPath[MAX_PATH];
	//SHGetFolderPathA(NULL, CSIDL_APPDATA|CSIDL_FLAG_CREATE, NULL, 0, szPath);
	//return szPath;
	return documents_dir;
}

#else

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

std::string GetAppDataFolder() {
    char path[MAX_PATH] = {0};
    char *home = getenv ("HOME");
    if (home != NULL) {
        snprintf(path, MAX_PATH, "%s/.amongtheheavens", home);
        return path;
    } else {
        return ".";
    }
}

std::string GetGameFolder() {
	//return ".";
    char szPath[MAX_PATH];
	getcwd(szPath, MAX_PATH);
	return std::string(szPath);
}

void CreateFolder(const std::string& folder) {
    struct stat st = {0};
    if (stat(folder.c_str(), &st) == -1) {
        mkdir(folder.c_str(), 0700);
    }
}

#endif

#ifdef ENGINE_MULTITHREADING
//some multithreading sync flags
bool
	g_bCanRender = false,
	g_bCanUpdate = true,
	g_bCanQuit = false;
#endif

bool
	g_bTakeScreenshot = false;

IRenderer* __HackRenderer = 0;

int InitializePython();

struct dispose_object_t
{
	const char* file;
	PyObject* obj;
	int count;
};

std::list<dispose_object_t> pyobjects;

void DisposablePyObject(const char* file, PyObject *pyobject, int count)
{
	dispose_object_t do_ = { file, pyobject, count };
	pyobjects.push_back(do_);
}

bool g_DeveloperMode = false;
bool g_DontCompile = false;
bool g_Phone = false;
bool g_Mobile = false;
bool b_WriteLogGA = false;

namespace client
{
	std::list<client_message_t> g_Messages;

	class Client: public IClient
	{
	public:
		Client(int argc, char **argv, const std::string& directory, IRenderer* renderer)
			: renderer(renderer)
			//, socket(io_service)
		{

			#define PARSE_ARGUMENT_FLAG(Flag) else if () g_AppFlags |= APPFLAG_##Flag;

			for (int i = 0; i < argc && argv; ++i)
			{
				std::string command = argv[i];
				g_DeveloperMode |= boost::iequals(command.c_str(), "-developer");
				g_DontCompile |= boost::iequals(command.c_str(), "-nocompile");
				g_Phone |= boost::iequals(command.c_str(), "-phone");
				b_WriteLogGA |= boost::iequals(command.c_str(), "-logGA");
			}


			GameDirectory = directory;

			framework->SetBucket(shared_ptr<IBucket>(CreateBucket()));
			framework->SetResources(shared_ptr<IResources>(new Manager<IResource>()));
			framework->SetLocalization(shared_ptr<ILocalization>(CreateLocalization()));
			framework->SetRendererObjectManager(shared_ptr<IRendererObjectManager>(CreateRendererObjectManager()));

			framework->GetFileSystem()->AddPath(GameDirectory);
			framework->GetFileSystem()->AddPath(GameDirectory + "/renderer/" + renderer->GetStats().name);

			if (g_Phone)
				framework->GetFileSystem()->AddPath(GameDirectory + "/phone");

#if defined(__APPLE__) && TARGET_OS_IPHONE
			framework->GetFileSystem()->AddPath(GameDirectory + "/mobile");
#endif
			srand(framework->GetTimer()->GetMilliseconds());

			//PyThreadState_Swap(Py_NewInterpreter());
			InitializePython();

			if (g_DontCompile)
			{
				PyDict_SetItemString(PyModule_GetDict(PyImport_ImportModule("sys")), "dont_write_bytecode", PyBool_FromLong(true));
			}

#ifdef WIN32
			PySys_SetArgvEx(argc, argv, 0);
#endif
			PySys_SetPath((char*)(GameDirectory + "/scripts").c_str());

#if defined __APPLE__
			boost::python::import("sys").attr("path").attr("append")((GameDirectory + "/scripts"));
#endif

			framework->Log(IFramework::MSG_LOG, "Client initialized");

			__HackRenderer = renderer;
		}

		~Client()
		{
			if(PyErr_Occurred())
			{
				PyErr_Print();
				PyErr_Clear();
			}
		}

		void PublishMessage(const std::string& message, const color_t& color)
		{
			client_message_t m = { color, message };
			g_Messages.push_back(m);
		}

		bool Connect(const std::string& ip, int port)
		{
			framework->Log(IFramework::MSG_LOG, "Connecting to %s", ip.c_str());

			/*asio::ip::tcp::resolver resolver(io_service);
			asio::ip::tcp::resolver::query query(ip, boost::lexical_cast<std::string>(port));
			asio::ip::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
			asio::ip::tcp::resolver::iterator end;

			boost::system::error_code error = asio::error::host_not_found;

			while (error && endpoint_iterator != end)
			{
				socket.close();
				socket.connect(*endpoint_iterator++, error);
			}

			if (error)
			{

				framework->Log(IFramework::MSG_ERROR, "Could not connect to %s", ip.c_str());
				return false;
			}

			framework->Log(IFramework::MSG_LOG, "Connection successful");

			*/

			return true;
		}

		void CreateRenderer()
		{
#ifdef WIN32
			if (!renderer->Open(framework->GetWindow().get()))
			{
				MessageBoxW(
					NULL,
					(LPCWSTR)L"Unfortunately your hardware is not capable of running the game.",
					(LPCWSTR)L"Video device is not supported",
					MB_ICONERROR | MB_OK
				);
				throw std::runtime_error("Can't open renderer device");
			}
			else
			{
				framework->GetWindow()->Show(true);
			}
#endif
		}

		bool RenderFrame()
		{
#ifdef __APPLE__
			if (!framework->GetScene())
			{
				renderer->StartFrame();
				renderer->FinishFrame();
				return true;
			}

			if (!framework->GetScene()->IsActive())
			{
				return false;
			}

			if (b_IsLoadingScreen)
			{
				framework->GetScene()->RenderLoading(renderer);
				return true;
			}

			if (!g_bCanRender)
			{
				//usleep(10000);

				if (b_IsLoadingScreen)
				{
					framework->GetScene()->RenderFrame(renderer);
					return true;
				}

				if (!framework->GetScene() || !framework->GetScene()->IsActive())
				{
					return false;
				}

				return false;
			}

			g_bCanRender = false;

			framework->GetRendererObjectManager()->Update(renderer);
			framework->GetScene()->RenderFrame(renderer);

			if (g_bTakeScreenshot)
			{
				renderer->TakeScreenshot();
				g_bTakeScreenshot = false;
			}

			g_bCanUpdate = true;
#endif
			return true;
		}

#if defined ENGINE_MULTITHREADING && defined WIN32
		void RenderThread()
		{
			CreateRenderer();

			while (!g_bCanQuit)
			{
				if (!framework->GetScene() || !framework->GetScene()->IsActive())
				{
					Sleep(10);
				}
				else if (b_IsLoadingScreen)
				{
					framework->GetScene()->RenderLoading(renderer);
				}
				else if (g_bCanRender)
				{
					g_bCanRender = false;

					framework->GetRendererObjectManager()->Update(renderer);
					framework->GetScene()->RenderFrame(renderer);

					if (g_bTakeScreenshot)
					{
						renderer->TakeScreenshot();
						g_bTakeScreenshot = false;
					}

					g_bCanUpdate = true;
				}
				else
				{
					Sleep(0);
				}
			}

			framework->GetRendererObjectManager()->Unload(renderer);
		}
#endif

		void UpdateThread()
		{
			PyImport_ImportModule("start");
			/*if (PyErr_Occurred()) {
				PyObject *ptype, *pvalue, *ptraceback;
				PyErr_Fetch(&ptype, &pvalue, &ptraceback);
				framework->Log(IFramework::MSG_ERROR, "%s", PyString_AsString(pvalue));
				PyErr_Clear();
			}*/
		}

		bool play_sound;

		void SoundThread()
		{
			play_sound = true;

			while (play_sound)
			{
				Sleep(50);

				framework->GetSoundSystem()->Update(0);
			}
		}

		void Run()
		{
			boost::thread sound(boost::bind(&Client::SoundThread, this));

#ifdef ENGINE_MULTITHREADING
	#ifdef __APPLE__
			UpdateThread(); // rendering is handled elsewhere
	#else
			boost::thread render(boost::bind(&Client::RenderThread, this));

			UpdateThread();

			g_bCanQuit = true;

			render.join();

			framework->SetScene(null_ptr());
	#endif
#else
			CreateRenderer();



			UpdateThread();

			framework->GetRendererObjectManager()->Unload(renderer);
			framework->SetScene(null_ptr());
#endif

#ifdef _DEBUG && defined WIN32
			for (std::list<dispose_object_t>::reverse_iterator it = pyobjects.rbegin(); it != pyobjects.rend(); it++)
			{
				for (int count = (*it).obj->ob_refcnt; count > 0; count--)
				{
					Py_DECREF((*it).obj);
				}
			}
#endif
			play_sound = false;

			sound.join();
		}

		void DeleteShit(PyObject* op, int step)
		{
			for (int count = op->ob_refcnt; count > 0; count -= step)
			{
				for (int i = 0; i < step; i++)
				{
					Py_DECREF(op);
				}
			}
		}

		PyObject* FindShitByName(const char* name)
		{
			for (std::list<dispose_object_t>::iterator it = pyobjects.begin(); it != pyobjects.end(); it++)
			{
				if(strstr((*it).file, name) != NULL) {
					return (*it).obj;
				}
			}
			return 0;
		}

		const char* CompileDate() const
		{
			return __DATE__;
		}

		void Evaluate(const std::string& command)
		{
			//framework->GetScene()->Evaluate(command);
		}

		void TakeScreenshot()
		{
			g_bTakeScreenshot = true;
		}

	private:
		//asio::io_service io_service;
		//asio::ip::tcp::socket socket;
		IRenderer* renderer;
	};
}

namespace boost
{
	template<class T>
	inline T* get_pointer(boost::shared_ptr<const T> const& p)
	{
		return const_cast<T*>(p.get());
	}

	namespace python
	{
		template<class T>
		struct pointee<boost::shared_ptr<T const> >
		{
			typedef T type;
		};
	}
}

namespace py = boost::python;

#ifdef WITH_SQLITE

/**
	Basic SQLITE3 iterface
	The best part is an easy acces to results of queries: like result[0].id, result[0].name, ....
**/

#include "sqlite3/sqlite3.h"
#include "sqlite3/sqliteInt.h"

namespace __db
{
	class Database: public IResource
	{
	public:
		class Respond
		{
		public:
			py::object get(const std::string& name) const
			{
				try
				{
					return this->m_dict[name];
				}
				catch (const boost::python::error_already_set&)
				{
					PyErr_Clear();
					return py::object();
				}
			}
			void set(const std::string& name, py::object obj)
			{
				this->m_dict[name] = obj;
			}

		public:
			py::dict m_dict;
		};

	public:
		Database(const std::string& name): db(0), path(GameDirectory / name)
		{
			if(sqlite3_open_v2(name.c_str(), &db, SQLITE_OPEN_READWRITE, 0))
			{
				sqlite3_close(db), db = 0;
				if (sqlite3_open_v2(GetPath().c_str(), &db, SQLITE_OPEN_READWRITE, 0))
				{
					framework->Log(IFramework::MSG_ERRROR, "Database %s was not found", name.c_str());
					sqlite3_close(db), db = 0;
				}
			}
		}
		~Database()
		{
			if (db)
			{
				sqlite3_close(db);
			}
		}

		py::list Query(const std::string& query)
		{
			sqlite3_stmt* stmt;

			if (sqlite3_prepare(this->db, query.c_str(), query.length(), &stmt, 0))
			{
				sqlite3_finalize(stmt);
				return py::list();
			}

			py::list result;

			while (sqlite3_step(stmt) == SQLITE_ROW)
			{
				shared_ptr<Respond> e(new Respond());

				for (int i(0); i < sqlite3_column_count(stmt); ++i)
				{
					std::string name(sqlite3_column_name(stmt, i));

					switch (sqlite3_column_type(stmt, i))
					{
					case SQLITE_INTEGER:
						e->set(name, py::object(sqlite3_column_int(stmt, i)));
						break;
					case SQLITE_TEXT:
						e->set(name, py::object(std::string((const char*)sqlite3_column_text(stmt, i))));
						break;
					case SQLITE_FLOAT:
						e->set(name, py::object(sqlite3_column_double(stmt, i)));
						break;
					}
				}
				result.append(e);
			}

			sqlite3_finalize(stmt);

			return result;
		}

		virtual const std::string& GetPath() const { return path; }

	private:
		sqlite3* db;
		const std::string path;
	};
}

void export_SqLite3()
{
	using __db::Database;

	py::scope _e = py::class_<Database, shared_ptr<Database>, boost::noncopyable>("Database", py::init<const std::string&>())
		.def("Query", &Database::Query)
		;

	DISPOSEME(2) py::class_<Database::Respond, shared_ptr<Database::Respond>, boost::noncopyable>("Respond", py::no_init)
		.def("__getattr__", &Database::Respond::get)
		;

	DISPOSEME(3) _e;
}

#endif

const int MAGIC_GA_NUMBER = 123456789;

#if defined(__APPLE__) && TARGET_OS_IPHONE
extern "C" {
void GA_addProgressionStartEvent(const char *progression);
void GA_addProgressionCompleteEvent(const char *progression, int score);
void GA_addProgressionFailEvent(const char *progression, int score);
void GA_addErrorEvent(const char *msg);
void GA_addDesignEvent(const char *evt, int value);
}
#else
void GA_addProgressionStartEvent(const char *progression) {
	if (!b_WriteLogGA)
		return;
	framework->Log(IFramework::MSG_LOG, "ga_start: %s", progression);
}
void GA_addProgressionCompleteEvent(const char *progression, int score) {
	if (!b_WriteLogGA)
		return;
	framework->Log(IFramework::MSG_LOG, "ga_complete: %s, %d", progression, score);
}
void GA_addProgressionFailEvent(const char *progression, int score) {
	if (!b_WriteLogGA)
		return;
	framework->Log(IFramework::MSG_LOG, "ga_fail: %s, %d", progression, score);
}
void GA_addErrorEvent(const char *msg) {
	if (!b_WriteLogGA)
		return;
	framework->Log(IFramework::MSG_LOG, "ga_error: %s", msg);
}
void GA_addDesignEvent(const char *evt, int value) {
	if (!b_WriteLogGA)
		return;
	if (value == MAGIC_GA_NUMBER) {
		framework->Log(IFramework::MSG_LOG, "ga_evt: %s", evt);
	} else {
		framework->Log(IFramework::MSG_LOG, "ga_evt: %s, %d", evt, value);
	}
}
#endif

void export_GA() {
	py::def("addProgressionStartEvent", &GA_addProgressionStartEvent);
	py::def("addProgressionCompleteEvent", &GA_addProgressionCompleteEvent);
	py::def("addProgressionFailEvent", &GA_addProgressionFailEvent);
	py::def("addErrorEvent", &GA_addErrorEvent);
	py::def("addDesignEvent", &GA_addDesignEvent, (py::arg("event"), py::arg("value") = MAGIC_GA_NUMBER));
}

#include <sstream>

template <typename T>
T GetVar_TXML(const tinyxml2::XMLElement *el, const char* name, const T& _default) {
	const tinyxml2::XMLAttribute *attr = el->FindAttribute(name);
	if (!attr)
		return _default;
	T var = _default;
	std::stringstream(attr->Value()) >> var;
	return var;
};

std::string GetVar_TXML_string(const tinyxml2::XMLElement *el, const char* name, const std::string& _default) {
	const tinyxml2::XMLAttribute *attr = el->FindAttribute(name);
	if (!attr)
		return _default;
	return attr->Value();
};

void export_TinyXml2() {
	typedef tinyxml2::XMLElement T;
	using tinyxml2::XMLDocument;

	struct f_{
		static shared_ptr<XMLDocument> LoadDocument(const std::string& filename) {
			file_t file(framework->GetFileSystem()->LoadFile(filename));
			shared_ptr<XMLDocument> doc(new XMLDocument);
			//bool success = doc->LoadFile(file) == tinyxml2::XML_NO_ERROR;
			doc->LoadFile(file);
			file.close();
			return doc;
		}
		static py::dict GetDict(const T* el) {
			py::dict dict;
			for (const tinyxml2::XMLAttribute* a = el->FirstAttribute(); a; a = a->Next()) {
				dict[a->Name()] = a->Value();
			}
			return dict;
		}
		static const T *FirstChildElement2(const tinyxml2::XMLDocument* doc) {
			return doc->FirstChildElement();
		}
		static const T *FirstChildElement(const tinyxml2::XMLDocument* doc, const char *c) {
			return doc->FirstChildElement(c);
		}
		static std::string FindAttribute(const T* el, const char *name) {
			const tinyxml2::XMLAttribute* a = el->FindAttribute(name);
			return a ? a->Value() : "";
		}
		static const T* NextElement2(const T* el, const std::pair<std::string, std::string>& pair) {
			XML_FOREACH(x, 0, el) {
				const tinyxml2::XMLAttribute *at = x->FindAttribute(pair.first.c_str());
				if (at && pair.second == at->Value()) {
					return x;
				}
			}
			return NULL;
		}
		static py::list GetElNodes(const T* doc) {
			py::list ls;
			XML_FOREACH(x, 0, doc) {
				ls.append(boost::ref(x));
			}
			return ls;
		}
		static const T* FindElementByAttribute(const T* el, const char *attr, const char *value) {
			XML_FOREACH(x, 0, el) {
				const tinyxml2::XMLAttribute *at = x->FindAttribute(attr);
				if (at && !strcmp(value, at->Value())) {
					return x;
				}
			}
			return NULL;
		}
	};

	DISPOSEME(3) py::class_<T, boost::noncopyable>("XmlElement", py::no_init)
		.def("get", &GetVar_TXML<float>)
		.def("get", &GetVar_TXML<color_t>)
		.def("get", &GetVar_TXML<vec2_t>)
		.def("get", &GetVar_TXML<vec3_t>)
		.def("get", &GetVar_TXML<vec4_t>)
		.def("get", &GetVar_TXML<quat_t>)
		.def("get", &GetVar_TXML<box2_t>)
		.def("get", &GetVar_TXML<box3_t>)
		.def("get", &GetVar_TXML_string)
		.def("__rshift__", (T* (T::*)(const char*))&T::FirstChildElement, py::return_internal_reference<>())
		.def("__rshift__", &f_::NextElement2, py::return_internal_reference<>())
		.def("__getitem__", &f_::FindAttribute)
		.def("FindElementByAttribute", &f_::FindElementByAttribute, py::return_internal_reference<>())
		.add_property("dict", &f_::GetDict)
		.add_property("name", &T::Name)
		.add_property("text", &T::GetText)
		.add_property("nodes", f_::GetElNodes)
		;

	DISPOSEME(3) py::class_<XMLDocument, shared_ptr<XMLDocument>, boost::noncopyable>("Reader", py::no_init)
		.def("__init__", py::make_constructor(&f_::LoadDocument))
		.def("__rshift__", &f_::FirstChildElement, py::return_internal_reference<>())
		.add_property("root", py::make_function(&f_::FirstChildElement2, py::return_internal_reference<>()))
		;
}

void export_Sound()
{
	struct functions_
	{
		static shared_ptr<const ISound> LoadSound(const std::string& name) { return framework->GetSoundSystem()->LoadSound(name); }
	};

	DISPOSEME(3) py::class_<ISound, shared_ptr<ISound>, boost::noncopyable>("Sound", py::no_init)
		.def("__init__", py::make_constructor(&functions_::LoadSound))
		.def("Play", &ISound::Play, (py::arg("volume") = 1.0f, py::arg("loop") = false))
		.def("Pause", &ISound::Pause)
		.def("Stop", &ISound::Stop)
		.add_property("loaded", &ISound::IsLoaded)
		.add_property("volume", &ISound::GetVolume, &ISound::SetVolume)
		.add_property("length", &ISound::GetLength)
		;

	py::register_ptr_to_python<shared_ptr<const ISound> >();
	py::implicitly_convertible<shared_ptr<ISound>, shared_ptr<const ISound> >();
}

void export_Material()
{
	using client::Material;

	struct functions_
	{
		static shared_ptr<const IMaterial> LoadMaterial(const std::string& name)
		{
			return framework->GetResources()->Load<Material>(name);
		}
	};

	{
		py::scope scope_ = py::class_<IMaterial, shared_ptr<IMaterial>, boost::noncopyable>("Material", py::no_init)
			.def("__init__", py::make_constructor(&functions_::LoadMaterial))
			.add_property("width", &IMaterial::GetWidth)
			.add_property("height", &IMaterial::GetHeight)
			;

		py::enum_<IMaterial::Pass>("Pass")
			.value("Solid", IMaterial::RENDER_SOLID)
			.value("Alpha", IMaterial::RENDER_ALPHA)
			.value("PlanarShadow", IMaterial::RENDER_PLANARSHADOWS)
			.value("VolumeShadow", IMaterial::RENDER_VOLUMESHADOWS)
			.value("ShadowMap", IMaterial::RENDER_SHADOWMAP)
			.value("Texture", IMaterial::RENDER_TEXTURE)
			.value("Stencil1", IMaterial::RENDER_INNER)
			.value("Stencil2", IMaterial::RENDER_OUTER)
			.export_values()
			;

		DISPOSEME(4) scope_;
	}

	struct MaterialProxy
	{
		MaterialProxy(const std::string& str)
			: value(functions_::LoadMaterial(str))
		{}
		operator shared_ptr<const IMaterial>() const { return value; }
		shared_ptr<const IMaterial> value;
	};

	py::register_ptr_to_python<shared_ptr<const IMaterial> >();
	py::implicitly_convertible<shared_ptr<IMaterial>, shared_ptr<const IMaterial> >();
	py::implicitly_convertible<std::string, MaterialProxy>();
	py::implicitly_convertible<MaterialProxy, shared_ptr<const IMaterial> >();
}

template <int type>
class WriteBuffer {
	std::string buffer;
public:
	void write(const std::string& m) {
		const char* s = m.c_str();
		for (const char* c = s; *c; c++) {
			if (*c == '\n') {
				if (type == IFramework::MSG_ERROR)  {
					GA_addErrorEvent(buffer.c_str());
				}
				framework->Log(IFramework::msg_type_t(type), buffer.c_str());
				buffer = std::string();
			} else {
				buffer.push_back(*c);
			}
		}
	}
};

void export_ErrorWriter()
{
#define EXPORT_WRITER(STREAM, PYNAME) \
	typedef WriteBuffer<IFramework::STREAM> STREAM##_writer;\
	static STREAM##_writer STREAM##_writer_instance; \
	DISPOSEME(5) py::class_<STREAM##_writer>(#STREAM"_writer").def("write", &STREAM##_writer::write); \
	py::import("sys").attr(PYNAME) = STREAM##_writer_instance;

	EXPORT_WRITER(MSG_ERROR, "stderr");
	EXPORT_WRITER(MSG_LOG, "stdout");

#undef EXPORT_WRITER
}

void export_Framework()
{
	DISPOSEME(4) py::class_<point_t>("Point")
		.def(py::init<int, int>())
		.def(py::init<std::string>())
		.def_readwrite("x", &point_t::x)
		.def_readwrite("y", &point_t::y)
		;

	DISPOSEME(4) py::class_<dim_t>("Size")
		.def(py::init<int, int>())
		.def(py::init<std::string>())
		.def_readwrite("width", &dim_t::width)
		.def_readwrite("height", &dim_t::height)
		;

	DISPOSEME(4) py::class_<range_t>("Range")
		.def(py::init<int, int>())
		.def(py::init<std::string>())
		.def_readwrite("min", &range_t::min_)
		.def_readwrite("max", &range_t::max_)
		;

	DISPOSEME(4) py::class_<rect_t>("Rectangle")
		.def(py::init<int, int, int, int>())
		.def(py::init<std::string>())
		.def_readwrite("left", &rect_t::left)
		.def_readwrite("right", &rect_t::right)
		.def_readwrite("top", &rect_t::top)
		.def_readwrite("bottom", &rect_t::bottom)
		.add_property("location", &rect_t::GetPosition, &rect_t::SetPosition)
		.add_property("size", &rect_t::GetSize, &rect_t::SetSize)
		;

	py::implicitly_convertible<std::string, point_t>();
	py::implicitly_convertible<std::string, dim_t>();
	py::implicitly_convertible<std::string, range_t>();
	py::implicitly_convertible<std::string, rect_t>();

	DISPOSEME(4) py::class_<std::vector<std::string> >("StringList")
        .def(py::vector_indexing_suite<std::vector<std::string> >())
    ;

	DISPOSEME(4) py::class_<IResources, shared_ptr<IResources>, boost::noncopyable>("Resources")
		.def("Append", &IResources::Append)
		.def("Clear", &IResources::Clear)
		;

	DISPOSEME(4) py::class_<ISoundSystem, shared_ptr<ISoundSystem>, boost::noncopyable>("ISoundSystem", py::no_init)
		.add_property("sound_enabled", &ISoundSystem::IsSoundEnabled, &ISoundSystem::SetSoundEnabled)
		.add_property("music_enabled", &ISoundSystem::IsMusicEnabled, &ISoundSystem::SetMusicEnabled)
		.def("LoadMusic", &ISoundSystem::LoadMusic)
		.def("LoadSound", &ISoundSystem::LoadSound)
		;

	DISPOSEME(4) py::class_<IFileSystem, shared_ptr<IFileSystem>, boost::noncopyable>("IFileSystem", py::no_init)
		.def("CheckFile", &IFileSystem::CheckFile)
		.def("AddPath", &IFileSystem::AddPath)
		;

	{
		struct functions_ {
#ifdef WIN32
			static void SetClipboard(IWindow* window, const std::string& value) {
				if (HGLOBAL hglb = GlobalAlloc(GMEM_MOVEABLE, 1024)) {
					memcpy(GlobalLock(hglb), value.c_str(), value.length() + 1);
					GlobalUnlock(hglb);
					OpenClipboard(window->GetWindow());
					EmptyClipboard();
					SetClipboardData(CF_TEXT, hglb);
					CloseClipboard();
				}
			}
			static std::string GetClipboard(const IWindow* window) {
				std::string value;
				if (IsClipboardFormatAvailable(CF_TEXT) && OpenClipboard(window->GetWindow())) {
					if (HGLOBAL hglb = GetClipboardData(CF_TEXT)) {
						if (void* lptstr = GlobalLock(hglb)) {
							value = (const char*)lptstr;
							GlobalUnlock(hglb);
						}
					}
					CloseClipboard();
				}
				return value;
			}
#endif
			static bool GetFullscreen(const IWindow* window) {
				return __HackRenderer->IsFullscreen();
			}
			static void SetFullscreen(const IWindow* window, bool value) {
				__HackRenderer->SetFullscreen(value);
			}
			static bool GetWidescreen(const IWindow* window) {
				return false;
			}
			static void SetWidescreen(const IWindow* window, bool value) {
			}
		};

		DISPOSEME(4) py::class_<ITimer, shared_ptr<ITimer>, boost::noncopyable>("ITimer", py::no_init)
			.add_property("msec", &ITimer::GetMilliseconds)
			.add_property("fps", &ITimer::GetFPS)
			;

		py::scope _e = py::class_<IWindow, shared_ptr<IWindow>, boost::noncopyable>("IWindow", py::no_init)
			.add_property("mouse", &IWindow::GetCursorPos, &IWindow::SetCursorPos)
			.add_property("cursor", &IWindow::GetCursor, &IWindow::SetCursor)
#ifdef WIN32
			.add_property("clipboard", &functions_::GetClipboard, &functions_::SetClipboard)
#endif
			.add_property("fullscreen", &functions_::GetFullscreen, &functions_::SetFullscreen)
			.add_property("widescreen", &functions_::GetWidescreen, &functions_::SetWidescreen)
			.add_property("size", &IWindow::GetSize)
			;

		py::enum_<IWindow::Cursor>("Cursor")
			.value("None", IWindow::CURSOR_NONE)
			.value("Arrow", IWindow::CURSOR_ARROW)
			.value("Hand", IWindow::CURSOR_HAND)
			.value("Hold", IWindow::CURSOR_HOLD)
			.value("Wait", IWindow::CURSOR_WAIT)
			.export_values()
			;

		DISPOSEME(4) _e;
	}

	DISPOSEME(4) py::class_<IClientScene, shared_ptr<IClientScene>, boost::noncopyable>("IScene", py::no_init)
		.def("Run", &IClientScene::Run)
		.add_property("navigation", &IClientScene::GetNavigation, &IClientScene::SetNavigation)
		;

	DISPOSEME(4) py::class_<IBucket, shared_ptr<IBucket>, boost::noncopyable>("IBucket", py::no_init)
		;

	struct f_
	{
		static void SetScene(shared_ptr<IClientScene> scene) { return framework->SetScene(scene); }
		static shared_ptr<IClientScene> GetScene() { return framework->GetScene(); }
		static shared_ptr<ISoundSystem> GetSoundSystem() { return framework->GetSoundSystem(); }
		static shared_ptr<IWindow> GetWindow() { return framework->GetWindow(); }
		static shared_ptr<IFileSystem> GetFileSystem() { return framework->GetFileSystem(); }
		static shared_ptr<IResources> GetResources() { return framework->GetResources(); }
		static shared_ptr<ITimer> GetTimer() { return framework->GetTimer(); }
		static shared_ptr<IVariables> GetVariables() { return framework->GetVariables(); }
		static shared_ptr<IBucket> GetBucket() { return framework->GetBucket(); }
		static shared_ptr<ILocalization> GetLocalization() { return framework->GetLocalization(); }
	};

#if defined(__APPLE__) && TARGET_OS_IPHONE
	g_Mobile = true;
#endif

	DISPOSEME(4) py::class_<IClientFramework, boost::noncopyable>("Lib", py::no_init)
		.add_static_property("scene", &f_::GetScene, &f_::SetScene)
		.add_static_property("manager", &f_::GetResources)
		.add_static_property("window", &f_::GetWindow)
		.add_static_property("vars", &f_::GetBucket)
		.add_static_property("sound", &f_::GetSoundSystem)
		.add_static_property("file", &f_::GetFileSystem)
		.add_static_property("timer", &f_::GetTimer)
		.add_static_property("texts", &f_::GetLocalization)
		.add_static_property("path_appdata", &GetAppDataFolder)
		.add_static_property("path_game", &GetGameFolder)
		.def("path_create", &CreateFolder).staticmethod("path_create")
		.def("addLoadingString", &AddLoadingString).staticmethod("addLoadingString")
		.def_readonly("phone", &g_Phone)
		.def_readonly("mobile", &g_Mobile)
		;
}

struct File {
	File(const std::string& path, const std::string& mode) {
		file = fopen(path.c_str(), mode.c_str());
		if (!file)
			throw std::runtime_error("Can't open file to write");
	}
	~File() {
		if (file)
			fclose(file);
	}

	void Close() {
		fclose(file);
		file = 0;
	}

	void WriteInt(int value) {
		if (!file) return;
		fwrite(&value, 4, 1, file);
	}
	void WriteShort(int value) {
		if (!file) return;
		fwrite(&value, 2, 1, file);
	}
	void WriteByte(unsigned char value) {
		if (!file) return;
		fwrite(&value, 1, 1, file);
	}
	void WriteFloat(float value) {
		if (!file) return;
		fwrite(&value, 4, 1, file);
	}
	void WriteString(const std::string& value) {
		if (!file) return;
		fwrite(value.c_str(), 1, value.length() + 1, file);
	}
	void WriteWString(const std::wstring& value) {
		if (!file) return;
		fwrite(value.c_str(), sizeof(wchar_t), value.length() + 1, file);
	}
	int ReadInt() {
		if (!file) return 0;
		int value = 0;
		fread(&value, 4, 1, file);
		return value;
	}
	int ReadShort() {
		if (!file) return 0;
		int value = 0;
		fread(&value, 2, 1, file);
		return value;
	}
	unsigned char ReadByte() {
		if (!file) return 0;
		unsigned char value = 0;
		fread(&value, 1, 1, file);
		return value;
	}
	float ReadFloat() {
		if (!file) return 0;
		float value = 0;
		fread(&value, 4, 1, file);
		return value;
	}
	std::string ReadString() {
		if (!file) return "";
		std::string value;
		unsigned char c;
		while (fread(&c, 1, 1, file) && c) {
			value += c;
		}
		return value;
	}
	std::wstring ReadWString() {
		if (!file) return L"";
		std::wstring value;
		wchar_t c;
		while (fread(&c, sizeof(wchar_t), 1, file) && c) {
			value += c;
		}
		return value;
	}
	void Rewind() {
		rewind(file);
	}

	bool Eof() const {
		return feof(file);
	}


	static bool Exists(const std::string& path) {
		FILE *file = fopen(path.c_str(), "rb");
		if (!file)
			return false;
		fclose(file);
		return true;
	}

	FILE* file;
};

void export_BasicTypes()
{

	DISPOSEME(4) py::class_<File>("File", py::no_init)
		.def(py::init<std::string, std::string>())
		.def("WriteInt", &File::WriteInt)
		.def("WriteShort", &File::WriteShort)
		.def("WriteByte", &File::WriteByte)
		.def("WriteFloat", &File::WriteFloat)
		.def("WriteString", &File::WriteString)
		.def("WriteWString", &File::WriteWString)
		.def("ReadInt", &File::ReadInt)
		.def("ReadShort", &File::ReadShort)
		.def("ReadByte", &File::ReadByte)
		.def("ReadFloat", &File::ReadFloat)
		.def("ReadString", &File::ReadString)
		.def("ReadWString", &File::ReadWString)
		.def("Rewind", &File::Rewind)
		.def("Close", &File::Close)
		.def("Exists", &File::Exists).staticmethod("Exists")
		.add_property("eof", &File::Eof)
		;
}

IClientFramework* framework;

#ifdef WIN32
__declspec(dllexport)
#endif
IClient* CreateClient(int argc, char **argv, const char* directory, const char* documents, IClientFramework* _framework, IRenderer* renderer)
{
	framework = _framework;

	documents_dir.assign(documents, strlen(documents));

	return new client::Client(argc, argv, directory, renderer);
}

#define export_Submodule(name, ...) \
	{ \
		py::object _module(py::handle<>(py::borrowed(PyImport_AddModule("core." name)))); \
		py::scope().attr(name) = _module; \
		py::scope _scope = _module; \
		__VA_ARGS__; \
	}

char const* greet()
{
   return "hello, world";
}

struct myclass
{
	void func() {}
};

BOOST_PYTHON_MODULE(core)
{
	py::object package = py::scope();
	package.attr("__path__") = "core";

	void export_Event();
	void export_Material();
	void export_Entity();
	void export_Controllers();
	void export_Widget();
	void export_Overlay();
	void export_Helper();
	void export_Model();
	void export_StaticMesh();
	void export_Sprite();
	void export_Emitter();
	void export_Primitive();
	void export_Grid();
	void export_Scene();
	void export_Camera();
	void export_Sound();
	void export_Random();
	void export_Bucket();
	void export_Text();
	//void export_Composer();
	void export_SysWidgets();
	void export_Console();
	void export_MathLib();
	void export_Effect();
	void export_Localization();
	void export_Steam();


#ifdef WITH_SQLITE
	export_Submodule("db", export_SqLite3());
#endif

	export_Submodule("geom", export_MathLib());
	export_Submodule("xml", export_TinyXml2());
	export_Submodule("text", export_Localization());
	export_Submodule("ga", export_GA());

	export_ErrorWriter();
	export_BasicTypes();

	export_Random();
	export_Framework();
	export_Bucket();
	export_Event();
	export_Material();
	export_Overlay();
	export_Helper();
	export_Entity();
	export_Widget();
	export_Sprite();
	export_Text();
	export_Model();
	export_StaticMesh();
	export_Primitive();
	export_Grid();
	export_Scene();
	export_Effect();

	export_Console();
#ifdef WIN32
	export_Submodule("system", export_SysWidgets());
#endif
	export_Submodule("mod", export_Controllers());
	export_Submodule("media", export_Camera(), export_Sound());
	export_Submodule("pflow", export_Emitter());
	//export_Submodule("compose", export_Composer());
	export_Submodule("steam", export_Steam());
}

#undef export_Submodule

int InitializePython()
{
	//PyEval_InitThreads();

	std::cout << "Script threads initialized" << std::endl;

	if (PyImport_AppendInittab("core", initcore) == -1)
	{
		throw std::runtime_error("Failed to add 'core' to the interpreter's builtin modules");
	}

	//if ((g_AppFlags & APPFLAG_ALLOWENVPYTHON) == 0) {
	Py_NoSiteFlag = 1;
	Py_FrozenFlag = 1;
	Py_IgnoreEnvironmentFlag = 1;
	//}
	Py_SetProgramName("engine");
	Py_Initialize();

	std::cout << "Script system initialized" << std::endl;

	return 0;
}
