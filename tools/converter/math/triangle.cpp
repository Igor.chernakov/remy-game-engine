#include "math.hpp"

namespace math
{
	Triangle::Triangle()
		: n(0, 0, 1)
	{
	}

	Triangle::Triangle(const Vector3& a, const Vector3& b, const Vector3& c)
		: a(a), b(b), c(c), n(Vector3::Normalize(Vector3::Cross(a - b, a - c)))
	{
	}

	Triangle::Triangle(const Vector3& a, const Vector3& b, const Vector3& c, const Vector3& n)
		: a(a), b(b), c(c), n(n)
	{
	}
}