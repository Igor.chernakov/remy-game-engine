#include "math.hpp"
#include <sstream>

namespace math
{
	const Box3 Box3::Identity(0,0,0,1,1,1);

	Box3::Box3(const std::string& param)
	{
		std::stringstream(param) >> *this;
	}

	Box3 Box3::Add(const Box3& r, const Vector3& v)
	{
		return Box3(MIN(v.x, r.vMin.x), MIN(v.y, r.vMin.y), MIN(v.z, r.vMin.z), MAX(v.x, r.vMax.x), MAX(v.y, r.vMax.y), MAX(v.z, r.vMax.z));
	}

	Box3 Box3::Multiply(const Box3& r, const Vector3& v)
	{
		return Box3(r.vMin * v, r.vMax * v);
	}

	Box3 Box3::Divide(const Box3& r, const Vector3& v)
	{
		return Box3(r.vMin / v, r.vMax / v);
	}

	Box3 Box3::Scale(const Box3& r, Real d)
	{
		return Box3(r.vMin * d, r.vMax * d);
	}

	Box3 Box3::Expand(const Box3& r, const Vector3& v)
	{
		return Box3(r.vMin.x - v.x, r.vMin.y - v.y, r.vMin.z - v.z, r.vMax.x + v.x, r.vMax.y + v.y, r.vMax.z + v.z);
	}

	Box3 Box3::Expand(const Box3& r, Real v)
	{
		return Expand(r, Vector3(v, v, v));
	}

	bool Box3::Contains(const Box3& r, const Vector3& p)
	{
		return (r.vMin.x < p.x) && (r.vMax.x > p.x) && (r.vMin.y < p.y) && (r.vMax.y > p.y) && (r.vMin.z < p.z) && (r.vMax.z > p.z);
	}
}
