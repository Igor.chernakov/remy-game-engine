#pragma once

#include "irendererobject.hpp"
#include "irendererbuffer.hpp"
#include "ishader.hpp"
#include "itexture.hpp"

struct IRendererObjectManager: IObject
{
	virtual void AddObject(shared_ptr<const IRendererObject> rendererobject) = 0;
	virtual void Update(struct IRenderer* renderer) = 0;
	virtual void Unload(struct IRenderer* renderer) = 0;
	virtual void Clear(struct IRenderer* renderer) = 0;

	// TODO: is there a better way to load stuff?
	virtual boost::shared_ptr<const IRendererBuffer> CreateBuffer(IRendererBuffer::Type type, const char* mem, size_t size) = 0;
	virtual boost::shared_ptr<const IShader> LoadShader(const std::string& filename) = 0;
	virtual boost::shared_ptr<const ITexture> CopyTexture(const ITexture* other) = 0;
	virtual boost::shared_ptr<const ITexture> LoadTexture(const std::string& filename) = 0;
	virtual boost::shared_ptr<const ITexture> LoadTexture(const char* mem, size_t size) = 0;
	virtual boost::shared_ptr<const ITexture> CreateTexture(int width, int height, ITexture::Format, ITexture::Pool, ITexture::Usage, const char* mem) = 0;
	virtual boost::shared_ptr<const ITexture> CreateRenderTarget(int width, int height) = 0;
};
