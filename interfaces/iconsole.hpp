#pragma once

#include "iobject.hpp"
#include "ioverlay.hpp"

struct IConsole: IObject
{
	virtual void Paint(IOverlay* painter) const = 0;
	virtual bool ProcessKey(int key) = 0;
	virtual void Exec(const std::string& command) = 0;
};