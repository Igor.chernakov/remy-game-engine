#pragma once

struct ICallback: IObject
{
	virtual void OnCallback() = 0;
};

struct IBucket: IObject
{
	virtual std::wstring FormatString(const std::wstring& text) const = 0;
	virtual void AddCallback(const std::wstring& text, ICallback* callback) = 0;
	virtual void RemoveCallback(ICallback* callback) = 0;
};

struct IClientScene2: IObject
{
	virtual bool IsPaused() const = 0;
};