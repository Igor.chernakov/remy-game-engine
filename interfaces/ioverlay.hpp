#pragma once

#include "iobject.hpp"
#include "imaterial.hpp"
#include "isurface.hpp"

struct IOverlay: IObject
{
    enum Align
    {
        ALIGN_INHERIT,
        ALIGN_LEFT,
        ALIGN_RIGHT,
        ALIGN_CENTER
    };

    enum Repeat
    {
        REPEATMODE_INHERIT,
        REPEATMODE_ROUND,
        REPEATMODE_REPEAT,
        REPEATMODE_STRETCH,
    };

	enum Whitespace
	{
		WHITESPACE_NORMAL,
		WHITESPACE_NOWRAP,
	};

	struct ColorPair
	{
		ColorPair() {}
		ColorPair(const color_t& top, const color_t& bottom): top(top), bottom(bottom) {}
		ColorPair(const color_t& solid): top(solid), bottom(solid) {}
		color_t top, bottom;
	};

	struct TextDimensions
	{
		TextDimensions()
			: align(ALIGN_LEFT)
			, max_length(-1)
			, line_height(0)
			, size(-1)
			, shuffle(0.0f)
			, letter_spacing(0)
			, whitespace(WHITESPACE_NORMAL)
		{}
		Align align;
		Whitespace whitespace;
		int max_length;
		int line_height;
		int size;
		int letter_spacing;
		float shuffle;
	};

    virtual const ISurface* String(
        const std::wstring& s,
        const mat4_t& m_Matrix,
        const std::string& font = "system",
        const ColorPair& nColor = ColorPair(),
		const ColorPair& nHighlightColor = ColorPair(),
		const TextDimensions& td = TextDimensions(),
		dim_t *out_size = 0) = 0;

    virtual const ISurface* Quad(
		boost::shared_ptr<const IMaterial> pMaterial,
        const box2_t& vScreenRect,
        const box2_t& vTextureRect = box2_t(0.0f,0.0f,1.0f,1.0f),
        const color_t& nColor = color_t(),
        const mat4_t *pMatrix = NULL) = 0;

    virtual void Render(struct IRenderer* renderer) const = 0;

    virtual void Erase() = 0;
};