//
//  main.m
//  launcher.ios
//
//  Created by Igor Chernakov on 13/10/2013.
//  Copyright (c) 2013 Igor Chernakov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "iosAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([iosAppDelegate class]));
	}
}
