/**
	BUCKET is a very useful class to store variables and update related UI strings when variables are modified,
	i.e. UI string "Health: {health}" would be updated each time 'health' variable is modified
**/

#include "c_client.hpp"
#include "c_event.hpp"

namespace py = boost::python;

// Helper function to check if an object has an attribute.
bool hasattr(const boost::python::object& obj, const char* name) {
  return PyObject_HasAttrString(obj.ptr(), name);
}

namespace client
{
	struct Bucket: IBucket
	{
		~Bucket()
		{
		}

		void Reset() {
			m_storage.clear();
		}

		void SetVariable(const std::string& name, py::object value)
		{
			m_storage[name].ValueChanged(name, value);
			m_storage[name].value = value;

			std::for_each(m_storage[name].callbacks.begin(), m_storage[name].callbacks.end(),
				boost::bind(&ICallback::OnCallback, _1));
		}

		py::object GetVariable(const std::string& name) const
		{
			storage_t::const_iterator it = m_storage.find(name);
			return it != m_storage.end() ? it->second.value : py::object();
		}

		struct keywords: std::list<std::string>
		{
			keywords(const std::wstring& text)
            {
				for (int a = 0, b = 0;
					a != text.npos && b != text.npos;
					a = text.find_first_of(L"{", b), b = text.find_first_of(L"}", a))
				{
					if (b > a) { //skip empty keywords
						std::wstring kw = text.substr(a + 1, b - a - 1);
						push_back(std::string(kw.begin(), kw.end()));
					}
				}
            }

			bool valid;
		};

		std::wstring FormatString(const std::wstring& text) const
		{
			keywords kw(text);

			struct printer
			{
				const Bucket* self;
				std::wstring& out;
				printer(const Bucket* self, std::wstring& out): self(self), out(out) {}
				void operator()(const std::string& keyword)
				{
					std::wstring wkeyword(keyword.begin(), keyword.end());
					py::object var = self->GetVariable(keyword);
					py::extract<std::wstring> xwstring(var);
					if (xwstring.check()) {
						boost::algorithm::replace_all(out, L"{" + wkeyword + L"}",
							(std::wstring)xwstring());
					} else {
						std::string value = py::extract<std::string>(var.attr("__str__")());
						boost::algorithm::replace_all(out, L"{" + wkeyword + L"}",
							std::wstring(value.begin(), value.end()));
					}
				}
			};

			std::wstring out = text;

			std::for_each(kw.begin(), kw.end(), printer(this, out));

			return out;
		}

		void AddCallback(const std::wstring& text, ICallback* callback)
		{
			struct add_callback
			{
				storage_t& dict;
				ICallback* cb;
				add_callback(storage_t& dict, ICallback* cb): dict(dict), cb(cb) {}
				void operator()(const std::string& key) { dict[key].callbacks.push_back(cb); }
			};

			keywords kw(text);

			std::for_each(kw.begin(), kw.end(), add_callback(m_storage, callback));

			callback->OnCallback(); //let's say the string is changed
		}

		void RemoveCallback(ICallback* callback)
		{
			struct remove_callback
			{
				ICallback* cb;
				remove_callback(ICallback* cb): cb(cb) {}
				void operator()(storage_t::value_type& value) { value.second.callbacks.remove(cb); }
			};

			std::for_each(m_storage.begin(), m_storage.end(), remove_callback(callback));
		}

		void AddHandler(const std::string& name, py::object handler)
		{
			m_storage[name].ValueChanged += handler;
		}

		void RemoveHandler(const std::string& name, py::object handler)
		{
			storage_t::iterator it = m_storage.find(name);
			if (it != m_storage.end()) {
				it->second.ValueChanged -= handler;
			}
		}

		struct value_holder
		{
			std::list<ICallback*> callbacks;
			py::object value;
			Event ValueChanged;
		};

		typedef std::map<std::string, value_holder> storage_t;

		storage_t m_storage;
	};
}

IBucket* CreateBucket()
{
	return new client::Bucket();
}

void export_Bucket()
{
	namespace py = py;

	using namespace client;

	DISPOSEME(4) py::class_<Bucket, shared_ptr<Bucket>, py::bases<IBucket>, boost::noncopyable>("Bucket", py::no_init)
		.def("__getattr__", &Bucket::GetVariable)
		.def("__setattr__", &Bucket::SetVariable)
		.def("AddHandler", &Bucket::AddHandler, (py::arg("variable"), py::arg("handler")))
		.def("RemoveHandler", &Bucket::RemoveHandler, (py::arg("variable"), py::arg("handler")))
		.def("Clear", &Bucket::Reset)
		;
}
