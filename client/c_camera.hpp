#pragma once

namespace client
{
	class Frustum
	{
	public:
		enum FrustumSide
		{
			LEFT,
			RIGHT,
			BOTTOM,
			TOP,
			BACK,
			FRONT
		};

		enum {
			PLB_NONE=0x00,
			PLB_LEFT=0x01,
			PLB_RIGHT=0x02,
			PLB_BOTTOM=0x04,
			PLB_TOP=0x08,
			PLB_NEAR=0x10,
			PLB_FAR=0x20,
			PLB_ALL=PLB_LEFT|PLB_RIGHT|PLB_BOTTOM|PLB_TOP,
		};

		enum PlaneData {
			A = 0,                          // The X value of the plane's normal
			B = 1,                          // The Y value of the plane's normal
			C = 2,                          // The Z value of the plane's normal
			D = 3                           // The distance the plane is from the origin
		};

		void CalculateFrustum(const mat4_t& projection, const mat4_t& modelview);
		void NormalizePlane(int side);

		bool PointInFrustum(const float &x, const float &y, const float &z) const;
		bool SphereInFrustum(const float &x, const float &y, const float &z, const float radius = 64) const;
		bool SphereInFrustum(const vec3_t& org, const float radius = 64) const;
		bool BoxInFrustum(const box3_t& box, const mat4_t& matrix) const;
		bool AABoxInFrustum(const box3_t& box) const;

	protected:
		float m_Frustum[6][4];
		mat4_t m_clip, m_invclip;
	};


	class Camera
		: public Entity
		, public Frustum
		, public ICamera
		, public boost::python::wrapper<Camera>
		, public IWrapper
	{
	public:
		Camera(
			const std::string& name = "Camera#",
			const vec3_t& position = vec3_t::Zero,
			const quat_t& rotation = quat_t::Identity,
			float fov = 45,
			Projection proj = PROJ_PERSPECTIVE);

	public:
		void SetTarget(const vec3_t&);

		void LookAt(const vec3_t &eye, const vec3_t &target, const vec3_t &yaw = vec3_t(0,0,1));
		void SetMode(Mode mode, IRenderer* renderer, int clip_flags = CLIP_SCISSOR | CLIP_OFFSET);
		Mode GetMode() const { return mode_; }
		vec3_t GetEye() const;
		vec3_t GetDirection() const;
		vec3_t GetTarget() const;
		vec3_t GetYaw() const;
		quat_t GetOrientation() const;
		void SetFov(float fov);
		float GetFov() const;
		float GetZFar() const;
		float GetZNear() const;
		void SetZFar(float);
		void SetZNear(float);
		float GetAspectRatio() const;
		vec2_t GetScreenCoords(const vec3_t& point) const;
		math::Line GetMouseRay() const;
		math::Line GetRay(const point_t& point) const;
		int GetWidth() const;
		int GetHeight() const;
		Projection GetProjection() const;
		void SetProjection(Projection);
		float GetPixelSize(const vec3_t& origin) const;
		bool BoxInFrustum(const box3_t& box, const mat4_t& matrix) const {	return Frustum::BoxInFrustum(box, matrix); }

		void Update() { m_viewport_old = m_viewport; m_offset_old = m_offset; } 

		void SetViewport(const rect_t& rect);
		const rect_t& GetViewport() const;

		void SetOffset(const point_t& point);
		const point_t& GetOffset() const;

		virtual shared_ptr<client::Entity> shared_from_this();
		virtual shared_ptr<const client::Entity> shared_from_this() const;
		virtual PyObject* GetPythonObject() const;
		virtual float GetDistance(const vec3_t& position) const;

	private:
		inline vec3_t ScreenToWorld(const point_t& point, float distance) const;
		inline float GetOrthoHeight() const;

	private:
		float znear, zfar, fov;
		Projection proj;
		Mode mode_;

		rect_t m_viewport, m_viewport_old;
		point_t m_offset, m_offset_old;
	};
}
