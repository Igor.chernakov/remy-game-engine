#pragma once

#define COPYSTR(dst, src) \
	{ \
		char buffer[2048] = { 0 };\
		for (char *c = buffer;; c++) { \
			COPYVAR(*c, src); \
			if (*c == 0) \
				break; \
		} \
		dst = buffer; \
	}

struct w3i_t {
	w3i_t(const BYTE **offset)
		: header(new header_t(offset))
	{};

	~w3i_t() {
		delete header;
	};

	struct header_t {
		header_t(const BYTE **offset) {
			COPYVAR(version, offset);
			COPYVAR(saves, offset);
			COPYVAR(editor, offset);
			COPYSTR(mapname, offset);
			COPYSTR(mapauthor, offset);
			COPYSTR(mapdescr, offset);
			COPYSTR(players, offset);
			COPYVAR(cambounds, offset);
			COPYVAR(camboundsnotes, offset);
			COPYVAR(playwidth, offset);
			COPYVAR(playheight, offset);
			COPYVAR(flags, offset);
			COPYVAR(ground, offset);
			COPYVAR(campaign, offset);
			COPYSTR(loadingtext, offset);
			COPYSTR(loadingtitle, offset);
			COPYSTR(loadingsubtitle, offset);
			COPYVAR(loadingnum, offset);
			COPYSTR(prologuetext, offset);
			COPYSTR(prologuetitle, offset);
			COPYSTR(prologuesubtitle, offset);
			COPYVAR(playersnum, offset);
			for (int i = 0; i < playersnum; i++) {
				Player p;
				COPYVAR(p.unknown, offset);
				COPYVAR(p.type, offset);
				COPYVAR(p.race, offset);
				COPYVAR(p.fixed, offset);
				COPYSTR(p.name, offset);
				COPYVAR(p.vStart, offset);
				COPYVAR(p.flags, offset);
				mPlayers.push_back(p);
			}
			COPYVAR(forcesnum, offset);
			COPYVAR(forcesflags, offset);
			COPYVAR(unknown, offset);
			for (int i = 0; i < forcesnum; i++) {
				Force p;
				COPYSTR(p.name, offset);
				COPYVAR(p.unknown, offset);
				mForces.push_back(p);
			}
			std::vector<Force> mForces;
			COPYVAR(endbyte, offset);
		}

		int version;
		int saves;
		int editor;
		std::string mapname;
		std::string mapauthor;
		std::string mapdescr;
		std::string players;
		float cambounds[8];
		int camboundsnotes[4];
		int playwidth;
		int playheight;
		int flags;
		char ground;
		int campaign;
		std::string loadingtext;
		std::string loadingtitle;
		std::string loadingsubtitle;
		int loadingnum;
		std::string prologuetext;
		std::string prologuetitle;
		std::string prologuesubtitle;
		int playersnum;
		struct Player {
			int unknown;
			int type;
			int race;
			int fixed;
			std::string name;
			math::Vector2 vStart;
			int flags[2];
		};
		std::vector<Player> mPlayers;
		int forcesnum;
		int forcesflags;
		int unknown;
		struct Force {
			std::string name;
			int unknown[2];
		};
		std::vector<Force> mForces;
		int endbyte;
	} *header;
};

#undef COPYSTR