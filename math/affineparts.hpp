#pragma once

#include "math.hpp"

namespace math
{
	struct AffineParts
	{
		static AffineParts Lerp(float k, const AffineParts& a, const AffineParts& b)
		{
			AffineParts r;
			r.position = Vector3::Lerp(k, a.position, b.position);
			r.rotation = Quaternion::Lerp(k, a.rotation, b.rotation);
			r.scale = Vector3::Lerp(k, a.scale, b.scale);
			return r;
		}
		Vector3 position;
		Quaternion rotation;
		Vector3 scale;
	};
}