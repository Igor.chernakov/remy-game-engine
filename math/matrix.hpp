#pragma once

#include "math.hpp"

namespace math {

	struct Quaternion;

	struct Matrix
	{
		union
		{
			Real m[16];
			Real v[4][4];
		};

		static const Matrix Identity;

		Matrix();
		Matrix(const Real* v);
		Matrix(Real, Real, Real, Real, Real, Real, Real, Real, Real, Real, Real, Real, Real, Real, Real, Real);
		Matrix(const Matrix& m);
		Matrix(const Quaternion& q);
		Matrix(const Vector3& axis_x, const Vector3& axis_y, const Vector3& axis_z, const Vector3& translation = Vector3(0,0,0));
		Matrix(const AffineParts& ap);

		const Matrix& Reset();

		static Matrix LookAt(const Vector3 &from, const Vector3 &to, const Vector3 &up);
		static Matrix Perspective(float fov, float aspect, float znear, float zfar);
		static Matrix Ortho(float width, float height, float near = 0.0f, float far = 1.0f);
		static Matrix Shadow(const Vector4 &light, const Vector4 &plane);
		static Matrix Transpose(const Matrix& m);
		Matrix& Transpose();
		static Matrix Invert(const Matrix& m);
		Matrix& Invert();
		static Matrix RotationYawPitchRoll(Real yaw, Real pitch, Real roll);
		static Matrix RotationQuaternion(const Quaternion& q);
		static Matrix Scaling(Real x, Real y, Real z);
		static Matrix Scaling(const Vector3& v);
		static Matrix Translation(Real x, Real y, Real z);
		static Matrix Translation(const Vector3& v);
		static Matrix Multiply(const Matrix& left, const Matrix& right);
		Matrix& Multiply(const Matrix& m);

		Matrix operator*(const Matrix& m) const;
		Matrix& operator*=(const Matrix& m);
		Real& operator[](int i) { return m[i]; }
		const Real& operator[](int i) const { return m[i]; }
		operator const Real*(void) const { return m; }
		operator Real*(void) { return m; }

		//Accessors

		const Vector3& GetFront() const;
		Matrix& SetFront(const Vector3& v);
		Matrix& SetFront(Real x, Real y, Real z);
		const Vector3& GetRight() const;
		Matrix& SetRight(const Vector3& v);
		Matrix& SetRight(Real x, Real y, Real z);
		const Vector3& GetUp() const;
		Matrix& SetUp(const Vector3& v);
		Matrix& SetUp(Real x, Real y, Real z);
		const Vector3& GetTranslation() const;
		Matrix& SetTranslation(const Vector3& v);
		Matrix& SetTranslation(Real x, Real y, Real z);
		Vector3 GetScale() const;
		Matrix& SetScale(const Vector3& v);
		Matrix& SetScale(Real x, Real y, Real z);
		Quaternion GetRotation() const;
		Matrix& SetRotation(const Quaternion& v);
		AffineParts GetAffineParts() const;
		void SetAffineParts(const AffineParts& ap);
	};

};
