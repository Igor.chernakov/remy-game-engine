/**
	This file exports all of the math operations to Python
**/

#include "c_client.hpp"

namespace py = boost::python;

template<class T, typename Y = float>
struct tuple_to_vector
{
	tuple_to_vector()
	{
		py::converter::registry::push_back(&convertible, &construct, py::type_id<T>());
	}

	static void* convertible(PyObject* x)
	{
		return (PyTuple_Check(x) && PyTuple_Size(x) == T::Dimensions) ? x : 0;
	}

	static void construct(PyObject* x, py::converter::rvalue_from_python_stage1_data* data)
	{
		void* storage = ((py::converter::rvalue_from_python_storage<T>*)data)->storage.bytes;
		py::object o(py::borrowed(x));
		T temp;
		for (int i = 0; i < T::Dimensions && i < py::len(o); ++i)
		{
			((Y*)&temp)[i] = py::extract<Y>(o[i]);
		}
		new (storage) T(temp);
		data->convertible = storage;
	}
};

struct tuple_to_quaternion
{
	tuple_to_quaternion()
	{
		py::converter::registry::push_back(&convertible, &construct, py::type_id<quat_t>());
	}

	static void* convertible(PyObject* x)
	{
		return PyTuple_Check(x) ? x : 0;
	}

	static void construct(PyObject* x, py::converter::rvalue_from_python_stage1_data* data)
	{
		void* storage = ((py::converter::rvalue_from_python_storage<quat_t>*)data)->storage.bytes;
		py::object o(py::borrowed(x));
		quat_t temp;
		switch (py::len(o))
		{
		case 4:
			temp = quat_t(
				py::extract<float>(o[0]),
				py::extract<float>(o[1]),
				py::extract<float>(o[2]),
				py::extract<float>(o[3]));
			break;
		case 3:
			temp = quat_t(
				py::extract<float>(o[0]),
				py::extract<float>(o[1]),
				py::extract<float>(o[2]));
			break;
		};
		new (storage) quat_t(temp);
		data->convertible = storage;
	}
};

void export_MathLib()
{
	using math::Real;
	using math::Vector2;
	using math::Vector3;
	using math::Vector4;
	using math::Quaternion;
	using math::Matrix;
	using math::Box2;
	using math::Box3;
	using math::Triangle;
	using math::Line;
	using math::Light;

	{
		typedef Vector2 T;

		DISPOSEME(3) py::class_<T>("Vector2")
			.def(py::init<float, float>())
			.def(py::init<std::string>())
			.def(py::init<T>())

			.def("dot", (float (T::*)(const T&) const)&T::Dot)
			.def("assign", &T::Assign, py::return_internal_reference<>())
			.def("normalize", (T& (T::*)())&T::Normalize, py::return_internal_reference<>())
			.def("maximize", (T& (T::*)())&T::Maximize, py::return_internal_reference<>())
			.def("minimize", (T& (T::*)())&T::Minimize, py::return_internal_reference<>())
			.def("add", (T& (T::*)(const T&))&T::Add, py::return_internal_reference<>())
			.def("substract", (T& (T::*)(const T&))&T::Substract, py::return_internal_reference<>())
			.def("multiply", (T& (T::*)(const T&))&T::Multiply, py::return_internal_reference<>())
			.def("divide", (T& (T::*)(const T&))&T::Divide, py::return_internal_reference<>())

			.def("Length", (float (*)(const T&))&T::Length).staticmethod("Length")
			.def("Dot", (float (*)(const T&, const T&))&T::Dot).staticmethod("Dot")
			.def("Normalize", (T (*)(const T&))&T::Normalize).staticmethod("Normalize")
			.def("Lerp", (T (*)(float, const T&, const T&))&T::Lerp).staticmethod("Lerp")
			.def("Add", (T (*)(const T&, const T&))&T::Add).staticmethod("Add")
			.def("Substract", (T (*)(const T&, const T&))&T::Substract).staticmethod("Substract")
			.def("Multiply", (T (*)(const T&, const T&))&T::Multiply).staticmethod("Multiply")
			.def("Divide", (T (*)(const T&, const T&))&T::Divide).staticmethod("Divide")

			.def(py::self + py::self)
			.def(py::self += py::self)
			.def(py::self - py::self)
			.def(py::self -= py::self)
			.def(py::self * py::self)
			.def(py::self *= py::self)
			.def(py::self / py::self)
			.def(py::self /= py::self)
			.def(py::self * float())
			.def(py::self *= float())
			.def(py::self / float())
			.def(py::self /= float())
			.def(-py::self)

			.def_readwrite("x", &T::x)
			.def_readwrite("y", &T::y)

			.def_readonly("One", &T::One)
			.def_readonly("Zero", &T::Zero)

			.add_property("length", (float (T::*)() const)&T::Length);

		py::implicitly_convertible<std::string, T>();
	}

	{
		typedef Vector3 T;

		DISPOSEME(3) py::class_<T>("Vector3")
			.def(py::init<float, float, float>())
			.def(py::init<std::string>())
			.def(py::init<T>())


			.def("dot", (float (T::*)(const T&) const)&T::Dot)
			.def("assign", &T::Assign, py::return_internal_reference<>())
			.def("cross", (T& (T::*)(const T&))&T::Cross, py::return_internal_reference<>())
			.def("normalize", (T& (T::*)())&T::Normalize, py::return_internal_reference<>())
			.def("maximize", (T& (T::*)())&T::Maximize, py::return_internal_reference<>())
			.def("minimize", (T& (T::*)())&T::Minimize, py::return_internal_reference<>())
			.def("add", (T& (T::*)(const T&))&T::Add, py::return_internal_reference<>())
			.def("substract", (T& (T::*)(const T&))&T::Substract, py::return_internal_reference<>())
			.def("multiply", (T& (T::*)(const T&))&T::Multiply, py::return_internal_reference<>())
			.def("multiply", (T& (T::*)(const Matrix&))&T::Multiply, py::return_internal_reference<>())
			.def("multiply", (T& (T::*)(const Quaternion&))&T::Multiply, py::return_internal_reference<>())
			.def("divide", (T& (T::*)(const T&))&T::Divide, py::return_internal_reference<>())

			.def("Length", (float (*)(const T&))&T::Length).staticmethod("Length")
			.def("Dot", (float (*)(const T&, const T&))&T::Dot).staticmethod("Dot")
			.def("Cross", (T (*)(const T&, const T&))&T::Cross).staticmethod("Cross")
			.def("Normalize", (T (*)(const T&))&T::Normalize).staticmethod("Normalize")
			.def("Lerp", (T (*)(float, const T&, const T&))&T::Lerp).staticmethod("Lerp")
			.def("Add", (T (*)(const T&, const T&))&T::Add).staticmethod("Add")
			.def("Substract", (T (*)(const T&, const T&))&T::Substract).staticmethod("Substract")
			.def("Multiply", (T (*)(const T&, const T&))&T::Multiply)//.staticmethod("Multiply")
			.def("Multiply", (T (*)(const T&, const Matrix&))&T::Multiply)//.staticmethod("Multiply")
			.def("Multiply", (T (*)(const T&, const Quaternion&))&T::Multiply).staticmethod("Multiply")
			.def("Divide", (T (*)(const T&, const T&))&T::Divide).staticmethod("Divide")

			.def(py::self + py::self)
			.def(py::self += py::self)
			.def(py::self - py::self)
			.def(py::self -= py::self)
			.def(py::self * py::self)
			.def(py::self *= py::self)
			.def(py::self / py::self)
			.def(py::self /= py::self)
			.def(py::self * py::other<Matrix>())
			.def(py::self *= py::other<Matrix>())
			.def(py::self * py::other<Quaternion>())
			.def(py::self *= py::other<Quaternion>())
			.def(py::self * py::other<float>())
			.def(py::self *= py::other<float>())
			.def(py::self / py::other<float>())
			.def(py::self /= py::other<float>())
			.def(-py::self)

			.def_readwrite("x", &T::x)
			.def_readwrite("y", &T::y)
			.def_readwrite("z", &T::z)

			.def_readonly("One", &T::One)
			.def_readonly("Zero", &T::Zero)

			.add_property("length", (float (T::*)() const)&T::Length);

		py::implicitly_convertible<std::string, T>();
	}

	{
		typedef Vector4 T;

		DISPOSEME(3) py::class_<T>("Vector4")
			.def(py::init<float, float, float, float>())
			.def(py::init<std::string>())
			.def(py::init<T>())

			.def("dot", (float (T::*)(const T&) const)&T::Dot)
			.def("assign", &T::Assign, py::return_internal_reference<>())
			.def("normalize", (T& (T::*)())&T::Normalize, py::return_internal_reference<>())
			.def("maximize", (T& (T::*)())&T::Maximize, py::return_internal_reference<>())
			.def("minimize", (T& (T::*)())&T::Minimize, py::return_internal_reference<>())
			.def("add", (T& (T::*)(const T&))&T::Add, py::return_internal_reference<>())
			.def("substract", (T& (T::*)(const T&))&T::Substract, py::return_internal_reference<>())
			.def("multiply", (T& (T::*)(const T&))&T::Multiply, py::return_internal_reference<>())
			.def("multiply", (T& (T::*)(const Matrix&))&T::Multiply, py::return_internal_reference<>())
			.def("divide", (T& (T::*)(const T&))&T::Divide, py::return_internal_reference<>())

			.def("Length", (float (*)(const T&))&T::Length).staticmethod("Length")
			.def("Dot", (float (*)(const T&, const T&))&T::Dot).staticmethod("Dot")
			.def("Normalize", (T (*)(const T&))&T::Normalize).staticmethod("Normalize")
			.def("Lerp", (T (*)(float, const T&, const T&))&T::Lerp).staticmethod("Lerp")
			.def("Add", (T (*)(const T&, const T&))&T::Add).staticmethod("Add")
			.def("Substract", (T (*)(const T&, const T&))&T::Substract).staticmethod("Substract")
			.def("Multiply", (T (*)(const T&, const T&))&T::Multiply)//.staticmethod("Multiply")
			.def("Multiply", (T (*)(const T&, const Matrix&))&T::Multiply).staticmethod("Multiply")
			.def("Divide", (T (*)(const T&, const T&))&T::Divide).staticmethod("Divide")

			.def(py::self + py::self)
			.def(py::self += py::self)
			.def(py::self - py::self)
			.def(py::self -= py::self)
			.def(py::self * py::self)
			.def(py::self *= py::self)
			.def(py::self / py::self)
			.def(py::self /= py::self)
			.def(py::self * py::other<Matrix>())
			.def(py::self *= py::other<Matrix>())
			.def(py::self * py::other<float>())
			.def(py::self *= py::other<float>())
			.def(py::self / py::other<float>())
			.def(py::self /= py::other<float>())
			.def(-py::self)

			.def_readwrite("x", &T::x)
			.def_readwrite("y", &T::y)
			.def_readwrite("z", &T::z)
			.def_readwrite("w", &T::w)

			.def_readonly("One", &T::One)
			.def_readonly("Zero", &T::Zero)

			.add_property("length", (float (T::*)() const)&T::Length);

		py::implicitly_convertible<std::string, T>();
	}

	{
		typedef Quaternion T;

		DISPOSEME(3) py::class_<T>("Quaternion")
			.def(py::init<const Matrix&>())
			.def(py::init<float, float, float>())
			.def(py::init<float, float, float, float>())
			.def(py::init<std::string>())
			.def(py::init<T>())

			.def("dot", (float (T::*)(const T&) const)&T::Dot)
			.def("assign", &T::Assign, py::return_internal_reference<>())
			.def("normalize", (T& (T::*)())&T::Normalize, py::return_internal_reference<>())
			.def("add", (T& (T::*)(const T&))&T::Add, py::return_internal_reference<>())
			.def("substract", (T& (T::*)(const T&))&T::Substract, py::return_internal_reference<>())
			.def("multiply", (T& (T::*)(const T&))&T::Multiply, py::return_internal_reference<>())
			.def("divide", (T& (T::*)(const T&))&T::Divide, py::return_internal_reference<>())

			.def("Length", (float (*)(const T&))&T::Length).staticmethod("Length")
			.def("Dot", (float (*)(const T&, const T&))&T::Dot).staticmethod("Dot")
			.def("Normalize", (T (*)(const T&))&T::Normalize).staticmethod("Normalize")
			.def("Lerp", (T (*)(float, const T&, const T&))&T::Lerp).staticmethod("Lerp")
			.def("Add", (T (*)(const T&, const T&))&T::Add).staticmethod("Add")
			.def("Substract", (T (*)(const T&, const T&))&T::Substract).staticmethod("Substract")
			.def("Multiply", (T (*)(const T&, const T&))&T::Multiply).staticmethod("Multiply")
			.def("Divide", (T (*)(const T&, const T&))&T::Divide).staticmethod("Divide")
			.def("RotationYawPitchRoll", (T (*)(float, float, float))&T::RotationYawPitchRoll).staticmethod("RotationYawPitchRoll")
			.def("RotationMatrix", (T (*)(const Matrix&))&T::RotationMatrix).staticmethod("RotationMatrix")

			.def(py::self + py::self)
			.def(py::self += py::self)
			.def(py::self - py::self)
			.def(py::self -= py::self)
			.def(py::self * py::self)
			.def(py::self *= py::self)
			.def(py::self / py::self)
			.def(py::self /= py::self)
			.def(py::self * py::other<float>())
			.def(py::self *= py::other<float>())
			.def(py::self / py::other<float>())
			.def(py::self /= py::other<float>())
			.def(-py::self)

			.def_readwrite("x", &T::x)
			.def_readwrite("y", &T::y)
			.def_readwrite("z", &T::z)
			.def_readwrite("w", &T::w)

			.def_readonly("Identity", &T::Identity)
			.def_readonly("Zero", &T::Zero)

			.add_property("length", (float (T::*)() const)&T::Length)
			.add_property("yaw", &T::GetYaw, &T::SetYaw)
			.add_property("pitch", &T::GetPitch, &T::SetPitch)
			.add_property("roll", &T::GetRoll, &T::SetRoll)
			;

		py::implicitly_convertible<std::string, T>();
	}

	{
		typedef Matrix T;

		DISPOSEME(3) py::class_<T>("Matrix")
			.def(py::init<const Quaternion&>())
			.def(py::init<const Vector3&, const Vector3&, const Vector3&, const Vector3&>())
			.def(py::init<T>())

			.def("reset", (T& (T::*)())&T::Reset, py::return_internal_reference<>())
			.def("transpose", (T& (T::*)())&T::Transpose, py::return_internal_reference<>())
			.def("invert", (T& (T::*)())&T::Invert, py::return_internal_reference<>())
			.def("multiply", (T& (T::*)(const T&))&T::Multiply, py::return_internal_reference<>())


			.def("LookAt", (T (*)(const Vector3&, const Vector3&, const Vector3&))&T::LookAt).staticmethod("LookAt")
			.def("Perspective", (T (*)(float, float, float, float))&T::Perspective).staticmethod("Perspective")
			.def("Ortho", (T (*)(float, float, float, float))&T::Ortho).staticmethod("Ortho")
			.def("Transpose", (T (*)(const T&))&T::Transpose).staticmethod("Transpose")
			.def("Invert", (T (*)(const T&))&T::Invert).staticmethod("Invert")
			.def("RotationYawPitchRoll", (T (*)(float, float, float))&T::RotationYawPitchRoll).staticmethod("RotationYawPitchRoll")
			.def("RotationQuaternion", (T (*)(const Quaternion&))&T::RotationQuaternion).staticmethod("RotationQuaternion")
			.def("Scaling", (T (*)(float, float, float))&T::Scaling)//.staticmethod("Scaling")
			.def("Scaling", (T (*)(const Vector3&))&T::Scaling).staticmethod("Scaling")
			.def("Translation", (T (*)(float, float, float))&T::Translation)//.staticmethod("Translation")
			.def("Translation", (T (*)(const Vector3&))&T::Translation).staticmethod("Translation")
			.def("Multiply", (T (*)(const T&, const T&))&T::Multiply).staticmethod("Multiply")

			.def(py::self * py::self)
			.def(py::self *= py::self)

			.add_property("front", py::make_function((const Vector3& (T::*)() const)&T::GetFront, py::return_internal_reference<>()), py::make_function((T& (T::*)(const Vector3&))&T::SetFront, py::return_internal_reference<>()))
			.add_property("right", py::make_function((const Vector3& (T::*)() const)&T::GetRight, py::return_internal_reference<>()), py::make_function((T& (T::*)(const Vector3&))&T::SetRight, py::return_internal_reference<>()))
			.add_property("up", py::make_function((const Vector3& (T::*)() const)&T::GetUp, py::return_internal_reference<>()), py::make_function((T& (T::*)(const Vector3&))&T::SetUp, py::return_internal_reference<>()))
			.add_property("translation", py::make_function((const Vector3& (T::*)() const)&T::GetTranslation, py::return_internal_reference<>()), py::make_function((T& (T::*)(const Vector3&))&T::SetTranslation, py::return_internal_reference<>()))
			.add_property("scale", py::make_function((const Vector3& (T::*)() const)&T::GetScale, py::return_internal_reference<>()), py::make_function((T& (T::*)(const Vector3&))&T::SetScale, py::return_internal_reference<>()))
			.add_property("rotation", (Quaternion (T::*)() const)&T::GetRotation, py::make_function((T& (T::*)(const Quaternion&))&T::SetRotation, py::return_internal_reference<>()))

			.def_readonly("Identity", &T::Identity);
	}

	{
		typedef Box2 T;

		DISPOSEME(3) py::class_<T>("Box2")
			.def(py::init<float, float, float, float>())
			.def(py::init<const Vector2&, const Vector2&>())
			.def(py::init<std::string>())
			.def(py::init<T>())

			.def("add", (T& (T::*)(const Vector2&))&T::Add, py::return_internal_reference<>())
			.def("multiply", (T& (T::*)(const Vector2&))&T::Multiply, py::return_internal_reference<>())
			.def("divide", (T& (T::*)(const Vector2&))&T::Divide, py::return_internal_reference<>())
			.def("expand", (T& (T::*)(Real))&T::Expand, py::return_internal_reference<>())
			.def("expand", (T& (T::*)(const Vector2&))&T::Expand, py::return_internal_reference<>())
			.def("contains", (bool (T::*)(const Vector2&) const)&T::Contains)

			.def("Add", (T (*)(const T&, const Vector2&))&T::Add).staticmethod("Add")
			.def("Multiply", (T (*)(const T&, const Vector2&))&T::Multiply).staticmethod("Multiply")
			.def("Divide", (T (*)(const T&, const Vector2&))&T::Divide).staticmethod("Divide")
			.def("Expand", (T (*)(const T&, Real))&T::Expand)//.staticmethod("Expand")
			.def("Expand", (T (*)(const T&, const Vector2&))&T::Expand).staticmethod("Expand")
			.def("Contains", (bool (*)(const T&, const Vector2&))&T::Contains).staticmethod("Contains")

			.def(py::self + py::other<Vector2>())
			.def(py::self += py::other<Vector2>())
			.def(py::self * py::other<Vector2>())
			.def(py::self *= py::other<Vector2>())
			.def(py::self / py::other<Vector2>())
			.def(py::self /= py::other<Vector2>())
			.def(py::self * py::other<float>())
			.def(py::self *= py::other<float>())
			.def(py::self / py::other<float>())
			.def(py::self /= py::other<float>())

			.def_readwrite("min", &T::vMin)
			.def_readwrite("max", &T::vMax)
			.add_property("width", &T::Width)
			.add_property("height", &T::Height)

			.def_readonly("Identity", &T::Identity);
	}

	{
		typedef Box3 T;

		DISPOSEME(3) py::class_<T>("Box3")
			.def(py::init<float, float, float, float, float, float>())
			.def(py::init<const Vector3&, const Vector3&>())
			.def(py::init<std::string>())
			.def(py::init<T>())

			.def("add", (T& (T::*)(const Vector3&))&T::Add, py::return_internal_reference<>())
			.def("multiply", (T& (T::*)(const Vector3&))&T::Multiply, py::return_internal_reference<>())
			.def("divide", (T& (T::*)(const Vector3&))&T::Divide, py::return_internal_reference<>())
			.def("expand", (T& (T::*)(Real))&T::Expand, py::return_internal_reference<>())
			.def("expand", (T& (T::*)(const Vector3&))&T::Expand, py::return_internal_reference<>())
			.def("contains", (bool (T::*)(const Vector3&) const)&T::Contains)

			.def("Add", (T (*)(const T&, const Vector3&))&T::Add).staticmethod("Add")
			.def("Multiply", (T (*)(const T&, const Vector3&))&T::Multiply).staticmethod("Multiply")
			.def("Divide", (T (*)(const T&, const Vector3&))&T::Divide).staticmethod("Divide")
			.def("Expand", (T (*)(const T&, Real))&T::Expand)//.staticmethod("Expand")
			.def("Expand", (T (*)(const T&, const Vector3&))&T::Expand).staticmethod("Expand")
			.def("Contains", (bool (*)(const T&, const Vector3&))&T::Contains).staticmethod("Contains")

			.def(py::self + py::other<Vector3>())
			.def(py::self += py::other<Vector3>())
			.def(py::self * py::other<Vector3>())
			.def(py::self *= py::other<Vector3>())
			.def(py::self / py::other<Vector3>())
			.def(py::self /= py::other<Vector3>())
			.def(py::self * py::other<float>())
			.def(py::self *= py::other<float>())
			.def(py::self / py::other<float>())
			.def(py::self /= py::other<float>())

			.def_readwrite("min", &T::vMin)
			.def_readwrite("max", &T::vMax)

			.add_property("width", &T::Width)
			.add_property("height", &T::Height)
			.add_property("length", &T::Length)

			.def_readonly("Identity", &T::Identity);
	}

	{
		typedef Triangle T;

		DISPOSEME(3) py::class_<T>("Triangle")
			.def(py::init<const Vector3&, const Vector3&, const Vector3&>())
			.def(py::init<const Vector3&, const Vector3&, const Vector3&, const Vector3&>())
			.def(py::init<T>())

			.def_readwrite("a", &T::a)
			.def_readwrite("b", &T::b)
			.def_readwrite("c", &T::c)
			.def_readwrite("n", &T::n)
			;
	}

	{
		typedef Line T;

		DISPOSEME(3) py::class_<T>("Line")
			.def(py::init<const Vector3&, const Vector3&>())
			.def(py::init<T>())

			//.def("Intersects", (bool (*)(const T&, const Triangle&, Vector3*))&T::Intersects).staticmethod("Intersects")
			//.def("Intersects", (bool (*)(const T&, const Box3&, Vector3*))&T::Intersects).staticmethod("Intersects")

			.def("intersects", (bool (T::*)(const Triangle&, Vector3*) const)&T::Intersects)
			.def("intersects", (bool (T::*)(const Box3&, Vector3*) const)&T::Intersects)

			.def("Multiply", (T (*)(const T&, const Matrix&))&T::Multiply).staticmethod("Multiply")

			.def(py::self * py::other<Matrix>())
			.def(py::self *= py::other<Matrix>())

			.def_readwrite("a", &T::a)
			.def_readwrite("b", &T::b)
			;
	}

	{
		typedef color_t T;

		DISPOSEME(3) py::class_<T>("Color")
			.def(py::init<float, float, float, float>((
				py::arg("red"), 
				py::arg("green"),
				py::arg("blue"),
				py::arg("alpha") = 1.0f)))
			.def("Lerp", (T (*)(float, const T&, const T&))&T::Lerp).staticmethod("Lerp")
			.def(py::init<unsigned int>())
			.def(py::init<std::string>())
			.def(py::init<T>())

			.def(py::self * py::other<color_t>())
			.def(py::self *= py::other<color_t>())
			.def(py::self == py::other<color_t>())

			.add_property("red", &T::GetRed, &T::SetRed)
			.add_property("green", &T::GetGreen, &T::SetGreen)
			.add_property("blue", &T::GetBlue, &T::SetBlue)
			.add_property("alpha", &T::GetAlpha, &T::SetAlpha)

			.def_readwrite("internal", &T::color)
			;

		py::implicitly_convertible<std::string, T>();

		py::enum_<math::InterpolationType>("Interpolation")
			.value("Linear", math::LINEAR)
			.value("Sine", math::SINE)
			.value("Quint", math::QUINT)
			.value("Quart", math::QUART)
			.value("Expo", math::EXPO)
			.value("Elastic", math::ELASTIC)
			.value("Cubic", math::CUBIC)
			.value("Circ", math::CIRC)
			.value("Bounce", math::BOUNCE)
			.value("Back", math::BACK)
			.export_values()
			;

		py::enum_<math::EaseFlags>("Ease")
			.value("In", math::EASE_IN)
			.value("Out", math::EASE_OUT)
			.value("InOut", math::EASE_IN_OUT)
			.export_values()
			;
	}

	{
		typedef light_t T;

		py::scope _e = py::class_<T>("Light")
			.def_readwrite("type", &T::type)
			.def_readwrite("diffuse", &T::diffuse)
			.def_readwrite("specular", &T::specular)
			.def_readwrite("ambient", &T::ambient)
			.def_readwrite("position", &T::position)
			.def_readwrite("direction", &T::direction)
			.def_readwrite("fake_shadow_direction", &T::fake_shadow_direction)
			.def_readwrite("range", &T::range)
			.def_readwrite("attenuation", &T::attenuation)
			.def_readwrite("angle", &T::angle)
			.def_readwrite("enabled", &T::enabled)
			;

		py::enum_<T::LightType>("Type")
			.value("Point", T::LIGHT_POINT)
			.value("Spot", T::LIGHT_SPOT)
			.value("Directional", T::LIGHT_DIRECTIONAL)
			.export_values()
			;

		DISPOSEME(2) py::class_<T::range_t>("Range")
			.def_readwrite("cutoff", &T::range_t::cutoff)
			.def_readwrite("falloff", &T::range_t::falloff)
			;

		DISPOSEME(2) py::class_<T::attenuation_t>("Attenuation")
			.def_readwrite("constant", &T::attenuation_t::constant)
			.def_readwrite("linear", &T::attenuation_t::linear)
			.def_readwrite("quadratic", &T::attenuation_t::quadratic)
			;

		DISPOSEME(2) py::class_<T::angle_t>("Angle")
			.def_readwrite("inner", &T::angle_t::inner)
			.def_readwrite("outer", &T::angle_t::outer)
			;

		DISPOSEME(3) _e;
	}

	struct BooleanProxy
	{
		BooleanProxy(const std::string& str)
			: value(str == "1" || boost::iequals(str, "TRUE"))
		{}
		operator bool() const { return value; }
		bool value;
	};

	struct FloatProxy
	{
		FloatProxy(const std::string& str)
			: value(boost::lexical_cast<float>(str))
		{}
		operator float() const { return value; }
		float value;
	};

	struct IntegerProxy
	{
		IntegerProxy(const std::string& str)
			: value(boost::lexical_cast<int>(str))
		{}
		operator int() const { return value; }
		int value;
	};

	struct InterpolationProxy
	{
		InterpolationProxy(const std::string& str)
			: value(math::LINEAR)
		{
			if (boost::iequals(str, "Linear")) value = math::LINEAR; else 
			if (boost::iequals(str, "Sine")) value = math::SINE; else 
			if (boost::iequals(str, "Quint")) value = math::QUINT; else 
			if (boost::iequals(str, "Quart")) value = math::QUART; else 
			if (boost::iequals(str, "Expo")) value = math::EXPO; else 
			if (boost::iequals(str, "Elastic")) value = math::ELASTIC; else 
			if (boost::iequals(str, "Cubic")) value = math::CUBIC; else 
			if (boost::iequals(str, "Circ")) value = math::CIRC; else 
			if (boost::iequals(str, "Bounce")) value = math::BOUNCE; else
			if (boost::iequals(str, "Back")) value = math::BACK; else 
				std::clog << "Can't convert string '" << str << "' to Interpolation type, assuming Linear." << std::endl;
		}
		operator math::InterpolationType() const { return value; }
		math::InterpolationType value;
	};

	struct EaseFlagsProxy
	{
		EaseFlagsProxy(const std::string& str)
			: value(math::EASE_IN_OUT)
		{
			if (boost::iequals(str, "In")) value = math::EASE_IN; else 
			if (boost::iequals(str, "Out")) value = math::EASE_OUT; else 
			if (boost::iequals(str, "InOut")) value = math::EASE_IN_OUT; else 
				std::clog << "Can't convert string '" << str << "' to Ease type, assuming InOut." << std::endl;
		}
		operator math::EaseFlags() const { return value; }
		math::EaseFlags value;
	};

	py::implicitly_convertible<std::string, BooleanProxy>();
	py::implicitly_convertible<std::string, FloatProxy>();
	py::implicitly_convertible<std::string, IntegerProxy>();
	py::implicitly_convertible<std::string, InterpolationProxy>();
	py::implicitly_convertible<std::string, EaseFlagsProxy>();

	py::implicitly_convertible<std::string, math::Box2>();

	py::implicitly_convertible<BooleanProxy, bool>();
	py::implicitly_convertible<FloatProxy, float>();
	py::implicitly_convertible<IntegerProxy, int>();
	py::implicitly_convertible<InterpolationProxy, math::InterpolationType>();
	py::implicitly_convertible<EaseFlagsProxy, math::EaseFlags>();

	tuple_to_vector<vec2_t>();
	tuple_to_vector<vec3_t>();
	tuple_to_vector<vec4_t>();
	tuple_to_vector<box2_t>();
	tuple_to_vector<box3_t>();
	tuple_to_quaternion();

	tuple_to_vector<point_t, int>();
	tuple_to_vector<dim_t, int>();
	tuple_to_vector<range_t, int>();
	tuple_to_vector<rect_t, int>();	
}
