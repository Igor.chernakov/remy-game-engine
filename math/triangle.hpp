#pragma once

namespace math
{
	struct Triangle
	{
		Vector3 a, b, c, n;

		Triangle();
		Triangle(const Vector3& a, const Vector3& b, const Vector3& c);
		Triangle(const Vector3& a, const Vector3& b, const Vector3& c, const Vector3& n);
	};
}