#pragma once

#define DEFINE_DEFAULT_VECTOR_OPS(T) \
	inline bool operator==(const T& param) const { return T::Equal(*this, param); } \
	inline bool operator!=(const T& param) const { return !T::Equal(*this, param); } \
	inline T& operator+=(const T& param) { return *this = T::Add(*this, param); } \
	inline T& operator-=(const T& param) { return *this = T::Substract(*this, param); } \
	inline T& operator*=(const T& param) { return *this = T::Multiply(*this, param); } \
	inline T& operator/=(const T& param) { return *this = T::Divide(*this, param); } \
	inline T operator+(const T& param) const { return T::Add(*this, param); } \
	inline T operator-(const T& param) const { return T::Substract(*this, param); } \
	inline T operator*(const T& param) const { return T::Multiply(*this, param); } \
	inline T operator/(const T& param) const { return T::Divide(*this, param); } \
	inline T& operator*=(Real x) { return *this = T::Scale(*this, x); } \
	inline T& operator/=(Real x) { return *this = T::Scale(*this, 1 / x); } \
	inline T operator*(Real x) const { return T::Scale(*this, x); } \
	inline T operator/(Real x) const { return T::Scale(*this, 1 / x); } \
	inline T operator-(void) const { return T::Negate(*this); } \
	inline bool Equal(const T& param) { return T::Equal(*this, param); } \
	static bool Equal(const T& left, const T& right); \
	inline T& Add(const T& param) { return *this = T::Add(*this, param); } \
	static T Add(const T& left, const T& right); \
	inline T& Substract(const T& param) { return *this = T::Substract(*this, param); } \
	static T Substract(const T& left, const T& right); \
	inline T& Multiply(const T& param) { return *this = T::Multiply(*this, param); } \
	static T Multiply(const T& left, const T& right); \
	inline T& Divide(const T& param) { return *this = T::Divide(*this, param); } \
	static T Divide(const T& left, const T& right); \
	inline T& Scale(Real d) { return *this = T::Scale(*this, d); } \
	static T Scale(const T& param, Real d); \
	inline T& Normalize() { return *this = T::Normalize(*this); } \
	static T Normalize(const T& param); \
	inline T& Negate() { return *this = T::Negate(*this); } \
	static T Negate(const T& param); \
	inline Real Length() const { return T::Length(*this); } \
	static Real Length(const T& param); \
	inline Real Dot(const T& param) const { return T::Dot(*this, param); } \
	static Real Dot(const T& left, const T& right); \
	static T Lerp(Real d, const T& left, const T& right); \
	inline const Real &operator[](int i) const { return v[i]; } \
	inline Real &operator[](int i) { return v[i]; } \
	inline operator Real*(void) { return v; } \
	inline Real operator%(const T& param) const { return T::Dot(*this, param); }

#define DEFINE_DEFAULT_PAIR_OPS(T, V) \
	inline T& operator+=(const V& v) { return *this = T::Add(*this, v); } \
	inline T& operator*=(const V& param) { return *this = T::Multiply(*this, param); } \
	inline T& operator/=(const V& param) { return *this = T::Divide(*this, param); } \
	inline T operator+(const V& param) const { return T::Add(*this, param); } \
	inline T operator*(const V& param) const { return T::Multiply(*this, param); } \
	inline T operator/(const V& param) const { return T::Divide(*this, param); } \
	inline T& operator*=(Real x) { return *this = T::Scale(*this, x); } \
	inline T& operator/=(Real x) { return *this = T::Scale(*this, 1 / x); } \
	inline T operator*(Real x) const { return T::Scale(*this, x); } \
	inline T operator/(Real x) const { return T::Scale(*this, 1 / x); } \
	inline T& Expand(Real param) { return *this = T::Expand(*this, param); } \
	static T Expand(const T& left, Real right); \
	inline T& Expand(const V& param) { return *this = T::Expand(*this, param); } \
	static T Expand(const T& left, const V& right); \
	inline T& Multiply(const V& param) { return *this = T::Multiply(*this, param); } \
	static T Multiply(const T& left, const V& right); \
	inline T& Divide(const V& param) { return *this = T::Divide(*this, param); } \
	static T Divide(const T& left, const V& right); \
	inline T& Add(const V& v) { return *this = T::Add(*this, v); } \
	static T Add(const T& r, const V& v); \
	inline T& Scale(Real d) { return *this = T::Scale(*this, d); } \
	static T Scale(const T& param, Real d); \
	inline bool Contains(const V& v) const { return T::Contains(*this, v); } \
	static bool Contains(const T& r, const V& p);
