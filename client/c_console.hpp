#pragma once

#include "../interfaces/iconsole.hpp"

namespace client
{
	class Console: public IConsole
	{
	public:
		Console();

		virtual void Paint(IOverlay* painter) const;
		virtual bool ProcessKey(int key);
		
		void Exec(const std::string& command);

	private:
		std::list<std::string> m_Commands;
		std::list<std::string>::iterator m_Command;
		bool m_Visible;
		shared_ptr<IWindow> m_Window;
	};
}