#pragma once

#include "resource.hpp"
#include "types.hpp"

typedef unsigned int hash_t;

namespace shared {
	namespace css {
		class Atom {
		public:
			Atom(const char **cc);
			const std::string& name() const { return m_name; }
			const std::string& id() const { return m_id; }
			const std::list<std::string>& classes() const { return m_classes; }
			const std::list<std::string>& states() const { return m_states; }

			struct {
				std::list<hash_t> classes;
				std::list<hash_t> states;
				hash_t name, id;
			} hash;
		private:
			std::string m_name, m_id;
			std::list<std::string> m_classes, m_states;
		};

		typedef std::list<Atom> Atoms;

		class Class: public std::map<std::string, std::string> {
		public:
			Class() {
			}

			Class(const Atoms &atoms) : m_atoms(atoms) {
			}

			Class operator+ (const Class& _class);

			inline Class& operator+= (const Class& _class) {
				return *this = *this + _class;
			}

			const Atoms& atoms() const {
				return m_atoms;
			}

		private:
			Atoms m_atoms;
		};

		class Sheet
			: public shared::Resource
			, public std::list<Class> {
		public:
			Sheet(const std::string& filename);
		};
	}
}