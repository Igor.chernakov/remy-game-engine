#pragma once

#include "irenderer.hpp"

struct IComposer: IObject
{
	virtual void Compose(IRenderer* renderer) = 0;
	virtual bool Disposed() const = 0;
};