#pragma once

#include "../interfaces/ihelper.hpp"

#include "c_surface.hpp"

namespace client {

	class Helper: public IHelper
	{
	public:
		Helper();
		~Helper();

		void Render(struct IRenderer* renderer) const;
		void Erase();
		void Line(const vec3_t &a, const vec3_t &b, color_t color = color_t(), const mat4_t *matrix = NULL);
		void Box(const box3_t &box, color_t color = color_t(), const mat4_t *matrix = NULL);

	private:
		static const int nMaxVertices = 4096;
		std::list<Surface> lines;
		std::list<mat4_t> matrices;
		vertex_t pVertices[nMaxVertices];
		mutable size_t nVertices;
	};

};
