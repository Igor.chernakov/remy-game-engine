GA-SDK-IOS
==========

GameAnalytics native iOS SDK.

Documentation in the [wiki](https://github.com/GameAnalytics/GA-SDK-IOS/wiki).

Read more about our launch of the new events (V2) [here](http://www.gameanalytics.com/update/).

> :information_source:
> Requirements:<br/>
> **iOS:** iOS 6.x and up

Changelog
---------
**2.0.4**
* increased allowed character count to 64 for many parameters

**2.0.3**
* fixed an issue with going-to-background on iOS6
* fixed submit of birthyear value

**2.0.2**
* fixed a bug for iOS6

**2.0.1**
* iOS SDK for V2 api
* progression event
* validated business event
* resource event
* custom dimensions

<!--
Install Cocoapods
==========
sudo gem update --system
sudo gem install cocoapods
pod setup

Install Podfile dependencies
==========
(terminal in project dir where Podfile is)
pod install
-->
