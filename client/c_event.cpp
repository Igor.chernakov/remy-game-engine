#include "c_client.hpp"
#include "c_event.hpp"

#include <boost/python/raw_function.hpp>

namespace py = boost::python;

namespace client
{
	Event::Event()
		: m_call_count(0)
	{
	}

	Event::~Event()
	{
	}

	py::object Event::Fire(py::tuple args, py::dict kw)
	{
		return py::extract<Event&>(args[0])()(py::tuple(args.slice(1, py::_)), kw);
	}

	int Event::GetHandlerCount() const 
	{
		return (int)m_handlers.size();
	}

	Event& Event::operator+= (py::object handler)
	{
		try 
		{
			PyObject* ptr = handler.ptr();

			if (PyMethod_Check(ptr) && PyMethod_Self(ptr))
			{
				PyObject* method = PyMethod_New(PyMethod_Function(ptr), 0, PyMethod_Class(ptr));
				PyObject* self_ref = PyWeakref_NewRef(PyMethod_Self(ptr), 0);
				m_handlers.push_back(Handler(py::object(py::handle<>(method)), py::object(py::handle<>(self_ref))));
			}
			else
			{
				m_handlers.push_back(Handler(handler, py::object()));
			}
		}
		catch script_error;
		return *this;
	}

	void Event::AddOneTimer(boost::python::object handler)
	{
		*this += handler;

		m_onetimers.push_back(m_handlers.back());
		m_handlers.pop_back();
	}

	Event& Event::operator-= (py::object handler)
	{
		try 
		{
			PyObject* ptr = handler.ptr();
			py::object first, second;
			if (PyObject* self = PyMethod_Self(ptr))
			{
				PyObject* method = PyMethod_New(PyMethod_Function(ptr), 0, PyMethod_Class(ptr));
				PyObject* self_ref = PyWeakref_NewRef(PyMethod_Self(ptr), 0);
				first = py::object(py::handle<>(method));
				second = py::object(py::handle<>(self_ref));
			}
			else
			{
				first = handler;
				second = py::object();
			}
			for (std::list<Handler>::iterator it = m_handlers.begin(); it != m_handlers.end(); it++) {
				if (it->first == first && it->second == second && !it->useless) {
					it->useless = true; //unsubscribe just once
					break;
				}
			}
		}
		catch script_error;
		return *this;
	}

	py::object Event::operator()(py::tuple args, py::dict kw)
	{
		using namespace py::detail;
		try 
		{
			struct is_expired
			{
				bool operator()(const Handler& handler) const
				{
					return handler.second && PyWeakref_GetObject(handler.second.ptr()) == Py_None;
				}
			};
			struct invoke
			{
				py::tuple args;
				py::dict kw;
				
				invoke(py::tuple args, py::dict kw): args(args), kw(kw) {}

				void operator()(const Handler& handler) const
				{
					if (handler.useless)
						return;
					if (handler.second)
					{
						PyObject* method = PyMethod_New(PyMethod_Function(handler.first.ptr()),
							PyWeakref_GetObject(handler.second.ptr()), PyMethod_Class(handler.first.ptr()));
						if (!PyObject_Call(method, args.ptr(), kw.ptr()))
						{
							PyErr_Print();
						}
						Py_DecRef(method);
					}
					else
					{
						handler.first(*args, **kw);
					}
				}
			};
			m_handlers.erase(std::remove_if(m_handlers.begin(), m_handlers.end(), is_expired()), m_handlers.end());
			m_onetimers.erase(std::remove_if(m_onetimers.begin(), m_onetimers.end(), is_expired()), m_onetimers.end());
			
			std::for_each(m_handlers.begin(), m_handlers.end(), invoke(args, kw));
			std::for_each(m_onetimers.begin(), m_onetimers.end(), invoke(args, kw));
			
			m_handlers.erase(
				std::remove_if(m_handlers.begin(), m_handlers.end(),
				boost::bind(&Handler::useless, _1)), m_handlers.end());
			m_onetimers.clear();

			m_call_count++;
		}
		catch script_error;
		return py::object();
	}
}

void export_Event()
{
	DISPOSEME(4) py::class_<client::Event>("Event")
		//.def("__len__", &client::Event::GetHandlerCount)
		.def("__iadd__", py::make_function(&client::Event::operator+=, py::return_internal_reference<>()))
		.def("__isub__", py::make_function(&client::Event::operator-=, py::return_internal_reference<>()))
		.def("__call__", py::raw_function(&client::Event::Fire))
		.def("Clear", &client::Event::Clear)
		.def("TriggerOnce", &client::Event::AddOneTimer)
		.add_property("hitcount", &client::Event::GetCallCount)
		;

	py::enum_<client::MouseButtons>("MouseButtons")
		.value("None", client::BUTTON_NONE)
		.value("Left", client::BUTTON_LEFT)
		.value("Right", client::BUTTON_RIGHT)
		.value("Middle", client::BUTTON_MIDDLE)
		.value("XButton1", client::BUTTON_X1)
		.value("XButton2", client::BUTTON_X2)
		.export_values()
		;

	DISPOSEME(4) py::class_<client::MouseEventArgs>("MouseEventArgs")
		.def_readonly("button", &client::MouseEventArgs::button)
		.def_readonly("clicks", &client::MouseEventArgs::clicks)
		.def_readonly("delta", &client::MouseEventArgs::delta)
		.def_readonly("x", &client::MouseEventArgs::x)
		.def_readonly("y", &client::MouseEventArgs::y)
		;
}