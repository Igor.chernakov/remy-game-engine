#pragma once

#define deg2rad(f) ((f) * math::PI / 180)
#define rad2deg(f) ((f) * 180 / math::PI)
#define anglemod(a) ((360.f/65536) * ((int)((a)*(65536.f/360.0f)) & 65535))

#include <float.h>
#include <math.h>
#include <iostream>
#include <string>

namespace math
{
	static const float PI = 3.14159265369f;
	static const float _2PI_ = 6.283185307179586476925286766559f;
	static const float EPS = 0.001f;

	float random();
	float signed_random();
	int sign(const float& k);
	unsigned int log2(unsigned int val);
	unsigned int pow2(unsigned int value);
}

#ifndef MAX 
#       define MAX(x, y) ((x) > (y) ? (x) : (y))
#endif

#ifndef MIN
#       define MIN(x, y) ((x) < (y) ? (x) : (y))
#endif

#include "macros.hpp"
#include "vector.hpp"
#include "vector2.hpp"
#include "vector3.hpp"
#include "vector4.hpp"
#include "quaternion.hpp"
#include "affineparts.hpp"
#include "matrix.hpp"
#include "box2.hpp"
#include "box3.hpp"
#include "line.hpp"
#include "triangle.hpp"
#include "easing.hpp"
#include "color.hpp"

namespace math
{
	struct Vertex
	{
		//keep this structire syncronized
		//with renderer

		static const int TEXCOORDS = 1;

		Vector3 position;
		Vector4 blend_weight;
		Color blend_indices;
		Vector3 normal;
		Color color;
		Vector2 tc[TEXCOORDS];
	};

	struct Light
	{
		Light(): enabled(false) {}

		enum LightType {
			LIGHT_POINT          = 1,
			LIGHT_SPOT           = 2,
			LIGHT_DIRECTIONAL    = 3,
			LIGHT_FORCE_DWORD    = 0x7fffffff, /* force 32-bit size enum */
		};

		LightType type;
		Color diffuse, specular, ambient;
		Vector3 position, direction, fake_shadow_direction;
		struct range_t { float cutoff, falloff; } range;
		struct attenuation_t { float constant, linear, quadratic; } attenuation;
		struct angle_t { float inner, outer; } angle;
		bool enabled;
	};

	typedef unsigned short Index;
}

typedef math::Real vec_t;
typedef math::Vector2 vec2_t;
typedef math::Vector3 vec3_t;
typedef math::Vector4 vec4_t;
typedef math::Quaternion quat_t;
typedef math::Matrix mat4_t;
typedef math::AffineParts ap_t;
typedef math::Box2 box2_t;
typedef math::Box3 box3_t;
typedef math::Line line_t;
typedef math::Color color_t;
typedef math::Vertex vertex_t;
typedef math::Index index_t;
typedef math::Light light_t;

#ifdef _MSC_VER
std::istream& operator>>(std::istream& str, vec2_t& v);
std::istream& operator>>(std::istream& str, vec3_t& v);
std::istream& operator>>(std::istream& str, vec4_t& v);
std::istream& operator>>(std::istream& str, quat_t& v);
std::istream& operator>>(std::istream& str, box2_t& v);
std::istream& operator>>(std::istream& str, box3_t& v);
std::istream& operator>>(std::istream& str, color_t& v);
#else
std::istream& operator>>(std::istream&& str, vec2_t& v);
std::istream& operator>>(std::istream&& str, vec3_t& v);
std::istream& operator>>(std::istream&& str, vec4_t& v);
std::istream& operator>>(std::istream&& str, quat_t& v);
std::istream& operator>>(std::istream&& str, box2_t& v);
std::istream& operator>>(std::istream&& str, box3_t& v);
std::istream& operator>>(std::istream&& str, color_t& v);
#endif

std::ostream& operator<<(std::ostream& str, const vec2_t& v);
std::ostream& operator<<(std::ostream& str, const vec3_t& v);
std::ostream& operator<<(std::ostream& str, const vec4_t& v);
std::ostream& operator<<(std::ostream& str, const quat_t& v);
std::ostream& operator<<(std::ostream& str, const box2_t& v);
std::ostream& operator<<(std::ostream& str, const box3_t& v);
std::ostream& operator<<(std::ostream& str, const color_t& v);
