#include "math.hpp"
#include <stdexcept>
#include <sstream>

using namespace math;

const Quaternion Quaternion::Identity(0,0,0,1);

const Quaternion Quaternion::Zero(0,0,0,0);

Quaternion::Quaternion()
{
	Assign(0, 0, 0, 1);
}
Quaternion::Quaternion(Real _x, Real _y, Real _z, Real _w)
{
	Assign(_x, _y, _z, _w);
}
Quaternion::Quaternion(Real yaw, Real pitch, Real roll)
{
	*this = RotationYawPitchRoll(yaw, pitch, roll);
}
Quaternion::Quaternion(const Real* _v)
{
	Assign(_v[0], _v[1], _v[2], _v[3]);
}
Quaternion::Quaternion(const Quaternion& _v)
{
	Assign(_v[0], _v[1], _v[2], _v[3]);
}
Quaternion::Quaternion(const Matrix& m)
{
	*this = RotationMatrix(m);
}

Quaternion::Quaternion(const std::string& param)
{
	std::stringstream(param) >> *this;

	if (fabs(w) <= EPS)
	{
		*this = Quaternion(x, y, z);
	}
}

void Quaternion::Assign(Real x, Real y, Real z, Real w)
{
	v[0] = x, v[1] = y, v[2] = z, v[3] = w;
}

bool Quaternion::Equal(const Quaternion& left, const Quaternion& right)
{
	return fabs(left.x - right.x) < EPS && fabs(left.y - right.y) < EPS && fabs(left.z - right.z) < EPS && fabs(left.w - right.w) < EPS;
}

Quaternion Quaternion::Add(const Quaternion& qa, const Quaternion& qb)
{
	return Quaternion(qa.x + qb.x, qa.y + qb.y, qa.z + qb.z, qa.w + qb.w);
}

Quaternion Quaternion::Substract(const Quaternion& qa, const Quaternion& qb)
{
	return Quaternion(qa.x - qb.x, qa.y - qb.y, qa.z - qb.z, qa.w - qb.w);
}

Quaternion Quaternion::Multiply(const Quaternion& qa, const Quaternion& qb)
{
	Quaternion r;

	r.x = qa.w * qb.x + qa.x * qb.w + qa.y * qb.z - qa.z * qb.y;
	r.y = qa.w * qb.y + qa.y * qb.w + qa.z * qb.x - qa.x * qb.z;
	r.z = qa.w * qb.z + qa.z * qb.w + qa.x * qb.y - qa.y * qb.x;
	r.w = qa.w * qb.w - qa.x * qb.x - qa.y * qb.y - qa.z * qb.z;

	return r;
}

Quaternion Quaternion::Divide(const Quaternion& qa, const Quaternion& qb)
{
	throw std::runtime_error("Quaternion::Divide() is not implemented");
}

Quaternion Quaternion::Scale(const Quaternion& q, Real k)
{
	return Quaternion(q.x * k, q.y * k, q.z * k, q.w * k);
}

Real Quaternion::Length(const Quaternion& param)
{
	return sqrt(Dot(param, param));
}

Quaternion Quaternion::Normalize(const Quaternion& param)
{
	Quaternion r;

	Real length = Length(param);

	if (length < EPS)
	{
		return param;
	}

	r.x = param.x / length;
	r.y = param.y / length;
	r.z = param.z / length;
	r.w = param.w / length;

	return r;
}

Quaternion Quaternion::Negate(const Quaternion& param)
{
	Quaternion r;

	r.x = -param.x;
	r.y = -param.y;
	r.z = -param.z;
	r.w =  param.w;

	r.Normalize();

	return r;
}

Real Quaternion::Dot(const Quaternion& left, const Quaternion& right)
{
	return left.w * right.w + left.x * right.x + left.y * right.y + left.z * right.z;
}

Quaternion Quaternion::RotationYawPitchRoll(Real yaw, Real pitch, Real roll)
{
	Quaternion r;

	r = Quaternion(Matrix::RotationYawPitchRoll(yaw, pitch, roll));

	return r;
}

Quaternion Quaternion::RotationMatrix(const Matrix& m)
{
	Quaternion r;

	// Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
	// article "Quaternion Calculus and Fast Animation".

	Real fTrace = m.v[0][0] + m.v[1][1] + m.v[2][2];
	Real fRoot;

	if ( fTrace > 0.0 )
	{
		// |w| > 1/2, may as well choose w > 1/2
		fRoot = sqrt(fTrace + 1.0f);  // 2w
		r.w = 0.5f * fRoot;
		fRoot = 0.5f / fRoot;  // 1/(4w)
		r.x = (m.v[1][2] - m.v[2][1]) * fRoot;
		r.y = (m.v[2][0] - m.v[0][2]) * fRoot;
		r.z = (m.v[0][1] - m.v[1][0]) * fRoot;
	}
	else
	{
		// |w| <= 1/2
		static size_t s_iNext[3] = { 1, 2, 0 };
		size_t i = 0;
		if ( m.v[1][1] > m.v[0][0] )
			i = 1;
		if ( m.v[2][2] > m.v[i][i] )
			i = 2;
		size_t j = s_iNext[i];
		size_t k = s_iNext[j];

		fRoot = sqrt(m.v[i][i] - m.v[j][j] - m.v[k][k] + 1.0f);
		Real* apkQuat[3] = { &r.x, &r.y, &r.z };
		*apkQuat[i] = 0.5f*fRoot;
		fRoot = 0.5f/fRoot;
		r.w = (m.v[j][k] - m.v[k][j]) * fRoot;
		*apkQuat[j] = (m.v[i][j] + m.v[j][i]) * fRoot;
		*apkQuat[k] = (m.v[i][k] + m.v[k][i]) * fRoot;
	}

	r.Normalize();

	return r;
}

Quaternion Quaternion::Lerp(Real t, const Quaternion& p, const Quaternion& q)
{
	Quaternion r;

	float p1[4];

	double omega, cosom, sinom, scale0, scale1;

	cosom = p.x * q.x + p.y * q.y + p.z * q.z + p.w * q.w;


	if ( cosom <0.0 )
	{
		cosom = -cosom;
		p1[0] = - p.x;  p1[1] = - p.y;
		p1[2] = - p.z;  p1[3] = - p.w;
	}
	else
	{
		p1[0] = p.x;    p1[1] = p.y;
		p1[2] = p.z;    p1[3] = p.w;
	}



	if ( (1.0 - cosom) > EPS )
	{
		omega = acos(cosom);
		sinom = sin(omega);
		scale0 = sin(t * omega) / sinom;
		scale1 = sin((1.0 - t) * omega) / sinom;
	}
	else
	{
		scale0 = t;
		scale1 = 1.0 - t;
	}

	r.x = (float)( scale0 * q.x + scale1 * p1[0]);
	r.y = (float)( scale0 * q.y + scale1 * p1[1]);
	r.z = (float)( scale0 * q.z + scale1 * p1[2]);
	r.w = (float)( scale0 * q.w + scale1 * p1[3]);

	return r;
}

Vector3 QuatToEuler(const Quaternion* quat)
{
	Vector3 rr;

	Real sqw = quat->w * quat->w;
	Real sqx = quat->x * quat->x;
	Real sqy = quat->y * quat->y;
	Real sqz = quat->z * quat->z;

	rr.x = rad2deg((Real)atan2l(2.0 * ( quat->y * quat->z + quat->x * quat->w ) , ( -sqx - sqy + sqz + sqw )));
	rr.y = rad2deg((Real)asinl(-2.0 * ( quat->x * quat->z - quat->y * quat->w )));
	rr.z = rad2deg((Real)atan2l(2.0 * ( quat->x * quat->y + quat->z * quat->w ) , (  sqx - sqy - sqz + sqw )));

	return rr;
}

void Quaternion::SetYaw(float value)
{
	*this = RotationYawPitchRoll(value, GetPitch(), GetRoll());
}

float Quaternion::GetYaw() const
{
	return QuatToEuler(this).x;
}

void Quaternion::SetPitch(float value)
{
	*this = RotationYawPitchRoll(GetYaw(), value, GetRoll());
}

float Quaternion::GetPitch() const
{
	return QuatToEuler(this).y;
}

void Quaternion::SetRoll(float value)
{
	*this = RotationYawPitchRoll(GetYaw(), GetPitch(), value);
}

float Quaternion::GetRoll() const
{
	return QuatToEuler(this).z;
}
