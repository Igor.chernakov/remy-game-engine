#pragma once

#include "../interfaces/ioverlay.hpp"
#include "c_surface.hpp"

namespace client {

	class Overlay
		: public IOverlay
	{
	public:
		Overlay();
		~Overlay();
		void Render(struct IRenderer* renderer) const;
		void Erase();

	public:

		const ISurface* String(
			const std::wstring& s,
			const mat4_t& m_Matrix,
			const std::string& font = "system",
			const ColorPair& nColor = ColorPair(),
			const ColorPair& nHiglightColor = ColorPair(),
			const TextDimensions& td = TextDimensions(),
			dim_t *out_size = 0);

		const ISurface* Quad(
			shared_ptr<const IMaterial> pMaterial,
			const box2_t& vScreenRect,
			const box2_t& vTextureRect = box2_t(0.0f,0.0f,1.0f,1.0f),
			const color_t& nColor = color_t(),
			const mat4_t *pMatrix = 0);

		int GetLineWidth(const std::wstring& text, const std::string& fontname, int size, int max_length = 0, int letter_spacing = 0) const;

		void LoadSystemFont();
		inline bool IsSystemFontLoaded() const { return _systemfontloaded; }

	public:
		/*class Font: public IResource
		{
			static const int max_chars = 256;
			static const int max_pages = 32;
			static const int chars_in_row = 16;

			std::string m_name;

		public:
			Font(const std::string& filename);

			const std::string& GetPath() const { return m_name; }

			inline size_t GetLetterSize() const { return m_image->GetWidth() / chars_in_row; }
			inline char GetWidth(char c) const { return m_table[*(unsigned char*)&c]; }
			inline shared_ptr<const IMaterial> GetImage() const { return m_image; }
			inline bool IsSquare() const { return m_image->GetWidth() == m_image->GetHeight(); }

			struct font_char_t {
				int x, y, width, height, xoffset, yoffset, xadvance, page, chnl;
			};

		private:
			shared_ptr<const IMaterial> m_image;
			char m_table[max_chars];

			font_char_t[256] font_chars;
			shared_ptr<const IMaterial>[max_pages] font_pages;
		};*/

		class Font2: public IResource
		{
			static const int max_chars = 65536;
			static const int max_pages = 32;

			std::string m_name;

		public:
			Font2(const std::string& filename);

			const std::string& GetPath() const { return m_name; }

			struct font_char_t {
				wchar_t id;
				short x, y, width, height, xoffset, yoffset, xadvance, page, chnl;
				box2_t uv;
			};

			inline const font_char_t& GetChar(unsigned short i) const { return font_chars[i]; }
			inline shared_ptr<const IMaterial> GetPage(int i) const { return font_pages[i]; }

			struct info_t {
				int size, outline, yoffset;
			} info;

			struct common_t {
				int lineHeight, base, scaleW, scaleH;
			} common;

		private:
			font_char_t font_chars[max_chars];
			shared_ptr<const IMaterial> font_pages[max_pages];
		};

		shared_ptr<const Font2> GetFont(const std::string& font) const;

	protected:
		typedef std::map<std::string, shared_ptr<const Font2>, ignorecase_less> Fonts;
		typedef std::list<surface::Mesh> Surfaces;
		typedef std::list<mat4_t> Matrices;

		mutable Fonts m_fonts;
		Surfaces m_surfaces;

		bool _systemfontloaded;
	};

};