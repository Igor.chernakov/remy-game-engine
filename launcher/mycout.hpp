#include <boost/iostreams/concepts.hpp> 
#include <boost/iostreams/stream_buffer.hpp>

#include "../interfaces/iclient.hpp"

namespace bio = boost::iostreams;

class Sink : public bio::sink
{
	color_t color;
	std::string buffer;

public:
	Sink(color_t color): color(color) {}

    std::streamsize write(const char* s, std::streamsize n)
    {
		extern IClient* client;
		for (const char* c = s; *c; c++) {
			if (*c == '\r') {
				client->PublishMessage(buffer, color);
				buffer = std::string();
			} else {
				buffer.push_back(*c);
			}
		}
        return n;
    }
};