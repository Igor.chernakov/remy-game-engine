#pragma once

class XmlSceneExport : public SceneExport {
public:
	XmlSceneExport(void)
	{
		memset(pFlags, 0, sizeof(pFlags));
	}

	~XmlSceneExport(void)
	{
	}

	int ExtCount(void)
	{ // Number of extensions supported
		return(1);
	}

	const TCHAR* Ext(int n)
	{ // Extension #n (i.e. "3DS")
		return("XML");
	}

	const TCHAR* LongDesc(void)
	{ // Long ASCII description (i.e. "Autodesk 3D Studio File")
		return("Shuttle Scene File");
	}

	const TCHAR* ShortDesc(void)
	{ // Short ASCII description (i.e. "3D Studio")
		return("Shuttle Scene");
	}

	const TCHAR* AuthorName(void)
	{ // ASCII Author name
		return("Igor Chernakov");
	}

	const TCHAR* CopyrightMessage(void)
	{ // ASCII Copyright message
		return("");
	}

	const TCHAR* OtherMessage1(void)
	{ // Other message #1
		return("");
	}

	const TCHAR* OtherMessage2(void)
	{ // Other message #2
		return("");
	}

	unsigned int Version(void)
	{ // Version number * 100 (i.e. v3.01 = 301)
		return(100);
	}

	void ShowAbout(HWND hWnd)
	{ // Show DLL's "About..." box
	}

	int DoExport(const TCHAR* name, ExpInterface* ei, Interface* i, BOOL suppressPrompts = FALSE, DWORD options = 0);

	BOOL SupportsOptions(int ext, DWORD options)
	{
		assert (ext == 0);	// We only support one extension
		switch (options)
		{
			case SCENE_EXPORT_SELECTED:
				return TRUE;
				break;
			default:
				return(FALSE);
		};
	}

private:
	void ProcessNode(std::ofstream& file, INode* node, Interface* gi, MySceneEnumProc& myScene, int level);
	void ProcessMesh(std::ofstream& file, INode* node, Interface* gi, int level);

public:
	enum Flags {
		FLAG_EXPORTMESHES,
		FLAG_EMBEDMESHES,
		FLAG_EXPORTANIMATIONS,
		FLAG_LAST
	};

	UINT pFlags[FLAG_LAST];
};
