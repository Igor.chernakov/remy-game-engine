#pragma once

#include <string>

#include "iobject.hpp"

struct ILocalization: IObject
{
	virtual const std::wstring& GetString(const std::string& name) const = 0;
};