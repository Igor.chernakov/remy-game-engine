#include "s_server.hpp"
#include "s_connection.hpp"


#include <ctime>

IServerFramework* framework = 0;

static Path GameDirectory;

const IFramework* GetFramework()
{
	return framework;
}

std::string make_daytime_string()
{
	using namespace std; // For time_t, time and ctime;
	time_t now = time(0);
	return ctime(&now);
}

namespace client
{
	class Server: public IServer
	{
	public:
		Server(const std::string& directory)
			: acceptor(io_service, boost::asio::ip::tcp::endpoint(
				boost::asio::ip::address_v4(0x7F000001)/*boost::asio::ip::tcp::v4()*/, GetPort()))
		{
			GameDirectory = directory;

			framework->GetFileSystem()->AddPath(GameDirectory);
			framework->GetFileSystem()->LoadPacks();

			framework->SetResources(shared_ptr<IResources>(new IResources()));

			srand(framework->GetTimer()->GetMilliseconds());

			PyThreadState_Swap(Py_NewInterpreter());

			PyDict_SetItemString(PyModule_GetDict(PyImport_ImportModule("sys")), "dont_write_bytecode", PyBool_FromLong(true));
			PySys_SetPath((char*)(GameDirectory / "scripts").native_file_string().c_str());

			std::cout << "Server initialized" << std::endl;
		}
		~Server()
		{
			acceptor.cancel();
		}

		void Run()
		{
			start_accept();

			boost::thread service_thread(boost::bind(&boost::asio::io_service::run, &this->io_service));
			
			PyImport_ImportModule("start");

			if(PyErr_Occurred())
			{
				PyErr_Print();
				PyErr_Clear();
			}

			service_thread.interrupt();
		}
		int GetPort() const
		{
			return 1001;
		}

	private:
		void start_accept()
		{
			Connection::pointer new_connection = Connection::create(acceptor.io_service());
			acceptor.async_accept(new_connection->socket(), boost::bind(&Server::handle_accept, this, new_connection, boost::asio::placeholders::error));
		}

		void handle_accept(Connection::pointer new_connection, const boost::system::error_code& error)
		{
			if (!error)
			{
				new_connection->start();
				start_accept();
			}
		}

	private:
		boost::asio::io_service io_service;
		boost::asio::ip::tcp::acceptor acceptor;
	};
}


extern "C"
{
#ifdef _MSC_VER
	__declspec(dllexport)
#endif
		IServer* CreateServer(const char* directory, IServerFramework* _framework)
	{
		framework = _framework;

		return new client::Server(directory);
	}
}
