/**********************************************************************
*<
FILE: pthelp.cpp

DESCRIPTION:  A dialogue helper implementation

CREATED BY: 

HISTORY: created 14 July 1995

*>	Copyright (c) 1995, All Rights Reserved.
**********************************************************************/

#include "hlp_dialogue.h"

//------------------------------------------------------

// in prim.cpp  - The dll instance handle
extern HINSTANCE hInstance;


#define AXIS_LENGTH 20.0f
#define ZFACT (float).005;

void *CreateDialogueHelpObject();
void resetDialogueParams();

struct DialogueHelpObjClassDesc: ClassDesc2
{
	int 			IsPublic() { return 1; }
	void *			Create(BOOL loading = FALSE) { return CreateDialogueHelpObject(); }
	const TCHAR *	ClassName() { return GetString(IDS_DB_DIALOGUE_CLASS); }
	SClass_ID		SuperClassID() { return HELPER_CLASS_ID; }
	Class_ID		ClassID() { return DIALOGUEHELP_CLASS_ID; }
	const TCHAR* 	Category() { return GetString(IDS_GAME_CATEGORY);  }
	void			ResetClassParams(BOOL fileReset) { if(fileReset) resetDialogueParams(); }
	const TCHAR*	InternalName() {return _T("DialogueHelperObj");}
	HINSTANCE		HInstance() {return hInstance;}			// returns owning module handle
};

static DialogueHelpObjClassDesc dialogueHelpObjDesc;

ClassDesc* GetDialogueHelpDesc() { return &dialogueHelpObjDesc; }



const int PBLOCK_REF_NO = 0;

enum
{
	dialogueobj_size,
	dialogueobj_centermarker,
	dialogueobj_axistripod,
	dialogueobj_cross,
	dialogueobj_box,
	dialogueobj_screensize,
	dialogueobj_drawontop
};

enum 
{
	dialogueobj_params,
};

static ParamBlockDesc2 dialogueobj_param_blk( 

	dialogueobj_params, _T("DialogueObjectParameters"),  0, &dialogueHelpObjDesc, P_AUTO_CONSTRUCT+P_AUTO_UI, PBLOCK_REF_NO,

	//rollout
	IDD_NEW_DIALOGUEPARAM, IDS_DIALOGUE_PARAMS, 0, 0, NULL,

	// params
	dialogueobj_size, _T("size"), TYPE_WORLD, P_ANIMATABLE, IDS_DIALOGUE_SIZE,
		p_default, 20.0,	
		p_ms_default, 20.0,
		p_range, 0.0f, float(1.0E30), 
		p_ui, TYPE_SPINNER, EDITTYPE_UNIVERSE, IDC_DIALOGUE_SIZE, IDC_DIALOGUE_SIZESPIN, SPIN_AUTOSCALE, 
	end, 

	dialogueobj_centermarker, _T("centermarker"), TYPE_BOOL, P_ANIMATABLE, IDS_DIALOGUE_CENTERMARKER,
		p_default, FALSE,
		p_ui, TYPE_SINGLECHEKBOX, IDC_DIALOGUE_MARKER, 
	end, 

	dialogueobj_axistripod, _T("axistripod"), TYPE_BOOL, P_ANIMATABLE, IDS_DIALOGUE_AXISTRIPOD,
		p_default, FALSE,
		p_ui, TYPE_SINGLECHEKBOX, IDC_DIALOGUE_AXIS, 
	end, 

	dialogueobj_cross, _T("cross"), TYPE_BOOL, P_ANIMATABLE, IDS_DIALOGUE_CROSS,
		p_default, FALSE,
		p_ui, TYPE_SINGLECHEKBOX, IDC_DIALOGUE_CROSS, 
	end, 

	dialogueobj_box, _T("box"), TYPE_BOOL, P_ANIMATABLE, IDS_DIALOGUE_BOX,
		p_default, TRUE,
		p_ui, TYPE_SINGLECHEKBOX, IDC_DIALOGUE_BOX, 
	end, 

	dialogueobj_screensize, _T("constantscreensize"), TYPE_BOOL, P_ANIMATABLE, IDS_DIALOGUE_SCREENSIZE,
		p_default, FALSE,
		p_ui, TYPE_SINGLECHEKBOX, IDC_DIALOGUE_SCREENSIZE,
	end, 

	dialogueobj_drawontop, _T("drawontop"), TYPE_BOOL, P_ANIMATABLE, IDS_DIALOGUE_DRAWONTOP,
		p_default, FALSE,
		p_ui, TYPE_SINGLECHEKBOX, IDC_DIALOGUE_DRAWONTOP,
	end, 

	end
);


void DrawAxis(ViewExp *vpt, const Matrix3 &tm, float length, BOOL screenSize);

// class variable for dialogue class.
IObjParam *DialogueHelpObject::ip = NULL;
DialogueHelpObject *DialogueHelpObject::editOb = NULL;


//HWND DialogueHelpObject::hParams = NULL;
//IObjParam *DialogueHelpObject::iObjParams;

//int DialogueHelpObject::dlgShowAxis = TRUE;
//float DialogueHelpObject::dlgAxisLength = AXIS_LENGTH;

void resetDialogueParams() 
{\
	//DialogueHelpObject::dlgShowAxis = TRUE;
	//DialogueHelpObject::dlgAxisLength = AXIS_LENGTH;
}

void *CreateDialogueHelpObject()
{
	return new DialogueHelpObject();
}


void DialogueHelpObject::BeginEditParams(
	IObjParam *ip, ULONG flags,Animatable *prev)
{	
	this->ip = ip;
	editOb   = this;
	dialogueHelpObjDesc.BeginEditParams(ip, this, flags, prev);	
}

void DialogueHelpObject::EndEditParams(
									   IObjParam *ip, ULONG flags,Animatable *next)
{	
	editOb   = NULL;
	this->ip = NULL;
	dialogueHelpObjDesc.EndEditParams(ip, this, flags, next);
	ClearAFlag(A_OBJ_CREATING);
}


DialogueHelpObject::DialogueHelpObject()
{	
	dialogueHelpObjDesc.MakeAutoParamBlocks(this);
	showAxis    = TRUE; //dlgShowAxis;
	axisLength  = 10.0f; //dlgAxisLength;
	suspendSnap = FALSE;
	SetAFlag(A_OBJ_CREATING);
}

DialogueHelpObject::~DialogueHelpObject()
{
	DeleteAllRefsFromMe();
}

IParamArray *DialogueHelpObject::GetParamBlock()
{
	return (IParamArray*)pblock2;
}

int DialogueHelpObject::GetParamBlockIndex(int id)
{
	if (pblock2 && id>=0 && id<pblock2->NumParams()) return id;
	else return -1;
}


class DialogueHelpObjCreateCallBack: public CreateMouseCallBack {
	DialogueHelpObject *ob;
public:
	int proc( ViewExp *vpt,int msg, int dialogue, int flags, IPoint2 m, Matrix3& mat );
	void SetObj(DialogueHelpObject *obj) { ob = obj; }
};

int DialogueHelpObjCreateCallBack::proc(ViewExp *vpt,int msg, int dialogue, int flags, IPoint2 m, Matrix3& mat ) {	

#ifdef _OSNAP
	if (msg == MOUSE_FREEMOVE)
	{
#ifdef _3D_CREATE
		vpt->SnapPreview(m,m,NULL, SNAP_IN_3D);
#else
		vpt->SnapPreview(m,m,NULL, SNAP_IN_PLANE);
#endif
	}
#endif

	if (msg==MOUSE_POINT||msg==MOUSE_MOVE) {
		switch(dialogue) {
			case 0: {

				// Find the node and plug in the wire color
				ULONG handle;
				ob->NotifyDependents(FOREVER, (PartID)&handle, REFMSG_GET_NODE_HANDLE);
				INode *node;
				node = GetCOREInterface()->GetINodeByHandle(handle);
				if (node) {
					Point3 color = GetUIColor(COLOR_POINT_OBJ);
					node->SetWireColor(RGB(color.x*255.0f, color.y*255.0f, color.z*255.0f));
				}

				ob->suspendSnap = TRUE;
#ifdef _3D_CREATE	
				mat.SetTrans(vpt->SnapPoint(m,m,NULL,SNAP_IN_3D));
#else	
				mat.SetTrans(vpt->SnapPoint(m,m,NULL,SNAP_IN_PLANE));
#endif				
				break;
					}

			case 1:
#ifdef _3D_CREATE	
				mat.SetTrans(vpt->SnapPoint(m,m,NULL,SNAP_IN_3D));
#else	
				mat.SetTrans(vpt->SnapPoint(m,m,NULL,SNAP_IN_PLANE));
#endif
				if (msg==MOUSE_POINT) {
					ob->suspendSnap = FALSE;
					return 0;
				}
				break;			
		}
	} else 
		if (msg == MOUSE_ABORT) {		
			return CREATE_ABORT;
		}
		return 1;
}

static DialogueHelpObjCreateCallBack dialogueHelpCreateCB;

CreateMouseCallBack* DialogueHelpObject::GetCreateMouseCallBack() {
	dialogueHelpCreateCB.SetObj(this);
	return(&dialogueHelpCreateCB);
}

void DialogueHelpObject::SetExtendedDisplay(int flags)
{
	extDispFlags = flags;
}


void DialogueHelpObject::GetLocalBoundBox(
	TimeValue t, INode* inode, ViewExp* vpt, Box3& box ) 
{
	Matrix3 tm = inode->GetObjectTM(t);	

	float size;
	int screenSize;
	pblock2->GetValue(dialogueobj_size, t, size, FOREVER);
	pblock2->GetValue(dialogueobj_screensize, t, screenSize, FOREVER);

	float zoom = 1.0f;
	if (screenSize) {
		zoom = vpt->GetScreenScaleFactor(tm.GetTrans())*ZFACT;
	}
	if (zoom==0.0f) zoom = 1.0f;

	size *= zoom;
	box =  Box3(Point3(0,0,0), Point3(0,0,0));
	box += Point3(size*0.5f,  0.0f, 0.0f);
	box += Point3( 0.0f, size*0.5f, 0.0f);
	box += Point3( 0.0f, 0.0f, size*0.5f);
	box += Point3(-size*0.5f,   0.0f,  0.0f);
	box += Point3(  0.0f, -size*0.5f,  0.0f);
	box += Point3(  0.0f,  0.0f, -size*0.5f);

	//JH 6/18/03
	//This looks odd but I'm being conservative an only excluding it for
	//the case I care about which is when computing group boxes
	//	box.EnlargeBy(10.0f/zoom);
	if(!(extDispFlags & EXT_DISP_GROUP_EXT)) 
		box.EnlargeBy(10.0f/zoom);

	/*
	if (showAxis)
	box = GetAxisBox(vpt,tm,showAxis?axisLength:0.0f, TRUE);
	else
	box = Box3(Point3(0,0,0), Point3(0,0,0));
	*/
}

void DialogueHelpObject::GetWorldBoundBox(
	TimeValue t, INode* inode, ViewExp* vpt, Box3& box )
{
	Matrix3 tm;
	tm = inode->GetObjectTM(t);
	Box3 lbox;

	GetLocalBoundBox(t, inode, vpt, lbox);
	box = Box3(tm.GetTrans(), tm.GetTrans());
	for (int i=0; i<8; i++) {
		box += lbox * tm;
	}
	/*
	if(!(extDispFlags & EXT_DISP_ZOOM_EXT) && showAxis)
	box = GetAxisBox(vpt,tm,showAxis?axisLength:0.0f, FALSE);
	else
	box = Box3(tm.GetTrans(), tm.GetTrans());
	*/
}


void DialogueHelpObject::Snap(TimeValue t, INode* inode, SnapInfo *snap, IPoint2 *p, ViewExp *vpt)
{
	if(suspendSnap)
		return;

	Matrix3 tm = inode->GetObjectTM(t);	
	GraphicsWindow *gw = vpt->getGW();	
	gw->setTransform(tm);

	Matrix3 invPlane = Inverse(snap->plane);

	// Make sure the vertex priority is active and at least as important as the best snap so far
	if(snap->vertPriority > 0 && snap->vertPriority <= snap->priority) {
		Point2 fp = Point2((float)p->x, (float)p->y);
		Point2 screen2;
		IPoint3 pt3;

		Point3 thePoint(0,0,0);
		// If constrained to the plane, make sure this dialogue is in it!
		if(snap->snapType == SNAP_2D || snap->flags & SNAP_IN_PLANE) {
			Point3 test = thePoint * tm * invPlane;
			if(fabs(test.z) > 0.0001)	// Is it in the plane (within reason)?
				return;
		}
		gw->wTransPoint(&thePoint,&pt3);
		screen2.x = (float)pt3.x;
		screen2.y = (float)pt3.y;

		// Are we within the snap radius?
		int len = (int)Length(screen2 - fp);
		if(len <= snap->strength) {
			// Is this priority better than the best so far?
			if(snap->vertPriority < snap->priority) {
				snap->priority = snap->vertPriority;
				snap->bestWorld = thePoint * tm;
				snap->bestScreen = screen2;
				snap->bestDist = len;
			}
			else
				if(len < snap->bestDist) {
					snap->priority = snap->vertPriority;
					snap->bestWorld = thePoint * tm;
					snap->bestScreen = screen2;
					snap->bestDist = len;
				}
		}
	}
}




int DialogueHelpObject::DrawAndHit(TimeValue t, INode *inode, ViewExp *vpt)
{
	float size;
	int centerMarker, axisTripod, cross, box, screenSize, drawOnTop;

	Color color(inode->GetWireColor());	

	Interval ivalid = FOREVER;
	pblock2->GetValue(dialogueobj_size, t,         size, ivalid);
	pblock2->GetValue(dialogueobj_centermarker, t, centerMarker, ivalid);
	pblock2->GetValue(dialogueobj_axistripod, t,   axisTripod, ivalid);
	pblock2->GetValue(dialogueobj_cross, t,        cross, ivalid);
	pblock2->GetValue(dialogueobj_box, t,          box, ivalid);
	pblock2->GetValue(dialogueobj_screensize, t,   screenSize, ivalid);
	pblock2->GetValue(dialogueobj_drawontop, t,    drawOnTop, ivalid);

	Matrix3 tm(1);
	Point3 pt(0,0,0);
	Point3 pts[5];

	vpt->getGW()->setTransform(tm);	
	tm = inode->GetObjectTM(t);

	int limits = vpt->getGW()->getRndLimits();
	if (drawOnTop) vpt->getGW()->setRndLimits(limits & ~GW_Z_BUFFER);

	if (inode->Selected()) {
		vpt->getGW()->setColor( TEXT_COLOR, GetUIColor(COLOR_SELECTION) );
		vpt->getGW()->setColor( LINE_COLOR, GetUIColor(COLOR_SELECTION) );
	} else if (!inode->IsFrozen() && !inode->Dependent()) {
		//vpt->getGW()->setColor( TEXT_COLOR, GetUIColor(COLOR_DIALOGUE_AXES) );
		//vpt->getGW()->setColor( LINE_COLOR, GetUIColor(COLOR_DIALOGUE_AXES) );		
		vpt->getGW()->setColor( TEXT_COLOR, color);
		vpt->getGW()->setColor( LINE_COLOR, color);
	}	

	if (axisTripod) {
		DrawAxis(vpt, tm, size, screenSize);
	}

	size *= 0.5f;

	float zoom = vpt->GetScreenScaleFactor(tm.GetTrans())*ZFACT;
	if (screenSize) {
		tm.Scale(Point3(zoom,zoom,zoom));
	}

	vpt->getGW()->setTransform(tm);

	if (!inode->IsFrozen() && !inode->Dependent() && !inode->Selected()) {
		//vpt->getGW()->setColor(LINE_COLOR, GetUIColor(COLOR_DIALOGUE_OBJ));
		vpt->getGW()->setColor( LINE_COLOR, color);
	}
/*
	if (centerMarker) {		
		vpt->getGW()->marker(&pt,X_MRKR);
	}

	if (cross) {
		// X
		pts[0] = Point3(-size, 0.0f, 0.0f); pts[1] = Point3(size, 0.0f, 0.0f);
		vpt->getGW()->polyline(2, pts, NULL, NULL, FALSE, NULL);

		// Y
		pts[0] = Point3(0.0f, -size, 0.0f); pts[1] = Point3(0.0f, size, 0.0f);
		vpt->getGW()->polyline(2, pts, NULL, NULL, FALSE, NULL);

		// Z
		pts[0] = Point3(0.0f, 0.0f, -size); pts[1] = Point3(0.0f, 0.0f, size);
		vpt->getGW()->polyline(2, pts, NULL, NULL, FALSE, NULL);
	}

	if (box) {
*/
		// Make the box half the size
		size = size * 0.5f;

		// Bottom
		pts[0] = Point3(-size, -size, -size); 
		pts[1] = Point3(-size,  size, -size);
		pts[2] = Point3( size,  size, -size);
		pts[3] = Point3( size, -size, -size);
		vpt->getGW()->polyline(4, pts, NULL, NULL, TRUE, NULL);

		// Top
		pts[0] = Point3(-size, -size,  size); 
		pts[1] = Point3(-size,  size,  size);
		pts[2] = Point3( size,  size,  size);
		pts[3] = Point3( size, -size,  size);
		vpt->getGW()->polyline(4, pts, NULL, NULL, TRUE, NULL);

		// Sides
		pts[0] = Point3(-size, -size, -size); 
		pts[1] = Point3(-size, -size,  size);
		vpt->getGW()->polyline(2, pts, NULL, NULL, FALSE, NULL);

		pts[0] = Point3(-size,  size, -size); 
		pts[1] = Point3(-size,  size,  size);
		vpt->getGW()->polyline(2, pts, NULL, NULL, FALSE, NULL);

		pts[0] = Point3( size,  size, -size); 
		pts[1] = Point3( size,  size,  size);
		vpt->getGW()->polyline(2, pts, NULL, NULL, FALSE, NULL);

		pts[0] = Point3( size, -size, -size); 
		pts[1] = Point3( size, -size,  size);
		vpt->getGW()->polyline(2, pts, NULL, NULL, FALSE, NULL);
	//}

	vpt->getGW()->setRndLimits(limits);

	return 1;
}

int DialogueHelpObject::HitTest(
								TimeValue t, INode *inode, int type, int crossing, int flags, IPoint2 *p, ViewExp *vpt) 
{
	Matrix3 tm(1);	
	HitRegion hitRegion;
	DWORD	savedLimits;
	Point3 pt(0,0,0);

	vpt->getGW()->setTransform(tm);
	GraphicsWindow *gw = vpt->getGW();	
	Material *mtl = gw->getMaterial();

	tm = inode->GetObjectTM(t);		
	MakeHitRegion(hitRegion, type, crossing, 4, p);

	gw->setRndLimits(((savedLimits = gw->getRndLimits())|GW_PICK)&~GW_ILLUM);
	gw->setHitRegion(&hitRegion);
	gw->clearHitCode();

	DrawAndHit(t, inode, vpt);

	/*
	if (showAxis) {
	DrawAxis(vpt,tm,axisLength,screenSize);
	}
	vpt->getGW()->setTransform(tm);
	vpt->getGW()->marker(&pt,X_MRKR);
	*/

	gw->setRndLimits(savedLimits);

	// CAL-08/27/03: This doesn't make sense. It shouldn't do this. (Defect #468271)
	// This will always select this helper when there's an intersection on the bounding box and the selection window.
	// TODO: There's still a problem with window selection. We need to check if it hits all components in DrawAndHit.
	/*
	if((hitRegion.type != DIALOGUE_RGN) && !hitRegion.crossing)
	return TRUE;
	*/

	return gw->checkHitCode();
}



int DialogueHelpObject::Display(
								TimeValue t, INode* inode, ViewExp *vpt, int flags) 
{
	DrawAndHit(t, inode, vpt);
	/*
	Matrix3 tm(1);
	Point3 pt(0,0,0);

	vpt->getGW()->setTransform(tm);	
	tm = inode->GetObjectTM(t);		

	if (showAxis) {
	DrawAxis(vpt,tm,axisLength,inode->Selected(),inode->IsFrozen());
	}


	vpt->getGW()->setTransform(tm);
	if(!inode->IsFrozen())
	vpt->getGW()->setColor(LINE_COLOR,GetUIColor(COLOR_DIALOGUE_OBJ));
	vpt->getGW()->marker(&pt,X_MRKR);
	*/

	return(0);
}



//
// Reference Managment:
//

// This is only called if the object MAKES references to other things.
RefResult DialogueHelpObject::NotifyRefChanged(
	Interval changeInt, RefTargetHandle hTarget, 
	PartID& partID, RefMessage message ) 
{
	switch (message) {
case REFMSG_CHANGE:
	if (editOb==this) InvalidateUI();
	break;
	}
	return(REF_SUCCEED);
}

void DialogueHelpObject::InvalidateUI()
{
	dialogueobj_param_blk.InvalidateUI(pblock2->LastNotifyParamID());
}

Interval DialogueHelpObject::ObjectValidity(TimeValue t)
{
	float size;
	int centerMarker, axisTripod, cross, box, screenSize, drawOnTop;

	Interval ivalid = FOREVER;
	pblock2->GetValue(dialogueobj_size, t,         size, ivalid);
	pblock2->GetValue(dialogueobj_centermarker, t, centerMarker, ivalid);
	pblock2->GetValue(dialogueobj_axistripod, t,   axisTripod, ivalid);
	pblock2->GetValue(dialogueobj_cross, t,        cross, ivalid);
	pblock2->GetValue(dialogueobj_box, t,          box, ivalid);
	pblock2->GetValue(dialogueobj_screensize, t,   screenSize, ivalid);
	pblock2->GetValue(dialogueobj_drawontop, t,    drawOnTop, ivalid);

	return ivalid;
}

ObjectState DialogueHelpObject::Eval(TimeValue t)
{
	return ObjectState(this);
}

RefTargetHandle DialogueHelpObject::Clone(RemapDir& remap) 
{
	DialogueHelpObject* newob = new DialogueHelpObject();	
	newob->showAxis = showAxis;
	newob->axisLength = axisLength;
	newob->ReplaceReference(0, pblock2->Clone(remap));
	BaseClone(this, newob, remap);
	return(newob);
}


void DialogueHelpObject::UpdateParamblockFromVars()
{
	SuspendAnimate();
	AnimateOff();

	pblock2->SetValue(dialogueobj_size, TimeValue(0), axisLength);
	pblock2->SetValue(dialogueobj_centermarker, TimeValue(0), TRUE);
	pblock2->SetValue(dialogueobj_axistripod, TimeValue(0), showAxis);
	pblock2->SetValue(dialogueobj_cross, TimeValue(0), FALSE);
	pblock2->SetValue(dialogueobj_box, TimeValue(0), FALSE);
	pblock2->SetValue(dialogueobj_screensize, TimeValue(0), TRUE);

	ResumeAnimate();
}


class DialogueHelperPostLoadCallback : public PostLoadCallback {
public:
	DialogueHelpObject *pobj;

	DialogueHelperPostLoadCallback(DialogueHelpObject *p) {pobj=p;}
	void proc(ILoad *iload) {
		pobj->UpdateParamblockFromVars();
	}
};

#define SHOW_AXIS_CHUNK				0x0100
#define AXIS_LENGTH_CHUNK			0x0110
#define DIALOGUE_HELPER_R4_CHUNKID		0x0120 // new version of dialogue helper for R4 (updated to use PB2)

IOResult DialogueHelpObject::Load(ILoad *iload)
{
	ULONG nb;
	IOResult res = IO_OK;
	BOOL oldVersion = TRUE;

	while (IO_OK==(res=iload->OpenChunk())) {
		switch (iload->CurChunkID()) {

case SHOW_AXIS_CHUNK:
	res = iload->Read(&showAxis,sizeof(showAxis),&nb);
	break;

case AXIS_LENGTH_CHUNK:
	res = iload->Read(&axisLength,sizeof(axisLength),&nb);
	break;

case DIALOGUE_HELPER_R4_CHUNKID:
	oldVersion = FALSE;
	break;
		}

		res = iload->CloseChunk();
		if (res!=IO_OK)  return res;
	}

	if (oldVersion) {
		iload->RegisterPostLoadCallback(new DialogueHelperPostLoadCallback(this));
	}

	return IO_OK;
}

IOResult DialogueHelpObject::Save(ISave *isave)
{
	/*
	isave->BeginChunk(SHOW_AXIS_CHUNK);
	isave->Write(&showAxis,sizeof(showAxis),&nb);
	isave->EndChunk();

	isave->BeginChunk(AXIS_LENGTH_CHUNK);
	isave->Write(&axisLength,sizeof(axisLength),&nb);
	isave->EndChunk();
	*/

	isave->BeginChunk(DIALOGUE_HELPER_R4_CHUNKID);
	isave->EndChunk();

	return IO_OK;
}


/*--------------------------------------------------------------------*/
// 
// Stole this from scene.cpp
// Probably couldn't hurt to make an API...
//
//


void Text( ViewExp *vpt, TCHAR *str, Point3 &pt )
{	
	vpt->getGW()->text( &pt, str );	
}

static void DrawAnAxis( ViewExp *vpt, Point3 axis )
{
	Point3 v1, v2, v[3];	
	v1 = axis * (float)0.9;
	if ( axis.x != 0.0 || axis.y != 0.0 ) {
		v2 = Point3( axis.y, -axis.x, axis.z ) * (float)0.1;
	} else {
		v2 = Point3( axis.x, axis.z, -axis.y ) * (float)0.1;
	}

	v[0] = Point3(0.0,0.0,0.0);
	v[1] = axis;
	vpt->getGW()->polyline( 2, v, NULL, NULL, FALSE, NULL );	
	v[0] = axis;
	v[1] = v1+v2;
	vpt->getGW()->polyline( 2, v, NULL, NULL, FALSE, NULL );
	v[0] = axis;
	v[1] = v1-v2;
	vpt->getGW()->polyline( 2, v, NULL, NULL, FALSE, NULL );
}


void DrawAxis(ViewExp *vpt, const Matrix3 &tm, float length, BOOL screenSize)
{
	Matrix3 tmn = tm;
	float zoom;

	// Get width of viewport in world units:  --DS
	zoom = vpt->GetScreenScaleFactor(tmn.GetTrans())*ZFACT;

	if (screenSize) {
		tmn.Scale( Point3(zoom,zoom,zoom) );
	}
	vpt->getGW()->setTransform( tmn );		

	Text( vpt, _T("x"), Point3(length,0.0f,0.0f) ); 
	DrawAnAxis( vpt, Point3(length,0.0f,0.0f) );	

	Text( vpt, _T("y"), Point3(0.0f,length,0.0f) ); 
	DrawAnAxis( vpt, Point3(0.0f,length,0.0f) );	

	Text( vpt, _T("z"), Point3(0.0f,0.0f,length) ); 
	DrawAnAxis( vpt, Point3(0.0f,0.0f,length) );
}
