#include "shared.hpp"
#include "types.hpp"
#include <ctype.h>

#include "../interfaces/iframework.hpp"

#include "css.hpp"

#include <fstream>

extern const IFramework* GetFramework();

namespace shared
{
	namespace css
	{
		class Class2: public std::map<std::string, std::string> {
		public:
			Class2(const char **cc);
			Class2();

			Class2 operator+ (const Class2& _class);

			inline Class2& operator+= (const Class2& _class) {
				return *this = *this + _class;
			}

			const std::list<Atoms>& atoms() const {
				return m_atoms;
			}

		private:
			std::string m_initializer;
			std::list<Atoms> m_atoms;
		};
	}
}


	struct push_back_impl
	{
		template <typename T, typename Arg>
		struct result
		{
			typedef void type;
		};

		void operator()(shared::css::Sheet& left, const shared::css::Class2& right) const {
			for (std::list<shared::css::Atoms>::const_iterator it = right.atoms().begin(); it != right.atoms().end(); it++) {
				shared::css::Class cl(*it);
				cl.insert(right.begin(), right.end());
				left.push_back(cl);
			}
		}

// switch macro
#define my_switch(type, a) while (true) { \
	typedef std::pair<type, unsigned int> type_pair; \
	typedef std::map<type, unsigned int, ignorecase_less> type_map; \
	static type_map _string_lookup_; \
	static bool _init_ = true; \
	unsigned int _val_ = 0; \
	if (!_init_) { \
	type_map::iterator _iter_ = _string_lookup_.find(a); \
	if (_iter_ != _string_lookup_.end()) { \
	_val_ = (*_iter_).second; } else { \
	_val_ = 0xffffffff; \
		} \
	} \
	switch(_val_) { \
	case 0: {

// case macro
#define my_case(a) } \
	case __LINE__: \
	if (_init_) \
	_string_lookup_.insert(type_pair(a, __LINE__)); \
		else {

// case macro
#define my_execute_case(a, b) } \
	case __LINE__: \
	if (_init_) \
	_string_lookup_.insert(type_pair(a, __LINE__)); \
		else { \
		b; \
		break;

// default case macro
#define my_defaultcase() } \
	default: \
	if (!_init_) {

// required macro for the end of the switch-case
#define my_endswitch() }} \
	if (_init_) { \
	_init_ = false; \
	}	else { \
	break; \
	} \
	}

#define pop_value(name) \
	if (values.size() > 0) \
	{ \
		sandbox[name] = values.front(); \
		values.pop_front(); \
	}
#define pop_dimenstions(name, attrib) \
	if (values.size() > 0) \
	{ \
		sandbox[std::string(name) + "-top" + attrib] = sandbox[std::string(name) + "-right" + attrib] = \
			sandbox[std::string(name) + "-bottom" + attrib] = sandbox[std::string(name) + "-left" + attrib] = values.front(); \
		values.pop_front(); \
	} \
	if (values.size() > 0) \
	{ \
		sandbox[std::string(name) + "-right" + attrib] = sandbox[std::string(name) + "-left" + attrib] = values.front(); \
		values.pop_front(); \
	} \
	pop_value(std::string(name) + "-bottom" + attrib) \
	pop_value(std::string(name) + "-left" + attrib)

#define pop_minmax(name, attrib) \
	if (values.size() > 0) \
	{ \
		sandbox[std::string(name) + "-min" + attrib] = \
			sandbox[std::string(name) + "-max" + attrib] = values.front(); \
		values.pop_front(); \
	} \
	pop_value(std::string(name) + "-max" + attrib)

		#define attr_t std::pair<std::string,std::string>

		void operator()(shared::css::Class2& left, const attr_t& right) const
		{
			shared::css::Class2 expander;
			expander[right.first] = right.second;

			while (true)
			{
				shared::css::Class2 sandbox;
				struct expand_into
				{
					shared::css::Class2& sandbox;
					expand_into(shared::css::Class2& sandbox): sandbox(sandbox) {}
					void operator()(const std::pair<std::string, std::string>& right)
					{
						std::list<std::string> values;
						boost::split(values, right.second, boost::is_space());
						my_switch(std::string, right.first);
						my_case("background")
							pop_value("background-color")
							pop_value("background-image")
							pop_value("background-repeat")
							pop_value("background-attachment")
							pop_value("background-position")
							break;
						my_case("border")
							pop_value("border-width")
							pop_value("border-style")
							pop_value("border-color")
							break;
						my_case("border-width")
							pop_dimenstions("border", "-width")
							break;
						my_case("border-style")
							pop_dimenstions("border", "-style")
							break;
						my_case("border-color")
							pop_dimenstions("border", "-color")
							break;
						my_case("border-image")
							pop_value("border-image-source")
							//pop_dimenstions("border-image-slice", "")
							pop_value("border-image-repeat")
							break;
						my_case("margin")
							pop_dimenstions("margin", "")
							break;
						my_case("padding")
							pop_dimenstions("padding", "")
							break;
						my_case("font")
							pop_value("font-family")
							pop_value("font-size")
							pop_value("font-style")
							pop_value("font-variant")
							pop_value("font-weight")
							break;
						my_case("list-style")
							pop_value("list-style-image")
							pop_value("list-style-position")
							pop_value("list-style-type")
							break;
						my_case("text-shadow")
							pop_value("text-shadow-h-shadow")
							pop_value("text-shadow-v-shadow")
							pop_value("text-shadow-color")
							break;
						my_case("text-outline")
							pop_value("text-outline-h-outline")
							pop_value("text-outline-v-outline")
							pop_value("text-outline-color")
							break;
						my_case("color")
							pop_minmax("color", "")
							break;
						my_case("highlight-color")
							pop_minmax("highlight-color", "")
							break;
						my_defaultcase()
							pop_value(right.first)
						my_endswitch();
					}
				};
				std::for_each(expander.begin(), expander.end(), expand_into(sandbox));
				if (expander == sandbox)
				{
					break;
				}
				expander = sandbox;
			}
			left += expander;
		}

#undef pop_dimenstions
#undef pop_value
#undef my_switch
#undef my_case
#undef my_execute_case
#undef my_defaultcase
#undef my_endswitch

		template <typename ContainerT>
		void operator()(ContainerT& c, typename ContainerT::value_type const& data) const
		{
			c.push_back(data);
		}
	};

hash_t BuildHash(const char *_str) {
	unsigned long hash = 5381;
	const unsigned char *str = (unsigned char *)_str;
	int c;

	while (c = *str++)
		hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

	return hash;
}

#define skip_whitespace(c) \
	switch (c) { \
		case ' ': \
		case '\n': \
		case '\r': \
		case '\t': \
			continue; \
	}

namespace shared
{
	namespace css
	{
		Sheet::Sheet(const std::string& filename): Resource(filename)
		{
			if (file_t file = GetFramework()->GetFileSystem()->LoadFile(filename)) {
				char* mem = new char[file.size + 1];
				mem[file.size] = 0;
				fread(mem, file.size, 1, file);

				for (const char *c = mem; c - mem < file.size; c++) {
					skip_whitespace(*c);
					push_back_impl()(*this, Class2(&c));
				}

				file.close();
				delete[] mem;
			}
			else 
			{
				throw std::runtime_error(std::string("Error loading stylesheet ") + filename);
			}
		}

		Atom::Atom(const char **cc)
			: m_name("*")
			, m_id("*")
		{
			enum state_t { s_name, s_id, s_class, s_state };
			state_t ss = s_name;
			for (const char *c = *cc; *c; c++, (*cc)++) {
				switch (*c) {
					case '*':
						//ss = s_name;
						//m_name = std::string();
						break;
					case '#':
						ss = s_id;
						m_id = std::string();
						break;
					case '.':
						ss = s_class;
						m_classes.push_back(std::string());
						break;
					case ':':
						ss = s_state;
						m_states.push_back(std::string());
						break;
					default:
						if (isalnum(*c) || *c == '-' || *c == '_') {
							switch (ss) {
								case s_name:
									m_name += *c;
									break;
								case s_id:
									m_id += *c;
									break;
								case s_class:
									m_classes.back() += *c;
									break;
								case s_state:
									m_states.back() += *c;
									break;
							}
						} else {
							goto done_atom;
						}
				}
			}
done_atom:
			m_classes.sort(); //for std::includes algorithm
			m_states.sort(); //for std::includes algorithm

			hash.name = BuildHash(m_name.c_str());
			hash.id = BuildHash(m_id.c_str());

			for (std::list<std::string>::const_iterator it = m_classes.begin(); it != m_classes.end(); it++) {
				hash.classes.push_back(BuildHash(it->c_str()));
			}
			for (std::list<std::string>::const_iterator it = m_states.begin(); it != m_states.end(); it++) {
				hash.states.push_back(BuildHash(it->c_str()));
			}
		}

		Class2::Class2() {
		}

		Class2::Class2(const char **cc) {
			enum state_t { s_atoms, s_inner, s_class, s_state };
			state_t ss = s_atoms;
			m_atoms.push_back(std::list<Atom>());

			for (const char *c = *cc; *c; c++, (*cc)++) {
				skip_whitespace(*c);
				switch (ss) {
				case s_atoms:
					switch (*c) {
					case ',':
						m_atoms.push_back(std::list<Atom>());
						break;
					case '{':
						ss = s_inner;
						break;
					default:
						m_atoms.back().push_front(Atom(cc));
						(*cc)--;
						c = *cc;
					}
					break;
				case s_inner:
					switch (*c) {
					case ';':
						break;
					case '}':
						return;
					default:
						{
							const char *str = c;
							for (; *c && *c != ';'; c++, (*cc)++);
							std::string arg(str, c - str);
							std::string arg_name = arg.substr(0, arg.find_first_of(':'));
							std::string arg_value = arg.substr(arg_name.size() + 1);
							arg_value = ltrim(arg_value);
							push_back_impl()(*this, attr_t(arg_name, arg_value));
						}
					}
					break;
				}
			}

		}

		Class2 Class2::operator+ (const Class2& _class) {
			Class2 result;
			result.m_atoms = atoms();
			struct append {
				Class2* result;
				append(Class2* result): result(result) {}
				void operator()(const std::pair<std::string, std::string>& value) {
					(*result)[value.first] = value.second;
				}
			};
			result.insert(begin(), end());
			std::for_each(_class.begin(), _class.end(), append(&result));
			return result;
		}

		Class Class::operator+ (const Class& _class) {
			Class result;
			result.m_atoms = atoms();
			struct append {
				Class* result;
				append(Class* result): result(result) {}
				void operator()(const std::pair<std::string, std::string>& value) {
					(*result)[value.first] = value.second;
				}
			};
			result.insert(begin(), end());
			std::for_each(_class.begin(), _class.end(), append(&result));
			return result;
		}

	}
}