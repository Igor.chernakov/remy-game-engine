#include "math.hpp"
#include <sstream>

namespace math
{
	const Box2 Box2::Identity(0,0,1,1);

	Box2::Box2(const std::string& param)
	{
		std::stringstream(param) >> *this;
	}

	Box2 Box2::Add(const Box2& r, const Vector2& v)
	{
		return Box2(MIN(v.x, r.vMin.x), MIN(v.y, r.vMin.y), MAX(v.x, r.vMax.x), MAX(v.y, r.vMax.y));
	}

	Box2 Box2::Multiply(const Box2& r, const Vector2& v)
	{
		return Box2(r.vMin * v, r.vMax * v);
	}

	Box2 Box2::Divide(const Box2& r, const Vector2& v)
	{
		return Box2(r.vMin / v, r.vMax / v);
	}

	Box2 Box2::Scale(const Box2& r, Real d)
	{
		return Box2(r.vMin * d, r.vMax * d);
	}

	Box2 Box2::Expand(const Box2& r, const Vector2& v)
	{
		return Box2(r.vMin.x - v.x, r.vMin.y - v.y, r.vMax.x + v.x, r.vMax.y + v.y);
	}

	Box2 Box2::Expand(const Box2& r, Real v)
	{
		return Expand(r, Vector2(v, v));
	}

	bool Box2::Contains(const Box2& r, const Vector2& p)
	{
		return (r.vMin.x < p.x) && (r.vMax.x > p.x) && (r.vMin.y < p.y) && (r.vMax.y > p.y);
	}
}
