#pragma once

SHEET(TerrainSheet, "TerrainArt\\Terrain.slk");
SHEET(CliffsSheet, "TerrainArt\\CliffTypes.slk");
SHEET(DoodadsSheet, "Doodads\\Doodads.slk");
SHEET(DestructablesSheet, "Units\\DestructableData.slk");
SHEET(UnitsSheet, "Units\\UnitUI.slk");