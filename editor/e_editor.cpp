#include "e_editor.hpp"
#include "e_window.hpp"

namespace editor
{
	IMPLEMENT_APP(Editor)

	bool Editor::OnInit()
	{
		Window *window = new Window(wxT("Game Editor"));
		window->Show(true);

		return true;
	}
}