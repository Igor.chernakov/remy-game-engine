#include <string.h>
#include "math.hpp"

using namespace math;

const Matrix Matrix::Identity;

Matrix::Matrix()
{
	Reset();
}

Matrix::Matrix(
			   Real m00, Real m01, Real m02, Real m03,
			   Real m10, Real m11, Real m12, Real m13,
			   Real m20, Real m21, Real m22, Real m23,
			   Real m30, Real m31, Real m32, Real m33)
{
	v[0][0] = m00; v[0][1] = m01; v[0][2] = m02; v[0][3] = m03;
	v[1][0] = m10; v[1][1] = m11; v[1][2] = m12; v[1][3] = m13;
	v[2][0] = m20; v[2][1] = m21; v[2][2] = m22; v[2][3] = m23;
	v[3][0] = m30; v[3][1] = m31; v[3][2] = m32; v[3][3] = m33;
}

Matrix::Matrix(const Real* copy)
{
	memcpy(this->m, copy, sizeof(this->m));
}

Matrix::Matrix(const Matrix& copy)
{
	memcpy(this->m, copy.m, sizeof(this->m));
}

Matrix::Matrix(const Quaternion& q)
{
	*this = RotationQuaternion(q);
}

Matrix::Matrix(const Vector3& axis_x, const Vector3& axis_y, const Vector3& axis_z, const Vector3& translation)
{
	Reset();
	SetFront(axis_x);
	SetRight(axis_y);
	SetUp(axis_z);
	SetTranslation(translation);
}

Matrix::Matrix(const AffineParts& ap)
{
	Reset();
	SetTranslation(ap.position);
	SetRotation(ap.rotation);
	SetScale(ap.scale);
}

const Matrix& Matrix::Reset()
{
	v[0][0] = 1; v[0][1] = 0; v[0][2] = 0; v[0][3] = 0;
	v[1][0] = 0; v[1][1] = 1; v[1][2] = 0; v[1][3] = 0;
	v[2][0] = 0; v[2][1] = 0; v[2][2] = 1; v[2][3] = 0;
	v[3][0] = 0; v[3][1] = 0; v[3][2] = 0; v[3][3] = 1;

	return *this;
}

Matrix Matrix::RotationYawPitchRoll(Real yaw, Real pitch, Real roll)
{
	Matrix r;

	Matrix xrot, yrot, zrot;

	xrot.m[5] = (float)cos(deg2rad(yaw));
	xrot.m[6] = (float)sin(deg2rad(yaw));
	xrot.m[9] = -(float)sin(deg2rad(yaw));
	xrot.m[10] = (float)cos(deg2rad(yaw));

	yrot.m[0] = (float)cos(deg2rad(pitch));
	yrot.m[2] = -(float)sin(deg2rad(pitch));
	yrot.m[8] = (float)sin(deg2rad(pitch));
	yrot.m[10] = (float)cos(deg2rad(pitch));

	zrot.m[0] = (float)cos(deg2rad(roll));
	zrot.m[1] = (float)sin(deg2rad(roll));
	zrot.m[4] = -(float)sin(deg2rad(roll));
	zrot.m[5] = (float)cos(deg2rad(roll));

	r = xrot * yrot * zrot;

	return r;
}

Matrix Matrix::RotationQuaternion(const Quaternion& q)
{
	Matrix r;

	Real fTx  = 2.0f*q.x;
	Real fTy  = 2.0f*q.y;
	Real fTz  = 2.0f*q.z;
	Real fTwx = fTx*q.w;
	Real fTwy = fTy*q.w;
	Real fTwz = fTz*q.w;
	Real fTxx = fTx*q.x;
	Real fTxy = fTy*q.x;
	Real fTxz = fTz*q.x;
	Real fTyy = fTy*q.y;
	Real fTyz = fTz*q.y;
	Real fTzz = fTz*q.z;

	r.v[0][0] = 1.0f-(fTyy+fTzz);
	r.v[1][0] = fTxy-fTwz;
	r.v[2][0] = fTxz+fTwy;
	r.v[0][1] = fTxy+fTwz;
	r.v[1][1] = 1.0f-(fTxx+fTzz);
	r.v[2][1] = fTyz-fTwx;
	r.v[0][2] = fTxz-fTwy;
	r.v[1][2] = fTyz+fTwx;
	r.v[2][2] = 1.0f-(fTxx+fTyy);

	return r;
}

Matrix Matrix::Scaling(Real x, Real y, Real z)
{
	Matrix m;

	m[0] = x; m[5] = y; m[10] = z;

	return m;
}

Matrix Matrix::Scaling(const Vector3& v)
{
	return Scaling(v.x, v.y, v.z);
}

Matrix Matrix::Translation(Real x, Real y, Real z)
{
	Matrix m;

	m[12] = x, m[13] = y, m[14] = z;

	return m;
}

Matrix Matrix::Translation(const Vector3& v)
{
	return Translation(v.x, v.y, v.z);
}

Matrix Matrix::Multiply(const Matrix& left, const Matrix& right)
{
	Matrix r;

	r.v[0][0] = left.v[0][0]*right.v[0][0]+left.v[1][0]*right.v[0][1]+left.v[2][0]*right.v[0][2]+left.v[3][0]*right.v[0][3];
	r.v[1][0] = left.v[0][0]*right.v[1][0]+left.v[1][0]*right.v[1][1]+left.v[2][0]*right.v[1][2]+left.v[3][0]*right.v[1][3];
	r.v[2][0] = left.v[0][0]*right.v[2][0]+left.v[1][0]*right.v[2][1]+left.v[2][0]*right.v[2][2]+left.v[3][0]*right.v[2][3];
	r.v[3][0] = left.v[0][0]*right.v[3][0]+left.v[1][0]*right.v[3][1]+left.v[2][0]*right.v[3][2]+left.v[3][0]*right.v[3][3];
	r.v[0][1] = left.v[0][1]*right.v[0][0]+left.v[1][1]*right.v[0][1]+left.v[2][1]*right.v[0][2]+left.v[3][1]*right.v[0][3];
	r.v[1][1] = left.v[0][1]*right.v[1][0]+left.v[1][1]*right.v[1][1]+left.v[2][1]*right.v[1][2]+left.v[3][1]*right.v[1][3];
	r.v[2][1] = left.v[0][1]*right.v[2][0]+left.v[1][1]*right.v[2][1]+left.v[2][1]*right.v[2][2]+left.v[3][1]*right.v[2][3];
	r.v[3][1] = left.v[0][1]*right.v[3][0]+left.v[1][1]*right.v[3][1]+left.v[2][1]*right.v[3][2]+left.v[3][1]*right.v[3][3];
	r.v[0][2] = left.v[0][2]*right.v[0][0]+left.v[1][2]*right.v[0][1]+left.v[2][2]*right.v[0][2]+left.v[3][2]*right.v[0][3];
	r.v[1][2] = left.v[0][2]*right.v[1][0]+left.v[1][2]*right.v[1][1]+left.v[2][2]*right.v[1][2]+left.v[3][2]*right.v[1][3];
	r.v[2][2] = left.v[0][2]*right.v[2][0]+left.v[1][2]*right.v[2][1]+left.v[2][2]*right.v[2][2]+left.v[3][2]*right.v[2][3];
	r.v[3][2] = left.v[0][2]*right.v[3][0]+left.v[1][2]*right.v[3][1]+left.v[2][2]*right.v[3][2]+left.v[3][2]*right.v[3][3];
	r.v[0][3] = left.v[0][3]*right.v[0][0]+left.v[1][3]*right.v[0][1]+left.v[2][3]*right.v[0][2]+left.v[3][3]*right.v[0][3];
	r.v[1][3] = left.v[0][3]*right.v[1][0]+left.v[1][3]*right.v[1][1]+left.v[2][3]*right.v[1][2]+left.v[3][3]*right.v[1][3];
	r.v[2][3] = left.v[0][3]*right.v[2][0]+left.v[1][3]*right.v[2][1]+left.v[2][3]*right.v[2][2]+left.v[3][3]*right.v[2][3];
	r.v[3][3] = left.v[0][3]*right.v[3][0]+left.v[1][3]*right.v[3][1]+left.v[2][3]*right.v[3][2]+left.v[3][3]*right.v[3][3];

	return r;
}

Matrix& Matrix::Multiply(const Matrix& m)
{
	return *this = Multiply(*this, m);
}

Matrix Matrix::operator*(const Matrix& m) const
{
	return Multiply(*this, m);
}

Matrix& Matrix::operator*=(const Matrix& m)
{
	return *this = Multiply(*this, m);
}

Matrix Matrix::Transpose(const Matrix& m)
{
	Matrix r;

    r = Matrix(
		m.v[0][0], m.v[1][0], m.v[2][0], m.v[3][0],
		m.v[0][1], m.v[1][1], m.v[2][1], m.v[3][1],
		m.v[0][2], m.v[1][2], m.v[2][2], m.v[3][2],
		m.v[0][3], m.v[1][3], m.v[2][3], m.v[3][3]
	);

	return r;
}

Matrix& Matrix::Transpose()
{
	return *this = Transpose(*this);
}

Matrix Matrix::Invert(const Matrix& m)
{
	Matrix r;

	float det, invDet;

	// 2x2 sub-determinants required to calculate 4x4 determinant
	float det2_01_01 = m.v[0][0] * m.v[1][1] - m.v[0][1] * m.v[1][0];
	float det2_01_02 = m.v[0][0] * m.v[1][2] - m.v[0][2] * m.v[1][0];
	float det2_01_03 = m.v[0][0] * m.v[1][3] - m.v[0][3] * m.v[1][0];
	float det2_01_12 = m.v[0][1] * m.v[1][2] - m.v[0][2] * m.v[1][1];
	float det2_01_13 = m.v[0][1] * m.v[1][3] - m.v[0][3] * m.v[1][1];
	float det2_01_23 = m.v[0][2] * m.v[1][3] - m.v[0][3] * m.v[1][2];

	// 3x3 sub-determinants required to calculate 4x4 determinant
	float det3_201_012 = m.v[2][0] * det2_01_12 - m.v[2][1] * det2_01_02 + m.v[2][2] * det2_01_01;
	float det3_201_013 = m.v[2][0] * det2_01_13 - m.v[2][1] * det2_01_03 + m.v[2][3] * det2_01_01;
	float det3_201_023 = m.v[2][0] * det2_01_23 - m.v[2][2] * det2_01_03 + m.v[2][3] * det2_01_02;
	float det3_201_123 = m.v[2][1] * det2_01_23 - m.v[2][2] * det2_01_13 + m.v[2][3] * det2_01_12;

	det = ( - det3_201_123 * m.v[3][0] + det3_201_023 * m.v[3][1] - det3_201_013 * m.v[3][2] + det3_201_012 * m.v[3][3] );

	if ( fabs( det ) < 1e-14 ) {
		return m;
	}

	invDet = 1.0f / det;

	// remaining 2x2 sub-determinants
	float det2_03_01 = m.v[0][0] * m.v[3][1] - m.v[0][1] * m.v[3][0];
	float det2_03_02 = m.v[0][0] * m.v[3][2] - m.v[0][2] * m.v[3][0];
	float det2_03_03 = m.v[0][0] * m.v[3][3] - m.v[0][3] * m.v[3][0];
	float det2_03_12 = m.v[0][1] * m.v[3][2] - m.v[0][2] * m.v[3][1];
	float det2_03_13 = m.v[0][1] * m.v[3][3] - m.v[0][3] * m.v[3][1];
	float det2_03_23 = m.v[0][2] * m.v[3][3] - m.v[0][3] * m.v[3][2];

	float det2_13_01 = m.v[1][0] * m.v[3][1] - m.v[1][1] * m.v[3][0];
	float det2_13_02 = m.v[1][0] * m.v[3][2] - m.v[1][2] * m.v[3][0];
	float det2_13_03 = m.v[1][0] * m.v[3][3] - m.v[1][3] * m.v[3][0];
	float det2_13_12 = m.v[1][1] * m.v[3][2] - m.v[1][2] * m.v[3][1];
	float det2_13_13 = m.v[1][1] * m.v[3][3] - m.v[1][3] * m.v[3][1];
	float det2_13_23 = m.v[1][2] * m.v[3][3] - m.v[1][3] * m.v[3][2];

	// remaining 3x3 sub-determinants
	float det3_203_012 = m.v[2][0] * det2_03_12 - m.v[2][1] * det2_03_02 + m.v[2][2] * det2_03_01;
	float det3_203_013 = m.v[2][0] * det2_03_13 - m.v[2][1] * det2_03_03 + m.v[2][3] * det2_03_01;
	float det3_203_023 = m.v[2][0] * det2_03_23 - m.v[2][2] * det2_03_03 + m.v[2][3] * det2_03_02;
	float det3_203_123 = m.v[2][1] * det2_03_23 - m.v[2][2] * det2_03_13 + m.v[2][3] * det2_03_12;

	float det3_213_012 = m.v[2][0] * det2_13_12 - m.v[2][1] * det2_13_02 + m.v[2][2] * det2_13_01;
	float det3_213_013 = m.v[2][0] * det2_13_13 - m.v[2][1] * det2_13_03 + m.v[2][3] * det2_13_01;
	float det3_213_023 = m.v[2][0] * det2_13_23 - m.v[2][2] * det2_13_03 + m.v[2][3] * det2_13_02;
	float det3_213_123 = m.v[2][1] * det2_13_23 - m.v[2][2] * det2_13_13 + m.v[2][3] * det2_13_12;

	float det3_301_012 = m.v[3][0] * det2_01_12 - m.v[3][1] * det2_01_02 + m.v[3][2] * det2_01_01;
	float det3_301_013 = m.v[3][0] * det2_01_13 - m.v[3][1] * det2_01_03 + m.v[3][3] * det2_01_01;
	float det3_301_023 = m.v[3][0] * det2_01_23 - m.v[3][2] * det2_01_03 + m.v[3][3] * det2_01_02;
	float det3_301_123 = m.v[3][1] * det2_01_23 - m.v[3][2] * det2_01_13 + m.v[3][3] * det2_01_12;

	r.v[0][0] =	- det3_213_123 * invDet;
	r.v[1][0] = + det3_213_023 * invDet;
	r.v[2][0] = - det3_213_013 * invDet;
	r.v[3][0] = + det3_213_012 * invDet;

	r.v[0][1] = + det3_203_123 * invDet;
	r.v[1][1] = - det3_203_023 * invDet;
	r.v[2][1] = + det3_203_013 * invDet;
	r.v[3][1] = - det3_203_012 * invDet;

	r.v[0][2] = + det3_301_123 * invDet;
	r.v[1][2] = - det3_301_023 * invDet;
	r.v[2][2] = + det3_301_013 * invDet;
	r.v[3][2] = - det3_301_012 * invDet;

	r.v[0][3] = - det3_201_123 * invDet;
	r.v[1][3] = + det3_201_023 * invDet;
	r.v[2][3] = - det3_201_013 * invDet;
	r.v[3][3] = + det3_201_012 * invDet;

	return r;
}

Matrix& Matrix::Invert()
{
	return *this = Invert(*this);
}

Matrix Matrix::LookAt(const Vector3& from, const Vector3& to, const Vector3& up)
{
	Matrix m;

	const Vector3 zaxis = Vector3::Normalize(from - to);
	const Vector3 xaxis = Vector3::Normalize(Vector3::Cross(up, zaxis));
	const Vector3 yaxis = Vector3::Cross(zaxis, xaxis);
	const Vector3 orign = Vector3(-Vector3::Dot(xaxis, from), -Vector3::Dot(yaxis, from), -Vector3::Dot(zaxis, from));

	m = Matrix(
		xaxis.x, yaxis.x, zaxis.x, 0,
		xaxis.y, yaxis.y, zaxis.y, 0,
		xaxis.z, yaxis.z, zaxis.z, 0,
		orign.x, orign.y, orign.z, 1);

	return m;
}

Matrix Matrix::Perspective(float fov, float aspect, float znear, float zfar)
{
	Matrix m;

	m = Matrix(
		1/tan(deg2rad(fov) / 2) / aspect, 0, 0, 0,
		0, 1/tan(deg2rad(fov) / 2), 0, 0,
		0, 0, zfar / (znear - zfar), -1,
		0, 0, znear * zfar / (znear - zfar), 0);

	return m;
}

Matrix Matrix::Ortho(float fWidth, float fHeight, float zNear, float zFar)
{
	Matrix m;

	m = Matrix(
		2 / fWidth, 0, 0, 0,
		0, 2 / fHeight, 0, 0,
		0, 0, 1 / (zFar - zNear), 0,
		0, 0, zNear / (zNear - zFar), 1);

	return m;
}

const Vector3& Matrix::GetFront() const
{
	return *((const Vector3*)&m[0]);
}

Matrix& Matrix::SetFront(Real x, Real y, Real z)
{
	m[0] = x, m[1] = y, m[2] = z;

	return *this;
}

Matrix& Matrix::SetFront(const Vector3& v)
{
	return SetFront(v.x, v.y, v.z);
}

const Vector3& Matrix::GetRight() const
{
	return *((const Vector3*)&m[4]);
}

Matrix& Matrix::SetRight(Real x, Real y, Real z)
{
	m[4] = x, m[5] = y, m[6] = z;

	return *this;
}

Matrix& Matrix::SetRight(const Vector3& v)
{
	return SetRight(v.x, v.y, v.z);
}

const Vector3& Matrix::GetUp() const
{
	return *((const Vector3*)&m[8]);
}

Matrix& Matrix::SetUp(Real x, Real y, Real z)
{
	m[8] = x, m[9] = y, m[10] = z;

	return *this;
}

Matrix& Matrix::SetUp(const Vector3& v)
{
	return SetUp(v.x, v.y, v.z);
}

const Vector3& Matrix::GetTranslation() const
{
	return *((const Vector3*)&m[12]);
}

Matrix& Matrix::SetTranslation(Real x, Real y, Real z)
{
	m[12] = x, m[13] = y, m[14] = z;

	return *this;
}

Matrix& Matrix::SetTranslation(const Vector3& v)
{
	return SetTranslation(v.x, v.y, v.z);
}

Vector3 Matrix::GetScale() const
{
	return Vector3(GetFront().Length(), GetRight().Length(), GetUp().Length());
}

Matrix& Matrix::SetScale(Real x, Real y, Real z)
{
	SetFront(GetFront() * x / GetScale().x);
	SetRight(GetRight() * y / GetScale().y);
	SetUp(GetUp() * z / GetScale().z);
	return *this;
}

Matrix& Matrix::SetScale(const Vector3& v)
{
	return SetScale(v.x, v.y, v.z);
}

Quaternion Matrix::GetRotation() const
{
	return Quaternion(*this);
}

Matrix& Matrix::SetRotation(const Quaternion& v)
{
	Matrix m = *this;
	*this = Matrix(v);
	this->SetTranslation(m.GetTranslation());
	return *this;
}

Matrix Matrix::Shadow(const Vector4 &light, const Vector4 &plane)
{
	enum { X, Y, Z, W };
	enum { A, B, C, D };

	//const Vector3 axis_z(pMatrix.m[8], pMatrix.m[9], pMatrix.m[10]);
	//const Vector3 axis_x(Vector3::Cross(-axis_z, Vector3(0,1,0)));
	//const Vector3 axis_y(Vector3::Cross(axis_z, axis_x));
	//const Matrix mFlat(axis_x, axis_y, axis_z);
	const Vector4 vLightPosition = light;// * mFlat;
	//const float vGroundPlane[4] = { axis_z.x, axis_z.y, axis_z.z, -axis_z.Dot(pMatrix.GetTranslation()) };
	//const float vGroundPlane[4] = { 0, 0, 0, -1 };
	const Vector4 vGroundPlane = plane;

	const float fDot =
		vGroundPlane[X] * vLightPosition.v[X] +
		vGroundPlane[Y] * vLightPosition.v[Y] +
		vGroundPlane[Z] * vLightPosition.v[Z] +
		vGroundPlane[W] * vLightPosition.v[W];

	float vShadowMat[4][4];

	vShadowMat[0][0] = fDot - vLightPosition[X] * vGroundPlane[X];
	vShadowMat[1][0] = 0.f - vLightPosition[X] * vGroundPlane[Y];
	vShadowMat[2][0] = 0.f - vLightPosition[X] * vGroundPlane[Z];
	vShadowMat[3][0] = 0.f - vLightPosition[X] * vGroundPlane[W];

	vShadowMat[X][1] = 0.f - vLightPosition[Y] * vGroundPlane[X];
	vShadowMat[1][1] = fDot - vLightPosition[Y] * vGroundPlane[Y];
	vShadowMat[2][1] = 0.f - vLightPosition[Y] * vGroundPlane[Z];
	vShadowMat[3][1] = 0.f - vLightPosition[Y] * vGroundPlane[W];

	vShadowMat[X][2] = 0.f - vLightPosition[Z] * vGroundPlane[X];
	vShadowMat[1][2] = 0.f - vLightPosition[Z] * vGroundPlane[Y];
	vShadowMat[2][2] = fDot - vLightPosition[Z] * vGroundPlane[Z];
	vShadowMat[3][2] = 0.f - vLightPosition[Z] * vGroundPlane[W];

	vShadowMat[X][3] = 0.f - vLightPosition[W] * vGroundPlane[X];
	vShadowMat[1][3] = 0.f - vLightPosition[W] * vGroundPlane[Y];
	vShadowMat[2][3] = 0.f - vLightPosition[W] * vGroundPlane[Z];
	vShadowMat[3][3] = fDot - vLightPosition[W] * vGroundPlane[W];

	Matrix m;

	memcpy(&m, vShadowMat, sizeof(vShadowMat));

	return m;
}

AffineParts Matrix::GetAffineParts() const
{
	AffineParts ap;
	ap.position = GetTranslation();
	ap.rotation = GetRotation();
	ap.scale = GetScale();
	return ap;
}

void Matrix::SetAffineParts(const AffineParts& ap)
{
	*this = ap;
}

