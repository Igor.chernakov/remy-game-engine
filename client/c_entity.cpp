/**
	ENTITY class is a bit messy because it is used for both WIDGETS (user interface) and GAME OBJECTS.

	See summary in ENTITY_EXPORT_FUNC
**/

#include "c_client.hpp"
#include "c_entity.hpp"
#include "c_helper.hpp"
#include "c_material.hpp"
#include "c_surface.hpp"
#include "c_controller.hpp"

#include <boost/parameter/aux_/parenthesized_type.hpp>
#include <numeric>	

#ifndef DECLARE_WIDGET
#	define ENTITY Entity
#	define ENTITY_(Class) Entity_##Class
#	define ENTITY_EXPORT_FUNC export_Entity
#else
#	define ENTITY Widget
#	define ENTITY_(Class) Widget_##Class
#	define ENTITY_EXPORT_FUNC export_Widget
	enum WidgetColorFlags {
		COLORFLAG_GETCOLOR = 1 << 0,
		COLORFLAG_STYLECOLOR = 1 << 1,
		COLORFLAG_BACKGROUNDCOLOR = 1 << 2,
		COLORFLAG_TEXTSHADOWCOLOR = 1 << 3,
	};
	extern hash_t BuildHash(const char *_str);
	hash_t darknessTag = BuildHash("darkness");
#endif

#include <boost/python/raw_function.hpp>
#include <boost/python/stl_iterator.hpp>

#ifdef TARGET_OS_IPHONE
extern bool is_phone;
#endif

extern hash_t BuildHash(const char *_str);
extern bool b_IsFirstFrame;
extern bool __hack_RenderingBackground;

#ifdef DECLARE_WIDGET 

namespace client {
	struct acceptor_t {
		virtual void apply(ENTITY::style_t* style, const std::string& value) const = 0;
	};

	template <typename T>
	struct acceptor: acceptor_t {
		typedef void (*funcT)(T* dst, const std::string& src);
		funcT func;
		size_t offset;
		acceptor(funcT func, T *var): func(func), offset((size_t)var) {}
		virtual void apply(ENTITY::style_t* style, const std::string& value) const {
			func((T*)((unsigned char*)style + offset), value);
		}
	};

	template <typename T> void set_default_value(T* dst) {}
	template <typename T> void to_value(T* dst, const std::string& src) {}
	template <> void set_default_value<ENTITY::float_t>(ENTITY::float_t* dst) { *dst = ENTITY::FLOAT_NONE; }
	template <> void set_default_value<ENTITY::fontstyle_t>(ENTITY::fontstyle_t* dst) { *dst = ENTITY::FONTSTYLE_NORMAL; }
	template <> void set_default_value<ENTITY::position_t>(ENTITY::position_t* dst) { *dst = ENTITY::POSITION_STATIC; }
	template <> void set_default_value<int>(int* dst) { *dst = 0; }
	template <> void set_default_value<color_t>(color_t* dst) { *dst = color_t(); }
	template <> void set_default_value<IOverlay::Align>(IOverlay::Align* dst) { *dst = IOverlay::ALIGN_LEFT; }
	template <> void set_default_value<IOverlay::Repeat>(IOverlay::Repeat* dst) { *dst = IOverlay::REPEATMODE_STRETCH; }
	template <> void set_default_value<IOverlay::Whitespace>(IOverlay::Whitespace* dst) { *dst = IOverlay::WHITESPACE_NORMAL; }
	template <> void set_default_value<std::string>(std::string* dst) { *dst = std::string(); }
	template <> void set_default_value<shared_ptr<const IMaterial> >(shared_ptr<const IMaterial>* dst) { *dst = null_ptr(); }
	template <> void set_default_value<shared_ptr<const ISound> >(shared_ptr<const ISound>* dst) { *dst = null_ptr(); }
	template <>	void to_value<int>(int* dst, const std::string& src) { *dst = boost::lexical_cast<int>(src); }
	template <> void to_value<color_t>(color_t* dst, const std::string& src) { *dst = color_t(src); }
	template <> void to_value<ENTITY::fontstyle_t>(ENTITY::fontstyle_t* dst, const std::string& src)
	{
		if (boost::iequals(src, "NORMAL")) *dst = ENTITY::FONTSTYLE_NORMAL; else
		if (boost::iequals(src, "ITALIC")) *dst = ENTITY::FONTSTYLE_ITALIC; else
		if (boost::iequals(src, "OBLIQUE")) *dst = ENTITY::FONTSTYLE_ITALIC;
	}
	template <> void to_value<ENTITY::float_t>(ENTITY::float_t* dst, const std::string& src)
	{
		if (boost::iequals(src, "LEFT")) *dst = ENTITY::FLOAT_LEFT; else
		if (boost::iequals(src, "RIGHT")) *dst = ENTITY::FLOAT_RIGHT; else
		if (boost::iequals(src, "NONE")) *dst = ENTITY::FLOAT_NONE;
	}
	template <> void to_value<ENTITY::position_t>(ENTITY::position_t* dst, const std::string& src)
	{
		if (boost::iequals(src, "STATIC")) *dst = ENTITY::POSITION_STATIC; else
		if (boost::iequals(src, "ABSOLUTE")) *dst = ENTITY::POSITION_ABSOLUTE; else
		if (boost::iequals(src, "FIXED")) *dst = ENTITY::POSITION_FIXED; else
		if (boost::iequals(src, "RELATIVE")) *dst = ENTITY::POSITION_RELATIVE; else
		if (boost::iequals(src, "INHERIT")) *dst = ENTITY::POSITION_INHERIT;
	}
	template <> void to_value<IOverlay::Align>(IOverlay::Align* dst, const std::string& src)
	{
		if (boost::iequals(src, "LEFT")) *dst = IOverlay::ALIGN_LEFT; else
		if (boost::iequals(src, "RIGHT")) *dst = IOverlay::ALIGN_RIGHT; else
		if (boost::iequals(src, "CENTER")) *dst = IOverlay::ALIGN_CENTER;
	}
	template <> void to_value<IOverlay::Repeat>(IOverlay::Repeat* dst, const std::string& src)
	{
		if (boost::iequals(src, "ROUND")) *dst = IOverlay::REPEATMODE_ROUND; else
		if (boost::iequals(src, "REPEAT")) *dst = IOverlay::REPEATMODE_REPEAT; else
		if (boost::iequals(src, "STRETCH")) *dst = IOverlay::REPEATMODE_STRETCH;
	}
	template <> void to_value<IOverlay::Whitespace>(IOverlay::Whitespace* dst, const std::string& src)
	{
		if (boost::iequals(src, "NORMAL")) *dst = IOverlay::WHITESPACE_NORMAL; else
		if (boost::iequals(src, "NOWRAP")) *dst = IOverlay::WHITESPACE_NOWRAP;
	}
	template <> void to_value<std::string>(std::string* dst, const std::string& src) { *dst = src; }
	template <> void to_value<shared_ptr<const IMaterial> >(shared_ptr<const IMaterial>* dst, const std::string& src)
	{
		*dst = framework->GetResources()->Load<Material>(src);
	}
	template <> void to_value<shared_ptr<const ISound> >(shared_ptr<const ISound>* dst, const std::string& src)
	{
		*dst = framework->GetSoundSystem()->LoadSound(src);
	}
	void to_inverse_int(int* dst, const std::string& src) { *dst = -boost::lexical_cast<int>(src); }
	template <typename T> acceptor_t* to_value_function(T* dst) { return new acceptor<T>(&to_value<T>, dst); }

	struct CssApplicator {
		typedef std::map<std::string, acceptor_t*, ignorecase_less> setters_t;

		std::list<acceptor_t*> to_delete;
		std::vector<std::string> locals;
		setters_t setters;
		ENTITY::style_t default_style;

		CssApplicator() {
			struct convert_field {
				std::string operator()(const std::string& field) {
					std::string name = field;
					replace(name.begin(), name.end(), '.', '-');
					return name;
				}
			};

			/**
				Names would be mapped from "border.top.width" (c++) to "border-top-width" (css).
			**/

#define LOCAL_STYLE_VALUE(What) \
	to_delete.push_back(to_value_function(&(((ENTITY::style_t*)(NULL))->What))); \
	setters[convert_field()(#What)] = to_delete.back(); \
	locals.push_back(convert_field()(#What)); \
	set_default_value(&default_style.What);

#define INHERITED_STYLE_VALUE(What) \
	to_delete.push_back(to_value_function(&(((ENTITY::style_t*)(NULL))->What))); \
	setters[convert_field()(#What)] = to_delete.back(); \
	set_default_value(&default_style.What);

			//non-inherited variables
			LOCAL_STYLE_VALUE(border.top.width);
			LOCAL_STYLE_VALUE(border.top.style);
			LOCAL_STYLE_VALUE(border.top.color);
			LOCAL_STYLE_VALUE(border.left.width);
			LOCAL_STYLE_VALUE(border.left.style);
			LOCAL_STYLE_VALUE(border.left.color);
			LOCAL_STYLE_VALUE(border.bottom.width);
			LOCAL_STYLE_VALUE(border.bottom.style);
			LOCAL_STYLE_VALUE(border.bottom.color);
			LOCAL_STYLE_VALUE(border.right.width);
			LOCAL_STYLE_VALUE(border.right.style);
			LOCAL_STYLE_VALUE(border.right.color);
			LOCAL_STYLE_VALUE(border.image.source);
			LOCAL_STYLE_VALUE(border.image.repeat);
			LOCAL_STYLE_VALUE(background.color);
			LOCAL_STYLE_VALUE(background.image);
			LOCAL_STYLE_VALUE(background.padding);
			LOCAL_STYLE_VALUE(margin.top);
			LOCAL_STYLE_VALUE(margin.left);
			LOCAL_STYLE_VALUE(margin.bottom);
			LOCAL_STYLE_VALUE(margin.right);
			LOCAL_STYLE_VALUE(padding.top);
			LOCAL_STYLE_VALUE(padding.left);
			LOCAL_STYLE_VALUE(padding.bottom);
			LOCAL_STYLE_VALUE(padding.right);
			LOCAL_STYLE_VALUE(max.width);
			LOCAL_STYLE_VALUE(min.width);
			LOCAL_STYLE_VALUE(position);
			LOCAL_STYLE_VALUE(top);
			LOCAL_STYLE_VALUE(left);
			LOCAL_STYLE_VALUE(width);
			LOCAL_STYLE_VALUE(height);
			LOCAL_STYLE_VALUE(snd.mouseup);
			LOCAL_STYLE_VALUE(snd.mousedown);
			LOCAL_STYLE_VALUE(snd.mouseenter);
			LOCAL_STYLE_VALUE(snd.mouseleave);

			//inherited variables
			INHERITED_STYLE_VALUE(color.min);
			INHERITED_STYLE_VALUE(color.max);
			INHERITED_STYLE_VALUE(highlight.color.min);
			INHERITED_STYLE_VALUE(highlight.color.max);
			INHERITED_STYLE_VALUE(direction);
			INHERITED_STYLE_VALUE(font.family);
			INHERITED_STYLE_VALUE(font.size);
			INHERITED_STYLE_VALUE(font.style);
			INHERITED_STYLE_VALUE(font.variant);
			INHERITED_STYLE_VALUE(font.weight);
			INHERITED_STYLE_VALUE(text.align);
			INHERITED_STYLE_VALUE(text.decoration);
			INHERITED_STYLE_VALUE(text.indent);
			INHERITED_STYLE_VALUE(text.transform);
			INHERITED_STYLE_VALUE(text.shadow.h.shadow);
			INHERITED_STYLE_VALUE(text.shadow.v.shadow);
			INHERITED_STYLE_VALUE(text.shadow.color);
			INHERITED_STYLE_VALUE(text.outline.h.outline);
			INHERITED_STYLE_VALUE(text.outline.v.outline);
			INHERITED_STYLE_VALUE(text.outline.color);
			INHERITED_STYLE_VALUE(letter.spacing);
			INHERITED_STYLE_VALUE(vertical.align);
			INHERITED_STYLE_VALUE(white.space);
			INHERITED_STYLE_VALUE(line.height);

#undef INHERITED_STYLE_VALUE
#undef LOCAL_STYLE_VALUE

			to_delete.push_back(to_value_function(&(((ENTITY::style_t*)(NULL))->_float)));
			setters[convert_field()("float")] = to_delete.back();
			locals.push_back("float");
			set_default_value(&default_style._float);

			to_delete.push_back(new acceptor<int>(&to_inverse_int, &(((ENTITY::style_t*)(NULL))->left)));
			setters["right"] = to_delete.back();

			to_delete.push_back(new acceptor<int>(&to_inverse_int, &(((ENTITY::style_t*)(NULL))->top)));
			setters["bottom"] = to_delete.back();

			default_style.background.color = 0;
		}
		~CssApplicator() {
			std::for_each(to_delete.begin(), to_delete.end(), deleter_t<acceptor_t>());
		}
	};

	CssApplicator g_styler;
}
#endif

namespace boost {
	template<class T>
	inline T* get_pointer(boost::shared_ptr<const T> const& p) {
		return const_cast<T*>(p.get());
	}
	namespace python {
		template<class T>
		struct pointee<boost::shared_ptr<T const> > {
			typedef T type;
		};
	}
}

unsigned int client::ENTITY::WaitForSync = 0;

struct ENTITY_(ExtraFunc) : client::ENTITY {
	//static const ENTITY& __GetLinked(const ENTITY* obj) { return *obj; }
	template <int N>
	static bool GetFlag(const ENTITY* e) { return e->GetFlags(N); }
	template <int N>
	static void SetFlag(ENTITY* e, bool value) { e->SetFlags(N, value); }
};

template <class T>
struct ENTITY_(ClassWrapper): T, boost::python::wrapper<T>, IWrapper {
#ifndef DECLARE_WIDGET
	ENTITY_(ClassWrapper)(
		const std::string& name,
		const vec3_t& position,
		const quat_t& rotation,
		const vec3_t& scale,
		const color_t& color): T(name, position, rotation, scale, color)
	{
	}

#else
	ENTITY_(ClassWrapper)(
		const std::string& name,
		const vec2_t& position,
		const vec_t& width,
		const vec_t& height,
		const vec_t& rotation,
		const std::wstring& text,
		shared_ptr<const IMaterial> image,
		const std::string& stylefilename,
		const std::string& classes) : T(name, position, width, height, rotation, vec2_t::One, color_t(), text, image)
	{
		T::LoadStyle(stylefilename);
		T::SetCustomClasses(classes);
	}

#endif
	virtual std::string GetClassName() const
	{
		return GetPythonObject()->ob_type->tp_name;
	}
	virtual shared_ptr<client::ENTITY> shared_from_this()
	{
		return boost::python::extract<shared_ptr<client::ENTITY> >(GetPythonObject());
	}
	virtual shared_ptr<const client::ENTITY> shared_from_this() const
	{
		return boost::python::extract<shared_ptr<const client::ENTITY> >(GetPythonObject());
	}
	virtual PyObject* GetPythonObject() const
	{
		return boost::python::detail::wrapper_base_::get_owner(*this);
	}
};

void ENTITY_EXPORT_FUNC()
{
	using namespace boost::python;
	using namespace client;

	typedef client::ENTITY T;

	struct functions_
	{
#ifdef DECLARE_WIDGET
		static dim_t GetSize(const T& ptr) { return dim_t(ptr.GetWidth(), ptr.GetHeight()); }
		static void SetSize(T& ptr, const dim_t& value) { 
			ptr.SetWidth(value.width); 
			ptr.SetHeight(value.height);
		}
		static void SetIUV(T& ptr, const rect_t& iuv) {
			float width = ptr.GetImage() ? ptr.GetImage()->GetWidth() : 1;
			float height = ptr.GetImage() ? ptr.GetImage()->GetHeight() : 1;
			ptr.SetUV(box2_t(iuv.left / width, iuv.top / height, iuv.right / width, iuv.bottom / height));
		}
		static rect_t GetIUV(const T& ptr) {
			int width = ptr.GetImage() ? ptr.GetImage()->GetWidth() : 1;
			int height = ptr.GetImage() ? ptr.GetImage()->GetHeight() : 1;
			return rect_t(
				ptr.GetUV().vMin.x * width,
				ptr.GetUV().vMin.y * height,
				ptr.GetUV().vMax.x * width,
				ptr.GetUV().vMax.y * height);
		}
		static void UpdateStyle(T& ptr) { ptr.UpdateStyle(true); }
#else
		static object Intersect(const T& ptr, const line_t& line) {
			const vec3_t tmp = ptr.m_hotspot;
			if (ptr.Intersect(line)) {
				const vec3_t result = ptr.m_hotspot;
				ptr.m_hotspot = tmp;
				return object(result);
			} else {
				return object();
			}
		}
#endif
		static T::Nodes::const_iterator linked_begin(const T& ptr) { return ptr.GetLinked().begin(); }
		static T::Nodes::const_iterator linked_end(const T& ptr) { return ptr.GetLinked().end(); }
		static T::const_iterator nodes_begin(const T& ptr) { return ptr.begin(); }
		static T::const_iterator nodes_end(const T& ptr) { return ptr.end(); }
		static T::Controllers::const_iterator ctrl_begin(const T& ptr) { return ptr.GetControllers().begin(); }
		static T::Controllers::const_iterator ctrl_end(const T& ptr) { return ptr.GetControllers().end(); }
		static shared_ptr<T> LinkTo(shared_ptr<T> ptr, shared_ptr<T> to) { if (to) to->Attach(ptr); return ptr; }
		static shared_ptr<T> LinkObject(shared_ptr<T> ptr, shared_ptr<T> object) { ptr->Attach(object); return ptr; }
		static shared_ptr<T> LinkController(shared_ptr<T> ptr, shared_ptr<Controller> object) { ptr->AddController(object); return ptr; }
		/*static shared_ptr<T> GetAttr(shared_ptr<T> ptr, const std::string& name) {
			if (shared_ptr<T> found = ptr->FindEntity(name)) {
				return found;
			} else {
				PyErr_Format(PyExc_AttributeError, "'%s' object has no attribute or child object '%s'.", 
					ptr->GetName().c_str(), name.c_str());
				return shared_ptr<T>();
			}
		}*/
		static void ClearControllers(T& ptr) 
		{
			for (ENTITY::Controllers::iterator it = ptr.m_controllers.begin(); it != ptr.m_controllers.end(); ++it)
			{
				(*it)->Dispose();
			}
		}

		static object Attach(tuple args, dict kwargs)
		{
			object self = args[0], entity = args[1]();

			shared_ptr<T> e;

			if (extract<shared_ptr<T> >(entity).check()) {
				e = extract<shared_ptr<T> >(entity)();
			} else if (extract<shared_ptr<Controller> >(entity).check()) {
				extract<shared_ptr<T> >(self)()->AddController(extract<shared_ptr<Controller> >(entity)());
			} else {
				framework->Log(IFramework::MSG_ERROR, "Can't create an unsupported class instance");
				return object();
			}

			struct set_attr
			{
				object self;
				object entity;
				dict kwargs;
				set_attr(object self, object entity, dict kwargs)
					: self(self)
					, entity(entity)
					, kwargs(kwargs)
				{}
				void operator()(object key) {
					if (key == "show") {
						self.attr(kwargs[key]) += entity.attr("Show");
						entity.attr("visible") = false;
					} else if (key == "hide") {
						self.attr(kwargs[key]) += entity.attr("Hide");
					} else {
						entity.attr(key) = kwargs[key];
					}
				}
			};

			std::for_each(
				stl_input_iterator<object>(kwargs.keys()),
				stl_input_iterator<object>(),
				set_attr(self, entity, kwargs));

			if (e) {
				extract<shared_ptr<T> >(self)()->Attach(e);
			}

			return entity;
		}
	};

	do {
	scope _e = class_<ENTITY_(ClassWrapper)<T>, shared_ptr<ENTITY_(ClassWrapper)<T> >, boost::noncopyable>
#ifndef DECLARE_WIDGET
	("Object", init<std::string, vec3_t, quat_t, vec3_t, color_t>((
			arg("id") = std::string("Object#"),
			arg("position") = vec3_t::Zero,
			arg("rotation") = quat_t::Identity,
			arg("scale") = vec3_t::One,
			arg("color") = color_t())))
		.def("Intersect", &functions_::Intersect)
		.def("AddTrigger", &T::AddTriggerObject)
		.add_property("position", py_cref(&T::GetPosition), &T::SetPosition)
		.add_property("rotation", py_cref(&T::GetRotation), &T::SetRotation)
		.add_property("scale", py_cref(&T::GetScale), &T::SetScale)
		.add_property("team", &T::GetTeam, &T::SetTeam)
		.def_readwrite("topmost", &T::m_TopMost)
		.def_readwrite("pivot", &T::m_pivot)
#else
	("Widget", init<std::string, vec2_t, float, float, float, std::wstring, shared_ptr<const IMaterial>, std::string, std::string>((
			arg("id") = std::string("Widget#"),
			arg("position") = vec2_t::Zero,
			arg("width") = 0.0f,
			arg("height") = 0.0f,
			arg("rotation") = 0.0f,
			arg("text") = std::wstring(),
			arg("image") = shared_ptr<const IMaterial>(),
			arg("stylefilename") = std::string(),
			arg("classes") = std::string())))
		.def("LoadStyle", &T::LoadStyle)
		.def("AddTimeHandler", &T::AddTimeHandler, (arg("sec"), arg("code")))
		.def("UpdateStyle", &functions_::UpdateStyle) // it's a hack to expose this :(
		.add_property("position", &T::GetPosition, &T::SetPosition)
		.add_property("rotation", &T::GetRotation, &T::SetRotation)
		.add_property("scale", &T::GetScale, &T::SetScale)
		.add_property("width", &T::GetWidth, &T::SetWidth)
		.add_property("height", &T::GetHeight, &T::SetHeight)
		.add_property("text_width", &T::GetTextWidth)
		.add_property("text_height", &T::GetTextHeight)
		.add_property("size", &functions_::GetSize, &functions_::SetSize)
		.add_property("iuv", &functions_::GetIUV, &functions_::SetIUV)
		.add_property("framesize", &T::GetFrameSize)
		.add_property("text", py_cref(&T::GetText), &T::SetText)
		.add_property("image", &T::GetImage, &T::SetImage)
		.add_property("time", &T::GetTime, &T::SetTime)
		.add_property("window", &T::GetWindow)
		.add_property("columns", &T::GetColumns, &T::SetColumns)
		.add_property("class", &T::GetClassName, &T::SetCustomClasses)
		.add_property("class_", &T::GetClassName, &T::SetCustomClasses)
		.add_property("classes", range(&T::custom_classes_begin, &T::custom_classes_end))
		.add_property("uv", py_cref(&T::GetUV), &T::SetUV)
		.def_readwrite("textshuffle", &T::text_shuffle)
		.def_readwrite("hack_underworld", &T::b_UnderWorld)
#endif
		.def("Show", &T::Show, (arg("name") = std::string()))
		.def("Hide", &T::Hide, (arg("name") = std::string()))
		.def("Attach", raw_function(&functions_::Attach, 2))
		.def("Attach", &T::Attach)//, (arg("node"), arg("absolute_transformation") = false))
		.def("AddEventHandler", &T::AddEventHandler, (arg("name"), arg("code"), arg("with_params") = true))
		.def("Dispose", &T::Dispose)
		.def("Undispose", &T::Undispose)
		.def("Clear", &T::Clear)
		.def("ClearControllers", &functions_::ClearControllers)
		.def("FindEntity", &T::FindEntity)
		.def("Animate", &T::Animate)
		.def("__rshift__", &T::FindEntity)
		.def("__rshift__", &functions_::LinkTo)
		.def("__lshift__", &functions_::LinkObject)
		.def("__lshift__", &functions_::LinkController)
		.def("__eq__", &T::operator==)
		.def("__ne__", &T::operator!=)
		.def("__str__", py_cref(&T::GetName))
		//.def("__getattr__", &functions_::GetAttr)
		//basic properties
		.add_property("id", py_cref(&T::GetName), &T::SetName)
		.add_property("bbox", py_cref(&T::GetBoundingBox))
		.add_property("matrix", py_cref(&T::GetMatrix), &T::SetMatrix)
		.add_property("world_color", &T::GetColor)
		.add_property("color", py_cref(&T::GetLocalColor), &T::SetLocalColor)
		.add_property("transparency", &T::GetTransparency, &T::SetTransparency)
		.add_property("childless", &T::is_childless)
		.add_property("selected", &T::IsSelected, &T::Select)
		//node-related
		.add_property("parent", &T::GetSharedParent)
		.add_property("linked", range(&functions_::linked_begin, &functions_::linked_end))
		.add_property("nodes", range(&functions_::nodes_begin, &functions_::nodes_end))
		.add_property("controllers", range(&functions_::ctrl_begin, &functions_::ctrl_end))
		//shortcuts to the flags
		.add_property("disposed", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_DISPOSED>)
		.add_property("hovered", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_HOVERED>)
		.add_property("visible", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_VISIBLE>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_VISIBLE>)
		.add_property("nopause", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOPAUSE>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOPAUSE>)
		.add_property("nohover", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOHOVER>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOHOVER>)
		.add_property("noclip", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOCLIP>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOCLIP>)
		.add_property("notime", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOTIME>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOTIME>)
		.add_property("noevents", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOEVENTS>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOEVENTS>)
		.add_property("nosignals", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOSIGNALS>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOSIGNALS>)
		.add_property("noscissor", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOSCISSOR>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOSCISSOR>)
		.add_property("nocamframe", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOCAMERAFRAME>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOCAMERAFRAME>)
		.add_property("norender", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NORENDER>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NORENDER>)
		.add_property("nosort", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOSORT>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOSORT>)
		.add_property("enabled", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_ACTIVE>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_ACTIVE>)
		.add_property("workspace", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_WORKSPACE>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_WORKSPACE>)
		.add_property("norotation", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOPRNTROT>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOPRNTROT>)
		.add_property("noposition", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOPRNTPOS>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOPRNTPOS>)
		.add_property("noscale", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOPRNTSCL>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOPRNTSCL>)
		.add_property("nocolor", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOPRNTCLR>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOPRNTCLR>)
		.add_property("noupdate", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOUPDATE>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOUPDATE>)
		.add_property("nohoversolo", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOHOVERSOLO>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOHOVERSOLO>)
		.add_property("nostyleupdate", &ENTITY_(ExtraFunc)::GetFlag<T::FLAG_NOSTYLEUPDATE>, &ENTITY_(ExtraFunc)::SetFlag<T::FLAG_NOSTYLEUPDATE>)
		//stuff
		.def_readonly("uid", &T::uid)
		.def_readwrite("updaterate", &T::m_speed)
		.def_readwrite("flags", &T::m_UserFlags)
		.def_readwrite("xdelay", &T::xdelay)
		//events
#ifndef DECLARE_WIDGET
		.def_readwrite("TriggerEnter", &T::TriggerEnter)
		.def_readwrite("TriggerLeave", &T::TriggerLeave)
#endif
		.def_readwrite("Click", &T::Click)
		.def_readwrite("DoubleClick", &T::DoubleClick)
		.def_readwrite("MouseMove", &T::MouseMove)
		.def_readwrite("MouseEnter", &T::MouseEnter)
		.def_readwrite("MouseLeave", &T::MouseLeave)
		.def_readwrite("MouseDown", &T::MouseDown)
		.def_readwrite("MouseUp", &T::MouseUp)
		.def_readwrite("GotFocus", &T::GotFocus)
		.def_readwrite("LostFocus", &T::LostFocus)
		.def_readwrite("ParentChanged", &T::ParentChanged)
		.def_readwrite("VisibleChanged", &T::VisibleChanged)
		.def_readwrite("KeyDown", &T::KeyDown)
		.def_readwrite("KeyUp", &T::KeyUp)
		.def_readwrite("Created", &T::Created)
		.def_readwrite("Disposed", &T::Disposed)
		.def_readwrite("Initializing", &T::Initializing)
	;

	register_ptr_to_python<shared_ptr<T> >();
	register_ptr_to_python<shared_ptr<const T> >();
	implicitly_convertible<shared_ptr<T>, shared_ptr<const T> >();

	DISPOSEME(4) _e;

	}
	while(0);

#ifdef DECLARE_WIDGET
	DISPOSEME(3) objects::detail::demand_iterator_class("iterator", 
		(std::list<std::string>::const_iterator*)0, objects::default_iterator_call_policies());
#endif

	DISPOSEME(3) objects::detail::demand_iterator_class("iterator", 
		(T::Nodes::const_iterator*)0, objects::default_iterator_call_policies());

	DISPOSEME(3) objects::detail::demand_iterator_class("iterator", 
		(T::const_iterator*)0, objects::default_iterator_call_policies());

	DISPOSEME(3) objects::detail::demand_iterator_class("iterator", 
		(T::Controllers::const_iterator*)0, objects::default_iterator_call_policies());

}

#include <fstream>

namespace client
{
	int ENTITY_(CounterInstance) = 0;
	int ENTITY_(Count) = 0;
	int ENTITY_(SortValue) = 0;

	void ENTITY_(ResetSortValue)() { ENTITY_(SortValue) = 0; }

	ENTITY::~ENTITY()
	{
#ifndef DECLARE_WIDGET
		//framework->Log(IFramework::MSG_LOG, "%s destroyed", m_name.c_str());
#endif
		ENTITY_(Count)--;
	}

#ifndef DECLARE_WIDGET
	ENTITY::ENTITY(
		const std::string& name,
		const vec3_t& position,
		const quat_t& rotation,
		const vec3_t& scale,
		const color_t& color)
		: uid(ENTITY_(CounterInstance)++)
		, m_flags(FLAG_ACTIVE | FLAG_VISIBLE | FLAG_NOCLIP)
		, m_transform(0)
		, m_dirty(false)
		, m_namehash(0)
		, RenderCallback(0)
		, AnimateCallback(0)
		, DebugCallback(0)
		, IntersectionChecker(0)
		, RecalculateCallback(0)
		, TeamChangedCallback(0)
		, ColorChangedCallback(0)
		, userdata(-1)
		, m_UserFlags(0)
		, m_speed(1.0f)
		, m_TopMost(false)
		, xdelay(0)
		, m_hack_initialized(false)
		, is_Background(false)
		, overrideParent(0)
	{
		SetName(name);
		SetPosition(position);
		SetRotation(rotation);
		SetScale(scale);
		SetLocalColor(color);

		ENTITY_(Count)++;

		//framework->Log(IFramework::MSG_LOG, "%s created", m_name.c_str());

	}

	void ENTITY::Render(IClientScene* scene) const {
		if (is_Background && !__hack_RenderingBackground)
			return;

		if (!CanBeSeen() || GetColor().a == 0)
			return;

		if (RenderCallback && !GetFlags(FLAG_NORENDER))
			(this->*RenderCallback)(scene);

		std::for_each(m_renderlist.begin(), m_renderlist.end(), boost::bind(&ENTITY::Render, _1, scene));
	}

	void ENTITY::Debug(IHelper* helper) const {
		if (DebugCallback) {
			(this->*DebugCallback)(helper);
		}

		for (EntityList::const_iterator it = m_debuglist.begin(); it != m_debuglist.end(); ++it) {
			if ((*it)->CanBeSeen()) {
				(*it)->Debug(helper);
			}
		}
	}
#else
	ENTITY::ENTITY(
		const std::string& name,
		const vec2_t& position,
		const vec_t& width,
		const vec_t& height,
		const vec_t& rotation,
		const vec2_t& scale,
		const color_t& color,
		const std::wstring& text,
		shared_ptr<const IMaterial> image)
		: uid(ENTITY_(CounterInstance)++)
		, m_flags(FLAG_ACTIVE | FLAG_VISIBLE)
		, m_transform(0)
		, m_dirty(false)
		, m_text_surface(0)
		, m_time(0.0f)
		, m_namehash(0)
		, userdata(-1)
		, bStyleChanged(true)
		, m_UserFlags(0)
		, m_speed(1.0f)
		, m_sortvalue(0)
		, m_columns(1)
		, image_uv(0, 0, 1, 1)
		, xdelay(0)
		, m_hack_initialized(false)
		, text_shuffle(0.0f)
		, overrideParent(0)
		, b_UnderWorld(false)
		, b_styleWasSet(false)
	{
		SetName(name);
		SetPosition(position);
		SetRotation(rotation);
		SetScale(scale);
		SetLocalColor(color);
		SetWidth(width);
		SetHeight(height);
		SetText(text);
		SetImage(image);

		ENTITY_(Count)++;
	}

	int ENTITY::GetWidth() const {
		((ENTITY*)this)->UpdateStyle();

		const int outer =
			m_style.margin.left + m_style.margin.right +
			m_style.border.left.width + m_style.border.right.width + 
			m_style.padding.left + m_style.padding.right;

		using namespace boost;

		if (m_custom.width) {
			return m_custom.width;
		} else if (m_style.width) {
			return m_style.width;
		} else {
			int width = GetParent() ? (GetParent()->GetColumnWidth() - outer) : 9999;

			if (m_image) {
				width = m_image->GetWidth() * fabs(image_uv.vMax.x - image_uv.vMin.x);
			} else if (GetText().length() && m_style.font.family.length()) {
				/*if (m_text_surface) {
					const vertex_t
						*a = m_text_surface->GetVertices(),
						*b = m_text_surface->GetVertices() + m_text_surface->GetVerticesCount();
					width = (int)*std::max_element(
						make_transform_iterator(a, bind(&vec3_t::x, bind(&vertex_t::position, _1))),
						make_transform_iterator(b, bind(&vec3_t::x, bind(&vertex_t::position, _1))));
				} else */ {
					width = GetTextWidth();
				}
			}

			return 
				std::max<int>(m_style.min.width ? m_style.min.width : 0, 
					std::min<int>(m_style.max.width ? m_style.max.width : 9999, width));
		}
	}
			

	int ENTITY::GetHeight() const {
		((ENTITY*)this)->UpdateStyle();

		using namespace boost;

		if (m_custom.height) {
			return m_custom.height;
		} else if (m_style.height) {
			return m_style.height;
		} else if (m_image) {
			return m_image->GetHeight() * fabs(image_uv.vMax.y - image_uv.vMin.y);
		} else if (m_text_surface) {
			return GetTextHeight();
		} else if (GetText().length() && m_style.font.family.length()) {
			dim_t text_size;
			mat4_t tmp_matrix;
			IOverlay::TextDimensions td;
			td.align = m_style.text.align;
			td.max_length = GetWidth();
			td.line_height = m_style.line.height;
			td.size = m_style.font.size;
			td.shuffle = text_shuffle;
			td.letter_spacing = m_style.letter.spacing;
			td.whitespace = m_style.white.space;
			ENTITY* overlay = const_cast<ENTITY*>(this);
			overlay->Overlay::String(
				GetText(),
				tmp_matrix,
				m_style.font.family,
				color_t(),
				color_t(),
				td,
				&text_size);
			return text_size.height;
		} else {
			int last_height = 0;

			for (Nodes::const_iterator it = GetLinked().begin(); it != GetLinked().end(); ) {
				int max_height = 0;
				for (int i = 0; i < GetColumns() && it != GetLinked().end(); i++, ++it) {
					int frame_height = (*it)->GetFrameHeight(false);
					max_height = max_height > frame_height ? max_height : frame_height;
				}
				last_height += max_height;
			}

			return last_height;

			/*return std::accumulate(
				make_transform_iterator(GetLinked().begin(), bind(&ENTITY::GetFrameHeight, _1, false)),
				make_transform_iterator(GetLinked().end(), bind(&ENTITY::GetFrameHeight, _1, false)), 0);*/
		}
	}

	int ENTITY::GetTextWidth() const
	{
		((ENTITY*)this)->UpdateStyle();

		//return text_size.width;

		if (GetText().length() && m_style.font.family.length()) {
			return GetLineWidth(GetText(), m_style.font.family, m_style.font.size,
				m_style.max.width ? m_style.max.width : 9999);
		} else {
			return 0;
		}
	}

	int ENTITY::GetTextHeight() const
	{
		((ENTITY*)this)->UpdateStyle();

		return text_size.height;
	}

	int ENTITY::GetFrameWidth() const
	{
		((ENTITY*)this)->UpdateStyle();

		const int outer =
			m_style.margin.left + m_style.margin.right +
			m_style.border.left.width + m_style.border.right.width + 
			m_style.padding.left + m_style.padding.right;
		return GetWidth() + outer;
	}

	int ENTITY::GetFrameHeight(bool force) const
	{
		if (IgnoreDim() && !force)
			return 0;

		if (!GetFlags(FLAG_VISIBLE) && !force)
			return 0;

		((ENTITY*)this)->UpdateStyle();

		const int outer =
			m_style.margin.top + m_style.margin.bottom +
			m_style.border.top.width + m_style.border.bottom.width + 
			m_style.padding.top + m_style.padding.bottom;
		return GetHeight() + outer;
	}

	bool ENTITY::IgnoreDim() const
	{
		((ENTITY*)this)->UpdateStyle();

		return m_style.position == POSITION_FIXED || m_style.position == POSITION_ABSOLUTE;
	}

	const std::wstring& ENTITY::GetText() const
	{
		return m_text;
	}

	void ENTITY::SetText(const std::wstring& value)
	{
		struct Callback: ICallback
		{
			Callback(ENTITY* self, const std::wstring& text, shared_ptr<IBucket> bucket)
				: self(self)
				, text(text)
				, bucket(bucket)
			{
				bucket->AddCallback(text, this);
			}
			~Callback()
			{
				bucket->RemoveCallback(this);
			}
			void OnCallback()
			{
				self->m_text = bucket->FormatString(text);
				self->OnStyleChanged();
			}
			ENTITY* self;
			std::wstring text;
			shared_ptr<IBucket> bucket;
		};

		if (value.length() > 0)
		{
			if (value[0] != L'*') {
				m_text_callback.reset(new Callback(this, value, framework->GetBucket()));
			} else {
				m_text_callback.reset(new Callback(this, 
					framework->GetLocalization()->GetString(std::string(++value.begin(), value.end())), 
					framework->GetBucket()));
			}
		}
		else
		{
			m_text_callback.reset();
			m_text = value;
			OnStyleChanged();
		}
	}
#endif

	void ENTITY::SetName(const std::string& value)
	{
		boost::algorithm::replace_first(m_name = value, "#", boost::lexical_cast<std::string>(uid));
		m_namehash = BuildHash(m_name.c_str());
	}

	void ENTITY::SetMatrix(const mat4_t& value)
	{
		SetLocalMatrix(GetParent() ? mat4_t::Invert(GetParent()->GetMatrix()) * value : value);
	}

	void ENTITY::SetLocalMatrix(const mat4_t& matrix)
	{
#ifdef DECLARE_WIDGET
		SetPosition(vec2_t(matrix.GetTranslation().x, matrix.GetTranslation().y));
		SetRotation(matrix.GetRotation().GetRoll());
#else
		SetPosition(matrix.GetTranslation());
		SetRotation(matrix.GetRotation());
#endif
	}

	void ENTITY::Attach(shared_ptr<ENTITY> element)//, bool absolute_transformation)
	{
		/*if (absolute_transformation)
		{
			const mat4_t& matrix = mat4_t::Invert(GetMatrix()) * element->GetMatrix();

			tree_with_python_objects<ENTITY>::Attach(element);

#ifdef DECLARE_WIDGET
			element->SetPosition(vec2_t(matrix.GetTranslation().x, matrix.GetTranslation().y));
			element->SetRotation(matrix.GetRotation().GetRoll());
#else
			element->SetPosition(matrix.GetTranslation());
			element->SetRotation(matrix.GetRotation());
#endif
		}
		else */
		{
			tree_with_python_objects<ENTITY>::Attach(element);
		}

#ifdef DECLARE_WIDGET
		OnStyleChanged();
#else
		for (ENTITY* e = this, *s = &*element; element->IsRenderable() && e; s = e, e = e->GetParent())
			if (std::find(e->m_renderlist.begin(), e->m_renderlist.end(), s) == e->m_renderlist.end()) {
				e->m_renderlist.push_back(s);
			} else {
				break;
			}

		for (ENTITY* e = this, *s = &*element; element->IsDebugable() && e; s = e, e = e->GetParent())
			if (std::find(e->m_debuglist.begin(), e->m_debuglist.end(), s) == e->m_debuglist.end()) {
				e->m_debuglist.push_back(s);
			} else {
				break;
			}

		for (ENTITY* e = this, *s = &*element; element->IsAnimatable() && e; s = e, e = e->GetParent())
			if (std::find(e->m_animatelist.begin(), e->m_animatelist.end(), s) == e->m_animatelist.end()) {
				e->m_animatelist.push_back(s);
			} else {
				break;
			}

		if (!element->Created.GetCallCount()) {
			element->Created(element, true);
		}
#endif
		if (GetFlags(FLAG_NOUPDATE)) element->SetFlags(FLAG_NOUPDATE, true);
		if (!GetFlags(FLAG_ACTIVE)) element->SetFlags(FLAG_ACTIVE, false);

		element->ParentChanged();
	}

	bool ENTITY::Detach()
	{
		return OnDetach() && tree_with_python_objects<ENTITY>::Detach();
	}

	bool ENTITY::OnDetach()
	{
#ifndef DECLARE_WIDGET
		for (ENTITY* e = GetParent(), *s = this; IsRenderable() && e && (s == this || !s->IsRenderable());
			e->m_renderlist.remove(s), s = e, e = e->GetParent());

		for (ENTITY* e = GetParent(), *s = this; IsDebugable() && e && (s == this || !s->IsDebugable());
			e->m_debuglist.remove(s), s = e, e = e->GetParent());

		for (ENTITY* e = GetParent(), *s = this; IsAnimatable() && e && (s == this || !s->IsAnimatable());
			e->m_animatelist.remove(s), s = e, e = e->GetParent());
#endif
		return true;
	}

	void ENTITY::Clear() 
	{
		std::for_each(GetLinked().begin(), GetLinked().end(), boost::bind(&ENTITY::SetFlags, _1, FLAG_DISPOSED, true));
	}

	void ENTITY::Recalculate(bool recursive) const
	{
		m_dirty = false;

#ifndef DECLARE_WIDGET
		if (m_transform & TF_ROTATION) {
			m_localmatrix = m_rotation;
		} else {
			m_localmatrix.Reset();
		}
		
		if (m_transform & TF_SCALE) {
			m_localmatrix.SetFront(m_localmatrix.GetFront() * m_scale.x);
			m_localmatrix.SetRight(m_localmatrix.GetRight() * m_scale.y);
			m_localmatrix.SetUp(m_localmatrix.GetUp() * m_scale.z);
		}

		if (m_transform & TF_POSITION) {
			m_localmatrix.SetTranslation(m_position);
		}

		if (overrideParent) {
			m_localmatrix = *overrideParent * m_localmatrix;
		} 

		if (GetParent()) {
			if (!GetFlags(FLAG_NOPRNTROT) && !GetFlags(FLAG_NOPRNTPOS) && !GetFlags(FLAG_NOPRNTSCL)) {
				m_worldmatrix = GetParent()->GetMatrix() * m_localmatrix;
			} else {
				mat4_t m = GetParent()->GetMatrix();
				if (GetFlags(FLAG_NOPRNTROT))
					m *= mat4_t::Invert(m.GetRotation());
				if (GetFlags(FLAG_NOPRNTPOS))
					m *= mat4_t::Invert(mat4_t::Translation(m.GetTranslation()));
				if (GetFlags(FLAG_NOPRNTSCL))
					m *= mat4_t::Invert(mat4_t::Scaling(m.GetScale()));
				m_worldmatrix = (m * m_localmatrix);
			}
		} else {
			m_worldmatrix = m_localmatrix;
		}

		if (RecalculateCallback)
		{
			(const_cast<ENTITY*>(this)->*RecalculateCallback)();
		}
#else
		ENTITY* overlay = const_cast<ENTITY*>(this);

		float _width = GetFrameWidth(), _height = GetFrameHeight(true);

		m_localmatrix = mat4_t::Translation(
			vec3_t(_width / 2, _height / 2, 0) +
			vec3_t(
				/*int*/(m_position.x + m_style.left + m_default_position.x), 
				/*int*/(m_position.y + m_style.top + m_default_position.y),
				0));

		if (m_transform & TF_ROTATION)
		{
			m_localmatrix *= m_rotation;
		}

		vec3_t scale = m_scale;
		
#ifdef TARGET_OS_IPHONE
		if (is_phone && m_hash.custom_classes.size() > 0 && *m_hash.custom_classes.begin() == darknessTag) {
			((ENTITY*)this)->m_transform |= TF_SCALE;
			scale *= vec3_t(1.25f, 1.5f, 1.25f);
		}
#endif
		
		if (m_transform & TF_SCALE)
		{
			m_localmatrix.SetFront(m_localmatrix.GetFront() * scale.x);
			m_localmatrix.SetRight(m_localmatrix.GetRight() * scale.y);
		}

		m_localmatrix *= mat4_t::Translation(
			-vec3_t(_width / 2, _height / 2, 0)
		);

		m_worldmatrix = (GetParent() && m_style.position != POSITION_FIXED) ? (
			GetParent()->GetMatrix() * 
			mat4_t::Translation(
				GetParent()->m_style.margin.left + GetParent()->m_style.border.left.width + GetParent()->m_style.padding.left,
				GetParent()->m_style.margin.top + GetParent()->m_style.border.top.width + GetParent()->m_style.padding.top, 0) *
			GetLocalMatrix()) : GetLocalMatrix();

		//now recalculate all surfaces with new matrix

		overlay->Overlay::Erase();
		overlay->m_text_surface = 0;

		int height = 0, width = GetWidth();
		
		overlay->text_size = dim_t();

		if (GetText().length() && m_style.font.family.length())
		{
			mat4_t tmp_matrix = GetMatrix() * mat4_t::Translation(
					m_style.margin.left + m_style.border.left.width + m_style.padding.left,
					m_style.margin.top + m_style.border.top.width + m_style.padding.top,
					0);

			IOverlay::TextDimensions td;
			
			td.align = m_style.text.align;
			td.max_length = width;
			td.line_height = m_style.line.height;
			td.size = m_style.font.size;
			td.shuffle = text_shuffle;
			td.letter_spacing = m_style.letter.spacing;
			td.whitespace = m_style.white.space;

			overlay->m_text_surface = overlay->Overlay::String(
				m_style.font.style == FONTSTYLE_ITALIC ? L"~i" + GetText() : GetText(),
				tmp_matrix,
				m_style.font.family,
				IOverlay::ColorPair(GetColor() * m_style.color.min, GetColor() * m_style.color.max),
				IOverlay::ColorPair(GetColor() * m_style.highlight.color.min, GetColor() * m_style.highlight.color.max),
				td,
				&overlay->text_size);

			((surface::Mesh*)overlay->m_text_surface)->userdata = COLORFLAG_GETCOLOR | COLORFLAG_STYLECOLOR;

			if (m_style.text.outline.h.outline != 0 ) {
				for (int i = 0; i < 2; ++i) {
					((surface::Mesh*)overlay->Overlay::String(
						m_style.font.style == FONTSTYLE_ITALIC ? L"~i" + GetText() : GetText(),
						tmp_matrix * mat4_t::Translation(m_style.text.outline.h.outline * (i ? -1 : 1), 0, 0),
						m_style.font.family,
						GetColor() * m_style.text.outline.color,
						GetColor() * m_style.text.shadow.color,
						td))->userdata = COLORFLAG_GETCOLOR | COLORFLAG_TEXTSHADOWCOLOR;;
				}
			}

			if (m_style.text.outline.v.outline != 0) {
				for (int i = 0; i < 2; ++i) {
					((surface::Mesh*)overlay->Overlay::String(
						m_style.font.style == FONTSTYLE_ITALIC ? L"~i" + GetText() : GetText(),
						tmp_matrix * mat4_t::Translation(0, m_style.text.outline.v.outline* (i ? -1 : 1), 0),
						m_style.font.family,
						GetColor() * m_style.text.outline.color,
						GetColor() * m_style.text.shadow.color,
						td))->userdata = COLORFLAG_GETCOLOR | COLORFLAG_TEXTSHADOWCOLOR;;
				}
			}

			if (m_style.text.shadow.h.shadow != 0 || m_style.text.shadow.v.shadow != 0) {
				((surface::Mesh*)overlay->Overlay::String(
					m_style.font.style == FONTSTYLE_ITALIC ? L"~i" + GetText() : GetText(),
					tmp_matrix * mat4_t::Translation(m_style.text.shadow.h.shadow, m_style.text.shadow.v.shadow, 0),
					m_style.font.family,
					GetColor() * m_style.text.shadow.color,
					GetColor() * m_style.text.shadow.color,
					td))->userdata = COLORFLAG_GETCOLOR | COLORFLAG_TEXTSHADOWCOLOR;;
			}

			//update width and height for text just generated

			width = GetWidth(); 
			height = GetHeight();
		}
		else
		{
			height = GetHeight();
		}

		if (m_image)
		{
			surface::Mesh* aaa = 
			((surface::Mesh*)overlay->Overlay::Quad(m_image,
				box2_t(
				m_style.margin.left + m_style.border.left.width + m_style.padding.left,
				m_style.margin.top + m_style.border.top.width + m_style.padding.top,
				m_style.margin.left + m_style.border.left.width + m_style.padding.left + width,
				m_style.margin.top + m_style.border.top.width + m_style.padding.top + height), 
				image_uv,
				GetColor(), &GetMatrix()));
			aaa->userdata = COLORFLAG_GETCOLOR;
			aaa->hack_underworld = b_UnderWorld;
		}

		if (m_style.border.image.source)
		{
			const float
				w = m_style.border.image.source->GetWidth(),
				h = m_style.border.image.source->GetHeight(),
				u[] = { 0, m_style.border.left.width / w, 1 - m_style.border.right.width / w, 1 },
				v[] = { 0, m_style.border.top.width / h, 1 - m_style.border.bottom.width / h, 1 };

			const int
				x[] = {
					m_style.margin.left,
					m_style.border.left.width,
					m_style.padding.left + width + m_style.padding.right,
					m_style.border.right.width },
				y[] = {
					m_style.margin.top,
					m_style.border.top.width,
					m_style.padding.top + height + m_style.padding.bottom,
					m_style.border.bottom.width };

			const mat4_t matrix(GetMatrix());
			const color_t color(GetColor());

			switch (m_style.border.image.repeat) {
			case IOverlay::REPEATMODE_STRETCH:
				for (int i = 0, xx = *x; i < 3; xx += x[++i]) {	
					for (int j = 0, yy = *y; j < 3; yy += y[++j]) {
						((surface::Mesh*)overlay->Overlay::Quad(m_style.border.image.source,
							box2_t(xx, yy, xx + x[i + 1], yy + y[j + 1]), 
							box2_t(u[i], v[j], u[i + 1], v[j + 1]),
							color, &matrix))->userdata = COLORFLAG_GETCOLOR | COLORFLAG_STYLECOLOR;
					}
				}
				break;
			case IOverlay::REPEATMODE_ROUND:
			case IOverlay::REPEATMODE_REPEAT:
				for (int i = 0, xx = *x; i < 3; xx += x[++i]) {	
					for (int j = 0, yy = *y; j < 3; yy += y[++j]) {
						for (float _x = 0; _x >= 0 && _x < x[i + 1]; _x += (u[i + 1] - u[i]) * w) {
							for (float _y = 0; _y >= 0 && _y < y[j + 1]; _y += (v[j + 1] - v[j]) * h) {
								float _xk = MIN(x[i + 1], _x + (u[i + 1] - u[i]) * w);
								float _yk = MIN(y[j + 1], _y + (v[j + 1] - v[j]) * h);
								float _xj = 1 - ((_x + (u[i + 1] - u[i]) * w) - _xk) / ((u[i + 1] - u[i]) * w);
								float _yj = 1 - ((_y + (v[j + 1] - v[j]) * h) - _yk) / ((v[j + 1] - v[j]) * h);
								if (m_style.border.image.repeat == IOverlay::REPEATMODE_REPEAT) {
									_xj = 1;
									_yj = 1;
								}
								((surface::Mesh*)overlay->Overlay::Quad(m_style.border.image.source,
									box2_t(xx + _x, yy + _y, xx + _xk, yy + _yk), 
									box2_t(u[i], v[j], 
										u[i] + (u[i + 1] - u[i]) * _xj,
										v[j] + (v[j + 1] - v[j]) * _yj),
									color, &matrix))->userdata = COLORFLAG_GETCOLOR | COLORFLAG_STYLECOLOR;
							}
						}
					}
				}
				break;
			}
		}

		if (m_style.background.image || m_style.background.color.GetAlpha() > math::EPS)
		{
			/* commented out - wanted to make border overlap bacgkround */
			/*
			overlay->Overlay::Quad(m_style.background.image,
				box2_t(
				m_style.margin.left + m_style.border.left.width,
				m_style.margin.top + m_style.border.top.width,
				m_style.margin.left + m_style.border.left.width + m_style.padding.left + GetWidth() + m_style.padding.right,
				m_style.margin.top + m_style.border.top.width + m_style.padding.top + GetHeight() + m_style.padding.bottom), 
				box2_t(0, 0, 1, 1),
				GetColor(), &GetMatrix());
				*/
			shared_ptr<const IMaterial> m = m_style.background.image ? m_style.background.image : shared_ptr<const IMaterial>();
			color_t c = m_style.background.image ? GetColor() : GetColor() * m_style.background.color;
			box2_t rect(
				m_style.margin.left + m_style.background.padding,
				m_style.margin.top + m_style.background.padding,
				m_style.margin.left + m_style.border.left.width + m_style.padding.left + width 
						+ m_style.padding.right + m_style.border.right.width - m_style.background.padding,
				m_style.margin.top + m_style.border.top.width + m_style.padding.top + height 
						+ m_style.padding.bottom + m_style.border.bottom.width - m_style.background.padding);
			box2_t uv;
			if (m) {
				uv = box2_t(0, 0, rect.Width() / m->GetWidth(), rect.Height() / m->GetHeight());
			}
			((surface::Mesh*)overlay->Overlay::Quad(m, rect,  uv, c, &GetMatrix()))->userdata = 
				m_style.background.image ? COLORFLAG_GETCOLOR : (COLORFLAG_GETCOLOR | COLORFLAG_STYLECOLOR);
		}

		overlay->m_surfaces.reverse();

#endif
	
		if (recursive)
		{
			std::for_each(GetLinked().begin(), GetLinked().end(), boost::bind(&ENTITY::Recalculate, _1, recursive));
		}
	}

#ifdef DECLARE_WIDGET
	void ENTITY::RecalculateColors(bool recursive)
	{
        struct r_surface
        {
			ENTITY* widget;
			r_surface(ENTITY* widget): widget(widget) {}
            inline void operator()(surface::Mesh& surface) const
            {
				color_t color;

				if (surface.userdata & COLORFLAG_GETCOLOR) color *= widget->GetColor();
				if (surface.userdata & COLORFLAG_STYLECOLOR) color *= widget->m_style.color.min;
				if (surface.userdata & COLORFLAG_TEXTSHADOWCOLOR) color *= widget->m_style.text.shadow.color;
				if (surface.userdata & COLORFLAG_BACKGROUNDCOLOR) color *= widget->m_style.background.color;

				surface.SetColor(color);
            }
        };

		std::for_each(m_surfaces.begin(), m_surfaces.end(), r_surface(this));
		
		if (recursive)
		{
			std::for_each(GetLinked().begin(), GetLinked().end(), boost::bind(&ENTITY::RecalculateColors, _1, recursive));
		}
	}
#endif

	void ENTITY::Animate(float timestep)
	{
		if (this == 0) // HACK: what the fuck??
			return;

		if (xdelay > 0) {
			xdelay -= timestep;
			return;
		}

		timestep *= m_speed;
		
#ifndef DECLARE_WIDGET
		m_hoverlist.clear();

		if (AnimateCallback && !GetFlags(FLAG_NOUPDATE)) {
			(this->*AnimateCallback)(timestep);
		}

		m_hotspot = vec3_t::Zero;

		if (IsInteractive() && IntersectionChecker && !IsUnhoverable()) {
			const line_t line = framework->GetScene()->GetCamera()->GetMouseRay();
			if (CheckIfIntersectsBoundingSphere(line)) {
				SetFlags(FLAG_HOVERED, (this->*IntersectionChecker)(line * mat4_t::Invert(GetMatrix())));
			} else {
				SetFlags(FLAG_HOVERED, false);
			}
		} else {
			SetFlags(FLAG_HOVERED, false);
		}

		if (triggers.size() > 0) {
			mat4_t invert = mat4_t::Invert(GetMatrix());
			for (Triggers::iterator it = triggers.begin(); it != triggers.end(); ++it) {
				bool inside = GetBoundingBox().Contains(it->first->GetMatrix().GetTranslation() * invert);
				if (inside && !it->second) {
					TriggerEnter(shared_from_this(), it->first);
				} else if (!inside && it->second) {
					TriggerLeave(shared_from_this(), it->first);
				}
				it->second = inside;
			}
		}

#else
		if (!Created.GetCallCount()) {
			Created(shared_from_this(), true);
		}

		UpdateStyle();

		m_sortvalue = ENTITY_(SortValue)++;

		m_hoverlist.clear();

		SetFlags(FLAG_HOVERED, IsHovered());

		struct fire_timer
		{
			PyObject* self;
			float from, to;
			fire_timer(PyObject* self, float& time, float timestep)
				: self(self)
				, from(time)
				, to(time + timestep)
			{
				time += timestep;
			}
			void operator()(const Timers::value_type& value)
			{
				using namespace boost::python;
				if (value.first >= from && value.first < to)
				{
					try
					{
						object(borrowed(self)).attr(value.second.c_str())();
					}
					catch script_error;
				}
			}
		};

		if (!GetFlags(FLAG_NOTIME))
		{
			std::for_each(m_timers.begin(), m_timers.end(), fire_timer(GetPythonObject(), m_time, timestep));
		}
#endif
		
		GetLinked().erase(
			std::remove_if(GetLinked().begin(),GetLinked().end(),
				boost::bind(&ENTITY::GetFlags, _1, FLAG_DISPOSED) && boost::bind(&ENTITY::OnDetach, _1)),
			GetLinked().end());

		m_controllers.erase(
			std::remove_if(m_controllers.begin(), m_controllers.end(),
			boost::bind(&Controller::Disposed, _1)), m_controllers.end());

#ifndef DECLARE_WIDGET
		if (!IsPaused())
#endif
		{
			Controllers controllers(m_controllers); //make a copy of controllers list

			std::for_each(controllers.begin(), controllers.end(), boost::bind(&Controller::Animate, _1, timestep));
		}
		
		std::for_each(GetLinked().begin(), GetLinked().end(), boost::bind(&ENTITY::Hack_EnsureParent, _1, this));
		
#ifdef DECLARE_WIDGET
		std::for_each(
			GetLinked().begin(),
			GetLinked().end(),
			boost::bind(&ENTITY::Animate, _1, timestep));

#else
		std::list<ENTITY*> entities(m_animatelist); //to prevent array modifications in cycle

		std::for_each(
			entities.begin(),
			entities.end(),
			boost::bind(&ENTITY::Animate, _1, timestep));

		if (!IsAnimatable()) //became unanimatable, so remove it from lists
		{
			for (ENTITY* e = GetParent(), *s = this; IsAnimatable() && e && (s == this || !s->IsAnimatable());
				e->m_animatelist.remove(s), s = e, e = e->GetParent());
		}

#endif
		ProcessHovers();

		m_hack_initialized = true;

		if (b_IsFirstFrame)
			Initializing();
	}
	
	bool ENTITY::IsUnhoverable(bool lazy) const
	{
#ifdef DECLARE_WIDGET
		if (!m_hack_initialized)
			return true;
#endif
		return (!lazy && GetFlags(FLAG_NOHOVERSOLO)) || GetFlags(FLAG_NOHOVER) || !GetFlags(FLAG_VISIBLE) || 
			(GetParent() && GetParent()->IsUnhoverable(true));
	}

	bool ENTITY::IsInteractive() const
	{
		return 
			MouseDown.GetHandlerCount() + 
			MouseUp.GetHandlerCount() + 
			Click.GetHandlerCount() +
			DoubleClick.GetHandlerCount();
	}

#ifndef DECLARE_WIDGET
	bool ENTITY::CheckIfIntersectsBoundingSphere(const line_t& line) const
	{
		const vec3_t rO = line.a, rV = vec3_t::Normalize(line.b - line.a), sO = GetMatrix().GetTranslation();
		const float sR = 
			vec3_t(
				MAX(fabs(GetBoundingBox().vMin.x), fabs(GetBoundingBox().vMax.x)), 
				MAX(fabs(GetBoundingBox().vMin.y), fabs(GetBoundingBox().vMax.y)),
				MAX(fabs(GetBoundingBox().vMin.z), fabs(GetBoundingBox().vMax.z))).Length();
		const vec3_t Q = sO - rO;

		double c = Q.Length();
		double v = vec3_t::Dot(Q, rV);
		double d = sR * sR - (c * c - v * v);

		if (d < 0.0)
			false;

		return true;
	}

	bool ENTITY::IsPaused() const
	{
		return !GetFlags(Entity::FLAG_NOPAUSE) && framework->GetScene()->IsPaused();
	}
#endif

	bool ENTITY::AddController(shared_ptr<Controller> controller)
	{
#ifndef DECLARE_WIDGET
		if (!IsAnimatable() && GetParent())
		{
			if (!controller->OnAttach(this))
			{
				return false;
			}

			m_controllers.push_back(controller);

			for (ENTITY* e = GetParent(), *s = this; e; s = e, e = e->GetParent())
				if (std::find(e->m_animatelist.begin(), e->m_animatelist.end(), s) == e->m_animatelist.end())
					e->m_animatelist.push_back(s);
				else
					break;
		}
		else
#endif
		{
			if (!controller->OnAttach(this))
			{
				return false;
			}

			m_controllers.push_back(controller);
		}

		return true;
	}

	void ENTITY::Select(bool value)
	{
		if (!value) {
			std::for_each(GetLinked().begin(), GetLinked().end(), boost::bind(&ENTITY::Select, _1, value));
		} else {
			GetRoot()->Select(false);
		}

		if (GetFlags(FLAG_SELECTED) != value) {
#ifdef DECLARE_WIDGET
			if (value) {
				AddPseudoClass(PCLASS_FOCUS);
			} else {
				RemovePseudoClass(PCLASS_FOCUS);
			}
#endif
			value ? GotFocus(shared_from_this(), true) : LostFocus(shared_from_this(), false);

			SetFlags(FLAG_SELECTED, value);
		}
	}

#ifndef DECLARE_WIDGET
	bool ENTITY::CanBeSeen() const
	{
		return GetFlags(FLAG_VISIBLE);// && !GetFlags(FLAG_DISPOSED);
	}

#else
	void ENTITY::Render(IClientScene* scene) const
	{
		if (!GetFlags(FLAG_VISIBLE) || xdelay > 0 || GetColor().a == 0)
			return;

		//Overlay::Render(renderer);

		struct render_surface {
			int flags;
			IClientScene* scene;
			render_surface(IClientScene* scene, int flags)
				: scene(scene)
				, flags(flags) 
			{}
			inline void operator()(const surface::Mesh& surface) const {
				((surface::Mesh*)&surface)->flags = flags;
				scene->PushSurface(&surface); 
			}
		};

		if (!GetFlags(FLAG_NORENDER))
			std::for_each(m_surfaces.begin(), m_surfaces.end(), render_surface(scene, m_flags));

		std::for_each(GetLinked().begin(), GetLinked().end(), boost::bind(&ENTITY::Render, _1, scene));
	}

	bool ENTITY::IsHovered() const
	{
		if (!framework->GetScene())
			return false;
		
		((ENTITY*)this)->UpdateStyle();

		vec2_t cursor = framework->GetScene()->GetCursorPos();

		if (GetFlags(FLAG_NOCAMERAFRAME))
		{
			cursor.x += framework->GetScene()->GetCamera()->GetOffset().x;
			cursor.y += framework->GetScene()->GetCamera()->GetOffset().y;
		}

		vec3_t mouse(cursor.x, cursor.y, 0);

		if ((m_transform & TF_ROTATION) || (m_transform & TF_SCALE))
		{
			mouse *= mat4_t::Invert(GetMatrix());
		}
		else
		{
			mouse -= vec3_t(GetMatrix().GetTranslation().x, GetMatrix().GetTranslation().y, 0);
		};

		return GetParent() && !IsUnhoverable() && 
			box2_t(
					m_style.margin.left,
					m_style.margin.top,
					m_style.margin.left + m_style.border.left.width + m_style.padding.left + GetWidth() + 
						m_style.padding.right + m_style.border.right.width,
					m_style.margin.top + m_style.border.top.width + m_style.padding.top + GetHeight() + 
						m_style.padding.bottom + m_style.border.bottom.width)
				.Contains(vec2_t(mouse.x, mouse.y));
	}

#endif

	void ENTITY::ProcessHovers()
	{
		if (m_hoverlist.front() != m_hoverlist.cache())
		{
			if (m_hoverlist.cache())
			{
				if (!GetParent())
				{
					MouseLeave(m_hoverlist.cache(), 0);

					if (!GetFlags(FLAG_NOSIGNALS))
					{
						m_hoverlist.cache()->MouseLeave(m_hoverlist.cache(), 0);
					}
				}
				else
				{
					m_hoverlist.cache()->MouseLeave(m_hoverlist.cache(), 0);
				}
#ifdef DECLARE_WIDGET
				m_hoverlist.cache()->RemovePseudoClass(PCLASS_HOVER);
				m_hoverlist.cache()->RemovePseudoClass(PCLASS_PRESSED);
#endif

				if (m_hoverlist.cache()->IsInteractive())
				{
					framework->GetWindow()->SetCursor(IWindow::CURSOR_ARROW);
				}
			} 
			if (m_hoverlist.front())
			{
				if (!GetParent())
				{
					MouseEnter(m_hoverlist.front(), 0);

					if (!GetFlags(FLAG_NOSIGNALS))
					{
						m_hoverlist.front()->MouseEnter(m_hoverlist.front(), 0);
					}
				}
				else
				{
					m_hoverlist.front()->MouseEnter(m_hoverlist.front(), 0);
				}
#ifdef DECLARE_WIDGET
				if (m_hoverlist.front()->IsInteractive())
				{
					m_hoverlist.front()->AddPseudoClass(PCLASS_HOVER);
				}
#endif
				if (m_hoverlist.front()->IsInteractive())
				{
					framework->GetWindow()->SetCursor(IWindow::CURSOR_HAND);
				}
			}
		}
		m_hoverlist.flush();
	}

	bool ENTITY::Predicate(shared_ptr<const ENTITY> left, shared_ptr<const ENTITY> right)
	{
#ifndef DECLARE_WIDGET
		vec3_t 
			left_ = left->m_hotspot != vec3_t::Zero ? left->m_hotspot * left->GetMatrix() : left->GetMatrix().GetTranslation(),
			right_ = right->m_hotspot != vec3_t::Zero ? right->m_hotspot * right->GetMatrix() : right->GetMatrix().GetTranslation(),
			camera = framework->GetScene()->GetCamera()->GetEye();

		return ((left_ - camera).Length() < (right_ - camera).Length() && (!right->m_TopMost || left->m_TopMost)) || 
			(left->m_TopMost && !right->m_TopMost);
#else
		return left->m_sortvalue > right->m_sortvalue;
#endif
	}

	void ENTITY::SetFlags(int flags, bool set)
	{
		//if (GetFlags(flags) == set)
		//	return;

		bool visible_changed = (flags & FLAG_VISIBLE) && (set ^ GetFlags(FLAG_VISIBLE));

#ifdef DECLARE_WIDGET
		if ((flags & FLAG_ACTIVE) && set != GetFlags(FLAG_ACTIVE))
		{
			if (!set) {
				AddPseudoClass(PCLASS_DISABLED);
			} else {
				RemovePseudoClass(PCLASS_DISABLED);
			}
		}
#endif

		if ((flags & FLAG_DISPOSED) && set && !GetFlags(FLAG_DISPOSED))
		{
			Disposed(shared_from_this(), true);
		}

		if (flags & FLAG_HOVERED)
		{
			if (set && GetFlags(FLAG_ACTIVE))
			{
				GetRoot()->m_hoverlist.push(shared_from_this(), ENTITY::Predicate);
				this->m_flags |= flags;
			}
			else
			{
				if (this->m_flags & FLAG_HOVERED)
				{
					GetRoot()->m_hoverlist.pop(shared_from_this());
					this->m_flags &= ~flags;
				}
			}
		} else {
			this->m_flags = set ? (this->m_flags | flags) : (this->m_flags & ~flags);
		}

		if (flags & (FLAG_NOSCISSOR | FLAG_NOUPDATE | FLAG_ACTIVE)) {
			std::for_each(GetLinked().begin(), GetLinked().end(), boost::bind(&ENTITY::SetFlags, _1, flags, set));
		}

		if (visible_changed)
			VisibleChanged(shared_from_this(), set);
	}

	bool ENTITY::GetFlags(int flags) const
	{
		return (m_flags & flags) == flags;
	};

	color_t ENTITY::GetColor() const
	{
		return (!GetFlags(FLAG_NOPRNTCLR) && GetParent()) ? (GetLocalColor() * GetParent()->GetColor()) : GetLocalColor();
	}

	void ENTITY::SetDirty(int flags) const
	{
		if (!m_dirty)
		{
			std::for_each(GetLinked().begin(), GetLinked().end(), boost::bind(&ENTITY::SetDirty, _1, flags));
			m_dirty = true;
		}

		if (flags & TF_POSITION)
		{
			m_transform = (m_position != vec3_t::Zero) ? (m_transform | TF_POSITION) : (m_transform & ~TF_POSITION);
		}

		if (flags & TF_ROTATION)
		{
			m_transform = (m_rotation != quat_t::Identity) ? (m_transform | TF_ROTATION) : (m_transform & ~TF_ROTATION);
		}

		if (flags & TF_SCALE)
		{
			m_transform = (m_scale != vec3_t::One) ? (m_transform | TF_SCALE) : (m_transform & ~TF_SCALE);
		}
	}

	shared_ptr<ENTITY> ENTITY::FindEntity(const std::string& name) const
	{
		return FindEntityHash(BuildHash(name.c_str()));
	}

	shared_ptr<ENTITY> ENTITY::FindEntityHash(int _hash) const
	{
		const_iterator e = std::find_if(begin(), end(), boost::bind(&ENTITY::NameMatchesHash, _1, _hash));
		return e != end() ? *e : null_ptr();
	}

	const mat4_t& ENTITY::GetMatrix() const
	{
		RecalculateIfDirty();

		return m_worldmatrix;
	}

	const mat4_t& ENTITY::GetLocalMatrix() const
	{
		RecalculateIfDirty();

		return m_localmatrix;
	} 

	void ENTITY::SetParent(ENTITY* parent)
	{
		tree_with_python_objects<ENTITY>::SetParent(parent);
		SetDirty();
	}

	bool ENTITY::RecalculateIfDirty() const
	{
		if (m_dirty || overrideParent)
		{
			if (GetParent())
			{
				GetParent()->RecalculateIfDirty();
			}
			Recalculate();
			return true;
		}
		else
		{
			return false;
		}
	}

	bool ENTITY::AddEventHandler(const std::string& name, const std::string& script, bool with_params)
	{
		if (!GetPythonObject())
		{
			return false;
		}

		using namespace boost::python;

		object self = object(borrowed<>(GetPythonObject()));

		object
			classobject = self.attr("__class__"),
			locals = self.attr("__dict__");

		dict globals;

		globals["__builtins__"] = import("__builtin__");
		globals["core"] = import("core");

		std::string code, file_name = name + " event handler of " + GetName() + " (class " + GetClassName() + ")";

		bool 
			is_event = PyObject_HasAttrString(self.ptr(), name.c_str()) && 
				(strcmp(object(self.attr(name.c_str())).ptr()->ob_type->tp_name, "Event") == 0),
			is_defined = !script.empty();

		PyErr_Clear();
		
	
		if (is_defined)
		{
			code = "def " + name + ((with_params && is_event) ? "_Callee(self, sender, e):\n" : "_Callee(self, **kwargs):\n") + script;
			boost::algorithm::replace_all(code, "\r", "");

			code = rtrim(code);
		}

		try 
		{
			if (!is_event)
			{
				setattr(self, name.c_str(), Event());
			}

			if (is_defined)
			{
				object pyc(handle<>(Py_CompileString(code.c_str(), file_name.c_str(), Py_single_input)));
				object eval(handle<>(PyEval_EvalCode((PyCodeObject*)pyc.ptr(), globals.ptr(), locals.ptr())));
				object function = self.attr((name + "_Callee").c_str());
				object method = object(handle<>(PyMethod_New(function.ptr(), self.ptr(), classobject.ptr())));

				self.attr(name.c_str()) += method;
			}
		}
		catch script_error;

		return true;
	}

	bool ENTITY::RemoveEventHandler(const std::string& name)
	{
		if (!GetPythonObject())
		{
			return false;
		}

		using namespace boost::python;

		object self = object(borrowed<>(GetPythonObject()));

		if (PyObject_HasAttrString(self.ptr(), name.c_str()) && 
			(strcmp(object(self.attr(name.c_str())).ptr()->ob_type->tp_name, "Event") == 0))
		{
			self.attr(name.c_str()) -= self.attr(("On" + name).c_str());

			return true;
		}
		else
		{
			return false;
		}
	}

	bool ENTITY::Show(const std::string& entity_name)
	{
		if (entity_name.empty())
		{
			SetFlags(FLAG_VISIBLE, true);
			return true;
		}
		else
		{
			if (shared_ptr<ENTITY> e = FindEntity(entity_name))
			{
				e->SetFlags(FLAG_VISIBLE, true);
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	bool ENTITY::Hide(const std::string& entity_name)
	{
		if (entity_name.empty())
		{
			SetFlags(FLAG_VISIBLE, false);
			return true;
		}
		else
		{
			if (shared_ptr<ENTITY> e = FindEntity(entity_name))
			{
				e->SetFlags(FLAG_VISIBLE, false);
				return true;
			}
			else
			{
				return false;
			}
		}
	}


#ifdef DECLARE_WIDGET

	bool ENTITY::AddTimeHandler(float sec, const std::string& script)
	{
		std::string name = "Time_msec" + boost::lexical_cast<std::string>(int(sec * 1000));

		if (sec >= 0 && AddEventHandler(name, script))
		{
			RemoveTimeHandler(sec);
			m_timers.push_back(Timers::value_type(sec, name));
			return true;
		}
		else
		{
			return false;
		}
	}

	void ENTITY::SetLocalColor(const color_t& value)
	{
		if (value == m_color)
		{
			return;
		}

		m_color = value;

		SetDirty(TF_COLOR);
	}

	bool ENTITY::RemoveTimeHandler(float sec)
	{
		return false;
	}

	template <class T1, class T2>
	bool contains(const T1& c1, const T2& c2) {
		for (typename T2::const_iterator it = c2.begin(); it != c2.end(); ++it) {
			if (std::find(c1.begin(), c1.end(), *it) == c1.end()) {
				return false;
			}
		}
		return true;
	}

	void ENTITY::OnStyleChanged() {
		bStyleChanged = true;
	}

	/**
		UpdateStyle is called in Animate function if style of the WIDGET is likely to be changed (i.e. WIDGET was HOVERED)
		and handles the visual part by applying Qt-like CSS scheme to it.
	**/

	void ENTITY::UpdateStyle(bool force) {
		if (!bStyleChanged && !force)
			return;

		if (GetFlags(FLAG_NOSTYLEUPDATE) && b_styleWasSet)
			return;

		//static int UnpdateStyleCalls = 0;
		//UnpdateStyleCalls++;
		//framework->Log(IFramework::MSG_LOG, "UpdateStyle %d %s", UnpdateStyleCalls, m_name.c_str());

		g_NotifyLoading();

		bStyleChanged = false;
		b_styleWasSet = true;

		if (GetParent()) {
			GetParent()->UpdateStyle(false);
			m_cached_style = GetParent()->m_cached_style;
		} else {
			m_cached_style = shared::css::Class();
		}

		typedef std::list<hash_t> autos_t;
		autos_t autos;

		struct accept {
			autos_t* autos;
			style_t* style;
			accept(autos_t* autos, style_t* style) : autos(autos), style(style)  {}
			void operator()(const std::pair<std::string, std::string>& it) const {
				try {
					CssApplicator::setters_t::const_iterator found = g_styler.setters.find(it.first);
					if (found != g_styler.setters.end()) {
						if (boost::iequals(it.second, "auto")) {
							autos->push_back(BuildHash(it.first.c_str()));
						} else {
							found->second->apply(style, it.second);
						}
					}
				} catch (const std::exception& e) {
					framework->Log(IFramework::MSG_ERROR, e.what());
				}
			}
		};

		struct collect {
			const ENTITY* entity;
			shared::css::Class* style;
			collect(const ENTITY* entity, shared::css::Class* style): entity(entity), style(style) {}
			void operator()(const shared::css::Class& class_) const {
				shared::css::Atoms::const_iterator atom = class_.atoms().begin();
				for (const ENTITY* e = entity; e && (atom != class_.atoms().end()); ++atom, e = e ? e->GetParent() : e) {
					for (; e; e = e->GetParent()) {
						if (atom->name() == "*" || e->GetClassName() == atom->name()) {
							if (atom->id() == "*" || e->GetNameHash() == atom->hash.id) {
								if (contains(e->m_hash.custom_classes, atom->hash.classes)) {
									if (contains(e->m_hash.pseudo_classes, atom->hash.states)) {
										goto check_next_atom;
									}
								}
							}
						}
						if (e == entity)
							return;
					}
					return; 
check_next_atom:
					continue;
				}
				*style += class_;
			}
		};

		m_style = g_styler.default_style;
		m_default_position = vec2_t();
		
		for (std::vector<std::string>::const_iterator it = g_styler.locals.begin();
			it != g_styler.locals.end(); ++it) {
			m_cached_style.erase(*it);
		}

		const hash_t 
			margin_left = BuildHash("margin-left"),
			margin_right = BuildHash("margin-right"),
			margin_top = BuildHash("margin-top"),
			margin_bottom = BuildHash("margin-bottom");

		if (GetStyle()) {
			std::for_each(GetStyle()->begin(), GetStyle()->end(), collect(this, &m_cached_style));
			std::for_each(m_cached_style.begin(), m_cached_style.end(), accept(&autos, &m_style));

			if (std::find(autos.begin(), autos.end(), margin_left) != autos.end()) {
				if (std::find(autos.begin(), autos.end(), margin_right) != autos.end())
					m_style.margin.left = m_style.margin.right = (GetParent()->GetColumnWidth() - GetFrameWidth()) / 2;
				else
					m_style.margin.left = GetParent()->GetColumnWidth() - GetFrameWidth();
			} else if (std::find(autos.begin(), autos.end(), margin_right) != autos.end()) {
				m_style.margin.right = GetParent()->GetColumnWidth() - GetFrameWidth();
			}

			if (std::find(autos.begin(), autos.end(), margin_top) != autos.end()) {
				if (std::find(autos.begin(), autos.end(), margin_bottom) != autos.end()) {
					m_style.margin.top = m_style.margin.bottom = (GetParent()->GetHeight() - GetFrameHeight(true)) / 2;
				} else {
					m_style.margin.top = GetParent()->GetHeight() - GetFrameHeight(true);
				}
			} else if (std::find(autos.begin(), autos.end(), margin_bottom) != autos.end()) {
				m_style.margin.bottom = GetParent()->GetHeight() - GetFrameHeight(true);
			}
		}

		if (GetParent() && m_style.position != POSITION_FIXED && m_style.position != POSITION_ABSOLUTE) {
			SetDirty(0);

			int last_height = 0;

			for (Nodes::const_iterator it = GetParent()->GetLinked().begin(); it != GetParent()->GetLinked().end(); ) {
				int max_height = 0;
				for (int i = 0; i < GetParent()->GetColumns() && it != GetParent()->GetLinked().end(); i++, ++it) {
					if (it->get() == this) {
						m_default_position.x = GetParent()->GetColumnWidth() * i;
						m_default_position.y = last_height;
						goto found_position;
					}
					int frame_height = (*it)->GetFrameHeight(false);
					max_height = max_height > frame_height ? max_height : frame_height;
				}
				last_height += max_height;
			}

			/*m_default_position.y = std::accumulate(GetParent()->GetLinked().begin()
				boost::make_transform_iterator(GetParent()->GetLinked().begin(), boost::bind(&ENTITY::GetFrameHeight, _1, false)),
				boost::make_transform_iterator(
					std::find(GetParent()->GetLinked().begin(), GetParent()->GetLinked().end(), shared_from_this()),
					boost::bind(&ENTITY::GetFrameHeight, _1, false)), 0);*/
		}

found_position:
		std::for_each(GetLinked().begin(), GetLinked().end(), boost::bind(&ENTITY::UpdateStyle, _1, true));

		Recalculate(false);
	}

	void ENTITY::SetCustomClasses(const std::string& cc) {
		m_custom_classes.clear();
		m_hash.custom_classes.clear();
		boost::split(m_custom_classes, cc, boost::is_space());
		for (std::list<std::string>::const_iterator it = m_custom_classes.begin(); it != m_custom_classes.end(); it++) {
			m_hash.custom_classes.push_back(BuildHash(it->c_str()));
		}
		OnStyleChanged();
	}

	void ENTITY::AddPseudoClass(const std::string& value) {
		if (std::find_if(m_pseudo_classes.begin(), m_pseudo_classes.end(),
			boost::bind(&boost::iequals<std::string, std::string>, _1, value, std::locale())) == m_pseudo_classes.end()) {
			if (value == "click" && m_style.snd.mousedown)
				m_style.snd.mousedown->Play();
			else if (value == "hover" && m_style.snd.mouseenter)
				m_style.snd.mouseenter->Play();
			m_pseudo_classes.push_back(value);
			m_hash.pseudo_classes.push_back(BuildHash(value.c_str()));
			OnStyleChanged();
			SetDirty();
		}
	}

	void ENTITY::RemovePseudoClass(const std::string& value) {
		std::list<std::string>::iterator it = std::find_if(m_pseudo_classes.begin(), m_pseudo_classes.end(),
			boost::bind(&boost::iequals<std::string, std::string>, _1, value, std::locale()));
		if (it != m_pseudo_classes.end()) {
			if (value == "click" && m_style.snd.mouseup)
				m_style.snd.mouseup->Play();
			else if (value == "hover" && m_style.snd.mouseleave)
				m_style.snd.mouseleave->Play();
			m_pseudo_classes.erase(it);

			m_hash.pseudo_classes.clear();
			for (std::list<std::string>::const_iterator it = m_pseudo_classes.begin(); it != m_pseudo_classes.end(); it++) {
				m_hash.pseudo_classes.push_back(BuildHash(it->c_str()));
			}

			OnStyleChanged();
			SetDirty();
		}
	}
	void ENTITY::LoadStyle(const std::string& filename)
	{
		if (filename.length() > 0)
		{
			m_stylesheet = framework->GetResources()->Load<shared::css::Sheet>(filename);
			OnStyleChanged();
			SetDirty();
		}
	}

	bool ENTITY::HasClass(const std::string& name) const
	{
		return std::find(m_custom_classes.begin(), m_custom_classes.end(), name) != m_custom_classes.end();
	}
#else
	void ENTITY::CallColorChangedCallback()
	{
		if (ColorChangedCallback) {
			(this->*ColorChangedCallback)();
		}

		std::for_each(GetLinked().begin(), GetLinked().end(), boost::bind(&ENTITY::CallColorChangedCallback, _1));
	}

	bool ENTITY::Intersect(const line_t& line_) const
	{
		if (this->IntersectionChecker) {
			const mat4_t matrix = mat4_t::Invert(GetMatrix());
			return (this->*IntersectionChecker)(line_t(line_.a * matrix, line_.b * matrix));
		} else {
			return false;
		}
	}

#endif

}

#undef ENTITY
#undef ENTITY_
#undef ENTITY_EXPORT_FUNC
