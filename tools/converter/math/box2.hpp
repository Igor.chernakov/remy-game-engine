#pragma once

#include "vector.hpp"
#include "vector2.hpp"
#include "macros.hpp"

namespace math
{
	struct Box2
	{
		static const int Dimensions = 4;
		
		Vector2 vMin, vMax;

		static const Box2 Identity;

		Box2() {  }
		Box2(const Vector2 &vMin, const Vector2 &vMax) : vMin(vMin), vMax(vMax) {};
		Box2(const Real *v) : vMin(v[0], v[1]), vMax(v[2], v[3]) {};
		Box2(Real v1, Real v2, Real v3, Real v4) : vMin(v1, v2), vMax(v3, v4) {};
		Box2(const std::string&);
		Box2(const Box2& other) { vMin = other.vMin; vMax = other.vMax; }

		DEFINE_DEFAULT_PAIR_OPS(Box2, Vector2);

		inline static Box2 Lerp(Real d, const Box2& left, const Box2& right)
		{
			return Box2(Vector2::Lerp(d, left.vMin, right.vMin), Vector2::Lerp(d, left.vMax, right.vMax));
		}

		void Reset()
		{
			vMin.Maximize();
			vMax.Minimize();
		}

		Real Width() const
		{
			return vMax.x - vMin.x;
		}

		Real Height() const
		{
			return vMax.y - vMin.y;
		}
	};
};
