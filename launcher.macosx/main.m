//
//  main.m
//  engine
//
//  Created by Igor Chernakov on 28/04/2013.
//  Copyright (c) 2013 Igor Chernakov. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
