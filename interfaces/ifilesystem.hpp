#pragma once

#include <string>
#include <list>
#include <boost/shared_ptr.hpp>

#include "iobject.hpp"
#include "../shared/types.hpp"

struct IFileSystem: IObject {
	enum FileMode {
		Textual,
		Binary,
	};

	virtual void AddPath(const std::string& path) = 0;
	virtual bool CheckFile(const std::string& filename) const = 0;
	virtual file_t LoadFile(const std::string& path) const = 0;
};
