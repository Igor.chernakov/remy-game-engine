#include "c_client.hpp"
#include "c_surface.hpp"

namespace client
{
	Surface Surface::CreateQuad(vertex_t* v, const box2_t& quad, const box2_t& uv)
	{
		v[0].position = vec3_t(quad.vMin.x, quad.vMin.y, 0);
		v[0].uv = vec2_t(uv.vMin.x, uv.vMin.y);
		v[0].color = -1;

		v[1].position = vec3_t(quad.vMax.x, quad.vMin.y, 0);
		v[1].uv = vec2_t(uv.vMax.x, uv.vMin.y);
		v[1].color = -1;

		v[2].position = vec3_t(quad.vMax.x, quad.vMax.y, 0);
		v[2].uv = vec2_t(uv.vMax.x, uv.vMax.y);
		v[2].color = -1;

		v[3].position = vec3_t(quad.vMax.x, quad.vMax.y, 0);
		v[3].uv = vec2_t(uv.vMax.x, uv.vMax.y);
		v[3].color = -1;

		v[4].position = vec3_t(quad.vMin.x, quad.vMax.y, 0);
		v[4].uv = vec2_t(uv.vMin.x, uv.vMax.y);
		v[4].color = -1;

		v[5].position = vec3_t(quad.vMin.x, quad.vMin.y, 0);
		v[5].uv = vec2_t(uv.vMin.x, uv.vMin.y);
		v[5].color = -1;

		Surface s;

		s.pVertices = v;
		s.nVertices = 6;

		return s;
	}

	Surface Surface::CreateBox(vertex_t* v, const vec3_t& x, const vec3_t& y, const vec3_t& z)
	{
		vertex_t* start = v;

		if (z.Length() < math::EPS)
		{
			v->position = -(x + y);
			v->uv = vec2_t(0, 0);
			v->color = -1;
			v++;

			v->position = x - y;
			v->uv = vec2_t(1, 0);
			v->color = -1;
			v++;

			v->position = x + y;
			v->uv = vec2_t(1, 1);
			v->color = -1;
			v++;

			v->position = x + y;
			v->uv = vec2_t(1, 1);
			v->color = -1;
			v++;

			v->position = y - x;
			v->uv = vec2_t(0, 1);
			v->color = -1;
			v++;

			v->position = -(x + y);
			v->uv = vec2_t(0, 0);
			v->color = -1;
			v++;
		}

		Surface s;

		s.pVertices = start;
		s.nVertices = v - start;

		return s;
	}

	namespace surface
	{
		Mesh& Mesh::AddBox(const vec3_t& x, const vec3_t& y, const vec3_t& z)
		{
			vertex_t v;

			if (z.Length() < math::EPS)
			{
				const int indices[6] = { 0, 1, 2, 2, 3, 0 };

				for (int i = 0; i < 6; m_indices.push_back(m_vertices.size() + indices[i++]));

				vertex_t v;

				v.position = -(x + y), v.uv = vec2_t(0, 0), v.color = -1;
				m_vertices.push_back(v);

				v.position = x - y, v.uv = vec2_t(1, 0), v.color = -1;
				m_vertices.push_back(v);

				v.position = x + y, v.uv = vec2_t(1, 1), v.color = -1;
				m_vertices.push_back(v);

				v.position = y - x, v.uv = vec2_t(0, 1), v.color = -1;
				m_vertices.push_back(v);
			}

			return *this;
		}

		Mesh& Mesh::AddQuad(const box2_t& r, const box2_t& uv, const color_t& color)
		{
			const int indices[6] = { 0, 1, 2, 2, 3, 0 };

			for (int i = 0; i < 6; m_indices.push_back(m_vertices.size() + indices[i++]));

			vertex_t v;

			v.position = vec3_t(r.vMin.x, r.vMin.y, 0), v.uv = vec2_t(uv.vMin.x, uv.vMin.y), v.color = color;
			m_vertices.push_back(v);

			v.position.x = r.vMax.x, v.uv.x = uv.vMax.x;
			m_vertices.push_back(v);

			v.position.y = r.vMax.y, v.uv.y = uv.vMax.y;
			m_vertices.push_back(v);

			v.position.x = r.vMin.x, v.uv.x = uv.vMin.x;
			m_vertices.push_back(v);

			return *this;
		}

		void Mesh::SetColor(const color_t& value)
		{
			//std::transform(m_vertices.begin(), m_vertices.end(), boost::bind(&vertex_t::color, _1) = value);
			for (size_t i = 0; i < m_vertices.size(); m_vertices[i++].color = value)
				;
		}
	}

	void Surface::SetColor(const color_t& nColor)
	{
		for (size_t i = 0; i < nVertices; i++)
		{
			const_cast<vertex_t*>(pVertices)[i].color = nColor;
		}
	}
}