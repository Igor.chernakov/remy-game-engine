#pragma once

//the main part of the map - node_t class

class node_t : public es::SceneManager {
public:
	node_t()
		: pSurfaces(NULL)
	{};
	~node_t() {
		if (pSurfaces) {
			for (std::list<TSurface*>::iterator it = pSurfaces->begin(); it != pSurfaces->end(); it++) {
				delete [] (*it)->pVertices;
				delete [] (*it)->pIndices;
				delete (*it);
			}
			delete pSurfaces;
		}
	};

private:
	static const int DEFAULT_GRASS_LAYERS = 6;
	static const int DEFAULT_GRASS_HEIGHT = 24;
	static const int MINIMAL_GRASS_ALPHA = 210;
	static const int MAXIMAL_GRASS_ALPHA = 255;
	static const int MINIMAL_GRASS_COLOR = 64;
	static const int MAXIMAL_GRASS_COLOR = 255;

	struct TSurface : Surface {
		TSurface() : mode(Map::RENDER_TERRAIN), nSortValue(0), nRenderFrame(-1) {};

		Map::render_mode_t mode;
		int nSortValue;
		int nRenderFrame;

		bool operator() ( const TSurface* f1, const TSurface* f2 ) {
			return f1->nSortValue < f2->nSortValue;
		}
	};

	void PushSurface(const TSurface *pSurface) {
		const int nMaxVertices = 1024;
		const int nMaxIndices = 2048;

		TSurface *s = NULL;

		for (std::list<TSurface*>::iterator it = pSurfaces->begin(); it != pSurfaces->end(); it++) {
			if (
				(*it)->pMaterial != pSurface->pMaterial ||
				(*it)->mode != pSurface->mode ||
				(*it)->nVertices + pSurface->nVertices >= nMaxVertices ||
				(*it)->nIndices + pSurface->nIndices >= nMaxIndices)
			{
				continue;
			}

			s = (*it);
			goto surface_match;
		}

		s = new TSurface;

		memcpy(s, pSurface, sizeof(TSurface));

		s->pVertices = new Surface::Vertex[nMaxVertices];
		s->pIndices = new Surface::Index[nMaxIndices];
		s->nIndices = 0;
		s->nVertices = 0;

		pSurfaces->push_front(s);

surface_match:
		memcpy(s->pVertices + s->nVertices, pSurface->pVertices, pSurface->nVertices * sizeof(Surface::Vertex));

		for (int i = 0; i < pSurface->nIndices; i++)
			s->pIndices[s->nIndices + i] = pSurface->pIndices[i] + s->nVertices;

		s->nVertices = s->nVertices + pSurface->nVertices;
		s->nIndices = s->nIndices + pSurface->nIndices;

		if (pSurface->mode == Map::RENDER_SHADOW)
			s->SetColor(0x80FFFFFF);
	}

	void PushCliff(const Map *pMap, const w3e_t::data_t::TilePoint *t) {
		char filename[16] = { NULL };

		BYTE minimal_level = -1;
		BYTE maximal_level = 0;

		int nRampPoints = 0;

		for (int i = 0; i < w3e_t::data_t::TilePoint::CORNERS_COUNT; i++) {
			if (t->c[i]->GetFlags() & w3e_t::data_t::TilePoint::FLAG_RAMP)
				nRampPoints++;
			minimal_level = min(minimal_level, t->c[i]->GetLayer());
			maximal_level = max(maximal_level, t->c[i]->GetLayer());
		}

		for (int i = 0; i < w3e_t::data_t::TilePoint::CORNERS_COUNT; i++) {
			switch (t->c[i]->GetLayer() - minimal_level) {
case 0:
	if (nRampPoints > 1 && t->c[i]->GetFlags() & w3e_t::data_t::TilePoint::FLAG_RAMP)
		filename[i] = 'L';
	else
		filename[i] = 'A';
	break;
case 1:
	if (nRampPoints > 1 && t->c[i]->GetFlags() & w3e_t::data_t::TilePoint::FLAG_RAMP)
		filename[i] = 'H';
	else
		filename[i] = 'B';
	break;
case 2:
	if (nRampPoints > 1 && t->c[i]->GetFlags() & w3e_t::data_t::TilePoint::FLAG_RAMP)
		filename[i] = 'X';
	else
		filename[i] = 'C';
	break;
			};
		}

		char sFilePath[MAX_PATH] = { 0 };

		math::Vector2 vOffset(1, 0);

		if (nRampPoints > 1) {
			sprintf_s(sFilePath, MAX_PATH, "models/doodads/cliffs/trans/%s0.md3mesh", filename);
			//hacky way to deal with shity-converted cliff models
			if ((filename[2] == 'H' && filename[3] == 'L') || (filename[2] == 'X' && filename[3] == 'H')) vOffset = math::Vector2(1,-1);
			if ((filename[1] == 'H' && filename[0] == 'L') || (filename[1] == 'X' && filename[0] == 'H')) vOffset = math::Vector2(1,-1);
			if ((filename[1] == 'H' && filename[2] == 'L') || (filename[1] == 'X' && filename[2] == 'H')) vOffset = math::Vector2(2,0);
			if ((filename[0] == 'H' && filename[3] == 'L') || (filename[0] == 'X' && filename[3] == 'H')) vOffset = math::Vector2(2,0);
		} else {
			sprintf_s(sFilePath, MAX_PATH, "models/doodads/cliffs/base/%s0.md3mesh", filename);
		}

		const StaticMesh *model = MeshManager::Get()->Load2(sFilePath);

		for (const StaticMesh::mesh_t *mesh = model->meshes; mesh - model->meshes < model->header.numMeshes; mesh++) {
			Surface::Vertex *v = new Surface::Vertex[mesh->header.numVertices];
			Surface::Index *ind = new Surface::Index[mesh->header.numIndices];

			for (int i = 0; i < mesh->header.numIndices; i++)
				ind[i] = mesh->pIndices[i];

			for (int i = 0; i < mesh->header.numVertices; i++) {
				v[i].nColor = 0xFFFFFFFF;
				v[i].vTexCoord[0] = mesh->pTexCoords[i];
				v[i].vPosition.x = mesh->pVertices[i].vertex[0] * StaticMesh::scale + t->position.x + vOffset.x * TILESIZE;
				v[i].vPosition.y = mesh->pVertices[i].vertex[1] * StaticMesh::scale + t->position.y + vOffset.y * TILESIZE;
				v[i].vPosition.z = mesh->pVertices[i].vertex[2] * StaticMesh::scale +
					pMap->GetPointInternalHeight(math::Vector2(v[i].vPosition.x, v[i].vPosition.y)) / TILESUBHEIGHT + (minimal_level - TILEZEROLAYER) * TILELAYERHEIGHT;
			}

			TSurface mSurface;

			mSurface.pMaterial = t->pCliffMaterial;
			mSurface.pIndices = ind;
			mSurface.pVertices = v;
			mSurface.nIndices = mesh->header.numIndices;
			mSurface.nVertices = mesh->header.numVertices;
			mSurface.nSortValue = 0;
			mSurface.mode = Map::RENDER_TERRAIN;

			PushSurface(&mSurface);

			mSurface.pMaterial = Map::pShadowMaterial;
			mSurface.mode = Map::RENDER_SHADOW;

			PushSurface(&mSurface);

			delete [] v;
		}

		math::Matrix vMatrix;
		vMatrix.SetTranslate(t->position.x + vOffset.x * TILESIZE, t->position.y + vOffset.y * TILESIZE, 0);

		TSurface *pSurface = new TSurface;
		pSurface->mode = RENDER_SHADOWVOLUME;
		model->MakeShadowVolume(pSurface, &vMatrix);

		for (int i = 0; i < pSurface->nVertices; i++) {
			float height = 
				pMap->GetPointInternalHeight(pSurface->pVertices[i - i % 2].vPosition.xy()) / TILESUBHEIGHT + (minimal_level - TILEZEROLAYER) * TILELAYERHEIGHT;

			pSurface->pVertices[i].vPosition.z += height;
		}

		if (pSurface->nVertices > 0)
			PushSurface(pSurface);

		delete [] pSurface->pVertices;
		delete [] pSurface->pIndices;
		delete pSurface;
		/*
		for (int j = 0; j < DEFAULT_GRASS_LAYERS; j++) {
		for (const StaticMesh::mesh_t *mesh = model->meshes; mesh - model->meshes < model->header.numMeshes; mesh++) {
		Surface::Vertex *v = new Surface::Vertex[mesh->header.numVertices];
		Surface::Index *ind = new Surface::Index[mesh->header.numIndices];

		const float k = (float)j / (float)(DEFAULT_GRASS_LAYERS - 1);
		const BYTE bColor = k * (MAXIMAL_GRASS_COLOR - MINIMAL_GRASS_COLOR) + MINIMAL_GRASS_COLOR;
		const BYTE bAlpha = (1 - k) * (MAXIMAL_GRASS_ALPHA - MINIMAL_GRASS_ALPHA) + MINIMAL_GRASS_ALPHA;
		const int nColor = (bAlpha << 24) | (bColor << 16) | (bColor << 8) | bColor;


		for (int i = 0; i < mesh->header.numIndices; i++)
		ind[i] = mesh->pIndices[i];

		for (int i = 0; i < mesh->header.numVertices; i++) {
		v[i].nColor = nColor;
		v[i].vTexCoord[0] = mesh->pTexCoords[i];
		v[i].vPosition.x = mesh->pVertices[i].vertex[0] * StaticMesh::scale + t->position.x + vOffset.x * TILESIZE;
		v[i].vPosition.y = mesh->pVertices[i].vertex[1] * StaticMesh::scale + t->position.y + vOffset.y * TILESIZE;
		v[i].vPosition.z = mesh->pVertices[i].vertex[2] * StaticMesh::scale +
		pMap->GetPointInternalHeight(math::Vector2(v[i].vPosition.x, v[i].vPosition.y)) / TILESUBHEIGHT +
		(minimal_level - TILEZEROLAYER) * TILELAYERHEIGHT +
		(j + 1) * ((float) DEFAULT_GRASS_HEIGHT / (float) DEFAULT_GRASS_LAYERS);
		}

		TSurface mSurface;

		mSurface.pMaterial = t->pCliffMaterial;
		mSurface.pIndices = ind;
		mSurface.pVertices = v;
		mSurface.nIndices = mesh->header.numIndices;
		mSurface.nVertices = mesh->header.numVertices;
		mSurface.nSortValue = 0;
		mSurface.mode = Map::RENDER_VEGETATION;

		PushSurface(&mSurface);

		delete [] v;
		}
		}
		*/
	}

	void PushQuad(
		const math::Vector3 *pQuad,
		int u,
		int v,
		const Material *pMaterial,
		int nSortValue,
		int nColor = 0xFFFFFFFF,
		//				Material::Layer::Blend blend = Material::Layer::BLEND_MIX,
		Map::render_mode_t nRenderMode = Map::RENDER_TERRAIN)
	{
		if (pMaterial->pLayers[0].pTexture->nWidth == pMaterial->pLayers[0].pTexture->nHeight && u > 3)
			u = v = 0;

		const math::Vector2 vTileTextureSize(float (pMaterial->pLayers[0].pTexture->nHeight) / (float(pMaterial->pLayers[0].pTexture->nWidth) * 4.0f), 0.25f);
		const math::Vector2 vTilePixelSize(1.0f / (float) pMaterial->pLayers[0].pTexture->nWidth, 1.0f / (float) pMaterial->pLayers[0].pTexture->nHeight);

		const math::Vector2 vTileSize(vTileTextureSize - vTilePixelSize * 2);
		const math::Vector2 vTilePos(u * vTileTextureSize.u + vTilePixelSize.u, v * vTileTextureSize.v + vTilePixelSize.v);

		const int nVertices = 4;
		const int nIndices = 6;

		Surface::Vertex verts[nVertices];
		Surface::Index inds[nIndices] = { 0, 3, 2, 2, 1, 0};

		verts[0].vPosition = pQuad[0];
		verts[0].vTexCoord[0] = vTilePos + math::Vector2(0, vTileSize.y);
		verts[0].nColor = nColor;
		verts[1].vPosition = pQuad[1];
		verts[1].vTexCoord[0] = vTilePos + math::Vector2(vTileSize.x, vTileSize.y);
		verts[1].nColor = nColor;
		verts[2].vPosition = pQuad[2];
		verts[2].vTexCoord[0] = vTilePos + math::Vector2(vTileSize.x, 0);
		verts[2].nColor = nColor;
		verts[3].vPosition = pQuad[3];
		verts[3].vTexCoord[0] = vTilePos + math::Vector2(0, 0);
		verts[3].nColor = nColor;

		TSurface mSurface;

		mSurface.pMaterial = pMaterial;
		mSurface.pVertices = verts;
		mSurface.pIndices = inds;
		mSurface.nVertices = nVertices;
		mSurface.nIndices = nIndices;
		mSurface.nSortValue = nSortValue;
		//				mSurface.pLayers[0].nBlend = blend;
		mSurface.mode = nRenderMode;

		PushSurface(&mSurface);
	}

	void PushWaterQuad(const w3e_t::data_t::TilePoint *point) {
		float fLevel[w3e_t::data_t::TilePoint::CORNERS_COUNT];
		BYTE bAlpha[w3e_t::data_t::TilePoint::CORNERS_COUNT];
		int nPointsOverWater = 0;

		for (int i = 0; i < w3e_t::data_t::TilePoint::CORNERS_COUNT; i++) {
			if (!point->c[i])
				return;

			fLevel[i] = (2 - TILEZEROLAYER) * TILELAYERHEIGHT +
				(point->c[i]->water_level & 0x3FFF - TILEDEFAULTHEIGHT) / TILESUBHEIGHT - 
				0.3 * TILELAYERHEIGHT;// - 89.6;
			bAlpha[i] = max(0, min(0xFF, (fLevel[i] - point->c[i]->soften_height) * 2));
			if (fLevel[i] > point->c[i]->soften_height)
				nPointsOverWater++;
		}

		if (nPointsOverWater == 0)
			return;


		const int nVertices = 4;
		const int nIndices = 6;

		Surface::Vertex v[nVertices];
		Surface::Index inds[nIndices] = { 0, 1, 2, 2, 3, 0};

		v[0].vPosition = math::Vector3(point->position.x, point->position.y, fLevel[w3e_t::data_t::TilePoint::CENTER]);
		v[0].vTexCoord[0] = v[0].vPosition.xy() / (2 * TILESIZE);
		v[0].bColor[Surface::Vertex::ALPHA] = bAlpha[w3e_t::data_t::TilePoint::CENTER];
		v[1].vPosition = math::Vector3(point->position.x, point->position.y + TILESIZE, fLevel[w3e_t::data_t::TilePoint::TOP]);
		v[1].vTexCoord[0] = v[1].vPosition.xy() / (2 * TILESIZE);
		v[1].bColor[Surface::Vertex::ALPHA] = bAlpha[w3e_t::data_t::TilePoint::TOP];
		v[2].vPosition = math::Vector3(point->position.x + TILESIZE, point->position.y + TILESIZE, fLevel[w3e_t::data_t::TilePoint::TOP_RIGHT]);
		v[2].vTexCoord[0] = v[2].vPosition.xy() / (2 * TILESIZE);
		v[2].bColor[Surface::Vertex::ALPHA] = bAlpha[w3e_t::data_t::TilePoint::TOP_RIGHT];
		v[3].vPosition = math::Vector3(point->position.x + TILESIZE, point->position.y, fLevel[w3e_t::data_t::TilePoint::RIGHT]);
		v[3].vTexCoord[0] = v[3].vPosition.xy() / (2 * TILESIZE);
		v[3].bColor[Surface::Vertex::ALPHA] = bAlpha[w3e_t::data_t::TilePoint::RIGHT];

		for (int i = 0; i < nVertices; i++) {
			for (int j = 0; j < 3; j++) {
				v[i].bColor[j] = 192;
			}
		};

		TSurface mSurface;

		mSurface.pMaterial = NULL;
		mSurface.pVertices = v;
		mSurface.pIndices = inds;
		mSurface.nVertices = nVertices;
		mSurface.nIndices = nIndices;
		mSurface.nSortValue = -1;
		//				mSurface.pLayers[0].nBlend = Material::Layer::BLEND_MIX;
		mSurface.mode = Map::RENDER_WATER;

		PushSurface(&mSurface);
	}

	void PushGrassQuad(const w3e_t::data_t::TilePoint* t) {
		math::Vector3 pVertices[4] = {
			t->c[w3e_t::data_t::TilePoint::CENTER]->position,
			t->c[w3e_t::data_t::TilePoint::RIGHT]->position,
			t->c[w3e_t::data_t::TilePoint::TOP_RIGHT]->position,
			t->c[w3e_t::data_t::TilePoint::TOP]->position
		};

		const Material *pMaterial = NULL;
		const int nGrassID = 2;

		int nU, nV;

		if (!t->GetUV(nGrassID, nU, nV, &pMaterial))
			return;

		for (int j = 0; j < DEFAULT_GRASS_LAYERS; j++) {
			for (int i = 0; i < w3e_t::data_t::TilePoint::CORNERS_COUNT; i++)
				pVertices[i].z += (float) DEFAULT_GRASS_HEIGHT / (float) DEFAULT_GRASS_LAYERS;

			const float k = (float)j / (float)(DEFAULT_GRASS_LAYERS - 1);
			const BYTE bColor = k * (MAXIMAL_GRASS_COLOR - MINIMAL_GRASS_COLOR) + MINIMAL_GRASS_COLOR;
			const BYTE bAlpha = (1 - k) * (MAXIMAL_GRASS_ALPHA - MINIMAL_GRASS_ALPHA) + MINIMAL_GRASS_ALPHA;
			const int nColor = (bAlpha << 24) | (bColor << 16) | (bColor << 8) | bColor;

			PushQuad(pVertices, nU, nV, file::FileManager::Get()->Load2<file::MaterialFile>("Textures/Terrain/Ashenvale/Ashen_GrassLayer"), 0, nColor, Map::RENDER_VEGETATION);
		}
	}

	void PushSurfaces(const Map *pMap, const std::list<const w3e_t::data_t::TilePoint*> &data) {
		SAFE_DEL(pSurfaces);

		pSurfaces = new std::list<TSurface*>;

		for (std::list<const w3e_t::data_t::TilePoint*>::const_iterator it = data.begin(); it != data.end(); it++) {
			//gather tile info
			const w3e_t::data_t::TilePoint *t = (*it);
			const float fScale = 1.0f / TILESUBHEIGHT;

			PushWaterQuad(t);

			if (t->IsCliff()) {
				PushCliff(pMap, t->c[w3e_t::data_t::TilePoint::CENTER]);
				continue;
			}

			const math::Vector3 pVertices[4] = {
				t->c[w3e_t::data_t::TilePoint::CENTER]->position,
				t->c[w3e_t::data_t::TilePoint::RIGHT]->position,
				t->c[w3e_t::data_t::TilePoint::TOP_RIGHT]->position,
				t->c[w3e_t::data_t::TilePoint::TOP]->position
			};

			const Material* pBaseMaterial = t->c[w3e_t::data_t::TilePoint::CENTER]->pGroundMaterial;
			BYTE nBaseGroundID = t->c[w3e_t::data_t::TilePoint::CENTER]->GetGroundID();

			for (int i = 0; i < w3e_t::data_t::TilePoint::CORNERS_COUNT; i++) {
				if (t->c[i] && nBaseGroundID > t->c[i]->GetGroundID()) {
					nBaseGroundID = t->c[i]->GetGroundID();
					pBaseMaterial = t->c[i]->pGroundMaterial;
				}
			}				

			//now push the tile
			PushQuad(pVertices, 4 + t->c[w3e_t::data_t::TilePoint::CENTER]->GetDetails() % 4, t->c[w3e_t::data_t::TilePoint::CENTER]->GetDetails() / 4, pBaseMaterial, nBaseGroundID);
			PushQuad(pVertices, 0, 0, Map::pShadowMaterial, 0, -1, Map::RENDER_SHADOW);

			const int max_ground_count = 16; //HACK

			for (int i = 0; i < max_ground_count; i++) {
				if (i == nBaseGroundID)
					continue;

				const Material *pMaterial = NULL;
				int nU = 0, nV = 0;

				if (t->GetUV(i, nU, nV, &pMaterial))
					PushQuad(pVertices, nU, nV, pMaterial, i);
			}

			PushGrassQuad(t);
		}
		pSurfaces->sort(TSurface());
	}

public:
	void Render(Map::render_mode_t nRenderMode) const {
		if (!Visible())
			return;

		if (!pSurfaces) {
			for (std::list<SceneManager*>::const_iterator it = pChildren.begin(); it != pChildren.end(); it++)
				((node_t*)(*it))->Render(nRenderMode);
		} else {
			for (std::list<TSurface*>::const_iterator it = pSurfaces->begin(); it != pSurfaces->end(); it++) {
				if (nRenderMode != (*it)->mode)
					continue;

				(*it)->nRenderFrame = framework::Render::Get()->GetFrameNum();

				switch ((*it)->mode) {
case Map::RENDER_WATER:
	{
		TSurface tmp = **it;
		if (Map::pWaterMaterial) {
			tmp.pMaterial = Map::pWaterMaterial;
		} else {
			//										tmp.pMaterial = World::Get()->materials.pShadowPlane;
		}
		framework::Render::Get()->PushSurface(&tmp);
	}
	break;
case Map::RENDER_VEGETATION:
	{
		TSurface tmp = **it;
		Material mMaterial = *tmp.pMaterial;
		mMaterial.pLayers[0].nBlend = Material::Layer::ALPHA_TEST;
		tmp.pMaterial = &mMaterial;
		framework::Render::Get()->PushSurface(&tmp);
	}
	break;
case Map::RENDER_SHADOWVOLUME:
	{
		TSurface tmp = **it;

		tmp.pMaterial = Map::pShadowVolumeFront;
		framework::Render::Get()->PushSurface(&tmp);

		tmp.pMaterial = Map::pShadowVolumeBack;
		framework::Render::Get()->PushSurface(&tmp);
	}
	break;
case Map::RENDER_SHADOW:
	{
		TSurface *s = const_cast<TSurface*>(*it);

		if (framework::Render::Get()->GetRenderState() != IRender::STATE_SHADOWMAP) {
			for (int i = 0; i < s->nVertices; i++) {
				math::Vector3 &v = s->pVertices[i].vPosition;
				math::Vector4 q = math::Vector4(v.v[0], v.v[1], v.v[2], 1.0f) * Map::mShadowMatrix;
				s->pVertices[i].vTexCoord[0].u = q.v[0] / q.v[3];
				s->pVertices[i].vTexCoord[0].v = q.v[1] / q.v[3];
				s->pVertices[i].nColor = 0x80FFFFFF;
			}
		}

		if (Map::nRenderFlags & Map::RENDERFLAG_COMPUTESHADOWBOX) {
			for (int i = 0; i < s->nVertices; i++) {
				math::Vector3 &v = s->pVertices[i].vPosition;

				Map::mShadowBoundingBox.vMin.x = min(Map::mShadowBoundingBox.vMin.x, s->pVertices[i].vTexCoord[0].u);
				Map::mShadowBoundingBox.vMin.y = min(Map::mShadowBoundingBox.vMin.y, s->pVertices[i].vTexCoord[0].v);

				Map::mShadowBoundingBox.vMax.x = max(Map::mShadowBoundingBox.vMax.x, s->pVertices[i].vTexCoord[0].u);
				Map::mShadowBoundingBox.vMax.y = max(Map::mShadowBoundingBox.vMax.y, s->pVertices[i].vTexCoord[0].v);
			}
		}

		framework::Render::Get()->PushSurface(s);
	}
	break;
default:
	framework::Render::Get()->PushSurface(*it);
				};
			}
		}
	}

	void Update() {
		if (!pSurfaces) {
			for (std::list<SceneManager*>::iterator it = pChildren.begin(); it != pChildren.end(); it++)
				((node_t*)(*it))->Update();
		} else {
			for (std::list<TSurface*>::iterator it = pSurfaces->begin(); it != pSurfaces->end(); it++) {
				if ((*it)->mode == Map::RENDER_SHADOW)
					continue;

				if ((*it)->nRenderFrame != framework::Render::Get()->GetFrameNum()) {
					if ((*it)->pVertexBuffer != NULL) {
						framework::Render::Get()->DeleteVertexBuffer((*it)->pVertexBuffer->pRenderInfo);
						SAFE_DEL((*it)->pVertexBuffer);
					}
					if ((*it)->pIndexBuffer != NULL) {
						framework::Render::Get()->DeleteIndexBuffer((*it)->pIndexBuffer->pRenderInfo);
						SAFE_DEL((*it)->pIndexBuffer);
					}
				} else {
					if ((*it)->pVertexBuffer == NULL) {
						Surface::Buffer *pVertexBuffer = new Surface::Buffer;
						pVertexBuffer->pRenderInfo = framework::Render::Get()->CreateVertexBuffer((*it)->nVertices);
						framework::Render::Get()->FillVertexBuffer(pVertexBuffer->pRenderInfo, (*it)->pVertices, (*it)->nVertices);
						(*it)->pVertexBuffer = pVertexBuffer;
					}
					if ((*it)->pIndexBuffer == NULL) {
						Surface::Buffer *pIndexBuffer = new Surface::Buffer;
						pIndexBuffer->pRenderInfo = framework::Render::Get()->CreateIndexBuffer((*it)->nIndices);
						framework::Render::Get()->FillIndexBuffer(pIndexBuffer->pRenderInfo, (*it)->pIndices, (*it)->nIndices);
						(*it)->pIndexBuffer = pIndexBuffer;
					}
				}
			}
		}
	}

	void GetMouseRayIntersection(math::Vector3 &vOutput) const  {
		if (!Visible())
			return;

		if (!pSurfaces) {
			for (std::list<SceneManager*>::const_iterator it = pChildren.begin(); it != pChildren.end(); it++)
				((node_t*)(*it))->GetMouseRayIntersection(vOutput);
		} else {
			const math::Vector3 vRayStart = camera::Camera::Get()->GetEye();

			for (std::list<const w3e_t::data_t::TilePoint*>::const_iterator it = data.begin(); it != data.end(); it++) {
				const w3e_t::data_t::TilePoint *t = *it;

				for (int i = 0; i < w3e_t::data_t::TilePoint::CORNERS_COUNT; i++)
					if (!t->c[i])
						goto the_end_of_the_loop;

				goto check_for_intersection;

the_end_of_the_loop:
				continue;

check_for_intersection:
				physics::Impact::LineToTriangle(
					vRayStart,
					vOutput,
					t->c[0]->position,
					t->c[1]->position,
					t->c[2]->position,
					(t->c[0]->position - t->c[1]->position).cross(t->c[1]->position - t->c[2]->position).normalized(),
					&vOutput);

				physics::Impact::LineToTriangle(
					vRayStart,
					vOutput,
					t->c[2]->position,
					t->c[3]->position,
					t->c[0]->position,
					(t->c[2]->position - t->c[3]->position).cross(t->c[3]->position - t->c[0]->position).normalized(),
					&vOutput);
			}
		}
	}

	bool Visible() const {
		return camera::Camera::Get()->AABoxInFrustum
			(
			bbox.vMin.x * TILESIZE,
			bbox.vMin.y * TILESIZE,
			bbox.vMin.z,
			bbox.vMax.x * TILESIZE, 
			bbox.vMax.y * TILESIZE, 
			bbox.vMax.z
			);
	}

	void ProcessBoundingBox(const Map *map) {
		short min_height = SHRT_MAX;
		short max_height = SHRT_MIN;

		for (int x = bbox.vMin.x; x < bbox.vMax.x; x++) {
			for (int y = bbox.vMin.y; y < bbox.vMax.y; y++) {
				short height = map->w3e->GetTilepoint(x, y)->height / (float)TILESUBHEIGHT + (map->w3e->GetTilepoint(x, y)->GetLayer() - TILEZEROLAYER) * TILELAYERHEIGHT;
				min_height = min(min_height, height);
				max_height = max(max_height, height);
			}
		}
		bbox.vMin.z = min_height;
		bbox.vMax.z = max_height;
	};

	void ProcessData(const Map *pMap) {
		for (int x = bbox.vMin.x; x < bbox.vMax.x; x++) {
			for (int y = bbox.vMin.y; y < bbox.vMax.y; y++) {
				data.push_back(pMap->w3e->GetTilepoint(x, y));
			}
		}

		PushSurfaces(pMap, data);

		for (int i = 0; i < pMap->doo->header->doodads_count; i++) {
			const doo_t::data_t::doodad_t *doodad = &pMap->doo->data->doodads[i];
			if (doodad->position.x - pMap->w3e->header->center.x < bbox.vMin.x * TILESIZE)
				continue;
			if (doodad->position.y - pMap->w3e->header->center.y < bbox.vMin.y * TILESIZE)
				continue;
			if (doodad->position.x - pMap->w3e->header->center.x >= bbox.vMax.x * TILESIZE)
				continue;
			if (doodad->position.y - pMap->w3e->header->center.y >= bbox.vMax.y * TILESIZE)
				continue;

			math::Matrix m;

			m.Rotate(0, 0, grad(doodad->angle));
			m.Scale(doodad->scale.x, doodad->scale.y, doodad->scale.z);
			m.SetTranslate(doodad->position - math::Vector3(pMap->w3e->header->center.x, pMap->w3e->header->center.y, TILELAYERHEIGHT * 4));

			m.SetTranslate(m.GetTranslate().x, m.GetTranslate().y, pMap->GetFloor(m.GetTranslate().xy()));

			const char sID[5] = { doodad->id[0], doodad->id[1], doodad->id[2], doodad->id[3], '\0' };

			std::string *pFilename = NULL;

			if (!pFilename)
				pFilename = (std::string*)DoodadsSheet::Get()->GetSlot("file", sID);

			if (!pFilename)
				pFilename = (std::string*)DestructablesSheet::Get()->GetSlot("file", sID);

			if (!pFilename)
				continue;

			const std::string *pTexturename = (std::string*)DestructablesSheet::Get()->GetSlot("texFile", sID);

			const std::string sFilenameNumered = FormatString("%s%i.mdx", pFilename->c_str(), doodad->variation);
			const std::string sFilenameNotNumered = FormatString("%s.mdx", pFilename->c_str());

			if (file::File::Check(sFilenameNumered)) {
				World::Get()->AddEntity(new Doodad(sFilenameNumered, m.GetTranslate(), grad(doodad->angle), doodad->scale.z, pTexturename));
			} else if (file::File::Check(sFilenameNotNumered)) {
				World::Get()->AddEntity(new Doodad(sFilenameNotNumered, m.GetTranslate(), grad(doodad->angle), doodad->scale.z, pTexturename));
			} else {
				Console::Write("Error loading \"%s\"", pFilename->c_str());
			}
		}

		for (int i = 0; i < pMap->units->header->doodads_count; i++) {
			const units_t::data_t::doodad_t *doodad = &pMap->units->data->doodads[i];

			if (doodad->position.x - pMap->w3e->header->center.x < bbox.vMin.x * TILESIZE)
				continue;
			if (doodad->position.y - pMap->w3e->header->center.y < bbox.vMin.y * TILESIZE)
				continue;
			if (doodad->position.x - pMap->w3e->header->center.x >= bbox.vMax.x * TILESIZE)
				continue;
			if (doodad->position.y - pMap->w3e->header->center.y >= bbox.vMax.y * TILESIZE)
				continue;

			math::Matrix m;
			m.Rotate(0, 0, grad(doodad->angle));
			m.Scale(doodad->scale.x, doodad->scale.y, doodad->scale.z);
			m.SetTranslate(doodad->position - math::Vector3(pMap->w3e->header->center.x, pMap->w3e->header->center.y, TILELAYERHEIGHT * 2.5));

			m.SetTranslate(m.GetTranslate().x, m.GetTranslate().y, pMap->GetFloor(m.GetTranslate().xy()));

			const char sID[5] = { doodad->id[0], doodad->id[1], doodad->id[2], doodad->id[3], '\0' };
			if (UnitsSheet::Get()->GetSlot("file", sID)) {
				const std::string sUnitname = *(UnitsSheet::Get()->GetSlot("file", sID)) + ".mdx";
				if (file::File::Check(sUnitname)) {
					World::Get()->AddEntity(new Doodad(sUnitname, m.GetTranslate(), grad(doodad->angle)));
				} else {
					Console::Write("Error: \"%s\"", sUnitname.c_str());
				}
			} else {
			}
		}
	};

	void AddChild(node_t *node) {
		pChildren.push_back(node);
	};

	const math::AxisAlignedBox &GetBoundingBox() const {
		mScaledBoundingBox.vMin = math::Vector3(bbox.vMin.x * TILESIZE, bbox.vMin.y * TILESIZE, bbox.vMin.z);
		mScaledBoundingBox.vMax = math::Vector3(bbox.vMax.x * TILESIZE, bbox.vMax.y * TILESIZE, bbox.vMax.z);

		return mScaledBoundingBox;
	}

private:
	std::list<TSurface*> *pSurfaces;
	std::list<const w3e_t::data_t::TilePoint*> data;

	mutable math::AxisAlignedBox mScaledBoundingBox;

public:
	math::AxisAlignedBox bbox;
} *root;

void BuildQSPTree(node_t *my_root = NULL) {
	const int nLeafSize = 8;

	if (!my_root) {
		root = new node_t;
		root->bbox.vMin.x = 0;
		root->bbox.vMin.y = 0;
		root->bbox.vMax.x = w3e->header->width - 1;
		root->bbox.vMax.y = w3e->header->height - 1;

		World::Get()->pSceneManager = root;

		BuildQSPTree(root);
	} else {
		my_root->ProcessBoundingBox(this);

		if (
			my_root->bbox.vMax.x - my_root->bbox.vMin.x <= nLeafSize || 
			my_root->bbox.vMax.y - my_root->bbox.vMin.y <= nLeafSize) 
		{
			my_root->ProcessData(this);
			return;
		}

		short center_x = my_root->bbox.vMin.x + (my_root->bbox.vMax.x - my_root->bbox.vMin.x) / 2;
		short center_y = my_root->bbox.vMin.y + (my_root->bbox.vMax.y - my_root->bbox.vMin.y) / 2;

		node_t *child[4] = {NULL};

		child[0] = new node_t;
		child[0]->bbox.vMin.x = my_root->bbox.vMin.x;
		child[0]->bbox.vMin.y = my_root->bbox.vMin.y;
		child[0]->bbox.vMax.x = center_x;
		child[0]->bbox.vMax.y = center_y;

		child[1] = new node_t;
		child[1]->bbox.vMin.x = center_x;
		child[1]->bbox.vMin.y = my_root->bbox.vMin.y;
		child[1]->bbox.vMax.x = my_root->bbox.vMax.x;
		child[1]->bbox.vMax.y = center_y;

		child[2] = new node_t;
		child[2]->bbox.vMin.x = my_root->bbox.vMin.x;
		child[2]->bbox.vMin.y = center_y;
		child[2]->bbox.vMax.x = center_x;
		child[2]->bbox.vMax.y = my_root->bbox.vMax.y;

		child[3] = new node_t;
		child[3]->bbox.vMin.x = center_x;
		child[3]->bbox.vMin.y = center_y;
		child[3]->bbox.vMax.x = my_root->bbox.vMax.x;
		child[3]->bbox.vMax.y = my_root->bbox.vMax.y;

		for (int i = 0; i < 4; i++) {
			my_root->AddChild(child[i]);
			BuildQSPTree(child[i]);
		}
	}
};
