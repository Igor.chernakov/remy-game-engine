#include "Plugin.h"

#include <vector>

extern ClassDesc* GetSceneExportClassDesc();
extern ClassDesc* GetModelExportClassDesc();
extern ClassDesc* GetAnimationExportClassDesc();
extern ClassDesc* GetStaticExportClassDesc();
extern ClassDesc* GetEffectExportClassDesc();
extern ClassDesc* GetDialogueHelpDesc();
extern ClassDesc* GetCamMapModDesc();

#pragma comment (lib, "core.lib")
#pragma comment (lib, "maxutil.lib")
#pragma comment (lib, "geom.lib")
#pragma comment (lib, "gfx.lib")
#pragma comment (lib, "paramblk2.lib")
#pragma comment (lib, "mesh.lib")
#pragma comment (lib, "bmm.lib")

struct ClassList: std::vector<ClassDesc*>
{
	ClassList()
	{
		push_back(GetStaticExportClassDesc());
		push_back(GetEffectExportClassDesc());
		push_back(GetSceneExportClassDesc());
		push_back(GetModelExportClassDesc());
		push_back(GetAnimationExportClassDesc());
		push_back(GetDialogueHelpDesc());
		push_back(GetCamMapModDesc());
	};
};

ClassList classlist;

extern "C" {

	__declspec( dllexport ) const TCHAR *LibDescription() {
		return _T("GameEngineEngine Plugin");
	}

	__declspec( dllexport ) int LibNumberClasses() {
		return (int)classlist.size();
	}

	__declspec( dllexport ) ClassDesc* LibClassDesc(int i) {
		if (i < LibNumberClasses())
			return classlist[i];
		else 
			return NULL;
	}

	__declspec( dllexport ) ULONG LibVersion() {
		return VERSION_3DSMAX;
	}

}

HINSTANCE hInstance;

/** public functions **/
BOOL WINAPI DllMain(HINSTANCE hinstDLL,ULONG fdwReason,LPVOID lpvReserved) {
	hInstance = hinstDLL;
	static int call=0;

	switch(fdwReason) {
		case DLL_PROCESS_ATTACH:
			break;
		case DLL_THREAD_ATTACH:
			break;
		case DLL_THREAD_DETACH:
			break;
		case DLL_PROCESS_DETACH:
			break;
	}
	return(TRUE);
}

TCHAR *GetString(int id) {
	static TCHAR buf[256];

	if (hInstance) {
		return LoadString(hInstance, id, buf, sizeof(buf)) ? buf : NULL;
	}

	return NULL;
}
