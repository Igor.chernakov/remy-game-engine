#include "e_window.hpp"


namespace editor
{
	enum
	{
		Wizard_Quit = wxID_EXIT,
		Wizard_RunModal = wxID_HIGHEST,
		Wizard_RunNoSizer,
		Wizard_RunModeless,
		Wizard_About = wxID_ABOUT
	};

	Window::Window(const wxString& title)
		: wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(800, 600))
		, m_splitter(new wxSplitterWindow(this, -1, wxPoint(0, 0), wxSize(400, 400), wxSP_NOBORDER))
		, m_viewport(0)
	{
		InitializeMenu();
		Centre();

		m_viewport = new Viewport(m_splitter);
		m_controls = new wxPanel(m_splitter), m_controls->Show(false);

		m_splitter->Initialize(m_viewport);

	}

	void Window::InitializeMenu()
	{
		wxMenu *menuFile = new wxMenu;
		menuFile->Append(Wizard_RunModal, _T("&Run wizard modal...\tCtrl-R"));
		menuFile->Append(Wizard_RunNoSizer, _T("Run wizard &without sizer..."));
		menuFile->Append(Wizard_RunModeless, _T("Run wizard &modeless..."));
		menuFile->AppendSeparator();
		menuFile->Append(Wizard_Quit, _T("E&xit\tAlt-X"), _T("Quit this program"));

		wxMenu *helpMenu = new wxMenu;
		helpMenu->Append(Wizard_About, _T("&About...\tF1"), _T("Show about dialog"));

		// now append the freshly created menu to the menu bar...
		wxMenuBar *menuBar = new wxMenuBar();
		menuBar->Append(menuFile, _T("&File"));
		menuBar->Append(helpMenu, _T("&Help"));

		// ... and attach this menu bar to the frame
		SetMenuBar(menuBar);
	}

	Window::~Window()
	{
		if (m_viewport) delete m_viewport;
	}
}