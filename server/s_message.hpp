#pragma once

namespace server
{
	struct Message
	{
		Message()
		{
		}

		Message(shared_ptr<Entity> sender, shared_ptr<Entity> receiver, const std::string& name)
			: sender(sender)
			, receiver(receiver)
			, name(name)
		{
		}

		shared_ptr<Entity> sender;
		shared_ptr<Entity> receiver;
		std::string name;
	};
}
