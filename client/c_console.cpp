#include "c_client.hpp"
#include "c_console.hpp"

enum Keys
{
	K_TAB = 9, K_ENTER = 13, K_ESCAPE = 27, K_SPACE = 32, K_BACKSPACE = 127,
	K_UPARROW = 128, K_DOWNARROW = 129, K_LEFTARROW = 130, K_RIGHTARROW = 131,
	K_ALT = 132, K_CTRL = 133, K_SHIFT = 134,
	K_F1 = 135, K_F2 = 136, K_F3 = 137, K_F4 = 138, K_F5 = 139, K_F6 = 140, 
	K_F7 = 141, K_F8 = 142, K_F9 = 143, K_F10 = 144, K_F11 = 145,K_F12 = 146,
	K_INS = 147, K_DEL = 148, K_PGDN = 149, K_PGUP = 150, K_HOME = 151, K_END = 152, K_PAUSE = 255,
	K_MOUSE1 = 200, K_MOUSE2 = 201, K_MOUSE3 = 202,
	K_JOY1 = 203, K_JOY2 = 204, K_JOY3 = 205, K_JOY4 = 206,
	K_AUX1 = 207, K_AUX2 = 208, K_AUX3 = 209, K_AUX4 = 210, K_AUX5 = 211, K_AUX6 = 212,
	K_AUX7 = 213, K_AUX8 = 214, K_AUX9 = 215, K_AUX10 = 216, K_AUX11 = 217, K_AUX12 = 218,
	K_AUX13 = 219, K_AUX14 = 220, K_AUX15 = 221, K_AUX16 = 222, K_AUX17 = 223, K_AUX18 = 224,
	K_AUX19 = 225, K_AUX20 = 226, K_AUX21 = 227, K_AUX22 = 228, K_AUX23 = 229, K_AUX24 = 230,
	K_AUX25 = 231, K_AUX26 = 232, K_AUX27 = 233, K_AUX28 = 234, K_AUX29 = 235, K_AUX30 = 236,
	K_AUX31 = 237, K_AUX32 = 238,
	K_MWHEELUP = 239, K_MWHEELDOWN = 240,
};

unsigned char scantokey[128] =
{
	0, 27, '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-', '=', K_BACKSPACE, 9,
	'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', '[', ']', 13, K_CTRL,'a', 's',
	'd', 'f', 'g', 'h', 'j', 'k', 'l', ';', '\'', '`', K_SHIFT,'\\', 'z', 'x', 'c', 'v',
	'b', 'n', 'm', ',', '.', '/', K_SHIFT,'*', K_ALT,' ', 0, K_F1, K_F2, K_F3, K_F4, K_F5,
	K_F6, K_F7, K_F8, K_F9, K_F10, K_PAUSE, 0, K_HOME, K_UPARROW,K_PGUP,'-',K_LEFTARROW,'5',K_RIGHTARROW,'+',K_END,
	K_DOWNARROW,K_PGDN,K_INS,K_DEL,0,0,0, K_F11, K_F12,0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

unsigned char shiftscantokey[128] =
{
	0, 27, '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', K_BACKSPACE, 9,
	'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', '{', '}', 13, K_CTRL,'A', 'S',
	'D', 'F', 'G', 'H', 'J', 'K', 'L', ':', '"', '~', K_SHIFT,'|', 'Z', 'X', 'C', 'V',
	'B', 'N', 'M', '<', '>', '?', K_SHIFT,'*', K_ALT,' ', 0, K_F1, K_F2, K_F3, K_F4, K_F5,
	K_F6, K_F7, K_F8, K_F9, K_F10, K_PAUSE, 0, K_HOME, K_UPARROW,K_PGUP,'_',K_LEFTARROW,'%',K_RIGHTARROW,'+',K_END,
	K_DOWNARROW,K_PGDN,K_INS,K_DEL,0,0, 0, K_F11, K_F12,0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

char MapKey(int key)
{
	return (char)key;
	/*
	key = (key>>16)&255;
	if (key > 127)
		return 0;
#ifdef WIN32
	if (GetKeyState(VK_SHIFT) & 0x80)
		return shiftscantokey[key];
#endif
	return scantokey[key];
	*/
}

client::Console::Console()
: m_Visible(false)
, m_Window(framework->GetWindow())
{
	m_Commands.push_back(std::string());
	m_Command = m_Commands.begin();
}

void client::Console::Paint(IOverlay* painter) const
{
	if (!m_Visible)
	{
		return;
	}

	const int HEIGHT = 200, SPACING = 12;

	struct paint_line
	{
		paint_line(IOverlay* painter, int start, int spacing)
			: painter(painter)
			, start(start)
			, spacing(spacing)
		{
		}

		inline void operator()(const client_message_t& message)
		{
			if (start > -spacing)
			{
				std::wstring buffer(message.text.begin(), message.text.end());
				painter->String(buffer, mat4_t::Translation(10,  (float)start, 0), "system", message.color);
				start -= spacing;
			}
		}

		IOverlay* painter;
		int start, spacing;
	};

	painter->Quad(
		null_ptr(),
		box2_t(0, 0, (float)m_Window->GetSize().width, (float)HEIGHT),
		box2_t::Identity,
		color_t(0,0,0));

	std::for_each(g_Messages.rbegin(), g_Messages.rend(), paint_line(painter, HEIGHT - SPACING * 2 - 12, SPACING));

	std::string cmd = "] " + *m_Command + ((framework->GetTimer()->GetMilliseconds() / 500) % 2 ? "_" : "");

	painter->String(
		std::wstring(cmd.begin(), cmd.end()),
		mat4_t::Translation(10, HEIGHT - SPACING - 6, 0),
		"system",
		color_t(1,1,0));
}

void client::Console::Exec(const std::string& command)
{
	std::vector<std::string> tokens;
	boost::split(tokens, command, boost::is_space());
	if (tokens.size() == 0)
		return;

	if (boost::iequals(tokens[0], "set") && tokens.size() == 3)
	{
		framework->GetVariables()->SetVariable(tokens[1], tokens[2]);
	}
	else
	{
		framework->GetScene()->Evaluate(command);
	}

	framework->Log(IFramework::MSG_LOG, "> %s", command.c_str());
}

bool client::Console::ProcessKey(int key)
{
	char c = MapKey(key);
	if (!m_Visible && c != 96)
	{
		return false;
	}
	switch (c)
	{
	case K_BACKSPACE:
		if (m_Command->length() > 0)
		{
			m_Command->erase(m_Command->length() - 1);
		}
		break;
	case -128: //K_UPARROW
		if (m_Command != m_Commands.begin())
		{
			--m_Command;
		}
		break;
	case -127: //K_DOWNARROW
		if (m_Command != --m_Commands.end())
		{
			++m_Command;
		}
		break;
	case K_ENTER:
		Exec(*m_Command);
		m_Commands.erase(++m_Command, m_Commands.end());
		m_Commands.push_back(std::string());
		m_Command = --m_Commands.end();
		break;
	case 96:
		m_Visible = !m_Visible;
		m_Command->clear();
		break;
	default:
		if (c > 31 && c < 128)
		{
			m_Command->push_back(c);
		}
		break;
	}

	return true;
}

void export_Console()
{
	namespace py = boost::python;
	using client::Console;

	DISPOSEME(4) py::class_<IConsole, shared_ptr<IConsole>, boost::noncopyable>("Console", py::no_init)
		.def("Exec", &IConsole::Exec)
		;

}