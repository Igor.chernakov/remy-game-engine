#pragma once

struct doo_t {
	doo_t(const BYTE **offset)
		: header(new header_t(offset))
		, data(new data_t(header, offset))
	{};

	~doo_t() {
		delete header;
		delete data;
	};

	struct header_t {
		header_t(const BYTE **offset) {
			COPYVAR(header, offset);
			COPYVAR(version, offset);
			COPYVAR(unknown, offset);
			COPYVAR(doodads_count, offset);
		}

		char header[4];
		int version;
		int unknown;
		int doodads_count;
	} *header;

	struct data_t {
		data_t(const header_t *header, const BYTE **offset) {
			doodads = new doodad_t[header->doodads_count];
			for (int i = 0; i < header->doodads_count; i++)
				COPYMEM(&doodads[i], offset, 42);
		}

		struct doodad_t {
			char id[4];
			int variation;
			math::Vector3 position;
			float angle;
			math::Vector3 scale;
			BYTE nFlags;
			BYTE lifetime;
			int id_num;
		} *doodads;
	} *data;

};