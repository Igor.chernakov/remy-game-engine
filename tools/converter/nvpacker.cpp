//-----------------------------------------------------------------------------
// Copyright NVIDIA Corporation 2004
// TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THIS SOFTWARE IS PROVIDED 
// *AS IS* AND NVIDIA AND ITS SUPPLIERS DISCLAIM ALL WARRANTIES, EITHER EXPRESS 
// OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF 
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  IN NO EVENT SHALL 
// NVIDIA OR ITS SUPPLIERS BE LIABLE FOR ANY SPECIAL, INCIDENTAL, INDIRECT, OR 
// CONSEQUENTIAL DAMAGES WHATSOEVER INCLUDING, WITHOUT LIMITATION, DAMAGES FOR 
// LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS 
// INFORMATION, OR ANY OTHER PECUNIARY LOSS) ARISING OUT OF THE USE OF OR 
// INABILITY TO USE THIS SOFTWARE, EVEN IF NVIDIA HAS BEEN ADVISED OF THE 
// POSSIBILITY OF SUCH DAMAGES.
// 
// File: Packer.cpp
// Desc: Packer class implementation.
//-----------------------------------------------------------------------------

#include <assert.h>
#include <list>
#include <sstream>

#include "nvpacker.hpp"

namespace convert
{
	HRESULT SaveSurfaceToTGAFile(LPDIRECT3DDEVICE9 pD3DDevice, RECT rect, const char *szFileName, LPDIRECT3DSURFACE9 pSurface);
}

namespace nv
{

	//-----------------------------------------------------------------------------
	// Name: TextureObject()
	// Desc: Constructor for class: set everything to good defaults 
	//-----------------------------------------------------------------------------
	TextureObject::TextureObject()
		: mpD3DDev(NULL)
		, mpFilename("")
		, mType(TEXTYPE_UNINITIALIZED)
	{
		;
	}

	//-----------------------------------------------------------------------------
	// Name: ~TextureObject()
	// Desc: Destructor for class: clean stuff up
	//-----------------------------------------------------------------------------
	TextureObject::~TextureObject()
	{
		;
	}

	//-----------------------------------------------------------------------------
	// Name: GetDevice()
	// Desc: Return a pointer to the d3d device stored
	//-----------------------------------------------------------------------------
	LPDIRECT3DDEVICE9 TextureObject::GetDevice() const
	{
		return mpD3DDev;
	}

	//-----------------------------------------------------------------------------
	// Name: GetFilename()
	// Desc: Return a pointer to the filename stored
	//-----------------------------------------------------------------------------
	const std::string&        TextureObject::GetFilename() const
	{
		return mpFilename;
	}

	//-----------------------------------------------------------------------------
	// Name: GetType()
	// Desc: Returns the type of this object
	//-----------------------------------------------------------------------------
	TextureObject::eTextureType TextureObject::GetType() const
	{
		return mType;
	}

	//-----------------------------------------------------------------------------
	// Name: Init()
	// Desc: initializes the innards of the object
	//-----------------------------------------------------------------------------~
	void TextureObject::Init(LPDIRECT3DDEVICE9 pD3d, const std::string& pFilename)
	{
		assert(mType != TEXTYPE_UNINITIALIZED);
		mpD3DDev   = pD3d;
		mpFilename = pFilename;
	}

	//-----------------------------------------------------------------------------
	// Name: PrintError()
	// Desc: Prints and error to stderr
	//-----------------------------------------------------------------------------~
	void TextureObject::PrintError(char const * pText) const
	{
		fprintf(stderr, "*** Error: %s\n", pText); 
	}

	//-----------------------------------------------------------------------------
	// Name: AtlasObject()
	// Desc: Constructor for class: set everything to good defaults 
	//-----------------------------------------------------------------------------
	AtlasObject::AtlasObject()
		: mAtlasId(-1)
	{
		;
	}

	//-----------------------------------------------------------------------------
	// Name: ~AtlasObject()
	// Desc: Destructor for class: clean stuff up
	//-----------------------------------------------------------------------------
	AtlasObject::~AtlasObject()
	{
		;
	}

	//-----------------------------------------------------------------------------
	// Name: Shrink()
	// Desc: Base class implementation simply does nothing: it's valid and a good
	//       fallback.
	//-----------------------------------------------------------------------------
	void AtlasObject::Shrink()
	{
		;
	}

	//-----------------------------------------------------------------------------
	// Name: GetFilename()
	// Desc: Returns the filname stored in this object
	//-----------------------------------------------------------------------------
	const std::string& AtlasObject::GetFilename() const
	{
		return mpFilename;
	}

	//-----------------------------------------------------------------------------
	// Name: GetId()
	// Desc: Returns the atlas id.
	//-----------------------------------------------------------------------------
	int AtlasObject::GetId() const
	{
		return mAtlasId;
	}

	//-----------------------------------------------------------------------------
	// Name: Texture2D()
	// Desc: Constructor for class: set everything to good defaults 
	//-----------------------------------------------------------------------------
	Texture2D::Texture2D()
		: mpTexture2D(NULL)
		, mpAtlas(boost::shared_ptr<const AtlasObject>())
		, mOffset()
	{
		mType = TEXTYPE_2D;
	}

	//-----------------------------------------------------------------------------
	// Name: ~Texture2D()
	// Desc: Destructor for class: clean stuff up
	//-----------------------------------------------------------------------------~
	Texture2D::~Texture2D()
	{ 
		if (mpTexture2D != NULL)
			mpTexture2D->Release();
	}

	//-----------------------------------------------------------------------------
	// Name: GetFormat()
	// Desc: returns the format of the currently loaded texture
	//-----------------------------------------------------------------------------~
	D3DFORMAT Texture2D::GetFormat() const
	{
		D3DSURFACE_DESC     desc;
		mpTexture2D->GetLevelDesc(0, &desc);

		return desc.Format;
	}

	//-----------------------------------------------------------------------------
	// Name: IsSupportedFormat()
	// Desc: return true if the format is supported 
	//       The list of supported formats are essentially all formats that we 
	//       currently know how to deal with, ie they have a known size.  
	//       It is a fail-safe mechansim to not have to deal w/ unknown 4CC codes.
	//-----------------------------------------------------------------------------~
	bool Texture2D::IsSupportedFormat(D3DFORMAT format) const
	{
		switch (format)
		{
		case D3DFMT_R8G8B8:		
		case D3DFMT_A8R8G8B8:	
		case D3DFMT_X8R8G8B8:	
		case D3DFMT_R5G6B5:		
		case D3DFMT_X1R5G5B5:	
		case D3DFMT_A1R5G5B5:	
		case D3DFMT_A4R4G4B4:	
		case D3DFMT_R3G3B2:		
		case D3DFMT_A8:			
		case D3DFMT_A8R3G3B2:	
		case D3DFMT_X4R4G4B4:	
		case D3DFMT_A2B10G10R10:
		case D3DFMT_G16R16:		
		case D3DFMT_A8P8:		
		case D3DFMT_P8:			
		case D3DFMT_L8:			
		case D3DFMT_A8L8:		
		case D3DFMT_A4L4:		
		case D3DFMT_V8U8:		
		case D3DFMT_Q8W8V8U8:	
		case D3DFMT_V16U16:		
		case D3DFMT_L6V5U5:		
		case D3DFMT_X8L8V8U8:	
		case D3DFMT_A2W10V10U10:
		case D3DFMT_DXT1:		
		case D3DFMT_DXT2:		
		case D3DFMT_DXT3:		
		case D3DFMT_DXT4:		
		case D3DFMT_DXT5:		
#if (DIRECT3D_VERSION == 0x0900)
		case D3DFMT_A8B8G8R8:	
		case D3DFMT_X8B8G8R8:	
		case D3DFMT_A2R10G10B10:
		case D3DFMT_A16B16G16R16:
		case D3DFMT_L16:		
		case D3DFMT_Q16W16V16U16:
		case D3DFMT_CxV8U8:		
		case D3DFMT_G8R8_G8B8:	
		case D3DFMT_R8G8_B8G8:	
		case D3DFMT_R32F:		
		case D3DFMT_G32R32F:	
		case D3DFMT_A32B32G32R32F:
		case D3DFMT_R16F:		
		case D3DFMT_G16R16F:	
		case D3DFMT_A16B16G16R16F:
#endif
			break;
		default:
			return false;
		}
		return true;
	}

	//-----------------------------------------------------------------------------
	// Name: GetNumTexels()
	// Desc: returns the number of texels in this texture
	//-----------------------------------------------------------------------------~
	long Texture2D::GetNumTexels() const
	{
		D3DSURFACE_DESC     desc;
		assert(mpTexture2D != NULL);
		mpTexture2D->GetLevelDesc(0, &desc);

		return desc.Width * desc.Height;
	}

	//-----------------------------------------------------------------------------
	// Name: GetWidth()
	// Desc: returns the width of the texture
	//-----------------------------------------------------------------------------~
	long Texture2D::GetWidth() const
	{
		D3DSURFACE_DESC     desc;
		assert(mpTexture2D != NULL);
		mpTexture2D->GetLevelDesc(0, &desc);

		return desc.Width;
	}

	//-----------------------------------------------------------------------------
	// Name: GetHeight()
	// Desc: returns the height of the texture
	//-----------------------------------------------------------------------------~
	long Texture2D::GetHeight() const
	{
		D3DSURFACE_DESC     desc;
		assert(mpTexture2D != NULL);
		mpTexture2D->GetLevelDesc(0, &desc);

		return desc.Height;
	}

	//-----------------------------------------------------------------------------
	// Name: GetD3DTexture()
	// Desc: returns the internal d3d texture
	//-----------------------------------------------------------------------------~
	LPDIRECT3DTEXTURE9 Texture2D::GetD3DTexture() const
	{
		assert(mpTexture2D != NULL);
		return mpTexture2D;
	}

	LPDIRECT3DTEXTURE9 Texture2D::GetD3DAtlasTexture() const
	{
		assert(mpAtlas != NULL);
		return boost::static_pointer_cast<const Atlas2D>(mpAtlas)->GetD3DTexture();
	}


	//-----------------------------------------------------------------------------
	// Name: LoadTexture()
	// Desc: loads the textureinto memory according to the given options
	//       If all goes well and as expected returns S_OK, otherwise
	//       the error that occured.
	//-----------------------------------------------------------------------------~
	HRESULT Texture2D::LoadTexture(const char* mem, int size)
	{
		assert(mpD3DDev != NULL); 

		// We always force max number of mip-levels: hopefully the texture-author provided them!
		// Note: D3DX does the right thing and only generates the ones that do not exist.
		// Unless -nomipmap was spec'd: in that case we force everything to one surface only

		UINT kMipLevels =  1;

		D3DXIMAGE_INFO info;
		LPDIRECT3DTEXTURE9 texture;
		D3DXGetImageInfoFromFileInMemory(mem, size, &info);


		HRESULT hr = D3DXCreateTextureFromFileInMemoryEx(mpD3DDev, mem, size, 
			info.Width, info.Height, kMipLevels, 
			0, D3DFMT_A8R8G8B8, D3DPOOL_SCRATCH, 
			D3DX_DEFAULT, D3DX_DEFAULT, 0, NULL, NULL, 
			&mpTexture2D);
		// Can only pack 2D textures of known types
		if ( (mpTexture2D->GetType() != D3DRTYPE_TEXTURE) || (! IsSupportedFormat((D3DFORMAT)GetFormat())))
		{
			return E_FAIL;
		}
		return S_OK;
	}

	//-----------------------------------------------------------------------------
	// Name: SetAtlas()
	// Desc: Sets the atlas for this texture: set mpatlas to passed in pointer
	//       and copy offset structure
	//-----------------------------------------------------------------------------
	void Texture2D::SetAtlas(boost::shared_ptr<const AtlasObject> pAtlas, OffsetStructure const &offset)
	{
		mpAtlas = pAtlas;
		mOffset = offset;
	}
	void Texture2D::GetScaleFactors( float* fScaleX, float* fScaleY, float* fWidthOffset, float* fHeightOffset )const
	{
		// if mpAtlas is NULL then we failed to insert this texture anywhere
		// in that case spit out this texture as its own atlas
		// Note that we still apply the halftexel offset as requested.
		float const kOffset  = 0.0f; //(options.IsSet(CLO_HALFTEXEL)) ? 0.5f : 0.0f;
		float       kUOffset = kOffset/static_cast<float>(GetWidth());
		float       kVOffset = kOffset/static_cast<float>(GetHeight());

		float uOffset = 0.0f + kUOffset;
		float vOffset = 0.0f + kVOffset;
		float wOffset = 0.0f;
		float uWidth  = 1.0f - (2.0f * kUOffset);
		float vHeight = 1.0f - (2.0f * kVOffset);

		if (mpAtlas != NULL)
		{
			float const kWidth   = static_cast<float>(mpAtlas->GetWidth());
			float const kHeight  = static_cast<float>(mpAtlas->GetHeight());

			kUOffset = kOffset/kWidth;
			kVOffset = kOffset/kHeight;
			uOffset = static_cast<float>(mOffset.uOffset)/kWidth  + kUOffset;
			vOffset = static_cast<float>(mOffset.vOffset)/kHeight + kVOffset;  
			wOffset = static_cast<float>(mOffset.slice); 
			uWidth  = static_cast<float>(mOffset.width) /kWidth   - 2.0f*kUOffset;
			vHeight = static_cast<float>(mOffset.height)/kHeight  - 2.0f*kVOffset;

		}

		*fScaleX        = uWidth;
		*fScaleY        = vHeight;
		*fWidthOffset   = uOffset;
		*fHeightOffset  = vOffset;
	}

	//-----------------------------------------------------------------------------
	// Name: Atlas2D()
	// Desc: Constructor for class: set everything to good defaults 
	//       Create a suitable atlas texture.
	//-----------------------------------------------------------------------------
	Atlas2D::Atlas2D( const std::string& szAtlasFileName,LPDIRECT3DDEVICE9 pDev,int uiWidth, int uiHeight )
		: AtlasObject()
		, mpTexture2D(NULL)
		, mpPacker2D(NULL)
	{
		mType = TEXTYPE_ATLAS2D;

		mFilename = szAtlasFileName;
		mAtlasId = 0;

		Init(pDev, mFilename);

		HRESULT hr = mpD3DDev->CreateTexture(uiWidth, uiHeight, 1, D3DUSAGE_DYNAMIC, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &mpTexture2D
#if (DIRECT3D_VERSION == 0x0900)
			, NULL
#endif
			); 

		if ( hr != D3D_OK )
		{
			return;
		}

		for (UINT i = 0; i < mpTexture2D->GetLevelCount(); i++)
		{
			D3DSURFACE_DESC desc;
			mpTexture2D->GetLevelDesc(i, &desc);
			//let's zero this texture for debug
			D3DLOCKED_RECT r;
			mpTexture2D->LockRect(i, &r, 0, 0);

			for (int j = 0; j < desc.Width * desc.Height; ((int*)r.pBits)[j++] = 0);
			mpTexture2D->UnlockRect(i);
		}

	}

	boost::shared_ptr<Atlas2D> Atlas2D::Create( const std::string& szAtlasFileName, LPDIRECT3DDEVICE9 pDev, int uiWidth, int uiHeight )
	{
		boost::shared_ptr<Atlas2D> atlas(new Atlas2D(szAtlasFileName, pDev, uiWidth, uiHeight));
		atlas->mpPacker2D = new Packer2D(atlas);
		return atlas;
	}


	//-----------------------------------------------------------------------------
	// Name: ~Atlas2D()
	// Desc: Destructor for class: clean stuff up
	//-----------------------------------------------------------------------------~
	Atlas2D::~Atlas2D()
	{ 
		delete mpPacker2D;
		if (mpTexture2D != NULL)
		{
			for (UINT i = 0; i < mpTexture2D->GetLevelCount(); i++)
			{
				D3DSURFACE_DESC desc;
				mpTexture2D->GetLevelDesc(i, &desc);
			}

			mpTexture2D->Release();
		}
	}

	//-----------------------------------------------------------------------------
	// Name: GetFormat()
	// Desc: returns the format of the texture
	//-----------------------------------------------------------------------------~
	D3DFORMAT Atlas2D::GetFormat() const
	{
		D3DSURFACE_DESC     desc;
		mpTexture2D->GetLevelDesc(0, &desc);

		return desc.Format;
	}

	//-----------------------------------------------------------------------------
	// Name: IsSupportedFormat()
	// Desc: return true if the format is supported 
	//       The list of supported formats are essentially all formats that we 
	//       currently know how to deal with, ie they have a known size.  
	//       It is a fail-safe mechansim to not have to deal w/ unknown 4CC codes.
	//-----------------------------------------------------------------------------~
	bool Atlas2D::IsSupportedFormat(D3DFORMAT format) const
	{
		switch (format)
		{
		case D3DFMT_R8G8B8:		
		case D3DFMT_A8R8G8B8:	
		case D3DFMT_X8R8G8B8:	
		case D3DFMT_R5G6B5:		
		case D3DFMT_X1R5G5B5:	
		case D3DFMT_A1R5G5B5:	
		case D3DFMT_A4R4G4B4:	
		case D3DFMT_R3G3B2:		
		case D3DFMT_A8:			
		case D3DFMT_A8R3G3B2:	
		case D3DFMT_X4R4G4B4:	
		case D3DFMT_A2B10G10R10:
		case D3DFMT_G16R16:		
		case D3DFMT_A8P8:		
		case D3DFMT_P8:			
		case D3DFMT_L8:			
		case D3DFMT_A8L8:		
		case D3DFMT_A4L4:		
		case D3DFMT_V8U8:		
		case D3DFMT_Q8W8V8U8:	
		case D3DFMT_V16U16:		
		case D3DFMT_L6V5U5:		
		case D3DFMT_X8L8V8U8:	
		case D3DFMT_A2W10V10U10:
		case D3DFMT_DXT1:		
		case D3DFMT_DXT2:		
		case D3DFMT_DXT3:		
		case D3DFMT_DXT4:		
		case D3DFMT_DXT5:		
#if (DIRECT3D_VERSION == 0x0900)
		case D3DFMT_A8B8G8R8:	
		case D3DFMT_X8B8G8R8:	
		case D3DFMT_A2R10G10B10:
		case D3DFMT_A16B16G16R16:
		case D3DFMT_L16:		
		case D3DFMT_Q16W16V16U16:
		case D3DFMT_CxV8U8:		
		case D3DFMT_G8R8_G8B8:	
		case D3DFMT_R8G8_B8G8:	
		case D3DFMT_R32F:		
		case D3DFMT_G32R32F:	
		case D3DFMT_A32B32G32R32F:
		case D3DFMT_R16F:		
		case D3DFMT_G16R16F:	
		case D3DFMT_A16B16G16R16F:
#endif
			break;
		default:
			return false;
		}
		return true;
	}

	//-----------------------------------------------------------------------------
	// Name: GetNumTexels()
	// Desc: returns the number of texels in this texture
	//-----------------------------------------------------------------------------~
	long Atlas2D::GetNumTexels() const
	{
		D3DSURFACE_DESC     desc;
		assert(mpTexture2D != NULL);
		mpTexture2D->GetLevelDesc(0, &desc);

		return desc.Width * desc.Height;
	}

	//-----------------------------------------------------------------------------
	// Name: WriteToDisk()
	// Desc: save the d3d texture into disk file w/ given filename
	//-----------------------------------------------------------------------------~

	void Atlas2D::WriteToDisk( const char* filename ) const
	{
		RECT r;
		LPDIRECT3DSURFACE9 surface;
		mpTexture2D->GetSurfaceLevel(0, &surface);
		//D3DXSaveSurfaceToFile(mFilename.c_str(), D3DXIFF_BMP, surface, NULL, NULL);
		r.left = 0; r.right = GetWidth(); r.top = 0; r.bottom = GetHeight();
		convert::SaveSurfaceToTGAFile(convert::device, r, filename, surface);
	}

	//-----------------------------------------------------------------------------
	// Name: GetWidth()
	// Desc: returns the width of the texture
	//-----------------------------------------------------------------------------~
	long Atlas2D::GetWidth() const
	{
		D3DSURFACE_DESC     desc;
		assert(mpTexture2D != NULL);
		mpTexture2D->GetLevelDesc(0, &desc);

		return desc.Width;
	}

	//-----------------------------------------------------------------------------
	// Name: GetHeight()
	// Desc: returns the height of the texture
	//-----------------------------------------------------------------------------~
	long Atlas2D::GetHeight() const
	{
		D3DSURFACE_DESC     desc;
		assert(mpTexture2D != NULL);
		mpTexture2D->GetLevelDesc(0, &desc);

		return desc.Height;
	}

	//-----------------------------------------------------------------------------
	// Name: GetD3DTexture()
	// Desc: returns the internal d3d texture
	//-----------------------------------------------------------------------------~
	LPDIRECT3DTEXTURE9  Atlas2D::GetD3DTexture() const
	{
		assert(mpTexture2D != NULL);
		return mpTexture2D;
	}

	//-----------------------------------------------------------------------------
	// Name: Insert()
	// Desc: Insert passed in texture into the atlas: return true if successful,
	//       false otherwise.
	//-----------------------------------------------------------------------------
	bool Atlas2D::Insert(Texture2D *pTexture)
	{
		assert(pTexture != NULL);
		if (mpPacker2D == NULL)
			return false;

		return mpPacker2D->Insert(pTexture);
	}

	//-----------------------------------------------------------------------------
	// Name: Shrink()
	// Desc: Attempt to make the stored atlas smaller, ie fit it to its actually
	//       used content.
	//-----------------------------------------------------------------------------
	void Atlas2D::Shrink()
	{
		// For the mip-levels, just ask the packer what the largest 
		// miplevel was that it had to deal with while inserting 
		// textures.  If it is 0 something is screwy and we just punt.
		int     newMipLevel = mpPacker2D->GetMaxMipLevels();
		long    newWidth    = GetWidth();
		long    newHeight   = GetHeight();
		Region  tester;

		if ((newMipLevel <= 0) || (mpPacker2D == NULL))
			return;

		// horizontal shrink first:
		// Can we get rid of the right half of the atlas?
		tester.mTop     = 0L;
		tester.mBottom  = newHeight;
		bool bShrinking = true;
		while (bShrinking && (newWidth > 1))
		{
			tester.mLeft   = newWidth/2L;
			tester.mRight  = newWidth;

			if (! mpPacker2D->Intersects(tester))
				newWidth   = newWidth/2L;
			else
				bShrinking = false;
		}
		// do the same vertically
		tester.mLeft  = 0L;
		tester.mRight = newWidth;
		bShrinking    = true;
		while (bShrinking && (newHeight > 1))
		{
			tester.mTop    = newHeight/2L;
			tester.mBottom = newHeight;

			if (! mpPacker2D->Intersects(tester))
				newHeight  = newHeight/2L;
			else
				bShrinking = false;
		}

		// Now create a new alas w/ these new dimensions and copy 
		// all the bits from one to the other. 
		LPDIRECT3DTEXTURE9      pOldAtlas = mpTexture2D;
		HRESULT hr = mpD3DDev->CreateTexture(newWidth, newHeight, newMipLevel, D3DUSAGE_DYNAMIC, (D3DFORMAT)GetFormat(), D3DPOOL_DEFAULT, &mpTexture2D
#if (DIRECT3D_VERSION == 0x0900)
			, NULL
#endif
			); 
		if ( hr != D3D_OK )
			return;

		tester.mLeft   = tester.mTop = 0L;
		tester.mRight  = newWidth;
		tester.mBottom = newHeight;

		for (UINT i = 0; i < mpTexture2D->GetLevelCount(); i++)
		{
			D3DSURFACE_DESC desc;
			mpTexture2D->GetLevelDesc(i, &desc);
		}

		for (UINT i = 0; i < pOldAtlas->GetLevelCount(); i++)
		{
			D3DSURFACE_DESC desc;
			pOldAtlas->GetLevelDesc(i, &desc);
		}

		mpPacker2D->CopyBits(tester, pOldAtlas);
		pOldAtlas->Release();
	}



	//-----------------------------------------------------------------------------
	// Name: Region()
	// Desc: Constructor
	//-----------------------------------------------------------------------------
	Region::Region()
	{
		;
	}

	//-----------------------------------------------------------------------------
	// Name: Region()
	// Desc: Destructor
	//-----------------------------------------------------------------------------
	Region::~Region()
	{
		;
	}

	//-----------------------------------------------------------------------------
	// Name: GetWidth()
	// Desc: returns width of region
	//-----------------------------------------------------------------------------
	long Region::GetWidth() const
	{
		assert(mRight >= mLeft);
		return (mRight - mLeft);
	}

	//-----------------------------------------------------------------------------
	// Name: GetHeight()
	// Desc: returns height of region
	//-----------------------------------------------------------------------------
	long Region::GetHeight() const
	{
		assert(mBottom >= mTop);
		return (mBottom - mTop);
	}

	//-----------------------------------------------------------------------------
	// Name: Intersect()
	// Desc: returns true if the passed in region intersects this region
	//-----------------------------------------------------------------------------
	bool Region::Intersect(Region const &region) const //rewritten; nvidia assholes have done in all wrong
	{
		if (region.mLeft >= mRight || (region.mRight + 2) <= mLeft || 
			region.mTop >= mBottom || (region.mBottom + 2) <= mTop) 
			return false;
		return true;

		// Note Region is defined as [left, right) and [top, bottom)!
		// general tests: test whether region's edges are inside this region
		//bool const leftIsInside   = (region.mLeft   >= mLeft) && (region.mLeft   <  mRight);
		//bool const rightIsInside  = (region.mRight  >  mLeft) && (region.mRight  <= mRight);
		//bool const topIsInside    = (region.mTop    >= mTop ) && (region.mTop    <  mBottom);
		//bool const bottomIsInside = (region.mBottom >  mTop ) && (region.mBottom <= mBottom);

		// if and only if (at least) one of the horizontal edges and 
		//                (at least) one of the vertical edges inside than the two inersect
		//if ((leftIsInside || rightIsInside) && (topIsInside || bottomIsInside))
		//	return true;
		//return false;
	}

	//-----------------------------------------------------------------------------
	// Name: Packer()
	// Desc: Constructor
	//-----------------------------------------------------------------------------
	Packer::Packer()
		: mTotalFreeTexels(0)
		, mMaxNumberMipLevels(0)

	{
		;
	}

	//-----------------------------------------------------------------------------
	// Name: Packer()
	// Desc: Constructor
	//-----------------------------------------------------------------------------
	Packer::~Packer()
	{
		;
	}

	//-----------------------------------------------------------------------------
	// Name: SizeOfTexel()
	// Desc: Returns the size (in bits) of the given texel format
	//-----------------------------------------------------------------------------
	int Packer::SizeOfTexel( D3DFORMAT format ) const
	{
		switch( format )
		{
		case D3DFMT_R8G8B8:			return 3*8;
		case D3DFMT_A8R8G8B8:		return 4*8;
		case D3DFMT_X8R8G8B8:		return 4*8;
		case D3DFMT_R5G6B5:			return 2*8;
		case D3DFMT_X1R5G5B5:		return 2*8;
		case D3DFMT_A1R5G5B5:		return 2*8;
		case D3DFMT_A4R4G4B4:		return 2*8;
		case D3DFMT_R3G3B2:			return 8;
		case D3DFMT_A8:				return 8;
		case D3DFMT_A8R3G3B2:		return 2*8;
		case D3DFMT_X4R4G4B4:		return 2*8;
		case D3DFMT_A2B10G10R10:	return 4*8;
		case D3DFMT_G16R16:			return 4*8;
		case D3DFMT_A8P8:			return 8;
		case D3DFMT_P8:				return 8;
		case D3DFMT_L8:				return 8;
		case D3DFMT_A8L8:			return 2*8;
		case D3DFMT_A4L4:			return 8;
		case D3DFMT_V8U8:			return 2*8;
		case D3DFMT_Q8W8V8U8:		return 4*8;
		case D3DFMT_V16U16:			return 4*8;
		case D3DFMT_L6V5U5:			return 2*8;
		case D3DFMT_X8L8V8U8:		return 4*8;
		case D3DFMT_A2W10V10U10:	return 4*8;
		case D3DFMT_DXT1:			return 4;
		case D3DFMT_DXT2:			return 8;
		case D3DFMT_DXT3:			return 8;
		case D3DFMT_DXT4:			return 8;
		case D3DFMT_DXT5:			return 8;
		case D3DFMT_UYVY:			return 2*8;
		case D3DFMT_YUY2:			return 2*8;
#if (DIRECT3D_VERSION == 0x0900)
		case D3DFMT_A8B8G8R8:		return 4*8;
		case D3DFMT_X8B8G8R8:		return 4*8;
		case D3DFMT_A2R10G10B10:	return 4*8;
		case D3DFMT_A16B16G16R16:	return 8*8;
		case D3DFMT_L16:			return 2*8;
		case D3DFMT_Q16W16V16U16:	return 8*8;
		case D3DFMT_CxV8U8:			return 2*8;
		case D3DFMT_G8R8_G8B8:		return 2*8;
		case D3DFMT_R8G8_B8G8:		return 2*8;
		case D3DFMT_R16F:			return 2*8;
		case D3DFMT_G16R16F:		return 4*8;
		case D3DFMT_A16B16G16R16F:	return 8*8;
		case D3DFMT_R32F:			return 4*8;
		case D3DFMT_G32R32F:		return 8*8;
		case D3DFMT_A32B32G32R32F:	return 16*8;
#endif
		default:
			return 0;
		}
	}

	//-----------------------------------------------------------------------------
	// Name: IsDXTnFormat()
	// Desc: Returns true if the given format is a DXT format, false otherwise
	//-----------------------------------------------------------------------------
	bool Packer::IsDXTnFormat( D3DFORMAT format ) const 
	{
		switch( format )
		{
		case D3DFMT_DXT1:		
		case D3DFMT_DXT2:		
		case D3DFMT_DXT3:		
		case D3DFMT_DXT4:		
		case D3DFMT_DXT5:		
			return true;
		default:
			return false;
		}
	}

	//-----------------------------------------------------------------------------
	// Name: GetMaxMipLevels()
	// Desc: Returns the largest number of mip-maps of all textures passed into 
	//       Insert() function.
	//-----------------------------------------------------------------------------
	int Packer::GetMaxMipLevels() const 
	{
		return mMaxNumberMipLevels;
	}


	//-----------------------------------------------------------------------------
	// Name: Packer2D()
	// Desc: Constructor
	//-----------------------------------------------------------------------------
	Packer2D::Packer2D(boost::shared_ptr<Atlas2D> pAtlas)
		: mpAtlas(pAtlas)
	{
		assert(pAtlas != NULL);

		mTotalFreeTexels = pAtlas->GetNumTexels();
	}

	//-----------------------------------------------------------------------------
	// Name: Packer2D()
	// Desc: Destructor
	//-----------------------------------------------------------------------------
	Packer2D::~Packer2D()
	{
		std::vector<Region *>::iterator  iterRegion;

		for (iterRegion = mUsedRegions.begin(); iterRegion != mUsedRegions.end(); ++iterRegion)
		{
			delete *iterRegion;
			*iterRegion = NULL;
		}
	}

	//-----------------------------------------------------------------------------
	// Name: Insert()
	// Desc: Insert passed-in texture into atlas.  If no free spot is found, 
	//       return false.  If a free spot is found, copy bits from texture 
	//       to atlas, update texture to point to proper sub-region in atlas
	//       add the newly filled region to the used region vector, then
	//       return true.
	//-----------------------------------------------------------------------------
	bool Packer2D::Insert(Texture2D *pTexture)
	{
		// check for trivial non-fit
		if (pTexture->GetNumTexels() > mTotalFreeTexels)
			return false;

		// find a free spot for this texture
		Region *pTest = new Region();

		long u, v;
		// *** Optimization ***
		// right now this insertion algorithm creates wide horizontal
		// atlases.  It seems it would be more optimal to create more
		// 'square' atlases; or even better insert in such a way as
		// to minimize the total number of used regions, i.e., 
		// always try to attach to one or more edges of existing regions.
		//
		// loop in v: slide test-region vertically across atlas
		for (v = 0; v + pTexture->GetHeight() <= mpAtlas.lock()->GetHeight(); v += 8)
		{
			pTest->mTop    = v;
			pTest->mBottom = v + pTexture->GetHeight();

			// loop in u: slide test-region horizontally across atlas
			for (u = 0; u + pTexture->GetWidth() <= mpAtlas.lock()->GetWidth(); u += 8)
			{
				pTest->mLeft  = u;
				pTest->mRight = u + pTexture->GetWidth();

				// go through all Used regions and see if they overlap
				Region const *pIntersection = Intersects(*pTest);
				if (pIntersection != NULL)
				{
					// found an intersecting used region: try next position
					// but actually advance position by the larger of pTexture's width
					// and this intersection
					/*float ratio =   static_cast<float>(pIntersection->mRight)
						/ static_cast<float>(pTest->mRight);
					ratio = ceilf(max(0.0f, ratio-1.0f));  */
					//u += static_cast<long>(ratio);
					//u = pIntersection->mRight + 2;
				}
				else
				{
					// no intersection found: 
					// merge this region into the used region's vector
					Merge(pTest);

					mTotalFreeTexels -= pTexture->GetNumTexels();
					assert(mTotalFreeTexels >= 0);
					CopyBits(*pTest, pTexture->GetD3DTexture());

					// also update the Texture2D object to set correct 
					// atlas pointers and offsets
					OffsetStructure offset;
					offset.uOffset = pTest->mLeft;
					offset.vOffset = pTest->mTop;
					offset.width   = pTest->GetWidth();
					offset.height  = pTest->GetHeight();
					offset.slice   = 0L;
					pTexture->SetAtlas(mpAtlas.lock(), offset);

					return true;
				}
			}
		}
		// could not insert: free allocated region and return failure
		delete pTest;
		return false;
	}

	//-----------------------------------------------------------------------------
	// Name: CopyBits()
	// Desc: copy the contents of all mip-maps of the passed in texture into 
	//       the mpAtlas bits at the offsets indicated by target region.
	//-----------------------------------------------------------------------------
	void Packer2D::CopyBits(Region const &target, LPDIRECT3DTEXTURE9 pTexture)
	{
		HRESULT         hr;
		RECT            srcRect,       dstRect;
		D3DLOCKED_RECT  srcLockedRect, dstLockedRect;

		srcRect.left = srcRect.top = 0;

		// If -nomipmap was set then mpAtlas only has one mip-map and kNumMipMaps is 1.
		// If it wasn't then mpAtlas has more mip-maps then the texture and kNumMipMaps
		// is the same as there are mip-maps in pTexture.
		int const kNumMipMaps = min(mpAtlas.lock()->GetD3DTexture()->GetLevelCount(), pTexture->GetLevelCount());
		if (kNumMipMaps > mMaxNumberMipLevels)
			mMaxNumberMipLevels = kNumMipMaps;
		for (long mipLevel = 0; mipLevel < kNumMipMaps; ++mipLevel)
		{
			long const div        = static_cast<long>(pow(2.0, mipLevel));
			long const mipWidth   = max(1L, target.GetWidth() /div);
			long const mipHeight  = max(1L, target.GetHeight()/div);

			srcRect.right  = mipWidth;
			srcRect.bottom = mipHeight;

			dstRect.left   = max(0L, target.mLeft/div);
			dstRect.top    = max(0L, target.mTop /div);
			dstRect.right  = dstRect.left + mipWidth;
			dstRect.bottom = dstRect.top  + mipHeight;

			// These calls to LockRect fail (generate errors:
			// Direct3D9: (ERROR) :Rects for DXT surfaces must be on 4x4 boundaries) 
			// in the 2003 Summer SDK Debug Runtime.  They work just fine 
			// (and as expected) when using the retail run-time: 
			// please make sure to use the retail run-time when running this!
			hr = pTexture->LockRect( mipLevel, &srcLockedRect, &srcRect, D3DLOCK_READONLY );
			assert(hr == S_OK);
			hr = mpAtlas.lock()->GetD3DTexture()->LockRect( mipLevel, &dstLockedRect, &dstRect, 0 );
			assert(hr == S_OK);

			int const kBytesPerRow = (mipWidth * SizeOfTexel((D3DFORMAT)mpAtlas.lock()->GetFormat()))/8;
			int const kBlockFactor = (IsDXTnFormat((D3DFORMAT)mpAtlas.lock()->GetFormat()) ? 4 : 1);
			UCHAR *srcPtr = reinterpret_cast<UCHAR*>(srcLockedRect.pBits);
			UCHAR *dstPtr = reinterpret_cast<UCHAR*>(dstLockedRect.pBits);

			for (int i = 0; i < mipHeight/kBlockFactor; ++i)
			{
				memcpy( dstPtr, srcPtr, kBlockFactor * kBytesPerRow );
				srcPtr += srcLockedRect.Pitch;
				dstPtr += dstLockedRect.Pitch;
			}

			hr = pTexture->UnlockRect( mipLevel );
			assert(hr == S_OK);
			hr = mpAtlas.lock()->GetD3DTexture()->UnlockRect( mipLevel );
			assert(hr == S_OK);
		}
	}

	//-----------------------------------------------------------------------------
	// Name: Merge()
	// Desc: *** Optimization ***
	//       Ideally here we should try to merge used region into 
	//       fewer and larger regions, ie can the new region combine 
	//       with any existing one, if yes, merge them and repeat
	//       until no more merges occur.
	//-----------------------------------------------------------------------------
	void Packer2D::Merge(Region * pNewRegion) 
	{
		mUsedRegions.push_back(pNewRegion);
		// since we have the 'advance more than 1 step in u if possible' optimization
		// it makes sense to favor wider over higher regions.
		// so try to merge horizontally before trying to merge vertically.
		;
	}

	//-----------------------------------------------------------------------------
	// Name: Intersects()
	// Desc: returns NULL if the passed in region does not intersect any stored 
	//       used regions.
	//       Returns a pointer to the intersecting region if it does intersect. 
	//-----------------------------------------------------------------------------
	Region const * Packer2D::Intersects(Region const &region) const
	{
		// go through all Used regions and see if they overlap
		std::vector<Region *>::const_iterator iterUsed;
		for (iterUsed = mUsedRegions.begin(); iterUsed != mUsedRegions.end(); ++iterUsed)
			if ((*iterUsed)->Intersect(region))
			{
				// found an intersecting used region: return the result
				return *iterUsed;
			}

			// made it through w/o finding intersection: return NULL;
			return NULL;
	}
}

namespace convert {

std::vector<boost::weak_ptr<nv::Atlas2D> > packers;

extern int ATLAS_SIZE;

PackedTexture::PackedTexture(const char* mem, int size)
	: m_name("$loadedfrombuffer")
	, m_texture(new nv::Texture2D())
	, m_minx(0), m_miny(0), m_maxx(1), m_maxy(1)
{
	m_texture->Init(convert::device, "$loadedfrombuffer");
	m_texture->LoadTexture(mem, size);

	bool inserted = false;

	packers.erase(
		std::remove_if(
		packers.begin(),
		packers.end(),
		std::mem_fun_ref(&boost::weak_ptr<nv::Atlas2D>::expired)), packers.end());

	for (int i = 0; i < packers.size(); i++)
	{
		if (packers[i].lock()->Insert(m_texture))
		{
			inserted = true;
			break;
		}
	}

	if (!inserted)
	{
		std::stringstream ss;
		ss << packers.size() + 1 << ".tga";
		boost::shared_ptr<nv::Atlas2D> p2DAtlas = nv::Atlas2D::Create(ss.str(), convert::device, ATLAS_SIZE, ATLAS_SIZE);
		p2DAtlas->Insert(m_texture);
		packers.push_back(p2DAtlas);
	}
}

}