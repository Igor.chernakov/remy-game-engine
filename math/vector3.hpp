#pragma once

#include "math.hpp"

namespace math
{
	struct Matrix;
	struct Quaternion;

	struct Vector3
	{
		static const int Dimensions = 3;

		union
		{
			Real v[Dimensions];

			struct
			{
				Real x,y,z;
			};
		};

		static const Vector3 Zero;
		static const Vector3 One;

		Vector3();
		Vector3(Real x, Real y, Real z);
		Vector3(const std::string& param);
		Vector3(const Real* param);
		Vector3(const Vector3& param);

		DEFINE_DEFAULT_VECTOR_OPS(Vector3);

		Vector3& Assign(Real x, Real y, Real z);

		Vector3& Maximize();
		Vector3& Minimize();

		static Vector3 Cross(const Vector3& left, const Vector3& right);
		Vector3& Cross(const Vector3& param);

		Vector3& operator*=(const Matrix& m);
		Vector3 operator*(const Matrix& m) const;

		Vector3& operator*=(const Quaternion& q);
		Vector3 operator*(const Quaternion& q) const;

		static Vector3 Multiply(const Vector3& v, const Matrix& m);
		Vector3& Multiply(const Matrix& m);

		static Vector3 Multiply(const Vector3& v, const Quaternion& q);
		Vector3& Multiply(const Quaternion& m);

		inline Vector3 operator^(const Vector3& param) const { return Cross(*this, param); }
	};
};
