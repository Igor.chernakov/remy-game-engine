#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <deque>
#include <vector>

struct ITagger {
	virtual void PopTag() = 0;
	virtual void PushTag(const std::string& name) = 0;
};

struct Tag {
	Tag(ITagger* file, const std::string& name) : file(file) {
		file->PushTag(name);
	}

	~Tag() {
		file->PopTag();
	}

	operator bool() const {
		return file != 0;
	}

	ITagger* file;
};

class FileOutput : public std::ofstream, public ITagger
{
public:
	enum Mode {
		TEXTUAL,
		BINARY,
	};

public:
	FileOutput(const std::string& filename, Mode mode = TEXTUAL);

	struct tag_t
	{
		tag_t();

		struct header_t {
			header_t(std::string& name, std::streamoff block_size);
			static const int NAMELEN = 4;
			std::string name() const { return std::string(m_name, NAMELEN); }
		private:
			char m_name[NAMELEN];
		public:
			std::streamoff size;
		};

		void StreamHeader(FileOutput* file);

		std::string name;
		std::streamoff offset;
		bool node;
	};

	template <typename T>
	void Write(const std::string& tag, const std::vector<T>& vec) {
		if (vec.size()) {
			Tag _tag(this, tag);
			write((char*)&vec.front(), (std::streamsize)(vec.size() * sizeof(T))); 
		}
	}

	void Write(const std::string& tag, const std::string& string) {
		if (string.size()) {
			Tag _tag(this, tag);
			write(&string[0], (std::streamsize)(string.size()));
		}
	}

	template <typename T>
	void Write(const std::string& tag, const T& value) {
		Tag _tag(this, tag);
		write((char*)&value, (std::streamsize)(sizeof(T)));
	}

	void PopTag();
	void PushTag(const std::string& name);

	const Mode mode;
	const std::string filename;

private:
	void Finalize();

	std::deque<tag_t> state;
};

