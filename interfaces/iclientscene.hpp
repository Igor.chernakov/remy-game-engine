#pragma once

#include "../math/math.hpp"

#include "icamera.hpp"
#include "iobject.hpp"
#include "inavigation.hpp"

#include <boost/shared_ptr.hpp>

typedef int ObjectHandle;

struct IClientScene: IObject
{
	enum scene_message_t
	{
		MSG_LBUTTON_DOWN,
		MSG_LBUTTON_UP,
		MSG_RBUTTON_DOWN,
		MSG_RBUTTON_UP,
		MSG_MBUTTON_DOWN,
		MSG_MBUTTON_UP,
		MSG_MOUSE_MOVE,
		MSG_KEY_DOWN,
		MSG_ACTIVATE,
		MSG_KEY_DOWN_FORMATTED,
	};

	virtual void OnSystemMsg(scene_message_t msg, int param) = 0;
	virtual vec2_t GetCursorPos() = 0;
	virtual boost::shared_ptr<ICamera> GetCamera() const = 0;
	virtual void SetCamera(boost::shared_ptr<ICamera> camera) = 0;
	virtual bool IsPaused() const = 0;
	virtual bool IsActive() const = 0;
	virtual boost::shared_ptr<INavigation> GetNavigation() const = 0;
	virtual void SetNavigation(boost::shared_ptr<INavigation> navigation) = 0;
	virtual std::string Run() = 0;
	virtual const light_t* GetLight() const = 0;
	virtual void PushSurface(const struct ISurface* surface, const struct IMaterial* material = 0) = 0;
	virtual void RenderFrame(struct IRenderer* renderer) const = 0;
	virtual void Evaluate(const std::string& command) = 0;
	virtual void RenderLoading(struct IRenderer* renderer) const = 0;
};
