#pragma once

#include "iobject.hpp"
#include "isound.hpp"

#include <string>

struct ISoundSystem: IObject
{
	virtual boost::shared_ptr<const ISound> LoadMusic(const std::string& path) = 0;
	virtual boost::shared_ptr<const ISound> LoadSound(const std::string& path) = 0;
	virtual void Activate(bool enable) = 0;
	virtual bool IsSoundEnabled() const = 0;
	virtual bool IsMusicEnabled() const = 0;
	virtual void SetSoundEnabled(bool value) = 0;
	virtual void SetMusicEnabled(bool value) = 0;
	virtual void Update(float timestep) = 0; // streaming (music)
	virtual boost::shared_ptr<const ISound> GetMusic() const = 0;
};