/**
	GRID OBJECT with A-Star implementation of PATH FINDING inside of
**/

#include "c_client.hpp"
#include "c_entity.hpp"

#include <iostream>
#include <fstream>

namespace client
{
	class Grid:
		public Entity,
		public INavigation
	{
		static const int Density = 96;

	protected:
		struct node_t
		{
			node_t(const point_t& point, const point_t& _to, const node_t* _from): point(point)
			{
				const point_t mFrom = _from ? _from->point : point;
				const int nStep = _from ? (_from->step + 1) : 1;
				const int nScale = 10;

				weight[0] = (int)((_from ? _from->weight[0] : 0) + 
					sqrt(float((point.x - mFrom.x) * (point.x - mFrom.x) + (point.y - mFrom.y) * (point.y - mFrom.y))) * nScale);

				weight[1] = (int)(abs(point.x - _to.x) * nScale + abs(point.y - _to.y) * nScale);

				step = nStep;
			};

			inline int GetWeight() const { return weight[0] + weight[1]; };
			inline bool operator== (const node_t& other) const { return (point == other.point) && (step == other.step); };

			int weight[2];
			int step;
			point_t point;
		};

	public:
		Grid(const std::string& name, const vec3_t& position, const quat_t& rotation, const vec2_t& size)
			: Entity(name, position, rotation, vec3_t::One, color_t())
			, m_size(size)
		{
			DebugCallback = (DebugCallbackT)&Grid::OnDebug;
		}

		struct Route: std::list<vec3_t>, IRoute
		{
			float GetLength() const
			{
				float result = 0;
				for (const_iterator a = begin(), b = ++begin(); a != end() && b != end(); result += (*a++ - *b++).Length());
				return result;
			}

			const vec3_t& GetWaypoint() const { return size() == 0 ? vec3_t::Zero : *begin(); }
			const vec3_t& GetDestination() const { return size() == 0 ? vec3_t::Zero : *--end(); }

			bool GoToNextWaypoint()
			{
				if (size() > 1)
				{
					pop_front();
					return true;
				}
				else
				{
					return false;
				}
			}
		};

		bool IsReachable(const vec3_t& a_) const
		{
			const vec2_t
				a__ = vec2_t((a_ * mat4_t::Invert(GetMatrix())).v) * Density / m_size;
			const point_t a(int(a__.x), int(a__.y));
			((Grid*)this)->ComputeMap();
			return !GetState(a);
		}

		shared_ptr<IRoute> GetRoute(const vec3_t& a_, const vec3_t& b_, bool use_navigation)
		{
			if (!use_navigation) //create a simple point-to-point route
			{
				shared_ptr<Route> route(new Route);
				route->push_back(a_);
				route->push_back(b_);
				return route;
			}

			memset(m_map, 0, sizeof(m_map));

			const vec2_t
				a__ = vec2_t((a_ * mat4_t::Invert(GetMatrix())).v) * Density / m_size,
				b__ = vec2_t((b_ * mat4_t::Invert(GetMatrix())).v) * Density / m_size;

			const point_t a(int(a__.x), int(a__.y)), b(int(b__.x), int(b__.y));

			ComputeMap();

			if (!IsValidPoint(a) || !IsValidPoint(b))
			{
				return null_ptr();
			}

			SetStep(a, 1);

			shared_ptr<Route> route(new Route);

			std::list<node_t> route_ex;

			route_ex.push_back(node_t(a, b, 0));

			PushSteps(b, route_ex);
			PopStep(a, b, GetStep(b), route);

			if (!route->size())
			{
				return null_ptr();
			}

			if (route->size() == 1)
			{
				return route;
			}

			//std::reverse(route->begin(), route->end());

			for (bool relaxed = true; route->size() > 2 && relaxed;)
			{
				relaxed = false;
				for (Route::iterator first = route->begin(), lead = ++route->begin(), second = lead++; lead != route->end();)
				{
					if (IsRouteFree(*first, *lead))
					{
						second = (lead = route->erase(second))++;
						relaxed = true;
					}
					else
					{
						first = second;
						second = lead++;
					}
				}
			}

			std::reverse(route->begin(), route->end());

			route->front().x = a_.x, route->front().y = a_.y;
			route->back().x = b_.x, route->back().y = b_.y;

			return route;
		}
		
		void OnDebug(IHelper* helper) const
		{
			helper->Line(vec3_t(0,0,0), vec3_t(m_size.x,0,0), color_t(), &GetMatrix());
			helper->Line(vec3_t(0,0,0), vec3_t(0,m_size.y,0), color_t(), &GetMatrix());
			helper->Line(vec3_t(m_size.x,m_size.y,0), vec3_t(m_size.x,0,0), color_t(), &GetMatrix());
			helper->Line(vec3_t(m_size.x,m_size.y,0), vec3_t(0,m_size.y,0), color_t(), &GetMatrix());
		}
		
		void DrawRoute(shared_ptr<IHelper> helper, const Route& route) const
		{
			const vec3_t* _point = 0;

			BOOST_FOREACH(const vec3_t& point, route)
			{
				if (_point)
				{
					helper->Line(*_point, point, color_t(0,1,0), 0);
				}

				_point = &point;
			}
		}

	protected:

		bool IsRouteFree(const vec2_t& a, const vec2_t& b) const
		{
			return IsRouteFree(vec3_t(a.x, a.y, 0), vec3_t(b.x, b.y, 0));
		}

		bool IsRouteFree(const vec3_t& a_, const vec3_t& b_) const
		{
			vec2_t 
				a = vec2_t((a_ * mat4_t::Invert(GetMatrix())).v) * Density / m_size,
				b = vec2_t((b_ * mat4_t::Invert(GetMatrix())).v) * Density / m_size;

			a.Assign((int)a.x + .5f, (int)a.y + .5f);
			b.Assign((int)b.x + .5f, (int)b.y + .5f);

			for (int i = 0, count = (int)MAX(fabs(a.x - b.x), fabs(a.y - b.y)); i < count; ++i)
			{
				const vec2_t point = vec2_t::Lerp((float)i / (float)count, a, b);

				if (GetState(point_t((int)point.x, (int)point.y)) > 0)
				{
					return false;
				}
			}

			return true;
		}

		void ComputeMap()
		{
			//TODO: optimize this routine

			for (Entity::iterator it = GetRoot()->begin(); it != GetRoot()->end(); ++it)
			{
				if (!(*it)->GetFlags(FLAG_NOCLIP)) 
				{
					AddEntityToMap((*it).get());
				}
			}
		}

		void AddEntityToMap(const client::Entity* e)
		{
			box3_t box = e->GetBoundingBox();

			if ((box.vMax - box.vMin).Length() < math::EPS || !e->CanBeSeen())
			{
				return;
			}

			box.vMax.z = FLT_MAX;
			box.vMin.z = -FLT_MAX;

			const mat4_t transform =
				mat4_t::Invert(e->GetMatrix()) *
				GetMatrix() *
				mat4_t::Scaling(m_size.x / Density, m_size.y / Density, 0);

			for (int x = 0; x < Density; ++x)
			{
				for (int y = 0; y < Density; ++y)
				{
					if (box.Contains(vec3_t(x + 0.5f, y + 0.5f, 0) * transform))
					{
						SetState(point_t(x, y), 1);
					}
				}
			}
		}

		void PushSteps(const point_t &target, std::list<node_t>& route)
		{
			const int 
				MaxIterations = 64 * 64,
				MaxSteps = 255,
				sides[8][2] = { {1,0}, {0,1}, {-1,0}, {0,-1}, {1,1}, {1,-1}, {-1,1}, {-1,-1} };

			for (int n = 0; n < MaxIterations; n++)
			{
				if (route.size() <= 0)
				{
					return;
				}

				const node_t& node = *route.begin();

				if (node.step >= MaxSteps)
				{
					return;
				}

				for (int i = 0; i < 8; i++)
				{
					const point_t point(node.point.x + sides[i][0], node.point.y + sides[i][1]);

					if (!GetState(point) && !GetStep(point))
					{
						node_t tmp(point, target, &node);

						for (std::list<node_t>::iterator it = route.begin(); it != route.end(); it++)
						{
							if ((*it).GetWeight() > tmp.GetWeight())
							{
								route.insert(it, tmp);

								goto node_has_been_added;
							}
						}

						route.push_back(tmp);

node_has_been_added:

						SetStep(point, node.step + 1);

						if ((int)point.x == (int)target.x && (int)point.y == (int)target.y)
						{
							return;
						}
					}
				}

				route.remove(node);
			}
		}

		bool PopStep(const point_t& target, const point_t& point, BYTE value, shared_ptr<Route> route) const
		{
			if (!IsValidPoint(point) || GetStep(point) != value || value == 0)
			{
				return false;
			}

			route->push_back(vec3_t(point.x * m_size.x / Density, point.y * m_size.y / Density, 0) * GetMatrix());

			if ((int)point.x == (int)target.x && (int)point.y == (int)target.y)
			{
				return true;
			}

			const int offset[8][3] = { {1,0}, {0,1}, {-1,0}, {0,-1}, {1,1}, {1,-1}, {-1,1}, {-1,-1} };

			for (int n = 0; n < 8; n++)
			{
				if (PopStep(target, point_t(point.x + offset[n][0], point.y + offset[n][1]), value - 1, route))
				{
					return true;
				}
			}

			return false; //TODO: deal with an error
		}

		inline bool IsValidPoint(const point_t& point) const
		{
			return rect_t(0, 0, Density, Density).Contains(point);
		}

		inline BYTE GetState(const point_t& point) const
		{
			return IsValidPoint(point) ? m_map[point.x][point.y].state : 1;
		}

		inline void SetState(const point_t& point, BYTE value)
		{
			if (IsValidPoint(point))
			{
				m_map[point.x][point.y].state = value;
			}
		}

		inline BYTE GetStep(const point_t& point) const
		{
			return IsValidPoint(point) ? m_map[point.x][point.y].step : 0;
		}

		inline void SetStep(const point_t& point, BYTE value)
		{
			if (IsValidPoint(point))
			{
				m_map[point.x][point.y].step = value;
			}
		}

	protected:
		vec2_t m_size;
		struct { BYTE state, step; } m_map[Density][Density];
	};
}

void export_Grid()
{
	namespace py = boost::python;

	using client::Grid;

	DISPOSEME(4) py::class_<IRoute, shared_ptr<IRoute>, boost::noncopyable>("Route", py::no_init)
		.def("GotoNext", &IRoute::GoToNextWaypoint)
		.add_property("length", &IRoute::GetLength)
		.add_property("point", py::make_function(&IRoute::GetWaypoint, py::return_value_policy<py::copy_const_reference>()))
		.add_property("destination", py::make_function(&IRoute::GetDestination, py::return_value_policy<py::copy_const_reference>()))
		;

	DISPOSEME(4) py::class_<INavigation, shared_ptr<INavigation>, boost::noncopyable>("Navigation", py::no_init)
		.def("GetRoute", &Grid::GetRoute)
		.def("DrawRoute", &Grid::DrawRoute)
		.def("IsReachable", &Grid::IsReachable)
		;
		
	DISPOSEME(4) py::class_<Grid, shared_ptr<Grid>, py::bases<client::Entity, INavigation>, boost::noncopyable>("Grid", 
		py::init<std::string, vec3_t, quat_t, vec2_t>((
			py::arg("id") = std::string("Grid#"),
			py::arg("position") = vec3_t::Zero,
			py::arg("rotation") = quat_t::Identity,
			py::arg("size") = vec2_t(512, 512))))
		;

	py::implicitly_convertible<shared_ptr<Grid>, shared_ptr<client::Entity> >();
	py::implicitly_convertible<shared_ptr<Grid>, shared_ptr<INavigation> >();
}
