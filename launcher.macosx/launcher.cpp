#include <iostream>

#include "launcher.h"
#include "window.h"

#include "../launcher/timer.hpp"
#include "../launcher/framework.hpp"
#include "../launcher/mycout.hpp"

#include "../interfaces/iclient.hpp"
#include "../interfaces/iserver.hpp"
#include "../interfaces/ifilesystem.hpp"
#include "../interfaces/isoundsystem.hpp"

extern IFileSystem* CreateFileSystem(IFramework*);
extern ISoundSystem* CreateSoundSystem(IClientFramework*);
extern IRenderer* CreateRenderer(IClientFramework*);
extern IClient* CreateClient(int argc, char **argv, const char*, const char*, IClientFramework*, IRenderer*);


IClientFramework* clientframework = 0;
IClient* client = 0;
IRenderer* renderer = 0;

enum AppFlags {
	APPFLAG_CONSOLE = 1 << 0,
	APPFLAG_ERROR = 1 << 1,
	APPFLAG_WINDOW = 1 << 2,
	APPFLAG_DEVWINDOW = 1 << 3,
	APPFLAG_ALLOWENVPYTHON = 1 << 4,
	APPFLAG_RIGHTWINDOW = 1 << 5,
};

int g_AppFlags = 0;

extern char documentsDirectoryC[512];

void SetErrorState()
{
	g_AppFlags |= APPFLAG_ERROR;
}

int RenderGameFrame()
{
	if (!client)
		return 0;
	return (int)client->RenderFrame();
}

int flag_fullscreen = 1;
bool b_tutorial = true;
bool b_sound = true;
bool b_music = true;

bool set_fullscreen(bool fulscreen)
{
	extern int screen_x, screen_y, desktop_width, desktop_height;
	
	if (flag_fullscreen == (int)fulscreen)
	{
		return 0;
	}
	else if (fulscreen > 0)
	{
		screen_x = desktop_width;
		screen_y = desktop_height;
		
		flag_fullscreen = true;
		
		return 1;
	}
	else
	{
		screen_x = 1024;
		screen_y = 768;
		
		flag_fullscreen = false;
		
		return 1;
	}
}

bool is_fullscreen()
{
	return (bool)flag_fullscreen;
}

void RunGame()
{
	if (FILE* f = fopen((std::string(documentsDirectoryC) + "/options.dat").c_str(), "rb")) {
		bool b_fullscreen = true;
		//_mkdir("savedata");
		fread(&b_fullscreen, 1, 1, f);
		fread(&b_tutorial, 1, 1, f);
		fread(&b_sound, 1, 1, f);
		fread(&b_music, 1, 1, f);
		fclose(f);
		
        //b_fullscreen = false;
        
		flag_fullscreen = (int)b_fullscreen;
	}
	
	try
	{
		ClientFramework c_framework;

		clientframework = &c_framework;

		c_framework.FileSystem.reset(CreateFileSystem(&c_framework));
		c_framework.Timer.reset(CreateTimer());
		c_framework.SoundSystem.reset(CreateSoundSystem(&c_framework));
		c_framework.Variables.reset(CreateVariables());
		c_framework.Window.reset(new VideoWindow());

		c_framework.SoundSystem->SetSoundEnabled(b_sound);
		c_framework.SoundSystem->SetMusicEnabled(b_music);
		
		c_framework.GetVariables()->SetVariable("opt_tutorial", b_tutorial ? "1" : "0");
		
		if ((renderer = CreateRenderer(&c_framework)))
		{
			if ((client = CreateClient(0, 0, ".", documentsDirectoryC, &c_framework, renderer)))
			{
				std::cout << "Starting client" << std::endl;

				client->Run();
				
				if (FILE* f = fopen((std::string(documentsDirectoryC) + "/options.dat").c_str(), "wb")) {
					bool b_fullscreen = renderer->IsFullscreen();
					b_tutorial = c_framework.GetVariables()->GetBoolean("opt_tutorial");
					b_sound = c_framework.GetSoundSystem()->IsSoundEnabled();
					b_music = c_framework.GetSoundSystem()->IsMusicEnabled();
					fwrite(&b_fullscreen, 1, 1, f);
					fwrite(&b_tutorial, 1, 1, f);
					fwrite(&b_sound, 1, 1, f);
					fwrite(&b_music, 1, 1, f);
					fclose(f);
				}
				IClient *tmp = client;

				client = NULL;
				
				delete tmp;
			}
		}
	}
	catch (const std::exception& ex)
	{
		std::cout << ex.what() << std::endl;
	}
}