#include "c_client.hpp"

#include <boost/random.hpp>

namespace client
{

#ifndef WIN32
#	include <sys/time.h>
	int GetSeed()
	{
		struct timeval tv;
		gettimeofday(&tv, 0);
		return (tv.tv_sec * 1000 + tv.tv_usec / 1000);
	}
#endif

	class Random
	{
		boost::mt19937 gen;
		boost::uniform_int<int> dst;
		boost::variate_generator< boost::mt19937, boost::uniform_int<int> > rand;

	public:
		Random()
#ifdef WIN32
			: gen(static_cast<unsigned long>(GetTickCount()))
#else
			: gen(static_cast<unsigned long>(GetSeed()))
#endif
			, dst(0, RAND_MAX)
			, rand(gen, dst)
		{
		}

		Random(unsigned long seed) 
			: gen(seed)
			, dst(0, RAND_MAX)
			, rand(gen, dst)
		{
		}

		double Next()
		{
			return rand() * (1.0 / (RAND_MAX + 1.0));
		}

		int Next(int hi)
		{
			return static_cast<unsigned>(Next() * hi);
		}

		int Next(int lo, int hi)
		{
			return lo + Next(hi - lo);
		}

		boost::python::object Next(boost::python::list list) 
		{
			return boost::python::len(list) > 0 ? list[Next(boost::python::len(list))] : boost::python::object();
		}
	};
}


void export_Random()
{
	namespace py = boost::python;

	using client::Random;

	DISPOSEME(4) py::class_<Random>("Random")
		.def(py::init<unsigned long>((py::arg("seed"))))
		.def("__call__", (double (Random::*)())&Random::Next)
		.def("__call__", (int (Random::*)(int))&Random::Next)
		.def("__call__", (int (Random::*)(int, int))&Random::Next)
		.def("__call__", (py::object (Random::*)(py::list))&Random::Next)
		;
}
