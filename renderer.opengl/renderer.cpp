#include "renderer.hpp"

#include <string.h>

const std::string diffuse_vertex_shader =
	"attribute vec4 Position;\n"
	"attribute vec4 Color;\n"
	"attribute vec2 TexCoord0;\n"
	"uniform mat4 ModelViewProjectionMatrix;\n"
	"varying lowp vec4 _Color;\n"
	"varying lowp vec2 _TexCoord0;\n"
	"void vs_main() {\n"
	"	gl_Position = ModelViewProjectionMatrix * Position;\n"
	"	_TexCoord0 = TexCoord0;\n"
	"	_Color = Color;\n"
	"}\n";

const std::string diffuse_pixel_shader =
	"uniform sampler2D MainTexture;\n"
	"varying lowp vec4 _Color;\n"
	"varying lowp vec2 _TexCoord0;\n"
	"void ps_main() {\n"
	"	gl_FragColor = texture2D(MainTexture, _TexCoord0) * _Color;\n"
	"}\n";

const std::string diffuse_alphatest_pixel_shader =
	"uniform sampler2D MainTexture;\n"
	"uniform lowp float AlphaReference;\n"
	"varying lowp vec4 _Color;\n"
	"varying lowp vec2 _TexCoord0;\n"
	"void ps_main() {\n"
	"	lowp vec4 color = texture2D(MainTexture, _TexCoord0) * _Color;\n"
	"	if (color.a > AlphaReference) {\n"
	"		gl_FragColor = color;\n"
	"	} else {\n"
	"		discard;\n"
	"	}\n"
	"}\n";

GLuint diffuse_program = 0;
GLuint diffuse_alphatest_program = 0;

#define BUFFER_SIZE 4096
#define MAX_ELEMS 4096 << 2

extern int screen_x, screen_y;

bool set_fullscreen(bool fulscreen);
bool is_fullscreen();

static IClientFramework* framework;
static IRenderer* renderer;

bool bGottenVariables = false;
GLfloat fAnisoLargest = 0;

enum VertexAttributes
{
	ATTR_POSITION = 0,
	ATTR_COLOR,
	ATTR_NORMAL,
	ATTR_TEXCOORD0,
	ATTR_BLENDWEIGHT,
	ATTR_BLENDINDICES,
	ATTR_COUNT,
};

DWORD Log2(DWORD val);

const char* render_name = "OpenGL";

class Renderer: public IRenderer
{
private:
	int frame_num, triangles, drawcalls;
	bool active;
	vertex_t input[BUFFER_SIZE];

	mat4_t mat_modelview, mat_projection, mat_modelviewprojection;
#ifdef WIN32
	HDC hDC;
#endif

public:

	Renderer()
		: frame_num(0)
		, triangles(0)
		, drawcalls(0)
		, active(true)
	{
	}

	virtual ~Renderer()
	{
		if (diffuse_program) glDeleteProgram(diffuse_program);
		if (diffuse_alphatest_program) glDeleteProgram(diffuse_alphatest_program);
	}

#ifdef WIN32
	virtual bool Open(IWindow* window)
	{
		HGLRC hRC;

		static PIXELFORMATDESCRIPTOR pfd =
		{
			sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
			1,											// Version Number
			PFD_DRAW_TO_WINDOW |						// Format Must Support Window
			PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
			PFD_DOUBLEBUFFER,							// Must Support Double Buffering
			PFD_TYPE_RGBA,								// Request An RGBA Format
			32,											// Select Our Color Depth
			0, 0, 0, 0, 0, 0,							// Color Bits Ignored
			0,											// No Alpha Buffer
			0,											// Shift Bit Ignored
			0,											// No Accumulation Buffer
			0, 0, 0, 0,									// Accumulation Bits Ignored
			24,											// 24Bit Z-Buffer (Depth Buffer)
			8,											// 8Bit Stencil Buffer
			0,											// No Auxiliary Buffer
			PFD_MAIN_PLANE,								// Main Drawing Layer
			0,											// Reserved
			0, 0, 0										// Layer Masks Ignored
		};

		hDC = window->GetDC();

		if (!hDC)
		{
			throw std::runtime_error("Can't create an OpenGL device context");
		}

		GLuint PixelFormat = ChoosePixelFormat(hDC,&pfd);

		if (!PixelFormat)
		{
			throw std::runtime_error("Can't find a suitable pixel format");
		}

		if(!SetPixelFormat(hDC,PixelFormat,&pfd))
		{
			throw std::runtime_error("Can't set the pixel format");
		}

		if (!(hRC=wglCreateContext(hDC)))
		{
			throw std::runtime_error("Can't create an OpenGL rendering context");
		}

		if(!wglMakeCurrent(hDC,hRC))
		{
			throw std::runtime_error("Can't activate an OpenGL rendering context");
		}

		if(wglSwapIntervalEXT)
		{
			wglSwapIntervalEXT(false);
		}

		framework->Log(IFramework::MSG_LOG, "OpenGL renderer initialized");

		return true;
	}
#endif

	virtual void OnActivate(bool active)
	{
		this->active = active;
	}

	virtual void TakeScreenshot()
	{
	}

	virtual void SetViewport(const rect_t &rect)
	{
#ifdef WIN32
		glViewport(rect.GetPosition().x,
				   framework->GetWindow()->GetSize().height - rect.GetPosition().y - rect.GetSize().height,
				   rect.GetSize().width,
				   rect.GetSize().height);
#else
		glViewport(rect.GetPosition().x,
				   screen_y - rect.GetPosition().y - rect.GetSize().height,
				   rect.GetSize().width,
				   rect.GetSize().height);
#endif
	}

	virtual rect_t GetViewport()
	{
		rect_t r;

		glGetIntegerv(GL_VIEWPORT, r.v);

		return r;
	}

	virtual void SetMatrix(MatrixType type, const float *matrix)
	{
		switch (type)
		{
		case MATRIX_PROJECTION:
			mat_projection = mat4_t(matrix);
			mat_modelviewprojection = mat_projection * mat_modelview;
			break;
		case MATRIX_MODELVIEW:
			mat_modelview = mat4_t(matrix);
			mat_modelviewprojection = mat_projection * mat_modelview;
			break;
		case MATRIX_TEXTURE:
			break;
		}
	}

	virtual void GetMatrix(MatrixType type, float *matrix)
	{
		switch (type)
		{
		case MATRIX_PROJECTION:
			memcpy(matrix, mat_projection.m, sizeof(mat4_t));
			break;
		case MATRIX_MODELVIEW:
			memcpy(matrix, mat_modelview.m, sizeof(mat4_t));
			break;
		case MATRIX_TEXTURE:
			break;
		}
	}

	virtual void PushSurface(const ISurface *s)
	{
		if (s->GetVerticesCount() > BUFFER_SIZE)
		{
			return; // FIXME: what happens if that happens
		}

		if (s->GetIndicesCount() > MAX_ELEMS)
		{
			return; // ???
		}

		if (s->GetVerticesCount() == 0)
		{
			return;
		}

		const IMaterial* material = s->GetMaterial();

		SetupCullFace(material);
		SetupDepthFunc(material);
		SetupColorMask(material);
		SetupStencil(material);
		SetupAlphaFunc(s, material);
		SetupPointers(s);
		SetupShader(s);

		for (int i = 0; i < (material ? material->GetLayersCount() : 1); ++i)
		{
			bool multitexturing = material &&
				!(material->surfaceflags & IMaterial::SF_NO_MULTITEXTURE) &&
				i < material->GetLayersCount() - 1;
			SetupBlending(s, material ? material->GetLayer(i) : NULL);
			SetupTexture(s, material ? material->GetLayer(i) : NULL, 0);
			if (multitexturing)
			{
				SetupTexture(s, material->GetLayer(++i), 1);
				DrawSurface(s);
				SetupTexture(s, NULL, 1);
			}
			else
			{
				DrawSurface(s);
			}
		}

	}

	void SetupColorMask(const IMaterial* m)
	{
		if (m == NULL || m->colormask)
		{
			glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_FALSE);
		}
		else
		{
			glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		}
	}

	void SetupStencil(const IMaterial* m)
	{
		if (m && m->stencil.front.func != IMaterial::FUNC_UNKNOWN)
		{
			glEnable(GL_STENCIL_TEST);

			GLenum func = GL_ALWAYS;
			GLenum fail = GL_KEEP;
			GLenum pass = GL_KEEP;

			switch (m->stencil.front.func)
			{
				case IMaterial::FUNC_ALWAYS: func = GL_ALWAYS; break;
				case IMaterial::FUNC_EQUAL: func = GL_EQUAL; break;
				case IMaterial::FUNC_NOTEQUAL: func = GL_NOTEQUAL; break;
				case IMaterial::FUNC_LESS: func = GL_LESS; break;
				case IMaterial::FUNC_GREATER: func = GL_GREATER; break;
			}

			switch (m->stencil.front.pass)
			{
				case IMaterial::OP_KEEP: pass = GL_KEEP; break;
				case IMaterial::OP_ZERO: pass = GL_ZERO; break;
				case IMaterial::OP_INCR: pass = GL_INCR; break;
				case IMaterial::OP_DECR: pass = GL_DECR; break;
				case IMaterial::OP_REPLACE: pass = GL_REPLACE; break;
			}

			switch (m->stencil.front.fail)
			{
				case IMaterial::OP_KEEP: fail = GL_KEEP; break;
				case IMaterial::OP_ZERO: fail = GL_ZERO; break;
				case IMaterial::OP_INCR: fail = GL_INCR; break;
				case IMaterial::OP_DECR: fail = GL_DECR; break;
				case IMaterial::OP_REPLACE: fail = GL_REPLACE; break;
			}

			glStencilFunc(func, m->stencil.front.ref, 0xFF);
			glStencilOp(GL_KEEP, fail, pass);
			glStencilMask(0xFF);
		}
		else
		{
			glDisable(GL_STENCIL_TEST);
			//glStencilMask(GL_FALSE);
		}
	}

	void SetupCullFace(const IMaterial* m)
	{
		switch (m ? m->cullface : IMaterial::CULL_BACK)
		{
		case IMaterial::CULL_BACK:
			glEnable(GL_CULL_FACE);
			glCullFace(GL_FRONT);
			break;
		case IMaterial::CULL_FRONT:
			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			break;
		default:
			glDisable(GL_CULL_FACE);
		}
	}

	void SetupDepthFunc(const IMaterial* m)
	{
		if (m && m->depthfunc.func)
		{
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(ConvertFunction(m->depthfunc.func));
		}
		else
		{
			glEnable(GL_DEPTH_TEST);
			glDepthFunc(GL_LEQUAL);
		}

		glDepthMask(m && !m->depthfunc.mask ? GL_FALSE : GL_TRUE);
	}

	inline void SetupAlphaFunc(const ISurface* surface, const IMaterial* material)
	{
#ifndef GL_ES_VERSION_2_0
		if (material && material->alphafunc.func)
		{
			glEnable(GL_ALPHA_TEST);
			glAlphaFunc(ConvertFunction(material->alphafunc.func),
				material->alphafunc.ref * surface->alphareference / 255.0f);
		}
		else
		{
			glDisable(GL_ALPHA_TEST);
		}
#endif
	}

	inline GLuint ConvertFunction(IMaterial::Func func)
	{
		switch (func)
		{
		case IMaterial::FUNC_UNKNOWN: return GL_ALWAYS;
		case IMaterial::FUNC_NEVER: return GL_NEVER;
		case IMaterial::FUNC_LESS: return GL_LESS;
		case IMaterial::FUNC_EQUAL: return GL_EQUAL;
		case IMaterial::FUNC_LESSEQUAL: return GL_LEQUAL;
		case IMaterial::FUNC_GREATER: return GL_GREATER;
		case IMaterial::FUNC_NOTEQUAL: return GL_NOTEQUAL;
		case IMaterial::FUNC_GREATEREQUAL: return GL_GEQUAL;
		case IMaterial::FUNC_ALWAYS: return GL_ALWAYS;
		};
	}

	inline GLuint ConvertBlending(ILayer::Blend blend)
	{
		switch (blend)
		{
		case ILayer::BLEND_UNKNOWN: return GL_ONE;
		case ILayer::BLEND_ZERO: return GL_ZERO;
		case ILayer::BLEND_ONE: return GL_ONE;
		case ILayer::BLEND_SRCCOLOR: return GL_SRC_COLOR;
		case ILayer::BLEND_INVSRCCOLOR: return GL_ONE_MINUS_SRC_COLOR;
		case ILayer::BLEND_SRCALPHA: return GL_SRC_ALPHA;
		case ILayer::BLEND_INVSRCALPHA: return GL_ONE_MINUS_SRC_ALPHA;
		case ILayer::BLEND_DESTALPHA: return GL_DST_ALPHA;
		case ILayer::BLEND_INVDESTALPHA: return GL_ONE_MINUS_DST_ALPHA;
		case ILayer::BLEND_DESTCOLOR: return GL_DST_COLOR;
		case ILayer::BLEND_INVDESTCOLOR: return GL_ONE_MINUS_DST_COLOR;
		case ILayer::BLEND_SRCALPHASAT: return GL_SRC_ALPHA_SATURATE;
		case ILayer::BLEND_BOTHSRCALPHA: return GL_SRC_ALPHA;
		case ILayer::BLEND_BOTHINVSRCALPHA: return GL_ONE_MINUS_SRC_ALPHA;
		}
	}

	void SetupBlending(const ISurface* surface, const ILayer* layer)
	{
		if (!layer || surface->color.a < 255)
		{
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		}
		else if (layer->blend.src != ILayer::BLEND_ONE || layer->blend.dst != ILayer::BLEND_ZERO)
		{
			glEnable(GL_BLEND);
			glBlendFunc(ConvertBlending(layer->blend.src), ConvertBlending(layer->blend.dst));
		}
		else
		{
			glDisable(GL_BLEND);
		}

		/*
		switch (layer.GetBlend())
		{
			case ILayer::BLEND_UNKNOWN:
				glDisable(GL_BLEND);
				glDisable(GL_ALPHA_TEST);
				break;
			case ILayer::BLEND_MIX:
				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glAlphaFunc(GL_GEQUAL, 1.0f / 255.0f);
				break;
			case ILayer::BLEND_ADD:
				glBlendFunc(GL_SRC_ALPHA, GL_ONE);
				glAlphaFunc(GL_GEQUAL, 1.0f / 255.0f);
				break;
			case ILayer::BLEND_MODULATE:
				glBlendFunc(GL_SRC_COLOR, GL_ZERO);
				glDisable(GL_ALPHA_TEST);
				break;
			case IMaterial::ALPHA_TEST:
				glDisable(GL_BLEND);
				glAlphaFunc(GL_GEQUAL, 0.5f);
				break;
			case ILayer::BLEND_LIGHT:
				glDisable(GL_ALPHA_TEST);
				glBlendFunc(GL_SRC_COLOR, GL_ONE);
				break;
		}*/
	}

	void SetupTexture(const ISurface* surface, const ILayer* layer, int unit)
	{
		const ITexture* texture = 0;

		//glActiveTextureARB(unit ? GL_TEXTURE1_ARB : GL_TEXTURE0_ARB);

		if (layer)
		{
			switch (layer->type)
			{
			case ILayer::TYPE_LIGHTMAP:
				texture = surface->GetLightmap();
				break;
			default:
				if (layer->GetTexturesCount() == 0) {
					return;
				}
				texture = layer->GetTexture(floor(surface->time * layer->anim_speed));
			}
		}

		if (texture && texture->render_info)
		{
			glBindTexture(GL_TEXTURE_2D, ((render_info_t*)texture->render_info)->texture_id);

			switch (layer->filter.mag)
			{
			case ILayer::FILTER_NONE:
			case ILayer::FILTER_POINT:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				break;
			case ILayer::FILTER_LINEAR:
			case ILayer::FILTER_ANISOTROPIC:
			case ILayer::FILTER_FLATCUBIC:
			case ILayer::FILTER_GAUSSIANCUBIC:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				break;
			}

			switch (layer->filter.min)
			{
			case ILayer::FILTER_NONE:
			case ILayer::FILTER_POINT:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				break;
			case ILayer::FILTER_LINEAR:
			case ILayer::FILTER_ANISOTROPIC:
			case ILayer::FILTER_FLATCUBIC:
			case ILayer::FILTER_GAUSSIANCUBIC:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				break;
			}

			switch (layer->address)
			{
			case ILayer::ADDRESS_CLAMP:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
				break;
			case ILayer::ADDRESS_REPEAT:
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				break;
			}

			switch (layer->filter.mip)
			{
			case ILayer::FILTER_ANISOTROPIC:
				if (fAnisoLargest > 0)
				{
					glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, fAnisoLargest);
				}
				break;
			default:
				glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 0);
			}
		}
		else
		{
			glBindTexture(GL_TEXTURE_2D, 0);
		}
	}

	void SetupSh   ader(const ISurface* s)
	{
		const IMaterial* material = s->GetMaterial();
#ifdef GL_ES_VERSION_2_0
		GLuint program = (material && material->alphafunc.func) ? diffuse_alphatest_program : diffuse_program;
#else
		GLuint program = diffuse_program;
#endif

		if (material && material->GetShader())
		{
			program = (GLuint)material->GetShader()->program;
		}

		glUseProgram(program);

		GLint location_ModelViewProjectionMatrix = glGetUniformLocation(program, "ModelViewProjectionMatrix");
		GLint location_ModelViewMatrix = glGetUniformLocation(program, "ModelViewMatrix");

		if (s->GetMatrix())
		{
			mat4_t tmp1 = mat_modelviewprojection * *s->GetMatrix();
			mat4_t tmp2 = mat_modelview * *s->GetMatrix();
			glUniformMatrix4fv(location_ModelViewProjectionMatrix, 1, false, (const GLfloat*)&tmp1);
			glUniformMatrix4fv(location_ModelViewMatrix, 1, false, (const GLfloat*)&tmp2);
		}
		else
		{
			glUniformMatrix4fv(location_ModelViewProjectionMatrix, 1, false, (const GLfloat*)&mat_modelviewprojection);
			glUniformMatrix4fv(location_ModelViewMatrix, 1, false, (const GLfloat*)&mat_modelview);
		}

		glUniform1i(glGetUniformLocation(program, "Texture0"), 0);
		glUniform1i(glGetUniformLocation(program, "Texture1"), 1);

#ifdef GL_ES_VERSION_2_0
		if (material)
		{
			if (GLint location = glGetUniformLocation(program, "AlphaReference"))
			{
				glUniform1f(location, material->alphafunc.ref * s->alphareference / 255.0f);
			}
		}
#endif
		if (s->GetVariablesCount() > 0)
		{
			const ISurface::variable_t *variable = s->GetVariables();
			for (int i = 0; i < s->GetVariablesCount(); ++i, ++variable)
			{
				GLint location = glGetUniformLocation(program, variable->name);

				if (location < 0)
					continue;

				switch (variable->vartype)
				{
#define PROCESS_VARTYPE(TYPE, TYPECAPITAL, OGLTYPE) \
					case ISurface::VARTYPE_##TYPECAPITAL: \
						glUniform##TYPE##v(location, variable->count, (const OGLTYPE*)variable->value); \
						break;
					PROCESS_VARTYPE(1i, INT, GLint)
					PROCESS_VARTYPE(1i, BOOL, GLint)
					PROCESS_VARTYPE(1f, FLOAT, GLfloat)
					PROCESS_VARTYPE(4f, VECTOR4, GLfloat)
#undef PROCESS_VARTYPE
					case ISurface::VARTYPE_MATRIX:
						glUniformMatrix4fv(location, variable->count, false, (const GLfloat*)variable->value);
						break;
				}

			}
		}
	}

	static const int MAX_VERTICES = 16000;
	color_t color_buffer[MAX_VERTICES];

	void SetupPointers(const ISurface* s)
	{
		vertex_t* i = this->input;

		if (s->GetVertexBuffer())
		{
			glBindBuffer(GL_ARRAY_BUFFER, (GLuint)s->GetVertexBuffer()->render_info);
			glVertexAttribPointer(ATTR_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(vertex_t), &i->color);
			i = 0;
		}
		else
		{
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			memcpy(input, s->GetVertices(), s->GetVerticesCount() * sizeof(vertex_t));
		}

		for (int n = 0; n < ATTR_COUNT; glEnableVertexAttribArray(n++));

		if (s->GetVertexBuffer() || s->GetVerticesCount() >= MAX_VERTICES)
		{
			glVertexAttribPointer(ATTR_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(vertex_t), &i->color);
		}
		else
		{
			color_t *dest = color_buffer;
			vertex_t *src = i;
			for (int n = 0; n < s->GetVerticesCount(); (dest++)->CopyBGRA((src++)->color), ++n);
			glVertexAttribPointer(ATTR_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(color_t), &color_buffer);
		}

		glVertexAttribPointer(ATTR_POSITION, 3, GL_FLOAT, GL_FALSE, sizeof(vertex_t), &i->position);
		glVertexAttribPointer(ATTR_NORMAL, 3, GL_FLOAT, GL_FALSE, sizeof(vertex_t), &i->normal);
		glVertexAttribPointer(ATTR_TEXCOORD0, 2, GL_FLOAT, GL_FALSE, sizeof(vertex_t), &i->uv);
		glVertexAttribPointer(ATTR_BLENDWEIGHT, 4, GL_FLOAT, GL_FALSE, sizeof(vertex_t), &i->blend_weight);
		glVertexAttribPointer(ATTR_BLENDINDICES, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(vertex_t), &i->blend_indices);
	}

	void DrawSurface(const ISurface* s)
	{
		const void* indices = s->GetIndices();

		if (s->GetIndexBuffer())
		{
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, (GLuint)s->GetIndexBuffer()->render_info);
			indices = 0;
		}
		else
		{
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		}

		if (s->GetIndicesCount() > 0)
		{
			switch (s->primitive)
			{
			case ISurface::TRIANGLES:
				glDrawElements(GL_TRIANGLES, s->GetIndicesCount(), GL_UNSIGNED_SHORT, indices);
				triangles += s->GetIndicesCount() / 3;
				break;
			case ISurface::LINES:
				glDrawElements(GL_LINES, s->GetIndicesCount(), GL_UNSIGNED_SHORT, indices);
				break;
			case ISurface::POINTS:
				glDrawElements(GL_POINTS, s->GetIndicesCount(), GL_UNSIGNED_SHORT, indices);
				break;
			}
		}
		else
		{
			switch (s->primitive)
			{
			case ISurface::TRIANGLES:
				glDrawArrays(GL_TRIANGLES, 0, s->GetVerticesCount());
				triangles += s->GetVerticesCount() / 3;
				break;
			case ISurface::LINES:
				glDrawArrays(GL_LINES, 0, s->GetVerticesCount());
				break;
			case ISurface::POINTS:
				glDrawArrays(GL_POINTS, 0, s->GetVerticesCount());
				break;
			}
		}

		++drawcalls;
	}

	bool CreateBuffer(IRendererBuffer* buffer)
	{
		if (!buffer)
			return false;

		void* mem = buffer->GetDataPointer();
		size_t size = buffer->GetSize();

		switch (buffer->type)
		{
		case IRendererBuffer::VERTEX_BUFFER:
			buffer->render_info = _CreateVertexBuffer(size);
			if (!buffer->render_info)
				return false;
			_FillVertexBuffer(buffer->render_info, mem, size);
			break;
		case IRendererBuffer::INDEX_BUFFER:
			buffer->render_info = _CreateIndexBuffer(size);
			if (!buffer->render_info)
				return false;
			_FillIndexBuffer(buffer->render_info, mem, size);
			break;
		default:
			return false;
		}

		return true;
	}

	void ReleaseBuffer(IRendererBuffer* buffer)
	{
		if (!buffer)
			return;

		switch (buffer->type)
		{
		case IRendererBuffer::VERTEX_BUFFER:
			_DeleteVertexBuffer(buffer->render_info);
			break;
		case IRendererBuffer::INDEX_BUFFER:
			_DeleteIndexBuffer(buffer->render_info);
			break;
		default:
			return;
		}

		buffer->render_info = NULL;
	}

	virtual GLuint _CreateVertexBuffer(size_t size)
	{
		GLuint buffer;;
		glGenBuffers(1, &buffer);
		return buffer;
	}

	virtual GLuint _CreateIndexBuffer(size_t size)
	{
		GLuint buffer;
		glGenBuffers(1, &buffer);
		return buffer;
	}

	virtual void _FillVertexBuffer(GLuint buffer, void *src, size_t size)
	{
		glBindBuffer(GL_ARRAY_BUFFER, buffer);
		glBufferData(GL_ARRAY_BUFFER, size, src, GL_STATIC_DRAW);
	}

	virtual void _FillIndexBuffer(GLuint buffer, void *src, size_t size)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, src, GL_STATIC_DRAW);
	}

	virtual void _DeleteVertexBuffer(GLuint vb)
	{
		glDeleteBuffers(1, &vb);
	}

	virtual void _DeleteIndexBuffer(GLuint ib)
	{
		glDeleteBuffers(1, &ib);
	}

	bool CreateTexture(ITexture* texture)
	{
		int format1 = GL_RGB;
		int bpp = 3;

		int width = (1 << Log2(texture->width));// >> output->GetPicMip();
		int height = (1 << Log2(texture->height));// >> output->GetPicMip();

		switch (texture->format)
		{
		case ITexture::FMT_A8: format1 = GL_ALPHA; bpp = 1; break;
		case ITexture::FMT_R8G8B8: format1 = GL_RGB; bpp = 3; break;
		case ITexture::FMT_A8R8G8B8: format1 = GL_RGBA; bpp = 4; break;
		default: bpp = 3;
		}

		texture->render_info = new render_info_t;

		glGenTextures(1, &((render_info_t*)texture->render_info)->texture_id);
		glBindTexture(GL_TEXTURE_2D, ((render_info_t*)texture->render_info)->texture_id);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glTexImage2D(GL_TEXTURE_2D, 0, format1, width, height, 0, format1, GL_UNSIGNED_BYTE, 0);

		return true;
	}

	bool CompileShader(IShader* shader)
	{
		return _CompileShader((GLuint)shader->vertex, (GLuint)shader->pixel, (GLuint*)&shader->program);
	}

	bool _CompileShader(GLuint shader_vertex, GLuint shader_pixel, GLuint* program)
	{
		GLuint handle = glCreateProgram();

		glAttachShader(handle, shader_vertex);
		glAttachShader(handle, shader_pixel);

		// Bind attribute locations
		// This needs to be done prior to linking

	    glBindAttribLocation(handle, ATTR_POSITION, "Position");
	    glBindAttribLocation(handle, ATTR_NORMAL, "Normal");
	    glBindAttribLocation(handle, ATTR_COLOR, "Color");
	    glBindAttribLocation(handle, ATTR_TEXCOORD0, "TexCoord0");
	    glBindAttribLocation(handle, ATTR_BLENDWEIGHT, "BlendWeight");
	    glBindAttribLocation(handle, ATTR_BLENDINDICES, "BlendIndices");

		glLinkProgram(handle);

		*program = handle;

		GLint status;
		glGetProgramiv(handle, GL_LINK_STATUS, &status);
		if (status == GL_FALSE) {
			int loglen;
			char logbuffer[1024];
			glGetProgramInfoLog(handle, sizeof(logbuffer), &loglen, logbuffer);
			framework->Log(IFramework::MSG_ERROR, "Shader Program: %s", logbuffer);
			return false;
		}

		return true;
	}

	bool LoadVertexShader(IShader* shader, const char *mem, int size)
	{
		return _LoadShader(mem, size, GL_VERTEX_SHADER, &shader->vertex);
	}

	bool LoadPixelShader(IShader* shader, const char *mem, int size)
	{
		return _LoadShader(mem, size, GL_FRAGMENT_SHADER, &shader->pixel);
	}

	dim_t GetBackBufferSize()
	{
#ifdef WIN32
		return dim_t(1024, 768);
#else
		return dim_t(screen_x, screen_y);
#endif
	}

	bool _LoadShader(const char *mem, int size, GLint type, GLuint* shader_handle)
	{
		const char *source[2] = {
			type == GL_VERTEX_SHADER ?
			"#define VERTEX_SHADER\n"
			"#define MATRIX_PALETTE_SIZE 29\n"
			"#define vs_main main\n"
#ifndef GL_ES_VERSION_2_0
			"#define lowp\n"
			"#define highp\n"
			"#define mediump\n"
#endif
			"uniform mat4 MatrixPalette[MATRIX_PALETTE_SIZE];\n"
			"attribute vec4 BlendWeight;\n"
			"attribute vec4 BlendIndices;\n"
			"vec4 vs_skin(vec4 in_vertex) {\n"
			"	vec4 out_vertex = ( MatrixPalette[int(BlendIndices.x * 255.0)] * in_vertex ) * BlendWeight.x;\n"
			"	if ( BlendWeight.y > 0.0 ) {\n"
			"		out_vertex += ( MatrixPalette[int(BlendIndices.y * 255.0)] * in_vertex ) * BlendWeight.y;\n"
			"		if ( BlendWeight.z > 0.0 ) {\n"
			"			out_vertex += ( MatrixPalette[int(BlendIndices.z * 255.0)] * in_vertex ) * BlendWeight.z;\n"
			"		}\n"
			"	}\n"
			"	out_vertex.w = in_vertex.w;\n"
			"	return out_vertex;\n"
			"}\n"
			:
			"#define PIXEL_SHADER\n"
			"#define ps_main main\n"
#ifndef GL_ES_VERSION_2_0
			"#define lowp\n"
			"#define highp\n"
			"#define mediump\n"
#endif
			, mem };

		const int length[2] = { (int)strlen(source[0]), size };

		GLuint handle = glCreateShader(type);
		glShaderSource(handle, 2, source, length);
		glCompileShader(handle);

		*shader_handle = handle;

		GLint status = GL_FALSE;
		int loglen;
		char logbuffer[1024];
		glGetShaderiv(handle, GL_COMPILE_STATUS, &status);

		if (status == GL_FALSE)
		{
			glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &loglen);
			glGetShaderInfoLog(handle, loglen, NULL, logbuffer);
			framework->Log(IFramework::MSG_ERROR, type == GL_VERTEX_SHADER ? "VS %s" : "PS %s", logbuffer);
			return false;
		}


		return true;
	}

	bool ReleaseShader(IShader* shader)
	{
		glDeleteProgram((GLuint)shader->program);
		glDeleteShader((GLuint)shader->vertex);
		glDeleteShader((GLuint)shader->pixel);
		return true;
	}

	bool ReleaseTexture(ITexture* texture)
	{
		if (texture->render_info) {
			glDeleteTextures(1, &((render_info_t*)texture->render_info)->texture_id);
			if (((render_info_t*)texture->render_info)->bitmap) {
				delete [] ((render_info_t*)texture->render_info)->bitmap;
			}
			delete (render_info_t*)texture->render_info;
		}
		return true;
	}

	bool LoadTexture(ITexture* texture, const char *mem, int size)
	{
		return LoadTextureFromMemory(texture, mem, size);
	}

	template<class T> void Swap(T &a, T &b) { T c = a; a = b; b = c; }

	virtual void* LockTexture(ITexture* texture)
	{
		int format = GL_RGB;
		int bpp = 3;

		switch (texture->format)
		{
		case ITexture::FMT_A8: format = GL_ALPHA; bpp = 1; break;
		case ITexture::FMT_R8G8B8: format = GL_RGB; bpp = 3; break;
		case ITexture::FMT_A8R8G8B8: format = GL_RGBA; bpp = 4; break;
		default: bpp = 3;
		}

		BYTE* ptr = ((render_info_t*)texture->render_info)->bitmap;

		for (int i = 0; i < texture->width * texture->height * bpp;) {
			Swap(ptr[0], ptr[2]);
			i += bpp, ptr += bpp;
		}

		if (texture->render_info) {
			return ((render_info_t*)texture->render_info)->bitmap;
		} else {
			return 0;
		}
	}

	virtual void UnlockTexture(ITexture* texture)
	{
		int format = GL_RGB;
		int bpp = 3;

		switch (texture->format)
		{
		case ITexture::FMT_A8: format = GL_ALPHA; bpp = 1; break;
		case ITexture::FMT_R8G8B8: format = GL_RGB; bpp = 3; break;
		case ITexture::FMT_A8R8G8B8: format = GL_RGBA; bpp = 4; break;
		default: bpp = 3;
		}

		BYTE* ptr = ((render_info_t*)texture->render_info)->bitmap;

		for (int i = 0; i < texture->width * texture->height * bpp;) {
			Swap(ptr[0], ptr[2]);
			i += bpp, ptr += bpp;
		}

		if (texture->miplevels == 1)
		{
			glBindTexture(GL_TEXTURE_2D, ((render_info_t*)texture->render_info)->texture_id);
			glTexImage2D(GL_TEXTURE_2D, 0, format, texture->width, texture->height, 0, format,
				GL_UNSIGNED_BYTE, ((render_info_t*)texture->render_info)->bitmap);
		}
		else
		{
#ifdef GL_ES_VERSION_2_0
			glBindTexture(GL_TEXTURE_2D, ((render_info_t*)texture->render_info)->texture_id);
			glTexImage2D(GL_TEXTURE_2D, 0, format, texture->width, texture->height, 0, format,
						 GL_UNSIGNED_BYTE, ((render_info_t*)texture->render_info)->bitmap);
			glGenerateMipmap(GL_TEXTURE_2D);
#else
			gluBuild2DMipmaps(GL_TEXTURE_2D, format, texture->width, texture->height, format, GL_UNSIGNED_BYTE, ((render_info_t*)texture->render_info)->bitmap);
#endif

		}
	}

//backbuffer operations
	virtual bool CopyBackBuffer(const ITexture* texture, const rect_t &rect)
	{
		if (!texture || !texture->render_info)
		{
			return false;
		}
#ifndef GL_ES_VERSION_2_0
		glReadBuffer(GL_BACK);
#else

#endif
		//framework->Log(IFramework::MSG_LOG, "copy buffer: %d %d %d %d", rect.left, rect.top, rect.GetSize().width, rect.GetSize().height);
		glBindTexture(GL_TEXTURE_2D, ((render_info_t*)texture->render_info)->texture_id);
#ifdef WIN32
		glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, rect.left, rect.top, rect.GetSize().width, rect.GetSize().height);
#else
		glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, rect.left, screen_y - rect.GetSize().height + rect.top, rect.GetSize().width, rect.GetSize().height);
#endif

		/*
		glColor3f(1, 1, 1);
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST), glDepthMask(GL_FALSE), glDepthFunc(GL_ALWAYS);
		glDisable(GL_STENCIL_TEST);
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);

		float w = (float)rect.GetSize().width / (float)texture->width;
		float h = (float)rect.GetSize().height / (float)texture->height;

		float[] quad = {
			-1, -1, 0,   0, h,
			-1,  1, 0,   0, 0,
			 1,  1, 0,   w, 0,

			 1,  1, 0,   w, 0,
			 1, -1, 0,   w, h,
			-1, -1, 0,   0, h,
		};

		glBegin(GL_QUADS);
			glTexCoord2f(0, h), glVertex2f(-1, -1);
			glTexCoord2f(0, 0), glVertex2f(-1,  1);
			glTexCoord2f(w, 0), glVertex2f( 1,  1);
			glTexCoord2f(w, h), glVertex2f( 1, -1);
		glEnd();

		glCopyTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, rect.left, rect.top, rect.GetSize().width, rect.GetSize().height);

		glBegin(GL_QUADS);
			glTexCoord2f(0, h), glVertex2f(-1, -1);
			glTexCoord2f(0, 0), glVertex2f(-1,  1);
			glTexCoord2f(w, 0), glVertex2f( 1,  1);
			glTexCoord2f(w, h), glVertex2f( 1, -1);
		glEnd();
		*/
		return true;
	}

	virtual ITexture::Format GetBackBufferFormat()
	{
		return ITexture::FMT_A8R8G8B8;
	}

// frame operations
	virtual void StartFrame()
	{
		if (!bGottenVariables)
		{
			//if (_GLEE_EXT_texture_filter_anisotropic)
			//{
			//	glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &fAnisoLargest);
			//}
			bGottenVariables = true;
		}

		if (!diffuse_program)
		{
			GLuint vs, ps;
			_LoadShader(diffuse_vertex_shader.c_str(), diffuse_vertex_shader.size(), GL_VERTEX_SHADER, &vs);
			_LoadShader(diffuse_pixel_shader.c_str(), diffuse_pixel_shader.size(), GL_FRAGMENT_SHADER, &ps);
			_CompileShader(vs, ps, &diffuse_program);
			glDeleteShader(vs);
			glDeleteShader(ps);
		}

		if (!diffuse_alphatest_program)
		{
			GLuint vs, ps;
			_LoadShader(diffuse_vertex_shader.c_str(), diffuse_vertex_shader.size(), GL_VERTEX_SHADER, &vs);
			_LoadShader(diffuse_alphatest_pixel_shader.c_str(), diffuse_alphatest_pixel_shader.size(), GL_FRAGMENT_SHADER, &ps);
			_CompileShader(vs, ps, &diffuse_alphatest_program);
			glDeleteShader(vs);
			glDeleteShader(ps);
		}

		int c = GetStats().default_color;

		glDepthMask(GL_TRUE);
		glStencilMask(0xFF);
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

		glClearColor(((BYTE*)(&c))[0] / 255.f, ((BYTE*)(&c))[1] / 255.f, ((BYTE*)(&c))[2] / 255.f, ((BYTE*)(&c))[3] / 255.f);
#ifdef GL_ES_VERSION_2_0
		glClearDepthf(GetStats().default_depth);
#else
		glClearDepth(GetStats().default_depth);
#endif
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

		++frame_num;
		triangles = drawcalls = 0;
	}

	virtual void FinishFrame()
	{
#ifdef WIN32
		SwapBuffers(hDC);
#elif defined __APPLE__
		//glFlush();
#else
		glXSwapBuffers(framework->GetWindow()->GetDisplay(), *framework->GetWindow()->GetWindow());
#endif
	}

	virtual void ClearScreen(int nMask, int c, float depth, int stencil)
	{
		glDepthMask(GL_TRUE);								// must be true before clearing!

		int mask = 0;

		if (nMask & IRenderer::STENCIL_BIT) mask |= GL_STENCIL_BUFFER_BIT;
		if (nMask & IRenderer::COLOR_BIT) mask |= GL_COLOR_BUFFER_BIT;
		if (nMask & IRenderer::DEPTH_BIT) mask |= GL_DEPTH_BUFFER_BIT;

		glClearColor(((BYTE*)(&c))[0] / 255.f, ((BYTE*)(&c))[1] / 255.f, ((BYTE*)(&c))[2] / 255.f, ((BYTE*)(&c))[3] / 255.f);

#ifdef GL_ES_VERSION_2_0
		glClearDepthf(depth);
#else
		glClearDepth(depth);
#endif

		glClearStencil(stencil);

		glClear(mask);
	}

	virtual const stats_t& GetStats()
	{
		static stats_t stats;

		stats.frame_num = frame_num;
		stats.triangles = triangles;
		stats.drawcalls = drawcalls;
		stats.memory = 0;
		stats.default_stencil = 0x80;
		stats.default_color = 0;//0xFF808080;
		stats.default_depth = 1.0f;
		stats.needs_ortho_shift = false;
		stats.needs_flip_copybackbuffer = true;
		stats.bone_index_stride = 1;

		stats.name = "opengl";
		stats.ps_ext = ".glsl";
		stats.vs_ext = ".glsl";

		return stats;
	}

	virtual bool SetFullscreen(bool fullscreen) {
#ifdef __APPLE__ || defined(__linux)
		return set_fullscreen(fullscreen);
#else
		return false;
#endif
	}

	virtual bool IsFullscreen() const {
#ifdef __APPLE__ || defined(__linux)
		return is_fullscreen();
#else
		return false;
#endif
	}
};

const char* GetRenderName()
{
	return render_name;
}

IRenderer *CreateRenderer(IClientFramework* _framework)
{
	framework = _framework;
	return renderer = new Renderer();
}
