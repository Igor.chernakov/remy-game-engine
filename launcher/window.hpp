#pragma once

#include "../interfaces/iclientframework.hpp"
#include "../interfaces/iclient.hpp"
#include "../interfaces/iwindow.hpp"

#include "../shared/types.hpp"

#include "resource.h"

#ifdef WIN32
#	include <windows.h>
#	define WM_POINTERUP 0x0247
#	define WM_POINTERUPDATE 0x0245
#	define WM_POINTERDOWN 0x0246
#endif

extern IClientFramework* framework;

class VideoWindow: public IWindow
{
	bool m_dev, m_fullscreen, m_right;
	dim_t m_size;
public:
	VideoWindow(const std::string& name, const dim_t& size, bool fullscreen, HINSTANCE hInst, bool is_dev, bool is_right)
		: m_originalproc(0)
		, m_dev(is_dev)
		, m_right(is_right)
		, m_fullscreen(fullscreen)
		, m_size(size)
	{
		WNDCLASSEX wndClass = { 0 };

		wndClass.hIcon = LoadIcon(hInst, MAKEINTRESOURCE(1));
		wndClass.hIconSm = LoadIcon(hInst, MAKEINTRESOURCE(1));
		wndClass.cbSize = sizeof(wndClass);
		wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
		wndClass.lpfnWndProc = VideoWindow::WndProc;
		wndClass.lpszClassName = "T_DIRECT3D";

		if (!m_fullscreen)
			wndClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);

		RegisterClassEx(&wndClass);

		this->hWnd = CreateWindowEx(
			0,
			"T_DIRECT3D",
			name.c_str(),
			//m_fullscreen ? WS_EX_TOPMOST | WS_POPUP : (
				m_dev ? (WS_POPUP|WS_VISIBLE|WS_SYSMENU) : 
					WS_OVERLAPPED|WS_CAPTION|WS_MINIMIZEBOX|WS_SYSMENU,//),
			0,
			0,
			size.width,
			size.height,
			NULL,
			NULL,
			hInst,
			NULL);

		this->hDC = ::GetDC(this->hWnd);

		//if (!m_fullscreen)
			Centralize();

		//SetWindowPos(hWnd, NULL, 0, 0, size.width, size.height, 0);

		Show(false);

		SetCursor(CURSOR_ARROW);
	}

	VideoWindow(HWND hwnd)
		: m_originalproc(GetWindowLong(hwnd, GWL_WNDPROC))
		, hWnd(hwnd)
	{
		//SetWindowLong(hWnd, GWL_WNDPROC, (LONG)VideoWindow::WndProc);
	}

	~VideoWindow()
	{
		DestroyWindow(hWnd);
	}

	bool IsFullscreen() const
	{
		return m_fullscreen;
	}

	void UnhookWindow()
	{
		if (m_originalproc)
		{
			//SetWindowLong(hWnd, GWL_WNDPROC, m_originalproc);
		}
	}

	void Show(bool show)
	{
		if (show)
		{
			::ShowWindow(hWnd, SW_SHOW);
		}
		else
		{
			::ShowWindow(hWnd, SW_HIDE);
		}

		::UpdateWindow(hWnd);
	}

	bool IsVisible() const
	{
		return false;
	}

	bool Centralize()
	{
		DEVMODE dmSettings;

		RECT cRect, rWindow;

		GetWindowRect(hWnd, &cRect);

		if(EnumDisplaySettings(NULL,ENUM_CURRENT_SETTINGS,&dmSettings))
		{
			if (m_right)
			{
				rWindow.left  = (dmSettings.dmPelsWidth - (cRect.right - cRect.left));
				rWindow.right	= (cRect.right - cRect.left) + rWindow.left;
			}
			else
			{
				rWindow.left  = (dmSettings.dmPelsWidth - (cRect.right - cRect.left)) / 2;
				rWindow.right	= (cRect.right - cRect.left) + rWindow.left;
			}
			rWindow.top = (dmSettings.dmPelsHeight - (cRect.bottom - cRect.top)) / 2;
			rWindow.bottom	= (cRect.bottom - cRect.top) + rWindow.top;
		}
		else
		{
			return false;
		}

		if (m_dev) {
			rWindow.bottom -= rWindow.top;
			rWindow.top = 0;
		}

		AdjustWindowRect( &rWindow, GetWindowLong(hWnd, GWL_STYLE), false);

		SetWindowPos(hWnd, NULL, rWindow.left, rWindow.top, rWindow.right  - rWindow.left, rWindow.bottom - rWindow.top, 0);

		return true;
	}

	HWND GetWindow() const
	{
		return this->hWnd;
	}

	HDC GetDC() const
	{
		return this->hDC;
	}

	dim_t GetSize() const
	{
		RECT r;

		GetClientRect(hWnd, &r);

		return dim_t(1024, 768);//dim_t(r.right - r.left, r.bottom - r.top);
	}

	point_t GetCursorPos() const
	{
		POINT _mouse;

		::GetCursorPos(&_mouse);

		extern IRenderer* renderer;

		if (renderer->IsFullscreen())
		{
			_mouse = MapCursorFrom(_mouse, renderer->GetBackBufferSize());
		}
		else
		{
			::ScreenToClient(this->hWnd, &_mouse);

			_mouse = MapCursorFrom(_mouse, renderer->GetBackBufferSize());
		}

		return point_t(_mouse.x, _mouse.y);
	}

	POINT MapCursorFrom(const POINT& _mouse, const dim_t& viewSize) const 
	{
		dim_t bb_size = viewSize;
		dim_t wn_size = GetSize();
		float bb_aspect = (float)bb_size.width / (float)bb_size.height;
		float wn_aspect = (float)wn_size.width / (float)wn_size.height;
		POINT mouse;
		mouse.y = _mouse.y * wn_size.height / bb_size.height;
		mouse.x = (_mouse.x - bb_size.height * (bb_aspect - wn_aspect) / 2) * wn_size.height / bb_size.height;
		return mouse;
	}

	void SetCursorPos(const point_t& pos)
	{
		POINT p;

		p.x = pos.x;

		p.y = pos.y;

		::ClientToScreen(this->hWnd, &p);

		::SetCursorPos(p.x, p.y);
	}

	void ShowCursor(bool show)
	{
		::ShowCursor(show);
	}

	void SetCursor(Cursor cursor)
	{
		this->m_cursor = cursor;

		switch (cursor)
		{
			case CURSOR_NONE:
				::SetCursor(LoadCursor(NULL,IDC_ARROW));
				::ShowCursor(false);
				break;
			case CURSOR_ARROW:
				::SetCursor(LoadCursor(NULL,IDC_ARROW));
				::ShowCursor(true);
				break;
			case CURSOR_HAND:
				::SetCursor(LoadCursor(NULL,IDC_HAND));
				::ShowCursor(true);
				break;
			case CURSOR_HOLD:
				::SetCursor(LoadCursor(NULL,IDC_HAND));
				::ShowCursor(true);
				break;
			case CURSOR_WAIT:
				::SetCursor(LoadCursor(NULL,IDC_WAIT));
				::ShowCursor(true);
				break;
		}
	}

	Cursor GetCursor() const
	{
		return m_cursor;
	}

private:

	static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
	{
		static RAWINPUTDEVICE rid;
		static BYTE keyboard[256] = { 0 };

		extern IRenderer* renderer;

		switch(msg)
		{
		case WM_CREATE:
			rid.dwFlags = /*RIDEV_NOLEGACY | */RIDEV_INPUTSINK;	// ignore legacy messages and receive system wide keystrokes	
			rid.usUsagePage = 1;							// raw keyboard data only
			rid.usUsage = 6;
			rid.hwndTarget = hWnd;
			RegisterRawInputDevices(&rid, 1, sizeof(rid));
			return DefWindowProc(hWnd,msg,wParam,lParam);
		}
		if (framework->GetScene())
		{
			switch(msg)
			{

			case WM_INPUT: {
				UINT dwSize;
				if (GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &dwSize, sizeof(RAWINPUTHEADER)) == -1)
				{
					break;
				}
				LPBYTE lpb = new BYTE[dwSize];
				if (lpb == NULL){
					break;
				}
				if (GetRawInputData((HRAWINPUT)lParam, RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER)) != dwSize)
				{
					delete[] lpb;
					break;
				}
				PRAWINPUT raw = (PRAWINPUT)lpb;
				WCHAR _String[3];
				HKL layout = GetKeyboardLayout(0);
				UINT Event = raw->data.keyboard.Message;
				UINT key = raw->data.keyboard.VKey;
				GetKeyboardState(keyboard);
				ToUnicodeEx(key, MapVirtualKeyEx(key, 0, layout), keyboard, _String, 1, 0, layout);
				WCHAR keyChar = _String[0];
				delete[] lpb;
				switch (Event) {
				case WM_KEYDOWN:
					if (key < 0x30 && key > 0) {
						//keyboard[key] = 0x80;
						keyChar = key;
						if (key == 8)
							keyChar = 127;
					}
					//framework->Log(IClientFramework::MSG_LOG, "%d", keyc);
					framework->GetScene()->OnSystemMsg(IClientScene::MSG_KEY_DOWN, (int)keyChar);
					break;
				case WM_KEYUP:
					//if (key < 0x30 && key > 0) 
					//	keyboard[key] = 0;
					break;
				}
				break;
			}
			//case WM_KEYDOWN:
			//case WM_SYSKEYDOWN:
			//	framework->GetScene()->OnSystemMsg(IClientScene::MSG_KEY_DOWN, (int)lParam);
			//	break;
			case WM_POINTERUP:
			case WM_POINTERDOWN:
			case WM_POINTERUPDATE:
				POINT p;
				p.x = LOWORD(lParam);
				p.y = HIWORD(lParam);
				::SetCursorPos(p.x, p.y);
				break;
			case WM_MOUSEMOVE:
				framework->GetScene()->OnSystemMsg(IClientScene::MSG_MOUSE_MOVE, (int)lParam);
				break;
			case WM_LBUTTONDOWN:
			case WM_LBUTTONDBLCLK:
				framework->GetScene()->OnSystemMsg(IClientScene::MSG_LBUTTON_DOWN, (int)lParam);
				break;
			case WM_LBUTTONUP:
				framework->GetScene()->OnSystemMsg(IClientScene::MSG_LBUTTON_UP, (int)lParam);
				break;
			case WM_RBUTTONDOWN:
			case WM_RBUTTONDBLCLK:
				framework->GetScene()->OnSystemMsg(IClientScene::MSG_RBUTTON_DOWN, (int)lParam);
				break;
			case WM_RBUTTONUP:
				framework->GetScene()->OnSystemMsg(IClientScene::MSG_RBUTTON_UP, (int)lParam);
				break;
			case WM_MBUTTONDOWN:
			case WM_MBUTTONDBLCLK:
				framework->GetScene()->OnSystemMsg(IClientScene::MSG_MBUTTON_DOWN, (int)lParam);
				break;
			case WM_MBUTTONUP:
				framework->GetScene()->OnSystemMsg(IClientScene::MSG_MBUTTON_UP, (int)lParam);
				break;
			case WM_CLOSE:
				//UnhookWindow();
				break;
			case WM_ACTIVATE:
				if (!(GetWindowLong(hWnd, GWL_STYLE) & WS_MINIMIZE)) //hack?
				{
					framework->GetScene()->OnSystemMsg(IClientScene::MSG_ACTIVATE, (int)wParam);
				}
				break;

			case WM_SYSCOMMAND:
				switch (wParam & 0xFFF0)
				{
				case SC_MINIMIZE:
					framework->GetScene()->OnSystemMsg(IClientScene::MSG_ACTIVATE, 0);
					break;
				// not needed as WM_ACTIVATE will be called
				/*case SC_RESTORE:
					framework->GetScene()->OnSystemMsg(IClientScene::MSG_ACTIVATE, 1);
					break;*/
				}
				break;
			}
		}

		extern IClient* client;

		switch(msg)
		{
//		if (renderer)
//			HANDLE_MSG(hWnd,WM_ACTIVATE, renderer->OnActivate);
		case WM_KEYDOWN:
		case WM_SYSKEYDOWN:
			switch (wParam) {
			case VK_RETURN:
				if (0 > ::GetKeyState(VK_MENU)) {
					extern IRenderer* renderer;
					renderer->SetFullscreen(!renderer->IsFullscreen());
				}
				break;
			case 115:
				if (GetKeyState(VK_MENU) & 0x80)
					PostQuitMessage(0);
				break;
			case 123:
				client->TakeScreenshot();
				break;
			};
			return TRUE;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		};
		return DefWindowProc(hWnd,msg,wParam,lParam);
	};

private:

    Cursor m_cursor;
	LONG m_originalproc;
	HWND hWnd;
	HDC hDC;
};
