#include <windows.h>

#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <sstream>
#include <algorithm>
#include <functional>

#include "writer.hpp"
#include "tinyxml2.h"
#include "file.h"

#include "math/math.hpp"

#include "../max7.export/fileoutput.h"

#define FRAMETIME 100

using namespace tinyxml2;

#define XML_FOREACH(variable, name, parent) \
	if (parent) \
	for (tinyxml2::XMLElement *variable = parent->FirstChildElement(name); \
		variable; \
		variable = variable->NextSiblingElement(name))

struct texture_t {
	texture_t(FILE* file, int size) {
		F_BlockReadLoop(file, size) {
			switch (F_BlockReadHeader(file)) {
			case MAKEID('N', 'A', 'M', 'E'):
				name = F_BlockReadString(file);
				break;
			case MAKEID('B', 'T', 'M', 'P'):
				F_BlockReadVector(file, buffer, char);
				break;
			default:
				F_BlockSkip(file);
			}
		}
	}

	void WriteOut(FileOutput* file) {
		Tag tag(file, "TXTR");
		file->Write("NAME", name);
		file->Write("BTMP", buffer);
	}

	std::string name;
	std::vector<char> buffer;
};

struct mesh_t {
	mesh_t(FILE* file, int size) : flags(0), txid(0), is_prop(false) {
		F_BlockReadLoop(file, size) {
			switch (F_BlockReadHeader(file)) {
			case MAKEID('N', 'A', 'M', 'E'):
				name = F_BlockReadString(file);
				break;
			case MAKEID('F', 'L', 'A', 'G'):
				F_BlockRead(file, flags);
				break;
			case MAKEID('T', 'X', 'I', 'D'):
				F_BlockRead(file, txid);
				break;
			case MAKEID('V', 'R', 'T', 'X'):
				F_BlockReadVector(file, vertices, vertex_t);
				break;
			case MAKEID('V', 'C', 'L', 'R'):
				F_BlockReadVector(file, colors, color_t);
				break;
			case MAKEID('F', 'A', 'C', 'E'):
				F_BlockReadVector(file, triangles, triangle_t);
				break;
			case MAKEID('W', 'G', 'H', 'T'):
				F_BlockReadVector(file, weights, weight_t);
				break;
			default:
				F_BlockSkip(file);
			}
		}
	}

	void WriteOut(FileOutput* file) {
		Tag tag(file, "MESH");
		file->Write("NAME", name);
		file->Write("FLAG", flags);
		file->Write("TXID", txid);
		file->Write("VRTX", vertices);
		file->Write("VCLR", colors);
		file->Write("FACE", triangles);
		file->Write("WGHT", weights);
		file->Write("RVTX", rvertices);
	}

	struct vertex_t {
		vec2_t st;
		struct Weight { int start, count; } weight;
	};

	struct triangle_t {
		WORD index[3];
	};

	struct weight_t {
		vec3_t position;
		vec3_t normal;
		int joint;
		float bias;
	};

	std::string name;
	std::vector<vertex_t> vertices;
	std::vector<triangle_t> triangles;
	std::vector<weight_t> weights;
	std::vector<color_t> colors;

	std::vector<math::Vertex> rvertices;
	int flags, txid;
	bool is_prop;
};

typedef math::AffineParts aff_t;

struct bone_t {
	char name[32];
	int id;
	int parent;
	aff_t transform;
	bool operator==(const bone_t& bone) {
		return strcmp(name, bone.name) == 0;
	}
};

std::map<std::string, struct sequence_t*> sequences;

struct event_t {
	char name[32];
	int frame;
};

struct sequence_t {
	struct squat_t {
		operator quat_t() const {
			quat_t r;
			r.x = x / 32767.0f;
			r.y = y / 32767.0f;
			r.z = z / 32767.0f;
			r.w = 1.0f - (r.x * r.x) - (r.y * r.y) - (r.z * r.z);
			r.w = sqrt(fabs(r.w));
			return r;
		}
		short x, y, z;
	};
				
	void Mod_ReadANIM(FILE* file, long size) {
		F_BlockReadLoop(file, size) {
			switch (F_BlockReadHeader(file)) {
				case MAKEID('R', 'A', 'T', 'E'):
					F_BlockRead(file, framerate);
					break;
				case MAKEID('S', 'I', 'Z', 'E'):
					F_BlockRead(file, framecount);
					break;
				case MAKEID('B', 'O', 'N', 'E'):
					F_BlockReadVector(file, bones, bone_t);
					break;
				case MAKEID('F', 'L', 'A', 'G'):
					boneflags.Load(file, F_BlockReadSize(file));
					break;
				case MAKEID('R', 'O', 'T', 'F'):
					F_BlockReadVector(file,rotations, quat_t);
					break;
				case MAKEID('R', 'O', 'T', 'N'):
					F_BlockReadConverVector(file, rotations, squat_t);
					break;
				case MAKEID('P', 'O', 'S', 'N'):
					F_BlockReadVector(file, positions, vec3_t);
					break;
				case MAKEID('S', 'C', 'A', 'L'):
					F_BlockReadVector(file, scales, vec3_t);
					break;
				default:
					F_BlockSkip(file);
			}
		}
	}

	sequence_t(const char* name, const char* filename)
		: framerate(1000 / FRAMETIME)
		, framecount(0)
		, visflags(0)
		, framestart(0)
		, start(0)
		, finish(0)
		, valid(false)
		, name(name)
	{
		if (sequences[name]) {
			*this = *sequences[name];
		} else {
			FILE* file = fopen(filename, "rb");
			if (!file)
				return;
			switch (F_BlockReadHeader(file)) {
			case MAKEID('A', 'N', 'I', 'M'):
				Mod_ReadANIM(file, F_BlockReadSize(file));
				valid = true;
				break;
			default:
				F_BlockSkip(file);
			}
			finish = framecount;
			fclose(file);
			ComputeFrames();
			sequences[name] = this;
		}
	}

	sequence_t(const char* name) 
		: framerate(1000 / FRAMETIME)
		, framecount(0)
		, visflags(0)
		, framestart(0)
		, start(0)
		, finish(0)
		, valid(false)
		, name(name)
	{
	}

	void ComputeFrames() {
		this->frames.resize(framecount * bones.size());
		aff_t* bone = &*this->frames.begin();
		for (int frame = 0; frame < framecount; ++frame) {
			for (size_t id = 0; id < bones.size(); ++id, ++bone) {
				*bone = bones[id].transform;
				if (boneflags._map[id].q != -1 && rotations.size() > 0) {
					bone->rotation = rotations[frame * boneflags._count.q + boneflags._map[id].q];
				}
				if (boneflags._map[id].t != -1 && positions.size() > 0) {
					bone->position = positions[frame * boneflags._count.t + boneflags._map[id].t];
				}
				if (boneflags._map[id].k != -1 && scales.size() > 0) {
					bone->scale = scales[frame * boneflags._count.k + boneflags._map[id].k];
				}
			}
		}
	}

	void WriteOut(FileOutput* file) {
		Tag tag(file, "ANIM");
		file->Write("NAME", name);
		file->Write("RATE", (float)framerate);
		file->Write("FRST", framestart);
		file->Write("LAST", framestart + (finish - start));
		file->Write("VSBL", visflags);
	}

	struct boneflags_t : std::vector<int> {
		void Load(FILE* file, int size) {
			resize(size / sizeof(int));
			fread(&*begin(), size, 1, file);
			_count = _Map();
			size_t n = 0;
			_map.resize(this->size());
			for (const_iterator flag = begin(); flag != end(); ++flag, ++n) {
				_map[n].t = (*flag & 0x1) ? _count.t++ : -1;
				_map[n].q = (*flag & 0x2) ? _count.q++ : -1;
				_map[n].k = (*flag & 0x4) ? _count.k++ : -1;
			}
		}

		struct _Map {
			_Map() : t(0), q(0), k(0) {}
			int t, q, k;
		} _count;

		std::vector<_Map> _map;
	};

	void AddEvent(const char* name, int frame) {
		event_t evt;
		strcpy(evt.name, name);
		evt.frame = frame;
		events.push_back(evt);
	}

	bone_t* AddProp(const char* model, const char* bone) {
		size_t i = 0;
		for (; i < bones.size(); i++) {
			if (strcmp(bone, bones[i].name) == 0) {
				props.push_back(model);
				return &bones[i];
			}
		}
		std::cout << "Can't find bone " << bone << std::endl;
		return 0;
	}

	std::string name;
	std::vector<std::string> props;
	std::vector<bone_t> bones;
	std::vector<quat_t> rotations;
	std::vector<vec3_t> positions;
	std::vector<vec3_t> scales;
	std::vector<aff_t> frames;
	std::vector<event_t> events;
	boneflags_t boneflags;
	int visflags;
	int framerate, framecount, framestart, start, finish;
	bool valid;
};

struct model_t {
	void Mod_ReadMODL(FILE* file, long size) {
		F_BlockReadLoop(file, size) {
			switch (F_BlockReadHeader(file)) {
			case MAKEID('T', 'X', 'T', 'R'):
				textures.push_back(texture_t(file, F_BlockReadSize(file)));
				break;
			case MAKEID('B', 'O', 'N', 'E'):
				F_BlockReadVector(file, bones, bone_t);
				break;
			case MAKEID('G', 'E', 'O', 'M'):
				Mod_ReadGEOM(file, F_BlockReadSize(file));
				break;
			default:
				F_BlockSkip(file);
			}
		}
	}

	std::vector<mat4_t> bindpose, inv_bindpose;

	void SetupGPUSkinning() {
		bindpose.resize(bones.size());
		inv_bindpose.resize(bones.size());

		for (int i = 0; i < bones.size(); i++) {
			ap_t ap;
			const bone_t &bone = bones[i];
			ap.position = bone.transform.position;
			ap.rotation = bone.transform.rotation;
			ap.scale = bone.transform.scale;
			bindpose[i] = bone.parent < 0 ? mat4_t(ap) : bindpose[bone.parent] * mat4_t(ap);
			inv_bindpose[i] = mat4_t::Invert(bindpose[i]);
		}

		mesh_t* mesh = &*meshes.begin();

		for (int j = 0; j < meshes.size(); ++j, ++mesh) {
			mesh->rvertices.resize(mesh->vertices.size());
			vertex_t *output = &*mesh->rvertices.begin();
			const mesh_t::vertex_t *input = &*mesh->vertices.begin();
			for (int i = 0; i < mesh->vertices.size(); ++i, ++output, ++input) {
				memset(output, sizeof(vertex_t), 0);
				output->tc[0] = input->st;
				output->color = color_t();
				for (int k = 0; k < MIN(input->weight.count, 4); ++k) {
					const mesh_t::weight_t *weight = &mesh->weights[k + input->weight.start];
					output->position += weight->position * bindpose[weight->joint] * weight->bias;
					const mat4_t rotation = bindpose[weight->joint].GetRotation();
					output->normal += weight->normal * rotation * weight->bias;
					output->blend_weight.v[k] = weight->bias;
					output->blend_indices.v[k] = weight->joint;
				}
			}
		}
	}

	struct anim_t {
		char name[32];
		int start;
		int finish;
		int visflags;
		int rate;
	};

	anim_t *FindSeq(const std::string& name, std::vector<anim_t> &anims) {
		for (int i = 0; i < anims.size(); i++) {
			if (name == anims[i].name) 
				return &anims[i];
		}
		return 0;
	}


	void WriteOut(const char* filename) {
		FileOutput *file = new FileOutput(filename, FileOutput::BINARY);
		{
			std::vector<anim_t> anims;
			for (int i = 0; i < sequences.size(); i++) {
				anim_t anm;
				memset(&anm, 0, sizeof(anim_t));
				strcpy(anm.name, sequences[i].name.c_str());
				anm.start = sequences[i].framestart;
				anm.finish = sequences[i].framestart + (sequences[i].finish - sequences[i].start);
				anm.rate = sequences[i].framerate;
				anm.visflags = sequences[i].visflags;
				anims.push_back(anm);
			}

			for (int i = 0; i < trans.size(); i++ ) {
				anim_t anm;
				memset(&anm, 0, sizeof(anim_t));
				strcpy(anm.name, trans[i].name.c_str());
				anm.start = frames.size() / bones.size();
				anm.finish = anm.start + trans[i].frames;
				anm.rate = 1000.f / FRAMETIME;
				anim_t *a = FindSeq(trans[i].from, anims);
				anim_t *b = FindSeq(trans[i].to, anims);
				if (!a || !b) {
					printf("Transition %s won't happen as there are no such sequence(s)\n", trans[i].name.c_str());
					continue;
				}
				anm.visflags = a->visflags & b->visflags;

				for (int j = 0; j < trans[i].frames; j++) {
					for (int k = 0; k < bones.size(); k++) {
						aff_t t;
						float frac = (float)j / (float)trans[i].frames;
#define DO_EASING(type, capital) \
							case math::capital: {\
							math::type easing; \
							if (trans[i].flags_ == math::EASE_IN) frac = easing.easeIn(frac, 0, 1, 1); \
							else if (trans[i].flags_ == math::EASE_OUT) frac = easing.easeOut(frac, 0, 1, 1); \
							else if (trans[i].flags_ == math::EASE_IN_OUT) frac = easing.easeInOut(frac, 0, 1, 1); \
								break;  \
						}
						switch (trans[i].easing_) {
						DO_EASING(Sine, SINE);
						DO_EASING(Quint, QUINT);
						DO_EASING(Quart, QUART);
						DO_EASING(Quad, QUAD);
						DO_EASING(Expo, EXPO);
						DO_EASING(Elastic, ELASTIC);
						DO_EASING(Cubic, CUBIC);
						DO_EASING(Circ, CIRC);
						DO_EASING(Bounce, BOUNCE);
						DO_EASING(Back, BACK);
						}
						frames.push_back(aff_t::Lerp(frac, 
							frames[a->start * bones.size() + k], 
							frames[b->start * bones.size() + k]));
					}
				}
				anims.push_back(anm);
			}

			for (int i = 0; i < lists.size(); i++ ) {
				anim_t anm;
				memset(&anm, 0, sizeof(anim_t));
				strcpy(anm.name, lists[i].name.c_str());
				anm.start = frames.size() / bones.size();
				anm.rate = 1000 / FRAMETIME;
				anm.visflags = -1;
				for (int m = 0; m < lists[i].list_.size(); m++) {
					anim_t *a = FindSeq(lists[i].list_[m], anims);
					if (!a) {
						printf("No such sequence %s in the list\n", lists[i].list_[m].c_str());
						continue;
					}
					float rateAspect = anm.rate / (float)a->rate;
					int newframes = (a->finish - a->start) * rateAspect;
					anm.visflags &= a->visflags;
					for (int j = 0; j < newframes; j++) {
						int frame = j * (a->finish - a->start) / newframes;
						for (int k = 0; k < bones.size(); k++) {
							aff_t *a_ = &frames[(a->start + frame) * bones.size() + k];
							frames.push_back(*a_);
						}
					}
				}
				anm.finish = frames.size() / bones.size();
				anims.push_back(anm);
			}

			SetupGPUSkinning();

			Tag tag(file, "MODL");
			file->Write("VERS", (int)200); 
			file->Write("BONE", bones); 
			file->Write("FRAM", frames);
			file->Write("ANIM", anims);
			file->Write("BIND", bindpose);
			file->Write("INVB", inv_bindpose);
			file->Write("EVNT", events);
			std::for_each(textures.begin(), textures.end(), 
				std::bind2nd(std::mem_fun_ref(&texture_t::WriteOut), file));
			std::for_each(meshes.begin(), meshes.end(), 
				std::bind2nd(std::mem_fun_ref(&mesh_t::WriteOut), file));
			//std::for_each(sequences.begin(), sequences.end(), 
			//	std::bind2nd(std::mem_fun_ref(&sequence_t::WriteOut), file));
		}
		delete file;
	}

	void Mod_ReadGEOM(FILE* file, long size) {
		F_BlockReadLoop(file, size) {
			switch (F_BlockReadHeader(file)) {
			case MAKEID('M', 'E', 'S', 'H'):
				meshes.push_back(mesh_t(file, F_BlockReadSize(file)));
				break;
			default:
				F_BlockSkip(file);
			}
		}
	}

	model_t(const char* name, const char* filename) 
		: valid(false)
		, name(name) 
	{
		FILE* file = fopen(filename, "rb");

		if (!file)
			return;

		switch (F_BlockReadHeader(file)) {
		case MAKEID('M', 'O', 'D', 'L'):
			Mod_ReadMODL(file, F_BlockReadSize(file));
			valid = true;
			break;
		default:
			F_BlockSkip(file);
		}

		std::cout << "Model " << name << ", " << meshes.size() << " meshes" << std::endl;

		fclose(file);
	}

	bool HasSequence(const char *name) {
		for (size_t id = 0; id < sequences.size(); ++id) {
			if (sequences[id].name == name)
				return true;
		}
		return false;
	}

	void AddSequence(sequence_t* sequence) {
		for (size_t id = 0; id < sequence->bones.size(); ++id) {
			FindOrAddBone(&sequence->bones[id]);
		}
		sequences.push_back(*sequence);
	}

	struct trans_t {
		std::string name, from, to;
		int frames;
		math::InterpolationType easing_;
		math::EaseFlags flags_;
	};

	std::vector<trans_t> trans;
	void AddTransition(const char *name, const char *from, const char *to, int frames, math::InterpolationType easing_, math::EaseFlags flags_) {
		trans_t tr = {name, from, to, frames, easing_, flags_};
		trans.push_back(tr);
	}

	struct list_t {
		std::string name;
		std::vector<std::string> list_;
	};

	std::vector<list_t> lists;
	void AddList(const char *name, std::vector<std::string> list_) {
		list_t tr = {name, list_};
		lists.push_back(tr);
	}

	void ConvertSequences() {
		for (size_t i = 0; i < sequences.size(); i++) {
			sequence_t &seq = sequences[i];
			for (size_t j = 0; j < seq.events.size(); j++) {
				event_t evt = seq.events[j];
				evt.frame += frames.size() / bones.size();
				events.push_back(evt);
			}
			ConvertSequence(&seq);
		}
	}

	void ConvertSequence(sequence_t* sequence) {
		int bonemap[256] = { -1 };
		sequence->framestart = frames.size() / bones.size();
		for (size_t i = 0; i < bones.size(); i++) {
			std::vector<bone_t>::iterator it = 
				std::find(sequence->bones.begin(), sequence->bones.end(), bones[i]);
			bonemap[i] = it == sequence->bones.end() ? -1 : bonemap[i] = it->id;
		}
		for (int frame = sequence->start; frame < sequence->finish; ++frame) {
			for (size_t id = 0; id < bones.size(); ++id) {
				int index = bonemap[id];
				if (index == -1) {
					frames.push_back(bones[id].transform);
				} else {
					frames.push_back(sequence->frames[frame * sequence->bones.size() + index]);
				}
			}
		}
		for (size_t i = 0; i < meshes.size(); i++) {
			if (!meshes[i].is_prop) {
				sequence->visflags |= 1 << i;
				continue;
			}
			for (size_t j = 0; j < sequence->props.size(); j++) {
				if (sequence->props[j] == meshes[i].name) {
					sequence->visflags |= 1 << i;
					break;
				}
			}
		}
	}

	void AddProp(model_t* model, bone_t* bone) {
		if (!bone)
			return;
		for (size_t i = 0; i < meshes.size(); i++) {
			if (model->name == meshes[i].name)
				return;
		}
		AddProp_UpdateJoints(model, bone);
		AddProp_UpdateTexture(model);
		meshes.push_back(model->meshes[0]);
		meshes.back().name = model->name;
		meshes.back().is_prop = true;
	}

	void AddProp_UpdateTexture(model_t* model) {
		size_t i = 0;
		texture_t* texture = &model->textures[0];
		for (; i < textures.size(); i++) {
			if (textures[i].name == texture->name) {
				break;
			}
		}
		if (i == textures.size()) {
			textures.push_back(*texture);
		}
		model->meshes[0].txid = (int)i;
	}

	void AddProp_UpdateJoints(model_t* model, bone_t* bone) {
		bone_t* target = FindOrAddBone(bone);
		for (size_t j = 0; j < model->meshes.size(); j++) {
			for (size_t k = 0; k < model->meshes[j].weights.size(); k++) {
				model->meshes[j].weights[k].joint = target->id;
			}
		}
	}

	bone_t* FindOrAddBone(bone_t* bone) {
		std::vector<bone_t>::iterator it = std::find(bones.begin(), bones.end(), *bone);
		if (it == bones.end()) {
			bones.push_back(*bone);
			bones.back().id = bones.size() - 1;
			return &bones.back();
		} else {
			return &*it;
		}
	}

	std::string name;
	std::vector<bone_t> bones;
	std::vector<mesh_t> meshes;
	std::vector<texture_t> textures;
	std::vector<sequence_t> sequences;
	std::vector<event_t> events;
	std::vector<aff_t> frames;
	bool valid;
};

bool replace(std::string& str, const std::string& from, const std::string& to) {
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos)
        return false;
    str.replace(start_pos, from.length(), to);
    return true;
}

namespace convert {
	bool SkModel(char* output, char* xml, char* model) {
		model_t *_model = NULL;

		std::istringstream xml_ss(xml);
		std::string xml_token;
		while(std::getline(xml_ss, xml_token, ',')) {
			tinyxml2::XMLDocument doc;

			if (doc.LoadFile(xml_token.c_str()) != XML_SUCCESS) {
				std::cout << "Can't load " << xml_token << std::endl;
				continue;
			}
			if (XMLElement* character = doc.FirstChildElement("character")) {
				const char* model_source = model ? model : character->Attribute("source");
				const char* model_sequence = character->Attribute("sequence");
				std::string sequence_name = model_source;
				replace(sequence_name, ".model", ".motion");
				model_sequence = model_sequence ? model_sequence : sequence_name.c_str();
				if (!_model) {
					_model = new model_t(model_source, model_source);
				}
				if (!_model->valid) {
					std::cout << "Model " << model_source << " is invalid" << std::endl;
					return false;
				}
				XML_FOREACH(sequence, "sequence", character) {
					const char* sequence_source = sequence->Attribute("source");
					const char* sequence_name = sequence->Attribute("id");
					const char* sequence_range = sequence->Attribute("range");
					if (_model->HasSequence(sequence_name))
						continue;
					sequence_t _sequence(sequence_name, sequence_source ? sequence_source : model_sequence);
					if (!_sequence.valid) {
						std::cout << "Sequence " << sequence_source << " is invalid" << std::endl;
						continue;
					}
					if (sequence_range) {
						sscanf(sequence_range, "%d,%d", &_sequence.start, &_sequence.finish);
					}
					std::cout << "Animation " << sequence_name << ", " << _sequence.finish - _sequence.start << " frames" << std::endl;
					XML_FOREACH(prop, "prop", sequence) {
						const char* prop_source = prop->Attribute("source");
						const char* prop_bone = prop->Attribute("bone");
						model_t _prop(prop_source, prop_source);
						if (!_prop.valid) {
							std::cout << "Prop " << prop_source << " is invalid" << std::endl;
							continue;
						}
						_model->AddProp(&_prop, _sequence.AddProp(prop_source, prop_bone));
					}
					XML_FOREACH(evt, "event", sequence) {
						const char* evt_name = evt->Attribute("name");
						const char* evt_frame = evt->Attribute("frame");
						if (!evt_name || !evt_frame)
							continue;
						std::cout << "\tEvent " << evt_name << std::endl;
						_sequence.AddEvent(evt_name, atoi(evt_frame));
					}
					_model->AddSequence(&_sequence);
				}
				XML_FOREACH(transition, "transition", character) {
					const char* sequence_name = transition->Attribute("id");
					const char* sequence_from = transition->Attribute("from");
					const char* sequence_to = transition->Attribute("to");
					const char* sequence_frames = transition->Attribute("frames");
					const char* easing = transition->Attribute("easing");
					const char* flags = transition->Attribute("flags");
					int frames = 10;
					if (sequence_frames) {
						frames = atoi(sequence_frames);
					}
					math::InterpolationType easing_ = math::SINE;
					math::EaseFlags flags_ = math::EASE_IN_OUT;
					if (flags) {
						if (!stricmp(flags, "in")) flags_ = math::EASE_IN;
						if (!stricmp(flags, "out")) flags_ = math::EASE_OUT;
						if (!stricmp(flags, "inout")) flags_ = math::EASE_IN_OUT;
					}

					if (easing) {
#define CHECK_EASING(name) if (!stricmp(easing, #name)) easing_ = math::name;
						CHECK_EASING(LINEAR);
						CHECK_EASING(SINE);
						CHECK_EASING(QUINT);
						CHECK_EASING(QUART);
						CHECK_EASING(QUAD);
						CHECK_EASING(EXPO);
						CHECK_EASING(ELASTIC);
						CHECK_EASING(CUBIC);
						CHECK_EASING(CIRC);
						CHECK_EASING(BOUNCE);
						CHECK_EASING(BACK);
					}
					_model->AddTransition(sequence_name, sequence_from, sequence_to, frames, easing_, flags_);
				}

				XML_FOREACH(transition, "list", character) {
					const char* sequence_name = transition->Attribute("id");
					const char* sequence_list = transition->Attribute("list");
					std::vector<std::string> list_;
					std::istringstream ss(sequence_list);
					std::string token;
					std::cout << "Adding list " << sequence_name << std::endl;
					while(std::getline(ss, token, ',')) {
						std::cout << "\t" << token << std::endl;
						list_.push_back(token);
					}
					_model->AddList(sequence_name, list_);
				}
			}

		}
		if (_model) {
			_model->ConvertSequences();
			_model->WriteOut(output);
			delete _model;
			return true;
		} else {
			return false;
		}
	}
}