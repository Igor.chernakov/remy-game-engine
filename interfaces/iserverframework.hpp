#pragma once

#include "iframework.hpp"
#include "iserverscene.hpp"

struct IServerFramework: IFramework
{
	virtual boost::shared_ptr<IServerScene> GetScene() const = 0;

	virtual void SetScene(boost::shared_ptr<IServerScene> ) = 0;
};