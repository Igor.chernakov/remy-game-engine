#include <windows.h>

#include <iostream>
#include <fstream>
#include <strstream>
#include <string>
#include <vector>
#include <list>
#include <map>
#include <algorithm>
#include <functional>

#include <d3d9.h>
#include <d3d9types.h>
#include <d3dx9tex.h>
#include <dxerr.h>

#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "dxerr.lib")

#include "writer.hpp"
#include "nvpacker.hpp"

#include "../max7.export/fileoutput.h"

extern bool __no_crop;

typedef struct {
	char		name[32];
	float		pack[4];
	int			size[2];
	RECT		rect;
} dsprframe_t;

struct anim_t {
	char name[32];
	int start;
	int finish;
	int visflags;
	int rate;
};


namespace convert
{
	static unsigned pow2(unsigned int value)
	{
		unsigned int r = 1;
		for (r; r < value; r <<= 1)
		{
		}
		return r;
	}


	static const int SPRITEFORMAT_VERSION = 100;

	LPDIRECT3D9 d3d;
	LPDIRECT3DDEVICE9 device;

	int ATLAS_SIZE = 128;

	struct deleter_t
	{
		void operator()(void* ptr)
		{
			if (ptr)
				delete ptr;
		}
	};

#pragma pack (push, 1)
	struct tga_header_t
	{
		byte  identsize;          // size of ID field that follows 18 byte header (0 usually)
		byte  colourmaptype;      // type of colour map 0=none, 1=has palette
		byte  imagetype;          // type of image 0=none,1=indexed,2=rgb,3=grey,+8=rle packed
		short colourmapstart;     // first colour map entry in palette
		short colourmaplength;    // number of colours in palette
		byte  colourmapbits;      // number of bits per palette entry 15,16,24,32
		short xstart;             // image x origin
		short ystart;             // image y origin
		short width;              // image width in pixels
		short height;             // image height in pixels
		byte  bits;               // image bits per pixel 8,16,24,32
		byte  descriptor;         // image descriptor bits (vh flip bits)
	};
#pragma pack (pop, 1)


	HRESULT SaveSurfaceToTGAFile(LPDIRECT3DDEVICE9 pD3DDevice, RECT rect, const char *szFileName, LPDIRECT3DSURFACE9 pSurface)
	{
		return D3DXSaveSurfaceToFile(szFileName, D3DXIFF_TGA, pSurface, NULL, &rect);
	}

	struct sprite_t
	{
		sprite_t(const char* filename, bool nocrop = false)
			: buffer(0)
			, length(0)
			, filename(filename)
		{
			if (__no_crop) {
				nocrop = true;
			}
			std::cout << filename << std::endl;

			D3DXIMAGE_INFO info;
			LPDIRECT3DTEXTURE9 texture;
			D3DXGetImageInfoFromFile(filename, &info);

			if (info.Width == 0 || info.Height == 0)
			{
				std::cerr << "Image has zero size." << std::endl;
				exit(1);
			}

			D3DXCreateTextureFromFileEx(
				device, filename,
				width = info.Width,
				height = info.Height,
				0, 0, D3DFMT_A8R8G8B8, D3DPOOL_SCRATCH, 
				D3DX_DEFAULT, D3DX_DEFAULT, 0, NULL, NULL, &texture);

			if (!texture)
			{
				std::cerr << "Error loading image." << std::endl;
				exit(1);
			}

			D3DLOCKED_RECT lock;
			LPDIRECT3DSURFACE9 surface;

			texture->GetSurfaceLevel(0, &surface);

			if (!nocrop) {
				surface->LockRect(&lock, 0, 0);

				//texture->LockRect(0, &lock, 0, D3DLOCK_READONLY);

				for (rect.left = 0; IsColumnEmpty(rect.left, lock, info) && rect.left < info.Width; rect.left++);
				for (rect.right = info.Width; IsColumnEmpty(rect.right - 1, lock, info) && rect.right > 0; rect.right--);
				for (rect.top = 0; IsRowEmpty(rect.top, lock, info) && rect.top < info.Height; rect.top++);
				for (rect.bottom = info.Height; IsRowEmpty(rect.bottom - 1, lock, info) && rect.bottom > 0; rect.bottom--);

				surface->UnlockRect();
			} else {
				rect.left = 0; rect.top = 0;
				rect.right = width; rect.bottom = height;
			}

			if (rect.right - rect.left > ATLAS_SIZE) ATLAS_SIZE = rect.right - rect.left;
			if (rect.bottom - rect.top > ATLAS_SIZE) ATLAS_SIZE = rect.bottom - rect.top;

			ATLAS_SIZE = pow2(ATLAS_SIZE);

			TCHAR szTempFileName[MAX_PATH];
			TCHAR lpTempPathBuffer[MAX_PATH];

			GetTempPath(MAX_PATH, lpTempPathBuffer);
			GetTempFileName(lpTempPathBuffer, TEXT("cnv"), 0, szTempFileName);

			SaveSurfaceToTGAFile(device, rect, szTempFileName, surface);

			FILE* fp = fopen(szTempFileName, "rb");
			fseek(fp, 0L, SEEK_END);
			length = ftell(fp);
			buffer = new char[length];
			rewind(fp);
			fread(buffer, 1, length, fp);
			fclose(fp);

			remove(szTempFileName);

			texture->Release();
		}

		inline bool IsColumnEmpty(int column, D3DLOCKED_RECT lock, D3DXIMAGE_INFO info) const
		{
			for (int it = 0; it < info.Height; it++)
				if ((GetValueAt(column, it, lock, info) >> 24) & 0xFF > 0)
					return false;
			return true;
		}

		inline bool IsRowEmpty(int row, D3DLOCKED_RECT lock, D3DXIMAGE_INFO info) const
		{
			for (int it = 0; it < info.Width; it++)
				if ((GetValueAt(it, row, lock, info) >> 24) & 0xFF > 0)
					return false;
			return true;
		}

		inline int GetValueAt(int x, int y, D3DLOCKED_RECT lock, D3DXIMAGE_INFO info) const
		{
			return ((int*)lock.pBits)[x + y * lock.Pitch / 4];
		}

		~sprite_t()
		{
			if (buffer)
				delete[] buffer;
		}

		RECT rect;
		int width, height;
		char *buffer;
		int length;
		std::string filename;
	};

	struct animation_t
	{
		animation_t(const std::string& name)
			: name(name)
			, fps(0) //by default sprite is still
			, pingpong(0)
			, pingpongb(0)
			, nocrop(false)
		{
		}
		~animation_t()
		{
			std::for_each(frames.begin(), frames.end(), deleter_t());
		}

		int pingpong, pingpongb;
		int fps;
		std::vector<sprite_t*> frames;
		std::string name;
		bool nocrop;
	};

	typedef std::list<animation_t> Animations;
	static Animations animations;

	void LoadSprites(const char* filename)
	{
		std::ifstream file(filename);
		if (file)
		{
			while (!file.eof())
			{
				std::string line;
				std::getline(file, line);
				if (line.empty()) continue;
				switch (line[0])
				{
					// new header
				case '[':
					{
						std::string header;
						size_t pos = line.find(']');
						if (pos != std::wstring::npos)
						{
							animations.push_back(animation_t(line.substr(1, pos - 1)));
						}
						else
						{
							std::cerr << "Error parsing file " << filename << std::endl;
							exit(1);
						}
					}
					break;
					// comments
				case ';':
				case ' ':
				case '#':
					break;
				default:
					if (animations.size() == 0)
					{
						std::cerr << "Error parsing file " << filename << std::endl;
						std::cerr << "Specify first animation name" << std::endl;
						exit(1);
					}
					if (line.find('=') != line.npos)
					{
						size_t pos = line.find('=');
						std::string name = line.substr(0, pos);
						if (name == "fps")
						{
							animations.back().fps = atoi(line.substr(pos + 1).c_str());
						}
						else if (name == "nocrop")
						{
							animations.back().nocrop = true;
						}
						else if (name == "pingpong")
						{
							animations.back().pingpong = atoi(line.substr(pos + 1).c_str());
							animations.back().pingpongb = atoi(line.substr(pos + 1).c_str());
						}
						else if (name == "pingpongb")
						{
							animations.back().pingpongb = atoi(line.substr(pos + 1).c_str());
						}
					}
					else
					{
						BOOL bContinue = TRUE;
						WIN32_FIND_DATA data;
						HANDLE hFind = FindFirstFile(line.c_str(), &data); 

						while (hFind && bContinue)
						{
							if (data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
							{
								// This is a directory
							}
							else
							{
								animations.back().frames.push_back(new sprite_t(data.cFileName, animations.back().nocrop));
							}
							bContinue = FindNextFile(hFind, &data);
						}
						FindClose(hFind);
					}
				}
			}
		}
	}

	bool Sprite(char* output, char* ini, int format)
	{
		HWND hWnd;

		WNDCLASSEX wndClass = { 0 };
		wndClass.cbSize = sizeof(wndClass);
		wndClass.style = CS_HREDRAW | CS_VREDRAW | CS_DBLCLKS;
		wndClass.lpfnWndProc = DefWindowProc;
		wndClass.lpszClassName = "EMPTYCONVERTERWINDOW";

		RegisterClassEx(&wndClass);

		hWnd = CreateWindowEx(0, "EMPTYCONVERTERWINDOW", "EMPTY", 0, 0, 0, 256, 256, NULL, NULL, NULL, NULL);

		D3DPRESENT_PARAMETERS params = {
			512, 512, D3DFMT_A8R8G8B8, 1,
			D3DMULTISAMPLE_NONE, 0, D3DSWAPEFFECT_COPY, hWnd, true,
			TRUE, D3DFMT_D24S8
		};


		d3d = Direct3DCreate9(D3D_SDK_VERSION);
		d3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &params, &device);

		LoadSprites(ini);

		std::vector<PackedTexture*> packed_textures;

		while (true) {
			for (Animations::const_iterator it = animations.begin(); it != animations.end(); ++it)
			{
				for (int i = 0; i < it->frames.size(); i++)
				{
					packed_textures.push_back( new PackedTexture(it->frames[i]->buffer, it->frames[i]->length) );
				}
			}

			if (packers.size() == 1) 
			{
				for (int i = 0; i < packers.size(); i++)
				{
					packers[i].lock()->Shrink();
					packers[i].lock()->WriteToDisk(output);
				}
				break;
			}

			if (ATLAS_SIZE == 1024) {
				std::cout << "Textures do not fit in 1024x1024 atlas" << std::endl;
				exit(1);
			}

			ATLAS_SIZE *= 2;

			std::for_each(packed_textures.begin(), packed_textures.end(), deleter_t());
			packed_textures.clear();
			packers.clear();
		}

		if (animations.size())
		{
			FILE* fp = fopen(output, "rb");
			fseek(fp, 0L, SEEK_END);
			int start = ftell(fp);
			fclose(fp);

			if (format == 1) {
				TCHAR szTempFileName[MAX_PATH];
				TCHAR lpTempPathBuffer[MAX_PATH];

				GetTempPath(MAX_PATH, lpTempPathBuffer);
				GetTempFileName(lpTempPathBuffer, TEXT("cnv"), 0, szTempFileName);

				//std::ofstream ofs((std::string(output) + ".py").c_str());

				{
					int id = 0;
					Writer file(szTempFileName);
					Area area("SPRT", file);
					{
						Area area("VERS", file, true);
						file << SPRITEFORMAT_VERSION * format;
					}
					for (Animations::const_iterator it = animations.begin(); it != animations.end(); ++it)
					{
						Area area("ANIM", file);
						{
							Area area("NAME", file, true);
							file << it->name;
						}
						{
							Area area("RATE", file, true);
							file << it->fps;
						}
						for (int i = 0; i < it->frames.size(); i++, id++)
						{
							int repeats = 1;

							if (it->pingpong && i == 0) {
								repeats = it->pingpong;
							}

							if (it->pingpongb && i == it->frames.size() - 1) {
								repeats = it->pingpongb;
							}

							for (int k = 0; k < repeats; k++){
								Area area("FRAM", file);
								const float* rect = packed_textures[id]->GetActiveArea(); {
									Area area("NAME", file, true);
									std::string name = it->frames[i]->filename;
									name = name.substr(0, name.find_last_of("."));
									file << name;
								}
								{
									Area area("SIZE", file, true);
									file << it->frames[i]->width << it->frames[i]->height;
								}
								{
									Area area("RECT", file, true);
									file << it->frames[i]->rect;
								}
								{
									Area area("PACK", file, true);
									file << rect[0] << rect[1] << rect[2] << rect[3];
								}
							}
						}

						if (it->pingpong && it->frames.size() > 2) //pingpong
						{
							int _id = id;
							id -= 2;
							for (int i = it->frames.size() - 2; i > 1; i--, id--)
							{
								Area area("FRAM", file);
								const float* rect = packed_textures[id]->GetActiveArea();
								{
									Area area("NAME", file, true);
									std::string name = it->frames[i]->filename;
									name = name.substr(0, name.find_last_of("."));
									file << name;
								}
								{
									Area area("SIZE", file, true);
									file << it->frames[i]->width << it->frames[i]->height;
								}
								{
									Area area("RECT", file, true);
									file << it->frames[i]->rect;
								}
								{
									Area area("PACK", file, true);
									file << rect[0] << rect[1] << rect[2] << rect[3];
								}
							}
							id = _id;
						}
					}
				}

				//ofs.close();

				std::ofstream finalizer(output, std::ios::out | std::ios::binary | std::ios::app);

				fp = fopen(szTempFileName, "rb");
				fseek(fp, 0L, SEEK_END);
				int size = ftell(fp);
				rewind(fp);
				while (true) {
					int buffer;
					int count = fread((char*)&buffer, 1, 4, fp);
					if (!count)
						break;
					finalizer.write((char*)&buffer, count);
				}
				fclose(fp);

				remove(szTempFileName);

				finalizer.write((char*)&size, 4);
				finalizer.write("SPRT", 4);
			}	
			else if (format == 2) {
				std::string spritefile = output;
				spritefile = spritefile.substr(0, spritefile.find_last_of(".")) + ".spr";

				std::vector<anim_t> anims2;
				std::vector<dsprframe_t> frames2;
				int id = 0;

				for (Animations::const_iterator it = animations.begin(); it != animations.end(); ++it) {
					anim_t anim;
					memset(&anim, 0, sizeof(anim_t));
					strcpy(anim.name, it->name.c_str());
					anim.rate = it->fps;
					anim.start = frames2.size();

					for (int i = 0; i < it->frames.size(); i++, id++) {
						int repeats = 1;

						if (it->pingpong && i == 0) {
							repeats = it->pingpong;
						}

						if (it->pingpongb && i == it->frames.size() - 1) {
							repeats = it->pingpongb;
						}

						for (int k = 0; k < repeats; k++){
							const float* rect = packed_textures[id]->GetActiveArea();
							dsprframe_t frm;
							memset(&frm, 0, sizeof(dsprframe_t));
							std::string name = it->frames[i]->filename;
							name = name.substr(0, name.find_last_of("."));
							strcpy(frm.name, name.c_str());
							frm.size[0] = it->frames[i]->width;
							frm.size[1] = it->frames[i]->height;
							frm.rect = it->frames[i]->rect;
							frm.pack[0] = rect[0];
							frm.pack[1] = rect[1];
							frm.pack[2] = rect[2];
							frm.pack[3] = rect[3];
							frames2.push_back(frm);
						}
					}

					if (it->pingpong && it->frames.size() > 2) //pingpong
					{
						int _id = id;
						id -= 2;
						for (int i = it->frames.size() - 2; i > 1; i--, id--)
						{
							const float* rect = packed_textures[id]->GetActiveArea();
							dsprframe_t frm;
							memset(&frm, 0, sizeof(dsprframe_t));
							std::string name = it->frames[i]->filename;
							name = name.substr(0, name.find_last_of("."));
							strcpy(frm.name, name.c_str());
							frm.size[0] = it->frames[i]->width;
							frm.size[1] = it->frames[i]->height;
							frm.rect = it->frames[i]->rect;
							frm.pack[0] = rect[0];
							frm.pack[1] = rect[1];
							frm.pack[2] = rect[2];
							frm.pack[3] = rect[3];
							frames2.push_back(frm);
						}
						id = _id;
					}


					anim.finish = frames2.size();

					anims2.push_back(anim);
				}

				FileOutput *file = new FileOutput(spritefile, FileOutput::BINARY);
				{
					std::string txtr = output;
					if (txtr.find_last_of("/") != std::string::npos) {
						txtr = txtr.substr(txtr.find_last_of("/")+1);
					}
					if (txtr.find_last_of("\\") != std::string::npos) {
						txtr = txtr.substr(txtr.find_last_of("\\")+1);
					}
					Tag tag(file, "SPRT");
					file->Write("VERS", 200); 
					file->Write("TXTR", txtr);
					file->Write("ANIM", anims2); 
					file->Write("FRAM", frames2); 
				}
				delete file;
			}
		}

		std::for_each(packed_textures.begin(), packed_textures.end(), deleter_t());

		d3d->Release();
		device->Release();

		DestroyWindow(hWnd);

		return true;
	}
}