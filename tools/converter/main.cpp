#include <string>

namespace convert
{
	void WarCraft3(const std::string& source, const std::string& destination);
	void Quake3(const std::string& source, const std::string& destination);
	bool SkModel(char* output, char* xml, char* model);
	bool Sprite(char* output, char* ini, int format);
}

bool __no_crop = false;

int main(int argc, char* argv[])
{
	//convert::WarCraft3(
	//	"D:\\Projects\\Buzz.Converter\\data\\Maps\\MysticIsles",
	//	"D:\\Projects\\Buzz.Engine\\bin\\data.amongtheheaven\\maps\\test.map");
	//convert::Quake3(
	//	"X:\\Projects\\.recycled\\Tactics\\Executable\\base\\maps\\hi1.bsp", 
	//	"X:\\Projects\\Buzz.Engine\\data.amongtheheaven\\maps\\q3.map");
	//convert::Sprite("c:\\tables.tga", "C:\\Engine\\amongtheheavens.files\\sprites\\tables\\indoor\\tables.ini");

	if (argc == 4 && strcmp(argv[1], "-sx") == 0)
	{
		convert::Sprite(argv[2], argv[3], 2);
	}
	if (argc == 4 && strcmp(argv[1], "-s") == 0)
	{
		convert::Sprite(argv[2], argv[3], 1);
	}
	if (argc == 4 && strcmp(argv[1], "-snc") == 0)
	{
		__no_crop = true;
		convert::Sprite(argv[2], argv[3], 1);
	}
	if (argc == 3 && strcmp(argv[1], "-s") == 0)
	{
		convert::Sprite(argv[2], "config.ini", 1);
	}
	if (argc >= 4 && strcmp(argv[1], "-m") == 0)
	{
		convert::SkModel(argv[2], argv[3], argc >= 5 ? argv[4] : 0);
	}
}