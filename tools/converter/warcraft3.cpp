#include <windows.h>

#include <iostream>
#include <fstream>
#include <strstream>
#include <string>
#include <vector>
#include <list>
#include <map>

#include <d3d9types.h>
#include <d3dx9tex.h>

#include "writer.hpp"

#pragma comment(lib, "d3dx9.lib")

const std::string working_directory = "D:\\Projects\\Buzz.Converter\\data\\";

#define MAKEFOURCC(ch0, ch1, ch2, ch3) ((int)(char)(ch0) | ((int)(char)(ch1) << 8) | ((int)(char)(ch2) << 16) | ((int)(char)(ch3) << 24 ))

struct Reader: public std::ifstream
{
	Reader(const std::string& filename)
		: std::ifstream(filename.c_str(), std::ios::in | std::ios::binary)
	{
		if (!*this)
		{
			std::cerr << "Can't find file " << filename << std::endl;
		}
	}
	~Reader()
	{
		close();
	}
	template <typename T>
	Reader& operator>> (T& dst)
	{
		read((char*)&dst, sizeof(dst));
		return *this;
	}
	template <>
	Reader& operator>> (std::string& dst)
	{
		char c;
		*this >> c;
		for (; c; *this >> c)
		{
			dst.push_back(c);
		}
		return *this;
	}
	template <typename T>
	Reader& operator>> (std::vector<T>& dst)
	{
		int count;
		*this >> count;
		for (; *this && count > 0; --count)
		{
			T value;
			*this >> value;
			int a = tellg();
			dst.push_back(value);
		}
		return *this;
	}
};

#pragma pack(push, before_WarCraft3)
#pragma pack(1)

namespace WarCraft3
{
	class Sheet
	{
	public:
		Sheet(const std::string& filename)
		{
			std::map<std::pair<int, int>, std::string> inner_table;
			std::ifstream file(filename.c_str());
			std::string value;
			int x = 0, y = 0;
			for (char c = 0; file >> c;)
			{
				switch (c)
				{
				case ';':
					file >> c;
					switch (c)
					{
					case 'X':
						file >> x;
						break;
					case 'Y':
						file >> y;
						break;
					case 'K':
						file >> value;
						inner_table[std::pair<int, int>(x, y)] = (*value.begin() == '"' ? value.substr(1, value.length() - 2) : value);
						break;
					}
				}
			}

			for (int x = 1; inner_table[std::pair<int, int>(x, 1)].length() > 0; ++x)
			{
				for (int y = 1; inner_table[std::pair<int, int>(1, y)].length() > 0; ++y)
				{
					table[inner_table[std::pair<int, int>(x, 1)] + ":" + inner_table[std::pair<int, int>(1, y)]] = inner_table[std::pair<int, int>(x, y)];
				}
			}
		}

		std::string Get(int row, const std::string& column)
		{
			char buffer[5] = {0};
			memcpy(buffer, &row, 4);
			return table[column + ":" + buffer];
		}

		std::string Get(const std::string& row, const std::string& column)
		{
			return table[column + ":" + row];
		}

	private:
		std::map<std::string, std::string> table;
	};

	Sheet TerrainSheet(working_directory + "TerrainArt\\Terrain.slk");
	Sheet CliffsSheet(working_directory + "TerrainArt\\CliffTypes.slk");
	Sheet DoodadsSheet(working_directory + "Doodads\\Doodads.slk");
	Sheet DestructablesSheet(working_directory + "Units\\DestructableData.slk");
	Sheet UnitsSheet(working_directory + "Units\\UnitUI.slk");

	struct Model
	{
		std::string filename;

		struct Chunk
		{
			Chunk(Reader& reader): header(0)
			{
				reader >> header >> data;
			}
			int header;
			std::vector<char> data;
		};

		Model(): loaded(false)
		{
		}

		Model(const std::string& filename): filename(filename)
		{
			Reader file(filename);
			file.seekg(4);
			for (Chunk chunk(file); file; chunk = Chunk(file))
			{
				switch (chunk.header)
				{
				case MAKEFOURCC('G','E','O','S'):
					meshes.push_back(Mesh(*((Reader*)&std::istrstream(&*chunk.data.begin(), chunk.data.size()).seekg(4))));
					break;
				case MAKEFOURCC('T','E','X','S'):
					textures.resize(chunk.data.size() / sizeof(Texture));
					memcpy(&*textures.begin(), &*chunk.data.begin(), chunk.data.size());
					break;
				}
			}
		}

		struct Texture
		{
			long nId;
			char path[256];
			int pointer;
			long nWrapping; //(1:WrapWidth; 2:WrapHeight; 3:Both)
		};

		struct Mesh
		{
			Mesh(Reader& memory)
			{
				for (int tag = 0; memory >> tag;)
				{
					switch (tag)
					{
					case MAKEFOURCC('V','R','T','X'): memory >> vertices; break;
					case MAKEFOURCC('N','R','M','S'): memory >> normals; break;
					case MAKEFOURCC('U','V','B','S'): memory >> uv; break;
					case MAKEFOURCC('P','T','Y','P'): memory >> primitivetypes; break;
					case MAKEFOURCC('P','C','N','T'): memory >> primitivecounts; break;
					case MAKEFOURCC('P','V','T','X'): memory >> triangles; break;
					case MAKEFOURCC('G','N','D','X'): memory >> vertexgroups; break;
					case MAKEFOURCC('M','T','G','C'): memory >> vertexgroupscount; break;
					case MAKEFOURCC('M','A','T','S'): memory >> matrices >> material >> group >> selectable >> default_bounds >> bounds; break;
					case MAKEFOURCC('U','V','A','S'): memory >> unknown; break;
					}
				}
			}

			struct BoundsEx
			{
				float radius;
				Bounds bounds;
			};

			std::vector<Vector3> vertices;
			std::vector<Vector3> normals;
			std::vector<Vector2> uv;
			std::vector<long> matrices;
			std::vector<long> primitivetypes;
			std::vector<long> primitivecounts;
			std::vector<short> triangles;
			std::vector<char> vertexgroups;
			std::vector<long> vertexgroupscount;
			std::vector<BoundsEx> bounds;
			BoundsEx default_bounds;
			long unknown;
			long material;
			long group;
			long selectable;// (0:none;4:Unselectable)
		};

		std::vector<Mesh> meshes;
		std::vector<Texture> textures;

		bool loaded;
	};

	struct Info
	{
		struct Player
		{
			int unknown, type, race, fixed;
			std::string name;
			Vector2 start;
			int flags[2];
		};

		Info(const std::string& filename)
		{
			Reader(filename)
				>> version >> saves >> editor >> mapname >> mapauthor >> mapdescr >> mapplayers >> cambounds
				>> camboundsnotes >> playsize >> flags >> ground >> campaign >> loadingtext
				>> loadingtitle >> loadingsubtitle >> loadingnum >> prologuetext >> prologuetitle
				>> prologuesubtitle >> players;
		}

		int version, saves, editor;
		std::string mapname, mapauthor, mapdescr, mapplayers;
		float cambounds[8];
		int camboundsnotes[4];
		Size playsize;
		int flags;
		char ground;
		int campaign;
		std::string loadingtext, loadingtitle, loadingsubtitle;
		int loadingnum;
		std::string prologuetext, prologuetitle, prologuesubtitle;
		std::vector<Player> players;
	};

	Reader& operator>> (Reader& reader, Info::Player& dst)
	{
		return reader >> dst.unknown >> dst.type >> dst.race >> dst.fixed >> dst.name >> dst.start >> dst.flags;
	}

	static const int TILESIZE = 128;

	struct Heightmap
	{
		Heightmap(const std::string& filename)
		{
			Reader file(filename);
			file >> header >> version >> tileset >> custom >> grounds >> cliffs >> size >> center;
			vertices.resize(size.width * size.height);
			file.read((char*)&*vertices.begin(), size.width * size.height * sizeof(Vertex));
			std::cout 
				<< "Size: " << size.width << "x" << size.width << std::endl
				<< "Version: " << version << std::endl
				<< "Tileset: " << tileset << std::endl
				<< "Grounds: " << grounds.size() << std::endl
				<< "Cliffs: " << cliffs.size() << std::endl
				;
		}

		int header;
		int version;
		char tileset;
		int custom;
		std::vector<int> grounds;
		std::vector<int> cliffs;
		Size size;
		Vector2 center;

		struct Vertex
		{
			enum Flags
			{
				RAMP = 0x01,
			};

			inline char flags() const { return data[0] >> 4; }
			inline char ground() const { return data[0] & 0xf; }
			inline char details() const { return data[1] & 0xf; }
			inline char cliff() const { return data[2] >> 4; }
			inline char level() const { return data[2] & 0xf - 4; }
			inline int  height() const { return (int)level() * TILESIZE + ((int)accurate_height - 0x2000) / 4; }
			inline bool ramp() const { return flags() & RAMP; }

			short accurate_height;
			short waterlevel;
			char data[3];
		};

		inline const Vertex& Get(int x, int y) const
		{
			return vertices[max(0, min(x, size.width - 1)) + max(0, min(y, size.height - 1)) * size.width];
		}

		inline float accurate_height(float _x, float _y) const
		{
			float x = _x / TILESIZE, y = _y / TILESIZE, kx = x - (int)x, ky = y - (int)y;
			return (((Get(x, y).accurate_height * (1 - kx) + Get(x + 1, y).accurate_height * kx) * (1 - ky) +
				(Get(x, y + 1).accurate_height * (1 - kx) + Get(x + 1, y + 1).accurate_height * kx) * ky) - 0x2000) / 4;
		}

		std::vector<Vertex> vertices;
	};

	struct Doodads
	{
		Doodads(const std::string& filename, const Heightmap& heightmap)
		{
			Reader(filename) >> header >> version >> unknown >> doodads;
		}

		struct Doodad
		{
			int id;
			int variation;
			Vector3 position;
			float angle;
			Vector3 scale;
			char nFlags;
			char lifetime;
			int id_num;
		};

		int header;
		int version;
		int unknown;
		std::vector<Doodad> doodads;
	};

	struct Map
	{
		Map(const std::string& dirname)
			: heightmap(dirname + "\\war3map.w3e")
			, doodads(dirname + "\\war3map.doo", heightmap)
			, info(dirname + "\\war3map.w3i")
		{
		}

		struct Quad
		{
			Quad(short id): _1(id+0), _2(id+1), _3(id+2), _4(id+2), _5(id+3), _6(id+0) {}
			short _1, _2, _3, _4, _5, _6;
		};

		struct Tile
		{
			union
			{
				const Heightmap::Vertex *v[4];

				struct
				{
					const Heightmap::Vertex *bl, *tl, *tr, *br;
				};
			};

			std::pair<int, int> origin;

			Tile(const Heightmap& hm, const std::pair<int, int>& t)
				: bl(&hm.Get(t.first, t.second))
				, tl(&hm.Get(t.first, t.second + 1))
				, tr(&hm.Get(t.first + 1, t.second + 1))
				, br(&hm.Get(t.first + 1, t.second))
				, origin(t)
			{
			}

			Tile& operator= (const Tile& tile)
			{
				memcpy((char*)v, tile.v, sizeof(v));
				return *this;
			}

			inline std::pair<int, int> uv(int g) const
			{
				enum { BOTTOM_LEFT = 0x1, TOP_LEFT = 0x2, TOP_RIGHT = 0x4, BOTTOM_RIGHT = 0x8 };

				int 
					u = -1,
					v = -1,
					flags = (bl->ground() == g) << 0 | (tl->ground() == g) << 1 | (tr->ground() == g) << 2 | (br->ground() == g) << 3;

				switch (flags)
				{
				case TOP_RIGHT: u = 0, v = 1; break;
				case TOP_LEFT: u = 0, v = 2; break;
				case TOP_LEFT | TOP_RIGHT: u = 0, v = 3; break;
				case BOTTOM_RIGHT: u = 1, v = 0; break;
				case BOTTOM_RIGHT | TOP_RIGHT: u = 1, v = 1; break;
				case BOTTOM_RIGHT | TOP_LEFT: u = 1, v = 2; break;
				case BOTTOM_RIGHT | TOP_RIGHT | TOP_LEFT: u = 1, v = 3; break;
				case BOTTOM_LEFT: u = 2, v = 0; break;
				case BOTTOM_LEFT | TOP_RIGHT: u = 2, v = 1; break;
				case BOTTOM_LEFT | TOP_LEFT: u = 2, v = 2; break;
				case BOTTOM_LEFT | TOP_RIGHT | TOP_LEFT: u = 2, v = 3; break;
				case BOTTOM_LEFT | BOTTOM_RIGHT: u = 3, v = 0; break;
				case BOTTOM_LEFT | TOP_RIGHT | BOTTOM_RIGHT: u = 3, v = 1; break;
				case BOTTOM_LEFT | TOP_LEFT | BOTTOM_RIGHT: u = 3, v = 2; break;
				case BOTTOM_LEFT | TOP_RIGHT | BOTTOM_RIGHT | TOP_LEFT: u = 4 + bl->details() % 4, v = bl->details() / 4; break;
				}

				return std::pair<int, int>(u, v);
			}

			inline bool cliff() const
			{
				for (int i(0); i < 4; ++i)
					if (!(v[i]->flags() & Heightmap::Vertex::RAMP))
						goto check_for_similar_level;
				return false;
check_for_similar_level:
				for (int i(0); i < 4; ++i)
					if (v[i]->level() != bl->level())
						return true;
				return false;
			}

			inline const Heightmap::Vertex& center() const { return *v[0]; }
			inline int min_ground() const { return min(bl->ground(), min(tl->ground(), min(tr->ground(), br->ground()))); }
			inline int max_ground() const { return max(bl->ground(), max(tl->ground(), max(tr->ground(), br->ground()))); }
			inline int min_level() const { return min(bl->level(), min(tl->level(), min(tr->level(), br->level()))); }
			inline int max_level() const { return max(bl->level(), max(tl->level(), max(tr->level(), br->level()))); }
			inline int ramps() const { return bl->ramp() + tl->ramp() + br->ramp() + tr->ramp(); }
		};

		void Serialize(const std::string& filename)
		{
			std::cout << "Saving to " << filename << std::endl;
			std::map<std::string, Model> models;
			std::map<std::string, std::vector<Doodads::Doodad> > _doodads;
			const int TESSELATION = 8;
			Writer file(filename);
			Area area("StaticMesh", file);

			for (int cliff = 0; cliff < heightmap.cliffs.size(); ++cliff)
			{
				const std::string ground = CliffsSheet.Get(heightmap.cliffs[cliff], "groundTile");
				int id = 0;
				for (; id < heightmap.grounds.size(); ++id)
				{
					if (heightmap.grounds[id] == MAKEFOURCC(ground[0], ground[1], ground[2], ground[3]))
					{
						break;
					}
				}
				if (id >= heightmap.grounds.size())
				{
					continue;
				}
				for (int x = 0; x < heightmap.size.width - 1; ++x)
				{
					for (int y = 0; y < heightmap.size.width - 1; ++y)
					{
						Tile tile(heightmap, std::pair<int, int>(x, y));
						if (!tile.cliff() || tile.v[0]->cliff()  != cliff)
						{
							continue;
						}
						for (int m = 0; m < 4; ++m)
						{
							const_cast<Heightmap::Vertex*>(tile.v[m])->data[0] = (tile.v[m]->flags() << 4) | id;
						}
					}
				}
			}

			for (std::vector<Doodads::Doodad>::const_iterator it = doodads.doodads.begin(); it != doodads.doodads.end(); ++it)
			{
				std::string filename = working_directory + (DoodadsSheet.Get(it->id, "file").length() > 0 ? DoodadsSheet.Get(it->id, "file") : DestructablesSheet.Get(it->id, "file"));

				if (FILE* test = fopen((filename + ".mdx").c_str(), "r"))
				{
					fclose(test);

					filename += ".mdx";
				}
				else
				{
					char* extension = "0.mdx";
					extension[0] += it->variation;
					filename += extension;
				}

				Model& model = models[filename];

				if (!model.loaded)
				{
					model = Model(filename);

					if (int length = DestructablesSheet.Get(it->id, "texFile").length())
					{
						memcpy(model.textures[0].path, DestructablesSheet.Get(it->id, "texFile").c_str(), length);
					}
				}

				_doodads[filename].push_back(*it);
			}

			for (int mx = 0; mx < heightmap.size.width - 1; mx += TESSELATION)
			{
				for (int my = 0; my < heightmap.size.height - 1; my += TESSELATION)
				{
					std::cout << ".";
					Area area("Mesh", file);

					///////////////////// GROUNDS /////////////////////

					for (int ground = 0; ground < heightmap.grounds.size(); ++ground)
					{
						std::vector<Quad> quads;
						{
							bool wide_texture = false;
							Area area("SubMesh", file);
							{
								Area area("Shader", file, true);
								const std::string filename
									((TerrainSheet.Get(heightmap.grounds[ground], "dir") + '\\' + TerrainSheet.Get(heightmap.grounds[ground], "file")));
								file << filename;
								D3DXIMAGE_INFO info;
								D3DXGetImageInfoFromFile((std::string(working_directory + "") + filename + ".tga").c_str(), &info);
								wide_texture = (info.Width > info.Height);
							}
							{
								Area area("Vertices", file, true);
								for (int x = mx; x < mx + TESSELATION; ++x)
								{
									for (int y = my; y < my + TESSELATION; ++y)
									{
										Tile tile(heightmap, std::pair<int, int>(x, y));

										if (tile.cliff())
										{
											continue;
										}

										const std::pair<int, int> grid(
											tile.min_ground() == ground ? std::pair<int, int>(4 + tile.bl->details() % 4, tile.bl->details() % 4) : tile.uv(ground));

										if (grid.first > -1 && grid.second > -1)
										{
											const Vector2 size(1.0f / (wide_texture ? 8.0f : 4.0f), 1.0f / 4.0f);
											const Vector2 uv = wide_texture ? Vector2(grid.first / 8.0f, grid.second / 4.0f) : 
													(grid.first >= 4 ? Vector2(0, 0) : Vector2(grid.first / 4.0f, grid.second / 4.0f));
											file
												<< Vertex(x * TILESIZE + heightmap.center.x, y * TILESIZE + heightmap.center.y, tile.bl->height(), uv.x, uv.y + size.y)
												<< Vertex(x * TILESIZE + heightmap.center.x, (y + 1) * TILESIZE + heightmap.center.y, tile.tl->height(), uv.x, uv.y)
												<< Vertex((x + 1) * TILESIZE + heightmap.center.x, (y + 1) * TILESIZE + heightmap.center.y, tile.tr->height(), uv.x + size.x, uv.y)
												<< Vertex((x + 1) * TILESIZE + heightmap.center.x, y * TILESIZE + heightmap.center.y, tile.br->height(), uv.x + size.x, uv.y + size.y);

											quads.push_back(Quad(quads.size() * 4));
										}
									}
								}
							}
							if (!area.fail(!quads.size()))
							{
								Area area("Triangles", file, true);
								for (int n = 0; n < quads.size(); ++n)
								{
									file << quads[n];
								}
							}
						}
					}

					///////////////////// CLIFFS /////////////////////

					for (int cliff = 0; cliff < heightmap.cliffs.size(); ++cliff)
					{
						std::vector<short> indices;
						short start = 0;
						Area area("SubMesh", file);
						{
							Area area("Shader", file, true);
							file << (CliffsSheet.Get(heightmap.cliffs[cliff], "texDir") + '\\' + heightmap.tileset + '_' + CliffsSheet.Get(heightmap.cliffs[cliff], "texFile"));
						}
						{
							Area area("Vertices", file, true);
							for (int x = mx; x < mx + TESSELATION; ++x)
							{
								for (int y = my; y < my + TESSELATION; ++y)
								{
									Tile tile(heightmap, std::pair<int, int>(x, y));

									if (!tile.cliff() || tile.center().cliff() != cliff)
									{
										continue;
									}

									std::string filename = working_directory + "Doodads\\Terrain\\" + ((tile.ramps() > 1) ? "CliffTrans\\CliffTrans" : "Cliffs\\Cliffs");

									for (int n(0); n < 4; ++n)
									{
										switch (tile.v[n]->level() - tile.min_level())
										{
										case 0:
											filename += (tile.ramps() > 1 && tile.v[n]->ramp()) ? 'L' : 'A';
											break;
										case 1:
											filename += (tile.ramps() > 1 && tile.v[n]->ramp()) ? 'H' : 'B';
											break;
										default:
											filename += (tile.ramps() > 1 && tile.v[n]->ramp()) ? 'X' : 'C';
										}
									}
									/*
									int count = 0;
									for (;; ++count)
									{
										char buffer[2] = { '0' + count, 0 };
										if (!std::ifstream((filename + buffer + ".mdx").c_str()).is_open())
										{
											break;
										}
									}

									{
										char buffer[2] = { '0' + rand() % count, 0 };
										filename += std::string(buffer) + ".mdx";
									}
									*/
									
									filename += "0.mdx";

									Model& model = models[filename];

									if (!model.loaded)
									{
										model = Model(filename);
									}

									int written = 0;

									for (int m(0); m < model.meshes.size(); ++m)
									{
										for (int n(0); n < model.meshes[m].vertices.size(); ++n, ++written)
										{
											float x = model.meshes[m].vertices[n].x + (tile.origin.first + 1) * TILESIZE;
											float y = model.meshes[m].vertices[n].y + (tile.origin.second) * TILESIZE;
											float z = model.meshes[m].vertices[n].z + heightmap.accurate_height(x, y) + tile.min_level() * TILESIZE;
											file << Vertex(x + heightmap.center.x, y + heightmap.center.y, z, model.meshes[m].uv[n].x, model.meshes[m].uv[n].y);
										}
									}
									for (int n(model.meshes[0].triangles.size() - 1); n >= 0; --n)
									{
										indices.push_back(model.meshes[0].triangles[n] + start);
									}

									start += written;
								}
							}
						}
						if (!area.fail(!indices.size()))
						{
							Area area("Triangles", file, true);
							for (int n = 0; n < indices.size(); ++n)
							{
								file << indices[n];
							}
						}
					}

					///////////////////// DOODADS /////////////////////

					for (std::map<std::string, std::vector<Doodads::Doodad> >::const_iterator doodad(_doodads.begin()); doodad != _doodads.end(); ++doodad)
					{
						std::list<const Doodads::Doodad*> doo;

						Bounds bounds(Vector3(mx * TILESIZE + heightmap.center.x, my * TILESIZE + heightmap.center.y, -FLT_MAX), Vector3((mx + TESSELATION) * TILESIZE + heightmap.center.x, (my + TESSELATION) * TILESIZE + heightmap.center.y, FLT_MAX));

						for (const Doodads::Doodad* start = &*doodad->second.begin(), *d = start; (d - start) < doodad->second.size(); ++d)
						{
							if (bounds.Contains(d->position))
							{
								doo.push_back(d);
							}
						}

						const Model& model = models[doodad->first];

						for (std::vector<Model::Mesh>::const_iterator mesh(model.meshes.begin()); mesh != model.meshes.end(); ++mesh)
						{
							if (doo.size() > 0 && mesh->vertices.size() > 0)
							{
								int id = 0;
								Area area("Submesh", file);
								{
									Area area("Matrices", file, true);
									for (std::list<const Doodads::Doodad*>::const_iterator inst(doo.begin()); inst != doo.end(); ++inst)
									{
										file << Matrix().SetPosition((*inst)->position).RotateYawPitchRoll(0, 0, (*inst)->angle).Scale((*inst)->scale);
										id = (*inst)->id;
									}
								}
								{
									Area area("Shader", file, true);
									std::string texture(model.textures[0].path);
									file << texture.substr(0, texture.find_last_of("."));
								}
								{
									Area area("Vertices", file, true);
									for (int n(0); n < mesh->vertices.size(); ++n)
									{
										file << Vertex(mesh->vertices[n].x, mesh->vertices[n].y, mesh->vertices[n].z, mesh->uv[n].x, mesh->uv[n].y);
									}
								}
								{
									Area area("Triangles", file, true);
									for (int n(mesh->triangles.size() - 1); n >= 0; --n)
									{
										file << mesh->triangles[n];
									}
								}
							}
						}
					}
				}
			}
		}

		Heightmap heightmap;
		Doodads doodads;
		Info info;
	};
}

#pragma pack(pop, before_WarCraft3)

namespace convert
{
	void WarCraft3(const std::string& source, const std::string& destination)
	{
		std::cout << "Loading " << source << std::endl;

		WarCraft3::Map(source).Serialize(destination);
	}
}