#pragma once

#include <map>

// Makes a 4-byte "packed ID" int out of 4 characters
#define MAKEID(d,c,b,a)					( ((int)(a) << 24) | ((int)(b) << 16) | ((int)(c) << 8) | ((int)(d)) )

// Compares a string with a 4-byte packed ID constant
#define STRING_MATCHES_ID( p, id )		( (*((int *)(p)) == (id) ) ? true : false )
#define ID_TO_STRING( id, p )			( (p)[3] = (((id)>>24) & 0xFF), (p)[2] = (((id)>>16) & 0xFF), (p)[1] = (((id)>>8) & 0xFF), (p)[0] = (((id)>>0) & 0xFF) )

//#define ARRAYSIZE(p)		(sizeof(p)/sizeof(p[0]))

// Keeps clutter down a bit, when using a float as a bit-vector
#define SETBITS(flBitVector, bits)		((flBitVector) = (int)(flBitVector) | (bits))
#define CLEARBITS(flBitVector, bits)	((flBitVector) = (int)(flBitVector) & ~(bits))
#define FBitSet(flBitVector, bit)		((flBitVector) & (bit))

#define ExecuteNTimes( nTimes, x )	\
	{								\
	static int __executeCount=0;\
	if ( __executeCount < nTimes )\
		{							\
		x;						\
		++__executeCount;		\
		}							\
	}


#define ExecuteOnce( x )			ExecuteNTimes( 1, x )


// Pad a number so it lies on an N byte boundary.
// So PAD_NUMBER(0,4) is 0 and PAD_NUMBER(1,4) is 4
#define PAD_NUMBER(number, boundary) \
	( ((number) + ((boundary)-1)) / (boundary) ) * (boundary)

// In case this ever changes
#ifndef M_PI
#	define M_PI 3.14159265358979323846
#endif

#ifdef __cplusplus
template< class T >
inline T clamp( T const &val, T const &minVal, T const &maxVal )
{
	if( val < minVal )
		return minVal;
	else if( val > maxVal )
		return maxVal;
	else
		return val;
}
#endif

// Strings.
/*
#define LINE_TERMINATOR TEXT("\r\n")
#define PATH_SEPARATOR TEXT("\\")
#define UNUSED_PATH_SEPARATOR TEXT("/")
*/

#define LINE_TERMINATOR "\r\n"
#define PATH_SEPARATOR "\\"
#define UNUSED_PATH_SEPARATOR "/"

// dinamic link libraries.
#define LIBRARY HMODULE
#define LIB_PREFIX		""
#define LIB_SUFFIX		".dll"
#define LOADLIB(file)	LoadLibrary(file)
#define UNLOADLIB(lib)	FreeLibrary((HMODULE)lib)
#define INVALID_LIBRARY	NULL
/*
#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
inline void* operator new(size_t nSize, const char*  lpszFileName, int nLine)
{
return ::operator new(nSize, _NORMAL_BLOCK, lpszFileName, nLine);
}
inline void __cdecl operator delete(void*  _P, const char*  lpszFileName, int nLine)
{
::operator delete(_P, _NORMAL_BLOCK, lpszFileName, nLine);
}
#undef DEBUG_NEW
#define DEBUG_NEW       new(__FILE__, __LINE__)
#define new DEBUG_NEW
#endif

#ifdef FUCKFUCKFUCK//_DEBUG

#define  Assert( _exp )           _Assert( _exp )
#define  AssertMsg( _exp, _msg )  _AssertMsg( _exp, _msg )
#define  AssertFunc( _exp, _f )   _AssertFunc( _exp, _f )
#define  AssertEquals( _exp, _expectedValue )              _AssertEquals( _exp, _expectedValue ) 
#define  AssertFloatEquals( _exp, _expectedValue, _tol )   _AssertFloatEquals( _exp, _expectedValue, _tol )
#define  Verify( _exp )           _Assert( _exp )

#define  AssertMsg1( _exp, _msg, a1 )									_AssertMsg( _exp, CDbgFmtMsg( _msg, a1 ) )
#define  AssertMsg2( _exp, _msg, a1, a2 )								_AssertMsg( _exp, CDbgFmtMsg( _msg, a1, a2 ) )
#define  AssertMsg3( _exp, _msg, a1, a2, a3 )							_AssertMsg( _exp, CDbgFmtMsg( _msg, a1, a2, a3 ) )
#define  AssertMsg4( _exp, _msg, a1, a2, a3, a4 )						_AssertMsg( _exp, CDbgFmtMsg( _msg, a1, a2, a3, a4 ) )
#define  AssertMsg5( _exp, _msg, a1, a2, a3, a4, a5 )					_AssertMsg( _exp, CDbgFmtMsg( _msg, a1, a2, a3, a4, a5 ) )
#define  AssertMsg6( _exp, _msg, a1, a2, a3, a4, a5, a6 )				_AssertMsg( _exp, CDbgFmtMsg( _msg, a1, a2, a3, a4, a5, a6 ) )
#define  AssertMsg6( _exp, _msg, a1, a2, a3, a4, a5, a6 )				_AssertMsg( _exp, CDbgFmtMsg( _msg, a1, a2, a3, a4, a5, a6 ) )
#define  AssertMsg7( _exp, _msg, a1, a2, a3, a4, a5, a6, a7 )			_AssertMsg( _exp, CDbgFmtMsg( _msg, a1, a2, a3, a4, a5, a6, a7 ) )
#define  AssertMsg8( _exp, _msg, a1, a2, a3, a4, a5, a6, a7, a8 )		_AssertMsg( _exp, CDbgFmtMsg( _msg, a1, a2, a3, a4, a5, a6, a7, a8 ) )
#define  AssertMsg9( _exp, _msg, a1, a2, a3, a4, a5, a6, a7, a8, a9 )	_AssertMsg( _exp, CDbgFmtMsg( _msg, a1, a2, a3, a4, a5, a6, a7, a8, a9 ) )


#else

#define  Assert( _exp )           ((void)0)
#define  AssertMsg( _exp, _msg )  ((void)0)
#define  AssertFunc( _exp, _f )   ((void)0)
#define  AssertEquals( _exp, _expectedValue )              ((void)0)
#define  AssertFloatEquals( _exp, _expectedValue, _tol )   ((void)0)
#define  Verify( _exp )			  (_exp)

#define  AssertMsg1( _exp, _msg, a1 )									((void)0)
#define  AssertMsg2( _exp, _msg, a1, a2 )								((void)0)
#define  AssertMsg3( _exp, _msg, a1, a2, a3 )							((void)0)
#define  AssertMsg4( _exp, _msg, a1, a2, a3, a4 )						((void)0)
#define  AssertMsg5( _exp, _msg, a1, a2, a3, a4, a5 )					((void)0)
#define  AssertMsg6( _exp, _msg, a1, a2, a3, a4, a5, a6 )				((void)0)
#define  AssertMsg6( _exp, _msg, a1, a2, a3, a4, a5, a6 )				((void)0)
#define  AssertMsg7( _exp, _msg, a1, a2, a3, a4, a5, a6, a7 )			((void)0)
#define  AssertMsg8( _exp, _msg, a1, a2, a3, a4, a5, a6, a7, a8 )		((void)0)
#define  AssertMsg9( _exp, _msg, a1, a2, a3, a4, a5, a6, a7, a8, a9 )	((void)0)

#endif
*/