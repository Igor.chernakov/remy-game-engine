#pragma once

#include "../interfaces/irenderer.hpp"
#include "../interfaces/iframework.hpp"
#include "../interfaces/isurface.hpp"

#include <direct.h>
#include <sstream>

#include <boost/algorithm/string/replace.hpp>

#pragma warning(disable : 4267) //conversion from 'size_t' to 'UINT', possible loss of data

static struct Renderer* pRender = NULL;

#define g_d3dd (Driver::m_pD3DDevice)

extern IClientFramework* framework;

void mod_videomemory(int value);

dim_t g_FullscreenSize;
dim_t g_WindowSize;

IRenderer::stats_t g_Stats;

int D3DGetFormatBitDepth(D3DFORMAT fmtFormat) 
{ 
	switch (fmtFormat) 
	{ 
	case D3DFMT_DXT1: 
		return 4; 
	case D3DFMT_R3G3B2: 
	case D3DFMT_A8: 
	case D3DFMT_P8: 
	case D3DFMT_L8: 
	case D3DFMT_A4L4: 
	case D3DFMT_DXT2: 
	case D3DFMT_DXT3: 
	case D3DFMT_DXT4: 
	case D3DFMT_DXT5: 
		return 8; 
	case D3DFMT_X4R4G4B4: 
	case D3DFMT_A4R4G4B4: 
	case D3DFMT_A1R5G5B5: 
	case D3DFMT_X1R5G5B5: 
	case D3DFMT_R5G6B5: 
	case D3DFMT_A8R3G3B2: 
	case D3DFMT_A8P8: 
	case D3DFMT_A8L8: 
	case D3DFMT_V8U8: 
	case D3DFMT_L6V5U5: 
	case D3DFMT_D16_LOCKABLE: 
	case D3DFMT_D15S1: 
	case D3DFMT_D16: 
	case D3DFMT_INDEX16: 
#if (DIRECT3D_VERSION == 0x0900)
	case D3DFMT_L16: 
	case D3DFMT_CxV8U8: 
	case D3DFMT_G8R8_G8B8: 
	case D3DFMT_R8G8_B8G8: 
	case D3DFMT_R16F: 
#endif
		return 16; 
	case D3DFMT_R8G8B8: 
		return 24; 
	case D3DFMT_A2W10V10U10: 
	case D3DFMT_A2B10G10R10: 
	case D3DFMT_X8R8G8B8: 
	case D3DFMT_A8R8G8B8: 
	case D3DFMT_X8L8V8U8: 
	case D3DFMT_Q8W8V8U8: 
	case D3DFMT_V16U16: 
	case D3DFMT_UYVY: 
	case D3DFMT_YUY2: 
	case D3DFMT_G16R16: 
	case D3DFMT_D32: 
	case D3DFMT_D24S8: 
	case D3DFMT_D24X8: 
	case D3DFMT_D24X4S4: 
	case D3DFMT_INDEX32: 
#if (DIRECT3D_VERSION == 0x0800)
	case D3DFMT_W11V11U10:
#elif (DIRECT3D_VERSION == 0x0900)
	case D3DFMT_A2R10G10B10: 
	case D3DFMT_D24FS8: 
	case D3DFMT_D32F_LOCKABLE: 
	case D3DFMT_MULTI2_ARGB8: 
	case D3DFMT_G16R16F: 
	case D3DFMT_R32F: 
#endif
		return 32; 
#if (DIRECT3D_VERSION == 0x0900)
	case D3DFMT_A16B16G16R16: 
	case D3DFMT_Q16W16V16U16: 
	case D3DFMT_A16B16G16R16F: 
	case D3DFMT_G32R32F: 
		return 64; 
	case D3DFMT_A32B32G32R32F: 
		return 128;
#endif
	} 
	return 0; 
} 

void allocate_surface(D3DSURFACE_DESC* desc)
{
	int a = D3DGetFormatBitDepth(desc->Format);
	int b = desc->Width;
	int c = desc->Height;
	int d = a * b * c / 8;
	mod_videomemory( d);
}

void deallocate_surface(D3DSURFACE_DESC* desc)
{
	int a = D3DGetFormatBitDepth(desc->Format);
	int b = desc->Width;
	int c = desc->Height;
	int d = a * b * c / 8;
	mod_videomemory(-d);
}

static unsigned pow2(unsigned int value)
{
	unsigned int r = 1;
	for (r; r < value; r <<= 1);
	return r;
}

struct Renderer 
	: IRenderer
{
	D3DPRESENT_PARAMETERS params;
	RECT WindowRect;
	LONG WindowStyle;
	HWND hWnd;

	bool bActive;

	mutable int nFrameNum;
	mutable int nDrawCalls;
	mutable int nTriangles;
	mutable int nMemory;
	mutable int nState;
	mutable bool bInternalVertexSource, bInternalIndexSource;
	mutable WORD nVertexOffset, nIndexOffset, nStartIndex;

	Renderer() 
		: hWnd(NULL)
		, bActive(false)
		, nFrameNum(0)
		, nMemory(0)
		, nDrawCalls(0)
		, nTriangles(0)
		, bInternalVertexSource(false)
		, bInternalIndexSource(false)
		, nVertexOffset(0)
		, nIndexOffset(0)
		, nStartIndex(0)
	{
	}

	~Renderer()
	{
		Driver::Close();
	}

	virtual bool Open(IWindow* window)
	{

		RECT ClientRect;
		hWnd = window->GetWindow();
		GetClientRect(hWnd, &ClientRect);
		GetWindowRect(hWnd, &WindowRect);
		WindowStyle = GetWindowLong(hWnd, GWL_STYLE);

		int width = 1024;//ClientRect.right - ClientRect.left;
		int height = 768;//ClientRect.bottom - ClientRect.top;

		memset(&params, 0, sizeof(D3DPRESENT_PARAMETERS));

		g_FullscreenSize = dim_t(GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
		g_WindowSize = dim_t(width, height);

		if ((float)g_FullscreenSize.width / (float)g_FullscreenSize.height < (float)width / (float)height)
		{
			g_FullscreenSize = g_WindowSize;
		}

		if (window->IsFullscreen())
		{
			params.Windowed = false;
			params.BackBufferWidth = g_FullscreenSize.width;
			params.BackBufferHeight = g_FullscreenSize.height;
		}
		else
		{
			params.Windowed = true;
			params.BackBufferWidth = g_WindowSize.width;
			params.BackBufferHeight = g_WindowSize.height;
		}
		params.BackBufferFormat = D3DFMT_X8R8G8B8;
		params.BackBufferCount = 1;
		params.MultiSampleType = D3DMULTISAMPLE_NONE;
		params.SwapEffect = D3DSWAPEFFECT_DISCARD;
		params.hDeviceWindow = hWnd;
		params.AutoDepthStencilFormat = D3DFMT_D24S8;
		params.EnableAutoDepthStencil = TRUE;
#if (DIRECT3D_VERSION == 0x0900)
		params.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
		//params.Flags = D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;
#endif

		if (!Driver::Open(hWnd, &params)){
			if (!params.Windowed) {
				params.Windowed = TRUE;
				return Driver::Open(hWnd, &params);
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	void OnActivate(bool active)
	{
		bActive = active;
	} 

	void ClearScreen(int nMask, int color, float depth, int stencil)
	{
		int mD3DMask = 0;

		if (nMask & STENCIL_BIT)
			mD3DMask |= D3DCLEAR_STENCIL;

		if (nMask & DEPTH_BIT)
			mD3DMask |= D3DCLEAR_ZBUFFER;

		if (nMask & COLOR_BIT)
			mD3DMask |= D3DCLEAR_TARGET;

		g_d3dd->Clear(0, NULL, mD3DMask, color, depth, stencil);
	}

	void StartFrame()
	{
		nFrameNum++;
		nTriangles = 0;
		nDrawCalls = 0;

		g_d3dd->BeginScene();

#if (DIRECT3D_VERSION == 0x0800)
		g_d3dd->SetVertexShader(Driver::INTERNAL_VERTEX_FORMAT);
#elif (DIRECT3D_VERSION == 0x0900)
		g_d3dd->SetFVF(Driver::INTERNAL_VERTEX_FORMAT);
#endif
		ClearScreen(DEPTH_BIT | STENCIL_BIT | COLOR_BIT, GetStats().default_color, GetStats().default_depth, GetStats().default_stencil);
	}


	void FinishFrame()
	{
		g_d3dd->EndScene();

		if (g_d3dd->Present(NULL, NULL, NULL, NULL) == D3DERR_DEVICELOST) {
			if( g_d3dd->TestCooperativeLevel() == D3DERR_DEVICENOTRESET )
				RestoreLostDevice();
		}
	}

	void TakeScreenshot()
	{
#if (DIRECT3D_VERSION == 0x0800)
		const char prefix[] = "d3d8_";
#elif (DIRECT3D_VERSION == 0x0900)
		const char prefix[] = "d3d9_";
#endif
		const char directory[] = "screenshots";

		char filename[MAX_PATH];

		_mkdir(directory);

		for (int i = 1;; i++)
		{
			sprintf_s(filename, MAX_PATH, "%s/%s%03d.bmp", directory, prefix, i);

			FILE* f;

			if (!fopen_s(&f, filename, "rb"))
				fclose(f);
			else
				break; // file doesn't exist - that's what we need
		}

		D3DXSaveSurfaceToFile(filename, D3DXIFF_BMP, Driver::GetBackBufferSurface(), NULL, NULL);
	}

	void GetMatrix(MatrixType type, float *matrix)
	{
		if (!matrix)
		{
			return;
		}

		D3DXMATRIX dx_matrix;

		D3DTRANSFORMSTATETYPE dx_type = D3DTS_PROJECTION;

		switch (type)
		{
		case MATRIX_PROJECTION:
			dx_type = D3DTS_PROJECTION;
			break;

		case MATRIX_MODELVIEW:
			dx_type = D3DTS_VIEW;
			break;

		case MATRIX_TEXTURE:
			dx_type = D3DTS_TEXTURE0;
			break;
		};

		g_d3dd->GetTransform(dx_type, &dx_matrix);

		memcpy(matrix, &dx_matrix, 16 * sizeof(float));
	}

	void SetMatrix(MatrixType type, const float *matrix)
	{
		if (!matrix)
		{
			return;
		}

		D3DXMATRIX dx_matrix;
		D3DTRANSFORMSTATETYPE dx_type = D3DTS_PROJECTION;

		switch (type)
		{
		case MATRIX_PROJECTION:
			dx_type = D3DTS_PROJECTION;
			break;

		case MATRIX_MODELVIEW:
			dx_type = D3DTS_VIEW;
			break;

		case MATRIX_TEXTURE:
			dx_type = D3DTS_TEXTURE0;
			break;
		};

		g_d3dd->SetTransform(dx_type, &D3DXMATRIX(matrix));
	}

	rect_t GetViewport()
	{
		D3DVIEWPORT viewport;

		g_d3dd->GetViewport(&viewport);

		rect_t rect;

		rect.left = viewport.X;
		rect.right = viewport.X + viewport.Width;
		rect.top = viewport.Y;
		rect.bottom = viewport.Y + viewport.Height;

		return rect;
	}

	void SetViewport(const rect_t &rect)
	{
		D3DVIEWPORT viewport;

		g_d3dd->GetViewport(&viewport);

		viewport.X = rect.left;
		viewport.Y = rect.top;
		viewport.Width = rect.right - rect.left;
		viewport.Height = rect.bottom - rect.top;

		g_d3dd->SetViewport(&viewport);
	}

	ITexture::Format GetBackBufferFormat()
	{
		return (ITexture::Format)Driver::m_Params.BackBufferFormat;
	}

#if 0
	bool CopyBackBuffer(const ITexture* texture, const rect_t& r)
	{
		if (!texture || !texture->render_info)
			return false;

		const POINT mDest = { 0, 0 };

		LPDIRECT3DSURFACE pRenderTarget;
		LPDIRECT3DSURFACE pBackBuffer;

		((LPDIRECT3DTEXTURE)(texture->render_info))->GetSurfaceLevel(0, &pRenderTarget);

		RECT mRect = r;

		pBackBuffer = Driver::GetBackBufferSurface();

		/*
		D3DLOCKED_RECT src_rect, dst_rect;

		pBackBuffer->LockRect(&src_rect, &mRect, D3DLOCK_READONLY);
		pRenderTarget->LockRect(&dst_rect, &mRect, D3DLOCK_DISCARD);

		memcpy(dst_rect.pBits, src_rect.pBits, texture->width * texture->height * 
			D3DGetFormatBitDepth(Driver::m_Params.BackBufferFormat));

		pBackBuffer->UnlockRect();
		pRenderTarget->UnlockRect();
		*/
		
#if (DIRECT3D_VERSION == 0x0800)
		STREAM(g_d3dd->CopyRects(pBackBuffer, &mRect, 1, pRenderTarget, &mDest));
#elif (DIRECT3D_VERSION == 0x0900)
		LPDIRECT3DSURFACE pSysSurface;
		//DWORD bMultisample = 0;
		//g_d3dd->GetRenderState(D3DRS_MULTISAMPLEANTIALIAS, &bMultisample);
		//if (bMultisample) {
		//	g_d3dd->SetRenderState(D3DRS_MULTISAMPLEANTIALIAS, FALSE);
		//}
		Driver::m_pSysTexture->GetSurfaceLevel(0, &pSysSurface);
		STREAM(g_d3dd->GetRenderTargetData(pBackBuffer, pSysSurface));
		//STREAM(g_d3dd->UpdateSurface(pSysSurface, NULL, pRenderTarget, NULL));
		//STREAM(g_d3dd->StretchRect(pBackBuffer, NULL, pRenderTarget, NULL, D3DTEXF_NONE));
		//if (bMultisample) {
		//	g_d3dd->SetRenderState(D3DRS_MULTISAMPLEANTIALIAS, TRUE);
		//}
		D3DLOCKED_RECT src, dst;
		RECT area = {0, 0, params.BackBufferWidth, params.BackBufferHeight};
		STREAM(pSysSurface->LockRect(&src, &area, D3DLOCK_READONLY));
		STREAM(pRenderTarget->LockRect(&dst, &area, 0));
		int height = params.BackBufferHeight > texture->height ? texture->height : params.BackBufferHeight;
		char* psrc = (char*)src.pBits;
		char* pdst = (char*)dst.pBits;
		for (int i = 0; i < height; ++i, psrc += src.Pitch, pdst += dst.Pitch)
		{
			memcpy(pdst, psrc, dst.Pitch);
		}
		pSysSurface->UnlockRect();
		pRenderTarget->UnlockRect();
#endif
		
		/*STREAM(D3DXFilterTexture(
			(LPDIRECT3DBASETEXTURE)texture->render_info,
			NULL,
			D3DX_DEFAULT,
			D3DX_FILTER_BOX));*/


		pBackBuffer->Release();
		pRenderTarget->Release();

		return true;
	}
#endif

	bool CreateTexture(ITexture *texture)
	{
#if (DIRECT3D_VERSION == 0x0800)
		STREAM(g_d3dd->CreateTexture(
			texture->width,
			texture->height,
			texture->miplevels,
			texture->usage,
			(D3DFORMAT)texture->format,
			(D3DPOOL)texture->pool,
			(LPDIRECT3DTEXTURE*)&texture->render_info));
#elif (DIRECT3D_VERSION == 0x0900)
		STREAM(g_d3dd->CreateTexture(
			texture->width,
			texture->height,
			texture->miplevels,
			texture->usage,
			(D3DFORMAT)texture->format,
			(D3DPOOL)texture->pool,
			(LPDIRECT3DTEXTURE*)&texture->render_info, NULL));
#endif

		if (texture->render_info)
		{
			for (UINT i = 0; i < texture->miplevels; i++)
			{
				D3DSURFACE_DESC desc;

				((LPDIRECT3DTEXTURE)texture->render_info)->GetLevelDesc(i, &desc);

				allocate_surface(&desc);
			}
			return true;
		}
		else
		{
			return false;
		}
	}

	bool ReleaseTexture(ITexture* texture)
	{
		if (LPDIRECT3DTEXTURE dxtexture = ((LPDIRECT3DTEXTURE)(texture->render_info)))
		{
			for (UINT i = 0; i < texture->miplevels; i++)
			{
				D3DSURFACE_DESC desc;
				dxtexture->GetLevelDesc(i,&desc);
				deallocate_surface(&desc);
			}

			dxtexture->Release();
			
			texture->render_info = 0;

			return true;
		}
		else
		{
			return false;
		}

	}

	void* LockTexture(ITexture* texture)
	{
		if (!texture->render_info)
			return 0;

		D3DLOCKED_RECT locked_rect;

		STREAM(((LPDIRECT3DTEXTURE)(texture->render_info))->LockRect(0, &locked_rect, 0, 0));

		return locked_rect.pBits;
	}

	void UnlockTexture(ITexture* texture) {
		if (!texture->render_info)
			return;
		STREAM(((LPDIRECT3DTEXTURE)(texture->render_info))->UnlockRect(0));
	}

	bool FillTextureParams(ITexture* texture, const char *mem, int size)
	{
		D3DXIMAGE_INFO info;

		memset(&info, 0, sizeof(info));

		STREAM(D3DXGetImageInfoFromFileInMemory(mem, size, &info));

		if (info.Width == 0)
			return false;

		texture->width = info.Width;
		texture->height = info.Height;
		texture->format = (ITexture::Format)info.Format;

		return true;
	}

	bool CompileShader(IShader* shader)
	{
		return true;
	}

	bool LoadVertexShader(IShader* shader, const char *mem, int size)
	{
#if (DIRECT3D_VERSION == 0x0800)

		DWORD dwDecl[] = {
			D3DVSD_STREAM(0),
			D3DVSD_REG( D3DVSDE_POSITION,    D3DVSDT_FLOAT3 ),           // D3DVSDE_POSITION = 0
			D3DVSD_REG( D3DVSDE_BLENDWEIGHT, D3DVSDT_FLOAT4 ),           // D3DVSDE_BLENDWEIGHT = 1
			D3DVSD_REG( D3DVSDE_BLENDINDICES,D3DVSDT_UBYTE4 ),		 	 // D3DVSDE_BLENDINDICES = 2
			D3DVSD_REG( D3DVSDE_NORMAL,      D3DVSDT_FLOAT3 ),           // D3DVSDE_NORMAL = 3
			D3DVSD_REG( D3DVSDE_DIFFUSE,     D3DVSDT_D3DCOLOR ),		 // D3DVSDE_DIFFUSE = 5
			D3DVSD_REG( D3DVSDE_TEXCOORD0,   D3DVSDT_FLOAT2 ),           // D3DVSDE_TEXCOORD0= 7
			D3DVSD_REG( D3DVSDE_TEXCOORD1,   D3DVSDT_FLOAT2 ),           // D3DVSDE_TEXCOORD1= 8
			D3DVSD_END()
		};

		LPD3DXBUFFER code = NULL;
		LPD3DXBUFFER errors = NULL;
		LPD3DXBUFFER constants = NULL;
		DWORD shaderOut = 0;

		const char* skinning_code =
			"; Prepare blended vertex register\n"
			"mov r0, v0\n"

			"; Compute the 1st bone transform\n"
			"\n"
			"; Obtain the 1st bone index\n"
			"mov a0.x, v2.x\n"
			"\n"
			"; Transform the vertex by this bone\n"
			"dp4 r1.x, v0, c[a0.x + 6]\n"
			"dp4 r1.y, v0, c[a0.x + 7]\n"
			"dp4 r1.z, v0, c[a0.x + 8]\n"
			"\n"
			"; Scale by the beta weight\n"
			"mul r0.xyz, r1.xyz, v1.x\n"
			"\n"

			"; Compute the 2nd bone transform\n"
			"\n"
			"; Obtain the 2nd bone index\n"
			"mov a0.x, v2.y\n"
			"\n"
			"; Transform the vertex by this bone\n"
			"dp4 r1.x, v0, c[a0.x + 6]\n"
			"dp4 r1.y, v0, c[a0.x + 7]\n"
			"dp4 r1.z, v0, c[a0.x + 8]\n"
			"\n"
			"; Scale by the beta weight and blend\n"
			"mad r0.xyz, r1.xyz, v1.y, r0.xyz\n"
			"\n"

			"; Compute the 3rd bone transform\n"
			"\n"
			"; Obtain the 3rd bone index\n"
			"mov a0.x, v2.z\n"
			"\n"
			"; Transform the vertex by this bone\n"
			"dp4 r1.x, v0, c[a0.x + 6]\n"
			"dp4 r1.y, v0, c[a0.x + 7]\n"
			"dp4 r1.z, v0, c[a0.x + 8]\n"
			"\n"
			"; Scale by the beta weight and blend\n"
			"mad r0.xyz, r1.xyz, v1.z, r0.xyz\n"
			"\n"

			"; Compute the 4th bone transform\n"
			"\n"
			"; Obtain the 4th bone index\n"
			"mov a0.x, v3.w\n"
			"\n"
			"; Transform the vertex by this bone\n"
			"dp4 r1.x, v0, c[a0.x + 6]\n"
			"dp4 r1.y, v0, c[a0.x + 7]\n"
			"dp4 r1.z, v0, c[a0.x + 8]\n"
			"\n"
			"; Scale by the beta weight and blend\n"
			"mad r0.xyz, r1.xyz, v1.w, r0.xyz\n";

		std::string shader_code = boost::replace_all_copy(std::string(mem, size), ";SKINING_CODE_HERE", skinning_code);

		D3DXAssembleShader( shader_code.c_str(), shader_code.size(), 0, &constants, &code, &errors );

		if( errors != NULL ) {
			framework->Log(IFramework::MSG_ERROR, "Vertex Shader: %s", (const char*)errors->GetBufferPointer());
			errors->Release(); 
		}
		
		if( code != NULL ) {
			g_d3dd->CreateVertexShader( dwDecl, (DWORD*)code->GetBufferPointer(), &shaderOut, 0 );
			shader->constants = (unsigned int)constants;
			shader->vertex = (unsigned int)shaderOut;
			code->Release(); 
			return true;
		} else {
			return false;
		}

		return false;
#elif (DIRECT3D_VERSION == 0x0900)
		const char* header = 
			"float4x3 MatrixPalette[MATRIX_PALETTE_SIZE];\n"
			"float4 vs_skin( in float4 vInPosition, in float4 vInBlendWeight, in float4 vInBlendIndices ) {\n"
			"	int aiIndices[4] = (int[4]) D3DCOLORtoUBYTE4( vInBlendIndices );\n"
			"	float afBlendWeights[4] = (float[4]) vInBlendWeight;\n"
			"	float3 vOutPosition = mul( vInPosition, MatrixPalette[aiIndices[0]] ) * afBlendWeights[0];\n"
			"	if (afBlendWeights[1] > 0) {\n"
			"		vOutPosition += mul( vInPosition, MatrixPalette[aiIndices[1]] ) * afBlendWeights[1];\n"
			"		if (afBlendWeights[2] > 0) {\n"
			"			vOutPosition += mul( vInPosition, MatrixPalette[aiIndices[2]] ) * afBlendWeights[2];\n"
			"		}\n"
			"	}\n"
			"	return float4(vOutPosition, vInPosition.w);\n"
			"}\n";

		int header_size = strlen(header);
		char* combined = new char[header_size + size];

		memcpy(combined, header, header_size);
		memcpy(combined + header_size, mem, size);

		LPD3DXCONSTANTTABLE constantTable;
		LPDIRECT3DVERTEXSHADER9 vertexShader;
		LPD3DXBUFFER code = NULL, errors = NULL;
		const D3DXMACRO macros[] = { 
			{ "VERTEX_SHADER", "1" }, 
			{ "MATRIX_PALETTE_SIZE", "29" }, 
			{ "PIXEL_SHADER_SUPPORTED", Driver::b_PixelShadersSupported ? "1" : "0" }, 
			{ 0, 0 }
		};
		STREAM(D3DXCompileShader(combined, header_size + size, macros, NULL, "vs_main", "vs_1_1", 0, &code, &errors, &constantTable));
		delete[] combined;

		if (errors)
		{
			framework->Log(IFramework::MSG_ERROR, "Vertex Shader: %s", (const char*)errors->GetBufferPointer());
			errors->Release();
		}
		if (code)
		{
			STREAM(g_d3dd->CreateVertexShader((DWORD*)code->GetBufferPointer(), &vertexShader));
			code->Release();
			if (vertexShader == NULL)
				return false;
			shader->constants = (unsigned int)constantTable;
			shader->vertex = (unsigned int)vertexShader;
			return true;
		}
		else 
		{
			return false;
		}
#endif
	}

	bool LoadPixelShader(IShader* shader, const char *mem, int size)
	{
#if (DIRECT3D_VERSION == 0x0800)
		return false;
		LPD3DXBUFFER code = NULL;
		LPD3DXBUFFER errors = NULL;
		LPD3DXBUFFER constants = NULL;
		DWORD shaderOut = 0;

		D3DXAssembleShader( mem, size, 0, &constants, &code, &errors );

		if( errors != NULL ) {
			framework->Log(IFramework::MSG_ERROR, "Pixel Shader: %s", (const char*)errors->GetBufferPointer());
			errors->Release(); 
		}
		
		if( code != NULL ) {
			g_d3dd->CreatePixelShader( (DWORD*)code->GetBufferPointer(), &shaderOut);
			shader->constants2 = (unsigned int)constants;
			shader->pixel = (unsigned int)shaderOut;
			code->Release(); 
			return true;
		} else {
			return false;
		}
#elif (DIRECT3D_VERSION == 0x0900)
		if (!Driver::b_PixelShadersSupported)
			return false;
		LPD3DXCONSTANTTABLE constantTable;
		LPDIRECT3DPIXELSHADER9 pixelShader;
		LPD3DXBUFFER code = NULL, errors = NULL;
		const D3DXMACRO macros[] = { { "PIXEL_SHADER", "1" }, { 0, 0 } };
		STREAM(D3DXCompileShader(mem, size, macros, NULL, "ps_main", "ps_2_0", 0, &code, &errors, &constantTable));
		if (errors)
		{
			framework->Log(IFramework::MSG_ERROR, "Pixel Shader: %s", (char*)errors->GetBufferPointer(), errors->GetBufferSize());
			errors->Release();
		}
		if (code)
		{
			STREAM(g_d3dd->CreatePixelShader((DWORD*)code->GetBufferPointer(), &pixelShader));
			code->Release();
			if (pixelShader == NULL)
				return false;
			shader->constants2 = (unsigned int)constantTable;
			shader->pixel = (unsigned int)pixelShader;
			return true;
		}
		else 
		{
			return false;
		}
#endif
	}

	bool ReleaseShader(IShader* shader)
	{
#if (DIRECT3D_VERSION == 0x0800)
		if( shader->vertex )
		{
			g_d3dd->DeleteVertexShader( (DWORD)shader->vertex );
			shader->vertex = NULL;
		}
		if( shader->pixel )
		{
			g_d3dd->DeletePixelShader( (DWORD)shader->pixel );
			shader->pixel = NULL;
		}
		if( shader->constants )
		{
			((LPD3DXBUFFER)shader->constants)->Release();
			shader->constants = NULL;
		}
		if (shader->constants2)
		{
			((LPD3DXBUFFER)shader->constants2)->Release();
			shader->constants2 = NULL;
		}
		return true;
#elif (DIRECT3D_VERSION == 0x0900)
		if (shader->constants)
		{
			((LPD3DXCONSTANTTABLE)shader->constants)->Release();
			shader->constants = NULL;
		}
		if (shader->constants2)
		{
			((LPD3DXCONSTANTTABLE)shader->constants2)->Release();
			shader->constants2 = NULL;
		}
		if (shader->vertex)
		{
			((LPDIRECT3DVERTEXSHADER9)shader->vertex)->Release();
			shader->vertex = NULL;
		}
		if (shader->pixel)
		{
			((LPDIRECT3DPIXELSHADER9)shader->pixel)->Release();
			shader->pixel = NULL;
		}
		return true;
#endif
	}

	bool LoadTexture(ITexture* texture, const char *mem, int size)
	{
		texture->miplevels = 1; // HACK!

		FillTextureParams(texture, mem, size);

		STREAM(D3DXCreateTextureFromFileInMemoryEx(
			g_d3dd, 
			mem, 
			size, 
			texture->width,
			texture->height,
			texture->miplevels,
			texture->usage,
			(D3DFORMAT)texture->format,
			(D3DPOOL)texture->pool,
			D3DX_DEFAULT,
			D3DX_DEFAULT,
			(D3DCOLOR)0,
			NULL,
			NULL,
			(LPDIRECT3DTEXTURE*)&texture->render_info));

		if (texture->render_info)
		{
			texture->miplevels = ((LPDIRECT3DTEXTURE)texture->render_info)->GetLevelCount();
			
			for (UINT i = 0; i < texture->miplevels; i++)
			{
				D3DSURFACE_DESC desc;
				((LPDIRECT3DTEXTURE)texture->render_info)->GetLevelDesc(i, &desc);
				texture->format = (texture->format == ITexture::FMT_UNKNOWN) ? (ITexture::Format)desc.Format : texture->format;
				allocate_surface(&desc);
			}
			return true;
		}
		else
		{
			return false;
		}
	}

	bool CreateBuffer(IRendererBuffer* buffer)
	{
		if (!buffer)
			return false;

		void* mem = buffer->GetDataPointer();
		size_t size = buffer->GetSize();

		switch (buffer->type)
		{
		case IRendererBuffer::VERTEX_BUFFER:
			buffer->render_info = (unsigned int)_CreateVertexBuffer(size);
			if (!buffer->render_info)
				return false;
			_FillVertexBuffer((void*)buffer->render_info, mem, size);
			break;
		case IRendererBuffer::INDEX_BUFFER:
			buffer->render_info = (unsigned int)_CreateIndexBuffer(size);
			if (!buffer->render_info)
				return false;
			_FillIndexBuffer((void*)buffer->render_info, mem, size);
			break;
		default:
			return false;
		}

		return true;
	}

	void ReleaseBuffer(IRendererBuffer* buffer)
	{
		if (!buffer)
			return;

		switch (buffer->type)
		{
		case IRendererBuffer::VERTEX_BUFFER:
			_DeleteVertexBuffer((void*)buffer->render_info);
			break;
		case IRendererBuffer::INDEX_BUFFER:
			_DeleteIndexBuffer((void*)buffer->render_info);
			break;
		default:
			return;
		}

		buffer->render_info = NULL;
	}

	void *_CreateVertexBuffer(size_t size)
	{
		LPDIRECT3DVERTEXBUFFER vb;
		g_d3dd->CreateVertexBuffer(size, D3DUSAGE_WRITEONLY, Driver::INTERNAL_VERTEX_FORMAT, D3DPOOL_MANAGED, &vb
#if (DIRECT3D_VERSION == 0x0900)
			, NULL
#endif
		);

		if (vb)
		{
			D3DVERTEXBUFFER_DESC desc;
			vb->GetDesc(&desc);
			nMemory += desc.Size;
		}

		return vb;
	}

	void *_CreateIndexBuffer(size_t size) {
		LPDIRECT3DINDEXBUFFER ib;
		g_d3dd->CreateIndexBuffer(size, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &ib
#if (DIRECT3D_VERSION == 0x0900)
			, NULL
#endif
		);

		if (ib) {
			D3DINDEXBUFFER_DESC desc;
			ib->GetDesc(&desc);
			nMemory += desc.Size;
		}

		return ib;
	}

	void _FillVertexBuffer(void *vb, void *src, size_t size) {
		if (!vb)
			return;

		VOID *pVertices = NULL;

#if (DIRECT3D_VERSION == 0x0800)
		((LPDIRECT3DVERTEXBUFFER) vb)->Lock (0, size, (BYTE**)&pVertices, 0);
#elif (DIRECT3D_VERSION == 0x0900)
		((LPDIRECT3DVERTEXBUFFER) vb)->Lock (0, size, (void**)&pVertices, 0);
#endif
		memcpy (pVertices, src, size);
		((LPDIRECT3DVERTEXBUFFER) vb)->Unlock();	
	}

	void _FillIndexBuffer(void *ib, void *src, size_t size) {
		if (!ib)
			return;

		VOID *pIndices = NULL;

#if (DIRECT3D_VERSION == 0x0800)
		((LPDIRECT3DINDEXBUFFER) ib)->Lock (0, size, (BYTE**)&pIndices, 0);
#elif (DIRECT3D_VERSION == 0x0900)
		((LPDIRECT3DINDEXBUFFER) ib)->Lock (0, size, (void**)&pIndices, 0);
#endif
		memcpy (pIndices, src, size);
		((LPDIRECT3DINDEXBUFFER) ib)->Unlock();
	}

	void _DeleteVertexBuffer(void *vb) {
		D3DVERTEXBUFFER_DESC desc;

		((LPDIRECT3DVERTEXBUFFER) vb)->GetDesc(&desc);
		((LPDIRECT3DVERTEXBUFFER) vb)->Release();

		nMemory -= desc.Size;
	}

	void _DeleteIndexBuffer(void *ib) {
		D3DINDEXBUFFER_DESC desc;

		((LPDIRECT3DINDEXBUFFER) ib)->GetDesc(&desc);
		((LPDIRECT3DINDEXBUFFER) ib)->Release();

		nMemory -= desc.Size;
	}

	bool __hack__has_team_color;
	bool __hack__has_overlay_color;

	void PushSurface(const ISurface* s)
	{
		if (!s || !s->GetVerticesCount())
			return;

		if (s->GetVerticesCount() > Driver::VERTEX_BUFFER_SIZE)
			return;

		if (s->GetIndicesCount() > Driver::INDEX_BUFFER_SIZE)
			return;

		const IMaterial* material = s->GetMaterial();

		__hack__has_team_color = false;
		__hack__has_overlay_color = false;

		ProcessMatrix(s);
		ProcessFillMode(s);
		ProcessLight(s);
		ProcessMaterial(s);
		ProcessShader(s);

		ProcessColors(material);
		ProcessCulling(material);
		ProcessDepth(material);
		ProcessStencil(material);
		ProcessAlphaFunc(s, material);
		//ProcessAliasing(material);

		RenderLayers(s, material);
	}
#if (DIRECT3D_VERSION == 0x0900)
	void SetupConstTable(const ISurface* surface, LPD3DXCONSTANTTABLE constantTable)
	{
			if (constantTable)
			{
				D3DXMATRIXA16 matWorld, matView, matProj;
				g_d3dd->GetTransform(D3DTS_WORLD, &matWorld);
				g_d3dd->GetTransform(D3DTS_VIEW, &matView);
				g_d3dd->GetTransform(D3DTS_PROJECTION, &matProj);

				vec4_t overrideColor;

				D3DXMATRIXA16 matWorldViewProj = matWorld * matView * matProj;
				constantTable->SetMatrixArray(g_d3dd, "WorldViewProj", &matWorldViewProj, 1);

				if (surface->GetVariablesCount() > 0)
				{
					const ISurface::variable_t *variable = surface->GetVariables();
					for (int i = 0; i < surface->GetVariablesCount(); ++i, ++variable)
					{
						if (!Driver::b_PixelShadersSupported)
						{
							if (strcmp(variable->name, "OverlayColor") == 0 && ((float*)variable->value)[3] > 0.95f) {
								__hack__has_overlay_color = true;
								overrideColor = vec4_t((float*)variable->value);
							} else if (!__hack__has_overlay_color && strcmp(variable->name, "TeamColor") == 0) {
								__hack__has_team_color = true;
								overrideColor = vec4_t((float*)variable->value);
							}
						}

						switch (variable->vartype)
						{
#define PROCESS_VARTYPE(TYPE, TYPECAPITAL, D3DXTYPE) \
	case ISurface::VARTYPE_##TYPECAPITAL: \
		constantTable->Set##TYPE##Array(g_d3dd, variable->name, (const D3DXTYPE*)variable->value, variable->count); \
		break;
							PROCESS_VARTYPE(Int, INT, INT)
							PROCESS_VARTYPE(Bool, BOOL, BOOL)
							PROCESS_VARTYPE(Float, FLOAT, FLOAT)
							PROCESS_VARTYPE(Vector, VECTOR4, D3DXVECTOR4)
							PROCESS_VARTYPE(Matrix, MATRIX, D3DXMATRIX)
#undef PROCESS_VARTYPE
						}
						
					}
				}

				if (__hack__has_overlay_color || __hack__has_team_color)
				{
					constantTable->SetVectorArray(g_d3dd, "Color", (const D3DXVECTOR4*)&overrideColor, 1);
				}
			}

	}
#else
	void SetupConstTable(const ISurface* surface, LPD3DXBUFFER constantTable)
	{
		D3DXMATRIX matWorld, matView, matProj;
		g_d3dd->GetTransform(D3DTS_WORLD, &matWorld);
		g_d3dd->GetTransform(D3DTS_VIEW, &matView);
		g_d3dd->GetTransform(D3DTS_PROJECTION, &matProj);

		D3DXMATRIX matWorldViewProj = matWorld * matView * matProj;

		D3DXMatrixTranspose( &matWorldViewProj, &matWorldViewProj );

		// Load the combined model-view-projection matrix in registers c[0]-c[3]
		g_d3dd->SetVertexShaderConstant( 0, &matWorldViewProj, 4 );
	    
		// Load a constant color into register c[4]
		//float fColor[4] = { 0.0f, 1.0f, 0.0f, 0.0f };
		//g_d3dd->SetVertexShaderConstant( 4, fColor, 1 );

		if (surface->GetVariablesCount() > 0) {
			const ISurface::variable_t *variable = surface->GetVariables();
			for (int i = 0; i < surface->GetVariablesCount(); ++i, ++variable) {
				if (strcmp(variable->name, "MatrixPalette") == 0) {
					for (int i = 0; i < variable->count; i++) {
						D3DXMATRIX mat = *((D3DXMATRIX*)&((mat4_t*)variable->value)[i]);
						D3DXMatrixTranspose(&mat, &mat);
						g_d3dd->SetVertexShaderConstant(6 + i * 3, &mat, 3);
					}
				} else if (strcmp(variable->name, "SurfaceColor") == 0) {
					g_d3dd->SetVertexShaderConstant(4, variable->value, 3);
				} else if (strcmp(variable->name, "TeamColor") == 0) {
					g_d3dd->SetVertexShaderConstant(5, variable->value, 3);
					__hack__has_team_color = true;
				}
			}
		}

		/*int const_count = 4; // after adding model-view-projection matrix

		if (surface->GetVariablesCount() > 0)
		{
			const ISurface::variable_t *variable = surface->GetVariables();
			for (int i = 0; i < surface->GetVariablesCount(); ++i, ++variable)
			{
				switch (variable->vartype)
				{
					//case ISurface::VARTYPE_VECTOR4:
					//	for (int i = 0; i < variable->count; const_count += 1, i++) {
					//		g_d3dd->SetVertexShaderConstant( const_count, &((vec4_t*)variable->value)[i], 1 );
					//	}
					//	break;
					case ISurface::VARTYPE_MATRIX:
						for (int i = 0; i < variable->count; const_count += 3, i++) {
							D3DXMATRIX mat = *((D3DXMATRIX*)&((mat4_t*)variable->value)[i]);
							D3DXMatrixTranspose(&mat, &mat);
							g_d3dd->SetVertexShaderConstant(const_count, &mat, 3);
						}
						break;
				}
			}
		}*/
	}
#endif

	inline void ProcessShader(const ISurface* surface)
	{
#if (DIRECT3D_VERSION == 0x0900)
		const IMaterial* material = surface->GetMaterial();
		if (material && material->GetShader())
		{
			SetupConstTable(surface, (LPD3DXCONSTANTTABLE)material->GetShader()->constants);
			SetupConstTable(surface, (LPD3DXCONSTANTTABLE)material->GetShader()->constants2);

			g_d3dd->SetVertexDeclaration(Driver::p_VertexDecl);
			g_d3dd->SetVertexShader((LPDIRECT3DVERTEXSHADER9)material->GetShader()->vertex);
			g_d3dd->SetPixelShader((LPDIRECT3DPIXELSHADER9)material->GetShader()->pixel);
		}
		else
		{
			g_d3dd->SetFVF(Driver::INTERNAL_VERTEX_FORMAT);
			g_d3dd->SetVertexShader(NULL);
			g_d3dd->SetPixelShader(NULL);
		}
#else
		const IMaterial* material = surface->GetMaterial();
		if (material && material->GetShader())
		{
			SetupConstTable(surface, (LPD3DXBUFFER)material->GetShader()->constants);

			g_d3dd->SetVertexShader((DWORD)material->GetShader()->vertex);
			g_d3dd->SetPixelShader((DWORD)material->GetShader()->pixel);
		}
		else
		{
			g_d3dd->SetVertexShader(Driver::INTERNAL_VERTEX_FORMAT);
			g_d3dd->SetPixelShader(0);
		}

		/*g_d3dd->SetRenderState( D3DRS_INDEXEDVERTEXBLENDENABLE, FALSE );
		g_d3dd->SetRenderState( D3DRS_VERTEXBLEND, D3DVBF_DISABLE );

		if (surface->GetVariablesCount() > 0) {
			const ISurface::variable_t *variable = surface->GetVariables();
			for (int i = 0; i < surface->GetVariablesCount(); ++i, ++variable) {
				if (strcmp(variable->name, "MatrixPalette") == 0 && variable->vartype == ISurface::VARTYPE_MATRIX) {
					g_d3dd->SetRenderState( D3DRS_VERTEXBLEND, D3DVBF_3WEIGHTS );
					g_d3dd->SetRenderState( D3DRS_INDEXEDVERTEXBLENDENABLE, TRUE );
					for (int i = 0; i < variable->count; i++) {
						D3DXMATRIX mat;
						mat = *((D3DXMATRIX*)&((mat4_t*)variable->value)[i]);
						//D3DXMatrixMultiply( &mat, ((D3DXMATRIX*)&((mat4_t*)variable->value)[i]), ((D3DXMATRIX*)surface->GetMatrix()));
						g_d3dd->SetTransform( D3DTS_WORLDMATRIX(i), &mat );
					}
					break;
				}
			}
		}*/

#endif
	}

	inline void ProcessAliasing(const IMaterial* material)
	{
		if (params.MultiSampleType == D3DMULTISAMPLE_NONE)
			return;

		g_d3dd->SetRenderState(D3DRS_MULTISAMPLEANTIALIAS, 
			(material && material->surfaceflags & IMaterial::SF_NO_ALIASING) ? TRUE : FALSE);
	}

	inline void ProcessMaterial(const ISurface* s)
	{
		D3DMATERIAL material;

		ZeroMemory( &material, sizeof(D3DMATERIAL) );

		material.Diffuse = D3DXCOLOR(s->color.color);
		material.Ambient = D3DXCOLOR(s->color.color);

		g_d3dd->SetMaterial( &material );
	}

	inline void RenderLayers(const ISurface* s, const IMaterial* material)
	{
		for (int i = 0; i < (material ? material->GetLayersCount() : 1); i++)
		{
			bool multitexturing = material &&
				!(material->surfaceflags & IMaterial::SF_NO_MULTITEXTURE) &&
				i < material->GetLayersCount() - 1;

			const ILayer* layer = material ? material->GetLayer(i) : NULL;

			ProcessTexture(s, layer, 0);
			ProcessBlending(s, layer);

			if (multitexturing)
			{
				ProcessStreams(s, i, 2);
				ProcessTexture(s, material->GetLayer(++i), 1);
				RenderSurface(s);
				g_d3dd->SetTexture(1, NULL);
			}
			else
			{
				ProcessStreams(s, i, material ? 1 : 0);
				RenderSurface(s);

#if (DIRECT3D_VERSION == 0x0800)
				if (material && material->stencil.back.func != IMaterial::FUNC_UNKNOWN)
				{
					g_d3dd->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
					ProcessStencilCCW(material);

					RenderSurface(s);

					ProcessCulling(material);
					ProcessStencil(material);
					break;
				}
#endif
			}
		}

	}

	inline vec3_t MulMat3(const mat4_t& m, const vec3_t& v) {
		float fInvW = 1.0f / ( m.v[0][3] * v.x + m.v[1][3] * v.y + m.v[2][3] * v.z + m.v[3][3] );

		return vec3_t(
			( m.v[0][0] * v.x + m.v[1][0] * v.y + m.v[2][0] * v.z ) * fInvW,
			( m.v[0][1] * v.x + m.v[1][1] * v.y + m.v[2][1] * v.z ) * fInvW,
			( m.v[0][2] * v.x + m.v[1][2] * v.y + m.v[2][2] * v.z ) * fInvW);
	}

	inline void ProcessLight(const ISurface *s) 
	{
#if (DIRECT3D_VERSION == 0x0800)
		g_d3dd->SetRenderState( D3DRS_LIGHTING, FALSE );
		return;
#endif
		if (!s->light || !s->light->enabled) {
			g_d3dd->LightEnable( 0, FALSE );
			g_d3dd->SetRenderState( D3DRS_AMBIENT, D3DCOLOR_XRGB(255, 255, 255) );
			g_d3dd->SetRenderState( D3DRS_LIGHTING, FALSE );
		} else {
			// Fill in a light structure defining our light
			D3DLIGHT light;

			ZeroMemory( &light, sizeof(D3DLIGHT) );

			light.Type = (D3DLIGHTTYPE)s->light->type;
			light.Diffuse = D3DXCOLOR((s->light->diffuse * s->color).color);
			light.Specular = D3DXCOLOR((s->light->specular * s->color).color);
			//light.Ambient = D3DXCOLOR(s->light->ambient.color);
			light.Position = D3DXVECTOR3(s->light->position.v);
			light.Direction = D3DXVECTOR3(s->light->direction.v);
			light.Range = s->light->range.cutoff;
			light.Falloff = s->light->range.falloff;
			light.Attenuation0 = s->light->attenuation.constant;
			light.Attenuation1 = s->light->attenuation.linear;
			light.Attenuation2 = s->light->attenuation.quadratic;
			light.Theta = s->light->angle.inner;
			light.Phi = s->light->angle.outer;

			// Tell the device about the light and turn it on
			g_d3dd->SetLight( 0, &light );
			g_d3dd->LightEnable( 0, TRUE );

			g_d3dd->SetRenderState( D3DRS_AMBIENT, s->light->ambient.color);
			g_d3dd->SetRenderState( D3DRS_LIGHTING, TRUE );
		}
	}

	inline void ProcessStreams(const ISurface *s, int layer, int count)
	{
#if (DIRECT3D_VERSION == 0x0800)
		BYTE 
#elif (DIRECT3D_VERSION == 0x0900)
		VOID 
#endif
			*pVertices = 0, *pIndices = 0;

		if (s->GetVertexBuffer() && s->GetVertexBuffer()->GetDataPointer())
		{
			g_d3dd->SetStreamSource(0, (LPDIRECT3DVERTEXBUFFER)s->GetVertexBuffer()->render_info, 
#if (DIRECT3D_VERSION == 0x0900)
				0,
#endif
				Driver::INTERNAL_VERTEX_SIZE);
			bInternalVertexSource = false;
			nStartIndex = 0;
		}
		else
		{
			int flags = D3DLOCK_NOOVERWRITE;

			if (nVertexOffset + s->GetVerticesCount() > Driver::VERTEX_BUFFER_SIZE) {
				nVertexOffset = 0;
				flags = D3DLOCK_DISCARD;
			}

			Driver::p_VertexBuffer->Lock(
				nVertexOffset * Driver::INTERNAL_VERTEX_SIZE,
				s->GetVerticesCount() * Driver::INTERNAL_VERTEX_SIZE,
				&pVertices, 
				flags);

			memcpy (pVertices, s->GetVertices(), Driver::INTERNAL_VERTEX_SIZE * s->GetVerticesCount());

			/*for (int i = 0; i < count; ++i)
			{
				switch (s->GetMaterial()->GetLayer(layer + i)->tcgen)
				{
				case ILayer::TCGEN_ENVIRONMENT:
					{
						vec3_t camera = framework->GetScene()->GetCamera()->GetEye();

						mat4_t m; 

						if (s->GetMatrix())
							m = *s->GetMatrix();

						for (int j = 0; j < s->GetVerticesCount(); ++j)
						{
							vertex_t* vertex = (vertex_t*)pVertices + j;
							vec3_t dir = vec3_t::Normalize(camera);// - vertex->position * m1);
							vec3_t reflected = MulMat3(m, vertex->normal);
							reflected = reflected * vec3_t::Dot(reflected, dir) * 2 - dir;
							vertex->tc[i].x = 0.5 + reflected.x * 0.5;
							vertex->tc[i].y = 0.5 - reflected.y * 0.5;
						}
					}
					break;
				}
			}*/

			Driver::p_VertexBuffer->Unlock();

			nStartIndex = nVertexOffset;
			nVertexOffset += s->GetVerticesCount();

			if (!bInternalVertexSource) { //to not set the source twice or whatever
				g_d3dd->SetStreamSource(0, Driver::p_VertexBuffer, 
#if (DIRECT3D_VERSION == 0x0900)
					0,
#endif
					Driver::INTERNAL_VERTEX_SIZE);
				bInternalVertexSource = true;
			}
		}

		if (s->GetIndexBuffer() && s->GetIndexBuffer()->GetDataPointer())
		{
			g_d3dd->SetIndices((LPDIRECT3DINDEXBUFFER) s->GetIndexBuffer()->render_info
#if (DIRECT3D_VERSION == 0x0800)
				, 0
#endif
				);
			bInternalIndexSource = false;
		}
		else if (s->GetIndices())
		{
			Driver::p_IndexBuffer->Lock (0, sizeof(index_t) * s->GetIndicesCount(), &pIndices, 0);
#if (DIRECT3D_VERSION == 0x0800)
			index_t *dst = (index_t *)pIndices;
			for (const index_t *src = s->GetIndices(), *max = s->GetIndices() + s->GetIndicesCount(); 
				src < max; 
				*(dst++) = *(src++) + nStartIndex);
#elif (DIRECT3D_VERSION == 0x0900)
			memcpy (pIndices, s->GetIndices(), sizeof(index_t) * s->GetIndicesCount()); //in d3d9 we use BaseVertexIndex
#endif
			Driver::p_IndexBuffer->Unlock();
			if (!bInternalIndexSource) {
				g_d3dd->SetIndices(Driver::p_IndexBuffer
#if (DIRECT3D_VERSION == 0x0800)
					, 0
#endif
					);
				bInternalIndexSource = true;
			}
		}
	}

	inline void ProcessDepth(const IMaterial* material)
	{
		g_d3dd->SetRenderState(D3DRS_ZWRITEENABLE, (material && material->depthfunc.mask) ? TRUE : FALSE);

		IMaterial::Func depthfunc = material ? material->depthfunc.func : IMaterial::FUNC_LESS;

		if (depthfunc)
		{
			g_d3dd->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
			g_d3dd->SetRenderState(D3DRS_ZFUNC, (D3DCMPFUNC)depthfunc);
		}
		else
		{
			g_d3dd->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
		}
	}

	inline void ProcessStencilCCW(const IMaterial* material)
	{
		if (!material)
		{
			g_d3dd->SetRenderState(D3DRS_STENCILENABLE, FALSE);
		}

		if (material->stencil.back.func != IMaterial::FUNC_UNKNOWN)
		{
			g_d3dd->SetRenderState(D3DRS_STENCILENABLE, TRUE);
			g_d3dd->SetRenderState(D3DRS_STENCILREF, material->stencil.back.ref);
			g_d3dd->SetRenderState(D3DRS_STENCILFUNC, (D3DCMPFUNC)material->stencil.back.func);
			g_d3dd->SetRenderState(D3DRS_STENCILPASS, (D3DSTENCILOP)material->stencil.back.pass);
			g_d3dd->SetRenderState(D3DRS_STENCILFAIL, (D3DSTENCILOP)material->stencil.back.fail);
		}
		else
		{
			g_d3dd->SetRenderState(D3DRS_STENCILENABLE, FALSE);
		}
	}

	inline void ProcessStencil(const IMaterial* material)
	{
		if (!material)
		{
			g_d3dd->SetRenderState(D3DRS_STENCILENABLE, FALSE);

#if (DIRECT3D_VERSION == 0x0900)
			g_d3dd->SetRenderState(D3DRS_TWOSIDEDSTENCILMODE, FALSE);
#endif
			return;
		}

		if (material->stencil.front.func != IMaterial::FUNC_UNKNOWN)
		{
			g_d3dd->SetRenderState(D3DRS_STENCILENABLE, TRUE);
			g_d3dd->SetRenderState(D3DRS_STENCILREF, material->stencil.front.ref);
			g_d3dd->SetRenderState(D3DRS_STENCILFUNC, (D3DCMPFUNC)material->stencil.front.func);
			g_d3dd->SetRenderState(D3DRS_STENCILPASS, (D3DSTENCILOP)material->stencil.front.pass);
			g_d3dd->SetRenderState(D3DRS_STENCILFAIL, (D3DSTENCILOP)material->stencil.front.fail);
		}
		else
		{
			g_d3dd->SetRenderState(D3DRS_STENCILENABLE, FALSE);
		}

#if (DIRECT3D_VERSION == 0x0900)
		if (material->stencil.back.func != IMaterial::FUNC_UNKNOWN)
		{
			g_d3dd->SetRenderState(D3DRS_TWOSIDEDSTENCILMODE, TRUE);
			//g_d3dd->SetRenderState(D3DRS_CCW_STENCILREF, material->stencil.back.ref);
			g_d3dd->SetRenderState(D3DRS_CCW_STENCILFUNC, (D3DCMPFUNC)material->stencil.back.func);
			g_d3dd->SetRenderState(D3DRS_CCW_STENCILPASS, (D3DSTENCILOP)material->stencil.back.pass);
			g_d3dd->SetRenderState(D3DRS_CCW_STENCILFAIL, (D3DSTENCILOP)material->stencil.back.fail);
		}
		else
		{
			g_d3dd->SetRenderState(D3DRS_TWOSIDEDSTENCILMODE, FALSE);
		}
#endif
	}

	inline void ProcessColors(const IMaterial* material)
	{
		if (material && !material->colormask)
		{
			g_d3dd->SetRenderState(D3DRS_COLORWRITEENABLE, 0);
		}
		else
		{
			g_d3dd->SetRenderState(D3DRS_COLORWRITEENABLE, 
				D3DCOLORWRITEENABLE_ALPHA | D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE);
		}
	}

	inline void ProcessCulling(const IMaterial* material)
	{
		IMaterial::CullFace cullmode = material ? material->cullface : IMaterial::CULL_NONE;

		g_d3dd->SetRenderState(D3DRS_CULLMODE, (D3DCULL)cullmode);
	}

	inline void ProcessAlphaFunc(const ISurface* surface, const IMaterial* material) 
	{
		if (material && material->alphafunc.func)
		{
			g_d3dd->SetRenderState(D3DRS_ALPHATESTENABLE, TRUE);
			g_d3dd->SetRenderState(D3DRS_ALPHAFUNC, (D3DCMPFUNC)material->alphafunc.func); 
			g_d3dd->SetRenderState(D3DRS_ALPHAREF, (DWORD)(material->alphafunc.ref * surface->alphareference));
		}
		else
		{
			g_d3dd->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
		}
	}

	inline void ProcessBlending(const ISurface* surface, const ILayer* layer) 
	{
		if (!layer || surface->color.a < 255)
		{
			g_d3dd->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE); 
			g_d3dd->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
			g_d3dd->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		}
		else if (layer->blend.src != ILayer::BLEND_ONE || layer->blend.dst != ILayer::BLEND_ZERO)
		{
			g_d3dd->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE); 
			g_d3dd->SetRenderState(D3DRS_SRCBLEND, (D3DBLEND)layer->blend.src);
			g_d3dd->SetRenderState(D3DRS_DESTBLEND, (D3DBLEND)layer->blend.dst);
		}
		else
		{
			g_d3dd->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE); 
		}
	}

#define COLOR_BLEND(colorarg1, colorarg2, colorop) \
	g_d3dd->SetTextureStageState(nUnit, D3DTSS_COLORARG1, colorarg1); \
	g_d3dd->SetTextureStageState(nUnit, D3DTSS_COLORARG2, colorarg2); \
	g_d3dd->SetTextureStageState(nUnit, D3DTSS_COLOROP, colorop);

#define ALPHA_BLEND(alphaarg1, alphaarg2, alphaop) \
	g_d3dd->SetTextureStageState(nUnit, D3DTSS_ALPHAARG1, alphaarg1); \
	g_d3dd->SetTextureStageState(nUnit, D3DTSS_ALPHAARG2, alphaarg2); \
	g_d3dd->SetTextureStageState(nUnit, D3DTSS_ALPHAOP, alphaop);

	inline void ProcessTexture(const ISurface* surface, const ILayer* layer, int nUnit)
	{
		if (!layer)
		{
			COLOR_BLEND(D3DTA_DIFFUSE, D3DTA_DIFFUSE, D3DTOP_SELECTARG1);
			ALPHA_BLEND(D3DTA_DIFFUSE, D3DTA_DIFFUSE, D3DTOP_SELECTARG1);
			g_d3dd->SetTexture(nUnit, 0);
			return;
		}

		const ITexture* texture = 0;

		switch (layer->type)
		{
		case ILayer::TYPE_LIGHTMAP:
			texture = surface->GetLightmap();
			break;
		default:
			if (layer->GetTexturesCount() == 0) {
				COLOR_BLEND(D3DTA_DIFFUSE, D3DTA_DIFFUSE, D3DTOP_SELECTARG1);
				ALPHA_BLEND(D3DTA_DIFFUSE, D3DTA_DIFFUSE, D3DTOP_SELECTARG1);
				g_d3dd->SetTexture(nUnit, 0);
				return;
			}
			texture = layer->GetTexture(floor(surface->time * layer->anim_speed));
		}

		if (texture)
		{
			g_d3dd->SetTexture(nUnit, (LPDIRECT3DTEXTURE)texture->render_info);

			if (nUnit == 0)
			{
				if (texture->format == ITexture::FMT_A8) {
					COLOR_BLEND(D3DTA_DIFFUSE, D3DTA_DIFFUSE, D3DTOP_SELECTARG1);
				} else {
					COLOR_BLEND(D3DTA_TEXTURE, D3DTA_DIFFUSE, 
						((surface->light && surface->light->enabled)) ? D3DTOP_MODULATE2X : D3DTOP_MODULATE);
				}

				ALPHA_BLEND(D3DTA_TEXTURE, D3DTA_DIFFUSE, D3DTOP_MODULATE);

				if (__hack__has_overlay_color)
				{
					g_d3dd->SetTexture(nUnit, 0);
				}
				else if (__hack__has_team_color)
				{
					COLOR_BLEND(D3DTA_TEXTURE, D3DTA_DIFFUSE, D3DTOP_MODULATEINVALPHA_ADDCOLOR);
					ALPHA_BLEND(D3DTA_TEXTURE, D3DTA_DIFFUSE, D3DTOP_SELECTARG2);
				}

				g_d3dd->SetTextureStageState(1, D3DTSS_COLOROP, D3DTOP_DISABLE);
				g_d3dd->SetTextureStageState(2, D3DTSS_COLOROP, D3DTOP_DISABLE);
			}
			else
			{
				switch (layer->blend.src << 8 | layer->blend.dst)
				{
				case ILayer::BLEND_ZERO << 8 | ILayer::BLEND_SRCCOLOR:
				case ILayer::BLEND_DESTCOLOR << 8 | ILayer::BLEND_ZERO:
					COLOR_BLEND(D3DTA_TEXTURE, D3DTA_CURRENT, D3DTOP_MODULATE);
					ALPHA_BLEND(D3DTA_TEXTURE, D3DTA_CURRENT, D3DTOP_MODULATE);
					break;
				case ILayer::BLEND_ONE << 8 | ILayer::BLEND_ONE:
				case ILayer::BLEND_SRCALPHA << 8 | ILayer::BLEND_ONE:
					COLOR_BLEND(D3DTA_TEXTURE, D3DTA_CURRENT, D3DTOP_ADD);
					ALPHA_BLEND(D3DTA_TEXTURE, D3DTA_CURRENT, D3DTOP_MODULATE);
					break;
				case ILayer::BLEND_ONE << 8 | ILayer::BLEND_INVSRCALPHA:
					COLOR_BLEND(D3DTA_TEXTURE, D3DTA_CURRENT, D3DTOP_MODULATEINVALPHA_ADDCOLOR);
					ALPHA_BLEND(D3DTA_TEXTURE, D3DTA_CURRENT, D3DTOP_SELECTARG2);
					break;
				case ILayer::BLEND_ONE << 8 | ILayer::BLEND_SRCALPHA:
					COLOR_BLEND(D3DTA_TEXTURE, D3DTA_CURRENT, D3DTOP_MODULATEALPHA_ADDCOLOR);
					ALPHA_BLEND(D3DTA_TEXTURE, D3DTA_CURRENT, D3DTOP_SELECTARG2);
					break;
				default:
					COLOR_BLEND(D3DTA_TEXTURE, D3DTA_CURRENT, D3DTOP_BLENDTEXTUREALPHA);
					ALPHA_BLEND(D3DTA_TEXTURE, D3DTA_CURRENT, D3DTOP_SELECTARG2);
				}

				nUnit = 0, COLOR_BLEND(D3DTA_TEXTURE, D3DTA_TEXTURE, D3DTOP_SELECTARG1);
				nUnit = 2, COLOR_BLEND(D3DTA_CURRENT, D3DTA_DIFFUSE, D3DTOP_MODULATE);
				nUnit = 1;
			}


#if (DIRECT3D_VERSION == 0x0800)
			g_d3dd->SetTextureStageState(nUnit, D3DTSS_MAGFILTER, (D3DTEXTUREFILTERTYPE)layer->filter.mag);
			g_d3dd->SetTextureStageState(nUnit, D3DTSS_MINFILTER, (D3DTEXTUREFILTERTYPE)layer->filter.min);
			g_d3dd->SetTextureStageState(nUnit, D3DTSS_MIPFILTER, (D3DTEXTUREFILTERTYPE)layer->filter.mip);

			switch (layer->address)
			{
			case ILayer::ADDRESS_CLAMP:
				g_d3dd->SetTextureStageState(nUnit, D3DTSS_ADDRESSV, D3DTADDRESS_CLAMP);
				g_d3dd->SetTextureStageState(nUnit, D3DTSS_ADDRESSU, D3DTADDRESS_CLAMP);
				break;
			case ILayer::ADDRESS_REPEAT:
				g_d3dd->SetTextureStageState(nUnit, D3DTSS_ADDRESSV, D3DTADDRESS_WRAP);
				g_d3dd->SetTextureStageState(nUnit, D3DTSS_ADDRESSU, D3DTADDRESS_WRAP);
				break;
			}
#elif (DIRECT3D_VERSION == 0x0900)
			g_d3dd->SetSamplerState(nUnit, D3DSAMP_MAGFILTER, (D3DTEXTUREFILTERTYPE)layer->filter.mag);
			g_d3dd->SetSamplerState(nUnit, D3DSAMP_MINFILTER, (D3DTEXTUREFILTERTYPE)layer->filter.min);
			g_d3dd->SetSamplerState(nUnit, D3DSAMP_MIPFILTER, D3DTEXF_NONE);//(D3DTEXTUREFILTERTYPE)layer->filter.mip);

			switch (layer->address)
			{
			case ILayer::ADDRESS_CLAMP:
				g_d3dd->SetSamplerState(nUnit, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
				g_d3dd->SetSamplerState(nUnit, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
				break;
			case ILayer::ADDRESS_REPEAT:
				g_d3dd->SetSamplerState(nUnit, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
				g_d3dd->SetSamplerState(nUnit, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
				break;
			}
#endif
		}
		else
		{
			g_d3dd->SetTexture(nUnit, 0);
		}
	}

#undef COLOR_BLEND
#undef ALPHA_BLEND

	inline void RenderSurface(const ISurface *s)
	{
		if (s->GetIndicesCount())
		{
			switch (s->primitive)
			{
#if (DIRECT3D_VERSION == 0x0800)
			case ISurface::TRIANGLES:
				g_d3dd->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, nStartIndex, s->GetVerticesCount(), 0, s->GetIndicesCount() / 3);
				nTriangles += s->GetIndicesCount() / 3;
				break;
			case ISurface::LINES:
				g_d3dd->DrawIndexedPrimitive(D3DPT_LINELIST, nStartIndex, s->GetVerticesCount(), 0, s->GetIndicesCount() / 2);
				break;
			case ISurface::POINTS:
				g_d3dd->DrawIndexedPrimitive(D3DPT_POINTLIST, 0, s->GetVerticesCount(), 0, s->GetIndicesCount());
				break;
#elif (DIRECT3D_VERSION == 0x0900)
			case ISurface::TRIANGLES:
				g_d3dd->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, nStartIndex, 0, s->GetVerticesCount(), 0, s->GetIndicesCount() / 3);
				nTriangles += s->GetIndicesCount() / 3;
				break;
			case ISurface::LINES:
				g_d3dd->DrawIndexedPrimitive(D3DPT_LINELIST, nStartIndex, 0, s->GetVerticesCount(), 0, s->GetIndicesCount() / 2);
				break;
			case ISurface::POINTS:
				g_d3dd->DrawIndexedPrimitive(D3DPT_POINTLIST, nStartIndex, 0, s->GetVerticesCount(), 0, s->GetIndicesCount());
				break;
#endif
			};
		}
		else
		{
			switch (s->primitive)
			{
			case ISurface::TRIANGLES:
				g_d3dd->DrawPrimitive(D3DPT_TRIANGLELIST, nStartIndex, s->GetVerticesCount() / 3);
				nTriangles += s->GetVerticesCount() / 3;
				break;
			case ISurface::LINES:
				g_d3dd->DrawPrimitive(D3DPT_LINELIST, nStartIndex, s->GetVerticesCount() / 2);
				break;
			case ISurface::POINTS:
				g_d3dd->DrawPrimitive(D3DPT_POINTLIST, nStartIndex, s->GetVerticesCount());
				break;
			};
		}
		nDrawCalls++;
	}

	inline void ProcessMatrix(const ISurface *s)
	{
		if (s->GetMatrix())
		{
			g_d3dd->SetTransform(D3DTS_WORLD, &D3DXMATRIX((float*)s->GetMatrix()));
		}
		else
		{
			D3DXMATRIX Identity;
			D3DXMatrixIdentity(&Identity);
			g_d3dd->SetTransform(D3DTS_WORLD, &Identity);
		}
	}

	inline void ProcessFillMode(const ISurface *s)
	{
		g_d3dd->SetRenderState(D3DRS_FILLMODE, (D3DFILLMODE)s->fillmode);
	}


	virtual const stats_t& GetStats()
	{
		g_Stats.frame_num = nFrameNum;
		g_Stats.triangles = nTriangles;
		g_Stats.drawcalls = nDrawCalls;
		g_Stats.memory = nMemory;

		return g_Stats;
	}

	virtual bool IsFullscreen() const {
		return !params.Windowed;
	}

	virtual bool SetFullscreen(bool fullscreen) 
	{
		if (fullscreen && params.Windowed) {
			params.Windowed = FALSE;
			params.BackBufferWidth = g_FullscreenSize.width;
			params.BackBufferHeight = g_FullscreenSize.height;
			SetWindowLong(hWnd, GWL_STYLE, WS_POPUP|WS_SYSMENU|WS_VISIBLE);
			RestoreLostDevice();
		} else if (!params.Windowed && !fullscreen) {
			params.Windowed = TRUE;
			params.BackBufferWidth = g_WindowSize.width;
			params.BackBufferHeight = g_WindowSize.height;
			SetWindowLong(hWnd, GWL_STYLE, WindowStyle);
			SetWindowPos(hWnd, HWND_NOTOPMOST,
				WindowRect.left, WindowRect.top,
				WindowRect.right - WindowRect.left,
				WindowRect.bottom - WindowRect.top,
				SWP_SHOWWINDOW);
			RestoreLostDevice();
		}

		return true;
	}

	dim_t GetBackBufferSize()
	{
		return dim_t(params.BackBufferWidth, params.BackBufferHeight);
	}

	void RestoreLostDevice() {
		Sleep( 100 );


		Driver::InvalidateDeviceObjects();
				
		HRESULT hr = g_d3dd->Reset(&params);

		if( FAILED(hr ) )
		{
			framework->Log(IFramework::MSG_ERROR, DXGetErrorDescription(hr));

			return;
		}
		
		Driver::RestoreDeviceObjects();

		nVertexOffset = 0;
		nIndexOffset = 0;
		nStartIndex = 0;
		
		bInternalIndexSource = false;
		bInternalVertexSource = false;
	}

	virtual void SetScissorRect(const rect_t& rect, bool enable) 
	{
#if (DIRECT3D_VERSION == 0x0900)
		g_d3dd->SetScissorRect((RECT*)&rect);
		g_d3dd->SetRenderState(D3DRS_SCISSORTESTENABLE, enable);
#endif
	}

	virtual rect_t GetScissorRect() const
	{
		RECT rect;
#if (DIRECT3D_VERSION == 0x0900)
		g_d3dd->GetScissorRect(&rect); 
#endif
		return rect;
	}


};

void mod_videomemory(int value)
{
	pRender->nMemory += value;
}

const char* GetRenderName()
{
	return sRenderName;
}

IRenderer *CreateRenderer(IClientFramework* _framework)
{
	memset(&g_Stats, 0, sizeof(g_Stats));

	g_Stats.default_stencil = 0;
	g_Stats.default_color = 0;//0x00808080;
	g_Stats.default_depth = 1.0f;
	g_Stats.needs_ortho_shift = true;

#if (DIRECT3D_VERSION == 0x0800)
	g_Stats.name = "direct3d8";
	g_Stats.vs_ext = ".vs";
	g_Stats.ps_ext = ".ps";
	g_Stats.bone_index_stride = 3;
#else
	g_Stats.name = "direct3d9";
	g_Stats.ps_ext = ".hlsl";
	g_Stats.vs_ext = ".hlsl";
	g_Stats.bone_index_stride = 1;
#endif

	//framework = _framework;
	return pRender = new Renderer;
}

#undef STREAM
