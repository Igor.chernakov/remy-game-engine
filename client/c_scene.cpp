/**
	SCENE class is inherited from ENTITY (GAME OBJECT) and is a root for the entire object tree
	To enable multi-threading - separate UPDATE and RENDER - use ENGINE_MULTITHREADING macros
**/

#include "c_client.hpp"
#include "c_entity.hpp"
#include "c_overlay.hpp"
#include "c_helper.hpp"
#include "c_console.hpp"
#include "c_surface.hpp"
#include "c_camera.hpp"
#include "c_material.hpp"
#include "c_steam.hpp"

#include "../interfaces/iconsole.hpp"
#include "../interfaces/ioverlay.hpp"
#include "../interfaces/ihelper.hpp"
#include "../interfaces/imaterial.hpp"
#include "../interfaces/icomposer.hpp"

#include <numeric>

#ifdef __APPLE__
#include "../launcher.macosx/launcher.h"
#endif

extern bool g_bCanUpdate;

namespace py = boost::python;

char MapKey(int key);

static const float FADE_TIME = 0.5;

extern bool g_DeveloperMode;
extern int screen_x, screen_y;
extern int mouse_x, mouse_y;

bool g_AppQuit = false;
bool __hack_RenderingBackground = false;

extern std::wstring GetLoadingString();

std::wstring g_LoadingString;

#ifdef ENGINE_MULTITHREADING
extern bool g_bCanUpdate, g_bCanRender;
#else
extern bool g_bTakeScreenshot;
extern IRenderer* __HackRenderer;
#endif

enum stattypes_t {
	STAT_UPDATEMS,
	STAT_RENDERMS,
	STAT_COUNT
};

bool test;

stat_t<unsigned> g_Stats[STAT_COUNT];

bool b_IsFirstFrame = true;
bool b_IsLoadingScreen = false;

template <typename T>
struct Buffers
{
	Buffers()
	{
		write = &_1, read = &_2;
	}
	void Swap()
	{
		if (write == &_1)
			write = &_2, read  = &_1;
		else
			write = &_1, read  = &_2;
	}
	T _1, _2;
	T *write;
	const T *read;
};

namespace client
{
	extern int Entity_Count;
	extern void Widget_ResetSortValue();

	const int RENDER_BUFFER_SIZE = 4 * 1024 * 1024; //4 Mb for render-related data stored on CPU
	const int SURFACE_BUFFER_SIZE = 1024;

	struct Desktop: Widget, boost::python::wrapper<Desktop>, IWrapper
	{
		Desktop()
		{
			SetName("desktop");
		}

		int GetWidth() const
		{
			if (framework && framework->GetScene() && framework->GetScene()->GetCamera())
			{
				return framework->GetScene()->GetCamera()->GetWidth();
			}
			else
			{
				return 512;
			}
		}
		int GetHeight() const
		{
			if (framework && framework->GetScene() && framework->GetScene()->GetCamera())
			{
				return framework->GetScene()->GetCamera()->GetHeight();
			}
			else
			{
				return 512;
			}
		}

		virtual std::string GetClassName() const
		{
			return "Desktop";
		}

		virtual shared_ptr<Widget> shared_from_this()
		{
			return boost::python::extract<shared_ptr<Widget> >(GetPythonObject());
		}

		virtual shared_ptr<const Widget> shared_from_this() const
		{
			return boost::python::extract<shared_ptr<const Widget> >(GetPythonObject());
		}

		virtual PyObject* GetPythonObject() const
		{
			return boost::python::detail::wrapper_base_::get_owner(*this);
		}
	};

	struct RenderSurface: ISurface
	{
		RenderSurface()
		{
			vertices_count = 0;
			indices_count = 0;
			variables_count = 0;
			vertex_buffer = 0;
			index_buffer = 0;
			vertices = 0;
			indices = 0;
			variables = 0;
			material = 0;
			lightmap = 0;
			use_matrix = false;
		}

		virtual const vertex_t* GetVertices() const { return vertices; }
		virtual const index_t* GetIndices() const { return indices; }
		virtual const variable_t* GetVariables() const { return variables; }
		virtual size_t GetVerticesCount() const { return vertices_count; };
		virtual size_t GetIndicesCount() const { return indices_count; }
		virtual size_t GetVariablesCount() const { return variables_count; }
		virtual const IRendererBuffer* GetVertexBuffer() const { return vertex_buffer; }
		virtual const IRendererBuffer* GetIndexBuffer() const { return index_buffer; }
		virtual const IMaterial* GetMaterial() const { return material; }
		virtual const ITexture* GetLightmap() const { return lightmap; }
		virtual const mat4_t* GetMatrix() const { return use_matrix ? &matrix : 0; }

		int vertices_count;
		int indices_count;
		int variables_count;
		vertex_t* vertices;
		index_t* indices;
		variable_t* variables;
		const IRendererBuffer* vertex_buffer;
		const IRendererBuffer* index_buffer;
		const IMaterial* material;
		const ITexture* lightmap;
		mat4_t matrix;
		bool use_matrix;
	};

	inline static bool CompateAlphaSurface(const RenderSurface* i, const RenderSurface* j) {
		return i->sortvalue > j->sortvalue;
	}

	inline static bool CompateSolidSurface(const RenderSurface* i, const RenderSurface* j) {
		return i->sortvalue < j->sortvalue;
	}

	struct Scene:
		IClientScene,
		Entity,
		py::wrapper<Scene>,
		IWrapper
	{
		bool b_TooMuchVertices, b_TooMuchIndices, b_TooMuchVariables;

		bool b_HasUpdated;

		Scene()
			: m_overlay(new Overlay)
			, m_camera(new Camera(
				"DefaultCamera",
				vec3_t(-400.0f, -400.0f, 400.0f),
				quat_t(-0.424708f, 0.17592f, 0.339851f, -0.820473f)))
			, m_console(new Console)
			, m_Time(0.0f)
			, m_StateTimer(0)
			, m_PausersCount(0)
			, m_State(STATE_OPENING)
			, m_pushstate(PUSH_GEOMETRY)
			, b_Active(true)
			, n_Sleep(0)
			, b_TooMuchVertices(false)
			, b_TooMuchIndices(false)
			, b_TooMuchVariables(false)
			, m_stopifnotactive(true)
			, m_loadinframe(0)
			, b_ProcessMouseMove(false)
			, b_HasUpdated(false)
			, background_color(color_t())
		{
			memset(&light, 0, sizeof(light));

			g_LoadingString = GetLoadingString();

			m_helpers._1.reset(new Helper);
			m_helpers._2.reset(new Helper);

			for (int i = 0; i < BUFFER_COUNT; i++)
			{
				m_renderbuffer[i]._1 = new char[RENDER_BUFFER_SIZE];
				m_renderbuffer[i]._2 = new char[RENDER_BUFFER_SIZE];
			}

			for (int i = 0; i < IMaterial::RENDER_COUNT; i++) {
				m_indexbuffer[i]._1 = new char[RENDER_BUFFER_SIZE];
				m_indexbuffer[i]._2 = new char[RENDER_BUFFER_SIZE];
				m_rendersurfaces[i]._1 = new RenderSurface*[SURFACE_BUFFER_SIZE];
				m_rendersurfaces[i]._2 = new RenderSurface*[SURFACE_BUFFER_SIZE];
				m_rendersurfacescount[i]._1 = 0;
				m_rendersurfacescount[i]._2 = 0;
				for (int j = 0; j < SURFACE_BUFFER_SIZE; j++) {
					m_rendersurfaces[i]._1[j] = new RenderSurface;
					m_rendersurfaces[i]._2[j] = new RenderSurface;
				}
			}

			b_IsFirstFrame = true;

			//framework->Log(IFramework::MSG_LOG, "Entity class size: %d", sizeof(Entity));
		}

		~Scene()
		{
#ifdef ENGINE_MULTITHREADING

			while (!g_bCanUpdate)
				Sleep(10);
#endif

			for (int i = 0; i < BUFFER_COUNT; i++) {
				delete[] m_renderbuffer[i]._1;
				delete[] m_renderbuffer[i]._2;
			}

			for (int i = 0; i < IMaterial::RENDER_COUNT; i++) {
				for (int j = 0; j < SURFACE_BUFFER_SIZE; j++) {
					delete m_rendersurfaces[i]._1[j];
					delete m_rendersurfaces[i]._2[j];
				}
				delete[] m_rendersurfaces[i]._1;
				delete[] m_rendersurfaces[i]._2;
				delete[] m_indexbuffer[i]._1;
				delete[] m_indexbuffer[i]._2;
			}
		}

		void Save(const std::string& filename)
		{
		}

		void SetBackground(shared_ptr<const IMaterial> material)
		{
			m_background = material;
			m_background_copy = material;
		}

		shared_ptr<const IMaterial> GetBackground() const
		{
			return m_background;
		}

		void ResetBackground()
		{
			SetBackground(m_background_copy);
		}

		void UpdateState(float timestep)
		{
			switch (m_State)
			{
			case STATE_OPENING:
				if ((m_StateTimer += timestep / FADE_TIME) > 1.0)
				{
					m_State = STATE_ACTIVE;
					SceneOpen();
				}
				break;
			case STATE_CLOSING:
				if ((m_StateTimer -= timestep / FADE_TIME) < 0)
				{
					SetFlags(FLAG_DISPOSED, true);
				}
				break;
			case STATE_ACTIVE:
				m_StateTimer = 1.0;
				break;
			}
		}

		void Update(float timestep, float multiplier)
		{
			if (!IsPaused())
			{
				m_Time += timestep * multiplier;
			}

			UpdateState(timestep);

			m_hoverlist.clear();

			if (m_ui)
			{
				m_ui->Animate(timestep);
			}

			timestep *= multiplier;

			GetLinked().erase(
				std::remove_if(GetLinked().begin(),GetLinked().end(),
				boost::bind(&Entity::GetFlags, _1, FLAG_DISPOSED) && boost::bind(&Entity::OnDetach, _1)),
				GetLinked().end());

			m_controllers.erase(
				std::remove_if(m_controllers.begin(), m_controllers.end(),
				boost::bind(&Controller::Disposed, _1)), m_controllers.end());

			m_composers.erase(
				std::remove_if(m_composers.begin(), m_composers.end(),
				boost::bind(&IComposer::Disposed, _1)), m_composers.end());

			*m_rendercomposers.write = m_composers;

			if (!IsPaused())
			{
				Controllers controllers(m_controllers);

				std::for_each(controllers.begin(), controllers.end(), boost::bind(&Controller::Animate, _1, timestep));
			}

			std::for_each(
				GetLinked().begin(),
				GetLinked().end(),
				boost::bind(&Entity::Animate, _1, timestep));

			if (m_ui && m_ui->GetHovered())
			{
				m_hoverlist.clear();
			}

			ProcessHovers();
		}

		void DrawStats(IOverlay* overlay, IRenderer* renderer) const
		{
			std::stringstream stats;
			stats.precision(1);
			stats.setf(std::ios::fixed, std::ios::floatfield);

			//if (framework->GetVariables()->GetBoolean("com_showfps"))
			{
				stats << "fps: " << framework->GetTimer()->GetFPS() << std::endl;
			}

			//if (framework->GetVariables()->GetBoolean("com_showtris"))
			{
				stats
					<< "tris: " << renderer->GetStats().triangles << std::endl
					<< "draws: " << renderer->GetStats().drawcalls << std::endl
					;
			}

			//if (framework->GetVariables()->GetBoolean("com_showspeed"))
			{
				stats
					<< "render: " << g_Stats[STAT_RENDERMS].GetAverage() << "ms" << std::endl
					<< "update: " << g_Stats[STAT_UPDATEMS].GetAverage() << "ms" << std::endl
					;

				if (test)
					stats << "wait for render" << std::endl;
			}

			//if (framework->GetVariables()->GetBoolean("com_showobjs"))
			{
				stats
					<< "objs: " << Entity_Count << std::endl
					;
			}

			//if (framework->GetVariables()->GetBoolean("com_showmemoryusage"))
			{
				stats
					<< "vram: " << renderer->GetStats().memory / (1024.f * 1024.f) << "mb" << std::endl
					;
			}

			//if (framework->GetVariables()->GetBoolean("com_showtime"))
			{
				char buffer[60] = { 0 };
				sprintf(buffer, "%02d:%02d", int(m_Time) / 60, int(m_Time) % 60);
				stats
					<< "time: " << buffer << std::endl
					;
			}

			if (!stats.str().empty())
			{
				color_t color(0, 1, 0);

				if (framework->GetTimer()->GetFPS() < 30)
					color = color_t(1,1,0);

				std::string str = stats.str();

				overlay->String(
					std::wstring(str.begin(), str.end()),
					mat4_t::Translation((float)GetCamera()->GetWidth() -  99, 11, 0),
					"system",
					color_t(0,0,0));

				overlay->String(
					std::wstring(str.begin(), str.end()),
					mat4_t::Translation((float)GetCamera()->GetWidth() - 100, 10, 0),
					"system",
					color);
			}
		}

		struct rendersurface {
			IRenderer* renderer;
			rendersurface(IRenderer* renderer) : renderer(renderer) {}
			void operator()(const RenderSurface* surface) {
				if (!surface)
					return;
				if (surface->flags & Entity::FLAG_NOSCISSOR) {
					framework->GetScene()->GetCamera()->SetMode(
						framework->GetScene()->GetCamera()->GetMode(), renderer, ICamera::CLIP_OFFSET);
					renderer->PushSurface(surface);
					framework->GetScene()->GetCamera()->SetMode(
						framework->GetScene()->GetCamera()->GetMode(), renderer);
				} else if (surface->flags & Entity::FLAG_NOCAMERAFRAME) {
					framework->GetScene()->GetCamera()->SetMode(
						framework->GetScene()->GetCamera()->GetMode(), renderer, ICamera::CLIP_NONE);
					renderer->PushSurface(surface);
					framework->GetScene()->GetCamera()->SetMode(
						framework->GetScene()->GetCamera()->GetMode(), renderer);
				} else {
					renderer->PushSurface(surface);
				}
			}
		};

		void RenderLoading(IRenderer* renderer) const
		{
			b_IsLoadingScreen = false;

			if (!renderer || !this)
				return;

			framework->GetRendererObjectManager()->Update(renderer);

			if (!(m_loadingdisplay && m_overlay && framework->GetWindow() && m_camera))
				return;

			renderer->StartFrame();

			dim_t screen = framework->GetWindow()->GetSize();
			dim_t gauge = dim_t(m_loadingdisplay->GetWidth(), m_loadingdisplay->GetHeight());

			m_camera->SetMode(ICamera::MODE_INTERFACE, renderer, false);

			m_overlay->Quad(m_loadingdisplay,
				box2_t(
					(screen.width  - gauge.width  / 4) / 2,
					(screen.height - gauge.height / 4) / 2 + screen.height / 8,
					(screen.width  + gauge.width  / 4) / 2,
					(screen.height + gauge.height / 4) / 2 + screen.height / 8),
				box2_t(
					(m_loadinframe % 4) * 0.25,
					(m_loadinframe / 4) * 0.25,
					(m_loadinframe % 4) * 0.25 + 0.25,
					(m_loadinframe / 4) * 0.25 + 0.25));

			std::wstring str = g_LoadingString;
			float line_width = ((Overlay*)m_overlay.get())->GetLineWidth(str, "dialogue", 0);
			m_overlay->String(str,
				mat4_t::Translation((screen.width-line_width) / 2, screen.height * 0.75, 0),
				"dialogue",
				color_t(1,1,1,0.33));

			m_overlay->Render(renderer);
			m_overlay->Erase();

			if (++m_loadinframe >= 16)
				m_loadinframe = 0;

			renderer->FinishFrame();
		}

#define RENDER_SURFACES(RenderPass) std::for_each( \
				*m_rendersurfaces[IMaterial::RENDER_##RenderPass].read, \
				*m_rendersurfaces[IMaterial::RENDER_##RenderPass].read + \
					*m_rendersurfacescount[IMaterial::RENDER_##RenderPass].read, \
				rendersurface(renderer));

		virtual void RenderFrame(IRenderer* renderer) const
		{
			if (!b_HasUpdated)
			{
				return;
			}

			const unsigned long start = framework->GetTimer()->GetMilliseconds();

			renderer->StartFrame();

			PreRenderStep(renderer);

			m_camera->SetMode(ICamera::MODE_INTERFACE, renderer);

			RENDER_SURFACES(INTERFACE2);

			m_camera->SetMode(ICamera::MODE_SCENE, renderer);

			RENDER_SURFACES(SOLID);
			//RENDER_SURFACES(INNER);
			//RENDER_SURFACES(OUTER);
			RENDER_SURFACES(ALPHA);

			m_camera->SetMode(ICamera::MODE_INTERFACE, renderer);

			//std::for_each(m_rendercomposers.read->begin(), m_rendercomposers.read->end(),
			//	boost::bind(&IComposer::Compose, _1, renderer));

			{
				m_camera->SetMode(ICamera::MODE_SCENE, renderer);

				//RENDER_SURFACES(TEXT);

				m_helpers.read->get()->Render(renderer);

				m_camera->SetMode(ICamera::MODE_INTERFACE, renderer);
			}

			RENDER_SURFACES(INTERFACE);

			if (hack_camera_overinterface)
			{
				hack_camera_overinterface->Update();
				hack_camera_overinterface->SetMode(ICamera::MODE_SCENE, renderer);
			}
			else
			{
				m_camera->SetMode(ICamera::MODE_SCENE, renderer);
			}

			RENDER_SURFACES(OVERINTERFACE);

			m_camera->SetMode(ICamera::MODE_INTERFACE, renderer, false);

			m_console->Paint(m_overlay.get());

#ifdef WIN32
			if (g_DeveloperMode/* && GetKeyState(VK_CONTROL) & 0x80*/)
				DrawStats(m_overlay.get(), renderer);
#endif

			/*m_overlay->String(
				"For evaluation",
				mat4_t::Translation(5, GetCamera()->GetHeight() - 20, 0),
				"dialogue",
				color_t(1,1,1,0.25));*/

			m_overlay->Render(renderer); //TODO: remove, depricated
			m_overlay->Erase();

			PostRenderStep(renderer);

			renderer->FinishFrame();

			g_Stats[STAT_RENDERMS].Append(framework->GetTimer()->GetMilliseconds() - start);
		}

		vec2_t GetCursorPos()
		{
			return vec2_t(
				(float)framework->GetWindow()->GetCursorPos().x - GetCamera()->GetOffset().x,
				(float)framework->GetWindow()->GetCursorPos().y - GetCamera()->GetOffset().y);
		}

		void PreRenderStep(IRenderer* renderer) const
		{
			PreRenderStep(renderer, false);
		}

		void PreRenderStep(IRenderer* renderer, bool force) const
		{
			if (!m_background)
				return;

			const float fwidth = (float)m_background->GetWidth();
			const float fheight = (float)m_background->GetHeight();

			vertex_t verts[6];

			Surface bg = Surface::CreateQuad(verts,
				box2_t(0, 0, GetCamera()->GetWidth(), GetCamera()->GetHeight()),
				box2_t(0, 0, GetCamera()->GetWidth() / fwidth, GetCamera()->GetHeight() / fheight));

			for (int i = 0; i < 6; verts[i++].position.z = 1);

			bg.pMaterial = m_background;
			bg.SetColor(background_color);

			GetCamera()->SetMode(ICamera::MODE_INTERFACE, renderer);

			renderer->PushSurface(&bg);

			for (background_overlays_t::const_iterator it = m_bovr.begin(); it != m_bovr.end(); ++it)
			{
				const float fwidth = (float)(it->mat ? it->mat : m_background_copy)->GetWidth();
				const float fheight = (float)(it->mat ? it->mat : m_background_copy)->GetHeight();

				Surface bg = Surface::CreateQuad(verts,
					box2_t(
						it->screen.left, it->screen.top,
						it->screen.right, it->screen.bottom),
					box2_t(
						it->uv.left / fwidth, it->uv.top / fheight,
						it->uv.right / fwidth, it->uv.bottom / fheight));

				for (int i = 0; i < 6; verts[i++].position.z = 0.99f);

				bg.pMaterial = it->mat ? it->mat : m_background_copy;
				bg.SetColor(background_color);

				renderer->PushSurface(&bg);
			}

			GetCamera()->SetMode(ICamera::MODE_SCENE, renderer);

			RENDER_SURFACES(UNKNOWN);

			GetCamera()->SetMode(ICamera::MODE_INTERFACE, renderer);

			if (m_background_overlay)
			{
				bg.pMaterial = m_background_overlay;
				renderer->PushSurface(&bg);
			}

			GetCamera()->SetMode(ICamera::MODE_SCENE, renderer);
		}

#undef RENDER_SURFACES

		virtual bool IsActive() const
		{
			return b_Active;
		}

		virtual void PostRenderStep(IRenderer* renderer) const
		{
			if (m_State == STATE_ACTIVE)
			{
				return;
			}

			vertex_t verts[6];

			Surface bg = Surface::CreateQuad(verts,
				box2_t(0, 0,
				(float)GetCamera()->GetWidth(),
				(float)GetCamera()->GetHeight()),
				box2_t::Identity);

			bg.SetColor(color_t(0, 0, 0, 1.0f - m_StateTimer));

			GetCamera()->SetMode(ICamera::MODE_INTERFACE, renderer);

			renderer->PushSurface(&bg);

			GetCamera()->SetMode(ICamera::MODE_SCENE, renderer);
		}

		std::string Run()
		{
			framework->GetTimer()->Reset();

#ifdef ENGINE_MULTITHREADING
			Sleep(1);
#endif

			while (true)
			{
				if (g_AppQuit)
				{
					return std::string();
				}

				Steam::runCallbacks();
#ifdef WIN32
				MSG msg;

				while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
				{
					if (msg.message == WM_QUIT)
					{
						g_AppQuit = true;
					}
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
#elif defined __APPLE__
				my_message_t message_type;
				int message_param;
				while (PopGameMessage(&message_type, &message_param))
				{
					this->OnSystemMsg((scene_message_t)message_type, message_param);
				}
#else
				Display* dpy = framework->GetWindow()->GetDisplay();
				XEvent event;

				while (XPending(dpy)) {
					XNextEvent(dpy, &event);
					switch (event.type) {
					case ClientMessage:
						if (XInternAtom(dpy, "WM_PROTOCOLS", False) == event.xclient.message_type) {
							goto return_result;
						}
						break;
                    case MotionNotify:
                        mouse_x = event.xmotion.x;
                        mouse_y = event.xmotion.y;
						this->OnSystemMsg(MSG_MOUSE_MOVE, 0);
                        break;
					case ButtonPress:
                        mouse_x = event.xbutton.x;
                        mouse_y = event.xbutton.y;
						switch (event.xbutton.button) {
						case 1: this->OnSystemMsg(MSG_LBUTTON_DOWN, 0); break;
						case 3: this->OnSystemMsg(MSG_RBUTTON_DOWN, 0); break;
						}
						break;
					case ButtonRelease:
                        mouse_x = event.xbutton.x;
                        mouse_y = event.xbutton.y;
						switch (event.xbutton.button) {
						case 1: this->OnSystemMsg(MSG_LBUTTON_UP, 0); break;
						case 3: this->OnSystemMsg(MSG_RBUTTON_UP, 0); break;
						}
						break;
                    case ConfigureNotify:
                        screen_x = event.xconfigure.width;
                        screen_y = event.xconfigure.height;
                        break;
					}
				}
#endif
				
				if (!b_Active && m_stopifnotactive)
				{
					framework->GetTimer()->Update();

					Sleep(10);
				}
#ifdef ENGINE_MULTITHREADING
				else if (!g_bCanUpdate)
				{
					Sleep(10);
				}
				else
				{
					g_bCanUpdate = false;
#else
				else
				{
#endif
					const unsigned long start = framework->GetTimer()->GetMilliseconds();

					for (int i = 0; i < IMaterial::RENDER_COUNT; i++) {
						m_rendersurfacescount[i].Swap();
						m_rendersurfaces[i].Swap();
						m_indexbuffer[i].Swap();
					}

					for (int i = 0; i < BUFFER_COUNT; m_renderbuffer[i++].Swap());

					m_rendercomposers.Swap();
					m_helpers.Swap();

#ifdef ENGINE_MULTITHREADING
					g_bCanRender = true;
#endif

					if (GetFlags(FLAG_DISPOSED))
						break;

					Widget_ResetSortValue();

					for (int i = 0; i < BUFFER_COUNT; m_renderbuffer_writer[i] = *m_renderbuffer[i].write, i++);

					for (int i = 0; i < IMaterial::RENDER_COUNT; i++)
					{
						m_indexbuffer_writer[i] = *m_indexbuffer[i].write;
						m_surface_writer[i] = *m_rendersurfaces[i].write;
						*m_rendersurfacescount[i].write = 0;
					}

					GetCamera()->Update();

					Update(framework->GetTimer()->Update(), m_speed);

					m_pushstate = PUSH_BACKGROUND;

					__hack_RenderingBackground = true;
					std::for_each(m_render2background.begin(), m_render2background.end(), boost::bind(&Entity::Render, _1, this));
					__hack_RenderingBackground = false;

					m_pushstate = PUSH_GEOMETRY;

					Render(this); //publish renderable surfaces into render queue

					m_helpers.write->get()->Erase();

					if (framework->GetVariables()->GetBoolean("debug"))
					{
						Debug(m_helpers.write->get());
					}

					m_pushstate = PUSH_INTERFACE;

					if (m_ui)
						m_ui->Render(this);


					{
						/*int i = IMaterial::RENDER_ALPHA;
						std::sort(*m_rendersurfaces[i].write,
							*m_rendersurfaces[i].write + *m_rendersurfacescount[i].write,
							CompateAlphaSurface);*/
					}
					{
						/*int i = IMaterial::RENDER_SOLID;
						std::sort(*m_rendersurfaces[i].write,
							*m_rendersurfaces[i].write + *m_rendersurfacescount[i].write,
							CompateSolidSurface);*/
					}

					g_Stats[STAT_UPDATEMS].Append(framework->GetTimer()->GetMilliseconds() - start);

#ifndef ENGINE_MULTITHREADING
					for (int i = 0; i < IMaterial::RENDER_COUNT; i++) {
						m_rendersurfacescount[i].Swap();
						m_rendersurfaces[i].Swap();
						m_indexbuffer[i].Swap();
					}

					for (int i = 0; i < BUFFER_COUNT; m_renderbuffer[i++].Swap());

					m_rendercomposers.Swap();
					m_helpers.Swap();

					framework->GetRendererObjectManager()->Update(__HackRenderer);

					if (true)//!b_IsFirstFrame)
					{
						RenderFrame(__HackRenderer);

						if (g_bTakeScreenshot)
						{
							__HackRenderer->TakeScreenshot();
							g_bTakeScreenshot = false;
						}
					}
#endif

					if (b_IsFirstFrame) {
						framework->GetTimer()->Reset();
						b_IsFirstFrame = false;
					}

					b_HasUpdated = true;

					if (n_Sleep > 0)
					{
#ifdef WIN32
						Sleep(n_Sleep);
#else
						sleep(n_Sleep);
#endif
					}
				}
			}

			test = false;

return_result:
#ifdef ENGINE_MULTITHREADING
			while (!g_bCanUpdate) {
				Sleep(10);
				test = true;
			}
#endif
			return m_result;
		}

		const std::string& GetResult() const
		{
			return m_result;
		}

		void AddToBackground(shared_ptr<Entity> entity)
		{
			entity->is_Background = true;
			m_render2background.push_back(entity);
		}

		void RemoveFromBackground(shared_ptr<Entity> entity)
		{
			entity->is_Background = false;
			m_render2background.remove(entity);
		}

		void Evaluate(const std::string& command)
		{
			Command(shared_from_this(), command);
		}

		void SetResult(const std::string& result)
		{
			this->m_result = result;
			//SetFlags(Entity::FLAG_DISPOSED, true);
			m_State = STATE_CLOSING;
		}

#define ON_SCENEEVENT(Event, Args, Command, SceneOnly) \
		if (m_ui && m_ui->GetHovered() && !SceneOnly) \
		{ \
			/*framework->Log(IFramework::MSG_LOG, m_ui->GetHovered()->GetName().c_str());*/\
			this->Event(m_ui->GetHovered(), Args); \
			if (!GetFlags(FLAG_NOSIGNALS)) m_ui->GetHovered()->Event(m_ui->GetHovered(), Args); \
			m_ui->GetHovered()->Command;\
		} \
		else \
		{ \
			if (GetHovered() && !GetFlags(FLAG_NOSIGNALS) && !SceneOnly) \
			{ \
				std::list<shared_ptr<Entity> > r; \
				r.push_back(GetHovered()); \
				for (Entity::iterator e = begin(); e != end(); ++e) \
					if ((*e)->GetFlags(FLAG_SELECTED) && std::find(r.begin(), r.end(), *e) == r.end()) \
						r.push_back(*e); \
				(*r.begin())->Event(GetHovered(), Args); \
				for (std::list<shared_ptr<Entity> >::iterator e = ++r.begin(); e != r.end(); ++e) \
					if ((*e)->GetFlags(FLAG_SELECTED)) \
						(*e)->Event(GetHovered(), Args); \
			} \
			this->Event(GetHovered(), Args); \
		}

		/**
			Basic interaction from user is processed here
		**/

		void OnSystemMsg(scene_message_t msg, int param)
		{
			if (m_State == STATE_CLOSING)
			{
				return;
			}

			MouseButtons button = BUTTON_NONE;

			switch (msg)
			{
			case MSG_LBUTTON_DOWN:
			case MSG_LBUTTON_UP:
				button = BUTTON_LEFT;
				break;
			case MSG_RBUTTON_DOWN:
			case MSG_RBUTTON_UP:
				button = BUTTON_RIGHT;
				break;
			case MSG_MBUTTON_DOWN:
			case MSG_MBUTTON_UP:
				button = BUTTON_MIDDLE;
				break;
			default:
				button = BUTTON_NONE;
			}

			MouseEventArgs args = {
				button, 0, 0,
				framework->GetWindow()->GetCursorPos().x,
				framework->GetWindow()->GetCursorPos().y
			};

			//to process hovers
#ifdef __APPLE__
			if (framework->GetScene()) {
				if (m_ui) {
					m_ui->Animate(0);
				}
				Animate(0);
			}
#endif

			bool sceneOnly = (m_State != STATE_ACTIVE);

			switch (msg)
			{
			case MSG_KEY_DOWN:
				if (!g_DeveloperMode || !m_console->ProcessKey(param)) {
					KeyDown(shared_from_this(), param);
					if (m_ui)
					{
						for (Widget::iterator e = m_ui->begin(); e != m_ui->end(); ++e) {
							if ((*e)->GetFlags(FLAG_SELECTED))
								(*e)->KeyDown(*e, param);
						}
					}
				}
				break;
			case MSG_KEY_DOWN_FORMATTED:
				KeyDown(shared_from_this(), param);
				if (m_ui)
				{
					for (Widget::iterator e = m_ui->begin(); e != m_ui->end(); ++e) {
						if ((*e)->GetFlags(FLAG_SELECTED))
							(*e)->KeyDown(*e, param);
					}
				}
				break;
			case MSG_LBUTTON_DOWN:
				ON_SCENEEVENT(Click, args, GetFlags(FLAG_VISIBLE), sceneOnly);
			case MSG_RBUTTON_DOWN:
			case MSG_MBUTTON_DOWN:
				ON_SCENEEVENT(MouseDown, args, AddPseudoClass(PCLASS_PRESSED), sceneOnly);
				break;
			case MSG_LBUTTON_UP:
			case MSG_RBUTTON_UP:
			case MSG_MBUTTON_UP:
				ON_SCENEEVENT(MouseUp, args, RemovePseudoClass(PCLASS_PRESSED), sceneOnly);
				break;
			case MSG_MOUSE_MOVE:
				if (GetFlags(FLAG_NOSIGNALS) || b_ProcessMouseMove)
				{
					ON_SCENEEVENT(MouseMove, args, GetFlags(FLAG_VISIBLE), sceneOnly);
				}
				break;
			case MSG_ACTIVATE:
				b_Active = (bool)param;
				Activate(shared_from_this(), (bool)param);
				framework->GetSoundSystem()->Activate((bool)param);
				break;
			}
		}

#undef ON_SCENEEVENT

		shared_ptr<ICamera> GetCamera() const
		{
			return this->m_camera;
		}

		void SetCamera(shared_ptr<ICamera> camera)
		{
			this->m_camera = camera;
		}

		bool IsPaused() const
		{
			return m_PausersCount > 0;
		}

		shared_ptr<Desktop> GetUI() const
		{
			return m_ui;
		}

		void SetUI(shared_ptr<Desktop> ui)
		{
			m_ui = ui;
		}

		float GetTime() const
		{
			return m_Time;
		}

		void SetTime(float value)
		{
			m_Time = value;
		}

		shared_ptr<INavigation> GetNavigation() const
		{
			return m_navigation;
		}

		void SetNavigation(shared_ptr<INavigation> navigation)
		{
			m_navigation = navigation;
		};

		shared_ptr<client::Entity> shared_from_this()
		{
			return py::extract<shared_ptr<client::Entity> >(GetPythonObject());
		}

		shared_ptr<const client::Entity> shared_from_this() const
		{
			return py::extract<shared_ptr<const client::Entity> >(GetPythonObject());
		}

		PyObject* GetPythonObject() const
		{
			return py::detail::wrapper_base_::get_owner(*this);
		}

		void Attach(shared_ptr<Entity> element)
		{
			Entity::Attach(element);

			if (shared_ptr<IComposer> composer = dynamic_pointer_cast<IComposer>(element))
			{
				m_composers.push_back(composer);
			}
		}

		inline int CountBufferWritten(int i) const
		{
			return m_renderbuffer_writer[i] - *m_renderbuffer[i].write;
		}

		bool CheckLetterInFont(int letter, const std::string& fontname) const
		{
			return
				dynamic_pointer_cast<Overlay>(m_overlay)->GetFont(fontname) &&
				dynamic_pointer_cast<Overlay>(m_overlay)->GetFont(fontname)->GetChar(letter).width > 0;
		}

		void NotifyLoading()
		{
			if (b_HasUpdated) {
				return;
			}

			static unsigned long __lastloading = 0;

			unsigned long start = framework->GetTimer()->GetMilliseconds();

			if (start - __lastloading < 50) {
				return;
			}
			__lastloading = start;

#ifndef ENGINE_MULTITHREADING
			RenderLoading(__HackRenderer);
#else
			b_IsLoadingScreen = true;
#endif
		}

		void PushSurface(const ISurface* surface, const IMaterial* _material)
		{
			const int num_vertices = surface->GetVerticesCount();
			const int num_indices = surface->GetIndicesCount();
			const int num_variables = surface->GetVariablesCount();

			const IMaterial* material = _material ? _material : surface->GetMaterial();

			if (!num_vertices || !material || !material->GetLayersCount())
				return;

			RenderSurface* rsurface = 0;

			int pass = 0;

			switch (m_pushstate)
			{
			case PUSH_GEOMETRY:
				pass = material ? material->renderpass : IMaterial::RENDER_SOLID;
				if (pass == IMaterial::RENDER_SOLID && surface->color.a < 255)
					pass = IMaterial::RENDER_ALPHA;
				break;
			case PUSH_INTERFACE:
				if (material) {
					((Material*)&*material)->depthfunc.func = IMaterial::FUNC_UNKNOWN;
					((Material*)&*material)->depthfunc.mask = false;
				}
				pass = IMaterial::RENDER_INTERFACE;
				break;
			case PUSH_BACKGROUND:
				pass = IMaterial::RENDER_UNKNOWN;
				break;
			default:
				return;
			}

			if (surface->hack_overinterface)
			{
				pass = IMaterial::RENDER_OVERINTERFACE;
			}

			if (surface->hack_underworld)
			{
				pass = IMaterial::RENDER_INTERFACE2;
			}

			if (*m_rendersurfacescount[pass].write >= SURFACE_BUFFER_SIZE)
				return;

			RenderSurface* rsurface_prev = *(m_surface_writer[pass] - 1);
			bool adding_to_previous = false;

			if (
				*m_rendersurfacescount[pass].write > 0 &&
				material == rsurface_prev->GetMaterial() &&
				!surface->GetVertexBuffer() &&
				!surface->GetIndexBuffer())
			{
				rsurface = rsurface_prev;
				adding_to_previous = true;
			}
			else
			{
				rsurface = *(m_surface_writer[pass]++);
				(*m_rendersurfacescount[pass].write)++;
			}

			vertex_t *vertices_pointer = (vertex_t *)m_renderbuffer_writer[BUFFER_VERTEX];
			if (num_vertices && !surface->GetVertexBuffer())
			{
				memcpy(vertices_pointer, surface->GetVertices(), sizeof(vertex_t) * num_vertices);

				if (adding_to_previous)
				{
					if (const mat4_t* mat1 = rsurface_prev->GetMatrix())
					{
						vertex_t *dst = vertices_pointer - rsurface_prev->GetVerticesCount();
						for (int i = rsurface_prev->GetVerticesCount(); i > 0; --i, (dst++)->position *= *mat1);
						rsurface->use_matrix = false;
					}

					if (const mat4_t* mat2 = surface->GetMatrix())
					{
						vertex_t *dst = vertices_pointer;
						for (int i = num_vertices; i > 0; --i, (dst++)->position *= *mat2);
					}
				}

				m_renderbuffer_writer[BUFFER_VERTEX] += sizeof(vertex_t) * num_vertices;
			}

			index_t *indices_pointer = (index_t *)m_indexbuffer_writer[pass];
			if (num_indices && !surface->GetIndexBuffer())
			{
				if (adding_to_previous)
				{
					index_t *dst = indices_pointer;
					const index_t *src = surface->GetIndices();
					int count = vertices_pointer - rsurface->GetVertices();
					for (int i = num_indices; i > 0; --i, *(dst++) = *(src++) + count);
				}
				else
				{
					memcpy(indices_pointer, surface->GetIndices(), sizeof(index_t) * num_indices);
				}
				m_indexbuffer_writer[pass] += sizeof(index_t) * num_indices;
			}

			if (adding_to_previous)
			{
				rsurface->vertices_count = int(vertices_pointer - rsurface->GetVertices()) + num_vertices;
				rsurface->indices_count += num_indices;
			}
			else
			{
				ISurface::variable_t *variables_pointer = (ISurface::variable_t *)m_renderbuffer_writer[BUFFER_VARIABLE];

				if (num_variables)
				{
					memcpy(variables_pointer, surface->GetVariables(), sizeof(ISurface::variable_t) * num_variables);
					m_renderbuffer_writer[BUFFER_VARIABLE] += sizeof(ISurface::variable_t) * num_variables;
					ISurface::variable_t *variables_iterator = variables_pointer;
					for (int i = 0; i < num_variables; ++i, ++variables_iterator)
					{
						size_t varsize = 0;
						switch (variables_iterator->vartype)
						{
						case ISurface::VARTYPE_BOOL: varsize = 1; break;
						case ISurface::VARTYPE_INT: varsize = 4; break;
						case ISurface::VARTYPE_FLOAT: varsize = 4; break;
						case ISurface::VARTYPE_VECTOR4: varsize = 16; break;
						case ISurface::VARTYPE_MATRIX: varsize = 64; break;
						}
						varsize *= variables_iterator->count;
						memcpy(m_renderbuffer_writer[BUFFER_VARIABLE], variables_iterator->value, varsize);
						variables_iterator->value = m_renderbuffer_writer[BUFFER_VARIABLE];
						m_renderbuffer_writer[BUFFER_VARIABLE] += varsize;
					}
				}

				rsurface->vertices_count = num_vertices;
				rsurface->indices_count = num_indices;
				rsurface->variables_count = num_variables;
				rsurface->vertices = vertices_pointer;
				rsurface->indices = indices_pointer;
				rsurface->variables = variables_pointer;
				rsurface->vertex_buffer = surface->GetVertexBuffer();
				rsurface->index_buffer = surface->GetIndexBuffer();
				rsurface->material = material;
				rsurface->lightmap = surface->GetLightmap();
				rsurface->light = surface->light;
				rsurface->time = surface->time;
				rsurface->color = surface->color;
				rsurface->alphareference = surface->alphareference;
				rsurface->flags = surface->flags;
				rsurface->sortvalue = (rsurface->flags & FLAG_NOSORT) ? -1 : surface->sortvalue;
				rsurface->pivot = surface->pivot;

				if (surface->GetMatrix()) {
					rsurface->matrix = *surface->GetMatrix();
					rsurface->use_matrix = true;
					if (rsurface->sortvalue == 0) {
						rsurface->sortvalue = GetCamera()->GetDistance(rsurface->pivot * rsurface->matrix);
					}
				} else {
					rsurface->use_matrix = false;
				}
			}
		}

		const light_t* GetLight() const
		{
			return &light;
		}

		shared_ptr<IConsole> GetConsole() const { return m_console; }

		Buffers<RenderSurface**> m_rendersurfaces[IMaterial::RENDER_COUNT];
		Buffers<int> m_rendersurfacescount[IMaterial::RENDER_COUNT];
		Buffers<std::list<shared_ptr<IComposer> > > m_rendercomposers;
		Buffers<shared_ptr<IHelper> > m_helpers;

		enum {
			BUFFER_VERTEX,
			BUFFER_VARIABLE,
			BUFFER_COUNT,
		};

		Buffers<char*> m_renderbuffer[BUFFER_COUNT];
		char* m_renderbuffer_writer[BUFFER_COUNT];

		Buffers<char*> m_indexbuffer[IMaterial::RENDER_COUNT];
		char* m_indexbuffer_writer[IMaterial::RENDER_COUNT];

		std::list<shared_ptr<Entity> > m_render2background;

		RenderSurface** m_surface_writer[IMaterial::RENDER_COUNT];

		Event SceneOpen, KeyDown, Command, Activate;

		std::string m_result;

		mutable int m_loadinframe;

		struct background_overlay_t {
			rect_t screen;
			rect_t uv;
			shared_ptr<const IMaterial> mat;
		};
		typedef std::list<background_overlay_t> background_overlays_t;
		background_overlays_t m_bovr;
		void AddBackgroundPiece(const point_t& position, const rect_t& uv, shared_ptr<const IMaterial> mat) {
			background_overlay_t bo = {
				rect_t(position.x, position.y, position.x + uv.GetSize().width, position.y + uv.GetSize().height),
				uv, mat };
			m_bovr.push_back(bo);
		}
		void AddBackgroundPieceRect(const rect_t& screen, const rect_t& uv, shared_ptr<const IMaterial> mat) {
			background_overlay_t bo = { screen, uv, mat };
			m_bovr.push_back(bo);
		}
		void ClearBackgroundPieces() {
			m_bovr.clear();
		}

		shared_ptr<const IMaterial>
			m_background,
			m_loadingdisplay,
			m_background_overlay,
			m_background_copy;
		shared_ptr<IOverlay> m_overlay;
		shared_ptr<ICamera> m_camera;
		shared_ptr<IConsole> m_console;
		shared_ptr<INavigation> m_navigation;
		shared_ptr<Desktop> m_ui;

		shared_ptr<ICamera> hack_camera_overinterface;

		std::list<shared_ptr<IComposer> > m_composers;

		light_t light;

		float m_Time;
		float m_StateTimer;

		color_t background_color;

		bool m_stopifnotactive, b_ProcessMouseMove;

		bool b_Active;
		int n_Sleep;

		enum PushState {
			PUSH_GEOMETRY,
			PUSH_BACKGROUND,
			PUSH_INTERFACE
		} m_pushstate;

		int m_PausersCount;

		mutable shared_ptr<const ITexture> t1;

		enum State { STATE_ACTIVE, STATE_OPENING, STATE_CLOSING } m_State;
	};

	struct Pauser
		: py::wrapper<Pauser>
		, IWrapper
	{
		Pauser()
			: m_scene(static_pointer_cast<Scene>(framework->GetScene()))
		{
			if (m_scene)
			{
				m_scene->m_PausersCount++;
			}
		}
		~Pauser()
		{
			if (m_scene)
			{
				m_scene->m_PausersCount--;
			}
		}

		virtual PyObject* GetPythonObject() const
		{
			return py::detail::wrapper_base_::get_owner(*this);
		}

		shared_ptr<Scene> m_scene;
	};

}

void g_NotifyLoading() {
	shared_ptr<client::Scene> scene = dynamic_pointer_cast<client::Scene>(framework->GetScene());
	if (scene) {
		scene->NotifyLoading();
	}
}

void export_Scene()
{
	typedef client::Scene T;

	DISPOSEME(4) py::class_<client::Pauser, shared_ptr<client::Pauser>, boost::noncopyable>("Pauser")
		;

	DISPOSEME(4) py::class_<client::Desktop, shared_ptr<client::Desktop>, py::bases<client::Widget>, boost::noncopyable>("Desktop")
		;

	DISPOSEME(4) py::class_<T, shared_ptr<T>, py::bases<client::Entity, IClientScene>, boost::noncopyable>("Scene")
		.def("AddToBackground", &T::AddToBackground)
		.def("RemoveFromBackground", &T::RemoveFromBackground)
		.def("NotifyLoading", &T::NotifyLoading)
		.def("AddBackgroundPiece", &T::AddBackgroundPiece, (
			py::arg("position"), py::arg("uv"), py::arg("material") = shared_ptr<const IMaterial>()))
		.def("AddBackgroundPieceRect", &T::AddBackgroundPieceRect, (
			py::arg("screen"), py::arg("uv"), py::arg("material") = shared_ptr<const IMaterial>()))
		.def("ClearBackgroundPieces", &T::ClearBackgroundPieces)
		.def("ResetBackground", &T::ResetBackground)
		.def("CheckLetterInFont", &T::CheckLetterInFont)
		.add_property("result", py::make_function(&T::GetResult, py::return_internal_reference<>()), &T::SetResult)
		.add_property("mouse", &T::GetCursorPos)
		.add_property("camera", &T::GetCamera, &T::SetCamera)
		.add_property("ui", &T::GetUI, &T::SetUI)
		.add_property("time", &T::GetTime, &T::SetTime)
		.add_property("paused", &T::IsPaused)
		.add_property("background", &T::GetBackground, &T::SetBackground)
		.add_property("console", &T::GetConsole)
		.def_readwrite("background_color", &T::background_color)
		.def_readwrite("light", &T::light)
		.def_readwrite("sleep", &T::n_Sleep)
		.def_readwrite("loadingdisplay", &T::m_loadingdisplay)
		.def_readwrite("stopifnotactive", &T::m_stopifnotactive)
		.def_readwrite("SceneOpen", &T::SceneOpen)
		.def_readwrite("KeyDown", &T::KeyDown)
		.def_readwrite("Command", &T::Command)
		.def_readwrite("Activate", &T::Activate)
		.def_readwrite("process_mousemove", &T::b_ProcessMouseMove)
		.def_readwrite("background_overlay", &T::m_background_overlay)
		.def_readwrite("hack_camera_overinterface", &T::hack_camera_overinterface)
		;
}
