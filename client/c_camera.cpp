#include "c_client.hpp"
#include "c_entity.hpp"
#include "c_camera.hpp"

namespace client
{
	void Frustum::CalculateFrustum(const mat4_t& proj, const mat4_t& modl)
	{
		m_clip = proj * modl;
		m_invclip = mat4_t::Invert(m_clip);

		// Now we actually want to get the sides of the camera.  To do this we take
		// the clipping planes we received above and extract the sides from them.

		// This will extract the RIGHT side of the frustum
		m_Frustum[RIGHT][A] = m_clip.m[ 3] - m_clip.m[ 0];
		m_Frustum[RIGHT][B] = m_clip.m[ 7] - m_clip.m[ 4];
		m_Frustum[RIGHT][C] = m_clip.m[11] - m_clip.m[ 8];
		m_Frustum[RIGHT][D] = m_clip.m[15] - m_clip.m[12];

		// Now that we have a normal (A,B,C) and a distance (D) to the plane,
		// we want to Normalize that normal and distance.

		// Normalize the RIGHT side
		NormalizePlane(RIGHT);

		// This will extract the LEFT side of the frustum
		m_Frustum[LEFT][A] = m_clip.m[ 3] + m_clip.m[ 0];
		m_Frustum[LEFT][B] = m_clip.m[ 7] + m_clip.m[ 4];
		m_Frustum[LEFT][C] = m_clip.m[11] + m_clip.m[ 8];
		m_Frustum[LEFT][D] = m_clip.m[15] + m_clip.m[12];

		// Normalize the LEFT side
		NormalizePlane(LEFT);

		// This will extract the BOTTOM side of the frustum
		m_Frustum[BOTTOM][A] = m_clip.m[ 3] + m_clip.m[ 1];
		m_Frustum[BOTTOM][B] = m_clip.m[ 7] + m_clip.m[ 5];
		m_Frustum[BOTTOM][C] = m_clip.m[11] + m_clip.m[ 9];
		m_Frustum[BOTTOM][D] = m_clip.m[15] + m_clip.m[13];

		// Normalize the BOTTOM side
		NormalizePlane(BOTTOM);

		// This will extract the TOP side of the frustum
		m_Frustum[TOP][A] = m_clip.m[ 3] - m_clip.m[ 1];
		m_Frustum[TOP][B] = m_clip.m[ 7] - m_clip.m[ 5];
		m_Frustum[TOP][C] = m_clip.m[11] - m_clip.m[ 9];
		m_Frustum[TOP][D] = m_clip.m[15] - m_clip.m[13];

		// Normalize the TOP side
		NormalizePlane(TOP);

		// This will extract the BACK side of the frustum
		m_Frustum[BACK][A] = m_clip.m[ 3] - m_clip.m[ 2];
		m_Frustum[BACK][B] = m_clip.m[ 7] - m_clip.m[ 6];
		m_Frustum[BACK][C] = m_clip.m[11] - m_clip.m[10];
		m_Frustum[BACK][D] = m_clip.m[15] - m_clip.m[14];

		// Normalize the BACK side
		NormalizePlane(BACK);

		// This will extract the FRONT side of the frustum
		m_Frustum[FRONT][A] = m_clip.m[ 3] + m_clip.m[ 2];
		m_Frustum[FRONT][B] = m_clip.m[ 7] + m_clip.m[ 6];
		m_Frustum[FRONT][C] = m_clip.m[11] + m_clip.m[10];
		m_Frustum[FRONT][D] = m_clip.m[15] + m_clip.m[14];

		// Normalize the FRONT side
		NormalizePlane(FRONT);
	}


	bool Frustum::PointInFrustum(const float &x, const float &y, const float &z) const
	{
		// Go through all the sides of the frustum
		for(int i = 0; i < 6; i++ )
		{
			// Calculate the plane equation and check if the point is behind a side of the frustum
			if(m_Frustum[i][A] * x + m_Frustum[i][B] * y + m_Frustum[i][C] * z + m_Frustum[i][D] <= 0)
			{
				// The point was behind a side, so it ISN'T in the frustum
				return false;
			}
		}

		// The point was inside of the frustum (In Front of ALL the sides of the frustum)
		return true;
	}

	bool Frustum::SphereInFrustum(const float &x, const float &y, const float &z, const float radius) const
	{
		// Go through all the sides of the frustum
		for(int i = 0; i < 6; i++ )
		{
			// If the center of the sphere is farther away from the plane than the radius
			if( m_Frustum[i][A] * x + m_Frustum[i][B] * y + m_Frustum[i][C] * z + m_Frustum[i][D] <= -radius )
			{
				// The distance was greater than the radius so the sphere is outside of the frustum
				return false;
			}
		}

		// The sphere was inside of the frustum!
		return true;
	}
	bool Frustum::SphereInFrustum(const math::Vector3& org, const float radius) const
	{
		return SphereInFrustum( org.x, org.y, org.z, radius );
	}
	bool Frustum::BoxInFrustum(const box3_t& box, const mat4_t& matrix) const
	{
		const math::Vector3 points[] =
		{
			math::Vector3(box.vMin.x, box.vMin.y, box.vMin.z) * matrix,
			math::Vector3(box.vMax.x, box.vMin.y, box.vMin.z) * matrix,
			math::Vector3(box.vMin.x, box.vMax.y, box.vMin.z) * matrix,
			math::Vector3(box.vMax.x, box.vMax.y, box.vMin.z) * matrix,
			math::Vector3(box.vMin.x, box.vMin.y, box.vMax.z) * matrix,
			math::Vector3(box.vMax.x, box.vMin.y, box.vMax.z) * matrix,
			math::Vector3(box.vMin.x, box.vMax.y, box.vMax.z) * matrix,
			math::Vector3(box.vMax.x, box.vMax.y, box.vMax.z) * matrix
		};

		for(int i = 0; i < 6; i++ )
		{
			for(int j = 0; j < 8; j++ )
			{
				if(m_Frustum[i][A] * points[j].x + m_Frustum[i][B] * points[j].y + m_Frustum[i][C] * points[j].z + m_Frustum[i][D] > 0)
				{
					goto next_cycle;
				}
			}

			return false;

next_cycle:

			continue;
		}

		return true;
	}

	bool Frustum::AABoxInFrustum(const box3_t& box) const
	{
		const math::Vector3 points[] =
		{
			math::Vector3(box.vMin.x, box.vMin.y, box.vMin.z),
			math::Vector3(box.vMax.x, box.vMin.y, box.vMin.z),
			math::Vector3(box.vMin.x, box.vMax.y, box.vMin.z),
			math::Vector3(box.vMax.x, box.vMax.y, box.vMin.z),
			math::Vector3(box.vMin.x, box.vMin.y, box.vMax.z),
			math::Vector3(box.vMax.x, box.vMin.y, box.vMax.z),
			math::Vector3(box.vMin.x, box.vMax.y, box.vMax.z),
			math::Vector3(box.vMax.x, box.vMax.y, box.vMax.z)
		};

		for(int i = 0; i < 6; i++ )
		{
			for(int j = 0; j < 8; j++ )
			{
				if(m_Frustum[i][A] * points[j].x + m_Frustum[i][B] * points[j].y + m_Frustum[i][C] * points[j].z + m_Frustum[i][D] > 0)
				{
					goto next_cycle;
				}
			}

			return false;


next_cycle:

			continue;
		}

		return true;
	}


	void Frustum::NormalizePlane(int side)
	{
		// Here we calculate the magnitude of the normal to the plane (point A B C)
		// Remember that (A, B, C) is that same thing as the normal's (X, Y, Z).
		// To calculate magnitude you use the equation:  magnitude = sqrt( x^2 + y^2 + z^2)
		float magnitude = (float)sqrt( m_Frustum[side][A] * m_Frustum[side][A] +
			m_Frustum[side][B] * m_Frustum[side][B] +
			m_Frustum[side][C] * m_Frustum[side][C] );

		// Then we divide the plane's values by it's magnitude.
		// This makes it easier to work with.
		m_Frustum[side][A] /= magnitude;
		m_Frustum[side][B] /= magnitude;
		m_Frustum[side][C] /= magnitude;
		m_Frustum[side][D] /= magnitude;
	}

	Camera::Camera(
		const std::string& name,
		const math::Vector3& position,
		const quat_t& rotation,
		float fov,
		Camera::Projection proj)
		: Entity(name, position, rotation, math::Vector3::One, color_t())
	{
		this->znear = 32.0f;
		this->zfar = 2048.0f;
		this->proj = proj;

		m_viewport = rect_t(0, 0, GetWidth(), GetHeight());

		SetFov(fov);
	}

	void Camera::LookAt(const math::Vector3 &eye, const math::Vector3 &target, const math::Vector3 &yaw)
	{
		SetPosition(eye);

		vec3_t zaxis = vec3_t::Normalize(eye - target);
		vec3_t xaxis = vec3_t::Normalize(vec3_t::Cross(yaw, zaxis));
		vec3_t yaxis = vec3_t::Cross(zaxis, xaxis);

		SetRotation(mat4_t(xaxis, yaxis, zaxis));
		SetRotation(GetRotation());
	}

	float Camera::GetOrthoHeight() const
	{
		const float default_distance = 1000;
		return 2 * (sin(deg2rad(fov/2)) / cos(deg2rad(fov/2)) * default_distance);
	}

	float Camera::GetAspectRatio() const
	{
		return (float)GetWidth() / (float)GetHeight();
	}

	vec3_t Camera::GetTarget() const
	{
		return GetEye() + GetDirection();
	}

	void Camera::SetTarget(const vec3_t& target)
	{
		LookAt(GetEye(), target, GetYaw());
	}


	mat4_t ScissorMatrix(const rect_t& viewport, const rect_t& scissor)
	{
		vec2_t csz(scissor.GetSize().width, scissor.GetSize().height);
		vec2_t tsz(viewport.right, viewport.bottom);

		float x = 0, y = 0;

		// set modified projection 'scissor' matix that negates scale and
		// translation that would be done by setting the viewport to the clip area.
		float m_00 = tsz.x / csz.x;
		float m_11 = tsz.y / csz.y;
		float m_30 = 1.0f * (tsz.x + 2.0f *
			(viewport.left -
			(scissor.left + csz.x * 0.5f))) / csz.x;
		float m_31 = -(tsz.y + 2.0f *
			(viewport.top -
			(scissor.top + csz.y * 0.5f))) / csz.y;

		return mat4_t(
			m_00, 0.0f, 0.0f, 0.0f,
			0.0f, m_11, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			m_30, m_31, 0.0f, 1.0f);
	}

	void Camera::SetMode(Mode mde, IRenderer* renderer, int clip_flags)
	{
		mat4_t mProj, mView;

		float aspect_screen = (float)renderer->GetBackBufferSize().width / (float)renderer->GetBackBufferSize().height;
		float aspect_window = (float)GetWidth() / (float)GetHeight();
		
#ifdef TARGET_OS_IPHONE
		aspect_window = aspect_screen;
#endif

		float _x = (float)renderer->GetBackBufferSize().width / (float)GetWidth();
		float _y = (float)renderer->GetBackBufferSize().height / (float)GetHeight();


		switch (mde)
		{
		case MODE_SCENE:
			{
				if (proj == PROJ_PERSPECTIVE) {
					mProj = mat4_t::Perspective(fov, (float)GetWidth() / (float)GetHeight(), znear, zfar);
					mProj *= mat4_t::Scaling(aspect_window / aspect_screen, 1, 1);
				} else {
					mProj = mat4_t::Ortho(GetAspectRatio() * GetOrthoHeight(), GetOrthoHeight(), znear, zfar);
					mProj *= mat4_t::Scaling(1, 1, -1);
					mProj *= mat4_t::Scaling(aspect_window / aspect_screen, 1, 1);
				}

				mView = mat4_t::LookAt(GetEye().v, (GetEye() + GetDirection()).v, GetYaw().v);
				mView *= mat4_t::Translation( - 0.5f, - 0.5f, 0);

				CalculateFrustum(mProj, mView);
			}
			break;
		case MODE_INTERFACE:
			{
				mProj = mat4_t::Ortho((float)GetWidth(), (float)GetHeight(), 0.0f, 1.0f);
				mView *= mat4_t::Scaling(aspect_window / aspect_screen, -1, 1);

				if (renderer->GetStats().needs_ortho_shift) {
					mView *= mat4_t::Translation(- (float)GetWidth() / 2 - 0.5f, -(float)GetHeight() / 2 - 0.5f, 0);
				} else {
					mView *= mat4_t::Translation(- (float)GetWidth() / 2, -(float)GetHeight() / 2, 0);
				}
			}
			break;
		case MODE_RAW_SCENE:
			{
				if (proj == PROJ_PERSPECTIVE) {
					mProj = mat4_t::Perspective(fov, (float)GetWidth() / (float)GetHeight(), znear, zfar);
				} else {
					mProj = mat4_t::Ortho(GetAspectRatio() * GetOrthoHeight(), GetOrthoHeight(), znear, zfar);
					mProj *= mat4_t::Scaling(1, 1, -1);
				}

				mView = mat4_t::LookAt(GetEye().v, (GetEye() + GetDirection()).v, GetYaw().v);
				mView *= mat4_t::Translation( - 0.5f, - 0.5f, 0);

				CalculateFrustum(mProj, mView);

				renderer->SetViewport(rect_t(0, 0, GetWidth(), GetHeight()));
				renderer->SetMatrix(IRenderer::MATRIX_PROJECTION, mProj.m);
				renderer->SetMatrix(IRenderer::MATRIX_MODELVIEW, mView.m);
				mode_ = mde;
			}
			return;
		case MODE_RAW_INTERFACE:
			{
				int width = renderer->GetBackBufferSize().width, height = renderer->GetBackBufferSize().height;
				mProj = mat4_t::Ortho((float)width, (float)height, 0.0f, 1.0f);
				mView *= mat4_t::Scaling(1, -1, 1);
				if (renderer->GetStats().needs_ortho_shift) {
					mView *= mat4_t::Translation(- (float)width / 2 - 0.5f, -(float)height / 2 - 0.5f, 0);
				} else {
					mView *= mat4_t::Translation(- (float)width / 2, -(float)height / 2, 0);
				}
				renderer->SetViewport(rect_t(0, 0, width, height));
				renderer->SetMatrix(IRenderer::MATRIX_PROJECTION, mProj.m);
				renderer->SetMatrix(IRenderer::MATRIX_MODELVIEW, mView.m);
				mode_ = mde;
			}
			return;
		}

		rect_t m_viewport_old_biased(m_viewport_old.left * _x, m_viewport_old.top * _y, 
			m_viewport_old.right * _x, m_viewport_old.bottom * _y);

		if (clip_flags & CLIP_SCISSOR) {
			if (clip_flags & CLIP_OFFSET) {
				//mProj = ScissorMatrix(rect_t(m_offset_old.x, m_offset_old.y, GetWidth(), GetHeight()), m_viewport_old) * mProj;
				mProj = ScissorMatrix(rect_t(0, 0, GetWidth() * _x, GetHeight() * _y), m_viewport_old_biased) * mProj;
				renderer->SetViewport(rect_t(
					(m_offset_old.x + m_viewport_old.left) * _x,
					(m_offset_old.y + m_viewport_old.top) * _y,
					(m_offset_old.x + m_viewport_old.right) * _x,
					(m_offset_old.y + m_viewport_old.bottom) * _y));
			} else {
				mProj = ScissorMatrix(rect_t(0, 0, GetWidth() * _x, GetHeight() * _y), m_viewport_old_biased) * mProj;
				renderer->SetViewport(m_viewport_old_biased);
			}
		} else {
			rect_t viewport(MAX(0, -m_offset_old.x) * _x, MAX(0, -m_offset_old.y) * _y, GetWidth() * _x, GetHeight() * _y);
			if (clip_flags & CLIP_OFFSET) {
				mProj = ScissorMatrix(rect_t(0, 0, GetWidth() * _x, GetHeight() * _y), viewport) * mProj;
				//mProj = ScissorMatrix(rect_t(m_offset_old.x, m_offset_old.y, GetWidth(), GetHeight()), viewport) * mProj;
				renderer->SetViewport(rect_t(
					m_offset_old.x * _x + viewport.left,
					m_offset_old.y * _y + viewport.top,
					m_offset_old.x * _x + viewport.right,
					m_offset_old.y * _y + viewport.bottom));
			} else {
				rect_t viewport(0, 0, GetWidth() * _x, GetHeight() * _y);
				mProj = ScissorMatrix(rect_t(0, 0, GetWidth() * _x, GetHeight() * _y), viewport) * mProj;
				renderer->SetViewport(viewport);
			}
		}


		//rect_t viewport(0, 0, GetWidth(), GetHeight());
		//renderer->SetViewport(rect_t(viewport.left * _x, viewport.top * _y, viewport.right * _x, viewport.bottom * _y));
		
		renderer->SetMatrix(IRenderer::MATRIX_PROJECTION, mProj.m);
		renderer->SetMatrix(IRenderer::MATRIX_MODELVIEW, mView.m);

		mode_ = mde;
	}

	void Camera::SetViewport(const rect_t& rect)
	{
		m_viewport = rect;
	}

	const rect_t& Camera::GetViewport() const
	{
		return m_viewport;
	}

	void Camera::SetOffset(const point_t& point)
	{
		m_offset = point;
	}

	const point_t& Camera::GetOffset() const
	{
		return m_offset;
	}

	math::Vector3 Camera::GetEye() const
	{
		return GetPosition();
	}
	math::Vector3 Camera::GetDirection() const
	{
		return math::Vector3(0,0,-1) * GetOrientation();
	}
	math::Vector3 Camera::GetYaw() const
	{
		return math::Vector3(0,1,0) * GetOrientation();
	}
	quat_t Camera::GetOrientation() const
	{
		return GetRotation();
	}
	void Camera::SetFov(float fov)
	{
		this->proj = fov > 0 ? PROJ_PERSPECTIVE : PROJ_ORTHOGONAL;
		this->fov = fabs(fov);
	}
	float Camera::GetFov() const
	{
		return fov;
	}
	float Camera::GetZFar() const
	{
		return zfar;
	}
	float Camera::GetZNear() const
	{
		return znear;
	}
	void Camera::SetZFar(float v)
	{
		zfar = v;
	}
	void Camera::SetZNear(float v)
	{
		znear = v;
	}
	math::Vector2 Camera::GetScreenCoords(const math::Vector3& point) const
	{
		rect_t vp(0, 0, GetWidth(), GetHeight());

		mat4_t mProj, mView;

		if (proj == PROJ_PERSPECTIVE)
		{
			mProj = mat4_t::Perspective(fov, (float)GetWidth() / (float)GetHeight(), znear, zfar);
		}
		else
		{
			mProj = mat4_t::Ortho(GetAspectRatio() * GetOrthoHeight(), GetOrthoHeight(), znear, zfar); 
			mProj *= mat4_t::Scaling(1, 1, -1);
		}
		
		mView = mat4_t::LookAt(GetEye().v, (GetEye() + GetDirection()).v, GetYaw().v);

		vec4_t out = vec4_t(point.x, point.y, point.z, 1.0f) * mView * mProj;
		
		out.x /= out.w; out.y /= out.w; out.z /= out.w;
		
		return math::Vector2((out.x + 1) / 2 * (vp.right - vp.left), (-out.y + 1) / 2 * (vp.bottom - vp.top));
	}

	math::Vector3 Camera::ScreenToWorld(const point_t& point, float distance) const
	{
		mat4_t mProj, mView, mInvClip;

		if (proj == PROJ_PERSPECTIVE) {
			mProj = mat4_t::Perspective(fov, (float)GetWidth() / (float)GetHeight(), znear, zfar);
		} else {
			mProj = mat4_t::Ortho(GetAspectRatio() * GetOrthoHeight(), GetOrthoHeight(), znear, zfar);
			mProj *= mat4_t::Scaling(1, 1, -1);
		}

		mView = mat4_t::LookAt(GetEye().v, (GetEye() + GetDirection()).v, GetYaw().v);
		mView *= mat4_t::Translation( - 0.5f, - 0.5f, 0);

		mInvClip = mat4_t::Invert(mProj * mView);

		return math::Vector3(
			2 * (float) (point.x - m_offset_old.x) / (float) GetWidth() - 1,
			1 - 2 * (float) (point.y - m_offset_old.y) / (float) GetHeight(),
			distance) * mInvClip;
	}
	math::Line Camera::GetMouseRay() const
	{
		point_t mouse = framework->GetWindow()->GetCursorPos();
		//mouse.x -= GetOffset().x;
		//mouse.y -= GetOffset().y;
		return math::Line(ScreenToWorld(mouse,  0), ScreenToWorld(mouse,  1));
	}
	math::Line Camera::GetRay(const point_t& point) const
	{
		return math::Line(ScreenToWorld(point,  0), ScreenToWorld(point,  1));
	}
	int Camera::GetWidth() const
	{
		return framework->GetWindow() ? framework->GetWindow()->GetSize().width : 256;
	}
	int Camera::GetHeight() const
	{
		return framework->GetWindow() ? framework->GetWindow()->GetSize().height : 256;
	}

	Camera::Projection Camera::GetProjection() const
	{
		return proj;
	}

	void Camera::SetProjection(Camera::Projection value)
	{
		proj = value;
	}

	float Camera::GetPixelSize(const math::Vector3& origin) const
	{
		if (proj == PROJ_PERSPECTIVE)
		{
			return ((GetEye() - origin).Dot(GetDirection())) / cos(deg2rad(fov));
		}
		else
		{
			return (float)GetOrthoHeight() / (float)GetHeight();
		}
	}

	shared_ptr<client::Entity> Camera::shared_from_this()
	{
		return boost::python::extract<shared_ptr<client::Entity> >(GetPythonObject());
	}

	shared_ptr<const client::Entity> Camera::shared_from_this() const
	{
		return boost::python::extract<shared_ptr<const client::Entity> >(GetPythonObject());
	}

	PyObject* Camera::GetPythonObject() const
	{
		return boost::python::detail::wrapper_base_::get_owner(*this);
	}

	float Camera::GetDistance(const vec3_t& position) const
	{
		return (position - GetEye()).Length();
	}
}

void export_Camera()
{
	namespace py = boost::python;

	using client::Camera;

	py::enum_<Camera::Projection>("Projection")
		.value("Perspective", Camera::PROJ_PERSPECTIVE)
		.value("Orthogonal", Camera::PROJ_ORTHOGONAL)
		.export_values()
		;

	DISPOSEME(3) py::class_<ICamera, shared_ptr<ICamera>, boost::noncopyable>("ICamera", py::no_init)
		.def("LookAt", &ICamera::LookAt, (py::arg("eye"), py::arg("target"), py::arg("yaw") = vec3_t(0, 0, 1)))
		.def("GetScreenCoords", &ICamera::GetScreenCoords)
		.def("GetRay", &ICamera::GetRay)
		.add_property("proj", &ICamera::GetProjection, &ICamera::SetProjection)
		.add_property("eye", &ICamera::GetEye)
		.add_property("yaw", &ICamera::GetYaw)
		.add_property("orientation", &ICamera::GetOrientation)
		.add_property("direction", &ICamera::GetDirection)
		.add_property("znear", &ICamera::GetZNear)
		.add_property("zfar", &ICamera::GetZFar)
		.add_property("fov", &ICamera::GetFov)
		.add_property("width", &ICamera::GetWidth)
		.add_property("height", &ICamera::GetHeight)
		;

	DISPOSEME(3) py::class_<Camera, shared_ptr<Camera>, py::bases<client::Entity, ICamera>, boost::noncopyable>("Camera", py::no_init)
		.def(py::init<std::string, vec3_t, quat_t, float, Camera::Projection>((
			py::arg("id") = std::string("Camera#"),
			py::arg("position") = math::Vector3::Zero,
			py::arg("rotation") = quat_t::Identity,
			py::arg("fov") = 45.0f,
			py::arg("projection") = Camera::PROJ_PERSPECTIVE)))
		.add_property("fov", &Camera::GetFov, &Camera::SetFov)
		.add_property("target", &Camera::GetTarget, &Camera::SetTarget)
		.add_property("zfar", &Camera::GetZFar, &Camera::SetZFar)
		.add_property("znear", &Camera::GetZNear, &Camera::SetZNear)
		.add_property("mouseray", &Camera::GetMouseRay)
		.add_property("viewport", py_cref(&Camera::GetViewport), &Camera::SetViewport)
		.add_property("offset", py_cref(&Camera::GetOffset), &Camera::SetOffset)
		;
}
