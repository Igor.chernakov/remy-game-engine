#pragma once

namespace client
{
	class Controller
	{
	protected:
		Controller()
			: m_useless(false)
			, m_enabled(true)
			, m_delay(0)
			, m_rate(1)
		{
		}

	public:
		inline bool IsActive() const { return !Disposed() && IsEnabled(); }
		inline void Dispose() { m_useless = true; }
		inline bool Disposed() const { return m_useless; }
		inline void SetEnabled(bool value) { m_enabled = value; } 
		inline bool IsEnabled() const { return m_enabled; } 

		inline void Enable() { SetEnabled(true); }
		inline void Disable() { SetEnabled(false); }

		virtual bool OnAttach(class Entity* self) = 0;
		virtual bool OnAttach(class Widget* self) = 0;

		virtual void Animate(float timestep) {
			if (!IsActive())
				return;
			m_delay -= timestep;
			if (m_delay < 0) {
				OnAnimate(-m_delay);
				m_delay = 0;
			}
		}

		virtual void Finalize() = 0;

		virtual float GetValue() const = 0;
		virtual void SetValue(float value) = 0;

		float m_delay;
		float m_rate;

	protected:
		virtual void OnAnimate(float timestep) = 0;

	private:
		bool m_enabled, m_useless;
	};
}
