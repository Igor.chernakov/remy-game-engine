#include "image.hpp"
#include "renderer.hpp"

#include "../jpeg8/cdjpeg.h"

extern IClientFramework* framework;

DWORD Log2(DWORD val)
{
	DWORD answer = 0;
	while (val>>=1) answer++;
	return answer;
}

#pragma pack (push, 1)
struct TgaHeader
{
	BYTE id_length, colormap_type, image_type;
	WORD colormap_index, colormap_length;
	BYTE colormap_size;
	WORD x_origin, y_origin, width, height;
	BYTE pixel_size, attributes;
};
#pragma pack (pop, 1)

#define TGA_ORIGIN_MASK 0x30

#define TGA_ORIG_LOWER_LEFT 0
#define TGA_ORIG_LOWER_RIGHT 1
#define TGA_ORIG_UPPER_LEFT 2
#define TGA_ORIG_UPPER_RIGHT 3

extern IRenderer* renderer;

bool Load_TGA(ITexture* texture, const BYTE* mem, size_t size, BYTE** bitmap)
{
	int columns, rows, numPixels;
	int row, column, bpp;
	BYTE *rgba, *dest;
	TgaHeader *header;
	BYTE red,green,blue,alpha,packetHeader,packetSize,j;
	const BYTE *tga = mem;

	header = (TgaHeader*)tga;

	tga += sizeof(TgaHeader) + header->id_length;

	// Only types 2 and 10 are supported
	if (header->image_type!=2 && /*header->image_type!=10 && */header->image_type!=3)
	{
		return false;
	}

	// Only 24bit or 32bit pixels are supported
	if (header->colormap_type!=0||(header->pixel_size!=32&&header->pixel_size!=8&&header->pixel_size!=24))
	{
		return false;
	}

	columns = header->width;
	rows = header->height;
	numPixels = columns * rows;
	bpp = header->pixel_size / 8;

	// Allocate memory for decoded image
	rgba = new BYTE[numPixels*(bpp)];

	if (!rgba)
	{
		return false;
	}

	texture->width = columns;
	texture->height = rows;

	// Uncompressed RGB image
	if (header->image_type==2 || header->image_type==3) {
		for(row=rows-1; row>=0; row--) {
			dest = rgba + row*columns*(bpp);
			for(column=0; column<columns; column++) {
				if (header->pixel_size==8) {
					*dest++ = *tga++;
					//*dest++ = 255;
				}
				else if (header->pixel_size==24) {
					blue = *tga; tga++;
					green = *tga; tga++;
					red = *tga; tga++;
					*dest++ = red;
					*dest++ = green;
					*dest++ = blue;
					//*dest++ = 255;
				}
				else if (header->pixel_size==32) {
					blue = *tga; tga++;
					green = *tga; tga++;
					red = *tga; tga++;
					alpha = *tga; tga++;
					*dest++ = red;
					*dest++ = green;
					*dest++ = blue;
					*dest++ = alpha;
				}
			}
		}
	}
	// RLE RGB image
	/*else if (header->image_type==10) {
		for(row=rows-1; row>=0; row--) {
			dest = rgba + row*columns*(bpp);
			for(column=0; column<columns; ) {
				packetHeader=*tga; tga++;
				packetSize = 1 + (packetHeader & 0x7f);
				// RLE packet
				if (packetHeader & 0x80) {
					if (header->pixel_size==24) {
						blue = *tga; tga++;
						green = *tga; tga++;
						red = *tga; tga++;
						//alpha = 255;
					}
					else if (header->pixel_size==32) {
						blue = *tga; tga++;
						green = *tga; tga++;
						red = *tga; tga++;
						alpha = *tga; tga++;
					}

					for(j=0;j<packetSize;j++) {
						*dest++=red;
						*dest++=green;
						*dest++=blue;
						if(header->pixel_size==32) *dest++=alpha;
						column++;
						if (column==columns) {
							column=0;
							if (row>0)
								row--;
							else
								goto end_decode;
							dest = rgba + row*columns*(bpp);
						}
					}
				}
				// Non-RLE packet
				else {
					for(j=0;j<packetSize;j++) {
						if (header->pixel_size==24) {
							blue = *tga; tga++;
							green = *tga; tga++;
							red = *tga; tga++;
							*dest++ = red;
							*dest++ = green;
							*dest++ = blue;
							//*dest++ = 255;
						}
						else {
							blue = *tga; tga++;
							green = *tga; tga++;
							red = *tga; tga++;
							alpha = *tga; tga++;
							*dest++ = red;
							*dest++ = green;
							*dest++ = blue;
							*dest++ = alpha;
						}
						column++;
						if (column==columns) {
							column=0;
							if (row>0)
								row--;
							else
								goto end_decode;
							dest = rgba + row*columns*(bpp);
						}
					}
				}
			}
end_decode:;
		}
	}*/

	if (header->attributes & TGA_ORIGIN_MASK) //FIXME: make origin check
	{
		BYTE *swap = new BYTE[columns * bpp];
		for (int i = 0; i < rows / 2; i++)
		{
			BYTE
				*src = rgba + i * columns * bpp, 
				*dst = rgba + (rows - i - 1) * columns * bpp;
			memcpy(swap, dst, columns * bpp);
			memcpy(dst, src, columns * bpp);
			memcpy(src, swap, columns * bpp);
		}
		delete [] swap;
	}

	/*switch (header->attributes & TGA_ORIGIN_MASK)
	{
	case TGA_ORIG_LOWER_LEFT:
		break;
	case TGA_ORIG_LOWER_RIGHT:
		break;
	case TGA_ORIG_UPPER_LEFT:
		break;
	case TGA_ORIG_UPPER_RIGHT:
		break;
	default:
		memset(rgba, 0, rows * columns * bpp);
	}*/

	*bitmap = rgba;

	switch (bpp)
	{
	case 1:
		texture->format = ITexture::FMT_A8;
		break;
	case 3:
		texture->format = ITexture::FMT_R8G8B8;
		break;
	case 4:
		texture->format = ITexture::FMT_A8R8G8B8;
		break;
	}

	return true;
}









//
// Load a jpeg using jpeglib
//

// This procedure is called by the IJPEG library when an error
// occurs.
static void jpg_error_exit (j_common_ptr pcinfo){
	// Create the message string
	char sz[256];
	(pcinfo->err->format_message) (pcinfo, sz);
//  console.Insert("JPEG error: %s", sz);
}


static void jpg_init_source (j_decompress_ptr cinfo){
}


static boolean jpg_fill_input_buffer (j_decompress_ptr cinfo){
	struct jpeg_source_mgr * src = cinfo->src;
	static JOCTET FakeEOI[] = { 0xFF, JPEG_EOI };

	/* Insert a fake EOI marker */
	src->next_input_byte = FakeEOI;
	src->bytes_in_buffer = 2;

	return TRUE;
}


static void jpg_skip_input_data (j_decompress_ptr cinfo, long num_bytes) {
	struct jpeg_source_mgr * src = cinfo->src;

	if(num_bytes >= (long)src->bytes_in_buffer) {
		jpg_fill_input_buffer(cinfo);
		return;
	}

	src->bytes_in_buffer -= num_bytes;
	src->next_input_byte += num_bytes;
}


void jpg_term_source (j_decompress_ptr cinfo){
	/* no work necessary here */
}

bool Load_JPG(ITexture* tex, const BYTE* mem, size_t size, BYTE** bitmap)
{
	jpeg_decompress_struct cinfo;	// IJPEG decoder state.
	jpeg_error_mgr         jerr;	// Custom error manager.

	cinfo.err = jpeg_std_error (&jerr);
	jerr.error_exit = jpg_error_exit;	// Register custom error manager.

	jpeg_create_decompress (&cinfo);

	cinfo.src = (struct jpeg_source_mgr *) (*cinfo.mem->alloc_small) ((j_common_ptr) &cinfo, JPOOL_PERMANENT, sizeof(struct jpeg_source_mgr));

	cinfo.src->init_source = jpg_init_source;
	cinfo.src->fill_input_buffer = jpg_fill_input_buffer;
	cinfo.src->skip_input_data = jpg_skip_input_data;
	cinfo.src->resync_to_restart = jpeg_resync_to_restart;	// use default method
	cinfo.src->term_source = jpg_term_source;
	cinfo.src->bytes_in_buffer = size;
	cinfo.src->next_input_byte = mem;

	jpeg_read_header (&cinfo, TRUE);
	jpeg_start_decompress(&cinfo);

	if(cinfo.output_components != 3)
	{
		return false;
	}

	tex->width = cinfo.output_width;
	tex->height = cinfo.output_height;

	if (cinfo.out_color_space == JCS_GRAYSCALE){
		return false;
	} else {
		tex->format = ITexture::FMT_R8G8B8;
		*bitmap = new BYTE[3 * tex->width * tex->height];
		if (!*bitmap) 
			return false;
		BYTE *pDst = *bitmap;
		BYTE **ppDst = &pDst;
		int num_scanlines=0;
		while (cinfo.output_scanline < cinfo.output_height){
			num_scanlines=jpeg_read_scanlines (&cinfo, ppDst, 1);
			*ppDst += num_scanlines * 3 * cinfo.output_width;
		}
	}

	jpeg_finish_decompress(&cinfo);
	jpeg_destroy_decompress (&cinfo);

	if (jerr.num_warnings > 0) {
		delete[] *bitmap;
		*bitmap = 0;
		return false;
	} else 
		return true;
}












bool LoadTextureFromMemory(ITexture* texture, const char* mem2, int size)
{
	BYTE* bitmap = 0;

	if (!Load_TGA(texture, (BYTE*)mem2, size, &bitmap) && !Load_JPG(texture, (BYTE*)mem2, size, &bitmap))
	{
		return false;
	}
	
	int newwidth = (1 << Log2(texture->width));// >> output->GetPicMip();
	int newheight = (1 << Log2(texture->height));// >> output->GetPicMip();
	
	if(texture->height != newheight || texture->width != newwidth)
	{
		return false;
	}
	
	texture->render_info = new render_info_t;

	glGenTextures(1, &((render_info_t*)texture->render_info)->texture_id);

	glBindTexture(GL_TEXTURE_2D, ((render_info_t*)texture->render_info)->texture_id);

	int format = GL_RGB;
	int bpp = 3;
	BYTE* mem = bitmap;

	switch (texture->format)
	{
	case ITexture::FMT_A8: format = GL_ALPHA; bpp = 1; break;
	case ITexture::FMT_R8G8B8: format = GL_RGB; bpp = 3; break;
	case ITexture::FMT_A8R8G8B8: format = GL_RGBA; bpp = 4; break;
	default: bpp = 3;
	}

	/*if(texture->height != newheight || texture->width != newwidth)
	{
		mem = new BYTE[newheight * newwidth * bpp];

		gluScaleImage(GL_TEXTURE_2D, texture->width, texture->height, format, bitmap,newwidth, newheight, format, mem);
	
	}*/

	if (texture->miplevels == 1)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, format, newwidth, newheight, 0, format, GL_UNSIGNED_BYTE, mem);
	}
	else
	{
		
#ifdef GL_ES_VERSION_2_0
		glTexImage2D(GL_TEXTURE_2D, 0, format, newwidth, newheight, 0, format, GL_UNSIGNED_BYTE, mem);
		glGenerateMipmap(GL_TEXTURE_2D);
#else
		gluBuild2DMipmaps(GL_TEXTURE_2D, format, newwidth, newheight, format, GL_UNSIGNED_BYTE, (BYTE*)mem);
#endif
		
	}

	if (mem != bitmap) {
		delete bitmap;
	}

	((render_info_t*)texture->render_info)->bitmap = mem;

	return true;
}
