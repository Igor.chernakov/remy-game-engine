#include "c_client.hpp"

#include <boost/algorithm/string.hpp>

#define IGNORECASE_LOCALIZATION

#ifdef IGNORECASE_LOCALIZATION
#define TO_UPPER(x) boost::to_upper_copy(x)
#else
#define TO_UPPER(x) x
#endif

#include <sstream>

extern hash_t BuildHash(const char *_str);

namespace client
{

	unsigned short swap_bytes(unsigned short in){
		return (in << 8 & 0xFF00) | (in >> 8 & 0x00FF);
	}

	class Localization: public ILocalization
	{
		mutable std::map<int, std::wstring> m_strings;

	public:
		Localization()
		{
		}

		Localization(const std::string& path)
		{
			AddFile(path);
		}

		virtual const std::wstring& GetString(const std::string& name) const
		{
			static std::wstring __hack_holder;
			const int hash = BuildHash(TO_UPPER(name).c_str());

			if (m_strings.count(hash) > 0)
			{
				return m_strings[hash];
			}
			else
			{
				__hack_holder = std::wstring(name.begin(), name.end());
				return __hack_holder;
			}
		}

		void AddString(int hash, const std::wstring& string)
		{
			if (hash == 0)
				return;
			m_strings[hash] = string;
		}

		void Reset()
		{
			m_strings.clear();
		}

		void AddFile(const std::string& path) {
			file_t file = framework->GetFileSystem()->LoadFile(path);
			if (!file.valid())
				return;
			struct { int hash; std::wstring text; } iterator;
			iterator.hash = 0;

			char* mem = new char[file.size];
			fread(mem, file.size, 1, file);

			unsigned short *__mem = (unsigned short*)mem;
			std::vector<unsigned short> __buffer;
			switch (*__mem) {
				case 0xFEFF: // little-endian
					__buffer.assign(__mem + 1, __mem + (file.size / 2 - 1));
					break;
				case 0xFFFE: // big-endian
                    __buffer.resize(file.size / 2 - 1);
					std::transform(__mem + 1, __mem + (file.size / 2 - 1), __buffer.begin(), swap_bytes);
					break;
				default: { // single-byte
					__buffer.assign(mem, mem + file.size);
				}
			}

			file.close();
			delete []mem;

			std::wstring buffer(__buffer.begin(), __buffer.end());
			std::wstringstream input(buffer);
			while (!input.eof()) {
				std::wstring line;
				std::getline(input, line);
				boost::trim(line);
				if (boost::starts_with(line, L"//") || line.size() <= 0) {
					continue;
				}
				if (line[0] == L'*') {
					AddString(iterator.hash, iterator.text);
					std::string id(line.begin(), line.end());
					iterator.hash = BuildHash(TO_UPPER(id).c_str() + 1);
					iterator.text = std::wstring();
					if (m_strings.count(iterator.hash) > 0) {
						std::cout << "Duplicate localization entry for " << id.c_str() + 1 << std::endl;
					}
				} else {
					iterator.text += iterator.text.size() ? L"\n" + line : line;
				}
			}

			AddString(iterator.hash, iterator.text);
		}
	};
}

ILocalization* CreateLocalization()
{
	return new client::Localization();
}

void export_Localization()
{
	namespace py = boost::python;

	using client::Localization;

	DISPOSEME(3) py::class_<ILocalization, shared_ptr<ILocalization>, boost::noncopyable>("ILocalization", py::no_init)
		.def("GetString", py_cref(&ILocalization::GetString))
		;

	DISPOSEME(3) py::class_<Localization, shared_ptr<Localization>, py::bases<ILocalization>, boost::noncopyable>("Localization")
		.def(py::init<std::string>())
		.def("__getattr__", py_cref(&Localization::GetString))
		.def("AddFile", &Localization::AddFile)
		.def("Clear", &Localization::Reset)
		;
}
