#pragma once

#include <wx/wx.h>
#include <wx/splitter.h>

#include "e_viewport.hpp"

namespace editor
{
	class Window: public wxFrame
	{
	public:
		Window(const wxString& title);
		~Window();

	private:
		void InitializeMenu();

	private:
		wxSplitterWindow* m_splitter;
		wxPanel* m_controls;
		Viewport* m_viewport;
	};
}