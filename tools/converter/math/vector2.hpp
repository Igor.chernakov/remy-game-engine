#pragma once

#include "vector.hpp"

#include "macros.hpp"

namespace math
{
	struct Vector2
	{
		static const int Dimensions = 2;

		union
		{
			Real v[Dimensions];
			struct
			{
				Real x,y;
			};
		};

		static const Vector2 Zero;
		static const Vector2 One;

		Vector2();
		Vector2(const Vector2& param);
		Vector2(const std::string& param);
		Vector2(const Real* param);
		Vector2(Real x, Real y);

		DEFINE_DEFAULT_VECTOR_OPS(Vector2);

		Vector2& Assign(Real x, Real y);

		Vector2& Maximize();
		Vector2& Minimize();
	};
};
