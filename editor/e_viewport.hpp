#pragma once

#include <wx/wx.h>

namespace editor
{
	class Viewport: public wxPanel
	{
	public:
		Viewport(wxWindow *parent);
	};
}