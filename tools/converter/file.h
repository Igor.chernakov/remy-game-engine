#pragma once

#define F_BlockSkip(file) \
fseek(file, F_BlockReadSize(file), SEEK_CUR);

#define F_BlockReadLoop(file, size) \
for (long eof = ftell(file) + size; ftell(file) < eof;)

#define F_BlockRead(file, variable) \
fread(&variable, F_BlockReadSize(file), 1, file);

#define F_BlockReadConverVector(file, ptr, type) \
if (int size = F_BlockReadSize(file)) { \
	std::vector<type> _load(size / sizeof(type)); \
	fread((char*)&*_load.begin(), sizeof(type), _load.size(), file); \
	ptr.resize(_load.size()); \
	for (size_t i = 0; i < ptr.size(); ptr[i] = _load[i], ++i); \
}

#define F_BlockReadVector(file, ptr, type) \
if (int size = F_BlockReadSize(file)) { \
	ptr.resize(size / sizeof(type)); \
	fread((char*)&*ptr.begin(), sizeof(type), ptr.size(), file); \
}

#define F_BlockReadArray(file, array, type) \
array##Num = F_BlockReadSize(file) / sizeof(type); \
array = malloc(sizeof(type) * array##Num);\
fread(array, sizeof(type), array##Num, file);

#define F_BlockReadArrayMax(file, array, type, count) \
array##Num = F_BlockReadSize(file) / sizeof(type); \
fread(array, sizeof(type), array##Num, file);

static int F_BlockReadHeader(FILE* file) {
	int header;
	fread(&header, 4, 1, file);
	return header;
}

static int F_BlockReadSize(FILE* file) {
	int size;
	fread(&size, 4, 1, file);
	return size < 0 ? -size : size;
}

static std::string F_BlockReadString(FILE* file) {
	int size = F_BlockReadSize(file); 
	char* buffer = new char[size];
	fread(buffer, size, 1, file);
	std::string variable(buffer, size);
	delete[] buffer;
	return variable;
}

#define MAKEID(d,c,b,a)					( ((int)(a) << 24) | ((int)(b) << 16) | ((int)(c) << 8) | ((int)(d)) )


