#pragma once

//WarCraft 3 map class

#define SHADOWMAP_SIZE 512
#define REFLECTIONMAP_SIZE 256
//#define FAKE_SHADOWS
//#define USE_CHECKERTEXTURE 

using namespace game;

namespace WarCraft3 {

#include "warcraft3_mesh.h"
#include "warcraft3_sheets.h"

	class Map : public ::Map {
		friend ::MapFactory;
	public:
		static math::Matrix mShadowMatrix;
		static math::AxisAlignedBox mShadowBoundingBox;

		static const Material *pShadowMaterial;
		static const Material *pWaterMaterial;
		static const Material *pShadowVolumeBack;
		static const Material *pShadowVolumeFront;

		static Texture *pShadowMap;
		static Texture *pReflectionMap;

		enum render_mode_t {
			RENDER_TERRAIN = 0x1,
			RENDER_WATER = 0x4,
			RENDER_VEGETATION = 0x8,
			RENDER_SHADOWVOLUME = 0x10,
			RENDER_SHADOW = 0x20,
		};

		enum render_flags_t {
			RENDERFLAG_NONE = 0x0,
			RENDERFLAG_COMPUTESHADOWBOX = 0x1
		};

		static int nRenderFlags;

	private:
		Map(const std::string &filename) 
			: w3i(NULL)
			, w3e(NULL)
			, doo(NULL)
			, units(NULL)
		{
			const file::File pFileW3I(filename + "\\war3map.w3i");
			const file::File pFileW3E(filename + "\\war3map.w3e");
			const file::File pFileDOO(filename + "\\war3map.doo");
			const file::File pFileUnitsDOO(filename + "\\war3mapUnits.doo");

			const BYTE *dataW3I = pFileW3I.mem;
			const BYTE *dataW3E = pFileW3E.mem;
			const BYTE *dataDOO = pFileDOO.mem;
			const BYTE *dataUnitsDOO = pFileUnitsDOO.mem;

			nFlags = es::BaseEntity::FLAG_SPECIAL;

			Console::Write("Loading map \"%s\"...", filename.c_str());

			LoadStaticMaterialsAndTextures();

			int nTicks = GetTickCount();

			w3i = new w3i_t(&dataW3I);
			w3e = new w3e_t(&dataW3E);
			doo = new doo_t(&dataDOO);
			units = new units_t(&dataUnitsDOO);

			BuildQSPTree();
			ProcessStartPoints();

			Pathfinder::Allocate(GetWidth() * TILEDIVISION, GetHeight() * TILEDIVISION, TILESIZE / TILEDIVISION);

			Console::Write("Map loaded in %.2f sec.", (GetTickCount() - nTicks) / 1000.0f);
		};

		virtual ~Map() {
			SAFE_DEL(w3i);
			SAFE_DEL(w3e);
			SAFE_DEL(doo);
			SAFE_DEL(units);
			SAFE_DEL(pReflectionMap);
/*
#ifndef USE_CHECKERTEXTURE
			SAFE_DEL(pShadowMap);
			SAFE_DEL(pShadowMaterial);
#endif
*/
		};

		void ProcessStartPoints() {
			for (int i = 0; i < w3i->header->playersnum; i++) {
				Player p;
				p.vStartPoint.x = w3i->header->mPlayers[i].vStart.x - w3e->header->center.x;
				p.vStartPoint.y = w3i->header->mPlayers[i].vStart.y - w3e->header->center.y;
				p.vStartPoint.z = GetFloor(p.vStartPoint.xy()) / 1.01;
				OnStartPoint(&p);
			}
		}

		void LoadStaticMaterialsAndTextures() {
			pShadowVolumeBack = file::FileManager::Get()->Load2<file::MaterialFile>("Textures/System/ShadowVolumeBack");
			pShadowVolumeFront = file::FileManager::Get()->Load2<file::MaterialFile>("Textures/System/ShadowVolumeFront");

			pReflectionMap = new file::TextureFile;
			pShadowMap = new file::TextureFile;
#ifndef USE_CHECKERTEXTURE
			pShadowMaterial = new Material(pShadowMap);
#else
			pShadowMaterial = file::FileManager::Get()->Load2<file::MaterialFile>("Textures/System/Checker");;
#endif
			const_cast<Material*>(pShadowMaterial)->mStencil.nFunc = Material::Stencil::FUNC_EQUAL;
			const_cast<Material*>(pShadowMaterial)->mStencil.nReference = framework::Render::Get()->nDefaultStencil;
			const_cast<Material*>(pShadowMaterial)->pLayers[0].mTexturingMode.nAddressing = Material::Layer::TexturingMode::ADDRESS_CLAMP;
//			const_cast<Material*>(pShadowMaterial)->pLayers[0].mTexturingMode.nMagFilter = Material::Layer::TexturingMode::FILTER_POINT;
//			const_cast<Material*>(pShadowMaterial)->pLayers[0].mTexturingMode.nMinFilter = Material::Layer::TexturingMode::FILTER_POINT;

			pShadowMap->nWidth = nShadowMapSize;
			pShadowMap->nHeight = nShadowMapSize;
			pShadowMap->nFormat = framework::Render::Get()->GetBackBufferFormat();
			pShadowMap->nMipLevels = 1;

			framework::Render::Get()->CreateTexture(pShadowMap);

			pReflectionMap->nWidth = nReflectionMapSize;
			pReflectionMap->nHeight = nReflectionMapSize;
			pReflectionMap->nFormat = framework::Render::Get()->GetBackBufferFormat();
			pReflectionMap->nMipLevels = 1;

			framework::Render::Get()->CreateTexture(pReflectionMap);
		}

		void CalculatePerspectiveShadowMatrix() const {
			math::Matrix mProjection, mView, mBias;

			mBias.Scale(0.5, -0.5, 0.5);
			mBias.SetTranslate(0.5, 0.5, 0.5);

//			framework::Render::Get()->GetMatrix(IRender::MATRIX_PROJECTION, &mProjection);
			framework::Render::Get()->GetMatrix(IRender::MATRIX_MODELVIEW, &mView);

			const float zNear = 768, zFar = 1500;

			mProjection.Perspective(
				camera::Camera::Get()->fov,
				(float)camera::Camera::Get()->width / (float)camera::Camera::Get()->height,
				zNear,
				zFar);

			math::Quaternion vLightDir = -(World::Get()->pSun->vPosition) * (mProjection * mView);
			math::Vector3 vLightPos = (vLightDir / vLightDir.w);

			math::Matrix mLightProjection;
			math::Matrix mLightView;
			math::Vector3 mCenter(0,0,0.5);

			float	fRadius = 1.0f;
			float	fDist = (vLightPos - mCenter).len();
			float	fAngle = 2.5f * asinf(fRadius / fDist);
			float	n = fDist - fRadius * 2.5f;
			float	f = fDist + fRadius * 2.5f;

			if (n < 0.001f)
				n = 0.001f;

			mLightProjection.Perspective(fAngle, 1.0f, n, f);
			mLightView.LookAt(vLightPos, mCenter, math::Vector3(0,0,1));

			mShadowMatrix = mLightProjection * mLightView * mProjection * mView;

			framework::Render::Get()->SetMatrix(IRender::MATRIX_PROJECTION, &mShadowMatrix);
			framework::Render::Get()->SetMatrix(IRender::MATRIX_MODELVIEW, &math::Matrix());

			mShadowMatrix = mBias * mShadowMatrix;
		};

		void CalculateFakeShadowMatrix() const {
			math::Matrix mView, mProjection, mBias;

			mBias.Scale(0.5, -0.5, 0.5);
			mBias.SetTranslate(0.5, 0.5, 0.5);

			mShadowMatrix.Reset();

			mShadowBoundingBox.vMin.maximize();
			mShadowBoundingBox.vMax.minimize();
/*
			math::Vector3 vPoint1 = camera::Camera::Get()->eye;
			math::Vector3 vPoint2 = camera::Camera::Get()->eye + (camera::Camera::Get()->direction + camera::Camera::Get()->direction.cross(math::Vector3(0,0,1)) * 0.5) * 1000;
			math::Vector3 vPoint3 = camera::Camera::Get()->eye + (camera::Camera::Get()->direction - camera::Camera::Get()->direction.cross(math::Vector3(0,0,1)) * 0.5) * 1000;

			vPoint1.z = GetFloor(vPoint1.xy());
			vPoint2.z = GetFloor(vPoint2.xy());
			vPoint3.z = GetFloor(vPoint3.xy());

			const math::Vector3 vNormal = -(vPoint1 - vPoint2).cross(vPoint1 - vPoint3).normalized();

			mShadowMatrix.SetTranslate(0,0,(vPoint1 + vPoint2 + vPoint3).z / 3);
*/
			framework::Render::Get()->GetMatrix(IRender::MATRIX_PROJECTION, &mProjection);
			framework::Render::Get()->GetMatrix(IRender::MATRIX_MODELVIEW, &mView);

			model::CreateShadowMatrix(World::Get()->pSun->vPosition, &mShadowMatrix);

			mShadowMatrix = mProjection * mView * mShadowMatrix * mView.Inverted() * mProjection.Inverted();

			mShadowMatrix.m[2] = mShadowMatrix.m[6] = 0;
			mShadowMatrix.m[14] = 1;
			mShadowMatrix.m[10] = -mShadowMatrix.m[14];

			mShadowMatrix = mShadowMatrix * mProjection;

			math::Matrix mBackup = mShadowMatrix;

			mShadowMatrix = mBias * mShadowMatrix * mView;

			nRenderFlags |= RENDERFLAG_COMPUTESHADOWBOX;

			root->Render(RENDER_SHADOW);

			nRenderFlags &= ~RENDERFLAG_COMPUTESHADOWBOX;

			mShadowMatrix = mBackup;

//			mShadowMatrix.Translate(-mShadowBoundingBox.vMin.x, -mShadowBoundingBox.vMin.y, 0);

			framework::Render::Get()->SetMatrix(IRender::MATRIX_PROJECTION, &mShadowMatrix);

			mShadowMatrix = mBias * mShadowMatrix * mView;
		}

		virtual void ComputeShadows() const {
			DWORD nColor = 0;
			float fDepth = 0;

			Tools::Swap<DWORD>(nColor, framework::Render::Get()->nDefaultColor);
			Tools::Swap<float>(fDepth, framework::Render::Get()->fDefaultDepth);

#ifdef FAKE_SHADOWS
			CalculateFakeShadowMatrix();
#else
			CalculatePerspectiveShadowMatrix();
#endif

			RECT mViewport = framework::Render::Get()->GetViewport(), mTextureViewport;

			mTextureViewport.left = 0;
			mTextureViewport.top = 0;
			mTextureViewport.right = pShadowMap->nWidth;
			mTextureViewport.bottom = pShadowMap->nHeight;

			framework::Render::Get()->SetViewport(mTextureViewport);
			framework::Render::Get()->SetRenderState(IRender::STATE_SHADOWMAP);
			framework::Render::Get()->ClearScreen(IRender::DEPTH_BIT | IRender::COLOR_BIT);

			root->Render(RENDER_SHADOW);

			std::for_each(World::Get()->pRenderList.begin(), World::Get()->pRenderList.end(),
				std::bind2nd(std::mem_fun(&es::BaseEntity::Render), RENDER_SOLID));

			framework::Render::Get()->SetRenderState(IRender::STATE_NORMAL);

			Tools::Swap<DWORD>(nColor, framework::Render::Get()->nDefaultColor);
			Tools::Swap<float>(fDepth, framework::Render::Get()->fDefaultDepth);

			framework::Render::Get()->CopyBackBuffer(pShadowMap, mTextureViewport);
			framework::Render::Get()->ClearScreen(IRender::DEPTH_BIT | IRender::COLOR_BIT);
			framework::Render::Get()->SetViewport(mViewport);

			camera::Camera::Get()->SetPerspective();
		}

		virtual void ComputeReflections() const {
		};

		virtual void Render(RenderType nRenderType) const {
			switch (nRenderType) {
				case RENDER_PLANARSHADOWS:
					root->Render(RENDER_TERRAIN);
					//render unit selections over the terrain without depth-test
					((World*)World::Get())->RenderUnitsSelection();
					break;
				case RENDER_SOLID:
//					root->Render(RENDER_VEGETATION);
					break;
				case RENDER_VOLUMESHADOWS:
					root->Render(RENDER_SHADOWVOLUME);
					root->Render(RENDER_SHADOW);
					break;
				case RENDER_ALPHA:
					pWaterMaterial = w3e->GetWaterMaterial();

					root->Render(RENDER_WATER);
#if 0
					framework::Render::Get()->ClearScreen(r::Render::STENCIL_BIT);
					root->Render(RENDER_SHADOWVOLUME);
					pWaterMaterial = NULL;
					root->Render(RENDER_WATER);
#endif
					break;
			};
		}

		virtual void Update(float timestep) {
			root->Update();
		}

		virtual int GetWidth() const {
			if (!w3e)
				return 0;
			return w3e->header->width - 1;
		}

		virtual int GetHeight() const {
			if (!w3e)
				return 0;
			return w3e->header->height - 1;
		}

		virtual void ProcessStaticGrid() const {
			memcpy(pFlagGrid, w3e->data->pStaticGrid, sizeof(FLAG_TYPE) * TILEDIVISION * TILEDIVISION * (w3e->header->width - 1) * (w3e->header->height - 1));
		}

		virtual void PushObjectOnStaticGrid(const es::BaseEntity* pObject) {
			math::AxisAlignedBox box = pObject->GetBoundingBox();

			box.vMin = (box.vMin + pObject->GetProperties()->vPosition) / (TILESIZE / TILEDIVISION);
			box.vMax = (box.vMax + pObject->GetProperties()->vPosition) / (TILESIZE / TILEDIVISION);

			for (int x = max(box.vMin.x, 0); x <= min(box.vMax.x, w3e->header->width * TILEDIVISION - 1); x++) {
				for (int y = max(box.vMin.y, 0); y <= min(box.vMax.y, w3e->header->height * TILEDIVISION - 1); y++) {
					const float fRayHeight = 1024;
					const int nNumPoints = 4;
					math::Vector3 vPoints[nNumPoints] = {
						math::Vector3(x + 0.1, y + 0.1, 0) * (TILESIZE / TILEDIVISION),
						math::Vector3(x + 0.9, y + 0.1, 0) * (TILESIZE / TILEDIVISION),
						math::Vector3(x + 0.9, y + 0.9, 0) * (TILESIZE / TILEDIVISION),
						math::Vector3(x + 0.1, y + 0.9, 0) * (TILESIZE / TILEDIVISION)
					};
					for (int i = 0; i < nNumPoints; i++)
						if (pObject->GetModel()->Intersection(vPoints[i], vPoints[i] + math::Vector3(0,0,fRayHeight), pObject->GetProperties()->vMatrix, NULL)) {
							w3e->data->pStaticGrid[y * TILEDIVISION * (w3e->header->width - 1) + x] = true;
							break;
						}
				}
			}

		}

		virtual bool GetWayPoint(math::Vector3 *output) const {
			if (!output)
				return false;
			/*			const math::Vector3 vRayDirection = (camera::Camera::Get()->GetMouseTarget() - camera::Camera::Get()->GetEye()).normalized();
			const math::Vector3 vRayStart = camera::Camera::Get()->GetEye();

			output.x = vRayStart.x - vRayDirection.x * vRayStart.z / vRayDirection.z;
			output.y = vRayStart.y - vRayDirection.y * vRayStart.z / vRayDirection.z;
			output.z = 0;
			*/
			math::Vector3 vWayPoint = camera::Camera::Get()->GetMouseTarget();
			root->GetMouseRayIntersection(vWayPoint);

			if ((vWayPoint - camera::Camera::Get()->GetMouseTarget()).len() > math::EPS) {
				output->x = vWayPoint.x;
				output->y = vWayPoint.y;
				output->z = 0;
				return true;
			} else
				return false;
		}

#define INTERPOLATE(v, value, null) \
	if (!w3e) \
	return null; \
	math::Vector2 k, position = v / TILESIZE; \
	k.x = position.x - (int)position.x; \
	k.y = position.y - (int)position.y; \
	w3e_t::data_t::TilePoint *t = w3e->GetTilepoint((int)position.x, (int)position.y); \
	if (!t || position.x > w3e->header->width - 2 || position.y > w3e->header->height - 2) \
	return null; \
	return \
	((t->c[0]->value) * (1 - k.x) + (t->c[3]->value) * k.x) * (1 - k.y) + \
	((t->c[1]->value) * (1 - k.x) + (t->c[2]->value) * k.x) * k.y;

		virtual float GetFloor(const math::Vector2 &v) const {
			INTERPOLATE(v, position.z, 0);
		}

		virtual float GetFloorSmooth(const math::Vector2 &v) const {
			INTERPOLATE(v, soften_height, 0);
		}

		virtual float GetPointInternalHeight(const math::Vector2 &v) const {
			INTERPOLATE(v, height, 0);
		}

		virtual math::Vector3 GetFloorNormal(const math::Vector2 &v) const {
			INTERPOLATE(v, normal, math::Vector3(0,0,1));
		}

#undef INTERPOLATE

	private:

#define COPYVAR(dst, src) { memcpy(&dst, *src, sizeof(dst)); *src += sizeof(dst); }
#define COPYMEM(dst, src, size) { memcpy(dst, *src, size); *src += size; }

#include "warcraft3_w3i.h"
#include "warcraft3_w3e.h"
#include "warcraft3_doo.h"
#include "warcraft3_units.h"
#include "warcraft3_node.h"

#undef COPYVAR
#undef COPYMEM

		w3i_t *w3i;
		w3e_t *w3e;
		doo_t *doo;
		units_t *units;

		static const int TILEDEFAULTHEIGHT = 0x2000;
		static const int TILESUBHEIGHT = 4;
		static const int TILELAYERHEIGHT = TILESIZE;
		static const int TILEZEROLAYER = 4;
		static const int nReflectionMapSize = REFLECTIONMAP_SIZE;
		static const int nShadowMapSize = SHADOWMAP_SIZE;
		static const int nShadowMapScale = 1000;
	};

	const Material *Map::pShadowMaterial = NULL;
	const Material *Map::pWaterMaterial = NULL;
	const Material *Map::pShadowVolumeBack = NULL;
	const Material *Map::pShadowVolumeFront = NULL;

	Texture *Map::pShadowMap = NULL;
	Texture *Map::pReflectionMap = NULL;

	math::Matrix Map::mShadowMatrix;
	math::AxisAlignedBox Map::mShadowBoundingBox;

	int Map::nRenderFlags = 0;
};
