/**
	CONTROLLERS are used to apply basic time-dependent functional to any GAME or UI object
	Those can be attached to objects and are automatically removed upon finishing firing an EVENT
**/

#include "c_client.hpp"
#include "c_controller.hpp"
#include "c_entity.hpp"

using namespace boost::python;

namespace client { namespace controller {
	
	/**
		TIMER can be considered a postponed function call
	**/

	struct Timer: Controller
	{
		Timer()
			: m_time(std::pair<float, float>(0, FLT_MAX))
			, m_repeat(std::pair<int, int>(0, 1))
			, m_entity(0)
			, m_widget(0)
			, m_value(0)
		{
		}

		void OnAnimate(float timestep)
		{
			timestep *= m_rate;

			for (m_time.first += timestep; m_time.first >= m_time.second; m_time.first -= m_time.second)
			{
				try
				{
					if (m_function)
					{
						call<void>(m_function.ptr(), 
							m_entity ? (object)m_entity->shared_from_this() : (object)m_widget->shared_from_this());
					}
					if ((++m_repeat.first >= m_repeat.second && m_repeat.second > 0) ||
						(m_condition && !call<bool>(m_condition.ptr(), 
							m_entity ? (object)m_entity->shared_from_this() : (object)m_widget->shared_from_this())))
					{
						Finished();
						Dispose();
						break;
					}
				}
				catch (const error_already_set&)
				{
					PyErr_Print();
					Dispose();
					break;
				}
			}

			m_value = m_time.first / m_time.second;
		}

		bool OnAttach(Entity* self) { return m_entity = self; }
		bool OnAttach(Widget* self) { return m_widget = self; }

		inline float GetTime() const { return m_time.second; }
		inline void SetTime(float value) { m_time = std::pair<float, float>(value * m_value, value); }

		inline int GetRepeats() const { return m_repeat.second; }
		inline void SetRepeats(int value) { m_repeat = std::pair<int, int>(0, (value == 1 && m_condition) ? 0 : value); }

		inline object GetCallback() const { return m_function; }
		inline void SetCallback(object value) { m_function = value; }

		inline object GetCondition() const { return m_condition; }
		inline void SetCondition(object value) { m_condition = value; SetRepeats(GetRepeats()); }

		float GetValue() const { return m_value; }

		void SetValue(float value)
		{
			m_value = value;
			m_time.first = value * m_time.second;
		}

		void Finalize()
		{
			if (m_function)
			{
				call<void>(m_function.ptr(), 
					m_entity ? (object)m_entity->shared_from_this() : (object)m_widget->shared_from_this());
			}

			Finished();

			Dispose();
		}

		Event Finished;

		Entity* m_entity;
		Widget* m_widget;

		object m_function, m_condition;
		std::pair<float, float> m_time;
		std::pair<int, int> m_repeat;
		float m_value;
	};

	/**
		TRANSITION is an analog of TWEEN library for ActionScript.
		Used to apply A->B animation to virtually any variable of an object in Python.
	**/

	struct Transition: Controller, boost::enable_shared_from_this<Transition>
	{
		void OnAnimate(float timestep)
		{
			timestep *= m_rate;

			PyObject *o = _entity ? _entity->GetPythonObject() : _widget->GetPythonObject();
			float out = 0, t = m_time, b = 0, c = 1, d = m_length;
			switch (easeflags) {
				case math::EASE_IN: out = easer->easeIn(t, b, c, d); break;
				case math::EASE_OUT: out = easer->easeOut(t, b, c, d); break;
				case math::EASE_IN_OUT: out = easer->easeInOut(t, b, c, d); break;
			}

			if ((m_time += timestep) >= m_length) {
				if (loop) {
					SetValue(0);
					m_delay = xdelay;
				} else {
					Finalize();
				}
			} else {
				BOOST_FOREACH(IWorker* worker, m_workers) {
					worker->Lerp(o, out);
				}
			}
		}

		Entity* _entity;
		Widget* _widget;
		
		bool loop;

		bool OnAttach(Entity* self) { _entity = self; return true; }
		bool OnAttach(Widget* self) { _widget = self; return true; }

		float GetValue() const { return m_time / m_length; }
		void SetValue(float value) { m_time = value * m_length; }

		void Finalize()
		{
			BOOST_FOREACH(IWorker* worker, m_workers) {
				worker->Finalize(_entity ? _entity->GetPythonObject() : _widget->GetPythonObject());
			}
			TransitionFinished();
			Dispose();
		}

		Event
			TransitionFinished;

		float m_time, m_length;

		struct IWorker
		{
			virtual void Lerp(PyObject *object, float k) = 0;
			virtual void Finalize(PyObject *object) = 0;
		};

		std::list<IWorker*> m_workers;

		template <typename T>
		static T MyLerp(float k, const T& from, const T& to)
		{
			return T::Lerp(k, from, to);
		}

		template <typename T, typename Y>
		struct Worker: IWorker
		{
			Worker(Y variable, const T& to)
				: m_variable(variable)
				, m_to(to)
				, got_from(false)
			{}
			virtual void Lerp(PyObject *o, float k) {
				object py(borrowed(o));
				if (!got_from) {
					m_from = extract<T>(py.attr(m_variable.c_str()));
					got_from = true;
				}
				py.attr(m_variable.c_str()) = MyLerp<T>(k, m_from, m_to);
			}
			virtual void Finalize(PyObject *o) {
				object(borrowed(o)).attr(m_variable.c_str()) = m_to;
			}
			Y m_variable;
			T m_from, m_to;
			bool got_from;
		};

		math::InterpolationType GetEaser() const { //TODO: do we really need this func?
			return math::LINEAR;
		}

		void SetEaser(math::InterpolationType it) {
			delete easer;

			switch (it) {
				case math::LINEAR: easer = new math::Linear; break;
				case math::SINE: easer = new math::Sine; break;
				case math::QUINT: easer = new math::Quint; break;
				case math::QUART: easer = new math::Quart; break;
				case math::QUAD: easer = new math::Quad; break;
				case math::EXPO: easer = new math::Expo; break;
				case math::ELASTIC: easer = new math::Elastic; break;
				case math::CUBIC: easer = new math::Cubic; break;
				case math::CIRC: easer = new math::Circ; break;
				case math::BOUNCE: easer = new math::Bounce; break;
				case math::BACK: easer = new math::Back; break;
				default: easer = new math::Linear;
			}
		}
		
		float xdelay;

		Transition()
			: easeflags(math::EASE_IN_OUT)
			, m_time(0)
			, m_length(0)
			, easer(new math::Linear)
			, _entity(0)
			, _widget(0)
			, loop(false)
			, xdelay(0)
		{
		}

		/*py::object Vars(py::tuple args, py::dict kwargs) 
		{  
			return py::object();
		} */

#define TransitionVariable(Type) \
		shared_ptr<Transition> AddVariable(const std::string& variable, const Type& to) {  \
			m_workers.push_back(new Worker<Type, std::string>(variable, to)); \
			return shared_from_this(); \
		}

#define TransitionNamedVariable(FunctionName, VariableName, Type) \
		shared_ptr<Transition> Add##FunctionName(const Type& to) {  \
			m_workers.push_back(new Worker<Type, std::string>(VariableName, to)); \
			return shared_from_this(); \
		}

		TransitionVariable(point_t);
		TransitionVariable(rect_t);
		TransitionVariable(int);
		TransitionVariable(float);
		TransitionVariable(vec2_t);
		TransitionVariable(vec3_t);
		TransitionVariable(quat_t);
		TransitionVariable(box2_t);
		TransitionVariable(box3_t);
		TransitionVariable(color_t);

		TransitionNamedVariable(Position, "position", vec2_t);
		TransitionNamedVariable(Position, "position", vec3_t);
		TransitionNamedVariable(Rotation, "rotation", float);
		TransitionNamedVariable(Rotation, "rotation", quat_t);
		TransitionNamedVariable(Scale, "scale", vec2_t);
		TransitionNamedVariable(Scale, "scale", vec3_t);

		TransitionNamedVariable(Color, "color", color_t);
		TransitionNamedVariable(Transparency, "transparency", float);


		~Transition() {
			delete easer;
			std::for_each(m_workers.begin(), m_workers.end(), deleter_t<IWorker>());
		}

		math::Easing* easer;
		math::EaseFlags easeflags;

#undef TransitionVariable
	};

	template <>
	float Transition::MyLerp<float>(float k, const float& from, const float& to)
	{
		return from * (1 - k) + to * k;
	}

	template <>
	int Transition::MyLerp<int>(float k, const int& from, const int& to)
	{
		return int(from * (1 - k) + to * k);
	}

	template <>
	point_t Transition::MyLerp<point_t>(float k, const point_t& from, const point_t& to)
	{
		return point_t(MyLerp(k, from.x, to.x), MyLerp(k, from.y, to.y));
	}

	template <>
	rect_t Transition::MyLerp<rect_t>(float k, const rect_t& from, const rect_t& to)
	{
		return rect_t(
			MyLerp(k, from.left, to.left), MyLerp(k, from.top, to.top),
			MyLerp(k, from.right, to.right), MyLerp(k, from.bottom, to.bottom));
	}

	/**
		ANIMATOR is used to apply looping animation to some variables of an object in Python.
	**/

	struct Animator: Controller, boost::enable_shared_from_this<Animator>
	{
		void OnAnimate(float timestep)
		{
			timestep *= m_rate;

			if (int(m_time) != int(m_time + timestep)) {
				CycleFinished();
			}

			m_time += timestep;

			if (time > 0 && m_time >= time) {
				Finalize(); 
				return;
			}

			float k = (0.5f - cos(2 * m_time * speed * M_PI) * 0.5f) * amount + 1;
			float k2 = cos(2 * m_time * speed * M_PI) * 0.5f * amount;

			if (_widget) {
				switch (type) {
				case ANIMATOR_PULSE:
					 _widget->SetScale(widget_scale * k);
				break;
				case ANIMATOR_COLORPULSE:
					 _widget->SetLocalColor(color_t(color.GetRed() * k, color.GetGreen() * k, color.GetBlue() * k, color.GetAlpha()));
				break;
				case ANIMATOR_ALPHAPULSE:
					 _widget->SetLocalColor(color_t(color.GetRed(), color.GetGreen(), color.GetBlue(), color.GetAlpha() * k));
				break;
				case ANIMATOR_ROTATE:
					 _widget->SetRotation(m_time * speed * 360 + widget_rotation);
					break;
				case ANIMATOR_SHAKE:
					 _widget->SetRotation(k2 + widget_rotation);
					break;
				default:
					break;
				}
			} else if (_entity) {
				switch (type) {
				case ANIMATOR_PULSE:
					 _entity->SetScale(entity_scale * k);
				break;
				case ANIMATOR_COLORPULSE:
					 _entity->SetLocalColor(color_t(color.GetRed() * k, color.GetGreen() * k, color.GetBlue() * k, color.GetAlpha()));
				break;
				case ANIMATOR_ALPHAPULSE:
					 _entity->SetLocalColor(color_t(color.GetRed(), color.GetGreen(), color.GetBlue(), color.GetAlpha() * k));
				break;
				case ANIMATOR_ROTATE:
					 _entity->SetRotation(entity_rotation * quat_t(0, 0, m_time * speed * 360));
					break;
				case ANIMATOR_SHAKE:
					 _entity->SetRotation(entity_rotation * quat_t(0, 0, k2));
					break;
				default:
					break;
				}
			}

		}

		Widget* _widget;
		Entity* _entity;

		color_t color;

		vec2_t widget_scale;
		float widget_rotation;

		vec3_t entity_scale;
		quat_t entity_rotation;

		bool OnAttach(Entity* self) { 
			_entity = self; 
			_entity->RemoveControllers<Animator>();
			entity_scale = self->GetScale(); 
			entity_rotation = self->GetRotation(); 
			color = self->GetLocalColor();
			return true;
		}
		bool OnAttach(Widget* self) { 
			_widget = self; 
			_widget->RemoveControllers<Animator>();
			widget_scale = self->GetScale(); 
			widget_rotation = self->GetRotation(); 
			color = self->GetLocalColor();
			return true;
		}

		float GetValue() const { return m_time / m_length; }
		void SetValue(float value) { m_time = value * m_length; }

		void Finalize()
		{
			if (_widget) {
				switch (type) {
				case ANIMATOR_PULSE: 
					_widget->SetScale(widget_scale);
					break;
				case ANIMATOR_ROTATE:
				case ANIMATOR_SHAKE:
					_widget->SetRotation(widget_rotation);
					break;
				case ANIMATOR_COLORPULSE: 
				case ANIMATOR_ALPHAPULSE:
					_widget->SetLocalColor(color);
					break;
				default:
					break;
				}
			} else if (_entity) {
				switch (type) {
				case ANIMATOR_PULSE: 
					_entity->SetScale(entity_scale);
					break;
				case ANIMATOR_ROTATE:
				case ANIMATOR_SHAKE:
					_entity->SetRotation(entity_rotation);
					break;
				case ANIMATOR_COLORPULSE: 
				case ANIMATOR_ALPHAPULSE:
					_entity->SetLocalColor(color);
					break;
				default:
					break;
				}
			}

			Dispose();
		}

		Event
			CycleFinished;

		float m_time, m_length;

		Animator()
			: m_time(0)
			, m_length(0)
			, _widget(0)
			, type(ANIMATOR_UNKNOWN)
			, speed(1.0f)
			, amount(0.5f)
			, time(-1)
		{
		}

		enum Type {
			ANIMATOR_UNKNOWN,
			ANIMATOR_PULSE,
			ANIMATOR_ROTATE,
			ANIMATOR_COLORPULSE,
			ANIMATOR_ALPHAPULSE,
			ANIMATOR_SHAKE,
		};

		Event Finished;

		Type type;
		float speed;
		float amount;
		float time;
	};

	/**
		ACCELERATOR is a basic class for movement controllers
	**/

	struct Accelerator: Controller
	{
		Accelerator()
			: m_topspeed(0)
			, m_acceleration(0)
			, m_speed(0)
			, m_total(0)
			, m_done(0)
			, m_self(0)
			, m_paused(false)
		{
		};

		float GetDistance(float timestep)
		{
			timestep *= m_rate;

			float distance = 0;

			if (m_acceleration > 0)
			{
				if (m_paused) {
					if ((m_speed -= timestep * m_acceleration) > 0) {
						distance = (m_speed - timestep * m_acceleration / 2.f) * timestep;
					} else {
						m_speed = 0;
					}
					m_done = 0;
					m_total -= distance;
					return distance;
				} else if (MAX(GetDistanceLeft(), 0) < MIN(m_done, m_speed * m_speed / (m_acceleration * 2))) {
					m_speed = sqrt(MAX(GetDistanceLeft(), 0) * m_acceleration * 2);
					distance = (m_speed/* - timestep * m_acceleration / 2.f*/) * timestep;
				} else {
					distance = MIN(m_speed + m_acceleration * timestep / 2.f, m_topspeed) * timestep;
					m_speed = MIN(MAX(sqrt(m_done * m_acceleration * 2), m_speed), m_topspeed);
				}
		
				m_total -= distance;
				m_done += distance;
			}
			else
			{
				distance = m_speed * timestep;
			}

			return distance;
		}

		virtual float GetDistanceLeft() const { return m_total; }

		float GetEstimatedTime_Strict() const
		{
			return GetEstimatedTime();
		}

		float GetEstimatedTime(float *total = 0) const
		{
			float _total = total ? *total : m_total;

			if (m_acceleration <= 0)
			{
				return _total / GetTopSpeed();
			}
			else
			{
				const float
					time_stop  =  m_topspeed            / m_acceleration,
					time_start = (m_topspeed - m_speed) / m_acceleration,
					dist_stop  = (m_topspeed - m_acceleration * time_stop  / 2) * time_stop,
					dist_start = (m_speed    + m_acceleration * time_start / 2) * time_start;

				if (dist_stop + dist_start <= _total) // will reach m_topspeed
				{
					return (_total - dist_stop - dist_start) / m_topspeed + time_stop + time_start;
				}
				else
				{
					const float
						time_stop = m_speed / m_acceleration,
						dist_stop = (m_speed - m_acceleration * time_stop / 2) * time_stop,
						new_total = (_total - dist_stop) / 2;

					const float 
						a = m_acceleration / 2,
						b = m_speed,
						c = -new_total,
						x0 = (sqrt(b * b - 4 * a * c) - m_speed) / (2 * a)/*,
						x1 = (sqrt(b * b - 4 * a * c) + m_speed) / (2 * a)*/;

					return 2 * /*MIN(x0, x1)*/x0 + time_stop;

					//return (2 * sqrt(m_acceleration * _total + m_speed * m_speed / 2) - m_speed) / m_acceleration;
				}
			}
		}

		inline float GetTopSpeed() const { return m_topspeed; }
		inline void SetTopSpeed(float value) { m_topspeed = value; }

		inline float GetAcceleration() const { return m_acceleration; }
		inline void SetAcceleration(float value) { m_acceleration = value; }

		inline float GetSpeed() const { return m_speed / m_topspeed; }
		inline void SetSpeed(float value) { m_speed = value * m_topspeed; }

		inline void SetSelf(Entity* ptr) { m_self = ptr; }
		inline Entity* GetSelf() const { return m_self; }

		float m_speed, m_done, m_total, m_acceleration, m_topspeed;
		Entity* m_self;

		bool m_paused;
	};

	struct Follow: Controller
	{
		Follow(): angle(90) {}

		void OnAnimate(float timestep)
		{
			timestep *= m_rate;

			if (!target)
				return;

			const vec3_t d = vec3_t::Normalize((_self->GetMatrix().GetTranslation() - 
				target->GetMatrix().GetTranslation()) * vec3_t(1, 1, 0));
			const mat4_t m(vec3_t::Cross(d, mat4_t::Identity.GetUp()), d, mat4_t::Identity.GetUp());
			const quat_t r = (m * mat4_t::Invert(_self->GetParent()->GetMatrix())).GetRotation();

			bool check = vec3_t::Dot(vec3_t(0, 0, 1) * _initial, vec3_t(0, 0, 1) * r) > cos(angle * 3.1415 / 180);

			if (in_bounds && !check)
				LostSight();

			if (!in_bounds && check)
				GotSight();

			if (in_bounds = check)
				_self->SetRotation(r);
		}

		bool OnAttach(Entity* self)
		{
			_self = self;
			_initial = self->GetRotation();
			_self->RemoveControllers<Follow>();
			in_bounds = true;
			return true;
		}

		bool OnAttach(Widget* self)
		{
			return false;
		}

		void Finalize() {}
		float GetValue() const { return 0; }
		void SetValue(float value) {}

		Event LostSight, GotSight;

		Entity* _self;
		quat_t _initial;
		bool in_bounds;
		float angle;
		shared_ptr<Entity> target;
	};

	/**
		LOOKAT is used to softly animate turning to specific point
	**/

	struct LookAt: Accelerator
	{
		int m_animLayer;

		LookAt(): variable("turning"), m_animLayer(1) {}

		void OnAnimate(float timestep)
		{
			timestep *= m_rate;

			const quat_t r = GetNeededRotation(m_point);
			const float distance = GetDistance(timestep);

			if (IsProper() && distance / GetDistanceLeft() < 1.0f && distance > 0)
			{
				GetSelf()->SetAnimLayerWeight(m_animLayer, GetSpeed());
				GetSelf()->SetRotation(quat_t::Lerp(MIN(1.0f, distance / GetDistanceLeft()), GetSelf()->GetRotation(), r));
			}
			else
			{
				GetSelf()->SetAnimLayerWeight(m_animLayer, 0);
				GetSelf()->SetRotation(r);
				RotationFinished();
				Dispose();

				m_finished = true;
			}
		}

		bool OnAttach(Entity* self)
		{
			SetSelf(self);
			GetSelf()->RemoveControllers<LookAt>();
			return true;
		}

		bool OnAttach(Widget* self)
		{
			return false;
		}

		quat_t GetNeededRotation(const vec3_t &point) const {
			vec3_t d(vec3_t::Normalize((GetSelf()->GetMatrix().GetTranslation() - point) * vec3_t(1, 1, 0)));
			mat4_t m(vec3_t::Cross(d, mat4_t::Identity.GetUp()), d, mat4_t::Identity.GetUp());
			return (m * mat4_t::Invert(GetSelf()->GetParent()->GetMatrix())).GetRotation();
		}

		float GetDistanceLeft(const vec3_t* m_lookat = 0) const
		{
			quat_t r = GetNeededRotation(m_lookat ? *m_lookat : m_point);
			return fabs((-GetSelf()->GetRotation() * r).GetRoll());
		}

		void Finalize()
		{
			SetTopSpeed(-1);
			OnAnimate(0);
			GetSelf()->SetRotation(GetNeededRotation(m_point));
		}

		float GetValue() const { return 0; }
		void SetValue(float value) {}

		const std::string& GetVariable() const { return variable; }
		void SetVariable(const std::string& value) {
			variable = value;
		}

		inline bool IsProper() const { return GetTopSpeed() > 0; }

		inline const vec3_t& GetTarget() const { return m_point; }
		inline void SetTarget(const vec3_t& value) { 
			m_total = GetDistanceLeft(&value);
			m_point = value;
			m_finished = false;
		}

		Event
			RotationFinished;

		vec3_t m_point;
		std::string variable;
		bool m_finished;
	};

	/**
		PATHFOLLOWER uses A-Star to calculate path and softly follows it
	**/

	struct PathFollower: Accelerator
	{
		bool m_started, m_stopping, m_done_but_paused, m_ignoreobsacles;

		int m_animLayer;

		PathFollower()
			: m_variable("running")
			, m_lookat(new LookAt)
			, m_follow(true)
			, m_started(false)
			, m_stopping(false)
			, m_use_lookat(false)
			, m_dont_go(false)
			, m_done_but_paused(false)
			, m_ignoreobsacles(true)
			, m_animLayer(2)
		{
			m_lookat->SetVariable("$undefined");
			m_lookat->m_animLayer = 0;
		}

		void OnAnimate(float timestep)
		{
			timestep *= m_rate;

			float distance = 0;

			if (m_done_but_paused && !m_paused)
				goto done_walking;

			if (!m_route)
			{
				MovementFailed();
				Dispose();
				return;
			}

			distance = GetDistance(timestep);

			if (!m_started) {
				MovementStarted(tuple(), dict());
				m_started = true;
			}

			if (distance < 0)
				goto done_walking;

			if (m_dont_go)
				goto done_walking2;

			while ((GetSelf()->GetMatrix().GetTranslation() - m_route->GetWaypoint()).Length() < distance)
			{
				distance -= (GetSelf()->GetMatrix().GetTranslation() - m_route->GetWaypoint()).Length();
				GetSelf()->SetPosition(m_route->GetWaypoint() * mat4_t::Invert(GetSelf()->GetParent()->GetMatrix()));
				if (!m_route->GoToNextWaypoint())
				{
					distance = -1;
					goto done_walking;
				}
			}

			if (m_follow) {
				float distance = m_lookat->GetDistanceLeft(&m_target_lookat);

				m_lookat->SetTopSpeed(sqrt(GetTopSpeed()) * 20);
				m_lookat->SetAcceleration(m_lookat->GetTopSpeed() * 3);

				if (m_use_lookat && (m_stopping || m_lookat->GetEstimatedTime(&distance) >= GetEstimatedTime())) {
					m_lookat->SetTarget(m_target_lookat);
					m_stopping = true;
				} else {
					m_lookat->SetTarget(m_route->GetWaypoint());
				}


				m_lookat->OnAnimate(timestep);
			}

			SetSpeedVariable(GetSpeed());

			GetSelf()->SetPosition(
				(GetSelf()->GetMatrix().GetTranslation() + 
				vec3_t::Normalize(m_route->GetWaypoint() - GetSelf()->GetMatrix().GetTranslation()) * distance) * 
				mat4_t::Invert(GetSelf()->GetParent()->GetMatrix()));

			return;

done_walking:
			if (m_follow && m_use_lookat && !m_lookat->m_finished) {
				//m_lookat->OnAnimate(timestep);
				//return;
				m_lookat->Finalize();
			}

done_walking2:
			if (m_paused) {
				m_done_but_paused = true;
				return;
			}

			SetSpeedVariable(0.0f);
			MovementFinished();
			Dispose();
		}

		float GetCurrentAngle()
		{
			return m_lookat ? m_lookat->GetDistanceLeft() : 0;
		}

		inline void SetSpeedVariable(float value)
		{
			GetSelf()->SetAnimLayerWeight(m_animLayer, value);
		}


		bool OnAttach(Entity* self)
		{
			SetSelf(self);
			GetSelf()->RemoveControllers<PathFollower>();
			m_lookat->OnAttach(self);
			return true;
		}

		bool OnAttach(Widget* self) { return false; }

		void Finalize()
		{
			GetSelf()->SetPosition(m_route->GetDestination() * mat4_t::Invert(GetSelf()->GetParent()->GetMatrix()));
			SetSpeedVariable(0.0f);
			MovementFinished();
			Dispose();
		}

		inline const std::string& GetVariable() const { return m_variable; }
		inline void SetVariable(const std::string& value) { m_variable = value; }

		inline const vec3_t& GetTarget() const { return m_target; }
		void SetTarget(const vec3_t& value)
		{
			bool is_noclip = GetSelf()->GetFlags(Entity::FLAG_NOCLIP);

			GetSelf()->SetFlags(Entity::FLAG_NOCLIP, true);

			if (m_route = framework->GetScene()->GetNavigation()->GetRoute(
				GetSelf()->GetMatrix().GetTranslation(), m_target = value, true))
			{
				m_total = m_route->GetLength();
				m_stopping = false;
				SetSpeed(GetSelf()->GetAnimLayerWeight(m_animLayer));
				if (m_route->GetLength() <= 0.01f)
				{
					m_dont_go = true;
				}
			}
			else if (m_route = framework->GetScene()->GetNavigation()->GetRoute(
				GetSelf()->GetMatrix().GetTranslation(), m_target = value, false))
			{
				m_total = m_route->GetLength();
				m_stopping = false;
				SetSpeed(GetSelf()->GetAnimLayerWeight(m_animLayer));
				if (m_route->GetLength() <= 0.01f)
				{
					m_dont_go = true;
				}
			}

			GetSelf()->SetFlags(Entity::FLAG_NOCLIP, is_noclip);
		}

		bool HasRoute() const {
			return m_route.get();
		}

		inline const vec3_t& GetLookAt() const { return m_target_lookat; }
		void SetLookAt(const vec3_t& value)
		{
			m_target_lookat = value;
			m_use_lookat = true;
		}

		float GetValue() const { return 0; }
		void SetValue(float value) {}

		Event
			MovementStarted,
			MovementFinished,
			MovementFailed;

		bool m_follow;

	private:
		shared_ptr<LookAt> m_lookat;
		shared_ptr<IRoute> m_route;
		std::string m_variable;
		vec3_t m_target;
		vec3_t m_target_lookat;
		bool m_use_lookat;
		bool m_dont_go;
	};
}}

void export_Controllers()
{
	using namespace boost::python;
	using namespace client;
	using namespace client::controller;

	DISPOSEME(3) class_<Controller, shared_ptr<Controller>, boost::noncopyable>("Controller", no_init)
		.def("Finalize", &Controller::Finalize)
		.def("Dispose", &Controller::Dispose)
		.def("Enable", &Controller::Enable)
		.def("Disable", &Controller::Disable)
		.add_property("disposed", &Controller::Disposed)
		.add_property("enabled", &Controller::IsEnabled, &Controller::SetEnabled)
		.add_property("value", &Controller::GetValue, &Controller::SetValue)
		.def_readwrite("delay", &Controller::m_delay)
		.def_readwrite("rate", &Controller::m_rate)
		;

	DISPOSEME(3) class_<LookAt, shared_ptr<LookAt>, bases<Controller>, boost::noncopyable>("LookAt")
		.def_readwrite("RotationFinished", &LookAt::RotationFinished)
		.add_property("speed", &LookAt::GetTopSpeed, &LookAt::SetTopSpeed)
		.add_property("acceleration", &LookAt::GetAcceleration, &LookAt::SetAcceleration)
		.add_property("target", py_cref(&LookAt::GetTarget), &LookAt::SetTarget)
		.add_property("variable", py_cref(&LookAt::GetVariable), &LookAt::SetVariable)
		.add_property("time", &LookAt::GetEstimatedTime_Strict)
		;

	DISPOSEME(3) class_<PathFollower, shared_ptr<PathFollower>, bases<Controller>, boost::noncopyable>("PathFollower")
		.def_readwrite("MovementStarted", &PathFollower::MovementStarted)
		.def_readwrite("MovementFinished", &PathFollower::MovementFinished)
		.def_readwrite("MovementFailed", &PathFollower::MovementFailed)
		.add_property("speed", &PathFollower::GetTopSpeed, &PathFollower::SetTopSpeed)
		.add_property("acceleration", &PathFollower::GetAcceleration, &PathFollower::SetAcceleration)
		.add_property("target", py_cref(&PathFollower::GetTarget), &PathFollower::SetTarget)
		.add_property("variable", py_cref(&PathFollower::GetVariable), &PathFollower::SetVariable)
		.add_property("lookat", py_cref(&PathFollower::GetLookAt), &PathFollower::SetLookAt)
		.add_property("time", &PathFollower::GetEstimatedTime_Strict)
		.add_property("has_route", &PathFollower::HasRoute)
		.add_property("angle", &PathFollower::GetCurrentAngle)
		.def_readwrite("follow", &PathFollower::m_follow)
		.def_readwrite("paused", &PathFollower::m_paused)
		.def_readwrite("ignoreobsacles", &PathFollower::m_ignoreobsacles)
		.def_readonly("distance_done", &PathFollower::m_done)
		.def_readonly("distance_left", &PathFollower::m_total)
		;

	DISPOSEME(3) class_<Follow, shared_ptr<Follow>, bases<Controller>, boost::noncopyable>("Follow")
		.def_readwrite("GotSight", &Follow::GotSight)
		.def_readwrite("LostSight", &Follow::LostSight)
		.def_readwrite("angle", &Follow::angle)
		.def_readwrite("target", &Follow::target)
		;

	DISPOSEME(3) class_<Timer, shared_ptr<Timer>, bases<Controller>, boost::noncopyable>("Timer")
		.add_property("time", &Timer::GetTime, &Timer::SetTime)
		.add_property("repeats", &Timer::GetRepeats, &Timer::SetRepeats)
		.add_property("callback", &Timer::GetCallback, &Timer::SetCallback)
		.add_property("condition", &Timer::GetCondition, &Timer::SetCondition)
		.def_readwrite("Finished", &Timer::Finished)
		;

#define add_variable(Type) \
		def("Variable", (shared_ptr<Transition>(Transition::*)(const std::string&, const Type&))&Transition::AddVariable)

#define add_named_variable(Name, Type) \
		def(#Name, (shared_ptr<Transition>(Transition::*)(const Type&))&Transition::Name)

	DISPOSEME(3) class_<Transition, shared_ptr<Transition>, bases<Controller>, boost::noncopyable>("Tween")
		//.def("Vars", py::raw_function(&Transition::Vars))
		.add_named_variable(AddPosition, vec2_t)
		.add_named_variable(AddPosition, vec3_t)
		.add_named_variable(AddRotation, float)
		.add_named_variable(AddRotation, quat_t)
		.add_named_variable(AddScale, vec2_t)
		.add_named_variable(AddScale, vec3_t)
		.add_named_variable(AddColor, color_t)
		.add_named_variable(AddTransparency, float)
		.add_variable(int)
		.add_variable(float)
		.add_variable(point_t)
		.add_variable(rect_t)
		.add_variable(quat_t)
		.add_variable(color_t)
		.add_variable(vec2_t)
		.add_variable(vec3_t)
		.add_variable(box2_t)
		.add_variable(box3_t)
		.add_property("easer", &Transition::GetEaser, &Transition::SetEaser)
		.def_readwrite("flags", &Transition::easeflags)
		.def_readwrite("time", &Transition::m_length)
		.def_readwrite("xdelay", &Transition::xdelay)
		.def_readwrite("loop", &Transition::loop)
		.def_readwrite("Finished", &Transition::TransitionFinished)
	;

	{
		scope e = class_<Animator, shared_ptr<Animator>, bases<Controller>, boost::noncopyable>("Animator")
			.def_readwrite("type", &Animator::type)
			.def_readwrite("speed", &Animator::speed)
			.def_readwrite("amount", &Animator::amount)
			.def_readwrite("time", &Animator::time)
			.def_readwrite("CycleFinished", &Animator::CycleFinished)
		;

		enum_<Animator::Type>("Type")
			.value("Unknown", Animator::ANIMATOR_UNKNOWN)
			.value("Pulse", Animator::ANIMATOR_PULSE)
			.value("ColorPulse", Animator::ANIMATOR_COLORPULSE)
			.value("AlphaPulse", Animator::ANIMATOR_ALPHAPULSE)
			.value("Rotate", Animator::ANIMATOR_ROTATE)
			.value("Shake", Animator::ANIMATOR_SHAKE)
			.export_values()
			;

		DISPOSEME(3) e;
	}

	struct AnimatorProxy
	{
		AnimatorProxy(const std::string& str)
			: value(Animator::ANIMATOR_UNKNOWN)
		{
			if (boost::iequals(str, "Pulse")) value = Animator::ANIMATOR_PULSE; else 
			if (boost::iequals(str, "Rotate")) value = Animator::ANIMATOR_ROTATE; else 
			if (boost::iequals(str, "ColorPulse")) value = Animator::ANIMATOR_COLORPULSE; else
			if (boost::iequals(str, "AlphaPulse")) value = Animator::ANIMATOR_ALPHAPULSE; else
			if (boost::iequals(str, "Shake")) value = Animator::ANIMATOR_SHAKE; else
				std::clog << "Can't convert string '" << str << "' to Animator type, assuming Unknown." << std::endl;
		}
		operator Animator::Type() const { return value; }
		Animator::Type value;
	};

	implicitly_convertible<std::string, AnimatorProxy>();
	implicitly_convertible<AnimatorProxy, Animator::Type>();


#undef trvariable
}