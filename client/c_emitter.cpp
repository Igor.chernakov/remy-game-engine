/**
	PARTICLE SYSTEMS are collections of particles with various FORCES and RENDERERS applied to them
	Roughly similar to (inspired by) PFlow in 3dsmax

	See summary in the bottom
**/

#include "c_client.hpp"
#include "c_entity.hpp"
#include "c_material.hpp"
#include "c_surface.hpp"

#include <numeric>

#define assume(condition) if (!(condition)) return;

namespace std
{
	template<class InIt1, class InIt2, class Fn>
	inline void for_each(InIt1 First1, InIt1 Last1, InIt2 First2, InIt2 Last2, Fn Func)
	{
		for (; First1 != Last1; ++First1)
			for (InIt2 inner = First2; inner != Last2; ++inner)
				Func(*First1, *inner);
	}
}

namespace client
{
	bool ReadBlending(const std::string& value, ILayer::Blending& blend);
	bool SeekForTextureExtension(const std::string& filename, std::string* output);

	namespace pflow
	{
		template <class T>
		class Curve
		{
		public:
			Curve()
			{
			}

			void AddValue(float time, const T& value)
			{
				value_t v = { time, value };
				values.push_back(v);
				values.sort(sorting_t());
			}

			T GetValue(float time) const
			{
				const value_t *t1 = 0, *t2 = 0;

				for (typename values_t::const_iterator it = values.begin(); it != values.end(); ++it)
				{
					if (it->time <= time)
					{
						t1 = &*it;
					}
					else
					{
						t2 = &*it;
						break;
					}
				}
				if (!t1 && !t2)
				{
					return T();
				}
				else if (t1 && t2)
				{
					float k = (time - t1->time) / (t2->time - t1->time);

					return T::Lerp(k, t1->value, t2->value);
				}
				else
				{
					return t1 ? t1->value : t2->value;
				}
			}

		private:
			struct value_t
			{
				float time;
				T value;
			};

			struct sorting_t
			{
				bool operator()(const value_t& t1, const value_t& t2)
				{
					return t2.time > t1.time;
				}
			};

			typedef std::list<value_t> values_t;

			values_t values;
		};

		typedef Curve<color_t> colorcurve_t;

		class ColorCurveValue: public Entity
		{
		public:
			ColorCurveValue()
				: time(0)
				, value(color_t())
			{
			}
			float time;
			color_t value;
		};

		class ColorCurve: public Entity
		{
		public:
			virtual void Attach(shared_ptr<Entity> entity)
			{
				if (dynamic_pointer_cast<ColorCurveValue>(entity))
				{
					shared_ptr<ColorCurveValue> ccv = dynamic_pointer_cast<ColorCurveValue>(entity);
					m_curve.AddValue(ccv->time, ccv->value);
				}
				else
				{
					Entity::Attach(entity);
				}
			}

			const colorcurve_t& GetCurve() const { return m_curve; }

		private:
			colorcurve_t m_curve;
		};

		/**
			Basic PARTICLE class, nothing special to it
		**/

		class Particle
		{
		public:
			Particle()
				: m_LifeTime(1.0)
				, m_Time(0.0f)
				, m_Size(5.0f)
				, m_Angle(0.0f)
				, m_AngularSpeed(0.0f)
				, m_Variation(math::random())
			{
			}
			virtual void SetPosition(const vec3_t& value)
			{
				m_PreviousPosition = m_Position;
				m_Position = value;
			}

			void Update(float timestep, const vec3_t& acceleration)
			{
				m_Time += timestep;
				assume(IsActive());
				m_Speed += acceleration;
				m_Angle += m_AngularSpeed * timestep;
				SetPosition(m_Position + m_Speed * timestep);
			}

			inline bool IsActive() const { return m_Time > 0 && !IsDead(); }
			inline bool IsDead() const { return m_Time >= m_LifeTime; }
			inline void SetPeriod(float start_time, float life_time) { m_Time = -start_time, m_LifeTime = life_time; }
			inline void SetSpeed(const vec3_t& value) { m_Speed = value; }
			inline const vec3_t& GetSpeed() const { return m_Speed; }
			inline void SetSize(float value) { m_Size = value; }
			inline float GetSize() const { return m_Size; }
			inline const vec3_t& GetPreviousPosition() const { return m_PreviousPosition; }
			inline float GetProgress() const { return m_Time / MAX(FLT_MIN, m_LifeTime); }
			inline float GetVariation() const { return m_Variation; }
			inline const vec3_t& GetPosition() const { return m_Position + m_ParentPosition; }
			inline float GetTime() const { return m_Time; }

			//HACK below
			inline float GetAngle() const { return m_Angle; }
			inline float GetAngularSpeed() const { return m_AngularSpeed; }
			inline void SetAngularSpeed(float value) { m_AngularSpeed = value; }

			vec3_t m_ParentPosition;

		private:
			vec3_t m_PreviousPosition, m_Speed;
			vec3_t m_Position, m_Scale;
			color_t m_Color;
			float m_LifeTime, m_Time, m_Size, m_Variation;

			float m_Angle, m_AngularSpeed; //HACK
		};

		/**
			DISPLAY can be either SPRITE or TRAIL
		**/

		class EmitterParameter: public Entity
		{
			box3_t mybox;
			static const int SIZE = 20;

		protected:
			EmitterParameter(
				const std::string& name = "EmitterParameter#",
				const vec3_t& position = vec3_t::Zero,
				const quat_t& rotation = quat_t::Identity,
				const vec3_t& scale = vec3_t::One,
				const color_t& color = color_t())
				: Entity(name, position, rotation, scale, color)
				, mybox(-SIZE, -SIZE, -SIZE, SIZE, SIZE, SIZE)
			{
				IntersectionChecker = (IntersectionCheckerT)&EmitterParameter::CheckIntersection;
				DebugCallback = (DebugCallbackT)&EmitterParameter::OnDebug;
			}

			bool CheckIntersection(const math::Line& line) const
			{
				return line.Intersects(GetBoundingBox(), &m_hotspot);
			}

			void OnDebug(IHelper* helper) const
			{
				helper->Box(GetBoundingBox(), color_t(0, 1, 0), &GetMatrix());
			}

			const box3_t& GetBoundingBox() const
			{
				return mybox;
			}

		};


		class IDisplay: public EmitterParameter
		{
		public:
			IDisplay(const std::string& name): EmitterParameter(name) {}
			virtual void RenderParticle(const Particle& particle, const color_t& color, const colorcurve_t& curve) = 0;
			virtual void BeginRenderParticles(int count) = 0;
			virtual void EndRenderParticles(IClientScene* scene) = 0;
			virtual void UpdateOnParticle(const Particle& particle, float timestep) {};
			virtual shared_ptr<const IMaterial> GetParticleLightMaterial() const = 0;
		};

		/**
			BIRTH volume can be of different geometric shape - BOX, SHPERE or CYLINDER
		**/

		class Birth: public EmitterParameter
		{
		public:
			Birth(const std::string& name)
				: EmitterParameter(name)
				, m_Count(0)
			{}

			virtual vec3_t GenerateParticlePosition() const = 0;

			virtual void SetParent(Entity* parent);

			const vec2_t& GetDuration() const { return m_Duration; }
			void SetDuration(const vec2_t& value) { m_Duration = value; SetParent(GetParent()); }

			const vec2_t& GetLifespan() const { return m_Lifespan; }
			void SetLifespan(const vec2_t& value) { m_Lifespan = value; SetParent(GetParent()); }

			const vec2_t& GetSize() const { return m_Size; }
			void SetSize(const vec2_t& value) { m_Size = value; SetParent(GetParent()); }

			const vec2_t& GetSpeed() const { return m_Speed; }
			void SetSpeed(const vec2_t& value) { m_Speed = value; SetParent(GetParent()); }

			int GetCount() const { return m_Count; }
			void SetCount(int value) { m_Count = value; SetParent(GetParent()); }

		private:
			vec2_t m_Duration, m_Lifespan, m_Size, m_Speed;
			int m_Count;
		};

		class Box: public Birth
		{
		public:
			Box()
				: Birth("EmitterBox#")
				, m_Size(100, 100, 100)
			{}

			virtual vec3_t GenerateParticlePosition() const
			{
				mat4_t m(GetRotation());

				m.SetTranslation(GetPosition());

				return vec3_t(
					math::signed_random() * m_Size.x / 2,
					math::signed_random() * m_Size.y / 2,
					math::signed_random() * m_Size.z / 2) * m;
			}

			const vec3_t& GetSize() const { return m_Size; }
			void SetSize(const vec3_t& value) { m_Size = value; }

		private:
			vec3_t m_Size;
		};

		class Sphere: public Birth
		{
		public:
			Sphere()
				: Birth("EmitterSphere#")
				, m_Radius(0, 100)
			{
			}

			virtual vec3_t GenerateParticlePosition() const
			{
				return
					vec3_t::Normalize(vec3_t(
						math::signed_random(),
						math::signed_random(),
						math::signed_random())) * (m_Radius.x + math::random() * (m_Radius.y - m_Radius.x)) + GetPosition();
			}

			const vec2_t& GetRadius() const
			{
				return m_Radius;
			}

			void SetRadius(const vec2_t& value)
			{
				m_Radius = value;
			}

		private:
			vec2_t m_Radius;
		};

		class Cylinder: public Birth
		{
		public:
			Cylinder()
				: Birth("EmitterCylinder#")
				, m_Radius(0, 100)
				, m_Height(100)
			{
			}

			virtual vec3_t GenerateParticlePosition() const
			{
				return
					vec3_t::Normalize(vec3_t(math::signed_random(), math::signed_random(), 0)) *
					(m_Radius.x + math::random() * (m_Radius.y - m_Radius.x)) + GetPosition() +
					vec3_t(0, 0, math::signed_random() * m_Height);
			}

			const vec2_t& GetRadius() const { return m_Radius; }
			void SetRadius(const vec2_t& value) { m_Radius = value; }
			float GetHeight() const { return m_Height; }
			void SetHeight(float value) { m_Height = value; }

		private:
			vec2_t m_Radius;
			float m_Height;
		};

		class Sprite: public IDisplay
		{
		public:
			Sprite()
				: IDisplay("EmitterSprite#")
				, m_VertexIterator(0)
			{
			}

			virtual shared_ptr<const IMaterial> GetParticleLightMaterial() const
			{
				return m_ParticleMaterial;
			}

			virtual void BeginRenderParticles(int count)
			{
				assume(count > 0);

				m_VertexIterator = m_VertexBuffer = new vertex_t[count * 4];
				m_IndexIterator = m_IndexBuffer = new index_t[count * 6];

				m_RightVector = vec3_t::Normalize(vec3_t::Cross(
					framework->GetScene()->GetCamera()->GetDirection(),
					framework->GetScene()->GetCamera()->GetYaw()));

				m_UpVector = vec3_t::Normalize(vec3_t::Cross(
					framework->GetScene()->GetCamera()->GetDirection(),
					m_RightVector));

				//m_RightVector *= mat4_t::Invert(mat4_t(GetMatrix().GetFront(), GetMatrix().GetRight(), GetMatrix().GetUp()));
				//m_UpVector *= mat4_t::Invert(mat4_t(GetMatrix().GetFront(), GetMatrix().GetRight(), GetMatrix().GetUp()));
			}

			virtual void EndRenderParticles(IClientScene* scene)
			{
				assume(m_VertexIterator);

				Surface particles;

				particles.pVertices = m_VertexBuffer;
				particles.nVertices = m_VertexIterator - m_VertexBuffer;
				particles.pIndices = m_IndexBuffer;
				particles.nIndices = m_IndexIterator - m_IndexBuffer;
				particles.pMaterial = m_ParticleMaterial;
				//particles.pMatrix = &GetMatrix();
				particles.flags |= FLAG_NOSORT;

				scene->PushSurface(&particles);

				delete[] m_VertexBuffer;
				delete[] m_IndexBuffer;

				m_VertexBuffer = m_VertexIterator = 0;
				m_IndexBuffer = m_IndexIterator = 0;
			}

			virtual void RenderParticle(const Particle& particle, const color_t& color, const colorcurve_t& curve)
			{
				assume(particle.IsActive() && m_VertexIterator && m_IndexIterator);

				const float angle = deg2rad(particle.GetAngle());//deg2rad(particle.GetRotation().GetRoll());

				m_IndexIterator = FillParticleIndices(m_IndexIterator, m_VertexIterator - m_VertexBuffer);
				m_VertexIterator = FillParticleVertices(m_VertexIterator, particle, particle.GetPosition(),
					(m_RightVector * cos(angle) + m_UpVector * sin(angle)) * particle.GetSize(),
					(m_UpVector * cos(angle) - m_RightVector * sin(angle)) * particle.GetSize(), color, curve);
			}

			static index_t* FillParticleIndices(
				index_t* ptr,
				int start)
			{
				*(ptr++) = start + 0;
				*(ptr++) = start + 1;
				*(ptr++) = start + 2;
				*(ptr++) = start + 1;
				*(ptr++) = start + 2;
				*(ptr++) = start + 3;
				return ptr;
			}

			static vertex_t* FillParticleVertices(
				vertex_t* ptr,
				const Particle& particle,
				const vec3_t& position,
				const vec3_t& right,
				const vec3_t& up,
				const color_t& color,
				const colorcurve_t& curve)
			{
				color_t _color = color * curve.GetValue(particle.GetProgress());

				ptr->uv = vec2_t(0, 0);
				ptr->position = position - up - right;
				ptr->color = _color;
				++ptr;

				ptr->uv = vec2_t(1, 0);
				ptr->position = position - up + right;
				ptr->color = _color;
				++ptr;

				ptr->uv = vec2_t(0, 1);
				ptr->position = position + up - right;
				ptr->color = _color;
				++ptr;

				ptr->uv = vec2_t(1, 1);
				ptr->position = position + up + right;
				ptr->color = _color;
				++ptr;

				return ptr;
			}

			shared_ptr<const IMaterial> m_ParticleMaterial;

		protected:
			vertex_t *m_VertexBuffer, *m_VertexIterator;
			index_t *m_IndexBuffer, *m_IndexIterator;
			vec3_t m_RightVector, m_UpVector;
		};

		class Trail: public Sprite
		{
			static const int ITERATIONS = 8;

		public:
			Trail()
				: Sprite()
				, m_Duration(1.0f)
				, m_IndexIterator(0)
			{
				SetName("EmitterTrail#");
			}

			virtual shared_ptr<const IMaterial> GetParticleLightMaterial() const
			{
				return shared_ptr<const IMaterial>();
			}

			void UpdateOnParticle(const Particle& particle, float timestep)
			{
				std::list<std::pair<float, vec3_t> >* trail = &m_Trails[&particle];

				if (trail->size() < 2)
				{
					trail->push_front(std::pair<float, vec3_t>(0, particle.GetPosition()));
					trail->push_front(std::pair<float, vec3_t>(0, particle.GetPosition()));
				}
				else
				{
					trail->front().first += timestep / (m_Duration * particle.GetSize() / 10.f);
					trail->front().second = particle.GetPosition();
					if (trail->front().first > 1.0f / (ITERATIONS - 1))
					{
						trail->push_front(std::pair<float, vec3_t>(0, particle.GetPosition()));
						if (trail->size() > ITERATIONS + 1)
						{
							trail->pop_back();
						}
					}
				}
			}

			virtual void BeginRenderParticles(int count)
			{
				assume(count > 0);

				m_VertexIterator = m_VertexBuffer = new vertex_t[count * ((ITERATIONS + 1) * 4)];
				m_IndexIterator = m_IndexBuffer = new index_t[count * ITERATIONS * 12];
			}

			virtual void EndRenderParticles(IClientScene* scene)
			{
				assume(m_VertexIterator);

				Surface particles;

				particles.pVertices = m_VertexBuffer;
				particles.nVertices = m_VertexIterator - m_VertexBuffer;
				particles.pIndices = m_IndexBuffer;
				particles.nIndices = m_IndexIterator - m_IndexBuffer;
				particles.pMaterial = m_ParticleMaterial;
				//particles.pMatrix = &GetMatrix();
				particles.flags |= FLAG_NOSORT;

				scene->PushSurface(&particles);

				delete[] m_VertexBuffer;
				delete[] m_IndexBuffer;

				m_VertexBuffer = m_VertexIterator = 0;
			}

			virtual void RenderParticle(const Particle& particle, const color_t& color, const colorcurve_t& curve)
			{
				assume(particle.IsActive() && m_VertexIterator);

				class create_indices
				{
				public:
					create_indices(index_t** indices, int start)
						: m_Indices(indices)
						, m_Index(start)
					{
					}

					inline void operator()(const std::pair<float, vec3_t>& e)
					{
						for (int count = m_Index + 4; m_Index < count;
							*(*m_Indices)++ = m_Index,
							*(*m_Indices)++ = m_Index + (m_Index % 2 ? 3 : 1),
							*(*m_Indices)++ = m_Index++ + 4)
						{
						}
					}

				private:
					index_t** m_Indices;
					int m_Index;
				};

				class create_vertices
				{
				public:
					create_vertices(vertex_t** vertices, const Particle& particle, const color_t& color, float duration)
						: m_Duration(duration)
						, m_Particle(&particle)
						, m_Vertices(vertices)
						, m_Color(color)
					{
					}

					inline void operator()(const std::pair<float, vec3_t>& e)
					{
						const vec3_t
							direction = m_Duration < 1.0f ?  m_Base - e.second : m_Particle->GetSpeed(),
							right = vec3_t::Normalize(vec3_t::Cross(direction,vec3_t(0,0,1))) * m_Particle->GetSize(),
							up = vec3_t::Normalize(vec3_t::Cross(direction,right)) * m_Particle->GetSize();

						m_Base = m_Duration < math::EPS ? vec3_t::Lerp(-m_Duration, e.second, m_Base) : e.second;
						m_Duration = m_Duration < math::EPS ? 0 : (m_Duration -= e.first) < 0 ? (m_Duration / e.first) : m_Duration;

						MakeVertex(right, false);
						MakeVertex(right, true);
						MakeVertex(up, false);
						MakeVertex(up, true);
					}

				private:
					inline void MakeVertex(const vec3_t& offset, bool left)
					{
						(*m_Vertices)->uv = vec2_t(1.0f - 0.5f * m_Duration, left ? 0.0f : 1.0f);
						//(*m_Vertices)->tc[1] = vec2_t(m_Particle->GetProgress() * 0.5f, m_Particle->GetVariation());
						(*m_Vertices)->position = m_Base + (left ? -offset : offset);
						(*m_Vertices)->color = m_Color;
						++(*m_Vertices);
					}

				private:
					float m_Duration;
					vec3_t m_Base;
					vertex_t** m_Vertices;
					const Particle* m_Particle;
					color_t m_Color;
				};

				std::for_each(++m_Trails[&particle].begin(), m_Trails[&particle].end(),
					create_indices(&m_IndexIterator, m_VertexIterator - m_VertexBuffer));

				std::for_each(m_Trails[&particle].begin(), m_Trails[&particle].end(),
					create_vertices(&m_VertexIterator, particle, color, m_Duration));
			}

			inline void SetDuration(float value)
			{
				m_Duration = value;
			}

			inline float GetDuration() const
			{
				return m_Duration;
			}


		private:
			float m_Duration;

			index_t *m_IndexBuffer, *m_IndexIterator;

			std::map<const Particle*, std::list<std::pair<float, vec3_t> > > m_Trails;
		};

		/**
			OBSTACLES are not only used to block particles from penetraing but also can
			be rendered projections upon them
		**/

		class IObstacle: public EmitterParameter
		{
		public:
			IObstacle(const std::string& name)
				: EmitterParameter(name)
				, m_RenderLight(true)
			{
			}

			inline bool IsRenderLight() const { return m_RenderLight; }
			inline void SetRenderLight(bool value) { m_RenderLight = value; }

			virtual void RenderParticle(const Particle& particle, vertex_t** vv, const color_t& color, const colorcurve_t& curve) const = 0;
			virtual void CheckCollision(Particle& particle) const = 0;

		private:
			bool m_RenderLight;
		};

		class Plane: public IObstacle
		{
		public:
			Plane()
				: IObstacle("EmitterPlane#")
				, m_Normal(vec3_t(0, 0, -1))
			{
				AnimateCallback = (AnimateCallbackT)&Plane::OnAnimate;
			}

			void OnAnimate(float timestep)
			{
				m_Right = vec3_t::Normalize(vec3_t::Cross(m_Normal, framework->GetScene()->GetCamera()->GetDirection()));
				m_Up = vec3_t::Normalize(vec3_t::Cross(m_Normal, m_Right));

				m_Right *= mat4_t::Invert(mat4_t(GetMatrix().GetFront(), GetMatrix().GetRight(), GetMatrix().GetUp()));
				m_Up *= mat4_t::Invert(mat4_t(GetMatrix().GetFront(), GetMatrix().GetRight(), GetMatrix().GetUp()));
			}

			inline float GetDistance(const Particle& p) const
			{
				return m_Normal.Dot(GetPosition() - p.GetPosition());
			}

			inline vec3_t GetProjection(const Particle& p, bool simple = false) const
			{
				if (simple)
				{
					return p.GetPosition() + m_Normal * GetDistance(p);
				}
				else
				{
					const float r1 = m_Normal.Dot(GetPosition() - p.GetPosition());
					const float r2 = m_Normal.Dot(GetPosition() - p.GetPreviousPosition());
					return math::sign(r1) == math::sign(r2) ?
						p.GetPosition() + m_Normal * GetDistance(p) :
						p.GetPosition() + (p.GetPosition() - p.GetPreviousPosition()) * (r1 / (r2 - r1));
				}
			}

			virtual void RenderParticle(const Particle& particle, vertex_t** vv, const color_t& color, const colorcurve_t& curve) const
			{
				assume(IsRenderLight() && particle.IsActive());

				const float distance = GetDistance(particle);
				const float size = 7 * particle.GetSize() * (MIN(1, MAX(0, distance / 100)) + 0.25f);
				const color_t c(1, 1, 1, 0.15f * (1 - MIN(1, MAX(0, distance / 100))));

				*vv = Sprite::FillParticleVertices(*vv, particle, GetProjection(particle, true), m_Right * size, m_Up * size, c * color, curve);
			}

			virtual void CheckCollision(Particle& particle) const
			{
				assume(GetDistance(particle) <= 0);
				particle.SetAngularSpeed(particle.GetAngularSpeed() * 0.5f);
				particle.SetSpeed((particle.GetSpeed() - m_Normal * (vec3_t::Dot(m_Normal, particle.GetSpeed()) * 2)) * 0.5f);
				particle.SetPosition(GetProjection(particle) +
					vec3_t::Normalize(particle.GetSpeed()) * (GetProjection(particle) - particle.GetPosition()).Length());
			}

			inline const vec3_t& GetNormal() const
			{
				return m_Normal;
			}

			inline void SetNormal(const vec3_t& value)
			{
				m_Normal = value;
			}

		private:
			vec3_t m_Normal, m_Right, m_Up;
		};

		/**
			FORCE interface (provides acceleration to particles)
		**/

		class IForce: public EmitterParameter
		{
		public:
			IForce(const std::string& name)
				: EmitterParameter(name)
				, m_CurrentTime(0)
				, m_StartTime(0)
				, m_LifeTime(-1)
			{
				AnimateCallback = (AnimateCallbackT)&IForce::OnAnimate;
			}

			virtual void ResetForce() {
				m_CurrentTime = 0;
			}

			virtual vec3_t ComputeAcceleration(float timestep, const Particle& particle) const = 0;

			void OnAnimate(float timestep)
			{
				m_CurrentTime += timestep;
			}

			inline bool IsActive() const
			{
				return m_CurrentTime > m_StartTime && (m_LifeTime < 0 || m_CurrentTime < m_StartTime + m_LifeTime);
			}

			inline vec2_t GetDuration() const
			{
				return vec2_t(-m_StartTime, m_LifeTime - m_StartTime);
			}

			inline void SetDuration(const vec2_t& duration)
			{
				SetStartTime(duration.x);
				SetLifeTime(duration.y - duration.x);
			}

			inline float GetStartTime() const
			{
				return -m_StartTime;
			}

			inline void SetStartTime(float value)
			{
				m_StartTime = -value;
			}

			inline float GetLifeTime() const
			{
				return m_LifeTime;
			}

			inline void SetLifeTime(float value)
			{
				m_LifeTime = value;
			}

		private:
			float m_StartTime, m_LifeTime, m_CurrentTime;
		};

		class Wind: public IForce
		{
		public:
			Wind(): IForce("EmitterWind#") {}
			vec3_t ComputeAcceleration(float timestep, const Particle& particle) const
			{
				return IsActive() ? m_Force * timestep : vec3_t::Zero;
			}

			const vec3_t& GetForce() const { return m_Force; }
			void SetForce(const vec3_t& value) { m_Force = value; }

		private:
			vec3_t m_Force;
		};

		class Gravity: public IForce
		{
		public:
			Gravity()
				: IForce("EmitterGravity#")
				, m_Force(100)
			{
			}

			vec3_t ComputeAcceleration(float timestep, const Particle& particle) const
			{
				return IsActive() ?
					vec3_t::Normalize(GetMatrix().GetTranslation() - particle.GetPosition()) * m_Force * timestep : vec3_t::Zero;
			}

			float GetForce() const { return m_Force; }
			void SetForce(float value) { m_Force = value; }

		private:
			float m_Force;
		};

		class Motor: public IForce
		{
		public:
			Motor()
				: IForce("EmitterMotor#")
				, m_Force(100)
				, m_Radius(-1)
			{
			}

			vec3_t ComputeAcceleration(float timestep, const Particle& particle) const
			{
				float k = 1;
				if (m_Radius > 0) {
					k = 1 - (GetMatrix().GetTranslation() - particle.GetPosition()).Length() / m_Radius;
					if (k > 1 || k < 0)
						return vec3_t::Zero;
					k = sqrt(k);
				}
				return IsActive() ?
					vec3_t::Normalize(vec3_t::Cross(vec3_t(0,0,1), GetMatrix().GetTranslation() - particle.GetPosition()))
						* m_Force * timestep * k : vec3_t::Zero;
			}

			float GetForce() const { return m_Force; }
			void SetForce(float value) { m_Force = value; }

			float GetRadius() const { return m_Radius; }
			void SetRadius(float value) { m_Radius = value; }

		private:
			float m_Force, m_Radius;
		};

		class Explosion: public IForce
		{
		public:
			Explosion()
				: IForce("EmitterExplosion#")
				, m_Force(1000)
				, m_Exploded(false)
				, m_Exploded2(false)
			{
				AnimateCallback = (AnimateCallbackT)&Explosion::OnAnimate;
			}

			void OnAnimate(float timestep)
			{
				IForce::OnAnimate(timestep);

				m_Exploded2 = m_Exploded;
				m_Exploded = m_Exploded || IsActive();
			}

			void ResetForce() {
				m_Exploded = m_Exploded2 = false;
				IForce::ResetForce();
			}

			vec3_t ComputeAcceleration(float timestep, const Particle& particle) const
			{
				if (!m_Exploded2) {
					const float force = m_Force * sqrt(math::random());
					const_cast<Particle&>(particle).SetAngularSpeed(force * ((rand() % 2) ? -1 : 1));
					return IsActive() ? (vec3_t::Normalize(particle.GetPosition() - GetMatrix().GetTranslation()) * force) : vec3_t::Zero;
				}
				return vec3_t();
			}

			float GetForce() const { return m_Force; }
			void SetForce(float value) { m_Force = value; }

		private:
			float m_Force;
			bool m_Exploded, m_Exploded2;
		};

		/**
			EMITTER gets together PARTICLES, DISPLAYS, FORCES and OBSTACLES
		**/

		class Emitter: public EmitterParameter
		{
			friend class Birth;

		public:
			bool m_dispose_when_finished;

			Emitter()
				: EmitterParameter("Emitter#")
				, m_dispose_when_finished(true)
			{
				RenderCallback = (RenderCallbackT)&Emitter::OnRender;
				AnimateCallback = (AnimateCallbackT)&Emitter::OnAnimate;
			}

			void OnAnimate(float timestep)
			{
				const std::list<shared_ptr<IForce> > forces(Gather<IForce>());
				const std::list<shared_ptr<IObstacle> > obstacles(Gather<IObstacle>());

				shared_ptr<const Entity> emitter = GetEmitter();

				vec3_t position = emitter ? emitter->GetMatrix().GetTranslation() : GetMatrix().GetTranslation();

				BOOST_FOREACH(Particle& particle, m_Particles)
				{
					if (!emitter || !particle.IsActive()) {
						particle.m_ParentPosition = position;
					}
					particle.Update(timestep, std::accumulate(
						boost::make_transform_iterator(forces.begin(),
							boost::bind(&IForce::ComputeAcceleration, _1, timestep, boost::ref(particle))),
						boost::make_transform_iterator(forces.end(),
							boost::bind(&IForce::ComputeAcceleration, _1, timestep, boost::ref(particle))),
						vec3_t::Zero));
				}

				std::for_each(obstacles.begin(), obstacles.end(), m_Particles.begin(), m_Particles.end(),
					boost::bind(&IObstacle::CheckCollision, _1, _2));

				std::for_each(m_Particles.begin(), m_Particles.end(),
					boost::bind(&IDisplay::UpdateOnParticle, m_Display, _1, timestep));

				 if (m_dispose_when_finished && std::count_if(m_Particles.begin(), m_Particles.end(), std::mem_fun_ref(&Particle::IsDead)) == m_Particles.size())
				 {
					 SetFlags(FLAG_DISPOSED, true);
				 }
			}

			void OnRender(IClientScene* scene) const
			{
				assume(m_Display);

				const color_t color = GetColor();

				colorcurve_t default_curve;
				const colorcurve_t *curve = m_ColorCurve ? &m_ColorCurve->GetCurve() : &default_curve;

				m_Display->BeginRenderParticles(std::count_if(m_Particles.begin(), m_Particles.end(),
					std::mem_fun_ref(&Particle::IsActive)));
				std::for_each(m_Particles.begin(), m_Particles.end(),
					boost::bind(&IDisplay::RenderParticle, m_Display, _1, color, *curve));
				m_Display->EndRenderParticles(scene);

				if (m_Display->GetParticleLightMaterial())
				{
					const std::list<shared_ptr<IObstacle> > obstacles(Gather<IObstacle>());

					std::vector<vertex_t> buffer(4 * obstacles.size() *
						std::count_if(m_Particles.begin(), m_Particles.end(), std::mem_fun_ref(&Particle::IsActive)));

					if (buffer.size() > 0)
					{
						vertex_t* pv;

						//gather vertices

						std::for_each(obstacles.begin(), obstacles.end(), m_Particles.begin(), m_Particles.end(),
							boost::bind(&IObstacle::RenderParticle, _1, _2, &(pv = &*buffer.begin()), color, *curve));

						//build indices

						std::vector<index_t> indices(6 * buffer.size() / 4);

						index_t* pi = &*indices.begin();

						for (int i = 0; i < indices.size(); i += 6)
						{
							pi = Sprite::FillParticleIndices(pi, i * 4 / 6);
						}

						//render surface

						Surface lights;

						lights.pVertices = &*buffer.begin();
						lights.nVertices = pv - &*buffer.begin();
						lights.pIndices = &*indices.begin();
						lights.nIndices = pi - &*indices.begin();
						lights.pMaterial = m_Display->GetParticleLightMaterial();
						//lights.pMatrix = &GetMatrix();
						lights.flags |= FLAG_NOSORT;

						scene->PushSurface(&lights);
					}
				}
			}


			virtual void Attach(shared_ptr<Entity> entity)
			{
				m_Display = dynamic_pointer_cast<IDisplay>(entity) ? dynamic_pointer_cast<IDisplay>(entity) : m_Display;
				m_ColorCurve = dynamic_pointer_cast<ColorCurve>(entity) ? dynamic_pointer_cast<ColorCurve>(entity) : m_ColorCurve;
				Entity::Attach(entity);
			}

			void ResetEmitter()
			{
				const std::list<shared_ptr<IForce> > forces(Gather<IForce>());
				std::for_each(forces.begin(), forces.end(), boost::bind(&IForce::ResetForce, _1));
				m_Particles = m_ParticlesBackup;
			}

			void SetEmitter(shared_ptr<const Entity> emitter) {
				m_emitter = emitter;
			}

			shared_ptr<const Entity>  GetEmitter() const {
				return m_emitter;
			}

		private:
			shared_ptr<const Entity> m_emitter;

			template <typename T>
			std::list<shared_ptr<T> > Gather() const
			{
				std::list<shared_ptr<T> > objects;
				BOOST_FOREACH(shared_ptr<Entity> linked, GetLinked()) objects.push_back(dynamic_pointer_cast<T>(linked));
				objects.erase(std::remove_if(objects.begin(), objects.end(), boost::bind(&shared_ptr<T>::operator!, _1)), objects.end());
				return objects;
			}

		private:
			std::vector<Particle> m_Particles, m_ParticlesBackup;
			shared_ptr<IDisplay> m_Display;
			shared_ptr<ColorCurve> m_ColorCurve;
		};

		void Birth::SetParent(Entity* parent)
		{
			if (Emitter* emitter = dynamic_cast<Emitter*>(parent))
			{
				emitter->m_Particles.reserve(m_Count);
				for (int i = 0; i < m_Count; ++i)
				{
					Particle particle;
					particle.SetPosition(GenerateParticlePosition());
					particle.SetPeriod(
						m_Duration.x + math::random() * (m_Duration.y - m_Duration.x),
						m_Lifespan.x + math::signed_random() * m_Lifespan.y / 2);
					particle.SetSize(m_Size.x + math::signed_random() * m_Size.y / 2);
					particle.SetSpeed(vec3_t::Normalize(particle.GetPosition()) *
						(m_Speed.x + math::signed_random() * m_Speed.y / 2));
					emitter->m_Particles.push_back(particle);
					emitter->m_ParticlesBackup.push_back(particle);
				}
			}
			Entity::SetParent(parent);
		}
	}
}

void export_Emitter()
{
	using namespace boost::python;
	using namespace client::pflow;

	DISPOSEME(3) class_<Emitter, shared_ptr<Emitter>, bases<client::Entity>, boost::noncopyable>("Emitter")
		.def("ResetEmitter", &Emitter::ResetEmitter)
		.def_readwrite("dispose_when_finished", &Emitter::m_dispose_when_finished)
		.add_property("owner", &Emitter::GetEmitter, &Emitter::SetEmitter)
		;

	DISPOSEME(3) class_<Sprite, shared_ptr<Sprite>, bases<client::Entity>, boost::noncopyable>("Sprite")
		.def_readwrite("material", &Sprite::m_ParticleMaterial)
		;

	DISPOSEME(3) class_<Trail, shared_ptr<Trail>, bases<Sprite>, boost::noncopyable>("Trail")
		.add_property("length", &Trail::GetDuration, &Trail::SetDuration)
		;

	DISPOSEME(3) class_<Birth, shared_ptr<Birth>, bases<client::Entity>, boost::noncopyable>("Birth", no_init)
		.add_property("duration", py_cref(&Birth::GetDuration), &Birth::SetDuration)
		.add_property("lifespan", py_cref(&Birth::GetLifespan), &Birth::SetLifespan)
		.add_property("size", py_cref(&Birth::GetSize), &Birth::SetSize)
		.add_property("speed", py_cref(&Birth::GetSpeed), &Birth::SetSpeed)
		.add_property("count", &Birth::GetCount, &Birth::SetCount)
		;

	DISPOSEME(3) class_<Box, shared_ptr<Box>, bases<Birth>, boost::noncopyable>("BirthBox")
		.add_property("size", py_cref(&Box::GetSize), &Box::SetSize)
		;

	DISPOSEME(3) class_<Sphere, shared_ptr<Sphere>, bases<Birth>, boost::noncopyable>("BirthSphere")
		.add_property("radius", py_cref(&Sphere::GetRadius), &Sphere::SetRadius)
		;

	DISPOSEME(3) class_<Cylinder, shared_ptr<Cylinder>, bases<Birth>, boost::noncopyable>("BirthCylinder")
		.add_property("radius", py_cref(&Cylinder::GetRadius), &Cylinder::SetRadius)
		.add_property("height", &Cylinder::GetHeight, &Cylinder::SetHeight)
		;

	DISPOSEME(3) class_<Wind, shared_ptr<Wind>, bases<client::Entity>, boost::noncopyable>("Wind")
		.add_property("duration", &Wind::GetDuration, &Wind::SetDuration)
		.add_property("force", py_cref(&Wind::GetForce), &Wind::SetForce)
		;

	DISPOSEME(3) class_<Gravity, shared_ptr<Gravity>, bases<client::Entity>, boost::noncopyable>("Gravity")
		.add_property("duration", &Gravity::GetDuration, &Gravity::SetDuration)
		.add_property("force", &Gravity::GetForce, &Gravity::SetForce)
		;

	DISPOSEME(3) class_<Motor, shared_ptr<Motor>, bases<client::Entity>, boost::noncopyable>("Motor")
		.add_property("duration", &Motor::GetDuration, &Motor::SetDuration)
		.add_property("force", &Motor::GetForce, &Motor::SetForce)
		.add_property("radius", &Motor::GetRadius, &Motor::SetRadius)
		;

	DISPOSEME(3) class_<Explosion, shared_ptr<Explosion>, bases<client::Entity>, boost::noncopyable>("Explosion")
		.add_property("force", &Explosion::GetForce, &Explosion::SetForce)
		.add_property("time", &Explosion::GetStartTime, &Explosion::SetStartTime)
		;

	DISPOSEME(3) class_<Plane, shared_ptr<Plane>, bases<client::Entity>, boost::noncopyable>("Plane")
		.add_property("normal", py_cref(&Plane::GetNormal), &Plane::SetNormal)
		.add_property("render_light", &Plane::IsRenderLight, &Plane::SetRenderLight)
		;

	DISPOSEME(3) class_<ColorCurve, shared_ptr<ColorCurve>, bases<client::Entity>, boost::noncopyable>("ColorCurve")
		;

	DISPOSEME(3) class_<ColorCurveValue, shared_ptr<ColorCurveValue>, bases<client::Entity>, boost::noncopyable>("ColorCurveValue")
		.def_readwrite("time", &ColorCurveValue::time)
		.def_readwrite("color", &ColorCurveValue::value)
		;
}
