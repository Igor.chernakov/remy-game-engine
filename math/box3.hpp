#pragma once

#include "vector.hpp"
#include "vector3.hpp"
#include "macros.hpp"

namespace math
{
	struct Box3
	{
		static const int Dimensions = 6;

		Vector3 vMin, vMax;

		static const Box3 Identity;

		Box3() {  }
		Box3(const Vector3 &vMin, const Vector3 &vMax) : vMin(vMin), vMax(vMax) {};
		Box3(const Real *v) : vMin(v[0], v[1], v[2]), vMax(v[3], v[4], v[5]) {};
		Box3(Real v1, Real v2, Real v3, Real v4, Real v5, Real v6) : vMin(v1, v2, v3), vMax(v4, v5, v6) {};
		Box3(const std::string&);
		Box3(const Box3& other) { vMin = other.vMin; vMax = other.vMax; }

		DEFINE_DEFAULT_PAIR_OPS(Box3, Vector3);

		inline static Box3 Lerp(Real d, const Box3& left, const Box3& right)
		{
			return Box3(Vector3::Lerp(d, left.vMin, right.vMin), Vector3::Lerp(d, left.vMax, right.vMax));
		}

		void Reset()
		{
			vMin.Maximize();
			vMax.Minimize();
		}

		Real Width() const
		{
			return vMax.x - vMin.x;
		}

		Real Length() const
		{
			return vMax.y - vMin.y;
		}

		Real Height() const
		{
			return vMax.z - vMin.z;
		}
	};
};
