#pragma once

#include "iobject.hpp"
#include "ilayer.hpp"
#include "ishader.hpp"

#include <boost/shared_ptr.hpp>
#include <string>

struct IMaterial: IObject
{
	enum StencilOp
	{
		OP_UNKNOWN = 0,
		OP_KEEP = 1,
		OP_ZERO = 2,
		OP_REPLACE = 3,
		OP_INCRSAT = 4,
		OP_DECRSAT = 5,
		OP_INVERT = 6,
		OP_INCR = 7,
		OP_DECR = 8,
	};

	enum Func
	{
		FUNC_UNKNOWN = 0,
		FUNC_NEVER = 1,
		FUNC_LESS = 2,
		FUNC_EQUAL = 3,
		FUNC_LESSEQUAL = 4,
		FUNC_GREATER = 5,
		FUNC_NOTEQUAL = 6,
		FUNC_GREATEREQUAL = 7,
		FUNC_ALWAYS = 8,
	};

	enum CullFace
	{
		CULL_UNKNOWN = 0,
		CULL_NONE = 1,
		CULL_FRONT = 2,
		CULL_BACK = 3,
	} cullface;

	enum SurfaceFlags
	{
		SF_NO_SHADOW = 1 << 0,
		SF_NO_SOLID = 1 << 1,
		SF_NO_BBOX = 1 << 2,
		SF_NO_MULTITEXTURE = 1 << 3,
		SF_NO_SOLID_BUT_ALPHA = 1 << 4,
		SF_NO_LIGHT = 1 << 5,
		SF_NO_ALIASING = 1 << 6,
	};

	enum Pass
	{
		RENDER_UNKNOWN,
		RENDER_SOLID,
		RENDER_ALPHA,
		RENDER_PLANARSHADOWS,
		RENDER_VOLUMESHADOWS,
		RENDER_SHADOWMAP,
		RENDER_TEXTURE,
		RENDER_INNER,
		RENDER_OUTER,
		RENDER_INTERFACE,
		RENDER_INTERFACE2,
		RENDER_OVERINTERFACE,
		RENDER_COUNT,
	} renderpass;

	struct AlphaFunction 
	{
		Func func;
		unsigned char ref;
	} alphafunc;

	struct StencilSide {
		struct StencilFunction {
			Func func;
			unsigned char ref;
			StencilOp pass;
			StencilOp fail;
		} front, back;
	} stencil;

	struct DepthFunction
	{
		Func func;
		bool mask;
	} depthfunc;

	int surfaceflags;
	bool colormask;
	
	std::string m_name;

	IMaterial()
	{
		cullface = CULL_BACK;
		surfaceflags = 0;
		stencil.front.func = FUNC_UNKNOWN;
		stencil.front.ref = 0;
		stencil.front.pass = OP_KEEP;
		stencil.front.fail = OP_KEEP;
		stencil.back.func = FUNC_UNKNOWN;
		stencil.back.ref = 0;
		stencil.back.pass = OP_KEEP;
		stencil.back.fail = OP_KEEP;
		alphafunc.func = FUNC_UNKNOWN;
		alphafunc.ref = 0;
		depthfunc.func = FUNC_LESSEQUAL;
		depthfunc.mask = true;
		colormask = true;
		renderpass = RENDER_SOLID;
	}

	virtual const IShader* GetShader() const = 0;
	virtual const ILayer* GetLayer(int i) const = 0;
	virtual int GetLayersCount() const = 0;
	virtual int GetWidth() const = 0;
	virtual int GetHeight() const = 0;
};
