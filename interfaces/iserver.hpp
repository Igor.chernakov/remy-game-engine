#pragma once

#include "iobject.hpp"

#include <string>

struct IServer: IObject
{
	virtual void Run() = 0;
	virtual int GetPort() const = 0;
};