#include "Plugin.h"

#include "ExportMesh.h"

#include <stdarg.h>

#include <iostream>
#include <fstream>
#include <algorithm>


static const float EPS = 0.001f;

bool bExportRotationSequence;
bool bExportPositionSequence;
bool bExportScaleSequence;
bool bOptimizeSequences;

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

class AnimationExport :
	public SceneExport,
	public XmlMeshExport
{
public:
	AnimationExport(void)
	{
	}

	int ExtCount(void)
	{ // Number of extensions supported
		return(1);
	}

	const TCHAR* Ext(int n)
	{ // Extension #n(i.e. "3DS")
		return("MOTION");
	}

	const TCHAR* LongDesc(void)
	{ // Long ASCII description(i.e. "Autodesk 3D Studio File")
		return("GameEngine Skeletal Animation File");
	}

	const TCHAR* ShortDesc(void)
	{ // Short ASCII description(i.e. "3D Studio")
		return("GameEngine Skeletal Animation");
	}

	const TCHAR* AuthorName(void)
	{ // ASCII Author name
		return("Igor Chernakov");
	}

	const TCHAR* CopyrightMessage(void)
	{ // ASCII Copyright message
		return("");
	}

	const TCHAR* OtherMessage1(void)
	{ // Other message #1
		return("");
	}

	const TCHAR* OtherMessage2(void)
	{ // Other message #2
		return("");
	}

	unsigned int Version(void)
	{ // Version number * 100(i.e. v3.01 = 301)
		return(100);
	}

	void ShowAbout(HWND hWnd)
	{ // Show DLL's "About..." box
	}

	int DoExport(const TCHAR* name, ExpInterface* ei, Interface* i, BOOL suppressPrompts = FALSE, DWORD options = 0);

	BOOL SupportsOptions(int ext, DWORD options)
	{
		return FALSE;
	}

	BOOL CanExport()
	{
		return pBones.size() > 0;
	}

	typedef std::list<INode*> Nodes;

	Nodes pBones;

	const TCHAR* filename;

	Interface* gi;

private:
};

class AnimationExportClassDesc :
	public ClassDesc
{
public:
	int IsPublic(void)
	{
		return(1);
	}

	void* Create(BOOL loading = FALSE)
	{
		return(new AnimationExport);
	}

	const TCHAR* ClassName(void)
	{
		return("GameEngine Skinned Model Export");
	}

	SClass_ID SuperClassID(void)
	{
		return(SCENE_EXPORT_CLASS_ID);
	}

	Class_ID ClassID(void)
	{
		return(Class_ID(0x6fd34951, 0x4ca96d97));
	}

	const TCHAR* Category(void)
	{
		return("Scene Export");
	}
};

ClassDesc* GetAnimationExportClassDesc(void)
{
	return(new AnimationExportClassDesc);
}

struct RootBoneSelectDialog : public HitByNameDlgCallback
{
	TCHAR *dialogTitle() 
	{
		return "Select root bones";
	}
	TCHAR *buttonText()
	{
		return "Select";
	}
	BOOL singleSelect()
	{
		return FALSE;
	}
	BOOL useProc()
	{
		return FALSE;
	}
	int filter(INode *node)
	{
		return TRUE;
	}
};


// Dialog proc
static INT_PTR CALLBACK ExportDlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	AnimationExport *exp = (AnimationExport*)GetWindowLongPtr(hWnd, GWLP_USERDATA); 

	switch(msg)
	{
		case WM_INITDIALOG:
			exp =(AnimationExport*)lParam;
			EnableWindow(GetDlgItem(hWnd, IDOK), exp->CanExport());
			SetWindowLongPtr(hWnd,GWLP_USERDATA,lParam); 
			CheckDlgButton(hWnd, IDC_EXPORTROTATIONS, TRUE);
			CheckDlgButton(hWnd, IDC_EXPORTPOSITIONS, TRUE);
			CheckDlgButton(hWnd, IDC_EXPORTSCALES, TRUE);
			CenterWindow(hWnd, GetParent(hWnd)); 

			SendDlgItemMessage(hWnd, IDC_SPIN1, UDM_SETBUDDY, (WPARAM)GetDlgItem(hWnd, IDC_EDIT1), 0);
			SendDlgItemMessage(hWnd, IDC_SPIN2, UDM_SETBUDDY, (WPARAM)GetDlgItem(hWnd, IDC_EDIT2), 0);

			if (exp->gi)
			{
				Interval it(exp->gi->GetAnimRange().Start() / GetTicksPerFrame(), exp->gi->GetAnimRange().End() / GetTicksPerFrame());
				SendDlgItemMessage(hWnd, IDC_SPIN1, UDM_SETRANGE, 0, MAKELPARAM(it.End(), it.Start()));
				SendDlgItemMessage(hWnd, IDC_SPIN2, UDM_SETRANGE, 0, MAKELPARAM(it.End(), it.Start()));
				SendDlgItemMessage(hWnd, IDC_SPIN1, UDM_SETPOS, 0, MAKELPARAM(it.Start(), 0));
				SendDlgItemMessage(hWnd, IDC_SPIN2, UDM_SETPOS, 0, MAKELPARAM(it.End(), 0));
			}
			else
			{
				//TODO: work with error
			}

			break;
		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case ID_ADDBONE:
					{
						RootBoneSelectDialog root;
						GetCOREInterface()->ClearNodeSelection();
						GetCOREInterface()->DoHitByNameDialog(&root);
						if (GetCOREInterface()->GetSelNodeCount() > 0)
						{
							for (int i = 0; i < GetCOREInterface()->GetSelNodeCount(); i++)
							{
								INode* node = GetCOREInterface()->GetSelNode(i);
								exp->pBones.push_back(node);
								SendMessage(GetDlgItem(hWnd, IDC_BONESLIST), LB_ADDSTRING, 0, (LPARAM)node->GetName());
							}

							GetCOREInterface()->ClearNodeSelection();
						}
					}
					EnableWindow(GetDlgItem(hWnd, IDOK), exp->CanExport());
					break;

				case ID_REMOVEBONE:
					{
						int sel = SendMessage(GetDlgItem(hWnd, IDC_BONESLIST), LB_GETCURSEL, 0, 0);
						if (sel > -1)
						{
							SendMessage(GetDlgItem(hWnd, IDC_BONESLIST), LB_DELETESTRING, sel, 0);
							for (AnimationExport::Nodes::iterator m = exp->pBones.begin(); m != exp->pBones.end(); m++, sel--)
							{
								if (sel == 0)
								{
									exp->pBones.erase(m);
									break;
								}
							}
						}
					}

					EnableWindow(GetDlgItem(hWnd, IDOK), exp->CanExport());

					break;

				case IDC_EXPORTASXML:
					EnableWindow(GetDlgItem(hWnd, IDC_EXPORTROTATIONS), !IsDlgButtonChecked(hWnd, IDC_EXPORTASXML));
					EnableWindow(GetDlgItem(hWnd, IDC_EXPORTPOSITIONS), !IsDlgButtonChecked(hWnd, IDC_EXPORTASXML));
					EnableWindow(GetDlgItem(hWnd, IDC_EXPORTSCALES), !IsDlgButtonChecked(hWnd, IDC_EXPORTASXML));
					break;

				case IDOK:
					{

						FileOutput::Mode filemode = FileOutput::BINARY;
						if (IsDlgButtonChecked(hWnd, IDC_EXPORTASXML))
						{
							filemode = FileOutput::TEXTUAL;
						}
						else
						{
							bOptimizeSequences = true;
							bExportRotationSequence = IsDlgButtonChecked(hWnd, IDC_EXPORTROTATIONS);
							bExportPositionSequence = IsDlgButtonChecked(hWnd, IDC_EXPORTPOSITIONS);
							bExportScaleSequence = IsDlgButtonChecked(hWnd, IDC_EXPORTSCALES);
						}

						TimeValue a = LOWORD(SendDlgItemMessage(hWnd, IDC_SPIN1, UDM_GETPOS, 0,0)) * GetTicksPerFrame();
						TimeValue b = LOWORD(SendDlgItemMessage(hWnd, IDC_SPIN2, UDM_GETPOS, 0,0)) * GetTicksPerFrame();

						FileOutput modelfile(exp->filename, filemode);
						exp->ProcessAnimations(modelfile, exp->pBones, exp->gi, MIN(a, b), MAX(a, b));
						modelfile.close();
					}

					EndDialog(hWnd, 1);
					break;

				case IDCANCEL:
					EndDialog(hWnd, 0);
					break;
			}

			break;
		default:
			return FALSE;
	}

	return TRUE;
}       


int AnimationExport::DoExport(const TCHAR* name, ExpInterface* ei, Interface* gi, BOOL SuppressPrompts, DWORD options)
{
	this->filename = name;
	this->gi = gi;

	if(!DialogBoxParam(hInstance, MAKEINTRESOURCE(IDD_ANIMATIONEXPORT_DLG), gi->GetMAXHWnd(), ExportDlgProc,(LPARAM)this))
	{
		return 1;
	}

	return 1;
}
