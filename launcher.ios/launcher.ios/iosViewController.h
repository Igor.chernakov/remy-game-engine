//
//  iosViewController.h
//  launcher.ios
//
//  Created by Igor Chernakov on 13/10/2013.
//  Copyright (c) 2013 Igor Chernakov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@interface iosViewController : GLKViewController
@end