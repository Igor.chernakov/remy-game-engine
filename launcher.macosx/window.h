//
//  Header.h
//  core
//
//  Created by Igor Chernakov on 01/05/2013.
//  Copyright (c) 2013 Igor Chernakov. All rights reserved.
//

#ifndef core_Header_h
#define core_Header_h

#include "../interfaces/iwindow.hpp"
#include "../interfaces/irenderer.hpp"

#include "launcher.h"

extern int desktop_width;
extern int desktop_height;

extern IRenderer* renderer;

struct VideoWindow: IWindow {
	virtual void Show(bool show) {}
	virtual bool IsVisible() const { return true; }
	virtual bool Centralize() { return true; }
	virtual bool IsFullscreen() const { return false; }
	virtual void* GetWindow() const { return NULL; }
	virtual dim_t GetSize() const { return dim_t(1024, 768); }
	virtual point_t GetCursorPos() const
	{
		if (renderer->IsFullscreen())
		{
			dim_t bb_size = dim_t(desktop_width, desktop_height);
			dim_t wn_size = GetSize();
			float bb_aspect = (float)bb_size.width / (float)bb_size.height;
			float wn_aspect = (float)wn_size.width / (float)wn_size.height;
			int _mouse_y = mouse_y * wn_size.height / bb_size.height;
			int _mouse_x = (mouse_x - bb_size.height * (bb_aspect - wn_aspect) / 2) * wn_size.height / bb_size.height;
			return point_t(_mouse_x, _mouse_y);
		}
		else
		{
			return point_t(mouse_x, mouse_y);
		}
	}
	virtual void SetCursorPos(const point_t&) {}
	virtual void SetCursor(Cursor) {}
	virtual Cursor GetCursor() const { return CURSOR_ARROW; }
};

#endif
