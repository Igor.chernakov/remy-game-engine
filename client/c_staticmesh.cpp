/**
	Simple STATIC MESH class, supports LIGHTMAPS
**/

#include "c_client.hpp"
#include "c_entity.hpp"
#include "c_material.hpp"

static const std::string TEXTURES_FOLDER("textures");

namespace client
{
	shared_ptr<const IMaterial> LoadMaterial(FILE* file, int size, const std::string& folder, bool is_global);

	namespace io 
	{
		struct StaticMeshFile: shared::Resource
		{
			void Mod_ReadSTMS(FILE* file, int size) {
				F_BlockReadLoop(file, size) {
					switch (F_BlockReadHeader(file)) {
					case MAKEID('L', 'M', 'A', 'P'):
						//lightmaps.push_back(
						//	framework->GetRendererObjectManager()->LoadTexture(block.GetData(), 
						//	block.GetSize()));
						break;
					case MAKEID('T', 'X', 'T', 'R'):
						textures.push_back(LoadMaterial(file, F_BlockReadSize(file), f_StripExtension(GetPath()), true));
						break;
					case MAKEID('G', 'E', 'O', 'M'):
						meshes.push_back(Mesh(this, file, F_BlockReadSize(file)));
						break;
					default:
						F_BlockSkip(file);
					}
				}
			}

			StaticMeshFile(const std::string& filename)
				: shared::Resource(filename)
			{
				file_t file = framework->GetFileSystem()->LoadFile(filename);

				if (!file.valid())
					return;

				switch (F_BlockReadHeader(file)) {
				case MAKEID('S', 'T', 'M', 'S'):
					Mod_ReadSTMS(file, F_BlockReadSize(file));
					break;
				default:
					framework->Log(IFramework::MSG_ERROR, "%s is not a static mesh file", file.name.c_str());
					F_BlockSkip(file);
				}

				file.close();

				BOOST_FOREACH(const io::StaticMeshFile::Mesh& mesh, meshes) {
					(this->bounds += mesh.bounds.vMin) += mesh.bounds.vMax;
				}
			}

			struct Submesh
			{
				Submesh(StaticMeshFile* mesh, FILE* file, int size) {
					bounds.Reset();
					matrices.push_back(mat4_t());
					F_BlockReadLoop(file, size) {
						switch (F_BlockReadHeader(file)) {
						case MAKEID('N', 'A', 'M', 'E'):
							name = F_BlockReadString(file);
							break;
						case MAKEID('T', 'X', 'I', 'D'): {
							int txid = 0;
							F_BlockRead(file, txid);
							material = mesh->textures[txid];
							break;
						}
						case MAKEID('L', 'M', 'I', 'D'): {
							int txid = 0;
							F_BlockRead(file, txid);
							lightmap = mesh->lightmaps[txid];
							break;
						}
						case MAKEID('V', 'R', 'T', 'X'):
							F_BlockReadConverVector(file, vertices, StaticVertex);
							break;
						case MAKEID('F', 'A', 'C', 'E'):
							F_BlockReadVector(file, triangles, Triangle);
							break;
						case MAKEID('M', 'A', 'T', 'R'):
							F_BlockReadVector(file, matrices, mat4_t);
							break;
						default:
							F_BlockSkip(file);
						}
					}
					if (material && !(material->surfaceflags & IMaterial::SF_NO_BBOX)) {
						if (matrices.size() > 1) {
							BOOST_FOREACH(const mat4_t& matrix, matrices) {
								bounds += matrix.GetTranslation();
							}
						} else {
							BOOST_FOREACH(const vertex_t& vertex, vertices) {
								bounds += vertex.position;
							}
						}
					}
				}

				struct Triangle { WORD index[3]; };

				shared_ptr<const IMaterial> material;
				shared_ptr<const ITexture> lightmap;

				std::vector<vertex_t> vertices;
				std::vector<Triangle> triangles;
				std::vector<mat4_t> matrices;
				std::string name;
				box3_t bounds;
			};

			struct Mesh: std::list<Submesh>
			{
				Mesh(StaticMeshFile* mesh, FILE* file, int size)
				{
					F_BlockReadLoop(file, size) {
						switch (F_BlockReadHeader(file)) {
						case MAKEID('M', 'E', 'S', 'H'):
							push_back(Submesh(mesh, file, F_BlockReadSize(file)));
							break;
						default:
							F_BlockSkip(file);
						}
					}

					BOOST_FOREACH(const StaticMeshFile::Submesh& submesh, *this) {
						(this->bounds += submesh.bounds.vMin) += submesh.bounds.vMax;
					}
				}

				box3_t bounds;
			};

			std::vector<Mesh> meshes;
			std::vector<shared_ptr<const ITexture> > lightmaps;
			std::vector<shared_ptr<const IMaterial> > textures;
			box3_t bounds;
		};
	}

	class StaticMesh: public Entity
	{
	public:
		StaticMesh(
			const std::string& name,
			const vec3_t& position,
			const quat_t& rotation,
			const vec3_t& scale,
			const color_t& color,
			const std::string& source)
			: Entity(name, position, rotation, scale, color)
		{
			RenderCallback = (RenderCallbackT)&StaticMesh::OnRender;
			IntersectionChecker = (IntersectionCheckerT)&StaticMesh::CheckIntersection;

			if (framework->GetFileSystem()->CheckFile(source))
			{
				SetModel(source);
			}
		}

		void SetModel(const std::string& source)
		{
			m_model = framework->GetResources()->Load<io::StaticMeshFile>(source);
		}

		std::string GetModel() const
		{
			return m_model ? m_model->GetPath() : std::string();
		}

		void OnRender(IClientScene* scene) const
		{
			mat4_t matrix;

			BOOST_FOREACH(const io::StaticMeshFile::Mesh& mesh, m_model->meshes)
			{
				if (!framework->GetScene()->GetCamera()->BoxInFrustum(mesh.bounds, GetMatrix()))
					continue;

				BOOST_FOREACH(const io::StaticMeshFile::Submesh& submesh, mesh)
				{

					BOOST_FOREACH(const mat4_t& instance, submesh.matrices)
					{
						if (!framework->GetScene()->GetCamera()->BoxInFrustum(submesh.bounds,
							matrix = GetMatrix() * instance))
							continue;

						Surface surface;
						surface.pMatrix = &matrix;
						surface.pLightmap = submesh.lightmap;
						surface.pMaterial = submesh.material;
						surface.pVertices = &*submesh.vertices.begin();
						surface.nVertices = submesh.vertices.size();
						surface.pIndices = (WORD*)&*submesh.triangles.begin();
						surface.nIndices = submesh.triangles.size() * 3;
						scene->PushSurface(&surface);
					}
				}
			}
		}

		const box3_t& GetBoundingBox() const
		{
			return m_model->bounds;
		}

		bool CheckIntersection(const math::Line& _line) const
		{
			if (!_line.Intersects(m_model->bounds))
				return false;

			bool result = false;
			math::Line line(_line);

			BOOST_FOREACH(const io::StaticMeshFile::Mesh& mesh, m_model->meshes)
			{
				if (!line.Intersects(mesh.bounds))
					continue;

				BOOST_FOREACH(const io::StaticMeshFile::Submesh& submesh, mesh)
				{

					BOOST_FOREACH(const mat4_t& instance, submesh.matrices)
					{
						if (!(line * mat4_t::Invert(instance)).Intersects(submesh.bounds))
							continue;

						BOOST_FOREACH(const io::StaticMeshFile::Submesh::Triangle& triangle, submesh.triangles)
						{
							if (line.Intersects(
									math::Triangle(
										submesh.vertices[triangle.index[0]].position,
										submesh.vertices[triangle.index[1]].position,
										submesh.vertices[triangle.index[2]].position),
									&line.b))
							{
								result = true;
								m_hotspot = line.b;
							}
						}
					}
				}
			}

			return result;
		}

	private:
		shared_ptr<const io::StaticMeshFile> m_model;
	};
}

void export_StaticMesh()
{
	namespace py = boost::python;

	typedef client::StaticMesh T;

	DISPOSEME(4) py::class_<T, shared_ptr<T>, py::bases<client::Entity>, boost::noncopyable>("StaticMesh", py::no_init)
		.def(py::init<std::string, vec3_t, quat_t, vec3_t, color_t, std::string>((
			py::arg("id") = std::string("StaticMesh#"),
			py::arg("position") = vec3_t::Zero,
			py::arg("rotation") = quat_t::Identity,
			py::arg("scale") = vec3_t::One,
			py::arg("color") = color_t(),
			py::arg("source") = std::string("none"))))
		.add_property("source", &T::GetModel, &T::SetModel)
		;
}
