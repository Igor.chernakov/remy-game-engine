#include "c_client.hpp"
#include "c_entity.hpp"
#include "c_material.hpp"

namespace client { namespace primitive
{
	class Plane
		: public Entity
		, public boost::python::wrapper<Plane>
		, public IWrapper
	{
	public:
		Plane(
			const std::string& name,
			const vec3_t& position,
			const quat_t& rotation,
			const vec3_t& scale,
			const color_t& color,
			const vec2_t& size,
			const box2_t& uv,
			shared_ptr<const IMaterial> material)
			: Entity(name, position, rotation, scale, color)
			, m_Normal(vec3_t(0,0,1))
			, m_Front(vec3_t(1,0,0))
			, m_Size(size)
			, m_Material(material)
			, m_UV(uv)
		{
			RenderCallback = (RenderCallbackT)&Plane::OnRender;
			IntersectionChecker = (IntersectionCheckerT)&Plane::CheckIntersection;
		}

		void OnRender(IClientScene* scene) const
		{
			const vec3_t
				rt(vec3_t::Normalize(vec3_t::Cross(m_Normal, -m_Front))),
				up(vec3_t::Normalize(vec3_t::Cross(m_Normal, -rt)));

			vertex_t v[6];

			Surface s(Surface::CreateBox(v, rt * m_Size.x / 2, up * m_Size.y / 2, vec3_t::Zero));

			for (int i = 0; i < 6; i++) {
				v[i].uv.x = m_UV.vMin.x + m_UV.Width() * v[i].uv.x;
				v[i].uv.y = m_UV.vMin.y + m_UV.Height() * v[i].uv.y;
			}

			s.pMaterial = m_Material;
			s.SetColor(GetColor());
			s.pMatrix = &GetMatrix();
			s.pivot = m_pivot;

			index_t i[6] = { 0, 1, 2, 3, 4, 5 };
			s.pIndices = i;
			s.nIndices = 6;

			scene->PushSurface(&s);
		}

		bool CheckIntersection(const math::Line& line) const
		{
			const vec3_t
				rt(vec3_t::Normalize(vec3_t::Cross(m_Normal, -m_Front))),
				up(vec3_t::Normalize(vec3_t::Cross(m_Normal, -rt)));

			vertex_t v[6];

			Surface s(Surface::CreateBox(v, rt * m_Size.x / 2, up * m_Size.y / 2, vec3_t::Zero));

			return  
				line.Intersects(math::Triangle(v[1].position, v[0].position, v[2].position, m_Normal), &m_hotspot) ||
				line.Intersects(math::Triangle(v[4].position, v[3].position, v[5].position, m_Normal), &m_hotspot);
		}

		shared_ptr<const IMaterial> GetMaterial() const { return m_Material; }
		void SetMaterial(shared_ptr<const IMaterial> value) { m_Material = value; }

		virtual shared_ptr<client::Entity> shared_from_this()
		{
			return boost::python::extract<shared_ptr<client::Entity> >(GetPythonObject());
		}

		virtual shared_ptr<const client::Entity> shared_from_this() const
		{
			return boost::python::extract<shared_ptr<const client::Entity> >(GetPythonObject());
		}

		virtual PyObject* GetPythonObject() const
		{
			return boost::python::detail::wrapper_base_::get_owner(*this);
		}

	public:
		box2_t m_UV;
		vec2_t m_Size;

	private:
		vec3_t m_Normal, m_Front;
		shared_ptr<const IMaterial> m_Material;
	};

	class Box
		: public Entity
		, public boost::python::wrapper<Box>
		, public IWrapper
	{
	public:
		Box(
			const std::string& name,
			const vec3_t& position,
			const quat_t& rotation,
			const vec3_t& scale,
			const color_t& color,
			const vec3_t& size)
			: Entity(name, position, rotation, scale, color)
			, m_box(-size.x / 2, -size.y / 2, 0, size.x / 2, size.y / 2, size.z)
			, m_size(size)
		{
			SetFlags(FLAG_NOCLIP, false);
			IntersectionChecker = (IntersectionCheckerT)&Box::CheckIntersection;
			DebugCallback = (DebugCallbackT)&Box::OnDebug;
		}

		const vec3_t& GetSize() const {
			return m_size;
		}

		void SetSize(const vec3_t& size) {
			m_size = size;
			m_box = box3_t(-size.x / 2, -size.y / 2, 0, size.x / 2, size.y / 2, size.z);
		}

		bool CheckIntersection(const math::Line& line) const
		{
			return line.Intersects(GetBoundingBox(), &m_hotspot);
		}

		void OnDebug(IHelper* helper) const
		{
			helper->Box(GetBoundingBox(), color_t(0, 1, 0), &GetMatrix());
		}
		
		virtual const box3_t& GetBoundingBox() const
		{
			return m_box;
		}

		virtual shared_ptr<client::Entity> shared_from_this()
		{
			return boost::python::extract<shared_ptr<client::Entity> >(GetPythonObject());
		}

		virtual shared_ptr<const client::Entity> shared_from_this() const
		{
			return boost::python::extract<shared_ptr<const client::Entity> >(GetPythonObject());
		}

		virtual PyObject* GetPythonObject() const
		{
			return boost::python::detail::wrapper_base_::get_owner(*this);
		}

	private:
		box3_t m_box;
		vec3_t m_size;
	};

}}

void export_Primitive()
{
	namespace py = boost::python;
	using client::primitive::Plane;
	using client::primitive::Box;

	DISPOSEME(4) py::class_<Plane, shared_ptr<Plane>, py::bases<client::Entity>, boost::noncopyable>("Plane", 
		py::init<std::string,vec3_t,quat_t,vec3_t,color_t,vec2_t,box2_t,shared_ptr<const IMaterial> >((
			py::arg("id") = std::string("Plane#"),
			py::arg("position") = vec3_t::Zero,
			py::arg("rotation") = quat_t::Identity,
			py::arg("scale") = vec3_t::One,
			py::arg("color") = color_t(),
			py::arg("size") = vec2_t(100, 100),
			py::arg("uv") = box2_t(0, 0, 1, 1),
			py::arg("material") = shared_ptr<const IMaterial>())))
		.add_property("material", &Plane::GetMaterial, &Plane::SetMaterial)
		.def_readwrite("uv", &Plane::m_UV)
		.def_readwrite("size", &Plane::m_Size)
		;

	DISPOSEME(4) py::class_<Box, shared_ptr<Box>, py::bases<client::Entity>, boost::noncopyable>("Box", 
		py::init<std::string,vec3_t,quat_t,vec3_t,color_t,vec3_t>((
			py::arg("id") = std::string("Plane#"),
			py::arg("position") = vec3_t::Zero,
			py::arg("rotation") = quat_t::Identity,
			py::arg("scale") = vec3_t::One,
			py::arg("color") = color_t(),
			py::arg("size") = vec3_t(100, 100, 100))))
		.add_property("size", py_cref(&Box::GetSize), &Box::SetSize)
		;

	py::implicitly_convertible<shared_ptr<Plane>, shared_ptr<client::Entity> >();
}
