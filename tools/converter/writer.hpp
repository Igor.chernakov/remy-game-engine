#pragma once

#include <fstream>
#include <string>
#include <vector>
#include <math.h>
#include <float.h>

#define MIN(x, y) ((x) > (y) ? (y) : (x))
#define MAX(y, x) ((x) > (y) ? (y) : (x))

struct Writer: public std::ofstream
{
	Writer(const std::string& filename, bool append_to_file = false)
		: std::ofstream(filename.c_str(), append_to_file ? (out | binary | app) : (out | binary))
	{
	}
	~Writer()
	{
		close();
	}
	template <typename T>
	Writer& operator<< (const T& dst)
	{
		write((const char*)&dst, sizeof(dst));
		return *this;
	}
	template <typename T>
	Writer& operator<< (const std::vector<T>& dst)
	{
		//*this << dst.size() * sizeof(T);
		for (int count = 0; *this && count < dst.size(); ++count)
		{
			//T value;
			//*this << value;
			//dst.push_back(value);
			*this << dst[count];
		}
		return *this;
	}
	template <>
	Writer& operator<< (const std::string& src)
	{
		write(src.data(), (std::streamsize)src.length());
		return *this;
	}
};


class Area
{
public:
	Area(const std::string& tag, Writer& writer, bool datablock = false)
		: writer(writer)
		, start((int)writer.tellp() + 4)
		, datablock(datablock)
		, failed(false)
	{
		char buffer[8] = { 0 };
		memcpy(buffer, &*tag.begin(), tag.length() > 4 ? 4 : tag.length());
		writer << buffer;
	}
	~Area()
	{
		if (!failed)
		{
			Writer::pos_type current(writer.tellp());
			writer.seekp(start);
			writer << (datablock ? 1 : -1) * (int)(current - start - 4);
			writer.seekp(current);
		}
		else
		{
			writer.seekp((int)start - 4);
		}
	}
	bool fail(bool value)
	{
		return failed = value;
	}

private:
	Writer::pos_type start;
	Writer& writer;
	bool datablock, failed;
};

struct Size
{
	int width, height;
};

struct Vector2
{
	Vector2(float x = 0, float y = 0): x(x), y(y) {}
	float x, y;
};

struct Vector3
{
	Vector3(float x = 0, float y = 0, float z = 0): x(x), y(y), z(z) {}
	float x, y, z;
};
struct Matrix
{
	Matrix()
	{
		m[0] = 1, m[1] = 0, m[2] = 0, m[3] = 0;
		m[4] = 0, m[5] = 1, m[6] = 0, m[7] = 0;
		m[8] = 0, m[9] = 0, m[10] = 1, m[11] = 0;
		m[12] = 0, m[13] = 0, m[14] = 0, m[15] = 1;
	}
	Matrix& SetPosition(const Vector3& position)
	{
		m[12] = position.x, m[13] = position.y, m[14] = position.z;
		return *this;
	}
	Matrix& RotateYawPitchRoll(float yaw, float pitch, float roll)
	{
		Matrix xrot, yrot, zrot;

		xrot.m[5] = (float)cos(yaw);
		xrot.m[6] = (float)sin(yaw);
		xrot.m[9] = -(float)sin(yaw);
		xrot.m[10] = (float)cos(yaw);

		yrot.m[0] = (float)cos(pitch);
		yrot.m[2] = -(float)sin(pitch);
		yrot.m[8] = (float)sin(pitch);
		yrot.m[10] = (float)cos(pitch);

		zrot.m[0] = (float)cos(roll);
		zrot.m[1] = (float)sin(roll);
		zrot.m[4] = -(float)sin(roll);
		zrot.m[5] = (float)cos(roll);

		return *this = *this * xrot * yrot * zrot;
	}

	Matrix operator* (const Matrix& right)
	{
		Matrix r, &left = *this;

		r.v[0][0] = left.v[0][0] * right.v[0][0] + left.v[1][0] * right.v[0][1] + left.v[2][0] * right.v[0][2] + left.v[3][0] * right.v[0][3];
		r.v[1][0] = left.v[0][0] * right.v[1][0] + left.v[1][0] * right.v[1][1] + left.v[2][0] * right.v[1][2] + left.v[3][0] * right.v[1][3];
		r.v[2][0] = left.v[0][0] * right.v[2][0] + left.v[1][0] * right.v[2][1] + left.v[2][0] * right.v[2][2] + left.v[3][0] * right.v[2][3];
		r.v[3][0] = left.v[0][0] * right.v[3][0] + left.v[1][0] * right.v[3][1] + left.v[2][0] * right.v[3][2] + left.v[3][0] * right.v[3][3];

		r.v[0][1] = left.v[0][1] * right.v[0][0] + left.v[1][1] * right.v[0][1] + left.v[2][1] * right.v[0][2] + left.v[3][1] * right.v[0][3];
		r.v[1][1] = left.v[0][1] * right.v[1][0] + left.v[1][1] * right.v[1][1] + left.v[2][1] * right.v[1][2] + left.v[3][1] * right.v[1][3];
		r.v[2][1] = left.v[0][1] * right.v[2][0] + left.v[1][1] * right.v[2][1] + left.v[2][1] * right.v[2][2] + left.v[3][1] * right.v[2][3];
		r.v[3][1] = left.v[0][1] * right.v[3][0] + left.v[1][1] * right.v[3][1] + left.v[2][1] * right.v[3][2] + left.v[3][1] * right.v[3][3];

		r.v[0][2] = left.v[0][2] * right.v[0][0] + left.v[1][2] * right.v[0][1] + left.v[2][2] * right.v[0][2] + left.v[3][2] * right.v[0][3];
		r.v[1][2] = left.v[0][2] * right.v[1][0] + left.v[1][2] * right.v[1][1] + left.v[2][2] * right.v[1][2] + left.v[3][2] * right.v[1][3];
		r.v[2][2] = left.v[0][2] * right.v[2][0] + left.v[1][2] * right.v[2][1] + left.v[2][2] * right.v[2][2] + left.v[3][2] * right.v[2][3];
		r.v[3][2] = left.v[0][2] * right.v[3][0] + left.v[1][2] * right.v[3][1] + left.v[2][2] * right.v[3][2] + left.v[3][2] * right.v[3][3];

		r.v[0][3] = left.v[0][3] * right.v[0][0] + left.v[1][3] * right.v[0][1] + left.v[2][3] * right.v[0][2] + left.v[3][3] * right.v[0][3];
		r.v[1][3] = left.v[0][3] * right.v[1][0] + left.v[1][3] * right.v[1][1] + left.v[2][3] * right.v[1][2] + left.v[3][3] * right.v[1][3];
		r.v[2][3] = left.v[0][3] * right.v[2][0] + left.v[1][3] * right.v[2][1] + left.v[2][3] * right.v[2][2] + left.v[3][3] * right.v[2][3];
		r.v[3][3] = left.v[0][3] * right.v[3][0] + left.v[1][3] * right.v[3][1] + left.v[2][3] * right.v[3][2] + left.v[3][3] * right.v[3][3];

		return r;
	}

	Matrix& Scale(const Vector3& op)
	{
		Matrix scale;
		scale.m[0] = op.x, scale.m[5] = op.y, scale.m[10] = op.z;
		return *this = *this * scale;
	}

	union
	{
		float m[16];
		float v[4][4];
	};
};
struct Bounds
{
	Bounds(const Vector3& vmin = Vector3(FLT_MAX, FLT_MAX, FLT_MAX), const Vector3& vmax = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX)): vmin(vmin), vmax(vmax) {}
	Bounds& operator+= (const Vector3& op)
	{
		vmin.x = MIN(vmin.x, op.x), vmin.y = MIN(vmin.y, op.y), vmin.z = MIN(vmin.z, op.z);
		vmax.x = MAX(vmax.x, op.x), vmax.y = MAX(vmax.y, op.y), vmax.z = MAX(vmax.z, op.z);
	}
	inline bool Contains(const Vector3& op) const
	{
		return op.x >= vmin.x && op.y >= vmin.y && op.z >= vmin.z && op.x < vmax.x && op.y < vmax.y && op.z < vmax.z;
	}
	Vector3 vmin, vmax;
};

struct Vertex
{
	Vertex(float x, float y, float z, float u, float v, float u2 = 0, float v2 = 0, int color = -1)
		: position(x, y, z)
		, texcoord(u, v)
		, lmcoord(u2, v2)
		, color(color)
	{
	}
	Vertex(const float* p, const float* t, const float* lm, int color = -1)
		: position(*p++, *p++, *p++)
		, texcoord(*t++, *t++)
		, lmcoord(*lm++, *lm++)
		, color(color)
	{
	}
	Vector3 position;
	int color;
	Vector2 texcoord, lmcoord;
};

