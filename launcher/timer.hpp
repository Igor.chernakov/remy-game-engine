#pragma once

#ifndef WIN32
#	include <sys/time.h>
#else
#	include <windows.h>
#endif

#include <stdlib.h>

#include "../interfaces/itimer.hpp"
#include "../shared/types.hpp"

#include <iostream>
#include <numeric>

class Timer: public ITimer
{
public:
	float ONE_SECOND;

	Timer()
	{
		ONE_SECOND = 1000.0f;
		Reset();
	}

	unsigned long GetMilliseconds()
	{
#ifdef WIN32
		return GetTickCount();
#else
		struct timeval tv;
		gettimeofday(&tv, 0);
		return (tv.tv_sec * 1000 + tv.tv_usec / 1000);
#endif
	}

	void Reset()
	{
		mTimeStep = 0;
		mMicro = GetMilliseconds();
		mTimeSteps.Reset();
	}

	float Update()
	{
		mTimeStep = GetMilliseconds() - mMicro;
		while (mTimeStep == 0) 
		{
#ifdef WIN32
			Sleep(1);
#else
			sleep(1);
#endif
			mTimeStep = GetMilliseconds() - mMicro;
		}
		mMicro += mTimeStep;
		mTimeSteps.Append(mTimeStep);
		return (float)mTimeStep / ONE_SECOND;
	}

	float GetFPS() const
	{
#ifdef __APPLE__
		extern float timeSinceLastUpdate;
		return 1.0f / timeSinceLastUpdate;
#else
		return ONE_SECOND / mTimeSteps.GetAverage();
#endif
	}

private:
	unsigned mMicro, mTimeStep;
	stat_t<unsigned> mTimeSteps;
};

static ITimer* CreateTimer()
{
	return new Timer();
}
