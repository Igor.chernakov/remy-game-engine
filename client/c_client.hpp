#pragma once

#ifdef WIN32
#	include <vld.h>
#endif

#define BOOST_PYTHON_STATIC_LIB
#define USE_GPU_SHADERS

#undef __int8_t
//#include <boost/asio.hpp>
#include <boost/python.hpp>
#include <boost/optional.hpp>

#ifdef __APPLE__
#	define ENGINE_MULTITHREADING
#endif

#ifdef _DEBUG
#	define SET_BREAKPOINT 
//_asm { int 3 }
#else
#	define SET_BREAKPOINT
#endif

#define script_error \
	(const boost::python::error_already_set&) \
		{ \
		PyErr_Print(); \
		PyErr_Clear(); \
		SET_BREAKPOINT \
		}

#define py_cref(func) \
	boost::python::make_function(func, boost::python::return_value_policy<boost::python::copy_const_reference>())
#define py_ref(func) \
	boost::python::make_function(func, boost::python::return_internal_reference<>())

#include "../interfaces/iclientframework.hpp"

extern IClientFramework* framework;

#ifdef WIN32

void DisposablePyObject(const char* file, PyObject *pyobject, int count);

struct DisposablePyObjectT
{
	int count;
	const char* file;
	DisposablePyObjectT(const char* file, int count): file(file), count(count) {}
	void operator += (boost::python::object& object)
	{
		DisposablePyObject(file, object.ptr(), count);
	}
};


#define DISPOSEME(x) DisposablePyObjectT(__FILE__, x) += 

#else

#define DISPOSEME(x)

#endif

namespace boost
{
	namespace python
	{
		template<class T1, class T2>
		struct pair_to_tuple
		{
			static PyObject* convert(const std::pair<T1, T2>& p)
			{
				return incref((boost::python::make_tuple(p.first, p.second)).ptr());
			}
		};

		template<class T1, class T2>
		struct tuple_to_pair
		{
			tuple_to_pair()
			{
				converter::registry::push_back(&convertible, &construct, type_id<std::pair<T1, T2> >());
			}

			static void* convertible(PyObject* x)
			{
				return PyTuple_Check(x) ? x: 0;
			}

			static void construct(PyObject* x, converter::rvalue_from_python_stage1_data* data)
			{
				void* storage = ((converter::rvalue_from_python_storage<std::pair<T1, T2> >*)data)->storage.bytes;
				object o(borrowed(x));
				std::pair<T1, T2> p;
				p.first = extract<T1>(o[0]);
				p.second = extract<T2>(o[1]);
				new (storage) std::pair<T1, T2>(p);
				data->convertible = storage;
			}
		};

		template <typename T1, typename T2>
		struct pair_register
		{
			pair_register()
			{
				to_python_converter<std::pair<T1, T2>, pair_to_tuple<T1, T2> >();
				to_python_converter<std::pair<const T1, T2>, pair_to_tuple<const T1, T2> >();
				to_python_converter<std::pair<T1, const T2>, pair_to_tuple<T1, const T2> >();
				to_python_converter<std::pair<const T1, const T2>, pair_to_tuple<const T1, const T2> >();
				tuple_to_pair<T1, T2>();
			}
		};
	}
}

namespace client
{
	struct client_message_t {
		color_t color;
		std::string text;
	};

	extern std::list<client_message_t> g_Messages;
}

struct StaticVertex
{
	vec3_t position;
	vec3_t normal;
	color_t color;
	vec2_t tc[2];

	operator vertex_t() const
	{
		vertex_t out = { position, vec4_t(), color_t(), normal, color, tc[0] };
		return out;
	}
};

