/* A simple program to show how to set up an X window for OpenGL rendering.
 * X86 compilation: gcc -o -L/usr/X11/lib   main main.c -lGL -lX11
 * X64 compilation: gcc -o -L/usr/X11/lib64 main main.c -lGL -lX11
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>


#include <GL/glx.h>    /* this includes the necessary X headers */
#include <GL/gl.h>

#include <X11/X.h>    /* X11 constant (e.g. TrueColor) */
#include <X11/keysym.h>

#include "../launcher/timer.hpp"
#include "../launcher/framework.hpp"
#include "../launcher/mycout.hpp"

#include "../interfaces/iclient.hpp"
#include "../interfaces/iserver.hpp"
#include "../interfaces/ifilesystem.hpp"
#include "../interfaces/isoundsystem.hpp"

extern IFileSystem* CreateFileSystem(IFramework*);
extern ISoundSystem* CreateSoundSystem(IClientFramework*);
extern IRenderer* CreateRenderer(IClientFramework*);
extern IClient* CreateClient(int argc, char **argv, const char*, const char*, IClientFramework*, IRenderer*);

void SetErrorState() {}

IClientFramework* clientframework = 0;
IClient* client = 0;
IRenderer* renderer = 0;

static int snglBuf[] = {GLX_RGBA, GLX_DEPTH_SIZE, 16, None};
static int dblBuf[]  = {GLX_RGBA, GLX_DEPTH_SIZE, 16, GLX_DOUBLEBUFFER, None};

Display   *dpy;
Window     win;
GLfloat    xAngle = 42.0, yAngle = 82.0, zAngle = 112.0;
GLboolean  doubleBuffer = GL_TRUE;

int screen_x = 1024, screen_y = 768;
int mouse_x, mouse_y;

void fatalError(char *message)
{
  fprintf(stderr, "main: %s\n", message);
  exit(1);
}
#if 0
void redraw(void)
{
  static GLboolean   displayListInited = GL_FALSE;

  if (displayListInited)
  {
    /* if display list already exists, just execute it */
    glCallList(1);
  }
  else
  {
    /* otherwise compile and execute to create the display list */
    glNewList(1, GL_COMPILE_AND_EXECUTE);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    /* front face */
    glBegin(GL_QUADS);
      glColor3f(0.0, 0.7, 0.1);  /* green */
      glVertex3f(-1.0, 1.0, 1.0);
      glVertex3f(1.0, 1.0, 1.0);
      glVertex3f(1.0, -1.0, 1.0);
      glVertex3f(-1.0, -1.0, 1.0);

      /* back face */
      glColor3f(0.9, 1.0, 0.0);  /* yellow */
      glVertex3f(-1.0, 1.0, -1.0);
      glVertex3f(1.0, 1.0, -1.0);
      glVertex3f(1.0, -1.0, -1.0);
      glVertex3f(-1.0, -1.0, -1.0);

      /* top side face */
      glColor3f(0.2, 0.2, 1.0);  /* blue */
      glVertex3f(-1.0, 1.0, 1.0);
      glVertex3f(1.0, 1.0, 1.0);
      glVertex3f(1.0, 1.0, -1.0);
      glVertex3f(-1.0, 1.0, -1.0);

      /* bottom side face */
      glColor3f(0.7, 0.0, 0.1);  /* red */
      glVertex3f(-1.0, -1.0, 1.0);
      glVertex3f(1.0, -1.0, 1.0);
      glVertex3f(1.0, -1.0, -1.0);
      glVertex3f(-1.0, -1.0, -1.0);
    glEnd();
    glEndList();
    displayListInited = GL_TRUE;
  }
  if (doubleBuffer)
    glXSwapBuffers(dpy, win);/* buffer swap does implicit glFlush */
  else
    glFlush();  /* explicit flush for single buffered case */
}
#endif

struct VideoWindow: IWindow {
	virtual void Show(bool show) {}
	virtual bool IsVisible() const { return true; }
	virtual bool Centralize() { return true; }
	virtual bool IsFullscreen() const { return false; }
	virtual Window* GetWindow() const { return &win; }
	virtual Display* GetDisplay() const { return dpy; }
	virtual dim_t GetSize() const {
        //XWindowAttributes wndAttr;
        //XGetWindowAttributes(dpy,win,&wndAttr);
        //return dim_t(wndAttr.width, wndAttr.height);
        return dim_t(1024, 768);
    }
	virtual point_t GetCursorPos() const
	{
		if (screen_x != 1024 || screen_y != 768)//renderer->IsFullscreen())
		{
			dim_t bb_size = dim_t(screen_x, screen_y);
			dim_t wn_size = GetSize();
			float bb_aspect = (float)bb_size.width / (float)bb_size.height;
			float wn_aspect = (float)wn_size.width / (float)wn_size.height;
			int _mouse_y = mouse_y * wn_size.height / bb_size.height;
			int _mouse_x = (mouse_x - bb_size.height * (bb_aspect - wn_aspect) / 2) * wn_size.height / bb_size.height;
			return point_t(_mouse_x, _mouse_y);
		}
		else
		{
			return point_t(mouse_x, mouse_y);
		}
	}
	virtual void SetCursorPos(const point_t&) {}
	virtual void SetCursor(Cursor) {}
	virtual Cursor GetCursor() const { return CURSOR_ARROW; }
};

std::string GetAppDataFolder();

void CreateFolder(const std::string& folder);

bool set_fullscreen(bool fullscreen) {
    return false;
}

bool is_fullscreen() {
    return false;
}

int main(int argc, char **argv)
{
  XVisualInfo         *vi;
  Colormap             cmap;
  XSetWindowAttributes swa;
  GLXContext           cx;
  XEvent               event;
  GLboolean            needRedraw = GL_FALSE, recalcModelView = GL_TRUE;
  int                  dummy;

  /*** (1) open a connection to the X server ***/

  dpy = XOpenDisplay(NULL);
  if (dpy == NULL)
    fatalError("could not open display");

  /*** (2) make sure OpenGL's GLX extension supported ***/

  if(!glXQueryExtension(dpy, &dummy, &dummy))
    fatalError("X server has no OpenGL GLX extension");

  /*** (3) find an appropriate visual ***/

  /* find an OpenGL-capable RGB visual with depth buffer */
  vi = glXChooseVisual(dpy, DefaultScreen(dpy), dblBuf);
  if (vi == NULL)
  {
    vi = glXChooseVisual(dpy, DefaultScreen(dpy), snglBuf);
    if (vi == NULL) fatalError("no RGB visual with depth buffer");
    doubleBuffer = GL_FALSE;
  }
  if(vi->c_class != TrueColor)
    fatalError("TrueColor visual required for this program");

  /*** (4) create an OpenGL rendering context  ***/

  /* create an OpenGL rendering context */
  cx = glXCreateContext(dpy, vi, /* no shared dlists */ None,
                        /* direct rendering if possible */ GL_TRUE);
  if (cx == NULL)
    fatalError("could not create rendering context");

  /*** (5) create an X window with the selected visual ***/

  /* create an X colormap since probably not using default visual */
  cmap = XCreateColormap(dpy, RootWindow(dpy, vi->screen), vi->visual, AllocNone);
  swa.colormap = cmap;
  swa.border_pixel = 0;
  swa.event_mask = KeyPressMask    | ExposureMask | PointerMotionMask
                 | ButtonPressMask | ButtonReleaseMask
                 | StructureNotifyMask;
  win = XCreateWindow(dpy, RootWindow(dpy, vi->screen), 0, 0,
                      screen_x, screen_y, 0, vi->depth, InputOutput, vi->visual,
                      CWBorderPixel | CWColormap | CWEventMask, &swa);
  XSetStandardProperties(dpy, win, "Among the Heavens", "main", None,
                         argv, argc, NULL);

  /*** (5.5) create game-related objects ***/

    ClientFramework c_framework;
    clientframework = &c_framework;

    c_framework.FileSystem.reset(CreateFileSystem(&c_framework));
    c_framework.Timer.reset(CreateTimer());
    c_framework.SoundSystem.reset(CreateSoundSystem(&c_framework));
    c_framework.Variables.reset(CreateVariables());
    c_framework.Window.reset(new VideoWindow());

    CreateFolder(GetAppDataFolder());

  /*** (6) bind the rendering context to the window ***/

  glXMakeCurrent(dpy, win, cx);

  /*** (7) request the X window to be displayed on the screen ***/

  XMapWindow(dpy, win);

    if ((renderer = CreateRenderer(&c_framework)))
    {
        if ((client = CreateClient(0, 0, ".", ".", &c_framework, renderer)))
        {
            client->Run();
            IClient *tmp = client;
            client = NULL;
            delete tmp;
        }
    }


#if 0

  /*** (8) configure the OpenGL context for rendering ***/

  glEnable(GL_DEPTH_TEST); /* enable depth buffering */
  glDepthFunc(GL_LESS);    /* pedantic, GL_LESS is the default */
  glClearDepth(1.0);       /* pedantic, 1.0 is the default */

  /* frame buffer clears should be to black */
  glClearColor(0.0, 0.0, 0.0, 0.0);

  /* set up projection transform */
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glFrustum(-1.0, 1.0, -1.0, 1.0, 1.0, 10.0);
  /* establish initial viewport */
  /* pedantic, full window size is default viewport */
  glViewport(0, 0, 300, 300);

  printf( "Press left mouse button to rotate around X axis\n" );
  printf( "Press middle mouse button to rotate around Y axis\n" );
  printf( "Press right mouse button to rotate around Z axis\n" );
  printf( "Press ESC to quit the application\n" );

  /*** (9) dispatch X events ***/

  while (1)
  {
    do
    {
      XNextEvent(dpy, &event);
      switch (event.type)
      {
        case KeyPress:
        {
          KeySym     keysym;
          XKeyEvent *kevent;
          char       buffer[1];
          /* It is necessary to convert the keycode to a
           * keysym before checking if it is an escape */
          kevent = (XKeyEvent *) &event;
          if (   (XLookupString((XKeyEvent *)&event,buffer,1,&keysym,NULL) == 1)
              && (keysym == (KeySym)XK_Escape) )
            exit(0);
          break;
        }
        case ButtonPress:
          recalcModelView = GL_TRUE;
          switch (event.xbutton.button)
          {
            case 1: xAngle += 10.0;
              break;
            case 2: yAngle += 10.0;
              break;
            case 3: zAngle += 10.0;
              break;
          }
          break;
        case ConfigureNotify:
          glViewport(0, 0, event.xconfigure.width,
                     event.xconfigure.height);
          /* fall through... */
        case Expose:
          needRedraw = GL_TRUE;
          break;
      }
    } while(XPending(dpy)); /* loop to compress events */

    if (recalcModelView)
    {
      glMatrixMode(GL_MODELVIEW);

      /* reset modelview matrix to the identity matrix */
      glLoadIdentity();

      /* move the camera back three units */
      glTranslatef(0.0, 0.0, -3.0);

      /* rotate by X, Y, and Z angles */
      glRotatef(xAngle, 0.1, 0.0, 0.0);
      glRotatef(yAngle, 0.0, 0.1, 0.0);
      glRotatef(zAngle, 0.0, 0.0, 1.0);

      recalcModelView = GL_FALSE;
      needRedraw = GL_TRUE;
    }
    if (needRedraw)
    {
      redraw();
      needRedraw = GL_FALSE;
    }
  }
#endif
  return 0;
}
