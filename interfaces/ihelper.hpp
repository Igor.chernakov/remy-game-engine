#pragma once

#include "../math/math.hpp"

#include "iobject.hpp"

struct IHelper: IObject
{
	virtual void Line(const vec3_t &a, const vec3_t &b, color_t color = color_t(), const mat4_t *matrix = NULL) = 0;
	virtual void Box(const box3_t &box, color_t color = color_t(), const mat4_t *matrix = NULL) = 0;
	virtual void Render(struct IRenderer* renderer) const = 0;
	virtual void Erase() = 0;
};
