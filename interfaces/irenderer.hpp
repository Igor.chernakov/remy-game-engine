#pragma once

#include "../shared/shared.hpp"

#include "iobject.hpp"
#include "itexture.hpp"
#include "irendererbuffer.hpp"
#include "ishader.hpp"
#include "iwindow.hpp"

struct IRenderer: IObject
{
// system operations
#ifdef WIN32
	virtual bool Open(IWindow* window) = 0;
#endif
	virtual void OnActivate(bool active) = 0;
	virtual void TakeScreenshot() = 0;

	enum MatrixType
	{
		MATRIX_PROJECTION,
		MATRIX_MODELVIEW,
		MATRIX_TEXTURE,
	};

	enum EffectType
	{
		EFFECT_BLUR = 0x01,
		EFFECT_CONTRAST = 0x02,
		EFFECT_DARK = 0x04,
	};

	virtual void SetViewport(const rect_t &rect) = 0;
	virtual rect_t GetViewport() = 0;

	virtual void SetMatrix(MatrixType, const float *matrix) = 0;
	virtual void GetMatrix(MatrixType, float *matrix) = 0;

// render operations
	virtual void PushSurface(const struct ISurface *s) = 0;

// buffer operations
	virtual bool CreateBuffer(IRendererBuffer* buffer) = 0;
	virtual void ReleaseBuffer(IRendererBuffer* buffer) = 0;

// texture operations
	virtual bool CreateTexture(ITexture* texture) = 0;
	virtual bool ReleaseTexture(ITexture* texture) = 0;
	virtual bool LoadTexture(ITexture* texture, const char *file_in_memory, int size) = 0;
	virtual void* LockTexture(ITexture* texture) = 0;
	virtual void UnlockTexture(ITexture* texture) = 0;

// shader operations
	virtual bool CompileShader(IShader* shader) = 0;
	virtual bool LoadVertexShader(IShader* shader, const char *mem, int size) = 0;
	virtual bool LoadPixelShader(IShader* shader, const char *mem, int size) = 0;
	virtual bool ReleaseShader(IShader* shader) = 0;

// backbuffer operations
	//virtual bool CopyBackBuffer(const ITexture* texture, const rect_t &rect) = 0;
	virtual ITexture::Format GetBackBufferFormat() = 0;
	virtual dim_t GetBackBufferSize() = 0;

// frame operations
	virtual void StartFrame() = 0;
	virtual void FinishFrame() = 0;

// screen operations
	enum screen_bit_t
	{
		STENCIL_BIT = 0x1,
		DEPTH_BIT = 0x2,
		COLOR_BIT = 0x4
	};

	virtual void ClearScreen(int mask, int color, float depth, int stencil) = 0;
	virtual bool SetFullscreen(bool fullscreen) = 0;
	virtual bool IsFullscreen() const = 0;

// accessors

	struct stats_t
	{
		int frame_num;
		int triangles;
		int drawcalls;
		int memory;
		int default_stencil;
		int default_color;
		int bone_index_stride;
		float default_depth;
		bool needs_ortho_shift;
		bool needs_flip_copybackbuffer;
		std::string name, ps_ext, vs_ext;
	};

	virtual const stats_t& GetStats() = 0;
};
