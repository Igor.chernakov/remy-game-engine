#pragma once

#include <boost/shared_ptr.hpp>

#include "../math/math.hpp"

#include "iobject.hpp"

struct IRoute: IObject
{
	virtual float GetLength() const = 0;
	virtual const vec3_t& GetWaypoint() const = 0;
	virtual const vec3_t& GetDestination() const = 0;
	virtual bool GoToNextWaypoint() = 0;
};

struct INavigation: IObject
{
	virtual boost::shared_ptr<IRoute> GetRoute(const vec3_t& a, const vec3_t& b, bool use_navigation = true) = 0;
	virtual bool IsReachable(const vec3_t& a) const = 0;
};
