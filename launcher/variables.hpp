#pragma once

#include <boost/lexical_cast.hpp>

class Variables: public IVariables
{
private:
	struct Variable
	{
		Variable(const std::string& value = std::string())
			: _string(value)
		{
			_float = ::atof(value.c_str());
			_int = ::atoi(value.c_str());
			_bool = boost::iequals(value, "true") || _int > 0;
		}

		float _float;
		int _int;
		bool _bool;
		std::string _string;
	};

public:
	float GetFloat(const std::string& key) const
	{
		return vars[key]._float;
	}
	int GetInteger(const std::string& key) const
	{
		return vars[key]._int;
	}
	bool GetBoolean(const std::string& key) const
	{
		return vars[key]._bool;
	}
	const std::string& GetString(const std::string& key) const
	{
		return vars[key]._string;
	}
	void SetVariable(const std::string& key, const std::string& value)
	{
		vars[key] = Variable(value);
	}

private:
	mutable std::map<std::string, Variable> vars;
};

static IVariables* CreateVariables()
{
	return new Variables();
}
