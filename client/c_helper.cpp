#include "c_client.hpp"
#include "c_helper.hpp"

void export_Helper()
{
	namespace py = boost::python;

	DISPOSEME(4) py::class_<IHelper, shared_ptr<IHelper>, boost::noncopyable>("Helper", boost::python::no_init)
		.def("Line", &IHelper::Line)
		;
}

namespace client
{
	Helper::Helper()
	{
		Erase();
	}

	Helper::~Helper()
	{
	}

	void Helper::Render(IRenderer* renderer) const
	{
		for (std::list<Surface>::const_iterator it = lines.begin(); it != lines.end(); ++it)
			renderer->PushSurface(&(*it));
	}

	void Helper::Erase()
	{
		matrices.clear();
		lines.clear();

		nVertices = 0;
	}

	void Helper::Box(const box3_t &box, color_t nColor, const mat4_t *matrix)
	{
		Surface buffer;

		buffer.pVertices = &pVertices[nVertices];
		buffer.nVertices = 0;
		buffer.primitive = Surface::LINES;

		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMin.x, box.vMin.y, box.vMin.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMax.x, box.vMin.y, box.vMin.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMax.x, box.vMin.y, box.vMin.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMax.x, box.vMax.y, box.vMin.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMax.x, box.vMax.y, box.vMin.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMin.x, box.vMax.y, box.vMin.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMin.x, box.vMax.y, box.vMin.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMin.x, box.vMin.y, box.vMin.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMin.x, box.vMin.y, box.vMax.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMax.x, box.vMin.y, box.vMax.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMax.x, box.vMin.y, box.vMax.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMax.x, box.vMax.y, box.vMax.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMax.x, box.vMax.y, box.vMax.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMin.x, box.vMax.y, box.vMax.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMin.x, box.vMax.y, box.vMax.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMin.x, box.vMin.y, box.vMax.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMin.x, box.vMin.y, box.vMax.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMin.x, box.vMin.y, box.vMin.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMin.x, box.vMax.y, box.vMax.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMin.x, box.vMax.y, box.vMin.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMax.x, box.vMax.y, box.vMax.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMax.x, box.vMax.y, box.vMin.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMax.x, box.vMin.y, box.vMax.z);
		pVertices[nVertices + buffer.nVertices++].position = vec3_t(box.vMax.x, box.vMin.y, box.vMin.z);

		buffer.SetColor(nColor);

		if (matrix) {
			matrices.push_back(*matrix);
			buffer.pMatrix = &(*(--matrices.end()));
		} else {
			buffer.pMatrix = NULL;
		}

		nVertices += buffer.nVertices;

		lines.push_back(buffer);
	}

	void Helper::Line(const vec3_t &a, const vec3_t &b, color_t nColor, const mat4_t *matrix)
	{
		Surface buffer;

		buffer.pVertices = &pVertices[nVertices];
		buffer.nVertices = 0;
		buffer.primitive = Surface::LINES;

		pVertices[nVertices + buffer.nVertices++].position = a;
		pVertices[nVertices + buffer.nVertices++].position = b;

		buffer.SetColor(nColor);

		if (matrix)
		{
			matrices.push_back(*matrix);
			buffer.pMatrix = &(*(--matrices.end()));
		}
		else
		{
			buffer.pMatrix = NULL;
		}

		nVertices += buffer.nVertices;

		lines.push_back(buffer);
	}
}
