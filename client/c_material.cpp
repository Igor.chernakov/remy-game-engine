/**
	MATERIALS consist from collections of LAYERS with different blending
**/

#include "c_client.hpp"
#include "c_material.hpp"

extern IRenderer* __HackRenderer;

tinyxml2::XMLDocument all_materials;
const tinyxml2::XMLElement *all_materials_root = 0;

std::string Attr_Str(const char* str) {
	return str ? std::string(str) : std::string();
}

#define ATTR(node, name) Attr_Str(node->Attribute(name))

int wildcmp(const char *wild, const char *string) {
	// Written by Jack Handy - jakkhandy@hotmail.com
	const char *cp = NULL, *mp = NULL;

	while ((*string) && (*wild != '*')) {
		if ((*wild != *string) && (*wild != '?')) {
			return 0;
		}
		wild++;
		string++;
	}

	while (*string) {
		if (*wild == '*') {
			if (!*++wild) {
				return 1;
			}
			mp = wild;
			cp = string+1;
		} else if ((*wild == *string) || (*wild == '?')) {
			wild++;
			string++;
		} else {
			wild = mp;
			string = cp++;
		}
	}

	while (*wild == '*') {
		wild++;
	}
	return !*wild;
}

namespace client
{
	static const char* TextureExtensions[] = { ".tga", ".jpg", ".jpeg", ".bmp", "" };

	class Texture: public ITexture
	{
	public:
		Texture(const char* mem, size_t size)
			: is_loaded(false)
			, is_raw(false)
			, mem(0)
			, size(0)
		{
			if (mem && size)
			{
				this->mem = new char[size];
				this->size = size;
				memcpy(this->mem, mem, size);
			}
		}

		Texture(int width, int height, ITexture::Format format = ITexture::FMT_UNKNOWN, const char* mem = 0)
			: is_loaded(false)
			, is_raw(true)
			, mem(0)
			, size(0)
		{
			this->width = width;
			this->height = height;
			this->format = format;
			this->miplevels = 1;

			this->size = width * height * GetBitDepth() / 8;

			if (mem)
			{
				this->mem = new char[this->size];
				memcpy(this->mem, mem, this->size);
			}
		}

		Texture(const Texture* other)
		{
			this->is_loaded = false;
			this->is_raw = other->is_raw;
			this->width = other->width;
			this->height = other->height;
			this->format = other->format;
			this->miplevels = other->miplevels;
			this->size = other->size;

			if (this->size > 0)
			{
				this->mem = new char[this->size];
				memcpy(this->mem, other->mem, this->size);
			}
			else
			{
				this->mem = 0;
			}
		}

		void Update(IRenderer* renderer)
		{
			is_loaded = true;

			if (format == FMT_UNKNOWN)
			{
				format = renderer->GetBackBufferFormat();
			}

			bool success = false;

			if (!is_raw) {
				success = renderer->LoadTexture(this, mem, size);
			} else {
				success = renderer->CreateTexture(this);

				void *lock = 0;

				if (mem) {
					if (lock = renderer->LockTexture(this)) {
						memcpy(lock, mem, size);
						renderer->UnlockTexture(this);
					} else {
						success = false;
					}
				}
			}
		}

		void Unload(IRenderer* renderer)
		{
			if (IsLoaded())
				renderer->ReleaseTexture(this);
			is_loaded = false;
		}

		~Texture()
		{
			if (mem)
				delete[] mem;
		}

 		inline bool IsLoaded() const
		{
			return is_loaded;
		}

	private:
		char* mem;
		size_t size;
		bool is_loaded, is_raw;
	};

	class RendererBuffer
		: public IRendererBuffer
	{
		void *mem;
		size_t size;
		bool is_loaded;

	public:
		RendererBuffer(Type type, const char* mem, size_t size)
			: size(size)
			, mem(0)
			, is_loaded(false)
		{
			this->type = type;

			if (mem)
			{
				this->mem = new char[this->size];
				memcpy(this->mem, mem, this->size);
			}
		}
		~RendererBuffer()
		{
			if (mem)
			{
				delete[] mem;
			}
		}

		void Update(IRenderer* renderer)
		{
			if (mem && renderer->CreateBuffer(this))
			{
				is_loaded = true;
			}
		}

		void Unload(IRenderer* renderer)
		{
			if (IsLoaded())
			{
				renderer->ReleaseBuffer(this);
			}
			is_loaded = false;
		}

		bool IsLoaded() const
		{
			return is_loaded;
		}

		void* GetDataPointer() const
		{
			return mem;
		}

		size_t GetSize() const
		{
			return size;
		}
	};

	class Shader
		: public IShader
		, public IResource
	{
	public:
		Shader(const std::string& filename)
			: m_name(filename)
			, is_loaded(false)
		{
		}

		void Update(IRenderer* renderer)
		{
			is_loaded = true;

			if (file_t file = framework->GetFileSystem()->LoadFile(GetPath() + __HackRenderer->GetStats().vs_ext)) {
				char* mem = new char[file.size];
				fread(mem, file.size, 1, file);
				renderer->LoadVertexShader(this, mem, file.size);
				delete[] mem;
				file.close();
			}

			if (file_t file = framework->GetFileSystem()->LoadFile(GetPath() + __HackRenderer->GetStats().ps_ext)) {
				char* mem = new char[file.size];
				fread(mem, file.size, 1, file);
				renderer->LoadPixelShader(this, mem, file.size);
				delete[] mem;
				file.close();
			}

			renderer->CompileShader(this);
		}

		void Unload(IRenderer* renderer)
		{
			if (IsLoaded())
				renderer->ReleaseShader(this);
			is_loaded = false;
		}

 		bool IsLoaded() const
		{
			return is_loaded;
		}

	private:
		bool is_loaded;
		std::string m_name;
		const std::string& GetPath() const { return m_name; }
	};

	class RendererObjectManager
		: public IRendererObjectManager
		, private Manager<IRendererObject>
	{
	public:
		RendererObjectManager()
		{
		}

	#pragma pack (push, 1)
		struct TgaHeader {
			BYTE id_length, colormap_type, image_type;
			WORD colormap_index, colormap_length;
			BYTE colormap_size;
			WORD x_origin, y_origin, width, height;
			BYTE pixel_size, attributes;
		};
	#pragma pack (pop, 1)

		shared_ptr<const ITexture> LoadTexture(const std::string& filename)
		{
			if (IsLoaded(filename))
			{
				return static_pointer_cast<const ITexture>(_Data[filename]);
			}
			else
			{
				shared_ptr<Texture> texture;

				if (file_t file = framework->GetFileSystem()->LoadFile(filename)) {
					char* mem = new char[file.size];
					fread(mem, file.size, 1, file);

					texture.reset(new Texture(mem, file.size));

					if (stricmp(f_Extension(filename).c_str(), ".tga") == 0) {
						const TgaHeader* header = (const TgaHeader*)mem;
						texture->width = header->width;
						texture->height = header->height;
					}

					file.close();
					delete[] mem;
				}

				Append(filename, texture);
				return texture;
			}
		}

		shared_ptr<const ITexture> LoadTexture(const char* mem, size_t size)
		{
			shared_ptr<Texture> texture(new Texture(mem, size));
			AddObject(texture);
			return texture;
		}

		shared_ptr<const ITexture> CreateTexture(int width, int height, ITexture::Format format, ITexture::Pool pool, ITexture::Usage usage, const char* mem)
		{
			shared_ptr<Texture> texture(new Texture(width, height, format, mem));
			texture->usage = usage;
			texture->pool = pool;
			AddObject(texture);
			return texture;
		}

		shared_ptr<const ITexture> CreateRenderTarget(int width, int height)
		{
			shared_ptr<Texture> texture(new Texture(width, height));
			texture->usage = ITexture::USAGE_DYNAMIC;
			texture->pool = ITexture::POOL_DEFAULT;
			AddObject(texture);
			return texture;
		}

		shared_ptr<const ITexture> CopyTexture(const ITexture* other)
		{
			shared_ptr<Texture> texture(new Texture((const Texture*)other));
			AddObject(texture);
			return texture;
		}

		shared_ptr<const IShader> LoadShader(const std::string& filename)
		{
			if (IsLoaded(filename))
			{
				return static_pointer_cast<const IShader>(_Data[filename]);
			}
			else
			{
				shared_ptr<Shader> shader(new Shader(filename));
				Append(filename, shader);
				return shader;
			}
		}

		boost::shared_ptr<const IRendererBuffer> CreateBuffer(IRendererBuffer::Type type, const char* mem, size_t size)
		{
			shared_ptr<RendererBuffer> buffer(new RendererBuffer(type, mem, size));
			AddObject(buffer);
			return buffer;
		}

		void AddObject(shared_ptr<const IRendererObject> rendererobject)
		{
			m_rendererobjects.push_back(const_pointer_cast<IRendererObject>(rendererobject));
		}

		void Update(IRenderer* renderer)
		{
			for (DataT::iterator _it = _Data.begin(); _it != _Data.end();)
			{
				int use_count = _it->second.use_count();
				if (use_count == 1) //it's not used anymore, let's remove it
				{
					if (_it->second->IsLoaded())
						const_pointer_cast<IRendererObject>(_it->second)->Unload(renderer);

					_Data.erase(_it++);
				}
				else
				{
					if (!_it->second->IsLoaded())
						const_pointer_cast<IRendererObject>(_it->second)->Update(renderer);

					++_it;
				}
			}

			for (RendererObjects::iterator _it = m_rendererobjects.begin(); _it != m_rendererobjects.end();)
			{
				int use_count = _it->use_count();
				if (use_count == 1) //it's not used anymore, let's remove it
				{
					if ((*_it)->IsLoaded())
					{
						const_pointer_cast<IRendererObject>(*_it)->Unload(renderer);
					}

					_it = m_rendererobjects.erase(_it);
				}
				else
				{
					if (!(*_it)->IsLoaded())
					{
						const_pointer_cast<IRendererObject>(*_it)->Update(renderer);
					}

					++_it;
				}
			}
		}

		void Unload(IRenderer* renderer)
		{
			for (DataT::iterator _it = _Data.begin(); _it != _Data.end(); ((IRendererObject*)(_it++)->second.get())->Unload(renderer));
			std::for_each(m_rendererobjects.begin(), m_rendererobjects.end(), boost::bind(&IRendererObject::Unload, _1, renderer));
		}

		void Clear(IRenderer* renderer)
		{
			Unload(renderer);
			_Data.clear();
			m_rendererobjects.clear();
		}


	private:
		typedef std::list<shared_ptr<IRendererObject> > RendererObjects;
		RendererObjects m_rendererobjects;
	};


	struct Layer: ILayer
	{
		Layer()
		{
			blend.src = BLEND_ONE;
			blend.dst = BLEND_ZERO;
			filter.mag = FILTER_LINEAR;
			filter.min = FILTER_LINEAR;
			filter.mip = FILTER_NONE;
			address = ADDRESS_CLAMP;
			tcgen = TCGEN_VERTEX;
			type = TYPE_DEFAULT;
			anim_speed = 0;
			flags = 0;
		}

		~Layer()
		{
		}

		int GetTexturesCount() const
		{
			return textures.size();
		}

		const ITexture* GetTexture(int i) const
		{
			return textures.size() ? textures[i % textures.size()].get() : 0;
		}

		int GetWidth() const
		{
			return textures.size() ? textures.front()->width : 32;
		}

		int GetHeight() const
		{
			return textures.size() ? textures.front()->height : 32;
		}

		typedef std::vector<shared_ptr<const ITexture> > Textures;
		Textures textures;
		int flags;
	};

	const tinyxml2::XMLElement* MaterialsListFind(const std::string& path) {
		std::string filename = path;
		std::replace(filename.begin(), filename.end(), '\\', '/');
		XML_FOREACH(mat, "material", all_materials_root) {
			std::string name = ATTR(mat, "name");
			if (name.length() && wildcmp(name.c_str(), filename.c_str()))
				return mat;
		}
		return 0;
	}

	/**
		Various STRING-TO-VALUE converters; Easy to append and use
	**/

	namespace parsers
	{
		template <typename T>
		struct Parser: std::map<
			std::string,
			T,
			boost::function<
				bool(const std::string&, const std::string&)
				>
			>
		{
			struct ignorecase_predicate
			{
				bool operator()(const std::string& a, const std::string& b) const
				{
					return boost::algorithm::lexicographical_compare(a, b, boost::is_iless());
				}
			};

			Parser()
				: std::map<
					std::string,
					T,
					boost::function<
						bool(const std::string&, const std::string&)
						>
					>(ignorecase_predicate())
			{
			}

			Parser& def(const std::string& key, const T& value)
			{
				(*this)[key] = value;
				return *this;
			}

			bool apply(const std::string& value, T* target) const
			{
				if (this->find(value) != this->end())
				{
					*target = this->find(value)->second;
					return true;
				}
				else
				{
					return false;
				}
			}

			template <class Y>
			bool apply(const std::string& value, Y* target, void (Y::*function)(T)) const
			{
				if (this->find(value) != this->end())
				{
					(target->*function)(this->find(value)->second);
					return true;
				}
				else
				{
					return false;
				}
			}
		};

		//helper function for blend shortcuts
		static ILayer::Blending MakeBlending(ILayer::Blend source, ILayer::Blend destination)
		{
			ILayer::Blending blend;
			blend.src = source;
			blend.dst = destination;
			return blend;
		}

		Parser<bool> boolean = Parser<bool>()
			.def("TRUE", true)
			.def("FALSE", false);

		Parser<IMaterial::Pass> pass = Parser<IMaterial::Pass>()
			.def("SOLID", IMaterial::RENDER_SOLID)
			.def("ALPHA", IMaterial::RENDER_ALPHA)
			.def("PLANARSHADOWS", IMaterial::RENDER_PLANARSHADOWS)
			.def("VOLUMESHADOWS", IMaterial::RENDER_VOLUMESHADOWS)
			.def("SHADOWMAP", IMaterial::RENDER_SHADOWMAP)
			.def("TEXTURE", IMaterial::RENDER_TEXTURE)
			.def("STENCIL1", IMaterial::RENDER_INNER)
			.def("STENCIL2", IMaterial::RENDER_OUTER)
			.def("INTERFACE", IMaterial::RENDER_INTERFACE);

		Parser<IMaterial::CullFace> culling = Parser<IMaterial::CullFace>()
			.def("NONE", IMaterial::CULL_NONE)
			.def("FRONT", IMaterial::CULL_FRONT)
			.def("BACK", IMaterial::CULL_BACK);

		Parser<IMaterial::Func> function = Parser<IMaterial::Func>()
			.def("LESS", IMaterial::FUNC_LESS)
			.def("EQUAL", IMaterial::FUNC_EQUAL)
			.def("LESSEQUAL", IMaterial::FUNC_LESSEQUAL)
			.def("GREATER", IMaterial::FUNC_GREATER)
			.def("NOTEQUAL", IMaterial::FUNC_NOTEQUAL)
			.def("GREATEREQUAL", IMaterial::FUNC_GREATEREQUAL)
			.def("ALWAYS", IMaterial::FUNC_ALWAYS);

		Parser<IMaterial::StencilOp> operation = Parser<IMaterial::StencilOp>()
			.def("KEEP", IMaterial::OP_KEEP)
			.def("ZERO", IMaterial::OP_ZERO)
			.def("REPLACE", IMaterial::OP_REPLACE)
			.def("INCREMENT", IMaterial::OP_INCR)
			.def("DECREMENT", IMaterial::OP_DECR);

		Parser<IMaterial::SurfaceFlags> surface_flags = Parser<IMaterial::SurfaceFlags>()
			.def("NO_SHADOW", IMaterial::SF_NO_SHADOW)
			.def("NO_SOLID", IMaterial::SF_NO_SOLID)
			.def("NO_BBOX", IMaterial::SF_NO_BBOX)
			.def("NO_MULTITEXTURE", IMaterial::SF_NO_MULTITEXTURE)
			.def("NO_SOLID_BUT_ALPHA", IMaterial::SF_NO_SOLID_BUT_ALPHA)
			.def("NO_LIGHT", IMaterial::SF_NO_LIGHT)
			.def("NO_ALIASING", IMaterial::SF_NO_ALIASING);

		Parser<ILayer::Blending> blend_shortcut = Parser<ILayer::Blending>()
			.def("NONE", MakeBlending(ILayer::BLEND_ONE, ILayer::BLEND_ZERO))
			.def("BLEND", MakeBlending(ILayer::BLEND_SRCALPHA, ILayer::BLEND_INVSRCALPHA))
			.def("MODULATE", MakeBlending(ILayer::BLEND_ZERO, ILayer::BLEND_SRCCOLOR))
			.def("MODULATE2X", MakeBlending(ILayer::BLEND_DESTCOLOR, ILayer::BLEND_SRCCOLOR))
			.def("ADD", MakeBlending(ILayer::BLEND_SRCALPHA, ILayer::BLEND_ONE));

		Parser<ILayer::Blend> blend = Parser<ILayer::Blend>()
			.def("ZERO", ILayer::BLEND_ZERO)
			.def("ONE", ILayer::BLEND_ONE)
			.def("SRCCOLOR", ILayer::BLEND_SRCCOLOR)
			.def("INVSRCCOLOR", ILayer::BLEND_INVSRCCOLOR)
			.def("SRCALPHA", ILayer::BLEND_SRCALPHA)
			.def("INVSRCALPHA", ILayer::BLEND_INVSRCALPHA)
			.def("DESTALPHA", ILayer::BLEND_DESTALPHA)
			.def("INVDESTALPHA", ILayer::BLEND_INVDESTALPHA)
			.def("DESTCOLOR", ILayer::BLEND_DESTCOLOR)
			.def("INVDESTCOLOR", ILayer::BLEND_INVDESTCOLOR)
			.def("SRCALPHASAT", ILayer::BLEND_SRCALPHASAT)
			.def("BOTHSRCALPHA", ILayer::BLEND_BOTHSRCALPHA)
			.def("BOTHINVSRCALPHA", ILayer::BLEND_BOTHINVSRCALPHA);

		Parser<ILayer::Filter> filter = Parser<ILayer::Filter>()
			.def("NONE", ILayer::FILTER_NONE)
			.def("POINT", ILayer::FILTER_POINT)
			.def("LINEAR", ILayer::FILTER_LINEAR)
			.def("ANISOTROPIC", ILayer::FILTER_ANISOTROPIC)
			.def("FLATCUBIC", ILayer::FILTER_FLATCUBIC)
			.def("GAUSSIANCUBIC", ILayer::FILTER_GAUSSIANCUBIC);

		Parser<ILayer::Addressing> address = Parser<ILayer::Addressing>()
			.def("REPEAT", ILayer::ADDRESS_REPEAT)
			.def("CLAMP", ILayer::ADDRESS_CLAMP);

		Parser<ILayer::Type> type = Parser<ILayer::Type>()
			.def("$DEFAULT", ILayer::TYPE_DEFAULT)
			.def("$LIGHTMAP", ILayer::TYPE_LIGHTMAP);

		Parser<ILayer::TcGen> tcgen = Parser<ILayer::TcGen>()
			.def("VERTEX", ILayer::TCGEN_VERTEX)
			.def("ENVIRONMENT", ILayer::TCGEN_ENVIRONMENT);

	}


	bool ReadBlending(const std::string& value, ILayer::Blending& blend)
	{
		std::list<std::string> blend_values;
		boost::split(blend_values, value, boost::is_space());

		switch (blend_values.size())
		{
		case 1:
			parsers::blend_shortcut.apply(*blend_values.begin(), &blend);
			return true;
		case 2:
			parsers::blend.apply(*blend_values.begin(), &blend.src);
			parsers::blend.apply(*++blend_values.begin(), &blend.dst);
			return true;
		default:
			return false;
		}
	}

	static shared_ptr<const ITexture> LoadRegularTexture(const std::string& filename)
	{
		return framework->GetRendererObjectManager()->LoadTexture(filename);
	}

	Material::Material()
	{
		Reset();
	}

	Material::Material(const std::string& path, shared_ptr<const ITexture> default_texture)
	{
		Reset();
		Read(path, default_texture);
	}

	void Material::Reset()
	{
		cullface = CULL_BACK;
		surfaceflags = 0;
		stencil.front.func = FUNC_UNKNOWN;
		stencil.front.ref = 0;
		stencil.front.pass = OP_KEEP;
		stencil.front.fail = OP_KEEP;
		stencil.back.func = FUNC_UNKNOWN;
		stencil.back.ref = 0;
		stencil.back.pass = OP_KEEP;
		stencil.back.fail = OP_KEEP;
		alphafunc.func = FUNC_UNKNOWN;
		alphafunc.ref = 0;
		depthfunc.func = FUNC_LESSEQUAL;
		depthfunc.mask = true;
		colormask = true;
		renderpass = RENDER_SOLID;
		textureloader = &LoadRegularTexture;
	}

	void Material::Read(const std::string& path, shared_ptr<const ITexture> default_texture)
	{
		m_name = path;

		//check for default materials

		if (stricmp(f_Name(path).c_str(), "$BLANK") == 0)
		{
			m_layers.push_back(shared_ptr<Layer>(new Layer()));
			return;
		}
		else if (stricmp(f_Name(path).c_str(), "$NODRAW") == 0)
		{
			surfaceflags = SF_NO_SHADOW;
			return;
		}
		else if (stricmp(f_Name(path).c_str(), "$DEPTHONLY") == 0)
		{
			surfaceflags = SF_NO_SHADOW;
			colormask = false;
			m_layers.push_back(shared_ptr<Layer>(new Layer()));
			return;
		}

		//try to load material from manager or from disk

		if (!all_materials_root) {
			all_materials.LoadFile("materials/materials.xml");
			all_materials_root = all_materials.FirstChildElement("xml");
		}

		if (const tinyxml2::XMLElement* node = MaterialsListFind(path)) {
			Read(node, default_texture);
		} else {
			shared_ptr<Layer> layer(new Layer);
			if (!AddTextureToLayer(layer.get(), GetPath(), default_texture))
			{
				throw std::runtime_error(("Error loading material " + GetPath() + ".").c_str());
			}
			m_layers.push_back(layer);
		}
	}

	void Material::Read(const tinyxml2::XMLElement *node, shared_ptr<const ITexture> default_texture)
	{
		std::list<std::string> surface_flags, stencil_func, alpha_func;

		std::string worker;

		boost::split(surface_flags, worker = ATTR(node, "flags"), boost::is_space());
		boost::split(stencil_func, worker = ATTR(node, "stencilfunc"), boost::is_space());
		boost::split(alpha_func, worker = ATTR(node, "alphafunc"), boost::is_space());

		struct apply_flags_to
		{
			Material* self;
			apply_flags_to(Material* self): self(self) {}
			void operator()(const std::string& flag) const
			{
				if (parsers::surface_flags.find(flag) != parsers::surface_flags.end())
				{
					self->surfaceflags = self->surfaceflags | parsers::surface_flags.find(flag)->second;
				}
			}
		};

		switch (stencil_func.size())
		{
		case 2:
			stencil.front.ref = (unsigned char)atoi((*++stencil_func.begin()).c_str());
		case 1:
			parsers::function.apply(*stencil_func.begin(), &stencil.front.func);
			break;
		};

		switch (alpha_func.size())
		{
		case 2:
			alphafunc.ref = (unsigned char)atoi((*++alpha_func.begin()).c_str());
		case 1:
			parsers::function.apply(*alpha_func.begin(), &alphafunc.func);
			break;
		};

		std::for_each(surface_flags.begin(), surface_flags.end(), apply_flags_to(this));

		parsers::pass.apply(ATTR(node, "renderpass"), &renderpass);
		parsers::culling.apply(ATTR(node, "cullface"), &cullface);
		parsers::boolean.apply(ATTR(node, "colormask"), &colormask);
		parsers::boolean.apply(ATTR(node, "depthmask"), &depthfunc.mask);
		parsers::function.apply(ATTR(node, "depthfunc"), &depthfunc.func);
		parsers::operation.apply(ATTR(node, "stencilpass"), &stencil.front.pass);
		parsers::operation.apply(ATTR(node, "stencilfail"), &stencil.front.fail);

#ifdef USE_GPU_SHADERS
		std::string shadername = ATTR(node, "shader");

		if (shadername.size() > 0)
		{
			m_shader = framework->GetRendererObjectManager()->LoadShader(shadername);
		}
#endif

		XML_FOREACH(xmlLayer, "layer", node) {
			shared_ptr<Layer> layer(new Layer());

			ReadBlending(ATTR(xmlLayer, "blendfunc"), layer->blend);

			parsers::address.apply(ATTR(xmlLayer, "addressing"), &layer->address);
			parsers::filter.apply(ATTR(xmlLayer, "magfilter"), &layer->filter.mag);
			parsers::filter.apply(ATTR(xmlLayer, "minfilter"), &layer->filter.min);
			parsers::filter.apply(ATTR(xmlLayer, "mipfilter"), &layer->filter.mip);
			parsers::tcgen.apply(ATTR(xmlLayer, "tcgen"), &layer->tcgen);

			layer->anim_speed = 0;

			xmlLayer->QueryFloatAttribute("animspeed", &layer->anim_speed);

			if (layer->tcgen == Layer::TCGEN_ENVIRONMENT)
				textureloader = &LoadRegularTexture;

			if (!parsers::type.apply(ATTR(xmlLayer, "map"), &layer->type))
				AddTextureToLayer(layer.get(), ATTR(xmlLayer, "map"));

			if (!layer->GetTexturesCount() && layer->type == ILayer::TYPE_DEFAULT)
				AddTextureToLayer(layer.get(), GetPath(), default_texture);

			m_layers.push_back(layer);
		}
	}

	ILayer* Material::AddLayer(const std::string& texture_filename)
	{
		shared_ptr<Layer> layer(new Layer());
		AddTextureToLayer(layer.get(), texture_filename);
		m_layers.push_back(layer);
		return layer.get();
	}

	ILayer* Material::AddLayer(shared_ptr<const ITexture> texture)
	{
		shared_ptr<Layer> layer(new Layer());
		AddTextureToLayer(layer.get(), "", texture);
		m_layers.push_back(layer);
		return layer.get();
	}

	ILayer* Material::AddLayer()
	{
		shared_ptr<Layer> layer(new Layer());
		m_layers.push_back(layer);
		return layer.get();
	}

	bool Material::AddTextureToLayer(ILayer* layer_, const std::string& filename, shared_ptr<const ITexture> _default) const
	{
		if (filename.empty())
			return false;

		Layer* layer = dynamic_cast<Layer*>(layer_);

		if (!layer)
			return false;

		if (_default) {
			layer->textures.push_back(_default);
			return true;
		}

		for (const char** ext = TextureExtensions; **ext; ++ext)
		{
			std::string f = f_StripExtension(filename) + *ext;

			if (framework->GetFileSystem()->CheckFile(f))
			{
				layer->textures.push_back(textureloader(f));
				return true;
			}
		}

		return false;
	}

	int Material::GetWidth() const
	{
		return m_layers.size() ? GetLayer(0)->GetWidth() : 1;
	}

	int Material::GetHeight() const
	{
		return m_layers.size() ? GetLayer(0)->GetHeight() : 1;
	}

	bool Material::DuplicateLayer(int i)
	{
		if (m_layers.size() <= i)
			return false;

		m_layers[i].reset(new Layer(*(Layer*)(m_layers[i].get())));

		return true;
	}

}

IRendererObjectManager* CreateRendererObjectManager()
{
	return new client::RendererObjectManager();
}
