#include "c_client.hpp"
#include "c_steam.hpp"

#include "../steam/steam_api.h"

#define toLogEx(...) framework->Log(IFramework::MSG_LOG, __VA_ARGS__)

namespace Steam
{

#if defined(__APPLE__) && TARGET_OS_IPHONE

	void init() {
	}
	
	bool isOverlaysEnabled() {
		return false;
	}
	
	void showOverlay(int id) {
	}
	
	void showWebPage(int id) {
	}
	
	void shutdown()	{
	}
	
	void runCallbacks() {
	}
	
	std::string getUserName() {
	}
	
	void setAchievement(const std::string &ach) {
	}
	
	void clearAchievement(const std::string &ach) {
	}
	
	std::string getLanguage() {
		return "en";
	}
	
	bool GetStatus() {
		return false;
	}
	
#else
	
#define _ACH_ID( id, name ) { id, #id, name, "", 0, 0 }
struct Achievement_t
{
	int m_eAchievementID;
	const char *m_pchAchievementID;
	char m_rgchName[128];
	char m_rgchDescription[256];
	bool m_bAchieved;
	int m_iIconImage;
};

bool gSteamStatus = false;

class CSteamAchievements
{
private:
	int64 m_iAppID; // Our current AppID
	Achievement_t *m_pAchievements; // Achievements data
	int m_iNumAchievements; // The number of Achievements
	bool m_bInitialized; // Have we called Request stats and received the callback?

	// Steam User interface
	ISteamUser *m_pSteamUser;

	// Steam UserStats interface
	ISteamUserStats *m_pSteamUserStats;


public:
	CSteamAchievements(/*Achievement_t *Achievements, int NumAchievements*/);
	~CSteamAchievements() {}

	bool RequestStats();
	bool SetAchievement(const char* ID);
	bool ClearAchievement(const char* ID);

	STEAM_CALLBACK( CSteamAchievements, OnUserStatsReceived, UserStatsReceived_t,
		m_CallbackUserStatsReceived );
	STEAM_CALLBACK( CSteamAchievements, OnUserStatsStored, UserStatsStored_t,
		m_CallbackUserStatsStored );
	STEAM_CALLBACK( CSteamAchievements, OnAchievementStored,
		UserAchievementStored_t, m_CallbackAchievementStored );
};

CSteamAchievements::CSteamAchievements(/*Achievement_t *Achievements, int NumAchievements*/):
 m_iAppID( 0 ),
 m_bInitialized( false ),
 m_CallbackUserStatsReceived( this, &CSteamAchievements::OnUserStatsReceived ),
 m_CallbackUserStatsStored( this, &CSteamAchievements::OnUserStatsStored ),
 m_CallbackAchievementStored( this, &CSteamAchievements::OnAchievementStored )
{
	m_pSteamUser = SteamUser();
	m_pSteamUserStats = SteamUserStats();


    m_iAppID = SteamUtils()->GetAppID();
    /* m_pAchievements = Achievements;
     m_iNumAchievements = NumAchievements;*/
    RequestStats();
}

bool CSteamAchievements::RequestStats()
{
	// Is Steam loaded? If not we can't get stats.
	if ( NULL == SteamUserStats() || NULL == SteamUser() )
	{
		return false;
	}
	// Is the user logged on?  If not we can't get stats.
	if ( !SteamUser()->BLoggedOn() )
	{
		return false;
	}
	// Request user stats.
	bool bSuccess =  SteamUserStats()->RequestCurrentStats();

	return bSuccess;
}

bool CSteamAchievements::ClearAchievement(const char* ID)
{
	// Have we received a call back from Steam yet?
	if (m_bInitialized)
	{
		SteamUserStats()->ClearAchievement(ID);
		return SteamUserStats()->StoreStats();
	}
	// If not then we can't set achievements yet
	return false;
}

bool CSteamAchievements::SetAchievement(const char* ID)
{
	// Have we received a call back from Steam yet?
	if (m_bInitialized)
	{
		SteamUserStats()->SetAchievement(ID);
		return SteamUserStats()->StoreStats();
	}
	// If not then we can't set achievements yet
	return false;
}

void CSteamAchievements::OnUserStatsReceived( UserStatsReceived_t *pCallback )
{
 // we may get callbacks for other games' stats arriving, ignore them
 if ( m_iAppID == pCallback->m_nGameID )
 {
   if ( k_EResultOK == pCallback->m_eResult )
   {
     //OutputDebugString("Received stats and achievements from Steam");
     m_bInitialized = true;

     // load achievements
     /*for ( int iAch = 0; iAch < m_iNumAchievements; ++iAch )
     {
       Achievement_t &ach = m_pAchievements[iAch];

       SteamUserStats()->GetAchievement(ach.m_pchAchievementID, &ach.m_bAchieved);
       _snprintf( ach.m_rgchName, sizeof(ach.m_rgchName), "%s",
          SteamUserStats()->GetAchievementDisplayAttribute(ach.m_pchAchievementID,
          "name"));
       _snprintf( ach.m_rgchDescription, sizeof(ach.m_rgchDescription), "%s",
          SteamUserStats()->GetAchievementDisplayAttribute(ach.m_pchAchievementID,
          "desc"));
     }*/

	 //ClearAchievement("NEW_ACHIEVEMENT_1_0");
   }
   else
   {
     char buffer[128];
     _snprintf( buffer, 128, "RequestStats - failed, %d", pCallback->m_eResult );
     //OutputDebugString( buffer );
   }
 }
}

void CSteamAchievements::OnUserStatsStored( UserStatsStored_t *pCallback )
{
 // we may get callbacks for other games' stats arriving, ignore them
 if ( m_iAppID == pCallback->m_nGameID )
 {
   if ( k_EResultOK == pCallback->m_eResult )
   {
     //OutputDebugString( "Stored stats for Steam" );
   }
   else
   {
     char buffer[128];
     _snprintf( buffer, 128, "StatsStored - failed, %d", pCallback->m_eResult );
     //OutputDebugString( buffer );
   }
 }
}

void CSteamAchievements::OnAchievementStored( UserAchievementStored_t *pCallback )
{
     // we may get callbacks for other games' stats arriving, ignore them
     if ( m_iAppID == pCallback->m_nGameID )
     {
          //OutputDebugString( "Stored Achievement for Steam" );
     }
}

CSteamAchievements*	g_SteamAchievements = NULL;

 //--------------------------------



void init()
{
 	gSteamStatus = SteamAPI_Init();



	if(!gSteamStatus)
	{
		toLogEx("STEAM: Can't init steam!");
		return;
	}

	g_SteamAchievements = new CSteamAchievements(/*g_Achievements, 4*/);

	toLogEx("STEAM: Steam initialized!");
	toLogEx("STEAM: Player name: %s", getUserName().c_str());
	toLogEx("STEAM: Player language: %s", getLanguage().c_str());

	//setAchievement("NEW_ACHIEVEMENT_1_0");
}

bool isOverlaysEnabled()
{
	if(!gSteamStatus)
		return false;

	return SteamUtils()->IsOverlayEnabled();
}


void showOverlay(int id)
{
	if(!gSteamStatus)
		return;

	SteamFriends()->ActivateGameOverlay( "Achievements" );
}

void showWebPage(int id)
{
	if(!gSteamStatus)
		return;

	SteamFriends()->ActivateGameOverlayToWebPage("http://steamcommunity.com/profiles/76561198113899651/myworkshopfiles/?section=greenlight");
}

void shutdown()
{
	if(!gSteamStatus)
		return;

	SteamAPI_Shutdown();

	if (g_SteamAchievements)
		delete g_SteamAchievements;
}

void runCallbacks()
{
	if(!gSteamStatus)
		return;

	SteamAPI_RunCallbacks();
}

std::string getUserName()
{
	if(!gSteamStatus)
		return 0;

	const char *n = SteamFriends()->GetPersonaName();
	return n;
}

//NEW_ACHIEVEMENT_1_0...17

void setAchievement(const std::string &ach)
{
	if(!gSteamStatus)
		return;

	if(!g_SteamAchievements)
		return;

	g_SteamAchievements->SetAchievement(ach.c_str());
}

void clearAchievement(const std::string &ach)
{
	if(!gSteamStatus)
		return;

	if(!g_SteamAchievements)
		return;

	g_SteamAchievements->ClearAchievement(ach.c_str());
}

std::string getLanguage()
{
	if(!gSteamStatus)
		return "en";

	const char *l2 = SteamApps()->GetAvailableGameLanguages();
	const char *l = SteamApps()->GetCurrentGameLanguage();

	toLogEx("STEAM: %s!",l2);
	toLogEx("STEAM: %s!",l);

	const char *lang[] = {"english","german","french","italian","spanish","russian","portuguese","japanese","czech","polish",0};
	const char *slang[] = {"en",    "de",    "fr",    "it",     "es",     "ru",     "pt",        "jp",      "cz",   "pl"};

	for(int i = 0; lang[i] != 0; i++)
		if(strcmp(l,lang[i]) == 0)
			return slang[i];

	return "en";
}

bool GetStatus() {
	return gSteamStatus;
}
	
#endif
	
	
}

namespace py = boost::python;

void export_Steam() {
	py::def("Init", &Steam::init);
	py::def("Shutdown", &Steam::shutdown);
	py::def("GetUserName", &Steam::getUserName);
	py::def("SetAchievement", &Steam::setAchievement);
	py::def("ClearAchievement", &Steam::clearAchievement);
	py::def("GetLanguage", &Steam::getLanguage);
	py::def("ShowOverlay", &Steam::showOverlay);
	py::def("ShowWebPage", &Steam::showWebPage);
	py::def("IsOverlaysEnabled", &Steam::isOverlaysEnabled);
	py::def("GetStatus", &Steam::GetStatus);
}
