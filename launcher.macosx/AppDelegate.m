#import "AppDelegate.h"

int desktop_width = 0;
int desktop_height = 0;

extern int screen_x, screen_y;

@implementation AppDelegate

@synthesize window;

- (void)dealloc
{
    [super dealloc];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication
{
    return YES;
}

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	NSRect mainDisplayRect = [[NSScreen mainScreen] frame];
	
	[[self window] setStyleMask:NSTitledWindowMask|NSClosableWindowMask];
	[[self window] setAcceptsMouseMovedEvents:YES];
	[[self window] setContentSize:NSMakeSize(1024, 768)];
	
	desktop_width = mainDisplayRect.size.width;
	desktop_height = mainDisplayRect.size.height;
}


@end
